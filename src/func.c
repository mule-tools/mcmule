#include <string.h>

extern double (*__functions_MOD_matel0)();
extern double (*__functions_MOD_matel1)();
extern double (*__functions_MOD_matel2)();
extern double (*__functions_MOD_matel_s)();
extern double (*__functions_MOD_matel_sh)();
extern double (*__functions_MOD_matel_hs)();
extern double (*__functions_MOD_matel_ss)();
extern double* (*__user_dummy_MOD_quant)();

extern void* (*__functions_MOD_partfunc)();

void set_func_(char*cwhat, void*dest,int size){
  int what=0;
  while(size){
    what <<= 1; size--;
    if(*cwhat++ == '1') what^=1;
  }
  switch(what){
    case 0b00000000:
      __functions_MOD_matel0 = dest;
      break;
    case 0b00000001:
      __functions_MOD_matel1 = dest;
      break;
    case 0b00000010:
      __functions_MOD_matel2 = dest;
      break;
    case 0b10000001:
      __functions_MOD_matel_s = dest;
      break;
    case 0b10000010:
      __functions_MOD_matel_hs = dest;
      break;
    case 0b10000100:
      __functions_MOD_matel_sh = dest;
      break;
    case 0b10000110:
      __functions_MOD_matel_ss = dest;
      break;

    case 0b11111111:
      __functions_MOD_partfunc = dest;
      break;
  }
}

void set_quant_(void *what)
{
  __user_dummy_MOD_quant = what;
}


#ifndef test
#include "mcmule.h"
void __user_dummy_MOD_set_observable(int*,int*,double*,double*,quantfunc,usereventfunc,inituserfunc,int*,int*,int*);
void __mcmule_MOD_runmcmule(int*,int*,int*,int*,int*,double*,double*,char*,char*,char*,size_t,size_t,size_t);

size_t mcmule_namelength;
void mcmule_set_observable(int nq, int nb, double *lower, double *upper,
    quantfunc qf, usereventfunc uev, inituserfunc iuf,
    int userdim, int names, int suffix) {
  int *p1 = NULL;
  int *p2 = NULL;
  int *p3 = NULL;
  if(userdim > 0) { p1 = &userdim; }
  if(names   > 0) { p2 = &names; mcmule_namelength = names; } else { mcmule_namelength = 6; }
  if(suffix  > 0) p3 = &suffix;
  __user_dummy_MOD_set_observable(&nq, &nb, lower, upper, qf,uev,iuf, p1,p2,p3);
}

void mcmule_runmcmule(int ncall_ad, int itmx_ad, int ncall, int itmx,
    int initial_ran_seed, double xicut1, double xicut2,
    char *piece, char *flav, char *tfilename) {

  size_t _piece = strlen(piece);
  size_t _flav  = strlen(flav );
  size_t _tfilename = 0;
  if(tfilename != NULL) _tfilename = strlen(tfilename);

  __mcmule_MOD_runmcmule(&ncall_ad,&itmx_ad,&ncall,&itmx,
    &initial_ran_seed, &xicut1, &xicut2,
    piece,flav,tfilename,_piece,_flav,_tfilename
  );
}
#endif
