  PROGRAM MCMULE_EXE
  use mcmule

  implicit none

  integer :: ncall_ad, itmx_ad, ncall, itmx, nenter_ad, nenter
  integer :: initial_ran_seed
  character(len=25) :: piece
  character(len=15) :: flav
  real(kind=prec) :: xi1, xi2

#ifndef SHARED
  BLOCK
  use user_dummy
  use user, only: the_nr_q => nr_q, &
                  the_nr_bins => nr_bins, &
                  the_min_val => min_val, &
                  the_max_val => max_val, &
                  the_inituser => inituser, &
                  the_userevent => userevent, &
                  the_quant => quant, &
                  the_userdim => userdim, &
                  the_bin_kind => bin_kind, &
                  namesLen, filenamesuffixLen

  nr_q = the_nr_q
  nr_bins = the_nr_bins
  allocate(min_val(size(the_min_val)))
  min_val = the_min_val
  allocate(max_val(size(the_max_val)))
  max_val = the_max_val

  userdim = the_userdim
  bin_kind = the_bin_kind

  inituser => the_inituser
  userevent => the_userevent
  call set_quant(the_quant)

  allocate(pass_cut(nr_q))
  allocate(character(namesLen) :: names(nr_q))
  allocate(character(filenamesuffixLen) :: filenamesuffix)

  END BLOCK
#else
  character(len=900) :: arg
  call get_command_argument(1, arg)
  if (len_trim(arg) == 0) then
    arg = "./user.so"
  endif
  call load_quant(trim(arg))
#endif

  read(5,*) nenter_ad
  read(5,*) itmx_ad
  read(5,*) nenter
  read(5,*) itmx
  read(5,*) initial_ran_seed
  read(5,*) xi1
  read(5,*) xi2
  read(5,*) piece
  read(5,*) flav

  ncall_ad = 1000*nenter_ad
  ncall = 1000*nenter

  call runmcmule(ncall_ad, itmx_ad, ncall, itmx, initial_ran_seed, xi1, xi2, piece, flav)

  END PROGRAM MCMULE_EXE
