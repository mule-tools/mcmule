
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                     MODULE INTEGRANDS
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use user_dummy
!  use limits
  use mat_el
  use vegas_m, only: bin_it
  use phase_space

  implicit none


  procedure(), pointer :: ps => null()
  real(kind=prec) :: masses(maxparticles)
  real(kind=prec) :: fluxfac, symmfac
  integer nparticle
  integer nini
  integer polarised



  contains

  SUBROUTINE INITPIECE(ndim, fxn)
  implicit none
  integer :: ndim
  procedure(integrand), pointer :: fxn
  masses = 0.
  polarised = 0


#ifdef HAVE_MUDEC
  if ( (flavour == 'mu-0') .or. (flavour == 'tau-0') ) then
    select case(which_piece)
      case('m2enn0')
        ndim = 5 ; fxn => sigma_0_menn
      case('m2ennF')
        ndim = 5 ; fxn => sigma_F_menn
      case('m2ennR')
        ndim = 8 ; fxn => sigma_R_menn
      case('m2enng0')
        ndim = 8 ; fxn => sigma_0_menng
      case('m2enngF')
        ndim = 8 ; fxn => sigma_C_menng
      case('m2enngR')
        ndim = 11 ; fxn => sigma_R_menng
      case default
        print*,"This process is not implemented with massless flavours"
    end select
    return
  endif
#endif

  symmfac = 1.
  fluxfac = 0.

  matel0 => null()
  matel1 => null()
  matel_s => null()
  matel_sh => null()
  matel_hs => null()
  matel_ss => null()

  select case(which_piece)

#ifdef HAVE_MUDEC

       !!!!!!!!!!!!!!!!!!!!!!!
       !!   MUON  DECAY     !!
       !!!!!!!!!!!!!!!!!!!!!!!


    case('m2enn0')
      call set_func('00000000', pm2ennav)
      ps => psd4 ; fxn => sigma_0
      nparticle = 4 ; ndim = 5
      masses(1:4) = (/ Mm, Me, 0._prec, 0._prec /)
      polarised = 1
    case('m2ennF')
      call set_func('00000000', pm2ennfav)
      ps => psd4 ; fxn => sigma_0
      nparticle = 4 ; ndim = 5
      masses(1:4) = (/ Mm, Me, 0._prec, 0._prec /)
      polarised = 1
      xieik1 = xinormcut*(1.-(Me/Mm)**2)
    case('m2ennR')
      call set_func('00000000', pm2enngav)
      call set_func('00000001', pm2ennav)
      call set_func('11111111', m2enn_part)
      ps => psd5_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 8
      masses(1:5) = (/ Mm, Me, 0._prec, 0._prec, 0._prec /)
      polarised = 1
      xicut1 = xinormcut*(1.-(Me/Mm)**2)
    case('m2ennFFz')
      call set_func('00000000', pm2ennffavz)
      ps => psd4 ; fxn => sigma_0
      nparticle = 4 ; ndim = 5
      masses(1:4) = (/ Mm, Me, 0._prec, 0._prec /)
      polarised = 1
      xieik2 = xinormcut1*(1.-(Me/Mm)**2)
    case('m2ennFF')
      call set_func('000000', pm2ennffav)
      ps => psd4 ; fxn => sigma_0
      nparticle = 4 ; ndim = 5
      masses(1:4) = (/ Mm, Me, 0._prec, 0._prec /)
      polarised = 1
      xieik2 = xinormcut1*(1.-(Me/Mm)**2)
    case('m2ennLL')
      call set_func('000000', pm2ennllav)
      ps => psd4 ; fxn => sigma_0
      nparticle = 4 ; ndim = 5
      masses(1:4) = (/ Mm, Me, 0._prec, 0._prec /)
      polarised = 1
    case('m2ennRF')
      call set_func('00000000', pm2enngfav)
      call set_func('00000001', pm2ennfav)
      call set_func('11111111', m2enn_part)
      ps => psd5_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 8
      masses(1:5) = (/ Mm, Me, 0._prec, 0._prec, 0._prec /)
      polarised = 1
      xicut1 = xinormcut1*(1.-(Me/Mm)**2)
      xieik1 = xinormcut2*(1.-(Me/Mm)**2)
    case('m2ennRFz')
      write(*,'(A)')char(27)//'[33m[WARN]'//char(27)//'[0m This which_piece is used for testing, continue at your own risk'
      call set_func('00000000', pm2enngfavz)
      call set_func('10000001', pm2enngfavz_s)
      ps => psd5_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 8
      masses(1:5) = (/ Mm, Me, 0._prec, 0._prec, 0._prec /)
      polarised = 1
      xicut1 = xinormcut1*(1.-(Me/Mm)**2)
      xieik1 = xinormcut2*(1.-(Me/Mm)**2)
      sSwitch = 0.01
      pcSwitch = 1e-4
    case('m2ennRR')
      call set_func('00000000', pm2ennggav)
      call set_func('00000001', pm2enngav)
      call set_func('00000010', pm2ennav)
      call set_func('11111111', m2enn_part)
      ps => psd6_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mm, Me, 0._prec, 0._prec, 0._prec, 0._prec /)
      polarised = 1
      xicut1 = xinormcut1*(1.-(Me/Mm)**2)
      xicut2 = xinormcut2*(1.-(Me/Mm)**2)
    case('m2ennNF')
      call set_func('00000000', pm2ennav_nf)
      ps => psd4 ; fxn => sigma_0nf
      nparticle = 4 ; ndim = 6
      masses(1:4) = (/ Mm, Me, 0._prec, 0._prec /)
      polarised = 1
      xieik2 = xinormcut1*(1.-(Me/Mm)**2)


       !!!!!!!!!!!!!!!!!!!!!!!
       !! LFV MUON DECAY    !!
       !!!!!!!!!!!!!!!!!!!!!!!

    case('m2ej0')
      call set_func('00000000', pm2ej)
      ps => psd3 ; fxn => sigma_0
      nparticle = 3 ; ndim = 2
      masses(1:3) = (/ Mm, Me, Mj /)
      polarised = 1
    case('m2ejF')
      call set_func('00000000', pm2ejf)
      ps => psd3 ; fxn => sigma_0
      nparticle = 3 ; ndim = 2
      masses(1:3) = (/ Mm, Me, Mj /)
      polarised = 1
      xieik1 = xinormcut*(1.-((Me+Mj)/Mm)**2)
    case('m2ejR')
      call set_func('00000000', pm2ejg)
      call set_func('00000001', pm2ej)
      call set_func('11111111', m2ej_part)
      ps => psd4_fks ; fxn => sigma_1
      nparticle = 4 ; ndim = 5
      masses(1:4) = (/ Mm, Me, Mj, 0._prec /)
      polarised = 1
      xicut1 = xinormcut*(1.-((Me+Mj)/Mm)**2)
    case('m2ejg0')
      call set_func('00000000', pm2ejg)
      ps => psd4 ; fxn => sigma_0
      nparticle = 4 ; ndim = 5
      masses(1:4) = (/ Mm, Me, Mj, 0._prec /)
      polarised = 1

       !!!!!!!!!!!!!!!!!!!!!!!
       !!  RAD MUON DECAY   !!
       !!!!!!!!!!!!!!!!!!!!!!!

    case('m2enng0')
      call set_func('00000000', pm2enngav)
      ps => psd5_25 ; fxn => sigma_0
      nparticle = 5 ; ndim = 8
      masses(1:5) = (/ Mm, Me, 0._prec, 0._prec, 0._prec /)
      polarised = 1
    case('m2enngV')
      call set_func('00000000', pm2ennglav)
      ps => psd5_25 ; fxn => sigma_0
      nparticle = 5 ; ndim = 8
      masses(1:5) = (/ Mm, Me, 0._prec, 0._prec, 0._prec /)
      polarised = 1
    case('m2enngC')
      call set_func('00000000', pm2enngcav)
      ps => psd5_25 ; fxn => sigma_0
      nparticle = 5 ; ndim = 8
      masses(1:5) = (/ Mm, Me, 0._prec, 0._prec, 0._prec /)
      polarised = 1
      xieik1 = xinormcut*(1.-(Me/Mm)**2)
    case('m2enngR')
      call set_func('00000000', pm2ennggav)
      call set_func('00000001', pm2enngav)
      call set_func('11111111', m2enn_part)
      ps => psd6_p_25_26_m50_fks ; fxn => sigma_1
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mm, Me, 0._prec, 0._prec, 0._prec, 0._prec /)
      polarised = 1
      xicut1 = xinormcut*(1.-(Me/Mm)**2)

#endif
#ifdef HAVE_MUDECRARE

       !!!!!!!!!!!!!!!!!!!!!!!
       !!  RARE MUON DECAY  !!
       !!!!!!!!!!!!!!!!!!!!!!!

    case('m2ennee0')
      call set_func('00000000', pm2enneeav)
      ps => psd6_23_24_34_e56 ; fxn => sigma_0
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mm, Me, 0._prec, 0._prec, Me, Me /)
      polarised = 1
      symmfac = 0.5
    case('m2ennee0b')
      write(*,'(A)')char(27)//'[33m[WARN]'//char(27)//'[0m This which_piece is used for testing, continue at your own risk'
      call set_func('00000000', pm2enneeav)
      ps => psd6_26_2x5 ; fxn => sigma_0
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mm, Me, 0._prec, 0._prec, Me, Me /)
      polarised = 1
      symmfac = 0.5
    case('m2enneeV')
      call set_func('00000000', pm2enneelav)
      ps => psd6_26_2x5 ; fxn => sigma_0
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mm, Me, 0._prec, 0._prec, Me, Me /)
      polarised = 1
      symmfac = 0.5
    case('m2enneeA')
      call set_func('00000000', pm2enneeav_a)
      ps => psd6_26_2x5 ; fxn => sigma_0
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mm, Me, 0._prec, 0._prec, Me, Me /)
      polarised = 1
      symmfac = 0.5
    case('m2enneeAi')
      write(*,'(A)')char(27)//'[33m[WARN]'//char(27)//'[0m This which_piece is used for testing, continue at your own risk'
      call set_func('00000000', pm2enneeav_ai)
      ps => psd6_26_2x5 ; fxn => sigma_0
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mm, Me, 0._prec, 0._prec, Me, Me /)
      polarised = 1
      symmfac = 0.5
    case('m2enneeC')
      call set_func('00000000', pm2enneecav)
      ps => psd6_23_24_34_e56 ; fxn => sigma_0
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mm, Me, 0._prec, 0._prec, Me, Me /)
      polarised = 1
      symmfac = 0.5
      xieik1 = xinormcut*(1.-(3*Me/Mm)**2)
    case('m2enneeR')
      call set_func('00000000', pm2enneegav)
      call set_func('00000001', pm2enneeav)
      call set_func('11111111', m2ennee_part)
      ps => psd7_27_37_47_2x5_fks ; fxn => sigma_1
      nparticle = 7 ; ndim = 14
      masses(1:7) = (/ Mm, Me, 0._prec, 0._prec, Me, Me, 0._prec /)
      polarised = 1
      symmfac = 0.5
      xicut1 = xinormcut*(1.-(3*Me/Mm)**2)

    case('t2mnnee0')
      call set_func('00000000', pt2mnneeav)
      ps => psd6_26_2x5 ; fxn => sigma_0
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mt, Mm, 0._prec, 0._prec, Me, Me /)
      polarised = 1
    case('t2mnneeV')
      call set_func('00000000', pt2mnneelav)
      ps => psd6_26_2x5 ; fxn => sigma_0
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mt, Mm, 0._prec, 0._prec, Me, Me /)
      polarised = 1
    case('t2mnneeA')
      call set_func('00000000', pt2mnneeav_a)
      ps => psd6_26_2x5 ; fxn => sigma_0
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mt, Mm, 0._prec, 0._prec, Me, Me /)
      polarised = 1
    case('t2mnneeC')
      call set_func('00000000', pt2mnneecav)
      ps => psd6_26_2x5 ; fxn => sigma_0
      nparticle = 6 ; ndim = 11
      masses(1:6) = (/ Mt, Mm, 0._prec, 0._prec, Me, Me /)
      polarised = 1
      xieik1 = xinormcut*(1.-((Mm+2*Me)/Mt)**2)
    case('t2mnneeR')
      call set_func('00000000', pt2mnneegav)
      call set_func('00000001', pt2mnneeav)
      call set_func('11111111', t2mnnee_part)
      ps => psd7_27_37_47_2x5_fks ; fxn => sigma_1
      nparticle = 7 ; ndim = 14
      masses(1:7) = (/ Mt, Mm, 0._prec, 0._prec, Me, Me, 0._prec /)
      polarised = 1
      xicut1 = xinormcut*(1.-((Mm+2*Me)/Mt)**2)

#endif
#ifdef HAVE_MUE

       !!!!!!!!!!!!!!!!!!!!!!!
       !!  mu e scattering  !!
       !!!!!!!!!!!!!!!!!!!!!!!

    case('em2em0')
      call set_func('00000000', em2em)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)

    case('em2emV')
      call set_func('00000000', em2emL)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
    case('em2emC')
      call set_func('00000000', em2emC)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)

    case('em2emFEE')
      call set_func('00000000', em2emf_ee)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emFEM')
      call set_func('00000000', em2emf_em)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emFMM')
      call set_func('00000000', em2emf_mm)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)

    case('em2emREE')
      call set_func('00000000', em2emg_ee)
      call set_func('00000001', em2em)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emREE15')
      call set_func('00000000', em2emg_ee)
      call set_func('00000001', em2em)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emREE35')
      call set_func('00000000', em2emg_ee)
      call set_func('00000001', em2em)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emREEco')
      call set_func('00000000', em2emg_ee)
      call set_func('00000001', em2em)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_cop13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emREM')
      call set_func('00000000', em2emg_em)
      call set_func('10000001', em2emg_em_s)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emREM15')
      call set_func('00000000', em2emg_em)
      call set_func('10000001', em2emg_em_s)
      call set_func('00000001', em2em)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emREM35')
      call set_func('00000000', em2emg_em)
      call set_func('10000001', em2emg_em_s)
      call set_func('00000001', em2em)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emREMco')
      call set_func('00000000', em2emg_em)
      call set_func('10000001', em2emg_em_s)
      call set_func('00000001', em2em)
      ps => psx3_cop13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRMM')
      call set_func('00000000', em2emg_mm)
      call set_func('00000001', em2em)
      call set_func('11111111', em2em_mm_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emFFEEEE')
      call set_func('00000000', em2emff_eeee)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik2 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emFFEEEEz')
      call set_func('00000000', em2emffz_eeee)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik2 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emFFMMMM')
      call set_func('00000000', em2emff_mmmm)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik2 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emFFMIXDz')
      call set_func('00000000', em2emffz_mixd)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik2 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emFF31z')
      call set_func('00000000', em2emffz_e3m1)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik2 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emFF22z')
      call set_func('00000000', em2emffz_e2m2)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik2 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emFF13z')
      call set_func('00000000', em2emffz_e1m3)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik2 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emFFz')
      call set_func('00000000', em2emffz)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik2 = xinormcut*(1.-(me+mm)**2/scms)

    case('em2emRFEEEE')
      call set_func('00000000', em2emgf_eeee)
      call set_func('00000001', em2emf_ee)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRFEEEE15')
      call set_func('00000000', em2emgf_eeee)
      call set_func('00000001', em2emf_ee)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRFEEEE35')
      call set_func('00000000', em2emgf_eeee)
      call set_func('00000001', em2emf_ee)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRFEEEEco')
      call set_func('00000000', em2emgf_eeee)
      call set_func('00000001', em2emf_ee)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_cop13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRFMMMM')
      call set_func('00000000', em2emgf_mmmm)
      call set_func('00000001', em2emf_mm)
      call set_func('11111111', em2em_mm_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRFEEEEpv')
      write(*,'(A)')char(27)//'[33m[WARN]'//char(27)//'[0m This which_piece is used for testing, continue at your own risk'
      call set_func('00000000', em2emgf_eeee_pv)
      call set_func('00000001', em2emf_ee)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-8
      collcut = 1.e-10
    case('em2emRFEEEEcl')
      write(*,'(A)')char(27)//'[33m[WARN]'//char(27)//'[0m This which_piece is used for testing, continue at your own risk'
      call set_func('00000000', em2emgf_eeee_co)
      call set_func('00000001', em2emf_ee)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-8
      collcut = 1.e-10
    case('em2emRFMIXD')
      call set_func('00000000', em2emgf_mixd)
      call set_func('00000001', em2emf_em)
      call set_func('00000010', em2em)
      call set_func('10000001', em2emgf_mixd_s)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRFMIXD15')
      call set_func('00000000', em2emgf_mixd)
      call set_func('00000001', em2emf_em)
      call set_func('00000010', em2em)
      call set_func('10000001', em2emgf_mixd_s)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRF3115')
      call set_func('00000000', em2emgf_e3m1)
      call set_func('00000001', em2emf_em)
      call set_func('00000010', em2em)
      call set_func('10000001', em2emgf_e3m1_s)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRF2215')
      call set_func('00000000', em2emgf_e2m2)
      call set_func('00000001', em2emf_em)
      call set_func('00000010', em2em)
      call set_func('10000001', em2emgf_e2m2_s)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRF1315')
      call set_func('00000000', em2emgf_e1m3)
      call set_func('00000001', em2emf_em)
      call set_func('00000010', em2em)
      call set_func('10000001', em2emgf_e1m3_s)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRFMIXD35')
      call set_func('00000000', em2emgf_mixd)
      call set_func('00000001', em2emf_em)
      call set_func('00000010', em2em)
      call set_func('10000001', em2emgf_mixd_s)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRF3135')
      call set_func('00000000', em2emgf_e3m1)
      call set_func('00000001', em2emf_em)
      call set_func('00000010', em2em)
      call set_func('10000001', em2emgf_e3m1_s)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRF2235')
      call set_func('00000000', em2emgf_e2m2)
      call set_func('00000001', em2emf_em)
      call set_func('00000010', em2em)
      call set_func('10000001', em2emgf_e2m2_s)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRF1335')
      call set_func('00000000', em2emgf_e1m3)
      call set_func('00000001', em2emf_em)
      call set_func('00000010', em2em)
      call set_func('10000001', em2emgf_e1m3_s)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('em2emRFMIXDco')
      call set_func('00000000', em2emgf_mixd)
      call set_func('00000001', em2emf_em)
      call set_func('00000010', em2em)
      call set_func('10000001', em2emgf_mixd_s)
      ps => psx3_cop13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRF')
      call set_func('00000000', em2emgf)
      call set_func('00000001', em2emf)
      call set_func('00000010', em2em)
      call set_func('11111111', em2em_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
    case('em2emRF15')
      call set_func('00000000', em2emgf)
      call set_func('00000001', em2emf)
      call set_func('00000010', em2em)
      call set_func('11111111', em2em_part)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
    case('em2emRF35')
      call set_func('00000000', em2emgf)
      call set_func('00000001', em2emf)
      call set_func('00000010', em2em)
      call set_func('11111111', em2em_part)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
    case('em2emRFco')
      call set_func('00000000', em2emgf)
      call set_func('00000001', em2emf)
      call set_func('00000010', em2em)
      call set_func('11111111', em2em_part)
      ps => psx3_cop13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)

    case('em2emRREEEE')
      call set_func('00000000', em2emgg_eeee)
      call set_func('00000001', em2emg_ee)
      call set_func('00000010', em2em)
      call set_func('11111111', em2em_ee_part)
      ps => psx4_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRREEEE1516')
      call set_func('00000000', em2emgg_eeee)
      call set_func('00000001', em2emg_ee)
      call set_func('00000010', em2em)
      call set_func('11111111', em2em_ee_part)
      ps => psx4_p_15_16_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRREEEE3536')
      call set_func('00000000', em2emgg_eeee)
      call set_func('00000001', em2emg_ee)
      call set_func('00000010', em2em)
      call set_func('11111111', em2em_ee_part)
      ps => psx4_p13_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRREEEEc')
      call set_func('00000000', em2emgg_eeee)
      call set_func('00000001', em2emg_ee)
      call set_func('00000010', em2em)
      call set_func('11111111', em2em_ee_part)
      ps => psx4_cop13_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRRMMMM')
      call set_func('00000000', em2emgg_mmmm)
      call set_func('00000001', em2emg_mm)
      call set_func('00000010', em2em)
      call set_func('11111111', em2em_mm_part)
      ps => psx4_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRRMIXD')
      call set_func('00000000', em2emgg_mixd)
      call set_func('00000001', em2emg_em)
      call set_func('00000010', em2em)
      call set_func('10000010', em2emgg_mixd_hs)
      call set_func('10000100', em2emgg_mixd_sh)
      call set_func('10000110', em2emgg_mixd_ss)
      call set_func('10000001', em2emg_em_s)
      ps => psx4_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRRMIXD1516')
      call set_func('00000000', em2emgg_mixd)
      call set_func('00000001', em2emg_em)
      call set_func('00000010', em2em)
      call set_func('10000010', em2emgg_mixd_hs)
      call set_func('10000100', em2emgg_mixd_sh)
      call set_func('10000110', em2emgg_mixd_ss)
      call set_func('10000001', em2emg_em_s)
      ps => psx4_p_15_16_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRR311516')
      call set_func('00000000', em2emgg_e3m1)
      call set_func('00000001', em2emg_em)
      call set_func('00000010', em2em)
      call set_func('10000010', em2emgg_e3m1_hs)
      call set_func('10000100', em2emgg_e3m1_sh)
      call set_func('10000110', em2emgg_e3m1_ss)
      call set_func('10000001', em2emg_em_s)
      ps => psx4_p_15_16_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRR221516')
      call set_func('00000000', em2emgg_e2m2)
      call set_func('00000001', em2emg_em)
      call set_func('00000010', em2em)
      call set_func('10000010', em2emgg_e2m2_hs)
      call set_func('10000100', em2emgg_e2m2_sh)
      call set_func('10000110', em2emgg_e2m2_ss)
      call set_func('10000001', em2emg_em_s)
      ps => psx4_p_15_16_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRR131516')
      call set_func('00000000', em2emgg_e1m3)
      call set_func('00000001', em2emg_em)
      call set_func('00000010', em2em)
      call set_func('10000010', em2emgg_e1m3_hs)
      call set_func('10000100', em2emgg_e1m3_sh)
      call set_func('10000110', em2emgg_e1m3_ss)
      call set_func('10000001', em2emg_em_s)
      ps => psx4_p_15_16_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRRMIXD3536')
      call set_func('00000000', em2emgg_mixd)
      call set_func('00000001', em2emg_em)
      call set_func('00000010', em2em)
      call set_func('10000010', em2emgg_mixd_hs)
      call set_func('10000100', em2emgg_mixd_sh)
      call set_func('10000110', em2emgg_mixd_ss)
      call set_func('10000001', em2emg_em_s)
      ps => psx4_p13_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRR313536')
      call set_func('00000000', em2emgg_e3m1)
      call set_func('00000001', em2emg_em)
      call set_func('00000010', em2em)
      call set_func('10000010', em2emgg_e3m1_hs)
      call set_func('10000100', em2emgg_e3m1_sh)
      call set_func('10000110', em2emgg_e3m1_ss)
      call set_func('10000001', em2emg_em_s)
      ps => psx4_p13_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRR223536')
      call set_func('00000000', em2emgg_e2m2)
      call set_func('00000001', em2emg_em)
      call set_func('00000010', em2em)
      call set_func('10000010', em2emgg_e2m2_hs)
      call set_func('10000100', em2emgg_e2m2_sh)
      call set_func('10000110', em2emgg_e2m2_ss)
      call set_func('10000001', em2emg_em_s)
      ps => psx4_p13_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRR133536')
      call set_func('00000000', em2emgg_e1m3)
      call set_func('00000001', em2emg_em)
      call set_func('00000010', em2em)
      call set_func('10000010', em2emgg_e1m3_hs)
      call set_func('10000100', em2emgg_e1m3_sh)
      call set_func('10000110', em2emgg_e1m3_ss)
      call set_func('10000001', em2emg_em_s)
      ps => psx4_p13_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11
    case('em2emRRMIXDc')
      call set_func('00000000', em2emgg_mixd)
      call set_func('00000001', em2emg_em)
      call set_func('00000010', em2em)
      call set_func('10000010', em2emgg_mixd_hs)
      call set_func('10000100', em2emgg_mixd_sh)
      call set_func('10000110', em2emgg_mixd_ss)
      call set_func('10000001', em2emg_em_s)
      ps => psx4_cop13_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('emZem0X')
      call set_func('00000000', emzemx)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      polarised = 0

    case('emZemFX')
      call set_func('00000000', emzemfx)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)
      polarised = 0

    case('emZemRX')
      call set_func('00000000', emzemgx)
      call set_func('00000001', emzemx)
      call set_func('11111111', em2em_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
      polarised = 0

    case('em2emA')
      call set_func('00000000', em2em_a)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)

      !!!!!!!!!!    Vacuum Polarization @ NNLO     !!!!!!!!!!!!

      !! classification according to [1901.03106]
      !! note: there is a IR cancellation among class 2 & 3
    case('em2emAA')
      call set_func('00000000', em2em_aa)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
    case('em2emAFEE')
      call set_func('00000000', em2em_afee)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emAFMM')
      call set_func('00000000', em2em_afmm)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emAFEM')
      call set_func('00000000', em2em_afem)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emAF')
      call set_func('00000000', em2em_af)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emAREE')
      call set_func('00000000', em2emg_aee)
      call set_func('00000001', em2em_a)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emAREE15')
      call set_func('00000000', em2emg_aee)
      call set_func('00000001', em2em_a)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emAREE35')
      call set_func('00000000', em2emg_aee)
      call set_func('00000001', em2em_a)
      call set_func('11111111', em2em_ee_part)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emARMM')
      call set_func('00000000', em2emg_amm)
      call set_func('00000001', em2em_a)
      call set_func('11111111', em2em_mm_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emAREM')
      call set_func('00000000', em2emg_aem)
      call set_func('10000001', em2emg_aem_s)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emAR')
      call set_func('00000000', em2emg_a)
      call set_func('00000001', em2em_a)
      call set_func('11111111', em2em_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emAR15')
      call set_func('00000000', em2emg_a)
      call set_func('00000001', em2em_a)
      call set_func('11111111', em2em_part)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emAR35')
      call set_func('00000000', em2emg_a)
      call set_func('00000001', em2em_a)
      call set_func('11111111', em2em_part)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('em2emNFEE')
      call set_func('00000000', em2em_nfee)
      ps => psx2 ; fxn => sigma_0nf
      nparticle = 4 ; ndim = 3
      masses(1:4) = (/ Me, Mm, Me, Mm /)
    case('em2emNFMM')
      call set_func('00000000', em2em_nfmm)
      ps => psx2 ; fxn => sigma_0nf
      nparticle = 4 ; ndim = 3
      masses(1:4) = (/ Me, Mm, Me, Mm /)
    case('em2emNFEM')
      call set_func('00000000', em2em_nfem)
      ps => psx2 ; fxn => sigma_0nf
      nparticle = 4 ; ndim = 3
      masses(1:4) = (/ Me, Mm, Me, Mm /)
    case('em2emNF')
      call set_func('00000000', em2em_nf)
      ps => psx2 ; fxn => sigma_0nf
      nparticle = 4 ; ndim = 3
      masses(1:4) = (/ Me, Mm, Me, Mm /)
   !!! needed to compare individual classes with [1901.03106]
   case('em2emNFEMCT')
     call set_func('00000000', em2em_nfem_ct)
     ps => psx2 ; fxn => sigma_0
     nparticle = 4 ; ndim = 2
     masses(1:4) = (/ Me, Mm, Me, Mm /)
     xieik1 = xinormcut*(1.-(me+mm)**2/scms)



       !!!!!!!!!!!!!!!!!!!!!!!
       !!  mu p scattering  !!
       !!!!!!!!!!!!!!!!!!!!!!!

    case('mp2mp0')
      call set_func('00000000', mp2mp)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
    case('mp2mp0nuc')
      call set_func('00000000', mp2mp_nuc)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)

    case('mp2mpF')
      call set_func('00000000', mp2mpf)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpFMP')
      call set_func('00000000', mp2mpf_mp)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpR')
      call set_func('00000000', mp2mpg)
      call set_func('00000001', mp2mp)
      call set_func('11111111', mp2mp_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpRMP')
      call set_func('00000000', mp2mpg_mp)
      call set_func('10000001', mp2mpg_mp_s)
      call set_func('00000001', mp2mp_nuc)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpR15')
      call set_func('00000000', mp2mpg)
      call set_func('00000001', mp2mp)
      call set_func('11111111', mp2mp_part)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpR35')
      call set_func('00000000', mp2mpg)
      call set_func('00000001', mp2mp)
      call set_func('11111111', mp2mp_part)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpRco')
      call set_func('00000000', mp2mpg)
      call set_func('00000001', mp2mp)
      call set_func('11111111', mp2mp_part)
      ps => psx3_cop13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpRMP15')
      call set_func('00000000', mp2mpg_mp)
      call set_func('10000001', mp2mpg_mp_s)
      call set_func('00000001', mp2mp_nuc)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpRMP35')
      call set_func('00000000', mp2mpg_mp)
      call set_func('10000001', mp2mpg_mp_s)
      call set_func('00000001', mp2mp_nuc)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpRMPco')
      call set_func('00000000', mp2mpg_mp)
      call set_func('10000001', mp2mpg_mp_s)
      call set_func('00000001', mp2mp_nuc)
      ps => psx3_cop13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)

    case('mp2mpFF')
      call set_func('00000000', mp2mpff)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik2 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpRF')
      call set_func('00000000', mp2mpgf)
      call set_func('00000001', mp2mpf)
      call set_func('11111111', mp2mp_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-8
      collcut = 1.e-10
    case('mp2mpRFpv')
    write(*,'(A)')char(27)//'[33m[WARN]'//char(27)//'[0m This which_piece is used for testing, continue at your own risk'
      call set_func('00000000', mp2mpgf_pv)
      call set_func('00000001', mp2mpf)
      call set_func('11111111', mp2mp_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-8
      collcut = 1.e-10
    case('mp2mpRFcol')
    write(*,'(A)')char(27)//'[33m[WARN]'//char(27)//'[0m This which_piece is used for testing, continue at your own risk'
      call set_func('00000000', mp2mpgf_co)
      call set_func('00000001', mp2mpf)
      call set_func('11111111', mp2mp_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-8
      collcut = 1.e-10
    case('mp2mpRF15')
      call set_func('00000000', mp2mpgf)
      call set_func('00000001', mp2mpf)
      call set_func('11111111', mp2mp_part)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1e-3
      softcut = 1.e-8
      collcut = 1.e-10
    case('mp2mpRF35')
      call set_func('00000000', mp2mpgf)
      call set_func('00000001', mp2mpf)
      call set_func('11111111', mp2mp_part)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      ntsSwitch = 1e-3
      softcut = 1.e-8
      collcut = 1.e-10
    case('mp2mpRFco')
      call set_func('00000000', mp2mpgf)
      call set_func('00000001', mp2mpf)
      call set_func('11111111', mp2mp_part)
      ps => psx3_cop13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-8
      collcut = 1.e-10

    case('mp2mpRR')
      call set_func('00000000', mp2mpgg)
      call set_func('00000001', mp2mpg)
      call set_func('00000010', mp2mp)
      call set_func('11111111', mp2mp_part)
      ps => psx4_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-9
      collcut = 1.e-11
    case('mp2mpRR1516')
      call set_func('00000000', mp2mpgg)
      call set_func('00000001', mp2mpg)
      call set_func('00000010', mp2mp)
      call set_func('11111111', mp2mp_part)
      ps => psx4_p_15_16_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-9
      collcut = 1.e-11
    case('mp2mpRR3536')
      call set_func('00000000', mp2mpgg)
      call set_func('00000001', mp2mpg)
      call set_func('00000010', mp2mp)
      call set_func('11111111', mp2mp_part)
      ps => psx4_p13_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-9
      collcut = 1.e-11
    case('mp2mpRRc')
      call set_func('00000000', mp2mpgg)
      call set_func('00000001', mp2mpg)
      call set_func('00000010', mp2mp)
      call set_func('11111111', mp2mp_part)
      ps => psx4_cop13_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Mm, Me, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(me+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(me+mm)**2/scms)
      softcut = 1.e-9
      collcut = 1.e-11

    case('mp2mpA')
      call set_func('00000000', mp2mp_a)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)

      !!!!!!!!!!    Vacuum Polarization @ NNLO     !!!!!!!!!!!!

      !! classification according to [1901.03106]
      !! note: there is a IR cancellation among class 2 & 3
    case('mp2mpAA')
      call set_func('00000000', mp2mp_aa)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
    case('mp2mpAF')
      call set_func('00000000', mp2mp_af)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Mm, Me, Mm /)
      xieik1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpAR')
      call set_func('00000000', mp2mpg_a)
      call set_func('00000001', mp2mp_a)
      call set_func('11111111', mp2mp_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpAR15')
      call set_func('00000000', mp2mpg_a)
      call set_func('00000001', mp2mp_a)
      call set_func('11111111', mp2mp_part)
      ps => psx3_p_15_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpAR35')
      call set_func('00000000', mp2mpg_a)
      call set_func('00000001', mp2mp_a)
      call set_func('11111111', mp2mp_part)
      ps => psx3_p13_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(me+mm)**2/scms)
    case('mp2mpNF')
      call set_func('00000000', mp2mp_nf)
      ps => psx2 ; fxn => sigma_0nf
      nparticle = 4 ; ndim = 3
      masses(1:4) = (/ Me, Mm, Me, Mm /)



       !!!!!!!!!!!!!!!!!!!!!!!
       !!  mupair           !!
       !!!!!!!!!!!!!!!!!!!!!!!

    case('ee2mm0')
      call set_func('00000000', pepe2mm)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Mm, Mm /)
      polarised = 2
    case('eeZmm0')
      call set_func('000000', pepezmm)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Mm, Mm /)
      polarised = 2
    case('eeZmm0X')
      call set_func('000000', pepezmmx)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Mm, Mm /)
      polarised = 2

    case('ee2mmF')
      call set_func('00000000', pepe2mmf)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Mm, Mm /)
      xieik1 = xinormcut*(1.-(mm+mm)**2/scms)
      polarised = 2
    case('eeZmmFX')
      call set_func('00000000', pepezmmfx)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Mm, Mm /)
      xieik1 = xinormcut*(1.-(mm+mm)**2/scms)
      polarised = 2

    case('ee2mmR')
      call set_func('00000000', pepe2mmg)
      call set_func('00000001', pepe2mm)
      call set_func('11111111', ee2mm_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Mm, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(mm+mm)**2/scms)
      polarised = 2
    case('eeZmmRX')
      call set_func('00000000', pepezmmgx)
      call set_func('00000001', pepeZmmx)
      call set_func('11111111', ee2mm_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Mm, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(mm+mm)**2/scms)
      polarised = 2

    case('ee2mmFFEEEE')
      call set_func('00000000', pepe2mmff_eeee)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Mm, Mm /)
      xieik2 = xinormcut*(1.-(mm+mm)**2/scms)
      polarised = 2
    case('ee2mmRFEEEE')
      call set_func('00000000', pepe2mmgf_eeee)
      call set_func('00000001', pepe2mmf_ee)
      call set_func('11111111', ee2mm_ee_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Mm, Mm, 0._prec /)
      xicut1 = xinormcut1*(1.-(mm+mm)**2/scms)
      xieik1 = xinormcut2*(1.-(mm+mm)**2/scms)
      softcut = 1.e-8
      collcut = 1.e-10
      polarised = 2

    case('ee2mmRREEEE')
      call set_func('00000000', pepe2mmgg_eeee)
      call set_func('00000001', pepe2mmg_ee)
      call set_func('00000010', pepe2mm)
      call set_func('11111111', ee2mm_ee_part)
      ps => psx4_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, Mm, Mm, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(mm+mm)**2/scms)
      xicut2 = xinormcut2*(1.-(mm+mm)**2/scms)
      softcut = 1.e-9
      collcut = 1.e-11
      polarised = 2


    case('ee2mmA')
      call set_func('00000000', pepe2mma)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Mm, Mm /)
      xieik1 = xinormcut*(1.-(mm+mm)**2/scms)
      polarised = 2

    case('eeZmmAX')
      call set_func('00000000', pepeZmmax)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Mm, Mm /)
      xieik1 = xinormcut*(1.-(mm+mm)**2/scms)
      polarised = 2
      !!!!!!!!!!    Vacuum Polarization @ NNLO     !!!!!!!!!!!!

      !! classification according to [1901.03106]
      !! note: there is a IR cancellation among class 2 & 3
    case('ee2mmAA')
      call set_func('00000000', pepe2mm_aa)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Mm, Mm /)
      polarised = 2
    case('ee2mmAFEE')
      call set_func('00000000', pepe2mm_afee)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Mm, Mm /)
      xieik1 = xinormcut*(1.-(mm+mm)**2/scms)
      polarised = 2
    case('ee2mmAREE')
      call set_func('00000000', pepe2mmg_aee)
      call set_func('00000001', pepe2mma)
      call set_func('11111111', ee2mm_ee_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Mm, Mm, 0._prec /)
      xicut1 = xinormcut*(1.-(mm+mm)**2/scms)
      polarised = 2
    case('ee2mmNFEE')
      call set_func('00000000', pepe2mm_nfee)
      ps => psx2 ; fxn => sigma_0nf
      nparticle = 4 ; ndim = 3
      masses(1:4) = (/ Me, Me, Mm, Mm /)
      polarised = 2


#endif
#ifdef HAVE_MISC


       !!!!!!!!!!!!!!!!!!!!!!!
       !!  ee -> nn trial   !!
       !!!!!!!!!!!!!!!!!!!!!!!

    case('ee2nn0')
      call set_func('00000000', ee2nn)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, 0._prec, 0._prec /)
    case('ee2nnF')
      call set_func('00000000', ee2nnf)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, 0._prec, 0._prec /)
      xieik1 = xinormcut*(1.-2*Me**2/scms)
    case('ee2nnR')
      call set_func('00000000', ee2nngav)
      call set_func('00000001', ee2nnav)
      call set_func('11111111', ee2nn_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, 0._prec, 0._prec, 0._prec /)
      xicut1 = xinormcut*(1.-2*Me**2/scms)

    case('ee2nnS')
      call set_func('00000000', ee2nns)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, 0._prec, 0._prec /)
      xieik1 = xinormcut*(1.-2*Me**2/scms)
    case('ee2nnSS')
      call set_func('00000000', ee2nnss)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, 0._prec, 0._prec /)
      xieik1 = xinormcut1*(1.-2*Me**2/scms)
      xieik2 = xinormcut2*(1.-2*Me**2/scms)
    case('ee2nnCC')
      call set_func('00000000', ee2nncc)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, 0._prec, 0._prec /)
      xieik1 = xinormcut*(1.-2*Me**2/scms)
      xieik2 = xinormcut*(1.-2*Me**2/scms)
    case('ee2nnI')
      ndim = 5 ; fxn => sigma_sci_ee2nn
      return
    case('ee2nnRF')
      call set_func('00000000', ee2nngf)
      call set_func('10000001', ee2nngf_s)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, 0._prec, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-2*Me**2/scms)
      xieik1 = xinormcut2*(1.-2*Me**2/scms)
    case('ee2nnRR')
      call set_func('00000000', ee2nnggav)
      call set_func('00000001', ee2nngav)
      call set_func('00000010', ee2nnav)
      call set_func('11111111', ee2nn_part)
      ps => psx4_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, 0._prec, 0._prec, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-2*Me**2/scms)
      xicut2 = xinormcut2*(1.-2*Me**2/scms)

#endif
#ifdef HAVE_EE

       !!!!!!!!!!!!!!!!!!!!!!!
       !! moller scattering !!
       !!!!!!!!!!!!!!!!!!!!!!!


    !!!! NLO !!!!

    case('ee2ee0')
      call set_func('00000000', ee2ee)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Me, Me /)
      symmfac = 0.5

    case('ee2eeA')
      call set_func('00000000', ee2ee_a)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Me, Me /)
      symmfac = 0.5

    case('ee2eeF')
      call set_func('00000000', ee2eef)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Me, Me /)
      symmfac = 0.5
      xieik1 = xinormcut*(1.-(2*me)**2/scms)

     case('ee2eeR')
      call set_func('00000000', ee2eeg)
      call set_func('00000001', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeR125')
      call set_func('00000000', ee2eeg)
      call set_func('00000001', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx3_p_15_25_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeR345')
      call set_func('00000000', ee2eeg)
      call set_func('00000001', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx3_p_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 2 * 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeR35')
      call set_func('00000000', ee2eeg)
      call set_func('00000001', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx3_p_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeR45')
      call set_func('00000000', ee2eeg)
      call set_func('00000001', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx3_p_45_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeR345co')
      call set_func('00000000', ee2eeg)
      call set_func('00000001', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx3_cop_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 2 * 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeR35co')
      call set_func('00000000', ee2eeg)
      call set_func('00000001', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx3_cop_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeR45co')
      call set_func('00000000', ee2eeg)
      call set_func('00000001', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx3_cop_45_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    !!!! NNLO !!!!!

    case('ee2eeRF')
      call set_func('00000000', ee2eegf)
      call set_func('00000001', ee2eef)
      call set_func('11111111', ee2ee_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeRF125')
      call set_func('00000000', ee2eegf)
      call set_func('00000001', ee2eef)
      call set_func('11111111', ee2ee_part)
      ps => psx3_p_15_25_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeRF345')
      call set_func('00000000', ee2eegf)
      call set_func('00000001', ee2eef)
      call set_func('11111111', ee2ee_part)
      ps => psx3_p_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 2 * 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeRF35')
      call set_func('00000000', ee2eegf)
      call set_func('00000001', ee2eef)
      call set_func('11111111', ee2ee_part)
      ps => psx3_p_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeRF45')
      call set_func('00000000', ee2eegf)
      call set_func('00000001', ee2eef)
      call set_func('11111111', ee2ee_part)
      ps => psx3_p_45_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeRF345co')
      call set_func('00000000', ee2eegf)
      call set_func('00000001', ee2eef)
      call set_func('11111111', ee2ee_part)
      ps => psx3_cop_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 2 * 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeRF35co')
      call set_func('00000000', ee2eegf)
      call set_func('00000001', ee2eef)
      call set_func('11111111', ee2ee_part)
      ps => psx3_cop_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeRF45co')
      call set_func('00000000', ee2eegf)
      call set_func('00000001', ee2eef)
      call set_func('11111111', ee2ee_part)
      ps => psx3_cop_45_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeRR')
      call set_func('00000000', ee2eegg)
      call set_func('00000001', ee2eeg)
      call set_func('00000010', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx4_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, Me, Me, 0._prec, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xicut2 = xinormcut2*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeRR15162526')
      call set_func('00000000', ee2eegg)
      call set_func('00000001', ee2eeg)
      call set_func('00000010', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx4_P_15_16_25_26_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, Me, Me, 0._prec, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xicut2 = xinormcut2*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeRR35364546')
      call set_func('00000000', ee2eegg)
      call set_func('00000001', ee2eeg)
      call set_func('00000010', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx4_P_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, Me, Me, 0._prec, 0._prec /)
      symmfac = 2 * 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xicut2 = xinormcut2*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeRR35364546c')
      call set_func('00000000', ee2eegg)
      call set_func('00000001', ee2eeg)
      call set_func('00000010', ee2ee)
      call set_func('11111111', ee2ee_part)
      ps => psx4_coP_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, Me, Me, 0._prec, 0._prec /)
      symmfac = 2 * 0.5
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xicut2 = xinormcut2*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeFF')
      call set_func('00000000', ee2eeffz)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Me, Me /)
      symmfac = 0.5
      xieik2 = xinormcut*(1.-(me+me)**2/scms)

    case('ee2eeAA')
      call set_func('00000000', ee2ee_aa)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Me, Me /)
      symmfac = 0.5
      xieik2 = xinormcut*(1.-(me+me)**2/scms)

    case('ee2eeAF')
      call set_func('00000000', ee2ee_af)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Me, Me /)
      symmfac = 0.5
      xieik1 = xinormcut*(1.-(me+me)**2/scms)

     case('ee2eeAR')
      call set_func('00000000', ee2eeg_a)
      call set_func('00000001', ee2ee_a)
      call set_func('11111111', ee2ee_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeAR125')
      call set_func('00000000', ee2eeg_a)
      call set_func('00000001', ee2ee_a)
      call set_func('11111111', ee2ee_part)
      ps => psx3_p_15_25_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeAR345')
      call set_func('00000000', ee2eeg_a)
      call set_func('00000001', ee2ee_a)
      call set_func('11111111', ee2ee_part)
      ps => psx3_p_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 2 * 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeAR345co')
      call set_func('00000000', ee2eeg_a)
      call set_func('00000001', ee2ee_a)
      call set_func('11111111', ee2ee_part)
      ps => psx3_cop_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      symmfac = 2 * 0.5
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('ee2eeNF')
      call set_func('00000000', ee2ee_nf)
      ps => psx2 ; fxn => sigma_0nf
      nparticle = 4 ; ndim = 3
      masses(1:4) = (/ Me, Me, Me, Me /)
      symmfac = 0.5



       !!!!!!!!!!!!!!!!!!!!!!!
       !! bhabha scattering !!
       !!!!!!!!!!!!!!!!!!!!!!!


    case('eb2eb0')
      call set_func('00000000', eb2eb)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Me, Me /)

    case('eb2ebA')
      call set_func('00000000', eb2eb_a)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Me, Me /)

    case('eb2ebF')
      call set_func('00000000', eb2ebf)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Me, Me /)
      xieik1 = xinormcut*(1.-(2*me)**2/scms)

     case('eb2ebR')
      call set_func('00000000', eb2ebg)
      call set_func('00000001', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebR125')
      call set_func('00000000', eb2ebg)
      call set_func('00000001', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx3_p_15_25_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebR35')
      call set_func('00000000', eb2ebg)
      call set_func('00000001', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx3_p_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebR45')
      call set_func('00000000', eb2ebg)
      call set_func('00000001', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx3_p_45_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebR35co')
      call set_func('00000000', eb2ebg)
      call set_func('00000001', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx3_cop_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebR45co')
      call set_func('00000000', eb2ebg)
      call set_func('00000001', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx3_cop_45_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11


    !!!! NNLO !!!!!

    case('eb2ebFF')
      call set_func('00000000', eb2ebffz)
      ps => psx2 ; fxn => sigma_0
      nparticle = 4 ; ndim = 2
      masses(1:4) = (/ Me, Me, Me, Me /)
      xieik2 = xinormcut*(1.-(me+me)**2/scms)

    case('eb2ebRF')
      call set_func('00000000', eb2ebgf)
      call set_func('00000001', eb2ebf)
      call set_func('11111111', eb2eb_part)
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebRF125')
      call set_func('00000000', eb2ebgf)
      call set_func('00000001', eb2ebf)
      call set_func('11111111', eb2eb_part)
      ps => psx3_p_15_25_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebRF35')
      call set_func('00000000', eb2ebgf)
      call set_func('00000001', eb2ebf)
      call set_func('11111111', eb2eb_part)
      ps => psx3_p_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebRF45')
      call set_func('00000000', eb2ebgf)
      call set_func('00000001', eb2ebf)
      call set_func('11111111', eb2eb_part)
      ps => psx3_p_45_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebRF35co')
      call set_func('00000000', eb2ebgf)
      call set_func('00000001', eb2ebf)
      call set_func('11111111', eb2eb_part)
      ps => psx3_cop_35_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebRF45co')
      call set_func('00000000', eb2ebgf)
      call set_func('00000001', eb2ebf)
      call set_func('11111111', eb2eb_part)
      ps => psx3_cop_45_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Me, Me, Me, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xieik1 = xinormcut2*(1.-(2*me)**2/scms)
      ntsSwitch = 1.e-3
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebRR')
      call set_func('00000000', eb2ebgg)
      call set_func('00000001', eb2ebg)
      call set_func('00000010', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx4_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, Me, Me, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xicut2 = xinormcut2*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebRR15162526')
      call set_func('00000000', eb2ebgg)
      call set_func('00000001', eb2ebg)
      call set_func('00000010', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx4_P_15_16_25_26_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, Me, Me, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xicut2 = xinormcut2*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebRR3536')
      call set_func('00000000', eb2ebgg)
      call set_func('00000001', eb2ebg)
      call set_func('00000010', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx4_P_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, Me, Me, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xicut2 = xinormcut2*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebRR3536c')
      call set_func('00000000', eb2ebgg)
      call set_func('00000001', eb2ebg)
      call set_func('00000010', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx4_coP_35_36_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, Me, Me, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xicut2 = xinormcut2*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebRR4546')
      call set_func('00000000', eb2ebgg)
      call set_func('00000001', eb2ebg)
      call set_func('00000010', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx4_P_45_46_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, Me, Me, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xicut2 = xinormcut2*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

    case('eb2ebRR4546c')
      call set_func('00000000', eb2ebgg)
      call set_func('00000001', eb2ebg)
      call set_func('00000010', eb2eb)
      call set_func('11111111', eb2eb_part)
      ps => psx4_coP_45_46_fkss ; fxn => sigma_2
      nparticle = 6 ; ndim = 8
      masses(1:6) = (/ Me, Me, Me, Me, 0._prec, 0._prec /)
      xicut1 = xinormcut1*(1.-(2*me)**2/scms)
      xicut2 = xinormcut2*(1.-(2*me)**2/scms)
      softcut = 1.e-10
      collcut = 1.e-11

#endif

    case default
      print*,"The piece ",which_piece," is not implemented."
      stop
  end select

  if(polarised.eq.1) then
    if(.not.associated(matel_s )) call set_func('10000001', pdefault_s)
    if(.not.associated(matel_hs)) call set_func('10000010', pdefault_sh)
    if(.not.associated(matel_sh)) call set_func('10000100', pdefault_sh)
    if(.not.associated(matel_ss)) call set_func('10000110', pdefault_ss)
  elseif(polarised.eq.2) then
    if(.not.associated(matel_s )) call set_func('10000001', ppdefault_s)
    if(.not.associated(matel_hs)) call set_func('10000010', ppdefault_sh)
    if(.not.associated(matel_sh)) call set_func('10000100', ppdefault_sh)
    if(.not.associated(matel_ss)) call set_func('10000110', ppdefault_ss)
  else
    if(.not.associated(matel_s )) call set_func('10000001', default_s)
    if(.not.associated(matel_hs)) call set_func('10000010', default_sh)
    if(.not.associated(matel_sh)) call set_func('10000100', default_sh)
    if(.not.associated(matel_ss)) call set_func('10000110', default_ss)
  endif

  ! Calculate the number of initial states. This works for
  ! the NF contributions
  nini = nint((3 * nparticle - ndim - 4) / 3.)
  if(fluxfac < zero) then
    if(nini == 1) then
      fluxfac = 0.5/masses(1)
    elseif(nini == 2) then
      fluxfac = 0.5/sq_lambda(scms,masses(1),masses(2))
    endif
  endif

  ndim = ndim + userdim
  END SUBROUTINE


  FUNCTION SIGMA_0(x, wgt, ndim)

  integer :: ndim
  real(kind=prec) :: x(ndim), wgt, sigma_0
  real (kind=prec) :: weight, wg
  real (kind=prec) :: var(nr_q), mat
  real (kind=prec) :: vecs(4, maxparticles)
  logical :: cuts
  sigma_0 = 0.

  call userevent(x(1:userdim), userdim)
  call gen_mom(ps, x(userdim+1:ndim), masses(1:nparticle), vecs, weight)

  if(weight > zero) then
    var = quant(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
    cuts = any(pass_cut)
    if(cuts) then
      if (polarised == 0) then
        mat = matel0(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      else if (polarised == 1) then
        mat = matel0(vecs(:,1), pol1, vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      else if (polarised == 2) then
        mat = matel0(vecs(:,1), pol1, vecs(:,2), pol2, vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      end if
      sigma_0 = fuckoff(mat,x)*weight * userweight
      sigma_0 = (symmfac*fluxfac)*sigma_0
      wg = sigma_0*wgt
      call bin_it(wg, var)
    endif
  endif
  END FUNCTION SIGMA_0


  FUNCTION SIGMA_0NF(x, wgt, ndim)

  integer :: ndim
  real(kind=prec) :: x(ndim), wgt, sigma_0nf
  real (kind=prec) :: weight, wg
  real (kind=prec) :: var(nr_q), mat
  real (kind=prec) :: vecs(4, maxparticles)
  logical :: cuts
  sigma_0nf = 0.

  call userevent(x(1:userdim), userdim)
  call gen_mom(ps, x(userdim+1:ndim-1), masses(1:nparticle), vecs, weight)

  if(weight > zero) then
    var = quant(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
    cuts = any(pass_cut)
    if(cuts) then
      if (polarised == 0) then
        mat = matel0(x(ndim), vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      else if (polarised == 1) then
        mat = matel0(x(ndim), vecs(:,1), pol1, vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      else if (polarised == 2) then
        mat = matel0(x(ndim), vecs(:,1), pol1, vecs(:,2), pol2, vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      end if
      sigma_0nf = fuckoff(mat,x)*weight * userweight
      sigma_0nf = (symmfac*fluxfac)*sigma_0nf
      wg = sigma_0nf*wgt
      call bin_it(wg, var)
    endif
  endif
  END FUNCTION SIGMA_0nf


  FUNCTION SIGMA_1(x, wgt, ndim)

  integer :: ndim
  real (kind=prec) :: sigma_1, x(ndim), arr(ndim)
  real (kind=prec) :: var(nr_q)
  real (kind=prec) :: wgt, weight, wg
  real (kind=prec) :: mat, xifix
  real (kind=prec) :: vecs(4, maxparticles)
  logical :: cuts

  wg=0.
  arr = x
  sigma_1 = 0._prec

  call userevent(x(1:userdim), userdim)
  call gen_mom_fks(ps, x(userdim+1:ndim), masses(1:nparticle), vecs, weight)

  if(xiout < softcut) return
  if(1._prec - yout < collcut) return

  xifix = xiout

  if(weight > zero ) then
    var = quant(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
    cuts = any(pass_cut)
    if(cuts) then
      if (polarised == 0) then
        mat = matel0(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      else if (polarised == 1) then
        mat = matel0(vecs(:,1), pol1, vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      else if (polarised == 2) then
        mat = matel0(vecs(:,1), pol1, vecs(:,2), pol2, vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      end if
      mat = xifix*weight * userweight*fuckoff(mat,arr)
      mat = (symmfac*fluxfac)*mat
      sigma_1 = mat
      wg = mat*wgt
      call bin_it(wg, var)
    end if
  end if

  if(xifix < xicut1) then   !! soft subtraction
    arr = x
    arr(userdim+1) = 0._prec
    call gen_mom_fks(ps, arr(userdim+1:ndim), masses(1:nparticle), vecs, weight)
    if(weight > zero) then
      var = quant(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      cuts = any(pass_cut)
      if(cuts) then
        if (polarised == 0) then
          mat = matel_s(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        else if (polarised == 1) then
          mat = matel_s(vecs(:,1), pol1, vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        else if (polarised == 2) then
          mat = matel_s(vecs(:,1), pol1, vecs(:,2), pol2, vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        end if
        mat = weight * userweight*fuckoff(mat,arr)/xifix
        mat = (symmfac*fluxfac)*mat
        sigma_1 = sigma_1 - mat
        wg = - mat*wgt
        call bin_it(wg,var)
      endif
    endif
  endif
  END FUNCTION SIGMA_1


  FUNCTION SIGMA_2(x, wgt, ndim)

  integer :: ndim
  real (kind=prec) :: sigma_2, x(ndim), mat
  real (kind=prec) :: var(nr_q)
  real (kind=prec) :: xifixA, xifixB, xpref, arr(ndim), wg
  real (kind=prec) :: wgt, weight
  real (kind=prec) :: vecs(4, maxparticles)
  logical :: cuts

  sigma_2 = 0._prec


  call userevent(x(1:userdim), userdim)
  call gen_mom_fkss(ps, x(userdim+1:ndim), masses(1:nparticle), vecs, weight)

  if(xioutA < softcut) return
  if(xioutB < softcut) return
  if(1._prec - youtA < collcut) return
  if(1._prec - youtB < collcut) return


  xifixA = xioutA ; xifixB = xioutB
  xpref = 1/xioutA / xioutB

  if(weight > zero ) then
    var = quant(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
    cuts = any(pass_cut)
    if(cuts) then
      if (polarised == 0) then
        mat = matel0(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      else if (polarised == 1) then
        mat = matel0(vecs(:,1), pol1, vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      else if (polarised == 2) then
        mat = matel0(vecs(:,1), pol1, vecs(:,2), pol2, vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      end if
      mat = xioutA**2*xioutB**2*fuckoff(mat,arr) / 2.!
      sigma_2 = xpref*mat*weight * userweight
      sigma_2 = (symmfac*fluxfac)*sigma_2
      wg = sigma_2*wgt
      call bin_it(wg, var)
    end if
  end if

  if (xifixA < xicut1) then ! sh subtraction
    arr = x
    arr(userdim+1) = 0._prec
    call gen_mom_fkss(ps, arr(userdim+1:ndim), masses(1:nparticle), vecs, weight)

    ! swap the hard and soft ones
    call swapmom(vecs, nparticle-1, nparticle)

    if(weight > zero) then
      var = quant(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      cuts = any(pass_cut)
      if(cuts) then
        if (polarised == 0) then
          mat = matel_sh(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        else if (polarised == 1) then
          mat = matel_sh(vecs(:,1), pol1, vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        else if (polarised == 2) then
          mat = matel_sh(vecs(:,1), pol1, vecs(:,2), pol2, vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        end if
        mat = xioutB**2*fuckoff(mat,arr)/ 2.!
        mat = xpref*mat*weight * userweight
        mat = (symmfac*fluxfac)*mat
        sigma_2 = sigma_2 - mat
        wg = - mat*wgt
        call bin_it(wg,var)
      endif
    endif
  endif

  if (xifixB < xicut2) then ! hs subtraction
    arr = x
    arr(userdim+2) = 0._prec
    call gen_mom_fkss(ps, arr(userdim+1:ndim), masses(1:nparticle), vecs, weight)
    if(weight > zero) then
      var = quant(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      cuts = any(pass_cut)
      if(cuts ) then
        if (polarised == 0) then
          mat = matel_hs(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        else if (polarised == 1) then
          mat = matel_hs(vecs(:,1), pol1, vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        else if (polarised == 2) then
          mat = matel_hs(vecs(:,1), pol1, vecs(:,2), pol2, vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        end if
        mat = xioutA**2*fuckoff(mat,arr)/ 2.!
        mat = xpref*mat*weight * userweight
        mat = (symmfac*fluxfac)*mat
        sigma_2 = sigma_2 - mat
        wg = - mat*wgt
        call bin_it(wg,var)
      endif
    endif
  endif


  if ((xifixA < xicut1).and.(xifixB < xicut2)) then ! ss subtraction
    arr = x
    arr(userdim+2) = 0._prec
    arr(userdim+1) = 0._prec
    call gen_mom_fkss(ps, arr(userdim+1:ndim), masses(1:nparticle), vecs, weight)
    if(weight > zero) then
      var = quant(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
      cuts = any(pass_cut)
      if(cuts ) then
         if (polarised == 0) then
          mat = matel_ss(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        else if (polarised == 1) then
          mat = matel_ss(vecs(:,1), pol1, vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        else if (polarised == 2) then
          mat = matel_ss(vecs(:,1), pol1, vecs(:,2), pol2, vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7))
        end if
        mat = fuckoff(mat, arr) / 2.!
        mat = xpref*mat*weight * userweight
        mat = (symmfac*fluxfac)*mat
        sigma_2 = sigma_2 + mat
        wg = + mat*wgt
        call bin_it(wg,var)
      endif
    endif
  endif


  END FUNCTION SIGMA_2



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!    SPECIAL INTEGRANDS       !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!        MUON  DECAY          !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#ifdef HAVE_MUDEC
  FUNCTION SIGMA_0_MENN(x, wgt, ndim)

  integer :: ndim
  real (kind=prec) :: sigma_0_menn, x(ndim)
  real (kind=prec) :: wgt, weight, wg
  real (kind=prec) :: var(nr_q), mat
  logical :: cuts
  real(kind=prec), dimension(4) :: p1, p2, p3, p4,p5,p6,p7

  sigma_0_menn = 0._prec

!  call ps_0(x,p1,Mm,p3,p2,Me,p4,0._prec,weight)
  call psd4(x,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,weight)

  if(weight > zero ) then
    var = quant(p1,p2,p3,p4,p5,p6,p7)
    cuts = any(pass_cut)
    if(cuts) then
      mat = pm2ennav(p1,pol1,p2,p3,p4)
      sigma_0_menn = mat*weight
      sigma_0_menn = 0.5*sigma_0_menn/Mm
      wg = sigma_0_menn*wgt
      call bin_it(wg, var)
    end if
  end if

  END FUNCTION SIGMA_0_MENN



  FUNCTION SIGMA_F_MENN(x, wgt, ndim)
  use mudec, only: m2ennl0

  integer :: ndim
  real (kind=prec) :: sigma_f_menn, x(ndim)
  real (kind=prec) :: wgt, weight, wg, Epart, mat0
  real (kind=prec) :: var(nr_q), mat, xicut
  logical :: cuts
  real(kind=prec), dimension(4) :: p1, p2, p3, p4,p5,p6,p7

  sigma_f_menn = 0._prec
  Epart = Mm

  call psd4(x,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,weight)
  xicut = xinormcut*(1.-(Me/Epart)**2)

  if(weight > zero ) then
    var = quant(p1,p2,p3,p4,p5,p6,p7)
    cuts = any(pass_cut)
    if(cuts) then
       mat0 = pm2ennav(p1,pol1,p2,p3,p4)
      select case(flavour)
      case("mu-0","tau-0")
        mat = m2ennl0(p1,p2,p3,p4)
!       mat = 0._prec  !! omit finite virtual corrections
        mat = mat + alpha/(2.*pi)*mat0*( Qtil("l",xicut,Epart,p2)  &
                  + 2*Ireg(xicut,Epart,p1,make_mlm(p2))      &
                  - Ireg(xicut,Epart,p1) )
      case default
        mat = pm2ennlav(p1,pol1,p2,p3,p4)
!       mat = 0._prec  !! omit finite virtual corrections
        mat = mat + alpha/(2.*pi)*mat0*(2*Ireg(xicut,Epart,p1,p2) &
                  - Ireg(xicut,Epart,p1)- Ireg(xicut,Epart,p2))
      end select
      sigma_f_menn = fuckoff(mat*weight,x)
      sigma_f_menn = 0.5*sigma_f_menn/Mm
      wg = sigma_f_menn*wgt
      call bin_it(wg, var)
    end if
  end if

  END FUNCTION SIGMA_F_MENN



  FUNCTION SIGMA_R_MENN(x, wgt, ndim)  ! real corrections
  use mudec, only: pm2enng, pm2enng_s, pm2enng_c, pm2enng_sc

  integer :: ndim
  real (kind=prec) :: sigma_r_menn, x(ndim), arr(8)
  real (kind=prec) :: var(nr_q), varS(nr_q), varC(nr_q), varSC(nr_q)
  real (kind=prec) ::   wgt, weight, wg, wgS, wgSC, wgC
  real (kind=prec) :: mat, xifix, yfix, xypref, xicut, Epart
  logical :: cuts
  real(kind=prec), dimension(4) :: p1, p2, p3, p4, p5,p6,p7

  wg=0.;wgS=0.;wgC=0.;wgSC=0.;
  arr = x
  sigma_r_menn = 0._prec
  Epart = Mm

  call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
  xicut = xinormcut*(1.-(Me/Epart)**2)

  if(xiout < 1.0E-09_prec) return
  if(1._prec - abs(yout) < 1.0E-09_prec) return

  xifix = xiout
  yfix = yout
  xypref = 1.0/xifix/(1.-yfix)

  if(weight > zero ) then
    var = quant(p1,p2,p3,p4,p5,p6,p7)
    cuts = any(pass_cut)
    if(cuts) then
      mat = (1._prec-yout)*xiout**2*pm2enng(p1,pol1,p2,p3,p4,p5)
      mat = xypref*mat*weight
      mat = 0.5*mat/Mm
      sigma_r_menn = mat
      wg = mat*wgt
      call bin_it(wg, var)
    end if
  end if

  if(xifix < xicut) then   !! soft subtraction
    arr = x
    arr(1) = 0._prec
    call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
    if(weight > zero) then
      varS = quant(p1,p2,p3,p4,p5,p6,p7)
      cuts = any(pass_cut)
      if(cuts ) then
        mat = (1._prec-yout)*pm2enng_s(p1,pol1,p2,p3,p4)
        mat = xypref*mat*weight
        mat = 0.5*mat/Mm
        sigma_r_menn = sigma_r_menn - mat
        wgS = - mat*wgt
        call bin_it(wgS,varS)
      endif
    endif
  endif

  select case(flavour)
  case("mu-0","tau-0")
    continue
  case default
    return
  end select

  if(1._prec - yfix < delcut) then
               !! collinear subtraction
    arr = x
    arr(2) = 1._prec
    call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
    if(weight > zero) then
      varC = quant(p1,p2,p3,p4,p5,p6,p7)
      cuts = any(pass_cut)
      if(cuts ) then
        mat = xiout**2*pm2enng_c(p1,pol1,p2,p3,p4,p5)
        mat = xypref*mat*weight
        mat = 0.5*mat/Mm
        sigma_r_menn = sigma_r_menn - mat
        wgC = - mat*wgt
        call bin_it(wgC,varC)
      endif
    endif
  endif

  if(1._prec - yfix < delcut .and. xifix < xicut) then
             !! add back soft-collinear
    arr = x
    arr(1) = 0._prec
    arr(2) = 1._prec
    call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
    if(weight > zero) then
      varSC = quant(p1,p2,p3,p4,p5,p6,p7)
      cuts = any(pass_cut)
      if(cuts ) then
        mat = pm2enng_sc(p1,pol1,p2,p3,p4)
        mat = xypref*mat*weight
        mat = 0.5*mat/Mm
        sigma_r_menn = sigma_r_menn + mat
        wgSC = mat*wgt
        call bin_it(wgSC,varSC)
      endif
    endif
  endif


  END FUNCTION SIGMA_R_MENN

               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
               !!                                       !!
               !!         RADIATIVE  DECAY              !!
               !!                                       !!
               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



  FUNCTION SIGMA_0_MENNG(x, wgt, ndim)
  use mudec, only: pm2enng

  integer :: ndim
  real (kind=prec) :: sigma_0_menng, x(ndim)
  real (kind=prec) :: wgt, weight, wg, Epart
  real (kind=prec) :: var(nr_q), mat
  logical :: cuts
  real(kind=prec), dimension(4) :: p1, p2, p3, p4, p5,p6,p7

  sigma_0_menng = 0._prec
  Epart = Mm

!  call ps_5(x,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,0._prec,weight)
!  call ps_5_fks(x,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
!  weight = weight*xiout
  call psd5_25(x,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,0._prec,weight)

  if(weight > zero ) then
    var = quant(p1,p2,p3,p4,p5,p6,p7)
    cuts = any(pass_cut)
    if(cuts) then
      mat = pm2enng(p1,pol1,p2,p3,p4,p5)
!unpol!      mat = m2enng(p1,p2,p3,p4,p5)
      sigma_0_menng = fuckoff(mat,x)*weight
      sigma_0_menng = 0.5*sigma_0_menng/Mm
      wg = sigma_0_menng*wgt
      call bin_it(wg, var)
    end if
  end if

  END FUNCTION SIGMA_0_MENNG



  FUNCTION SIGMA_C_MENNG(x, wgt, ndim)
  use mudec, only: pm2enng

  integer :: ndim
  real (kind=prec) :: sigma_c_menng, x(ndim)
  real (kind=prec) :: wgt, weight, wg, Epart, mat0
  real (kind=prec) :: var(nr_q), mat, xicut
  logical :: cuts
  real(kind=prec), dimension(4) :: p1, p2, p3, p4, p5,p6,p7

!  count = count+1
!  if(count > 5) stop

  sigma_c_menng = 0._prec
  Epart = Mm

!  call ps_5(x,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,0._prec,weight)
  call psd5_25(x,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,0._prec,weight)
  xicut = xinormcut*(1.-(Me/Epart)**2)

  if(weight > zero ) then
    var = quant(p1,p2,p3,p4,p5,p6,p7)
    cuts = any(pass_cut)
    if(cuts) then
      mat0 = pm2enng(p1,pol1,p2,p3,p4,p5)
      select case(flavour)
      case("mu-0","tau-0")
        mat = 0._prec  !! omit finite virtual corrections
        mat = mat + alpha/(2.*pi)*mat0*( Qtil("l",xicut,Epart,p2)  &
                  + 2*Ireg(xicut,Epart,p1,make_mlm(p2))      &
                  - Ireg(xicut,Epart,p1) )
      case default
        mat = 0._prec  !! omit finite virtual corrections
        mat = mat + alpha/(2.*pi)*mat0*(2*Ireg(xicut,Epart,p1,p2) &
                  - Ireg(xicut,Epart,p1)- Ireg(xicut,Epart,p2))
      end select
      sigma_c_menng = fuckoff(mat,x)*weight
      sigma_c_menng = 0.5*sigma_c_menng/Mm
      wg = sigma_c_menng*wgt
      call bin_it(wg, var)
    end if
  end if

  END FUNCTION SIGMA_C_MENNG




  FUNCTION SIGMA_R_MENNG(x, wgt, ndim)  ! real corrections
  use mudec, only: pm2enngg, pm2enngg_s, pm2enngg_c, pm2enngg_sc

  integer :: ndim
  real (kind=prec) :: sigma_r_menng, x(ndim), arr(11)
  real (kind=prec) :: var(nr_q), varS(nr_q), varC(nr_q), varSC(nr_q)
  real (kind=prec) ::   wgt, weight, wg, wgS, wgSC, wgC
  real (kind=prec) :: mat, xifix, yfix, xypref, xicut, Epart
  logical :: cuts
  real(kind=prec), dimension(4) :: p1, p2, p3, p4, p5, p6,p7

  wg=0.;wgS=0.;wgC=0.;wgSC=0.;
  arr = x
  sigma_r_menng = 0._prec
  Epart = Mm

  call psd6_25_26_m50_fks(arr,p1,mm,p2,me,p3,0._prec,p4,0._prec,p5,0._prec,p6,weight)
  xicut = xinormcut*(1.-(Me/Epart)**2)

  if(xiout < 1.0E-09_prec) return
  if(1._prec - abs(yout) < 1.0E-09_prec) return

  xifix = xiout
  yfix = yout
  xypref = 1.0/xifix/(1.-yfix)

  if(weight > zero ) then
    var = quant(p1,p2,p3,p4,p5,p6,p7)
    cuts = any(pass_cut)
    if(s(p2,p6) < s(p2,p5) .and. cuts) then
      mat = (1._prec-yout)*xiout**2*pm2enngg(p1,pol1,p2,p3,p4,p5,p6)
      mat = xypref*mat*weight
      mat = 0.5*mat/Mm
      sigma_r_menng = mat
      wg = mat*wgt
      call bin_it(wg, var)
    end if
  end if

  if(xifix < xicut) then   !! soft subtraction
    arr = x
    arr(1) = 0._prec
    call psd6_25_26_m50_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,0._prec,p6,weight)
    if(weight > zero) then
      varS = quant(p1,p2,p3,p4,p5,p6,p7)
      cuts = any(pass_cut)
      if(cuts ) then
        mat = (1._prec-yout)*pm2enngg_s(p1,pol1,p2,p3,p4,p5)
        mat = xypref*mat*weight
        mat = 0.5*mat/Mm
        sigma_r_menng = sigma_r_menng - mat
        wgS = - mat*wgt
        call bin_it(wgS,varS)
      endif
    endif
  endif

  select case(flavour)
  case("mu-0","tau-0")
    continue
  case default
    return
  end select

  if(1._prec - yfix < delcut) then
               !! collinear subtraction
    arr = x
    arr(2) = 1._prec
    call psd6_25_26_m50_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,0._prec,p6,weight)
    if(weight > zero) then
      varC = quant(p1,p2,p3,p4,p5,p6,p7)
      cuts = any(pass_cut)
      if(cuts ) then
        mat = xiout**2*pm2enngg_c(p1,pol1,p2,p3,p4,p5,p6)
        mat = xypref*mat*weight
        mat = 0.5*mat/Mm
        sigma_r_menng = sigma_r_menng - mat
        wgC = - mat*wgt
        call bin_it(wgC,varC)
      endif
    endif
  endif

  if(1._prec - yfix < delcut .and. xifix < xicut) then
             !! add back soft-collinear
    arr = x
    arr(1) = 0._prec
    arr(2) = 1._prec
    call psd6_25_26_m50_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,0._prec,p6,weight)
    if(weight > zero) then
      varSC = quant(p1,p2,p3,p4,p5,p6,p7)
      cuts = any(pass_cut)
      if(cuts ) then
        mat = pm2enngg_sc(p1,pol1,p2,p3,p4,p5)
        mat = xypref*mat*weight
        mat = 0.5*mat/Mm
        sigma_r_menng = sigma_r_menng + mat
        wgSC = mat*wgt
        call bin_it(wgSC,varSC)
      endif
    endif
  endif


  END FUNCTION SIGMA_R_MENNG

#endif


                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  !!                               !!
                  !!         RARE  DECAY           !!
                  !!                               !!
                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#ifdef HAVE_MUDECRARE

  FUNCTION BUBBLE(yy)

  real (kind=prec) :: bubble, yy, sq

  if(yy > 1) then
    sq = sqrt(yy-1)
    bubble = - 5./3. - yy + sq*(2+yy)*atan(1./sq)
  else
    sq = sqrt(1-yy)
    bubble = - 5./3. - yy - sq*(1+yy/2.)*Log((1.-sq)/(1.+sq))
                  !! Log((1.-sq)/(1.+sq)) + imag*pi   for Im part
  endif

  END FUNCTION BUBBLE



  FUNCTION SIGMA_FV_MENNEE(x, wgt, ndim)
  use mudecrare, only: pm2ennee

  integer ndim
  real (kind=prec) :: sigma_fv_mennee, x(ndim)
  real (kind=prec) :: wgt, weight, wg
  real (kind=prec) :: var(nr_q), mat, yy56, yy26,ah56,ah26
  real (kind=prec) :: der56,der26,errdersta,errdersys, deg,errdegsta,errdegsys, st2
  logical :: cuts
  real(kind=prec), dimension(4) :: p1, p2, p3, p4, p5, p6, p7


  sigma_fv_mennee = 0._prec

!  call ps_6_mfks(x,p1,Mm,p3,Me,p5,0._prec,p6,0._prec,p2,Me,p4,Me,weight)
  call psd6_23_24_34_e56(x,p1,Mm,p2,Me,p5,0._prec,p6,0._prec,p3,Me,p4,Me,weight)

  if(weight > zero ) then
    var = quant(p1,p2,p3,p4,p5,p6,p7)
    cuts = any(pass_cut)
    if(cuts) then
      call hadr5x(0.001*sqrt(sq(p4+p3)),st2,der56,errdersta,errdersys,deg,errdegsta,errdegsys)
      call hadr5x(0.001*sqrt(sq(p2+p3)),st2,der26,errdersta,errdersys,deg,errdegsta,errdegsys)
      der56 = 137.035999*alpha*der56
      der26 = 137.035999*alpha*der26
      yy56 = 4*Mmu**2/sq(p4+p3)
      yy26 = 4*Mmu**2/sq(p2+p3)
!      yy56 = 4*Mtau**2/sq(p4+p3)
!      yy26 = 4*Mtau**2/sq(p2+p3)
!      ah56 = alpha/(3.*pi)*(bubble(yy56) + log(Mmu**2/mu**2)) ! + der56
!      ah26 = alpha/(3.*pi)*(bubble(yy26) + log(Mmu**2/mu**2)) ! + der26
!      ah56 = alpha/(3.*pi)*bubble(yy56)   ! + der56
!      ah26 = alpha/(3.*pi)*bubble(yy26)   ! + der26
      ah56 = alpha/(3.*pi)*log(Mel**2/musq)
      ah26 = alpha/(3.*pi)*log(Mel**2/musq)
      mat = pm2ennee(p1,pol1,p2,p3,p4,p4,p3,ah56,ah26)
      sigma_fv_mennee = fuckoff(mat,x)*weight
      sigma_fv_mennee = 0.5*sigma_fv_mennee ! 2 identical particles in final state
      sigma_fv_mennee = 0.5*sigma_fv_mennee/Mm
      wg = sigma_fv_mennee*wgt
      call bin_it(wg, var)
    end if
  end if

  END FUNCTION SIGMA_FV_MENNEE

#endif


                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!    e e -> g* -> nu nu       !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#ifdef HAVE_MISC

  FUNCTION SIGMA_SCI_EE2NN(x, wgt, ndim)  ! I integral corrections

  integer :: ndim
  real (kind=prec) :: sigma_sci_ee2nn, x(ndim), arr(5)
  real (kind=prec) :: var(nr_q), varS(nr_q)
  real (kind=prec) ::   wgt, weight, wg, wgS, ctfin, ctpole
  real (kind=prec) :: mat, xifix, yfix, xypref, xicut1, xicut2, Epart, mat0, lin
  logical :: cuts
  real(kind=prec), dimension(4) :: p1, p2, p3, p4, p5, p6, p7

  wg=0.;wgS=0.
  arr = x
  sigma_sci_ee2nn = 0._prec

  call psx3_fks(arr, p1,me,p2,me,p3,0._prec,p4,0._prec,p5,weight)
  xicut1 = xinormcut1*(1.-2*Me**2/scms)
  xicut2 = xinormcut2*(1.-2*Me**2/scms)
  Epart = sqrt(scms)

  !if(xiout < 1.0E-09_prec) return
  if(1._prec - abs(yout) < 1.0E-09_prec) return

  xifix = xiout
  yfix = yout
  xypref = 1.0/xifix/(1.-yfix)

  if(weight > zero ) then
    var = quant(p1,p2,p3,p4,p5,p6,p7)
    cuts = any(pass_cut)
    if(cuts) then

      ctfin = ee2nneik(p1, p2, xicut2, ctpole)

      mat0 = ee2nngav(p1,p2,p3,p4,p5,lin)

      mat = ctfin*mat0+ctpole*lin+ctpole*mat0 * (&
        2.+2*log(2*musq/scms) - log(1-yout**2) - 2*log(xifix) - log(1-xifix) &
      )
      mat = (1._prec-yout)*xiout**2*mat
      mat = fuckoff(mat,arr,'event')
      mat = xypref*mat*weight
      mat = 0.25*mat / p1(3) / sqrt(scms)
      sigma_sci_ee2nn = mat
      wg = mat*wgt
      !call bin_it(wg, var)
    end if
  end if

  if(xifix < xicut1) then   !! soft subtraction
    arr = x
    arr(1) = 0._prec
    call psx3_fks(arr, p1,me,p2,me,p3,0._prec,p4,0._prec,p5,weight)
    if(weight > zero) then
      varS = quant(p1,p2,p3,p4,p5,p6,p7)
      cuts = any(pass_cut)
      if(cuts ) then
        ctfin = ee2nneik(p1, p2, xicut2, ctpole)

        mat0 = ee2nng_s(p1,p2,p3,p4,lin)

        mat = ctfin*mat0+ctpole*lin+ctpole*mat0 * (&
          2.+2*log(2*musq/scms) - log(1-yout**2) - 2*log(xifix) &
        )
        mat = (1._prec-yout)*mat
        mat = fuckoff(mat,arr,'counter event')
        mat = xypref*mat*weight
        mat = 0.25*mat / p1(3) / sqrt(scms)
        sigma_sci_ee2nn = sigma_sci_ee2nn - mat
        wgS = - mat*wgt
        !call bin_it(wgS,varS)
      endif
    endif
  endif
  if (abs(sigma_sci_ee2nn).gt.HUGE(prec)) sigma_sci_ee2nn = 0.


  END FUNCTION SIGMA_SCI_EE2NN

#endif

                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!           STUFF             !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#ifdef HAVE_MUDEC

  FUNCTION SIGMA0_MENNG(x, wgt, ndim)
  use mudec, only: m2enng

  integer ndim
  real (kind=prec) :: sigma0_menng, x(ndim)
  real (kind=prec) :: wgt, weight, wg
  real (kind=prec) :: var(nr_q), mat
  logical :: cuts
  real(kind=prec), dimension(4) :: p1, p2, p3, p4, p5


  sigma0_menng = 0._prec

!proper one::
!  call ps_5(x,p1,Mm,p2,0._prec,p3,20._prec,p4,0._prec,p5,0._prec,weight)
!  call ps_5x(x,p1,Mm,p2,20._prec,p3,p4,0._prec,p5,0._prec,weight)
!  call ps_5_fks(x,p1,Mm,p2,20._prec,p3,p4,0._prec,p5,0._prec,weight)

! call ps_5(x,p1,Mm,p3,0._prec,p2,Me,p4,0._prec,p5,0._prec,weight)
  call psd5_fks(x,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
!  call ps_5_fksr(x(1:6),p1,Mm,p2,Me,p5,weight)

! call ps_5(x,p1,Mm,p2,0._prec,p3,0._prec,p4,0._prec,p5,0._prec,weight)
! call ps_5xx(x,p1,Mm,p2,0._prec,p3,p4,0._prec,p5,0._prec,weight)

  if(weight > zero ) then
!!    var = quant_xs(p1,p2,p3,p4)
!!    cuts = any(pass_cut)
!!    if(eta(p5) < 1.3_prec) cuts = 0
    var = 1._prec
    cuts = .true.
!    if(s(p2,p5) < 40._prec) cuts = 0
    if(cuts) then
      mat = xiout*m2enng(p1,p2,p3,p4,p5)
!      mat = m2enngav(p1,p2,p5)
      sigma0_menng = mat*weight
      sigma0_menng = 0.5*sigma0_menng/Mm
      wg = sigma0_menng*wgt
      call bin_it(wg, var)
    end if
  end if

  END FUNCTION SIGMA0_MENNG


#endif
#ifdef HAVE_MUDECRARE

  FUNCTION SIGMA0_MENNEE(x, wgt, ndim)
  use mudecrare, only: pm2ennee

  integer ndim
  real (kind=prec) :: sigma0_mennee, x(ndim)
  real (kind=prec) :: wgt, weight, wg
  real (kind=prec) :: var(nr_q), mat, pol0(4)
  logical :: cuts
  real(kind=prec), dimension(4) :: p1, p2, p3, p4, p5, p6, p7


  sigma0_mennee = 0._prec

  call psd6(x,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,Me,p6,Me,weight)

  if(weight > zero ) then
    var = quant(p1,p2,p3,p4,p5,p6,p7)
    cuts = any(pass_cut)
    if(cuts) then
      pol0 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)
      mat = pm2ennee(p1,pol0,p2,p3,p4,p5,p6)
      sigma0_mennee = mat*weight
      sigma0_mennee = 0.5*sigma0_mennee ! 2 identical particles in final state
      sigma0_mennee = 0.5*sigma0_mennee/Mm
      wg = sigma0_mennee*wgt
      call bin_it(wg, var)
    end if
  end if

  END FUNCTION SIGMA0_MENNEE




  FUNCTION SIGMAP0_MENNEE(x, wgt, ndim)
  use mudecrare, only: pm2ennee

  integer ndim
  real (kind=prec) :: sigmap0_mennee, x(ndim)
  real (kind=prec) :: wgt, weight, wg
  real (kind=prec) :: var(nr_q), mat
  logical :: cuts
  real(kind=prec), dimension(4) :: p1, p2, p3, p4, p5, p6, p7


  sigmap0_mennee = 0._prec

  call psd6(x,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,Me,p6,Me,weight)

  if(weight > zero ) then
    var = quant(p1,p2,p3,p4,p5,p6,p7)
    cuts = any(pass_cut)
    if(cuts) then
      mat = pm2ennee(p1,pol1,p2,p3,p4,p5,p6)
      sigmap0_mennee = mat*weight
      sigmap0_mennee = 0.5*sigmap0_mennee ! 2 identical particles in final state
      sigmap0_mennee = 0.5*sigmap0_mennee/Mm
      wg = sigmap0_mennee*wgt
      call bin_it(wg, var)
    end if
  end if

  END FUNCTION SIGMAP0_MENNEE


#endif

                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE INTEGRANDS
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
