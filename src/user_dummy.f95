                 !!!!!!!!!!!!!!!!!!!!!
                  MODULE  USER_DUMMY
                 !!!!!!!!!!!!!!!!!!!!!

  use functions
  use iso_c_binding, only: c_int

  implicit none
  integer :: nr_q
  integer :: nr_bins
  real(kind=prec), pointer :: min_val(:), max_val(:)
  integer :: userdim = 0

  logical, allocatable ::  pass_cut(:)
  character(len=:), dimension(:), allocatable :: names
  character(len=:), allocatable :: filenamesuffix
  real(kind=prec) :: userweight
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  integer(c_int), parameter :: rtld_lazy=1

  INTERFACE
    FUNCTION DLOPEN(filename,mode) bind(c,name="dlopen")
      ! void *dlopen(const char *filename, int mode);
      use iso_c_binding
      implicit none
      type(c_ptr) :: dlopen
      character(c_char), intent(in) :: filename(*)
      integer(c_int), value :: mode
    END FUNCTION

    FUNCTION DLSYM(handle,name) bind(c,name="dlsym")
      ! void *dlsym(void *handle, const char *name);
      use iso_c_binding
      implicit none
      type(c_funptr) :: dlsym
      type(c_ptr), value :: handle
      character(c_char), intent(in) :: name(*)
    END FUNCTION

    FUNCTION DLSYMp(handle,name) bind(c,name="dlsym")
      ! void *dlsym(void *handle, const char *name);
      use iso_c_binding
      implicit none
      type(c_ptr) :: dlsymp
      type(c_ptr), value :: handle
      character(c_char), intent(in) :: name(*)
    END FUNCTION

    FUNCTION DLERROR() bind(c,name="dlerror")
      use iso_c_binding
      implicit none
      type(c_ptr) :: dlerror
    END FUNCTION

    FUNCTION STRLEN(string) bind(c)
      use iso_c_binding
      implicit none
      type(c_ptr), value :: string
      integer(c_int) :: strlen
    END FUNCTION
  END INTERFACE

  abstract interface
     function quantfunc(q1,q2,q3,q4,q5,q6,q7)
      use global_def, only: prec
      import nr_q
      real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
      real (kind=prec) :: quantfunc(nr_q)
     end function

     subroutine usereventfunc(x, ndim)
       use global_def, only: prec
       integer :: ndim
       real(kind=prec) :: x(ndim)
     end subroutine

     subroutine inituserfunc
     end subroutine
  end interface
  procedure(quantfunc), pointer :: quant
  procedure(usereventfunc), pointer :: userevent
  procedure(inituserfunc), pointer :: inituser

contains

  SUBROUTINE LOAD_QUANT(filename)
  use iso_c_binding, only: c_ptr, c_funptr, c_int, c_char, &
    c_null_char, &
    c_f_procpointer, c_f_pointer, c_loc, c_associated
  implicit none
  character(len=*) :: filename
  type(c_ptr) :: quant_lib,err
  character(kind=c_char), pointer :: errmsg(:)
  integer :: namesLen, filenamesuffixLen
  procedure(), pointer :: myquant

  quant_lib = dlopen(filename//c_null_char, rtld_lazy )
  if(.not.c_associated(quant_lib)) then
    err = dlerror()
    call c_f_pointer(err, errmsg, shape = [strlen(err)])
    print*, errmsg
    stop 9
  endif

  namesLen = load_int(quant_lib, "__user_MOD_nameslen", "mcmule_namelen", 6)
  filenamesuffixLen = load_int(quant_lib, "__user_MOD_filenamesuffixlen", "mcmule_filename_suffix_length", 10)
  nr_q = load_int(quant_lib, "__user_MOD_nq", "mcmule_number_hist", 1)
  nr_bins = load_int(quant_lib, "__user_MOD_nbins", "mcmule_number_bins", 100)
  userdim = load_int(quant_lib, "__user_MOD_userdim", "mcmule_user_integration_dimension", 0)
  bin_kind = load_int(quant_lib, "__user_MOD_bin_kind", "mcmule_bin_kind", 0)

  call c_f_pointer(dlsymp(quant_lib, "__user_MOD_min_val"//c_null_char), min_val, (/ nr_q /))
  call c_f_pointer(dlsymp(quant_lib, "__user_MOD_max_val"//c_null_char), max_val, (/ nr_q /))

  if(.not.associated(min_val)) then
    call c_f_pointer(dlsymp(quant_lib, "mcmule_lower_bounds"//c_null_char), min_val, (/ nr_q /))
    call c_f_pointer(dlsymp(quant_lib, "mcmule_upper_bounds"//c_null_char), max_val, (/ nr_q /))
  endif

  call c_f_procpointer(dlsym(quant_lib, "__user_MOD_inituser"//c_null_char), inituser)
  call c_f_procpointer(dlsym(quant_lib, "__user_MOD_userevent"//c_null_char), userevent)
  call c_f_procpointer(dlsym(quant_lib, "__user_MOD_quant"//c_null_char), myquant)

  if(.not.associated(myquant)) then
    call c_f_procpointer(dlsym(quant_lib, "mcmule_user_initialisation"//c_null_char), inituser)
    call c_f_procpointer(dlsym(quant_lib, "mcmule_user_integration"//c_null_char), userevent)
    call c_f_procpointer(dlsym(quant_lib, "mcmule_measurement_function"//c_null_char), myquant)
  endif
  if(.not.associated(inituser)) then
    inituser => emptyinituser
  endif
  if(.not.associated(userevent)) then
    userevent => emptyuserevent
  endif

  call set_quant(myquant)

  allocate(pass_cut(nr_q))
  allocate(character(namesLen) :: names(nr_q))
  allocate(character(filenamesuffixLen) :: filenamesuffix)

  contains
    function load_int(quant_lib, name1, name2, default)
    use iso_c_binding
    implicit none
    type(c_ptr), intent(in) :: quant_lib
    character(len=*), intent(in) :: name1, name2
    integer, intent(in) :: default
    integer :: load_int
    type(c_ptr) :: ptr
    integer, pointer :: var

    ptr = dlsymp(quant_lib, name1//c_null_char)
    if(c_associated(ptr)) then
      call c_f_pointer(ptr, var) ; load_int = var
      return
    endif

    ptr = dlsymp(quant_lib, name2//c_null_char)
    if(c_associated(ptr)) then
      call c_f_pointer(ptr, var) ; load_int = var
      return
    endif

    load_int = default
    end function
  END SUBROUTINE LOAD_QUANT

  SUBROUTINE SET_OBSERVABLE(number_hist, number_bins, lower_bounds, upper_bounds, &
      measurement_function, user_integration, user_initialisation, &
      user_integration_dimension, names_length, filenamesuffix_length)
  implicit none
  integer, intent(in) :: number_hist, number_bins
  real(kind=prec), intent(in), target :: lower_bounds(number_hist), upper_bounds(number_hist)
  procedure(quantfunc) :: measurement_function
  procedure(usereventfunc), optional :: user_integration
  procedure(inituserfunc), optional :: user_initialisation
  integer, intent(in), optional :: user_integration_dimension, names_length, filenamesuffix_length

  quant => measurement_function

  if(present(user_integration)) then
    userevent => user_integration
  else
    userevent => emptyuserevent
  endif

  if(present(user_initialisation)) then
    inituser => user_initialisation
  else
    inituser => emptyinituser
  endif

  nr_q = number_hist
  nr_bins = number_bins
  bin_kind = 0

  min_val => lower_bounds
  max_val => upper_bounds

  if(present(user_integration_dimension)) then
    userdim = user_integration_dimension
  else
    userdim = 0
  endif
  if(present(names_length)) then
    allocate(character(names_length) :: names(nr_q))
  else
    allocate(character(6) :: names(nr_q))
  endif
  if(present(filenamesuffix_length)) then
    allocate(character(filenamesuffix_length) :: filenamesuffix)
  else
    allocate(character(10) :: filenamesuffix)
  endif
  allocate(pass_cut(nr_q))

  END SUBROUTINE SET_OBSERVABLE

  SUBROUTINE EMPTYUSEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE EMPTYUSEREVENT

  SUBROUTINE EMPTYINITUSER
  END SUBROUTINE
                 !!!!!!!!!!!!!!!!!!!!!!!
                  END MODULE  USER_DUMMY
                 !!!!!!!!!!!!!!!!!!!!!!!


