                 !!!!!!!!!!!!!!!!!!!!!!!!!
                     MODULE TESTTOOLS
                 !!!!!!!!!!!!!!!!!!!!!!!!!
  use collier
  use phase_space
  use integrands, only: pass_cut, initpiece
  use vegas_m
  use mat_el
  implicit none

  integer npass(2), nfail(2)
  real(kind=prec) timing(2)
  character(len=47) :: blocktitle
  real(kind=prec), dimension(4) :: p1, p2, p3, p4, p5, p6, p7
  logical :: verboseint = .false.
  logical :: vegastruezero = .false.

  ! Mock user.f95
  integer, parameter :: nr_q = 1
  integer, parameter :: nr_bins = 40
  real, parameter :: &
     min_val(nr_q) = (/ 0. /)
  real, parameter :: &
     max_val(nr_q) = (/ 0. /)
  integer :: userdim = 0
  integer :: bin_kind = 0
  integer :: namesLen = 6
  integer :: filenameSuffixLen = 10

contains

  SUBROUTINE BLOCKSTART(msg)
  implicit none
  integer nsp
  character(len=*) msg
  blocktitle = msg
  npass(2)=0 ; nfail(2)= 0
  call cpu_time(timing(2))

  nsp = (47-len(msg))/2
  print*, "----------------------------------------------"
  print*, repeat(' ',nsp)//msg//repeat(' ',nsp)
  print*, "----------------------------------------------"

  END SUBROUTINE
  SUBROUTINE BLOCKEND(n)
  implicit none
  integer n
  real(kind=prec) :: now
  call cpu_time(now)
  print*
  write(*,900) trim(blocktitle), char(10),npass(2), n, now-timing(2)
  print*
900 format("Test block ",A," complete.",A,"Passed ",I2,"/",I2," tests in ",F5.1,"s")
  END SUBROUTINE

  SUBROUTINE CLEANSLATE
  implicit none
  call initvegas
  call initflavour('clean')
  musq = 0.
  xicut1=0.
  xieik1=0.
  xicut2=0.
  xieik2=0.
  xinormcut1=0.
  xinormcut2=0.
  nel=0
  nmu=0
  ntau=0
  nhad=0
  which_piece=" "
  flavour=" "
  Clj=0.
  Crj=0.
  Mj=0.

  pol1=0.
  pol2=0.
  p1=0.
  p2=0.
  p3=0.
  p4=0.
  p5=0.
  p6=0.
  p7=0.


  ENDSUBROUTINE

  SUBROUTINE CHECK(msg, val, ref, threshold)
  implicit none
  character(len=*) msg
  real(kind=prec) val, rel, tthreshold
  real(kind=prec), optional :: threshold, ref
  character(len=15) :: green = char(27)//'[32m[PASS]'//char(27)//'[0m'
  character(len=15) :: red = char(27)//'[31m[FAIL]'//char(27)//'[0m'


  tthreshold = 1.e-10
  if (present(threshold)) tthreshold = threshold
  if (present(ref)) then
    rel = abs(1-real(val)/real(ref))
  else
    rel = abs(val)
  endif

  if (rel .lt. tthreshold) then
    write(*,900)green,msg, rel
    npass = npass + 1
  else
    write(*,901)red,msg, rel,tthreshold
    nfail = nfail + 1
  endif
900 format(A,' ',A,' at ', ES13.5)
901 format(A,' ',A,' at ', ES13.5, ' (threshold: ',ES13.5,')')
  END SUBROUTINE

  SUBROUTINE CHECKcplx(msg, val, ref, threshold)
  implicit none
  character(len=*) msg
  complex(kind=prec) :: val
  real(kind=prec) rel, tthreshold
  complex(kind=prec), optional :: ref
  real(kind=prec), optional :: threshold
  character(len=15) :: green = char(27)//'[32m[PASS]'//char(27)//'[0m'
  character(len=15) :: red = char(27)//'[31m[FAIL]'//char(27)//'[0m'


  tthreshold = 1.e-10
  if (present(threshold)) tthreshold = threshold
  if (present(ref)) then
    rel = abs(val-ref)/abs(ref)
  else
    rel = abs(val)
  endif

  if (rel .lt. tthreshold) then
    write(*,900)green,msg, rel
    npass = npass + 1
  else
    write(*,901)red,msg, rel,tthreshold
    nfail = nfail + 1
  endif
900 format(A,' ',A,' at ', ES13.5)
901 format(A,' ',A,' at ', ES13.5, ' (threshold: ',ES13.5,')')
  END SUBROUTINE

  SUBROUTINE CHECKrange(msg, val, min, max)
  implicit none
  character(len=*) msg
  real(kind=prec) val, min,max
  character(len=15) :: green = char(27)//'[32m[PASS]'//char(27)//'[0m'
  character(len=15) :: red = char(27)//'[31m[FAIL]'//char(27)//'[0m'


  if (min.lt.val.and.val.lt.max) then
    write(*,900)green,msg,val,min,max
    npass = npass + 1
  else
    write(*,901)red,msg,val,min,max
    nfail = nfail + 1
  endif
900 format(A,' ',A,' is ',ES13.5,' in (',ES8.2,',',ES8.2,')')
901 format(A,' ',A,' is ',ES13.5,' not in (',ES8.2,',',ES8.2,')')
  END SUBROUTINE


#define CALCBOTH(j, matel_s) \
    if (polarised.eq.1) then ;\
      full(j) = matel0 (vecs(:,1), pol1, vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7)) ;\
      lim(j)  = matel_s(vecs(:,1), pol1, vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7)) ;\
    elseif (polarised.eq.2) then ;\
      full(j) = matel0 (vecs(:,1), pol1, vecs(:,2), pol2, vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7)) ;\
      lim(j)  = matel_s(vecs(:,1), pol1, vecs(:,2), pol2, vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7)) ;\
    else ;\
      full(j) = matel0 (vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7)) ;\
      lim(j)  = matel_s(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), vecs(:,5), vecs(:,6), vecs(:,7)) ;\
    endif

  SUBROUTINE TEST_SOFTLIMIT(y0, pieces, expectation, imax, step)
  use integrands
  implicit none
  real(kind=prec), intent(in) :: y0(:)
  character (len=*), intent(in) :: pieces(:)
  integer, intent(in), optional :: expectation(:)
  integer :: eexpectation(size(pieces))
  integer, intent(in), optional :: imax
  integer iimax
  real(kind=prec), intent(in), optional :: step
  real(kind=prec) :: sstep

  real(kind=prec) :: y(size(y0))
  real (kind=prec) :: vecs(4, maxparticles), weight
  procedure(integrand), pointer :: fxn
  integer ndim, i, j

  real(kind=prec), dimension(size(pieces)) :: full, lim, lastratio, ratio
  integer, dimension(size(pieces)) :: lastgood
  character(len=5) :: red = char(27)//'[31m'
  character(len=5) :: green = char(27)//'[32m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=5+7+4) :: anscol


  if(present(expectation)) then
    eexpectation = expectation
  else
    eexpectation = 9
  endif
  if(present(imax)) then
    iimax = imax
  else
    iimax = 12
  endif
  if(present(step)) then
    sstep = step
  else
    sstep = 10
  endif

  write(*,900, advance='no')
  do j=1, size(pieces)
    write(*,901, advance='no') pieces(j)
  enddo
  write(*,*)
  write(*,*) repeat("-", 7 + 16 * size(pieces))
  y = y0
  lastratio = 1.
  do i=1, iimax
    y(1) = y(1) / sstep

    do j=1, size(pieces)
      which_piece = pieces(j)
      call initpiece(ndim, fxn)
      call gen_mom_fks(ps, y, masses(1:nparticle), vecs, weight)
      CALCBOTH(j, matel_s)
    enddo

    full = full * xiout**2
    ratio = abs(1-lim/full)

    write(*,902, advance="no") xiout
    do j=1,size(pieces)
      if (ratio(j)<lastratio(j)) then
        lastgood(j) = i
        lastratio(j) = ratio(j)
        write(*,903, advance="no") norm, ratio(j), norm
      else
        write(*,903, advance="no") red, ratio(j), norm
      endif
    enddo
    write(*,*)
  enddo

  do j=1,size(pieces)
    if(lastgood(j) >= eexpectation(j)) then
      anscol = green // '[PASS]' // norm
      npass = npass + 1
    else
      anscol = red   // '[FAIL]' // norm
      nfail = nfail + 1
    endif
    write(*,904) anscol, pieces(j), lastgood(j), eexpectation(j)
  enddo

  call blockend(size(pieces))

900 format(2x,"xi",3x)
901 format(A16)
902 format(2x,ES6.0)
903 format(A,ES13.3,3x,A)
904 format(A,' ',A,': ', I2, ' > ', I2)
  END SUBROUTINE TEST_SOFTLIMIT


  SUBROUTINE TEST_SOFTLIMIT2(y, piece, expectation, imax, step)
  use integrands
  implicit none
  real(kind=prec), intent(in) :: y(:)
  character (len=*), intent(in) :: piece
  integer, intent(in), optional :: expectation(3)
  integer :: eexpectation(3)
  integer, intent(in), optional :: imax
  integer iimax
  real(kind=prec), intent(in), optional :: step
  real(kind=prec) :: sstep

  real(kind=prec) :: xi
  real (kind=prec) :: vecs(4, maxparticles), weight
  procedure(integrand), pointer :: fxn
  integer ndim, i, j

  real(kind=prec), dimension(3) :: full, lim, lastratio, ratio
  integer, dimension(3) :: lastgood
  character(len=5) :: red = char(27)//'[31m'
  character(len=5) :: green = char(27)//'[32m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=5+7+4) :: anscol
  character(len=*), dimension(3), parameter :: scen &
    = (/ 'soft-hard', 'hard-soft', 'soft-soft' /)

  if(present(expectation)) then
    eexpectation = expectation
  else
    eexpectation = 9
  endif
  if(present(imax)) then
    iimax = imax
  else
    iimax = 12
  endif
  if(present(step)) then
    sstep = step
  else
    sstep = 10
  endif

  which_piece = piece
  call initpiece(ndim, fxn)

  write(*,900, advance='no')
  write(*,901, advance='no') 'hard-soft'
  write(*,901, advance='no') 'soft-hard'
  write(*,901              ) 'soft-soft'
  write(*,*) repeat("-", 7 + 16 * 3)

  lastratio = 1.
  xi = 1.
  do i=0, iimax-1
    xi = xi / sstep

    call gen_mom_fkss(ps, [   y(1), xi*y(2), y(3:size(y))], masses(1:nparticle), vecs, weight)
    xioutB = 0.
    CALCBOTH(1, matel_hs)

    call gen_mom_fkss(ps, [xi*y(1),    y(2), y(3:size(y))], masses(1:nparticle), vecs, weight)
    xioutA = 0.
    call swapmom(vecs, nparticle-1, nparticle)
    CALCBOTH(2, matel_sh)

    call gen_mom_fkss(ps, [xi*y(1), xi*y(2), y(3:size(y))], masses(1:nparticle), vecs, weight)
    xioutA = 0. ; xioutB = 0.
    CALCBOTH(3, matel_ss)

    full = full * (y(1)*y(2)*xi**(/1,1,2/))**2
    lim  = lim  * (/ y(1), y(2), 1._prec /)**2

    ratio = abs(1-lim/full)

    write(*,902, advance='no') xi
    do j=1,3
      if (ratio(j)<lastratio(j)) then
        lastgood(j) = i
        lastratio(j) = ratio(j)
        write(*,903, advance="no") norm, ratio(j), norm
      else
        write(*,903, advance="no") red, ratio(j), norm
      endif
    enddo
    write(*,*)
  enddo

  do j=1,3
    if(lastgood(j) >= eexpectation(j)) then
      anscol = green // '[PASS]' // norm
      npass = npass + 1
    else
      anscol = red   // '[FAIL]' // norm
      nfail = nfail + 1
    endif
    write(*,904) anscol, scen(j), lastgood(j), eexpectation(j)
  enddo

900 format(2x,"xi",3x)
901 format(A16)
902 format(2x,ES6.0)
903 format(A,ES13.3,3x,A)
904 format(A,' ',A,': ', I2, ' > ', I2)
  END SUBROUTINE TEST_SOFTLIMIT2


  SUBROUTINE TEST_INT(piece, ncall, itmx, ans, tol, gtol)
  implicit none
  character (len=*) :: piece
  real(kind=prec) :: avgi, sd, chi2a, ans
  real(kind=prec) :: arr(maxdim)
  real(kind=prec), optional :: tol, gtol
  real(kind=prec) :: threshold
  integer, optional :: ncall, itmx
  integer ran_seed, ndim, i, j
  procedure(integrand), pointer :: fxn

  ran_seed = 12145
  which_piece = piece
  call initpiece(ndim, fxn)

  if(vegastruezero) then
    threshold = zero
  else
    threshold = 1e-10
    if(present(tol)) threshold = tol
  endif

  if(present(gtol)) threshold = gtol

  if (present(itmx)) then
    call vegas(ndim,1000*ncall,itmx,fxn,avgi,sd,chi2a,ran_seed, silent=.true.)

    if(verboseint) then
      if(present(tol)) then
        write(*,911) piece, repeat(' ', 19-len_trim(piece)), ncall, itmx, avgi, tol
      else
        write(*,901) piece, repeat(' ', 19-len_trim(piece)), ncall, itmx, avgi
      endif
    endif
  else
    avgi = 0.
    do while (abs(avgi) < zero)
      do i=1,ndim
        arr(i) = ran2(ran_seed)
      enddo
      avgi = fxn(arr(1:ndim), 1., ndim)
    enddo

    if(verboseint) then
      if(present(tol)) then
        write(*,912) piece, repeat(' ', 19-len_trim(piece)), avgi, tol
      else
        write(*,902) piece, repeat(' ', 19-len_trim(piece)), avgi
      endif
    endif
  endif
  call check(which_piece, avgi, ans, threshold=threshold)

901 format('call test_INT("',A,'"', A, ',', I3, ', ', I2, ', ', ES23.16E2, ')')
902 format('call test_INT("',A,'"', A, ', ans=', ES23.16E2, ')')
911 format('call test_INT("',A,'"', A, ',', I3, ', ', I2, ', ', ES23.16E2, ', tol=', ES5.0E1,')')
912 format('call test_INT("',A,'"', A, ', ans=', ES23.16E2, ', tol=', ES5.0E1, ')')
  END SUBROUTINE

  SUBROUTINE PRINTHEAD
  implicit none
  integer nsp
  print*,"+----------------------------------------------------------+"
  print*,"|          Running test suite for monte-carlo              |"
  print*,"|                                                          |"
  print*,"| Source version: ",fullsha," |"
  if(index(gitbranch,"not-git") > 0) then
  print*,"| Source tracking not active. Cannot determine version     |"
  else
  print*,"| Git revision  : ",gitrev," |"
  do nsp = 1,len(gitbranch)
    if (iachar(gitbranch(nsp:nsp)) == 0) exit
  enddo
  print*, "| Git branch is "//gitbranch(1:nsp-1)//'.'//repeat(' ', 43-nsp)//'|'
  endif
  print*,"+----------------------------------------------------------+"
  ENDSUBROUTINE

  SUBROUTINE INITUSER
  return
  END SUBROUTINE INITUSER
  SUBROUTINE USEREVENT(x, ndim)
  use user_dummy, only: userweight
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  return
  END SUBROUTINE USEREVENT

  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: gah(4), gas(4), tee, t13
  real (kind=prec) :: quant(nr_q)

  pol1 = (/ 0._prec, 0._prec, 0.85_prec, 0._prec /)

  quant = 0.
  pass_cut = .true.

  select case(which_piece)
    case ('m2enn0', 'm2ennF', 'm2ennR', &
          'm2ennRF', 'm2ennRR', 'm2ennNF', 'm2ej0', 'm2ejF', 'm2ejR')
      if (cos_th(q2, pol1) < 0) then
        pass_cut = .false.
      endif

    case ('m2ennFF', 'm2ennFFz')
      pass_cut = .true.
      pol1 = 0._prec

    case ('m2ejg0')
      if (cos_th(q2, pol1) < 0) then
        pass_cut = .false.
      endif
      if (q4(4) < 10.) pass_cut = .false.

    case ('m2enng0', 'm2enngV', 'm2enngR', 'm2enngC')

      if(q5(4) > q6(4)) then
        gah=q5;gas=q6
      else
        gah=q6;gas=q5
      endif
      if (gah(4) < 10.) pass_cut = .false.

    case ("em2em0", "em2emOLE", &
          "em2emV", "em2emC", &
          "em2emFEE", "em2emREE", "em2emREE15", "em2emREE35", "em2emREEco", &
          "em2emFMM", "em2emRMM", "em2emFEM", "em2emREM", &
          "em2emFFEEEE", &
          "em2emRFEEEE", "em2emRFEEEE15", "em2emRFEEEE35", "em2emRFEEEEco", &
          "em2emRFEEEEpv", "em2emRFEEEEcl", &
          "em2emRFMIXD", &
          "em2emRREEEE", "em2emRREEEE1516", "em2emRREEEE3536", "em2emRREEEEc", &
          "em2emRRMIXD", "em2emRRMIXD1516", "em2emRRMIXD3536", "em2emRRMIXDc", &
          "em2emA", "em2emAA", "em2emAFEE", "em2emAFEM","em2emAFMM", "em2emAF", &
          "em2emAREE", "em2emAREE15", "em2emAREE35", "em2emAREM", "em2emARMM", &
          "em2emAR",  "em2emAR15",  "em2emAR35", "em2emNFEE", "em2emNFEM",&
          "em2emFFEEEEz", "em2emFFMIXDz", "em2emFFMMMM", "em2emFFz", &
          "em2emREM15", "em2emREM35", &
          "em2emRF", "em2emRF15", "em2emRF35", "em2emRFMIXD15", &
          "em2emRFMIXD35", "em2emRFMMMM", "em2emRRMMMM", "em2emNFEMCT",  &
          "em2emNFMM", "em2emNF","mp2mp0", "mp2mp0nuc","mp2mpF","mp2mpFMP","mp2mpR", 'mp2mpR15',&
          'mp2mpR35', 'mp2mpRco','mp2mpRMP', 'mp2mpRMP15','mp2mpRMP35','mp2mpRMPco',"mp2mpFF", &
          "mp2mpRF", "mp2mpRF15", "mp2mpRF35", "mp2mpRFco", &
          "mp2mpRFbis", "mp2mpRFpv", "mp2mpRFcol", &
          "mp2mpRR", "mp2mpRR1516", "mp2mpRR3536", "mp2mpRRc", &
          "mp2mpA", "mp2mpAA", "mp2mpAF", "mp2mpAR", "mp2mpAR15", "mp2mpAR35", "mp2mpNF")

      tee = sq(q1-q3)
      if (tee > -1.02e+3) pass_cut = .false.

    case ('ee2nn0', 'ee2nnF', 'ee2nnR', 'ee2nnS', 'ee2nnI', 'ee2nnRF', 'ee2nnRR', 'ee2nnSS', 'ee2nnCC')
      pol1 = 0._prec

    case ('m2ennee0', 'm2enneeV', 'm2enneeA', 'm2enneeC', 'm2enneeR', &
          't2mnnee0', 't2mnneeV', 't2mnneeA', 't2mnneeC', 't2mnneeR', &
          'm2ennee0b', 'm2enneeAi')
      pol1 = (/ 0._prec, 0._prec, -0.85_prec, 0._prec /)

    case ('ee2ee0', 'ee2eeF', 'ee2eeR', 'ee2eeR125', 'ee2eeR345', 'ee2eeR35', 'ee2eeR45', &
          'ee2eeA', 'ee2eeAA', 'ee2eeAF', 'ee2eeAR', 'ee2eeAR125', 'ee2eeAR345', 'ee2eeNF', &
          'ee2eeRF', 'ee2eeRF125', 'ee2eeRF345', 'ee2eeRF35', 'ee2eeRF45', &
          'ee2eeRR', 'ee2eeRR15162526', 'ee2eeRR35364546', 'ee2eeFF')
      t13 = sq(q1-q3)
      if (t13 > -1000) pass_cut = .false.
      if (t13 < -3000) pass_cut = .false.

    case('eb2eb0', 'eb2ebF', 'eb2ebR', 'eb2ebR125', 'eb2ebR35', 'eb2ebR45', &
        'eb2ebFF', 'eb2ebRF', 'eb2ebRF125', 'eb2ebRF35', 'eb2ebRF45', 'eb2ebRR', 'eb2ebRR15162526', 'eb2ebRR3536', 'eb2ebRR4546', 'eb2ebA')
      t13 = sq(q1-q3)
      if (t13 > -10000) pass_cut = .false.
      if (t13 < -30000) pass_cut = .false.
    case default
      call crash("This which piece is not implemented")
  end select


  END FUNCTION QUANT




                 !!!!!!!!!!!!!!!!!!!!!!!!!
                   END MODULE TESTTOOLS
                 !!!!!!!!!!!!!!!!!!!!!!!!!
