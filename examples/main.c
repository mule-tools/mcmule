#include <stdio.h>
#include <mcmule.h>

void quant(double**res,double*p1,double*p2,double*p3,double*p4,double*p5,double*p6,double*p7) {
  double Q[4] = {p1[0]-p3[0],p1[1]-p3[1],p1[2]-p3[2],p1[3]-p3[3]};
  res[0][0] = Q[3]*Q[3]-Q[2]*Q[2]-Q[1]*Q[1]-Q[0]*Q[0];
  res[0][1] = p3[2] / p3[3];

  if(*res[0] > -10000)
    mcmule_pass_cut[0] = 0;
  else
    mcmule_pass_cut[0] = 1;

  MCMULE_SET_NAME(0, "tee")
  MCMULE_SET_NAME(1, "cth")
}

int main() {
  double min_val[2] = { -170.e3, -1. };
  double max_val[2] = {    0.e3, +1. };
  mcmule_set_observable(2, 100, &min_val[0],&max_val[0],quant, NULL, NULL, 0, 0, 0);
  mcmule_runmcmule(1000, 10, 1000, 10, 123123, 0.1, 0.1, "em2em0", "muone", NULL);
}
