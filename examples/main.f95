  PROGRAM MCMULE_TEST
  use mcmule
  implicit none
  integer :: bandcut
  real(kind=prec):: &
     min_val(24) = (/ 0.           ,0.               ,0.             ,0.          ,-180000.  , &
                        -180000.     ,0.               ,0.             ,0.          ,0.        , &
                        0.           ,-5.E-2                                                   , &
                        0.           ,0.               ,0.             ,0.          ,-180000.  , &
                        -180000.     ,0.               ,0.             ,0.          ,0.        , &
                        0.           ,-5.E-2   /)
  real(kind=prec):: &
     max_val(24) = (/ 36.E-3       ,6.E-3            ,180000.        ,180000.     ,0.        , &
                        0.           ,1.E+5            ,220.           ,220.        ,220.      , &
                        2.           ,5.E-2                                                    , &
                        36.E-3       ,6.E-3            ,180000.        ,180000.     ,0.        , &
                        0.           ,1.E+5            ,220.           ,220.        ,220.      , &
                        2.           ,5.E-2    /)
  call set_observable(24, 300, min_val, max_val, quant)
  call runmcmule(1000, 5, 1000, 5, 1231, 0.1_prec, 0.1_prec, "em2em0", "muone")

contains


  SUBROUTINE FIX_MU

  !! ==== Specify the scale mu AND musq==mu**2 ==== !!

  musq = me**2

  END SUBROUTINE FIX_MU


  SUBROUTINE INITUSER
  read*, bandcut
  write(filenamesuffix,'(I1)') bandcut

  if(bandcut==0) then
    print*, "filenamesuffix=0 -> no band cut"
  elseif(bandcut==1) then
    print*, "filenamesuffix=1 -> with band cut"
  elseif(bandcut==2) then
    nhad = 0._prec
    nel  = 1._prec
    nmu  = 1._prec
    ntau = 1._prec
    print*, "filenamesuffix=2 -> no band cut, only leptonic vp"
  elseif(bandcut==3) then
    nhad = 0._prec
    nel  = 1._prec
    nmu  = 1._prec
    ntau = 1._prec
    print*, "filenamesuffix=3 -> with band cut, only leptonic vp"
  else
    call crash("inituser")
  endif
  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: q1lab(4), q2lab(4), q3lab(4), q4lab(4)
  real (kind=prec) :: theta_e, theta_m, phi_e, phi_m, tee, tmm, Ee, Em, mgg2, pTe, pTm, pTem
  real (kind=prec) :: gmu, gam, theta_m_el, bdev, phi_aco
  real (kind=prec) :: quant(nr_q)

  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  pass_cut = .true.
  call fix_mu

  q1lab = boost_rf(q1,q1)
  q2lab = boost_rf(q1,q2)
  q3lab = boost_rf(q1,q3)
  q4lab = boost_rf(q1,q4)

  theta_e = acos(cos_th(q2lab,q3lab))
  theta_m = acos(cos_th(q2lab,q4lab))
  phi_e = phi(q3lab)
  phi_m = phi(q4lab)
  tee = sq(q1-q3)
  tmm = sq(q2-q4)
  Ee = q3lab(4)
  Em = q4lab(4)
  mgg2 = sq(q3+q5+q6)
  pTe = PT(q3lab)
  pTm = PT(q4lab)
  pTem = PT(q3lab+q4lab)
  phi_aco = pi-abs(phi_e-phi_m)

  !elasticity curve
  gmu = (q2lab(4)*me+mm**2)/(q2lab(4)*me+me**2)
  gam = (q2lab(4)+me)/sqrt(scms)
  theta_m_el = atan(2*tan(theta_e)/((1+gam**2*tan(theta_e)**2)*(1+gmu)-2))
  bdev = theta_m/theta_m_el

  !energy&angular cut
  if(Ee<1000.) pass_cut = .false.
  if(theta_m<0.3E-3) pass_cut = .false.
  !band cut
  if(bandcut==1.OR.bandcut==3) then
    if(bdev<0.9) pass_cut = .false.
    if(bdev>1.1) pass_cut = .false.
  endif

  !acoplanarity cut
  if(abs(pi-abs(phi_e-phi_m))>3.5E-3) pass_cut(13:24) = .false.

  names(1) = "the"
  names(2) = "thm"
  names(3) = "Ee"
  names(4) = "Em"
  names(5) = "tee"
  names(6) = "tmm"
  names(7) = "mgg2"
  names(8) = "pTe"
  names(9) = "pTm"
  names(10) = "pTem"
  names(11) = "bd"
  names(12) = "ph"

  quant(1) = theta_e
  quant(2) = theta_m
  quant(3) = Ee
  quant(4) = Em
  quant(5) = tee
  quant(6) = tmm
  quant(7) = mgg2
  quant(8) = pTe
  quant(9) = pTm
  quant(10) = pTem
  quant(11) = bdev
  quant(12) = phi_aco

  names(13) = "theA"
  names(14) = "thmA"
  names(15) = "EeA"
  names(16) = "EmA"
  names(17) = "teeA"
  names(18) = "tmmA"
  names(19) = "mgg2A"
  names(20) = "pTeA"
  names(21) = "pTmA"
  names(22) = "pTemA"
  names(23) = "bdA"
  names(24) = "phA"

  quant(13) = theta_e
  quant(14) = theta_m
  quant(15) = Ee
  quant(16) = Em
  quant(17) = tee
  quant(18) = tmm
  quant(19) = mgg2
  quant(20) = pTe
  quant(21) = pTm
  quant(22) = pTem
  quant(23) = bdev
  quant(24) = phi_aco

  END FUNCTION QUANT

  END PROGRAM MCMULE_TEST
