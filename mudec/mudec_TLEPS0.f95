
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE mudec_TLEPS0
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function TLEPS0(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, &
         np4,mu2)
!
      use global_def, only: pi, prec
      implicit none
!

    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13



    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i


!
      real(kind=prec) TLEPS0,EL,GF,MOU2,MIN2, &
         p1p2,p1p4,p2p4,asym, &
         np2,np3,np4,mu2, &
         aa,bb
!
      aa=0._prec
      bb=-1._prec
!

!
!      call setlambda(-1._prec)
!      call setdelta(0._prec)
!      call setmudim(mu2)
!      call setminmass(0._prec)
!

!
      TLEPS0=real((-32*EL**2*GF**2*(MOU2**2*p1p4* &
           (MIN2*p1p4 - 3*sqrt(MIN2)*np4*p2p4 +  &
             3*p1p4*(-p1p2 + p2p4)) +  &
          MOU2*(3*MIN2**1.5*np4*(2*p1p4 - p2p4)*p2p4 +  &
             MIN2**2*(-2*p1p4**2 + p2p4**2) +  &
             p1p4*(-4*p1p4**3 + 8*p1p4**2*p2p4 +  &
                2*p1p2*(7*p1p4 - 3*p2p4)*p2p4 -  &
                11*p1p4*p2p4**2 + 3*p2p4**3 +  &
                p1p2**2*(-10*p1p4 + 6*p2p4)) +  &
             sqrt(MIN2)*np4* &
              (4*p1p4**3 - 8*p1p4**2*p2p4 +  &
                3*(p1p2 - p2p4)*p2p4**2 +  &
                p1p4*p2p4*(-7*p1p2 + 11*p2p4)) +  &
             MIN2*(-8*p1p4**2*p2p4 + 3*p2p4**3 +  &
                p1p2*(10*p1p4**2 - 2*p1p4*p2p4 - 3*p2p4**2))) &
            + (p1p4 - p2p4)* &
           (-3*MIN2**2.5*np4*p2p4 + MIN2**3*(p1p4 + p2p4) +  &
             MIN2**1.5*np4* &
              (-4*p1p4**2 + 4*p1p4*p2p4 +  &
                7*(p1p2 - p2p4)*p2p4) -  &
             4*p1p4*(p1p2 - p2p4)* &
              (2*p1p2**2 + 2*p1p4**2 - 2*p1p2*p2p4 -  &
                2*p1p4*p2p4 + p2p4**2) +  &
             MIN2*(4*p1p4**3 - 4*p1p4**2*p2p4 +  &
                5*p1p4*p2p4**2 + 4*p2p4**3 +  &
                2*p1p2**2*(7*p1p4 + 2*p2p4) -  &
                2*p1p2*p2p4*(9*p1p4 + 4*p2p4)) +  &
             MIN2**2*(5*p2p4*(p1p4 + p2p4) -  &
                p1p2*(7*p1p4 + 5*p2p4)) -  &
             4*sqrt(MIN2)*np4* &
              (p1p2**2*p2p4 +  &
                p2p4*(2*p1p4**2 - 2*p1p4*p2p4 + p2p4**2) -  &
                2*p1p2*(p1p4**2 - p1p4*p2p4 + p2p4**2))) +  &
          sqrt(MIN2)*np2* &
           (3*MOU2**2*p1p4**2 +  &
             (p1p4 - p2p4)*(3*MIN2 - 4*p1p2 + 4*p2p4)* &
              (p1p4*(-2*p1p2 + p2p4) + MIN2*(p1p4 + p2p4)) +  &
             MOU2*(MIN2*(-6*p1p4**2 + 3*p2p4**2) +  &
                p1p4*(10*p1p2*p1p4 - 6*p1p2*p2p4 -  &
                   7*p1p4*p2p4 + 3*p2p4**2)))))/ &
      (3.*p1p4**2*(p1p4 - p2p4)**2))
!
!	  T1BOXAC=T1BOXAC+real(
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE mudec_TLEPS0
                          !!!!!!!!!!!!!!!!!!!!!!
