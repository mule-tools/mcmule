                 !!!!!!!!!!!!!!!!!!!!!!!!!
                     MODULE MUDEC_TEST
                 !!!!!!!!!!!!!!!!!!!!!!!!!
  use testtools

contains

  SUBROUTINE TESTMUDEC(ONLYFAST)
  logical onlyfast

  call testmudecff
  call testmudecffChen
  call testmudecmatel
  call testmudeccol
  call testmjmatel
  call testmudecsoft
  call testmudecmassification
  if(.not.onlyfast) then
    call testmudecspeedFF
    call testmudecspeed
    call testmudecvegas
    call testmjvegas
  endif
  call testmudecpointwise
  call testmjpointwise
  END SUBROUTINE TESTMUDEC


  SUBROUTINE TESTMUDECFF
  use mudec_h2lff
  use mudec_h20ff
  implicit none
  real(kind=prec) :: ol1(-2:2), ol2(-2:2), tl1(-4:0), tl2(-4:0)
  call blockstart("mudec ff")
  call init_hpls(0.3, 0.01)

  call check("F1 a^1 ep^-1", ff1a1es(), +3.248495242049358989123344e0)
  call check("F1 a^1 ep^ 0", ff1a1ef(), +2.052181859329036520300261e1)
  call check("F1 a^1 ep^+1", ff1a1el(), +8.151753282417219295365189e1)
  call check("F1 a^2 ep^-2", ff1a2ed(), +5.276360668808661723318332e0)
  call check("F1 a^2 ep^-1", ff1a2es(), +6.666503005850382070713379e1)
  call check("F1 a^2 ep^ 0", ff1a2ef(), +4.808563950549486399879157e2)
  call check("F2 a^1 ep^ 0", ff2a1ef(), -1.188916479795774596375462e0)
  call check("F2 a^1 ep^+1", ff2a1el(), -5.077904525084570305436700e0)
  call check("F2 a^2 ep^-1", ff2a2es(), -3.862189527810646623624964e0)
  call check("F2 a^2 ep^ 0", ff2a2ef(), -3.964827420963052492573208e1)
  call check("F3 a^1 ep^ 0", ff3a1ef(), +2.307306240748826479956638e0)
  call check("F3 a^1 ep^+1", ff3a1el(), +6.929293647070190329817592e0)
  call check("F3 a^2 ep^-1", ff3a2es(), +7.495273345023355640864092e0)
  call check("F3 a^2 ep^ 0", ff3a2ef(), +7.679625302515490979416808e1)

  call blockend(14)

  musq = Mel**2
  call blockstart("mudec(0) ff")
  call h20ff(0.3, log(musq/Mmu**2), ol1, tl1, ol2, tl2)
  call check("F1 a^1 ep^-2", ol1(-2)*2, -1.)
  call check("F1 a^1 ep^-1", ol1(-1)*2, 7.4498476234700455)
  call check("F1 a^1 ep^0", ol1(0)*2, -31.708958005582858)
  call check("F1 a^1 ep^1", ol1(1)*2, 94.97493593280808)
  call check("F1 a^1 ep^2", ol1(2)*2, -225.64807431094806)
  call check("F2 a^1 ep^0", ol2(0)*2, -2.37783295959155)
  call check("F2 a^1 ep^1", ol2(1)*2, 15.199493446947553)
  call check("F2 a^1 ep^2", ol2(2)*2, -59.97345463537275)
  call check("F1 a^2 ep^-4", tl1(-4)*4, 0.5)
  call check("F1 a^2 ep^-3", tl1(-3)*4, -7.4498476234700455)
  call check("F1 a^2 ep^-2", tl1(-2)*4, 59.459072812044)
  call check("F1 a^2 ep^-1", tl1(-1)*4, -334.35438059182394)
  call check("F1 a^2 ep^0", tl1(0)*4, 1486.929242087892)
  call check("F2 a^2 ep^-2", tl2(-2)*4, 2.37783295959155)
  call check("F2 a^2 ep^-1", tl2(-1)*4, -32.913986669969404)
  call check("F2 a^2 ep^0", tl2(0)*4, 253.5899814315627)
  call blockend(16)
  END SUBROUTINE


  SUBROUTINE TESTMUDECFFCHEN
  use mudec_h2lff_full
  use massification
  use mudec_h20ff
  implicit none
  real(kind=prec) :: z, x, xchen
  complex(kind=prec) :: chen(1:40,-2:2)
  complex(kind=prec) :: ffval1(1:2,-1:1)
  complex(kind=prec) :: ffval2(1:2,-2:0)
  complex(kind=prec) :: ffvalr(1:2,-2:0)
  complex(kind=prec) :: ffval2R(1:2,-2:0)
  complex(kind=prec) :: bubint(0:2)
  real(kind=prec) :: F11(-1:1), F21(0:1), F12(-2:0), F22(-1:0)
  real(kind=prec) :: G11(-1:1), G21(0:1), G12(-2:0), G22(-1:0)

  real(kind=prec) :: ol1(-2:2), tl1(-4:0), ol2(-2:2), tl2(-4:0)
  real(kind=prec) :: olZ1(-1:1), tlZ1(-2:0), olZ2(-1:1), tlZ2(-2:0)

  call initflavour("mu-e")

  x = 0.3
  z = Me/Mm
  xchen = (1 - x + z**2 - Sqrt((1 - x + z**2)**2 - 4*z**2))/(2.*z)

  chen = getchenints(cmplx(xchen), cmplx(z), bubint)

  call blockstart("mudec ff Chen integrals")
  call check("chen[1, -2]", real(chen(1,-2)), 1.)
  call check("chen[1, 0]", real(chen(1,0)), 3.2898681336964524)
  call check("chen[1, 2]", real(chen(1,2)), 6.493939402266829)
  call check("chen[2, -2]", real(chen(2,-2)), 1.)
  call check("chen[2, -1]", real(chen(2,-1)), 21.32639502269502)
  call check("chen[2, 0]", real(chen(2,0)), 230.69743046571193)
  call check("chen[2, 1]", real(chen(2,1)), 1686.7555292053544)
  call check("chen[2, 2]", real(chen(2,2)), 9373.668066378206)
  call check("chen[3, -2]", real(chen(3,-2)), 1.)
  call check("chen[3, -1]", real(chen(3,-1)), 10.66319751134751)
  call check("chen[3, 0]", real(chen(3,0)), 60.14175871670032)
  call check("chen[3, 1]", real(chen(3,1)), 237.15482642258942)
  call check("chen[3, 2]", real(chen(3,2)), 732.2187397153294)
  call check("chen[4, -2]", real(chen(4,-2)), -10688.280611257986)
  call check("chen[4, -1]", real(chen(4,-1)), -227942.49442909998)
  call check("chen[4, 0]", real(chen(4,0)), -2.5360849406876727e6)
  call check("chen[4, 1]", real(chen(4,1)), -1.9631101288146302e7)
  call check("chen[5, -2]", real(chen(5,-2)), -0.25)
  call check("chen[5, 0]", real(chen(5,0)), -2.4674011002723395)
  call check("chen[5, 1]", real(chen(5,1)), -2.4041138063191885)
  call check("chen[6, 0]", real(chen(6,0)), -5.831664033764348)
  call check("chen[6, 1]", real(chen(6,1)), -26.926357792963103)
  call check("chen[7, 0]", real(chen(7,0)), -0.9999577805982759)
  call check("chen[7, 1]", real(chen(7,1)), 1.0003046636891497)
  call check("chen[8, 0]", real(chen(8,0)), -30.070742955119236)
  call check("chen[8, 1]", real(chen(8,1)), -305.46828485361243)
  call check("chen[9, 0]", real(chen(9,0)), -947.5558013034283)
  call check("chen[9, 1]", real(chen(9,1)), -15560.094290963112)
  call check("chen[10, 0]", real(chen(10,0)), -12.254069510221338)
  call check("chen[10, 1]", real(chen(10,1)), -78.50925315595234)
  call check("chen[11, -1]", real(chen(11,-1)), 1.1884892114884429)
  call check("chen[11, 0]", real(chen(11,0)), 1.931049039961154)
  call check("chen[11, 1]", real(chen(11,1)), 10.818345078857663)
  call check("chen[12, -1]", real(chen(12,-1)), 13.026420035177056)
  call check("chen[12, 0]", real(chen(12,0)), 94.76056690024835)
  call check("chen[12, 1]", real(chen(12,1)), 466.74414355736)
  call check("chen[13, -1]", real(chen(13,-1)), -1.1884892114884429)
  call check("chen[13, 0]", real(chen(13,0)), -14.181170215923382)
  call check("chen[13, 1]", real(chen(13,1)), -89.27575894905722)
  call check("chen[13, 2]", real(chen(13,2)), -392.69062786507766)
  call check("chen[14, -1]", real(chen(14,-1)), -13.026420035177058)
  call check("chen[14, 0]", real(chen(14,0)), -78.41894652364336)
  call check("chen[14, 1]", real(chen(14,1)), -328.35362868215094)
  call check("chen[14, 2]", real(chen(14,2)), -1024.1764405692388)
  call check("chen[15, 0]", real(chen(15,0)), 169.68761893286194)
  call check("chen[15, 1]", real(chen(15,1)), 2043.036272266132)
  call check("chen[15, 2]", real(chen(15,2)), 14145.825858244538)
  call check("chen[16, 0]", real(chen(16,0)), -305.97390377439683)
  call check("chen[16, 1]", real(chen(16,1)), -5457.102051084821)
  call check("chen[17, 0]", real(chen(17,0)), -22.099841315154872)
  call check("chen[17, 1]", real(chen(17,1)), -216.83282752525045)
  call check("chen[18, -2]", real(chen(18,-2)), -3.553727311666375)
  call check("chen[18, -1]", real(chen(18,-1)), -39.96351076867993)
  call check("chen[19, 0]", real(chen(19,0)), -17.484010587583946)
  call check("chen[19, 1]", real(chen(19,1)), -129.89345385114157)
  call check("chen[20, 0]", real(chen(20,0)), -2.0742820547273313)
  call check("chen[20, 1]", real(chen(20,1)), -2.444658743529994)
  call check("chen[21, 0]", real(chen(21,0)), 25.87167357420445)
  call check("chen[21, 1]", real(chen(21,1)), 316.74233560815816)
  call check("chen[22, 0]", real(chen(22,0)), 0.9259242045154865)
  call check("chen[22, 1]", real(chen(22,1)), 4.326349215164377)
  call check("chen[23, 0]", real(chen(23,0)), -286.4156111320856)
  call check("chen[23, 1]", real(chen(23,1)), -4058.691354517797)
  call check("chen[24, 0]", real(chen(24,0)), -19.558292642311155)
  call check("chen[24, 1]", real(chen(24,1)), -166.53556606987985)
  call check("chen[25, -1]", real(chen(25,-1)), -25405.812391682302)
  call check("chen[25, 0]", real(chen(25,0)), -258089.47384933394)
  call check("chen[25, 1]", real(chen(25,1)), -1.191496597769905e6)
  call check("chen[26, 0]", real(chen(26,0)), -2.541548672843559)
  call check("chen[26, 1]", real(chen(26,1)), -4.5974124891389)
  call check("chen[27, 0]", real(chen(27,0)), -19.558292642311155)
  call check("chen[27, 1]", real(chen(27,1)), -148.3548884606515)
  call check("chen[28, -1]", real(chen(28,-1)), -6.513210017588538)
  call check("chen[28, 0]", real(chen(28,0)), -55.048814453265734)
  call check("chen[28, 1]", real(chen(28,1)), -299.6446383993138)
  call check("chen[29, 0]", real(chen(29,0)), -156.36628932887692)
  call check("chen[30, -1]", real(chen(30,-1)), -25.257955611367027)
  call check("chen[30, 0]", real(chen(30,0)), -272.4631589761355)
  call check("chen[30, 1]", real(chen(30,1)), -1490.734358943638)
  call check("chen[31, 0]", real(chen(31,0)), -8.56023292113031)
  call check("chen[32, -1]", real(chen(32,-1)), -25.257955611367027)
  call check("chen[32, 0]", real(chen(32,0)), -99.573157514464)
  call check("chen[32, 1]", real(chen(32,1)), 1549.1476875868868)
  call check("chen[33, -1]", real(chen(33,-1)), -98.02068113216241)
  call check("chen[33, 0]", real(chen(33,0)), -1171.125467563483)
  call check("chen[34, 0]", real(chen(34,0)), 9.960885002371912)
  call check("chen[35, 0]", real(chen(35,0)), -0.8224670334241131)
  call check("chen[35, 1]", real(chen(35,1)), 1.3169446513992678)
  call check("chen[36, 0]", real(chen(36,0)), -35163.03378698329)
  call check("chen[36, 1]", real(chen(36,1)), -693597.2528029744)
  call check("chen[37, 0]", real(chen(37,0)), -1.2707743364217388)
  call check("chen[37, 1]", real(chen(37,1)), 0.6785330971714385)
  call check("chen[38, 0]", real(chen(38,0)), 6.354388840442559)
  call check("chen[38, 1]", real(chen(38,1)), 39.107718294173374)
  call check("chen[39, 0]", real(chen(39,0)), -143.20780556604265)
  call check("chen[39, 1]", real(chen(39,1)), -1905.4065056721988)
  call check("chen[40, 0]", real(chen(40,0)), 662.0012280925592)
  call check("chen[40, 1]", real(chen(40,1)), 11110.817649085062)

  call check("bubint(0)", real(bubint(0)), 1.1674203744204696)
  call check("bubint(1)", real(bubint(1)), 2.9204185773536793)
  call check("bubint(2)", real(bubint(2)), 3.2559846530780874)
  call blockend(101)

  call blockstart("mudec ff form factors CDR")
  ffval2 = ff2(cmplx(x), cmplx(z), chen)
  call check("\tilde F_1^(2) ep^-2", real(FFval2(1,-2))*4, 182.75806481144005)
  call check("\tilde F_1^(2) ep^-1", real(FFval2(1,-1))*4, 1740.9442523340117)
  call check("\tilde F_1^(2) ep^0 ", real(FFval2(1, 0))*4, 12432.439777003143)
  call check("\tilde F_2^(2) ep^-1", real(FFval2(2,-1))*4, 635.2473669266747)
  call check("\tilde F_2^(2) ep^0 ", real(FFval2(2, 0))*4, 3683.939490269404)

  ffval1 = ff1(cmplx(x), cmplx(z), cmplx(log(z)), bubint)
  call check("F_1^(1) ep^-1", real(FFval1(1,-1)),  99.9123094315455 / 8 / pi)
  call check("F_1^(1) ep^ 0", real(FFval1(1, 0)),  707.266609890124 / 8 / pi)
  call check("F_1^(1) ep^ 1", real(FFval1(1, 1)),  3167.64958411538 / 8 / pi)
  call check("F_2^(1) ep^ 0", real(FFval1(2, 0)), -31.4533567234277 / 8 / pi)
  call check("F_2^(1) ep^ 1", real(FFval1(2, 1)), -173.247330364242 / 8 / pi)

  ffvalr = ffr(cmplx(x), cmplx(z), cmplx(log(z)), bubint)
  call check("dF_1 ep^-2", real(FFvalr(1,-2))*4, -151.150701276801)
  call check("dF_1 ep^-1", real(FFvalr(1,-1))*4, -1293.45518953628)
  call check("dF_1 ep^-0", real(FFvalr(1, 0))*4, -8814.91881030989)
  call check("dF_2 ep^-1", real(FFvalr(2,-1))*4, -655.147971485621)
  call check("dF_2 ep^-0", real(FFvalr(2, 0))*4, -3926.41779679998)

  ffval2R = ffval2 + ffvalr
  call check("F_1^(2) ep^-2", real(FFval2R(1,-2))*4,  4991.234787972746 / 16 / pi**2)
  call check("F_1^(2) ep^-1", real(FFval2R(1,-1))*4,  70664.64037794381 / 16 / pi**2)
  call check("F_1^(2) ep^-0", real(FFval2R(1, 0))*4,  571256.0136610749 / 16 / pi**2)
  call check("F_2^(2) ep^-1", real(FFval2R(2,-1))*4, -3142.577509691938 / 16 / pi**2)
  call check("F_2^(2) ep^-0", real(FFval2R(2, 0))*4, -38290.63938423128 / 16 / pi**2, threshold=1e-9)
  call blockend(20)

  call blockstart("mudec ff FDH")
  call h2lff(x, z, 0., f11, f12, f21, f22, g11, g12, g21, g22)

  call check("F_1^(1) ep^-1", F11(-1),  3.975384480439476)
  call check("F_1^(1) ep^ 0", F11( 0),  28.14124426196541)
  call check("F_1^(1) ep^ 1", F11( 1),  123.5834540917287)
  call check("F_2^(1) ep^ 0", F21( 0), -1.251489299841441)
  call check("F_2^(1) ep^ 1", F21( 1), -5.641802951393861)
  call check("F_1^(2) ep^-2", F12(-2), 7.9018408836595535)
  call check("F_1^(2) ep^-1", F12(-1), 111.8722656992719)
  call check("F_1^(2) ep^-0", F12( 0), 894.6273584458788)
  call check("F_2^(2) ep^-1", F22(-1), -4.975151140152772)
  call check("F_2^(2) ep^-0", F22( 0), -55.64442549797761, threshold=1e-9)

  ! This is interpolated in Mathematica, hence the threshold
  call check("F_1^(1) ep^-1 nnlo.nb", F11(-1), 3.97538456562157, threshold=1e-4)
  call check("F_1^(1) ep^ 0 nnlo.nb", F11( 0), 28.1412447851218, threshold=1e-4)
  call check("F_1^(1) ep^ 1 nnlo.nb", F11(+1), 123.583455688329, threshold=1e-4)
  call check("F_1^(2) ep^-2 nnlo.nb", F12(-2), 7.90184107103590, threshold=1e-4)
  call check("F_1^(2) ep^-1 nnlo.nb", F12(-1), 111.872268877085, threshold=1e-4)
  call check("F_1^(2) ep^ 0 nnlo.nb", F12( 0), 894.620215370565, threshold=1e-4)
  call check("G_1^(1) ep^-1 nnlo.nb", G11(-1), 3.97538456562157, threshold=1e-4)
  call check("G_1^(1) ep^ 0 nnlo.nb", G11( 0), 28.0724967821313, threshold=1e-4)
  call check("G_1^(1) ep^ 1 nnlo.nb", G11(+1), 123.128154200165, threshold=1e-4)
  call check("G_1^(2) ep^-2 nnlo.nb", G12(-2), 7.90184107103590, threshold=1e-4)
  call check("G_1^(2) ep^-1 nnlo.nb", G12(-1), 111.598969098650, threshold=1e-4)
  call check("G_1^(2) ep^ 0 nnlo.nb", G12( 0), 891.243304893334, threshold=1e-4)
  call check("F_2^(1) ep^ 0 nnlo.nb", F21( 0),-1.25148921738102, threshold=1e-4)
  call check("F_2^(1) ep^ 1 nnlo.nb", F21(+1),-5.64180226235748, threshold=1e-4)
  call check("F_2^(2) ep^-1 nnlo.nb", F22(-1),-4.97515115552368, threshold=1e-4)
  call check("F_2^(2) ep^ 0 nnlo.nb", F22( 0),-55.6641951882051, threshold=1e-3)
  call check("G_2^(1) ep^ 0 nnlo.nb", G21( 0),-1.12548906682923, threshold=1e-4)
  call check("G_2^(1) ep^+1 nnlo.nb", G21( 1),-4.50528193742329, threshold=1e-4)
  call check("G_2^(2) ep^-1 nnlo.nb", G22(-1),-4.47425204692698, threshold=1e-4)
  call check("G_2^(2) ep^ 0 nnlo.nb", G22( 0),-49.0548545008534, threshold=1e-3)

  call blockend(30)

  call blockstart("mudec ff form factors v massified")
  call h20ff(x, 0., ol1, tl1, ol2, tl2)
  call massify(log(z**2), 0.5, 1., ol1, tl1, olZ1, tlZ1)
  call massify(log(z**2), 0.5, 0., ol2, tl2, olZ2, tlZ2)

  call check("F_1^(1) ep^-1 v. massified", F11(-1), olZ1(-1), threshold=1e-3)
  call check("F_1^(1) ep^ 0 v. massified", F11( 0), olZ1( 0), threshold=2e-3)
  call check("F_1^(1) ep^ 1 v. massified", F11( 1), olZ1( 1), threshold=2e-3)
  call check("F_2^(1) ep^ 0 v. massified", F21( 0), olZ2( 0), threshold=1e-1)
  call check("F_2^(1) ep^ 1 v. massified", F21( 1), olZ2(+1), threshold=5e-1)

  call check("F_1^(2) ep^-2 v. massified", F12(-2), tlZ1(-2), threshold=1e-3)
  call check("F_1^(2) ep^-1 v. massified", F12(-1), tlZ1(-1), threshold=2e-3)
  call check("F_1^(2) ep^-0 v. massified", F12( 0), tlZ1(-0), threshold=5e-3)
  call check("F_2^(2) ep^-1 v. massified", F22(-1), tlZ2(-1), threshold=1e-1)
  call check("F_2^(2) ep^-0 v. massified", F22( 0), tlZ2(-0), threshold=5e-1)

  call blockend(10)
  END SUBROUTINE

  SUBROUTINE TESTMUDECMATEL
  use vegas_m
  use mudec_mat_el, only: pm2ennlavz, pm2ennlav2, pm2ennllavz_old, pm2ennffavz_old, pm2ennllav, pm2ennlav0
  implicit none
  integer i,ranseed
  real(kind=prec) :: arr(8), weight, pol2(4)
  real(kind=prec), parameter :: xicut = .8
  real(kind=prec) double, single, finite, lin
  real(kind=prec) :: ref

  call initflavour("mu-eOld")
  call blockstart("mudec n matel")
  musq = 0.4*mm**2
  pol1 = (/ 0., 0., 0.8, 0. /)
  pol2 = (/ 0., 0., 0.0, 0. /)

  ranseed = 23313
  do i=1,5
    arr(i) = ran2(ranseed)
  enddo
  call psd4(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,weight)

  finite = pm2ennav(p1,pol2,p2,p3,p4,single)
  call check(" m2enn  ep^ 0", finite, 342653161.41427898)
  call check(" m2enn  ep^ 1", single, -135609060.15321922)

  finite = pm2ennav(p1,pol1,p2,p3,p4,single)
  call check("pm2enn  ep^ 0", finite,  3.7773749153467834e8)
  call check("pm2enn  ep^ 1", single, -1.4480122101244792e8)


  finite = pm2ennlav(p1,pol2,p2,p3,p4, single)
  call check(" m2ennl ep^-1", single, 468783053.17430252)
  call check(" m2ennl ep^ 0", finite, 2739553395.0334105)

  finite = pm2ennlav(p1,pol1,p2,p3,p4, single)
  call check("pm2ennl ep^-1", single, 5.1678184975456995e8)
  call check("pm2ennl ep^ 0", finite, 3.0193229868427277e9)

  finite = pm2ennlav2(p1,pol2,p2,p3,p4, single, lin)
  call check(" m2ennl ep^-1", single, 468783053.17430252)
  call check(" m2ennl ep^ 0", finite, 2739553395.0334105)
  call check(" m2ennl ep^ 1", lin   , 1.106972974642307e10, threshold=1e-7)

  finite = pm2ennlav2(p1,pol1,p2,p3,p4, single, lin)
  call check("pm2ennl ep^-1", single, 5.1678184975456995e8)
  call check("pm2ennl ep^ 0", finite, 3.0193229868427277e9)
  call check("pm2ennl ep^ 1", lin   , 1.2200881677254194e10)

  finite = pm2ennlavz(p1,pol1,p2,p3,p4, single, lin)
  call check("pm2ennlz ep^-1", single, 5.1678184975456995e8, threshold=1e-4)
  call check("pm2ennlz ep^ 0", finite, 3.0193229868427277e9, threshold=1e-4)
  call check("pm2ennlz ep^ 1", lin   , 1.2200881677254194e10, threshold=1e-4)


  call check("pm2ennll ep^ 0", pm2ennllavz_old(p1, pol1, p2, p3, p4), 1.1406192697089768e12 / 8. / pi**2)
  call check("pm2ennll ep^ 0", pm2ennllavz(p1, pol1, p2, p3, p4, double, single), 1.1406192697089768e12 / 8. / pi**2)
  call check("pm2ennll ep^-2", double, 1.3954661721635416e10 / 8. / pi**2)
  call check("pm2ennll ep^-1", single, 1.6306160233928925e11 / 8. / pi**2)

  xieik2 = 1.
  ref = 3.376185552957104e8 + 8.418077028309364e8 * log(xieik2) + 6.413410868182428e8 * log(xieik2)**2
  call check(" m2ennff exp. (xc=1.)", pm2ennffavz_old(p1, pol2, p2, p3, p4), 2*ref)
  xieik2 = 0.3
  ref = 3.376185552957104e8 + 8.418077028309364e8 * log(xieik2) + 6.413410868182428e8 * log(xieik2)**2
  call check(" m2ennff exp. (xc=.3)", pm2ennffavz_old(p1, pol2, p2, p3, p4), 2*ref)

  xieik2 = 1.
  call check("pm2ennff exp. (xc=1.)", pm2ennffavz_old(p1, pol1, p2, p3, p4), 2*3.7158802555700123e8)

  xieik2 = 1.
  ref = 3.386617722537384e8 + 8.418077028309393e8 * log(xieik2) + 6.413410868182429e8 * log(xieik2)**2
  call check(" m2ennff full (xc=1.)", pm2ennffavz(p1, pol2, p2, p3, p4), 2*ref, threshold=5e-2)
  xieik2 = 0.3
  ref = 3.386617722537384e8 + 8.418077028309393e8 * log(xieik2) + 6.413410868182429e8 * log(xieik2)**2
  call check("pm2ennff full (xc=.3)", pm2ennffavz(p1, pol2, p2, p3, p4), 2*ref, threshold=5e-2)

  xieik2 = 1.
  ref = 3.367984700896159e8 + 8.418077028302244e8*Log(xieik2) + 6.413410868190018e8*Log(xieik2)**2
  call check(" m2ennff nexp (xc=1.)", pm2ennffavz(p1, pol2, p2, p3, p4), 2*ref)
  xieik2 = 0.3
  ref = 3.367984700896159e8 + 8.418077028302244e8*Log(xieik2) + 6.413410868190018e8*Log(xieik2)**2
  call check(" m2ennff nexp (xc=1.)", pm2ennffavz(p1, pol2, p2, p3, p4), 2*ref)


  finite = pm2ennllav(p1, pol1, p2, p3, p4)
  call check("pm2ennll v. massified", finite, pm2ennllavz(p1, pol1, p2, p3, p4), threshold=5e-2)
  xieik2 = 0.02
  call check(" m2ennff full (xc=.3)", pm2ennffav(p1, pol2, p2, p3, p4), pm2ennffavz(p1, pol2, p2, p3, p4), threshold=5e-2)
  musq = 0.4*mm**2

  nel = 1; nmu = 1; ntau = 1; nhad = 0
  call check("pm2ennav_nf", pm2ennav_nf(.01,p1,pol1,p2,p3,p4),-77593.5393208520,threshold=1e-7)
  call check("pm2ennav_nf", pm2ennav_nf(.1,p1,pol1,p2,p3,p4),-4.05042086546368e6,threshold=1e-7)
  call check("pm2ennav_nf", pm2ennav_nf(.9,p1,pol1,p2,p3,p4),-7.91770702812629e8,threshold=1e-7)
  call check("pm2ennav_nf", pm2ennav_nf(.999,p1,pol1,p2,p3,p4),-7.44564942724982e9,threshold=1e-7)

  call psd4(arr,p1,Mm,p2,0.,p3,0._prec,p4,0._prec,weight)
  finite = pm2ennlav0(p1, pol2, p2, p3, p4, double, single)
  call check(" m2ennl0, ep^-2", double, -2.7268688785671543e7*2)
  call check(" m2ennl0, ep^-1", single, -4.5032146004331170e7*2)
  call check(" m2ennl0, ep^ 0", finite, -1.4418951731406736e8*2)
  finite = pm2ennlav0(p1, pol1, p2, p3, p4, double, single)
  call check("pm2ennl0, ep^-2", double, -3.006075323922366e7*2)
  call check("pm2ennl0, ep^-1", single, -4.964302609152952e7*2)
  call check("pm2ennl0, ep^ 0", finite, -1.593203690386710e8*2)

  call blockend(36)


  call initflavour("mu-eOld")
  call blockstart("mudec n+1 matel")
  ranseed = 23313
  do i=1,8
    arr(i) = ran2(ranseed)
  enddo
  call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)

  print*, "----------------------------------------------"
  print*, "            fully massive electron            "
  print*, "----------------------------------------------"

  call check(" m2enng", pm2enngav(p1,pol2,p2,p3,p4,p5), 0.5*1.1090579644178507e8,1e-14)
  call check("pm2enng", pm2enngav(p1,pol1,p2,p3,p4,p5), 0.5*1.5351072643264967e8,1e-14)

  finite = pm2ennglav(p1,pol2,p2,p3,p4,p5,single)
  call check(" m2enngl ep^-1",single,7.367233751589806e7)
  call check(" m2enngl ep^ 0",finite,4.398220722023348e8)
  finite = pm2ennglav(p1,pol1,p2,p3,p4,p5,single)
  call check("pm2enngl ep^-1",single,1.019738770461223e8)
  call check("pm2enngl ep^ 0",finite,6.082171587230729e8)
  xieik1 = xicut
  finite = pm2enngfav(p1,pol2,p2,p3,p4,p5)
  call check(" m2enngf (0.8)",finite,8.193452982244721e7)
  finite = pm2enngfav(p1,pol1,p2,p3,p4,p5)
  call check("pm2enngf (0.8)",finite,1.128456057964394e8)


  print*, "----------------------------------------------"
  print*, "              massless electron               "
  print*, "----------------------------------------------"
  finite = pm2ennglav0(p1,pol2,p2,p3,p4,p5,single,double)
  call check(" m2enngl0 ep^-2",double,-8.82692430785778e6)
  call check(" m2enngl0 ep^-1",single,-1.67705979687696e7)
  call check(" m2enngl0 ep^ 0",finite,-4.29712088547953e7)
  finite = pm2ennglav0(p1,pol1,p2,p3,p4,p5,single,double)
  call check("pm2enngl0 ep^-2",double,-1.221796446430408e7)
  call check("pm2enngl0 ep^-1",single,-2.321335981607474e7)
  call check("pm2enngl0 ep^ 0",finite,-5.665112363482363e7)


  print*, "----------------------------------------------"
  print*, "             massified electron               "
  print*, "----------------------------------------------"
  finite = pm2ennglavz(p1,pol2,p2,p3,p4,p5,single)
  call check(" m2ennglz ep^-1",single,7.367807273857147e7)
  call check(" m2ennglz ep^ 0",finite,4.398352157488666e8)
  finite = pm2ennglavz(p1,pol1,p2,p3,p4,p5,single)
  call check("pm2ennglz ep^-1",single,1.019829833271501e8)
  call check("pm2ennglz ep^ 0",finite,6.116350803936851e8)
  xieik1 = xicut
  finite = pm2enngfavz(p1,pol2,p2,p3,p4,p5)
  call check(" m2enngfz (0.8)",finite,8.1947673365204871e7)
  finite = pm2enngfavz(p1,pol1,p2,p3,p4,p5)
  call check("pm2enngfz (0.8)",finite,1.1626352746249932e8)

  call blockend(20)

  musq=mm**2
  pol1 = (/ 0., 0., 0., 0. /)

  END SUBROUTINE

  SUBROUTINE TESTMJMATEL
  use vegas_m
  implicit none
  integer i,ranseed
  real(kind=prec) :: arr(5), weight, pol1(4), pol2(4)
  real(kind=prec), parameter :: xicut = 0.1

  CLj = 0.1_prec
  CRj = 0.02_prec
  Mj = 1._prec !MeV

  pol1 = (/ 0., 0., 0., 0. /)
  pol2 = (/ 0., 0., 0.85, 0. /)

  print*, " "
  call initflavour("mu-eOld")
  musq = Mm**2
  call blockstart("Majoron test")
  ranseed = 42
  do i=1,2
    arr(i) = ran2(ranseed)
  enddo
  call psd3(arr(1:2),p1,Mm,p2,Me,p3,Mj,weight)

  print*, "----------------------------------------------"
  print*, "                  mu --> e + J                "
  print*, "----------------------------------------------"

  !print*, "Mm:", sqrt(sq(p1)), "Me:", sqrt(sq(p2))
  !print*, "Mj:", sqrt(sq(p3)), "s2n:", s(pol2,p2)
  !print*, m2ejl(p1,pol2,p2,p3)


  call check("m2ej ", pm2ej (p1,pol1,p2,p3), 116.52663743110394,1e-16)
  call check("m2ej ", pm2ej (p1,pol2,p2,p3), 112.04758385058322,1e-16)
  call check("m2ejl", pm2ejl(p1,pol1,p2,p3), 1152.5348227430686  ,1e-16)
  call check("m2ejl", pm2ejl(p1,pol2,p2,p3), 1108.2047768908712  ,1e-16)



  pol1 = (/ 0., 0., 0., 0. /)
  pol2 = (/ 0., 0., 0.85, 0. /)

  CLj = 0.1_prec
  CRj = 0.02_prec
  Mj = 1._prec !MeV

  ranseed = 4242
  do i=1,5
    arr(i) = ran2(ranseed)
  enddo
  call psd4_fks(arr,p1,Mm,p2,Me,p3,Mj,p4,weight)

  print*, "----------------------------------------------"
  print*, "             mu --> e + J + gamma             "
  print*, "----------------------------------------------"

  !print*, "Mm:", sqrt(sq(p1)), "Me:", sqrt(sq(p2))
  !print*, "Mj:", sqrt(sq(p3)), "Mg:", sqrt(sq(p4))
  !print*, "s14:", s(p1,p4), "s24:", s(p2,p4)
  !print*, "s2n:", s(pol1,p2), "s4n:", s(pol1,p4)
  !print*, "s2n:", s(pol2,p2), "s4n2:", s(pol2,p4)

  call check("m2ejg", pm2ejg(p1,pol1,p2,p3,p4), 48627.8811945461  ,1e-16)
  call check("m2ejg", pm2ejg(p1,pol2,p2,p3,p4), 10722.023858830065,1e-16)

  call blockend(6)

  END SUBROUTINE


  SUBROUTINE TESTMUDECSOFT
  use vegas_m
  implicit none
  real(kind=prec) :: arr(8)
  integer i, ranseed

  ranseed = 23313
  musq = Mm**2
  xinormcut1 = 0.3
  xinormcut2 = 0.3

  do i=1,8
    arr(i) = ran2(ranseed)
  enddo

  call blockstart("mudec \xi->0")
  call test_softlimit(arr, [ "m2ennR ", "m2ennRF" ], (/ 12, 8/) )

  call blockstart("mudec majoron \xi->0")
  CLj = 0.1_prec
  CRj = 0.02_prec
  Mj = 1._prec !MeV
  call test_softlimit(arr, [ "m2ejR"] , (/ 8 /))
  END SUBROUTINE

  SUBROUTINE TESTMUDECCOL
  use vegas_m
  use mudec
  implicit none
  integer, parameter :: ntest = 2
  integer i,j,ido, ranseed, lastgood(ntest)
  real(kind=prec) :: arr(8), weight
  real(kind=prec) :: full(ntest), lim(ntest), lastratio(ntest), ratio(ntest)
  character(len=5) :: red = char(27)//'[31m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=5),dimension(ntest) :: colours
  real(kind=prec) :: z

  call initflavour("mu-e")
  call blockstart("mudec y -> 1")
  ranseed = 2312913
  xieik1 = 0.3
  musq = Mm**2
  pol1 = 0.

  do i=1,8
    arr(i) = ran2(ranseed)
  enddo
  lastratio = 1.
  do i=2,10
    arr(2) = 1-0.8_prec/(3.**i)
    call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)

    full = (/ pm2enngav  (p1,pol1,p2,p3,p4,p5) , &
              pm2ennglav (p1,pol1,p2,p3,p4,p5) /)

    z = p2(4) / (p2(4)+p5(4))
    lim  = (/ split0_fsr(p2,p5) * pm2ennav  (p1, pol1, p2 / z, p3, p4), &
              split0_fsr(p2,p5) * pm2ennlav0(p1, pol1, p2 / z, p3, p4) &
            + split1_fsr(p2,p5) * pm2ennav  (p1, pol1, p2 / z, p3, p4) /)

    print*, "        "
    print*, " WEIGHT: ", weight, yout
    ratio = abs(1-lim/full)
    print*,ratio(1), lastratio(1)
    do j=1,2
      if (ratio(j)<lastratio(j)) then
        lastgood(j) = i
        lastratio(j) = ratio(j)
        colours(j) = norm
      else
        colours(j) = red
      endif
    enddo
    write(*,900) colours(1),ratio(1), colours(2),ratio(2), &
                 norm
  enddo
  call check("tree-level",lastratio(1),threshold=5e-4)
  call check("real-virtual",lastratio(2),threshold=5e-4)

  call blockend(2)

900 format("1-ratio: ",A,ES13.3, A,ES13.3,A)
  END SUBROUTINE TESTMUDECCOL


  SUBROUTINE TESTMUDECSPEEDFF
  use mudec_h2lff_full
  use massification
  use mudec_h20ff
  implicit none
  integer :: niter
  real(kind=prec) :: F11(-1:1), F21(0:1), F12(-2:0), F22(-1:0)
  real(kind=prec) :: ol1(-2:2), tl1(-4:0), ol2(-2:2), tl2(-4:0)
  real(kind=prec) :: olZ1(-1:1), tlZ1(-2:0), olZ2(-1:1), tlZ2(-2:0)
  real(kind=prec) :: x, z
  real (kind=prec) :: startt, endt, rate(2)
  integer i, j, ranseed

  call initflavour("mu-e")
  musq = Mm**2
  z = Me / Mm

  call blockstart("mudec FF performance")

  niter = 20
  ranseed = 2332
  call cpu_time(startt)
  do i=1,niter
    x = ran2(ranseed)
    call h2lff(x, z, 0., f11, f12, f21, f22)
  enddo
  call cpu_time(endt)
  rate(1) = niter/(endt-startt)

  ranseed = 2332
  niter = 30000 * niter
  call cpu_time(startt)
  do i=1,niter
    x = ran2(ranseed)
    call h20ff(x, 0., ol1, tl1, ol2, tl2)
    call massify(log(z**2), 0.5, 1., ol1, tl1, olZ1, tlZ1)
    call massify(log(z**2), 0.5, 0., ol2, tl2, olZ2, tlZ2)
  enddo
  call cpu_time(endt)
  rate(2) = niter/(endt-startt)

  print*,rate
  print*,rate(2)/rate(1)
  END SUBROUTINE TESTMUDECSPEEDFF

  SUBROUTINE TESTMUDECSPEED
  implicit none
  integer, parameter :: niter = 20000
  real (kind=prec) :: weight, arr(8), finite
  real (kind=prec) :: startt, endt, rate
  integer i, j, ranseed

  call initflavour("mu-e")
  musq = Mm**2
  call blockstart("mudec performance")
  ! on my notebook I saw the following rates
  !   no optimisation:     ~32 kEv/s
  !   -O3 optimisation:   ~122 kEv/s
  !   MMA               : ~121 kEv/s
  !   MMA + -O3         : ~123 kEv/s
  ! -> use only O3 optimisation and don't bother with MMA optimisation

  pol1 = (/ 0., 0., 0.8, 0. /)
  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
    if (weight.lt.zero) cycle
    finite=pm2ennglav(p1, pol1, p2, p3,p4,p5)
  enddo
  call cpu_time(endt)
  rate = niter/(endt-startt)
  call checkrange("event rate (Ev/s)", rate, 50.e2,1e50)



#ifdef RADMUDECCDR
  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    call psd5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
    if (weight.lt.zero) cycle
    finite=pm2ennglavcdr(p1, pol1, p2, p5)
  enddo
  call cpu_time(endt)
  call checkrange("speed-up", rate/(niter/(endt-startt)), 10.,100.)
  call blockend(2)
#else
  write(*,'(A)')char(27)//'[33m[CANT]'//char(27)//'[0m cannot compare event rate. Compile with USE_RADMUDEC_CDR=1 to enable'
  call blockend(1)
#endif

  END SUBROUTINE



  SUBROUTINE TESTMUDECVEGAS
  xinormcut = 0.8
  xinormcut1 = 0.2
  xinormcut2 = 0.3

  call blockstart("mudec VEGAS test")
  call initflavour("mu-e")
  musq = Mm**2

  call test_INT("m2enn0"             , 10, 10,  9.4776373945996421E+05)
  call test_INT("m2ennF"             , 10, 10,  2.0428804010385450E+06)
  call test_INT("m2ennR"             , 30, 20, -2.5134241661530929E+06)
  call test_INT("m2ennFFz"           , 10, 10,  4.1779653062962005E+06)
  ! mathematica integrated
  call test_int("m2ennFFz"           , 10, 10,  4.2167546747618970E+06,gtol=1e-2)
  call test_INT("m2ennRF"            , 30, 20,  1.3457970051011752E+05)
  call test_INT("m2ennRR"            , 30, 20, -1.4735266125101727E+06)
  call test_INT("m2enng0"            , 10, 10,  3.9577793579551121E+06)
  call test_INT("m2enngV"            , 10, 10,  2.8597068579158325E+07)
  call test_INT("m2enngC"            , 10, 10, -2.1165168481096186E+07)
  call test_INT("m2enngR"            , 10, 10, -1.5713177783653779E+07)
  call test_INT("m2ennNF"            , 10, 10, -8.4957845926738973E+05)
  call blockend(12)
  END SUBROUTINE

  SUBROUTINE TESTMUDECPOINTWISE
  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 0
  xinormcut = 1
  xinormcut1 = 1
  xinormcut2 = 1

  call blockstart("mudec point-wise test")
  call initflavour("mu-e")
  musq = Mm**2

  call test_INT("m2enn0"             , ans= 3.9045258942553564E+05)
  call test_INT("m2ennF"             , ans= 6.7760979669019056E+05)
  call test_INT("m2ennR"             , ans= 2.8710636508609913E+05)
  call test_INT("m2ennFFz"           , ans= 1.5535308364316488E+06)
  call test_INT("m2ennNF"            , ans=-2.0058452878743043E+04)
  call test_INT("m2ennRF"            , ans= 8.8186389204829792E+05,tol=1e-8)
  call test_INT("m2ennRR"            , ans=-7.0654649099267437E+05)
  call test_INT("m2enng0"            , ans= 2.2278505943480162E+06)
  call test_INT("m2enngV"            , ans= 1.8031929597686503E+07)
  call test_INT("m2enngC"            , ans=-9.6760256030575540E+06)
  call test_INT("m2enngR"            , ans=-1.7243074233377245E+05)
  call blockend(10)


  END SUBROUTINE


  SUBROUTINE TESTMJVEGAS
  CLj = 0.1_prec
  CRj = 0.02_prec
  Mj = 1._prec !MeV
  xinormcut = 0.8
  xinormcut1 = 0.2
  xinormcut2 = 0.3

  call initflavour("mu-e")
  musq = Mm**2

  call blockstart("mu-> ej VEGAS test")
  call test_INT("m2ej0"              , 10, 10,  6.6820354734549343E-03)
  call test_INT("m2ejF"              , 10, 10,  1.1454456367351137E-02, tol=1.E-9)
  call test_INT("m2ejR"              , 30, 50, -9.5085986158096464E-03, tol=1.E-1)
  call test_INT("m2ejg0"             , 30, 50,  1.7057462832334110E-02, tol=1.E-1)

  call blockend(4)
  END SUBROUTINE

  SUBROUTINE TESTMJPOINTWISE
  CLj = 0.1_prec
  CRj = 0.02_prec
  Mj = 1._prec !MeV
  xinormcut = 1
  xinormcut1 = 1
  xinormcut2 = 1

  call initflavour("mu-e")
  musq = Mm**2

  call blockstart("mu-> ej VEGAS pointwise")
  call test_INT("m2ej0"              , ans= 2.1031561261180699E-02)
  call test_INT("m2ejF"              , ans= 4.9076000260425190E-02)
  call test_INT("m2ejR"              , ans=-7.5011104083675777E-03)
  call test_INT("m2ejg0"             , ans= 1.6540966584828996E-05)

  call blockend(4)
  END SUBROUTINE




  SUBROUTINE TESTMUDECMASSIFICATION
  use massification
  use mudec_h20ff
  use mudec_h2lff
  implicit none
  real(kind=prec) :: x
  real(kind=prec) :: ol1(-2:2), tl1(-4:0), ol2(-2:2), tl2(-4:0)
  real(kind=prec) :: olZ1(-1:1), tlZ1(-2:0), olZ2(-1:1), tlZ2(-2:0)

  musq = Mmu**2
  call blockstart("mudec massification")
  call h20ff(0.3, log(musq/Mmu**2), ol1, tl1, ol2, tl2)
  call init_hpls(0.3, Mel/Mmu)

  call massify(log(Mel**2/musq), 0.5, 1., ol1, tl1, olZ1, tlZ1, checkpole=1e-14)
  call massify(log(Mel**2/musq), 0.5, 0., ol2, tl2, olZ2, tlZ2, checkpole=1e-14)

  call check("F1 a^1 ep^-1", olZ1(-1), ff1a1es())
  call check("F1 a^1 ep^ 0", olZ1( 0), ff1a1ef())
  call check("F1 a^1 ep^+1", olZ1(+1), ff1a1el())
  call check("F1 a^2 ep^-2", tlZ1(-2), ff1a2ed())
  call check("F1 a^2 ep^-1", tlZ1(-1), ff1a2es())
  call check("F1 a^2 ep^ 0", tlZ1( 0), ff1a2ef())

  call check("F2 a^1 ep^ 0", olZ2( 0), ff2a1ef())
  call check("F2 a^1 ep^+1", olZ2(+1), ff2a1el())
  call check("F2 a^2 ep^-1", tlZ2(-1), ff2a2es())
  call check("F2 a^2 ep^ 0", tlZ2( 0), ff2a2ef())

  END SUBROUTINE

                 !!!!!!!!!!!!!!!!!!!!!!!!!
                   END MODULE MUDEC_TEST
                 !!!!!!!!!!!!!!!!!!!!!!!!!
