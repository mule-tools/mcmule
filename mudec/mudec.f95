
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MUDEC
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use phase_space, only: ksoft, ksoftA, ksoftB
  use mudec_mat_el

!!!  use amplitude_lo, only: amplitude_n_LO

  implicit none

  contains


  FUNCTION M2ENN_part(q1, q2, q3, q4)
    !! mu+(q1) -> e+(q2) nu_e(q3) \bar{nu}_mu(q4)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  nu_mu(q4)
    !! for massive electron
    !! average over neutrino tensor taken
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  type(particles) :: m2enn_part
  m2enn_part = parts((/part(q1, 1, 1), part(q2, 1, -1)/))
  END FUNCTION M2ENN_part


  FUNCTION M2EJ_part(p1, p2, p3)
    !! mu+(p1) -> e+(p2) J(p3)
    !! mu-(p1) -> e-(p2) J(p3)
  real (kind=prec) :: p1(4),p2(4),p3(4)
  type(particles) :: m2ej_part
  m2ej_part = parts((/part(p1, 1, 1), part(p2, 1, -1)/))
  END FUNCTION M2EJ_part


          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!        SINGULAR    LIMITS          !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
  ! for massive (and massless) electron
  FUNCTION PM2ENNG_S(q1,n1,q2,q3,q4)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),n1(4)
  real (kind=prec) :: pm2enng_s

  pm2enng_s = 2*eik(ksoft, parts((/part(q1, 1, 1), part(q2, 1, -1)/)))*pm2enn(q1,n1,q2,q3,q4)
  pm2enng_s = (4.*pi*alpha)*pm2enng_s

  END FUNCTION PM2ENNG_S



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(q5)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(q5)
  ! for massless electron
  FUNCTION PM2ENNG_C(q1,n1,q2,q3,q4,q5)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4),n1(4)
  real (kind=prec) :: pm2enng_c, zz

  zz = q2(4)/(q2(4)+q5(4))

  pm2enng_c =  1._prec/q5(4)/q2(4)*                      &
             (1.+zz**2)/(1.-zz)*pm2enn(q1,n1,q2/zz,q3,q4)
  pm2enng_c = (4.*pi*alpha)*pm2enng_c

  END FUNCTION PM2ENNG_C



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
  ! for massless electron
  FUNCTION PM2ENNG_SC(q1,n1,q2,q3,q4)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),n1(4)
  real (kind=prec) :: pm2enng_sc, s12

  s12 = sq(q1)

  pm2enng_sc =  8._prec/s12*pm2enn(q1,n1,q2,q3,q4)
  pm2enng_sc = (4.*pi*alpha)*pm2enng_sc

  END FUNCTION PM2ENNG_SC



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(q5) y(ksoft)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(q5) y(ksoft)
  ! for massive (and massless) electron
  FUNCTION PM2ENNGG_S(q1,n1,q2,q3,q4,q5)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4),n1(4)
  real (kind=prec) :: pm2enngg_s

  pm2enngg_s = 2*eik(ksoft, parts((/part(q1, 1, 1), part(q2, 1, -1)/)))*pm2enng(q1,n1,q2,q3,q4,q5)
  pm2enngg_s = (4.*pi*alpha)*pm2enngg_s

  END FUNCTION PM2ENNGG_S

  FUNCTION PM2ENNGGav_S(q1,n1,q2,q3,q4,q5)
    !! mu+(q1) -> e+(q2) nu_e(q3) \bar{nu}_mu(q4) g(q5) g(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  nu_mu(q4) g(q5) g(ksoft)
    !! for massive (and massless) electron

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4),n1(4)
  real (kind=prec) :: pm2ennggav_s

  pm2ennggav_s = 2*eik(ksoft, parts((/part(q1, 1, 1), part(q2, 1, -1)/)))*pm2enngav(q1,n1,q2,q3,q4,q5)
  pm2ennggav_s = (4.*pi*alpha)*pm2ennggav_s

  END FUNCTION PM2ENNGGav_S


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(q5) y(q6)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(q5) y(q6)
  ! for massless electron
  FUNCTION PM2ENNGG_C(q1,n1,q2,q3,q4,q5,q6)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4),q6(4),n1(4)
  real (kind=prec) :: pm2enngg_c, zz

  zz = q2(4)/(q2(4)+q6(4))

  pm2enngg_c =  1._prec/q6(4)/q2(4)*                      &
               (1.+zz**2)/(1.-zz)*pm2enng(q1,n1,q2/zz,q3,q4,q5)
  pm2enngg_c = (4.*pi*alpha)*pm2enngg_c

  END FUNCTION PM2ENNGG_C



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(q5) y(ksoft)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(q5) y(ksoft)
  ! for massless electron
  FUNCTION PM2ENNGG_SC(q1,n1,q2,q3,q4,q5)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4),n1(4)
  real (kind=prec) :: pm2enngg_sc, s12

  s12 = sq(q1)

  pm2enngg_sc =  8._prec/s12*pm2enng(q1,n1,q2,q3,q4,q5)
  pm2enngg_sc = (4.*pi*alpha)*pm2enngg_sc

  END FUNCTION PM2ENNGG_SC



          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!             NNLO                   !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
  ! for massive electron
  ! average over neutrino tensor taken
  FUNCTION PM2ENNGav_S(q1,n1,q2,q3,q4,lin)
  real (kind=prec) :: q1(4),q2(4),n1(4)
  real (kind=prec) :: q3(4), q4(4)
  real (kind=prec),optional :: lin
  real (kind=prec) :: pm2enngav_s

  ! TODO
  if (present(lin)) then
    pm2enngav_s = 2*eik(ksoft, parts((/part(q1, 1, 1), part(q2, 1, -1)/)))*pm2ennav(q1,n1,q2,q3,q4, lin)
    pm2enngav_s = (4.*pi*alpha)*pm2enngav_s

    lin = 4*eik(ksoft, parts((/part(q1, 1, 1), part(q2, 1, -1)/)))*lin
    lin = (4.*pi*alpha)*lin
  else
    pm2enngav_s = 2*eik(ksoft, parts((/part(q1, 1, 1), part(q2, 1, -1)/)))*pm2ennav(q1,n1,q2,q3,q4)
    pm2enngav_s = (4.*pi*alpha)*pm2enngav_s
  endif

  END FUNCTION PM2ENNGav_S

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
  ! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
  ! for massive electron
  ! average over neutrino tensor taken
  FUNCTION PM2ENNGlav_S(q1,n1,q2,q3,q4, sing)

  real (kind=prec) :: q1(4),q2(4),n1(4)
  real (kind=prec) :: q3(4), q4(4)
  real (kind=prec) :: pm2ennglav_s,pole
  real (kind=prec), optional :: sing

  ! TODO
  pm2ennglav_s = pm2ennlav(q1,n1,q2,q3,q4,pole)
  pm2ennglav_s = 2*eik(ksoft, parts((/part(q1, 1, 1), part(q2, 1, -1)/)))*pm2ennglav_s
  pm2ennglav_s = (4.*pi*alpha)*pm2ennglav_s

  if (present(sing)) then
    sing = 2*eik(ksoft, parts((/part(q1, 1, 1), part(q2, 1, -1)/)))*pole
    sing = (4.*pi*alpha)*sing
  endif
  END FUNCTION PM2ENNGlav_S


  FUNCTION PM2ENNGfavz_S(q1,n1,q2,q3,q4)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
    !! for massified electron
    !! average over neutrino tensor taken

  real (kind=prec) :: q1(4),q2(4),n1(4)
  real (kind=prec) :: q3(4), q4(4)
  real (kind=prec) :: pm2enngfavz_s, mat0, ctfin

  mat0 = pm2enngav_s(q1,n1,q2,q3,q4)
  ctfin = m2enneik(q1, q2, xieik1)

  pm2enngfavz_s = pm2ennlav(q1,n1,q2,q3,q4)
  pm2enngfavz_s = 2*eik(ksoft, parts((/part(q1, 1, 1), part(q2, 1, -1)/)))*pm2enngfavz_s
  pm2enngfavz_s = (4.*pi*alpha)*pm2enngfavz_s

  pm2enngfavz_s = pm2enngfavz_s+ctfin*mat0

  END FUNCTION PM2ENNGfavz_S



  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) y(ksoft) y(ksoft)
  ! mu-(p1) -> e-(p2) \bar{nu}_e(p3)  \nu_\mu(p4) y(ksoft) y(ksoft)
  ! for massive electron
  ! average over neutrino tensor taken
  FUNCTION PM2ENNGGav_SS(p1, n1, p2,q3,q4)
  real(kind=prec) :: p1(4), p2(4), n1(4)
  real (kind=prec) :: q3(4), q4(4)
  real(kind=prec) :: pm2ennggav_ss

  pm2ennggav_ss = (2*eik(ksoftB, parts((/part(p1, 1, 1), part(p2, 1, -1)/))))&
                 *(2*eik(ksoftA, parts((/part(p1, 1, 1), part(p2, 1, -1)/))))*pm2ennav(p1,n1, p2,q3,q4)
  pm2ennggav_ss = (4.*pi*alpha)**2*pm2ennggav_ss

  END FUNCTION PM2ENNGGav_SS


                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE MUDEC
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
