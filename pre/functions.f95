
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE FUNCTIONS
                          !!!!!!!!!!!!!!!!!!!!!!

  USE GLOBAL_DEF
#ifndef HEADER_ONLY
  USE collier
#endif


  IMPLICIT NONE

  TYPE MLM   ! MassLess Momentum
    real(kind=prec), dimension(4) :: momentum
  END TYPE MLM

  TYPE PARTICLE
    real(kind=prec), dimension(4) :: momentum
    integer :: effcharge
    integer :: charge
    logical :: incoming
    integer :: lepcharge
  END TYPE PARTICLE

  TYPE PARTICLES
    type(particle), dimension(maxparticles) :: vec
    integer :: n
    character(len=1) :: combo
  END TYPE PARTICLES

  INTERFACE S
    module procedure Sversion1, Sversion2, Sversion3, Sversion4, Sversion5, Sversion6
  END INTERFACE

  INTERFACE SQ
    module procedure SQversion1
  END INTERFACE

  INTERFACE EIK
    module procedure eik00, eik0M, eikM0, eikMM, eik_part
  END INTERFACE
  INTERFACE IEIK
    module procedure ieikMM, ieik_part
  END INTERFACE

  INTERFACE IREG
    module procedure ireg_00, ireg_0M, ireg_M0, ireg_MM, ireg_SELF
  END INTERFACE
  INTERFACE ISIN
    module procedure isin_00, isin_0m, isin_m0, isin_MM, isin_SELF
  END INTERFACE
  INTERFACE ILIN
    module procedure ilin_MM, ilin_SELF
  END INTERFACE

  INTERFACE DISCB
    module procedure discbSMN, discbSMM
  END INTERFACE
  INTERFACE DISCB_CPLX
    module procedure discbSMM_cplx, discbSMN_cplx
  END INTERFACE
  INTERFACE SCALARC0IR6
    module procedure scalarc0ir6SMM, scalarc0ir6SMN
  END INTERFACE
  INTERFACE SCALARC0IR6_CPLX
    module procedure scalarc0ir6SMM_cplx, scalarc0ir6SMN_cplx
  END INTERFACE
  INTERFACE SCALARC0
    module procedure scalarc0SM
  END INTERFACE
  INTERFACE SCALARC0_CPLX
    module procedure scalarc0SM_cplx
  END INTERFACE
  INTERFACE SCALARD0IR16
    module procedure scalard0ir16XYM, scalard0ir16mn
  END INTERFACE
  !INTERFACE SCALARD0IR16_CPLX
  !  module procedure scalard0ir16XYM_cplx
  !END INTERFACE


  ABSTRACT INTERFACE
    function partInterface(q1,q2,q3,q4,q5,q6,q7)
      use global_def, only: prec, maxparticles
      import particle
      import particles
      real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
      type(particles) :: partInterface
     end function
  END INTERFACE

  procedure(partInterface), pointer :: partfunc

  real(kind=prec), pointer :: matel0, matel1, matel2 => null()
  external matel0, matel1, matel2
  real(kind=prec), pointer :: matel_s, matel_hs, matel_sh, matel_ss => null()
  external matel_s, matel_hs, matel_sh, matel_ss

  CONTAINS


  SUBROUTINE SWAPMOM(vecs, i, j)
  real(kind=prec) :: vecs(:,:)
  real(kind=prec) :: dummy(size(vecs(:,1)))
  integer :: i, j

  dummy = vecs(:,i)
  vecs(:,i) = vecs(:,j)
  vecs(:,j) = dummy

  END SUBROUTINE SWAPMOM


  FUNCTION MAKE_MLM(qq)
  real(kind=prec), intent(in) :: qq(4)
  type(mlm) :: make_mlm

  make_mlm%momentum = qq

  END FUNCTION MAKE_MLM


  FUNCTION PART(qq, charge, inc, lepcharge)
  real(kind=prec), intent(in) :: qq(4)
  integer, intent(in) :: charge
  integer, intent(in) :: inc
  integer, intent(in), optional :: lepcharge
  type(particle) :: part

  part%momentum = qq
  part%charge = charge
  part%incoming = inc.eq.1

  if(inc.eq.1) then
    part%effcharge = +charge
  else
    part%effcharge = -charge
  endif

  if(present(lepcharge)) then
    part%lepcharge = lepcharge
  else
    ! default value
    part%lepcharge = 0
  endif
  END FUNCTION PART

  FUNCTION PARTS(ps, combo)
  type(particle), intent(in), dimension(:) :: ps
  character(len=1), intent(in), optional :: combo
  integer i
  type(particles) :: parts

  parts%n = size(ps)
  do i=1,size(ps)
    parts%vec(i) = ps(i)
  enddo

  if (present(combo)) then
    parts%combo = combo
  else
    ! default value, avoids combo-wise selection in (I)EIK_PART and NTSSOFT
    parts%combo = "*"
  endif
  END FUNCTION PARTS

  FUNCTION COMBOEIK(pi,pj)
  implicit none
  type(particle) pi,pj
  character(len=1) :: comboeik

  if(pi%lepcharge.ne.pj%lepcharge) then
    ! any mixed combo
    comboeik = "x"
  elseif(pi%lepcharge.eq.1) then
    comboeik = "e"
  elseif(pi%lepcharge.eq.2) then
    comboeik = "m"
  elseif(pi%lepcharge.eq.3) then
    comboeik = "t"
  endif
  END FUNCTION COMBOEIK

  FUNCTION COMBONTS(pi,pj,pk,mx)
  implicit none
  type(particle) pi,pj,pk
  integer sum
  integer, intent(in), optional :: mx
  character(len=1) :: combonts

  sum = 2*pi%lepcharge + pj%lepcharge + pk%lepcharge ! lepcharge relevant to each addendum in the ntssoft sum

  ! electron_lepcharge (qe) = 1
  ! muon_lepcharge     (qm) = 2
  ! tau_lepcharge      (qt) = 3

  if(sum.eq.4) then
    combonts = "e"         ! 2*qe + qe + qe = 4
  elseif(sum.eq.8) then
    combonts = "m"         ! 2*qm + qm + qm = 8
  elseif(sum.eq.12) then
    combonts = "t"         ! 2*qt + qt + qt = 12
  elseif((present(mx)).and.(mx.eq.1)) then
    if(sum.eq.5) then
      combonts = "1"         ! 2*qe + qe + qm = 5
    elseif(sum.eq.6) then
      combonts = "2"         ! 2*qe + qm + qm = 6
    elseif(sum.eq.7) then
      combonts = "3"         ! 2*qm + qe + qm = 7
    endif
  else
    combonts = "x"         ! any other combo
  endif
  END FUNCTION COMBONTS

  FUNCTION Sversion1(k1,k2)
     ! S(k1,k2) = 2 k1.k2

  type(mlm), intent(in) :: k1,k2
  real (kind=prec) :: Sversion1,dot_dot,q1(4),q2(4)
  q1 = k1%momentum
  q2 = k2%momentum

  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  Sversion1 =  2*dot_dot

  END FUNCTION Sversion1


  FUNCTION Sversion2(q1,q2)
     ! S(q1,q2) =  2 q1.q2

  real (kind=prec), intent(in) :: q1(4),q2(4)
  real (kind=prec) :: Sversion2,dot_dot
  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  Sversion2 =  2*dot_dot

  END FUNCTION Sversion2



  FUNCTION Sversion3(k1,q2)
     ! S(q1,q2) =  2 q1.q2

  type(mlm), intent(in) :: k1
  real (kind=prec), intent(in) :: q2(4)
  real (kind=prec) :: q1(4)
  real (kind=prec) :: Sversion3,dot_dot
  q1 = k1%momentum
  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  Sversion3 =  2*dot_dot

  END FUNCTION Sversion3



  FUNCTION Sversion4(q1,k2)
     ! S(q1,q2) = 2 q1.q2

  type(mlm), intent(in) :: k2
  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec) :: q2(4)
  real (kind=prec) :: Sversion4,dot_dot
  q2 = k2%momentum
  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  Sversion4 =  2*dot_dot

  END FUNCTION Sversion4

  FUNCTION Sversion5(k1,k2)
     ! S(q1,q2) = 2 q1.q2

  type(particle), intent(in) :: k1, k2
  real (kind=prec) :: q1(4), q2(4)
  real (kind=prec) :: Sversion5,dot_dot
  q1 = k1%momentum
  q2 = k2%momentum
  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  Sversion5 =  2*dot_dot

  END FUNCTION Sversion5

  FUNCTION Sversion6(q1,k2)
     ! S(q1,q2) = 2 q1.q2

  type(particle), intent(in) :: k2
  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec) :: q2(4)
  real (kind=prec) :: Sversion6,dot_dot
  q2 = k2%momentum
  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  Sversion6 =  2*dot_dot

  END FUNCTION Sversion6


  FUNCTION SQversion1(q1)
     ! SQ(q1) = (q1)^2

  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec) :: SQversion1

  SQversion1  = q1(4)**2 - q1(1)**2 - q1(2)**2 - q1(3)**2

  END FUNCTION SQversion1


  FUNCTION ASYMTENSOR(p1,p2,p3,p4)
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: asymtensor

  asymtensor &
       =  + p1(4)*p2(3)*p3(2)*p4(1) - p1(3)*p2(4)*p3(2)*p4(1) - p1(4)*p2(2)*p3(3)*p4(1) &
          + p1(2)*p2(4)*p3(3)*p4(1) + p1(3)*p2(2)*p3(4)*p4(1) - p1(2)*p2(3)*p3(4)*p4(1) &
          - p1(4)*p2(3)*p3(1)*p4(2) + p1(3)*p2(4)*p3(1)*p4(2) + p1(4)*p2(1)*p3(3)*p4(2) &
          - p1(1)*p2(4)*p3(3)*p4(2) - p1(3)*p2(1)*p3(4)*p4(2) + p1(1)*p2(3)*p3(4)*p4(2) &
          + p1(4)*p2(2)*p3(1)*p4(3) - p1(2)*p2(4)*p3(1)*p4(3) - p1(4)*p2(1)*p3(2)*p4(3) &
          + p1(1)*p2(4)*p3(2)*p4(3) + p1(2)*p2(1)*p3(4)*p4(3) - p1(1)*p2(2)*p3(4)*p4(3) &
          - p1(3)*p2(2)*p3(1)*p4(4) + p1(2)*p2(3)*p3(1)*p4(4) + p1(3)*p2(1)*p3(2)*p4(4) &
          - p1(1)*p2(3)*p3(2)*p4(4) - p1(2)*p2(1)*p3(3)*p4(4) + p1(1)*p2(2)*p3(3)*p4(4)
  END FUNCTION ASYMTENSOR



!==============  eikonal factors  ==============!


  FUNCTION EIK00(k1,kg,k2)

  type(mlm), intent(in) :: k1,k2,kg
  real (kind=prec) :: eik00

  eik00 = 2*s(k1,k2)/(s(k1,kg)*s(kg,k2))

  END FUNCTION EIK00


  FUNCTION EIK0M(k1,kg,q2)

  type(mlm), intent(in) :: k1,kg
  real (kind=prec), intent(in) :: q2(4)
  real (kind=prec) :: eik0M

  eik0m = 2*s(k1,q2)/(s(k1,kg)*s(kg,q2)) - 2*sq(q2)/(s(kg,q2)**2)

  END FUNCTION EIK0M


  FUNCTION EIKM0(q1,kg,k2)

  type(mlm), intent(in) :: k2,kg
  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec) :: eikM0

  eikm0 = 2*s(k2,q1)/(s(k2,kg)*s(kg,q1)) - 2*sq(q1)/(s(kg,q1)**2)

  END FUNCTION EIKM0


  FUNCTION EIKMM(q1,kg,q2)

  type(mlm), intent(in) :: kg
  real (kind=prec), intent(in) :: q1(4), q2(4)
  real (kind=prec) :: eikMM

  eikmm = 2*s(q1,q2)/(s(q1,kg)*s(kg,q2))     &
       - 2*sq(q1)/(s(kg,q1)**2) - 2*sq(q2)/(s(kg,q2)**2)

  END FUNCTION EIKMM


  FUNCTION EIK_PART(kg, pp)
  type(mlm), intent(in) :: kg
  type(particles), intent(in) :: pp
  real (kind=prec) :: eik_part
  integer i, j, sgn

  eik_part = 0.
  do i=1,pp%n
    if(pp%vec(i)%charge == 0) cycle
    if(pp%combo.ne."x") eik_part = eik_part - 2*sq(pp%vec(i)%momentum)/(s(kg,pp%vec(i)%momentum)**2)
    do j=i+1,pp%n
      if(pp%vec(j)%charge == 0) cycle
      if((pp%combo.ne."*").and.(comboeik(pp%vec(i),pp%vec(j)).ne.pp%combo)) cycle
      if(pp%vec(i)%effcharge.eq.pp%vec(j)%effcharge) then
        sgn = -1
      else
        sgn = +1
      endif
      eik_part = eik_part + sgn * 2*s(pp%vec(i)%momentum, pp%vec(j)%momentum)/(s(pp%vec(i)%momentum,kg)*s(kg, pp%vec(j)%momentum))
    enddo
  enddo

  END FUNCTION EIK_PART

!===========  end of eikonal factors and AP splitting kernels ===========!



!==============  massive splitting functions  ==============!

  FUNCTION SPLIT0_SOFT(p,k)
  real(kind=prec), intent(in) :: p(4), k(4)
  real(kind=prec) :: split0_soft
  real(kind=prec) :: m2, spk, omega

  m2 = sq(p)
  spk = s(p,k)
  omega = k(4)/p(4)
  split0_soft = 1/spk/omega - m2/spk**2
  split0_soft = 16*pi*alpha*split0_soft
  END FUNCTION

  FUNCTION SPLIT0_ISR(p,k)
  real(kind=prec) :: split0_isr
  real(kind=prec) :: p(4), k(4)
  real(kind=prec) :: m2, x, spk

  m2 = sq(p)
  spk = s(p,k)
  x = (p(4)-k(4))/p(4)

  split0_isr = (1+x**2)/(1-x)/x/spk-2*m2/spk**2
  split0_isr = 8*pi*alpha*split0_isr

  END FUNCTION


  FUNCTION SPLIT0_FSR(p,k)
  real(kind=prec) :: split0_fsr
  real(kind=prec) :: p(4), k(4)
  real(kind=prec) :: m2, z, spk

  m2 = sq(p)
  spk = s(p,k)
  z = p(4)/(p(4)+k(4))

  split0_fsr = (1+z**2)/(1-z)/spk-2*m2/spk**2
  split0_fsr = 8*pi*alpha*split0_fsr

  END FUNCTION


  FUNCTION SPLIT1_AC_ISR(p,k,doub,sing)
  real(kind=prec) :: split1_ac_isr
  real(kind=prec) :: p(4), k(4)
  real(kind=prec), optional :: doub, sing
  real(kind=prec) :: m2, logmu, pref
  real (kind=prec), parameter :: zeta2 = 1.6449340668482264365

  m2 = sq(p)
  logmu = log(musq/m2)
  pref = split0_isr(p,k)*alpha/pi/4

  split1_ac_isr = pref*(3+2*zeta2+logmu+logmu**2)

  if(present(doub)) doub = pref*2
  if(present(sing)) sing = pref*(1+2*logmu)

  END FUNCTION

  FUNCTION SPLIT1_AC_FSR(p,k,doub,sing)
  real(kind=prec) :: split1_ac_fsr
  real(kind=prec) :: p(4), k(4)
  real(kind=prec), optional :: doub, sing
  real(kind=prec) :: m2, logmu, pref
  real (kind=prec), parameter :: zeta2 = 1.6449340668482264365

  m2 = sq(p)
  logmu = log(musq/m2)
  pref = split0_fsr(p,k)*alpha/pi/4

  split1_ac_fsr= pref*(3+2*zeta2+logmu+logmu**2)

  if(present(doub)) doub = pref*2
  if(present(sing)) sing = pref*(1+2*logmu)

  END FUNCTION

  FUNCTION SPLIT1_ISR(p,k,doub,sing)
  real(kind=prec) :: split1_isr
  real(kind=prec) :: p(4), k(4)
  real(kind=prec), optional :: doub, sing
  real(kind=prec) :: m2, x, spk, pref, logmu, logx, log1mx, logspk, lix, lispk
  real (kind=prec), parameter :: zeta2 = 1.6449340668482264365

  m2 = sq(p)
  spk = s(p,k)
  x = (p(4)-k(4))/p(4)

  logmu = log(musq/m2)
  logx = log(x)
  log1mx = log(1-x)
  logspk = log(m2/spk)
  lix = li2(x)
  lispk = li2(1-m2/spk)

  split1_isr = (4*logspk**2*(-(m2**2*(-1 + x)) + m2*spk*x))/(spk**3*x) + (8*log1mx*logx*(4*m2 +&
             & (spk*(1 + x**2))/((-1 + x)*x)))/spk**2 + (2*logmu**2*(-2*m2 + (spk + spk*x**2)/&
             &(x - x**2)))/spk**2 + logspk*((-4*m2**2*spk*(-3 + x) + 8*m2**3*(-1 + x) - 4*m2*s&
             &pk**2*(1 + 2*x))/((m2 - spk)**2*spk**2*x) + (8*logx*(4*m2 + (spk*(1 + x**2))/((-&
             &1 + x)*x)))/spk**2) + logmu*((8*logx*(2*m2 + (spk*(1 + x**2))/((-1 + x)*x)))/spk&
             &**2 + (2*(-2*m2 + (spk + spk*x**2)/(x - x**2)))/spk**2) + (2*(-4*lispk*m2*(m2*(-&
             &1 + x) - spk*x) + (spk*(-2*m2**2*(-2 + x + x**2) + spk**2*(3 + 2*x + x**2) + m2*&
             &spk*(-7 - 2*x + 3*x**2) + 4*lix*(m2 - spk)*(4*m2*(-1 + x)*x + spk*(1 + x**2))))/&
             &((m2 - spk)*(-1 + x))))/(spk**3*x) - (4*(2*m2**2*(-1 + x)**2 + 8*m2*spk*(-1 + x)&
             &*x + 3*spk**2*(1 + x**2))*zeta2)/(spk**3*(-1 + x)*x)
  split1_isr = alpha**2*split1_isr

  pref = alpha/pi/4*split0_isr(p,k)
  if(present(doub)) doub = pref*2
  if(present(sing)) sing = pref*(1+2*logmu-4*logx)

  END FUNCTION


  FUNCTION SPLIT1_FSR(p,k,doub,sing)
  real(kind=prec) :: split1_fsr
  real(kind=prec) :: p(4), k(4)
  real(kind=prec), optional :: doub, sing
  real(kind=prec) :: m2, z, spk, pref, logmu, logz, log1mz, logspk, liz, lispk, img
  real (kind=prec), parameter :: zeta2 = 1.6449340668482264365

  m2 = sq(p)
  spk = s(p,k)
  z = p(4)/(p(4)+k(4))

  logmu = log(musq/m2)
  logz = log(z)
  log1mz = log(1-z)
  logspk = log(m2/spk)
  liz = li2(z)
  lispk = li2(1+m2/spk,img)

  split1_fsr = (8*lispk*m2*(spk - m2*(-1 + z)))/spk**3 + (4*logspk**2*m2*(spk - m2*(-1 + z)))/s&
             &pk**3 - (8*liz*(4*m2*(-1 + z) + spk*(1 + z**2)))/(spk**2*(-1 + z)) - (8*log1mz*l&
             &ogz*(4*m2*(-1 + z) + spk*(1 + z**2)))/(spk**2*(-1 + z)) + (2*logmu**2*(-2*m2 - (&
             &spk*(1 + z**2))/(-1 + z)))/spk**2 + (4*logz**2*(4*m2 + (spk*(1 + z**2))/(-1 + z)&
             &))/spk**2 - (2*(spk**2*(1 + 2*z + 3*z**2) + m2**2*(-2 - 2*z + 4*z**2) + m2*spk*(&
             &-3 + 2*z + 7*z**2)))/(spk**2*(m2 + spk)*(-1 + z)) + logspk*((-4*m2*(2*m2**2*(-1 &
             &+ z) + spk**2*(2 + z) + m2*spk*(-1 + 3*z)))/(spk**2*(m2 + spk)**2) - (8*logz*(4*&
             &m2*(-1 + z) + spk*(1 + z**2)))/(spk**2*(-1 + z))) + logmu*((-8*logz*(2*m2*(-1 + &
             &z) + spk*(1 + z**2)))/(spk**2*(-1 + z)) + (2*(-2*m2 - (spk*(1 + z**2))/(-1 + z))&
             &)/spk**2) + (4*(2*m2*spk + 4*m2**2*(-1 + z) + (spk**2*(1 + z**2))/(-1 + z))*zeta&
             &2)/spk**3
  split1_fsr = alpha**2*split1_fsr

  pref = alpha/pi/4*split0_fsr(p,k)
  if(present(doub)) doub = pref*2
  if(present(sing)) sing = pref*(1+2*logmu+4*logz)

  END FUNCTION




!============== end of massive splitting functions  ==============!




  FUNCTION BRANCHCUT(x,y,in,pre)
  real(kind=prec) :: x,y,in,branchcut,prefactor
  real(kind=prec),optional :: pre

  prefactor = 1.0

  if(present(pre)) prefactor = pre

  if(x > y) then
    branchcut = in**2 - prefactor*Pi**2
  else
    branchcut = in**2
  end if

  END FUNCTION

  recursive FUNCTION LI2(x, img) result(di)

  !Optional argument img needed for x > 1.0

   real (kind=prec):: X,Y,T,S,A,PI3,PI6,ZERO,ONE,HALF,MALF,MONE,MTWO
   real (kind=prec):: C(0:18),H,ALFA,B0,B1,B2,LI2_OLD
   real (kind=prec):: di
   real (kind=prec), optional :: img
   integer :: i

   DATA ZERO /0.0_prec/, ONE /1.0_prec/
   DATA HALF /0.5_prec/, MALF /-0.5_prec/
   DATA MONE /-1.0_prec/, MTWO /-2.0_prec/
   DATA PI3 /3.289868133696453_prec/, PI6 /1.644934066848226_prec/

   DATA C( 0) / 0.4299669356081370_prec/
   DATA C( 1) / 0.4097598753307711_prec/
   DATA C( 2) /-0.0185884366501460_prec/
   DATA C( 3) / 0.0014575108406227_prec/
   DATA C( 4) /-0.0001430418444234_prec/
   DATA C( 5) / 0.0000158841554188_prec/
   DATA C( 6) /-0.0000019078495939_prec/
   DATA C( 7) / 0.0000002419518085_prec/
   DATA C( 8) /-0.0000000319334127_prec/
   DATA C( 9) / 0.0000000043454506_prec/
   DATA C(10) /-0.0000000006057848_prec/
   DATA C(11) / 0.0000000000861210_prec/
   DATA C(12) /-0.0000000000124433_prec/
   DATA C(13) / 0.0000000000018226_prec/
   DATA C(14) /-0.0000000000002701_prec/
   DATA C(15) / 0.0000000000000404_prec/
   DATA C(16) /-0.0000000000000061_prec/
   DATA C(17) / 0.0000000000000009_prec/
   DATA C(18) /-0.0000000000000001_prec/

   if(x > 1.00000000001_prec) then
      if (present(img)) then
      di = PI6 - log(x)*log(x-1) - LI2(1-x)
      img = -log(x)*pi
      return
      else
     call crash("LI2")
     endif
   elseif(x > 1.0_prec) then
     x = 1._prec
   endif

   ! This works well
   !IF(X > 0.99999999999_prec) THEN
   ! this keeps the error for x->1 well below 10^-13
   IF(1-X < 1e-14) THEN
    LI2_OLD=PI6
    di = Real(LI2_OLD,prec)
    RETURN
   ELSE IF(X .EQ. MONE) THEN
    LI2_OLD=MALF*PI6
    di = Real(LI2_OLD,prec)
    RETURN
   END IF
   T=-X
   IF(T .LE. MTWO) THEN
    Y=MONE/(ONE+T)
    S=ONE
    A=-PI3+HALF*(LOG(-T)**2-LOG(ONE+ONE/T)**2)
   ELSE IF(T .LT. MONE) THEN
    Y=MONE-T
    S=MONE
    A=LOG(-T)
    A=-PI6+A*(A+LOG(ONE+ONE/T))
   ELSE IF(T .LE. MALF) THEN
    Y=(MONE-T)/T
    S=ONE
    A=LOG(-T)
    A=-PI6+A*(MALF*A+LOG(ONE+T))
   ELSE IF(T .LT. ZERO) THEN
    Y=-T/(ONE+T)
    S=MONE
    A=HALF*LOG(ONE+T)**2
   ELSE IF(T .LE. ONE) THEN
    Y=T
    S=ONE
    A=ZERO
   ELSE
    Y=ONE/T
    S=MONE
    A=PI6+HALF*LOG(T)**2
   END IF

   H=Y+Y-ONE
   ALFA=H+H
   B1=ZERO
   B2=ZERO
   DO  I = 18,0,-1
     B0=C(I)+ALFA*B1-B2
     B2=B1
     B1=B0
   ENDDO
   LI2_OLD=-(S*(B0-H*B2)+A)
         ! Artificial conversion
   di = Real(LI2_OLD,prec)

  END FUNCTION LI2

  FUNCTION LI3(x)
   ! This was hacked from LI2 to also follow C332
   !! Trilogarithm for arguments x < = 1.0
   ! In theory this could also produce Re[Li [x]] for x>1
   !                                        3

   real (kind=prec):: X,S,A
   real (kind=prec):: CA(0:18),HA,ALFAA,BA0,BA1,BA2, YA
   real (kind=prec):: CB(0:18),HB,ALFAB,BB0,BB1,BB2, YB
   DATA CA(0) / 0.4617293928601208/
   DATA CA(1) / 0.4501739958855029/
   DATA CA(2) / -0.010912841952292843/
   DATA CA(3) / 0.0005932454712725702/
   DATA CA(4) / -0.00004479593219266303/
   DATA CA(5) / 4.051545785869334e-6/
   DATA CA(6) / -4.1095398602619446e-7/
   DATA CA(7) / 4.513178777974119e-8/
   DATA CA(8) / -5.254661564861129e-9/
   DATA CA(9) / 6.398255691618666e-10/
   DATA CA(10) / -8.071938105510391e-11/
   DATA CA(11) / 1.0480864927082917e-11/
   DATA CA(12) / -1.3936328400075057e-12/
   DATA CA(13) / 1.8919788723690422e-13/
   DATA CA(14) / -2.6097139622039465e-14/
   DATA CA(15) / 3.774985548158685e-15/
   DATA CA(16) / -5.671361978114946e-16/
   DATA CA(17) / 1.1023848202712794e-16/
   DATA CA(18) / -5.0940525990875006e-17/
   DATA CB(0) / -0.016016180449195803/
   DATA CB(1) / -0.5036424400753012/
   DATA CB(2) / -0.016150992430500253/
   DATA CB(3) / -0.0012440242104245127/
   DATA CB(4) / -0.00013757218124463538/
   DATA CB(5) / -0.000018563818526041144/
   DATA CB(6) / -2.841735345177361e-6/
   DATA CB(7) / -4.7459967908588557e-7/
   DATA CB(8) / -8.448038544563037e-8/
   DATA CB(9) / -1.5787671270014e-8/
   DATA CB(10) / -3.0657620579122164e-9/
   DATA CB(11) / -6.140791949281482e-10/
   DATA CB(12) / -1.2618831590198e-10/
   DATA CB(13) / -2.64931268635803e-11/
   DATA CB(14) / -5.664711482422879e-12/
   DATA CB(15) / -1.2303909436235178e-12/
   DATA CB(16) / -2.7089360852246495e-13/
   DATA CB(17) / -6.024075373994343e-14/
   DATA CB(18) / -1.2894320641440237e-14/
   real (kind=prec):: Li3
   real (kind=prec), parameter :: zeta2 = 1.6449340668482264365
   real (kind=prec), parameter :: zeta3 = 1.2020569031595942854
   integer :: i


   if(x > 1.00000000001_prec) then
     call crash("LI3")
   elseif(x > 1.0_prec) then
     x = 1._prec
   endif

   IF(X > 0.999999_prec) THEN
    LI3=zeta3
    RETURN
   ELSE IF(X .EQ. -1._prec) THEN
    LI3=-0.75_prec*zeta3
    RETURN
   END IF
   IF(X .LE. -1._prec) THEN
    YA=1._prec/x ; YB=0._prec
    S=-1._prec
    A=-LOG(-X)*(zeta2+LOG(-x)**2/6._prec)
   ELSE IF(X .LE. 0._prec) THEN
    YA=x ; YB=0._prec
    S=-1._prec
    A=0._prec
   ELSE IF(X .LE. 0.5_prec) THEN
    YA=0._prec ; YB=x
    S=-1._prec
    A=0._prec
   ELSE IF(X .LE. 1._prec) THEN
    YA=(x-1._prec)/x ; YB=1._prec-x
    S=1._prec
    A=zeta3 + zeta2*Log(x) - (Log(1._prec - X)*Log(X)**2)/2._prec + Log(X)**3/6._prec
   ELSE IF(X .LE. 2._prec) THEN
    YA=1._prec - X ; YB=(X-1._prec)/X
    S=1._prec
    A=zeta3 + zeta2*Log(x) - (Log(X - 1._prec)*Log(X)**2)/2._prec + Log(X)**3/6._prec
   ELSE
    YA=0._prec ; YB=1._prec/X
    S=-1._prec
    A=2*zeta2*Log(x)-Log(x)**3/6._prec
   END IF


   HA=-2._prec*YA-1._prec ; HB= 2._prec*YB
   ALFAA=HA+HA ; ALFAB = HB+HB

   BA0 = 0. ; BA1=0. ; BA2=0.
   BB0 = 0. ; BB1=0. ; BB2=0.
   DO  I = 18,0,-1
     BA0=CA(I)+ALFAA*BA1-BA2 ; BA2=BA1 ; BA1=BA0
     BB0=CB(I)+ALFAB*BB1-BB2 ; BB2=BB1 ; BB1=BB0
   ENDDO
   Li3 = A + S * (  (BA0 - HA*BA2) + (BB0 - HB*BB2) )

  END FUNCTION LI3




  FUNCTION LN(arg1, arg2)

    !! log(sij/mm) for positive and negative sij

  complex (kind=prec) :: Ln
  real(kind=prec), intent(in) :: arg1, arg2
  real(kind=prec) :: abs_part

  abs_part = 1._prec

  if(arg1 > 0._prec) then
    ln = log(arg1/arg2)
  elseif(arg1 < 0._prec ) then
    ln = log(-arg1/arg2) - imag*abs_part*pi
  else
    call crash("LNc1")
  endif

  if(arg2 < 0._prec) call crash("LN")

  END FUNCTION LN



  FUNCTION DILOG(arg1,arg2)

    !! dilogarithm of 1 - arg1/arg2 for pos and neg arg2

  complex (kind=prec) :: Dilog
  real(kind=prec), intent(in) :: arg1,arg2
  real(kind=prec) :: abs_part

  abs_part = 1._prec

  if(arg1/arg2 > 0._prec) then
    dilog = li2(1._prec - arg1/arg2)
  elseif(arg1 > 0._prec .and. arg2 < 0._prec) then
    dilog = pi**2/6 - li2(arg1/arg2)                             &
        + (log(-arg2/arg1) - imag*abs_part*pi)*log(1._prec - arg1/arg2)
  else
   print*, "arg1, arg2: ",arg1,arg2
   call crash("DILOG")
  endif

  END FUNCTION DILOG


  FUNCTION CL2(x)
  !!! Clausen function: Cl2(x) = Im[LI2(exp(i*phi))]
  !!! allowed input values: [0,2*pi)
  real(kind=prec) :: cl2
  real(kind=prec), intent(in) :: x

  if(x<0 .or. x>2*pi) call crash("CL2")

  if(abs(x)<zero .or. abs(x-2*pi)<zero) then
    cl2 = 0._prec
    return
  end if

  if(0<=x .and. x<=pi) then
    cl2 = cl2_part(x)
  else if(pi<x .and. x<=2*pi) then
    cl2 = -cl2_part(2*pi-x)
  end if
  END FUNCTION


  FUNCTION CL2_PART(x)
  !!! only input values: [0,pi]
  real(kind=prec) :: cl2_part
  real(kind=prec), intent(in) :: x

  If(0<=x .and. x<=2*pi/3) then
    cl2_part = x + 0.013888888888888888*x**3 + 0.00006944444444444444*x**5 + 7.873519778281683e&
      &-7*x**7 + 1.1482216343327455e-8*x**9 + 1.8978869988971e-10*x**11 + 3.38730137095&
      &3521e-12*x**13 + 6.372636443183181e-14*x**15 + 1.2462059912950672e-15*x**17 + 2.&
      &5105444608999545e-17*x**19 - 1.*x*log(x)
  else if (2*pi/3<x .and. x<=pi) then
    cl2_part = 2.177586090303602 + 0.041666666666666664*(-3.141592653589793 + x)**3 + 0.0010416&
      &666666666667*(-3.141592653589793 + x)**5 + 0.0000496031746031746*(-3.14159265358&
      &9793 + x)**7 + 2.927965167548501e-6*(-3.141592653589793 + x)**9 + 1.941538399871&
      &733e-7*(-3.141592653589793 + x)**11 + 1.3870999114054669e-8*(-3.141592653589793 &
      &+ x)**13 + 1.0440290284867003e-9*(-3.141592653589793 + x)**15 + 8.16701096395222&
      &4e-11*(-3.141592653589793 + x)**17 + 6.5812165661369675e-12*(-3.141592653589793 &
      &+ x)**19 - 0.6931471805599453*x
  else
    call crash("CL2_PART")
  end if
  END FUNCTION


  FUNCTION CLENSHAW(a_cheb, xmin, xmax, x, n)
  real (kind=prec) :: clenshaw
  integer :: n
  real (kind=prec), intent(in) :: a_cheb(n), xmin, xmax, x
  real (kind=prec) :: d(n), xnorm
  integer :: j

  xnorm = (-2*x+xmin+xmax)/(xmin-xmax)
  d = a_cheb
  do j = n, 3, -1
    d(j-1) = d(j-1) + 2*xnorm*d(j)
    d(j-2) = d(j-2) - d(j)
  end do
  clenshaw = d(1)+xnorm*d(2)

  END FUNCTION CLENSHAW




!======================    soft integrals    ======================!


  FUNCTION ISIN_00(xicut,epcmf,k1,k2) ! both massless
  real (kind=prec), intent(in) :: epcmf,xicut
  type(mlm), intent(in) :: k1,k2
  real (kind=prec) isin_00, q1(4), q2(4)
  q1 = k1%momentum
  q2 = k2%momentum

  isin_00 = -(log(s(k1,k2)/musq)-log(4*q1(4)*q2(4)/xicut**2/epcmf**2))
  END FUNCTION ISIN_00

  FUNCTION ISIN_M0(xicut,epcmf,q1,k2)  ! first massive second massless
  real (kind=prec), intent(in) :: epcmf,xicut,q1(4)
  type(mlm), intent(in) :: k2
  real (kind=prec) isin_m0,m12,q2(4)
  m12 = sq(q1)
  q2 = k2%momentum
  isin_m0 = -(log(s(q1,k2)/musq)-0.5*log(4.*m12**2*q2(4)**2/xicut**2/epcmf**2/musq))
  END FUNCTION ISIN_M0
  FUNCTION ISIN_0M(xicut,epcmf,k1,q2)
           ! first massless second massive, "same" as M0
  real (kind=prec), intent(in) :: epcmf,xicut,q2(4)
  type(mlm), intent(in) :: k1
  real (kind=prec) isin_0m,m12,q1(4)
  m12 = sq(q1)
  q1 = k1%momentum
  isin_0m = -(log(s(q2,k1)/musq)-0.5*log(4.*m12**2*q1(4)**2/xicut**2/epcmf**2/musq))
  END FUNCTION ISIN_0M

  FUNCTION ISIN_SELF(xicut,epcmf,q1)  ! massive self-eikonal
  real (kind=prec), intent(in) :: q1(4),epcmf,xicut
  real (kind=prec) :: isin_self

  isin_self = -1

  END FUNCTION ISIN_SELF


  FUNCTION ISIN_MM(xicut,epcmf,qk,ql)  ! both massive eq(A.11) of FFMS

  real (kind=prec), intent(in) :: qk(4),ql(4),epcmf,xicut
  real (kind=prec) :: isin_mm, vkl
  real (kind=prec) :: KdotL, mk2, ml2, logV, z


  mk2 = sq(qk)
  ml2 = sq(ql)
  KdotL = 0.5_prec*s(ql,qk)
  vkl = sqrt(1. - mk2*ml2/KdotL**2)

  if(abs(1._prec-vkl)<1E-8) then
    z = sqrt(abs(mk2*ml2))/abs(KdotL)
    logV = -2*log(z/2)-z**2/2-3*z**4/16
    isin_mm = -0.5*logV/vkl
  else if(vkl > 1.0E-4) then
    isin_mm = - 0.5_prec/vkl*log((1.+vkl)/(1.-vkl))
  else
    isin_mm = - 0.5* (2 + (2*vkl**2)/3. + (2*vkl**4)/5. + (2*vkl**6)/7. )
  endif

  END FUNCTION ISIN_MM


  FUNCTION IREG_00(xicut,epcmf,k1,k2) ! both massless

  real (kind=prec), intent(in) :: epcmf,xicut
  type(mlm), intent(in) :: k1,k2
  real (kind=prec) :: ireg_00, e1, e2, arg1, arg2, arg24
  real (kind=prec) :: log1, log2, log24, twologtwo, q1(4), q2(4)

  q1 = k1%momentum
  q2 = k2%momentum

  e1 = q1(4); e2 = q2(4)
  arg1 = xicut**2*epcmf**2/musq
  arg2 = s(k1,k2)/e1/e2
  arg24 = arg2/4._prec
  log1 = log(arg1)
  log2 = log(arg2)
  log24 = log(arg24)
  twologtwo = 0.960906027836403_prec    ! 2 (log 2)^2

  if(arg2 > 4.000001_prec) call crash("IREG")
  if(arg2 > 3.999999_prec) arg2 = 3.999999_prec

  ireg_00 = 0.5_prec*log1**2 + log1*log24 - li2(arg24)          &
          + 0.5_prec*log2**2 - log(4._prec - arg2)*log24        &
          - twologtwo

  select case(cgamma)
  case("exp")
    ireg_00 = ireg_00 - pi**2/12.
  case("gam")
    ireg_00 = ireg_00 - 0. ! do nothing
  case default
    call crash("IREG_00")
  end select

    ! NOTE  -pi^2/12 term to change convention in prefactor
    ! Note that the overall factor 1/8/pi**2 is NOT included
    ! since the psi_mn have also a different normalization

  END FUNCTION IREG_00


  FUNCTION IREG_M0(xicut,epcmf,q1,k2)  ! first massive second massless

  real (kind=prec), intent(in) :: q1(4),epcmf,xicut
  type(mlm), intent(in) :: k2
  real (kind=prec) :: ireg_m0, ml, ek, el, bl, q2(4)
  real (kind=prec) :: logX, logSQ, logKK, KdotL

  logX = log(xicut)
  logSQ = log(epcmf**2/musq)

  q2 = k2%momentum
  ek = q2(4); el = q1(4)
  KdotL = 0.5_prec*s(q1,k2)
  ml = sqrt(0.5_prec*s(q1,q1))
  bl = sqrt(q1(1)**2+q1(2)**2+q1(3)**2)/el
!  bl = sqrt(1._prec - (ml/el)**2)

  logKK = log(KdotL/ml/ek)
  ireg_m0 = logX*(logX+logSQ+2.*logKK) + 0.25*logSQ**2              &
          - 0.25*(log((1.+bl)/(1.-bl)))**2                          &
          + 0.5*(log(KdotL/(1.-bl)/el/ek))**2  + logSQ*logKK        &
          - li2(1. - (1.+bl)*ek*el/KdotL)                           &
          + li2(1. - KdotL/(1.-bl)/ek/el)

  select case(cgamma)
  case("exp")
    ireg_m0 = ireg_m0 - pi**2/8.
  case("gam")
    ireg_m0 = ireg_m0 - pi**2/12.
  case default
    call crash("IREG_M0")
  end select

    ! NOTE -pi^2/12 -> -pi^2/12 -pi^2/24 = -pi^2/8
    ! to change convention in prefactor

  END FUNCTION IREG_M0



  FUNCTION IREG_0M(xicut,epcmf,k1,q2)
           ! first massless second massive, "same" as M0

  real (kind=prec), intent(in) :: q2(4),epcmf,xicut
  type(mlm), intent(in) :: k1
  real (kind=prec) :: ireg_0m, ml, ek, el, bl
  real (kind=prec) :: logX, logSQ, logKK, KdotL

  logX = log(xicut)
  logSQ = log(epcmf**2/musq)

  ek = k1%momentum(4); el = q2(4)
  KdotL = 0.5_prec*s(k1,q2)
  ml = sqrt(0.5_prec*s(q2,q2))
  bl = sqrt(q2(1)**2+q2(2)**2+q2(3)**2)/el
!  bl = sqrt(1._prec - (ml/el)**2)

  logKK = log(KdotL/ml/ek)
  ireg_0m = logX*(logX+logSQ+2.*logKK)  + 0.25*logSQ**2             &
          - 0.25*(log((1.+bl)/(1.-bl)))**2                          &
          + 0.5*(log(KdotL/(1.-bl)/el/ek))**2  + logSQ*logKK        &
          - li2(1. - (1.+bl)*ek*el/KdotL)                           &
          + li2(1. - KdotL/(1.-bl)/ek/el)

  select case(cgamma)
  case("exp")
    ireg_0m = ireg_0m - pi**2/8.
  case("gam")
    ireg_0m = ireg_0m - pi**2/12.
  case default
    call crash("IREG_0M")
  end select

    ! NOTE -pi^2/12 -> -pi^2/12 -pi^2/24 = -pi^2/8
    ! to change convention in prefactor

  END FUNCTION IREG_0M



  FUNCTION IREG_SELF(xicut,epcmf,q1)  ! massive self-eikonal

  real (kind=prec), intent(in) :: q1(4),epcmf,xicut
  real (kind=prec) :: ireg_self, el, bl
  real (kind=prec) :: logX, logSQ

  logX = log(xicut)
  logSQ = log(epcmf**2/musq)

  el = q1(4)
  bl = sqrt(q1(1)**2+q1(2)**2+q1(3)**2)/el

  if(bl > 1.0E-4) then
     ireg_self = 2.*logX + logSQ - 1._prec/bl*log((1.+bl)/(1.-bl))
  else
     ireg_self = 2.*logX + logSQ - (2 + (2*bl**2)/3. + (2*bl**4)/5. + (2*bl**6)/7. )
  endif

  END FUNCTION IREG_SELF




  FUNCTION IREG_MM(xicut,epcmf,qk,ql)  ! both massive eq(A.12) of FFMS

  real (kind=prec), intent(in) :: qk(4),ql(4),epcmf,xicut
  real (kind=prec) :: ireg_mm, vkl, akl, bk, bl, nu, lam_nu_2, z
  real (kind=prec) :: KdotL, mk2, ml2, ek, el, LogV, LogX

  ek = qk(4)
  el = ql(4)
  bk = sqrt(qk(1)**2+qk(2)**2+qk(3)**2)/ek
  bl = sqrt(ql(1)**2+ql(2)**2+ql(3)**2)/el

  mk2 = sq(qk)
  ml2 = sq(ql)
  KdotL = 0.5_prec*s(ql,qk)
  vkl = sqrt(1. - mk2*ml2/KdotL**2)
  akl = (1.+vkl)*KdotL/mk2
  lam_nu_2 = akl**2*mk2 - ml2  ! 2 \lambda \nu
  nu = 0.5_prec*lam_nu_2/(akl*ek - el)
!  lam = akl*ek - el
!  nu = 0.5_prec*(akl**2*mk2 - ml2)/lam

  logX = log((xicut*epcmf)**2/musq)

  if(abs(1._prec-vkl)<1E-8) then
      z = sqrt(abs(mk2*ml2))/abs(KdotL)
      logV = -2*log(z/2)-z**2/2-3*z**4/16
  else
      logV = log((1.+vkl)/(1.-vkl))
  endif

  ireg_mm = jaux(akl*ek, akl*ek*bk, nu, lam_nu_2)   &
          - jaux(el, el*bl, nu, lam_nu_2)
  ireg_mm = 0.5*(1.+vkl)*KdotL**2*ireg_mm/mk2
  ireg_mm = ireg_mm + 0.5*logV*logX/vkl

  END FUNCTION IREG_MM


  FUNCTION JAUX(xx,yy,nu,lam_nu_2)

  real(kind=prec) :: jaux, xx, yy, nu, lam_nu_2

  jaux = (log((xx-yy)/(xx+yy)))**2        &
       + 4*Li2(1._prec - (xx+yy)/nu)      &
       + 4*Li2(1._prec - (xx-yy)/nu)

  jaux = jaux/lam_nu_2

  END FUNCTION JAUX



  FUNCTION ILIN_SELF(xicut,epcmf,q1)  ! massive self-eikonal
  real (kind=prec), intent(in) :: q1(4),epcmf,xicut
  real (kind=prec) :: ilin_self, el, bl, x
  real (kind=prec) :: logX, logSQ, logC, log0, log10

  logX = log(xicut)
  logSQ = log(epcmf**2/musq)

  el = q1(4)
  bl = sqrt(q1(1)**2+q1(2)**2+q1(3)**2)/el
  logC = logSQ+2*logX

  if(bl > 1.0E-4) then
    x = (1-bl) / (1+bl)
    log0 = log(x)
    log10 = -log0*log(1.-x) - Li2(x)

    ilin_self = -(3*log0**2+12*log10+6*log0*logC+2*pi**2+3*logC**2*bl)/6./bl

  else
    ilin_self = - 4. - 0.5_prec*logC**2 + 2*logC*(1._prec + bl**2/3. + bl**4/5. + bl**6/7.)
  endif

  END FUNCTION ILIN_SELF

  FUNCTION ILIN_MM(xicut,epcmf,qk,ql)  ! both massive eq(A.12) of FFMS
  real (kind=prec), intent(in) :: qk(4),ql(4),epcmf,xicut
  real (kind=prec) :: ilin_mm, bl, mk2, ml2,x, log0, log10, logC,logStuff


  if (abs(qk(1))+abs(qk(2))+abs(qk(3)).gt.zero) call crash("ILIN_MM only for CMS of qk yet!")

  mk2 = sq(qk)
  ml2 = sq(ql)
  bl = sqrt(1. - mk2*ml2/(0.5*s(qk,ql))**2)

  logC = log(epcmf**2/musq) + 2*log(xicut)

  x = (1-bl) / (1+bl)
  log0 = log(x)
  log10 = -log0*log(1.-x) - Li2(x)

  logStuff = -0.5 * log0**2*log(1.-x) - Li3(x) - 2*Li3(1-x) + z3

  ilin_mm = (log0**3 + 3*log0**2*logC + 12*log10*logC + 3*log0*logC**2 +  12*logStuff + log0*Pi**2 + 2*logC*Pi**2)/(12.*bl)


  END FUNCTION ILIN_MM



  FUNCTION IEIKMM(xicut, epcmf, q1, q2, pole)
  implicit none
  real(kind=prec) :: xicut, epcmf, ieikmm
  real(kind=prec) :: q1(4), q2(4)
  real(kind=prec), optional, intent(out) :: pole

  ieikmm = 2*Ireg(xicut, Epcmf, q1, q2) - Ireg(xicut, Epcmf, q1) - Ireg(xicut, Epcmf, q2)

  if (present(pole)) then
    pole = 2*Isin(xicut, Epcmf, q1, q2) - Isin(xicut, Epcmf, q1) - Isin(xicut, Epcmf, q2)
  endif
  END FUNCTION IEIKMM


  FUNCTION IEIK_PART(xicut, epcmf, pp, pole)
  type(particles), intent(in) :: pp
  real(kind=prec), intent(in) :: xicut, epcmf
  real (kind=prec) :: Ieik_part
  real(kind=prec), optional, intent(out) :: pole
  integer i, j, sgn

  if(present(pole)) pole = 0.
  ieik_part = 0.
  do i=1,pp%n
    if(pp%vec(i)%charge == 0) cycle
    if(pp%combo.ne."x") then
      ieik_part = ieik_part - Ireg(xicut, Epcmf, pp%vec(i)%momentum)
      if(present(pole)) pole = pole - (-1) !pole of integrated self eikonal
    endif
    do j=i+1,pp%n
      if(pp%vec(j)%charge == 0) cycle
      if ((pp%combo.ne."*").and.(comboeik(pp%vec(i),pp%vec(j)).ne.pp%combo)) cycle
      if(pp%vec(i)%effcharge.eq.pp%vec(j)%effcharge) then
        sgn = -1
      else
        sgn = +1
      endif
      ieik_part = ieik_part + sgn * 2*Ireg(xicut, Epcmf, pp%vec(i)%momentum, pp%vec(j)%momentum)
      if(present(pole)) then
        pole = pole + sgn * 2*Isin(xicut, Epcmf, pp%vec(i)%momentum, pp%vec(j)%momentum)
      endif
    enddo
  enddo

  END FUNCTION IEIK_PART


  FUNCTION SOFTFUNC(part1, part2, k, pole)
  type(particle), intent(in) :: part1, part2
  real(kind=prec), intent(in) :: k(4)
  real (kind=prec) :: softFunc
  real(kind=prec), optional, intent(out) :: pole

  real(kind=prec) :: I1(-1:0), I2(-1:0), ans(-1:0)
  real(kind=prec) :: p1(4), p2(4), m12, m22
  real(kind=prec) :: s12, s1k
  real(kind=prec) :: v, omv, logC
  real(kind=prec) :: arg

  p1 = part1%momentum
  p2 = part2%momentum
  if (.not. part1%incoming) p1 = -p1
  if (.not. part2%incoming) p2 = -p2

  m12 = sq(p1) ; m22 = sq(p2)
  s12 = s(p1, p2) ; s1k = s(p1, k)

  arg = s12**2 / m12 / m22 / 4.
  if(arg > 1e5) then
    v =  1 - 1./(2*arg) - 1./(8*arg**2) - 1./(16*arg**3) &
           - 5./(128*arg**4) - 7./(256*arg**5)
    omv =  + 1./(2*arg) + 1./(8*arg**2) + 1./(16*arg**3) &
           + 5./(128*arg**4) + 7./(256*arg**5)
  else
    v = sqrt(1-1/arg)
    omv = 1-v
  endif

  I1 = -s1k / m12 * (/ 1._prec, log(m12*musq/s1k**2) + 2 /)

  logC = log(omv/(1+v))

  I2 = 2 / s12 / v * (/ -logC, pi**2/3 - 0.5*logC**2 &
    - logC * (2*log(v) + log(musq * abs(s12)**2 / m22 / s1k**2)) &
    - 2*Li2(omv/(1+v)) /)

  if(s12 > 0) then
    I2 = I2 - sign((/ 0., 4*pi**2/s12 / v /), s1k)
    ! We drop the imaginary part
    ! I2 = I2 - 4*pi*imag / s12 / v * &
    !    (/ 1, log(musq*v**2**abs(sij)**2/m22/s1k**2) /)
  endif

  ans = m12 / ((0.5*s(p1,p2))**2 - m12*m22) * s(p2,k)/s(p1,k) * &
    0.5*( s(p1,p2) * I1 + m22 * s(p1,k) * I2)

  if(present(pole)) pole = (-1)*ans(-1)
  softfunc = (-1)*ans(0)
  END FUNCTION SOFTFUNC


  FUNCTION NTSSOFT(pp, kk, pole)
  type(particles), intent(in) :: pp
  real(kind=prec), intent(in) :: kk(4)
  real(kind=prec) :: ntssoft, softfin, softsin
  real(kind=prec), optional, intent(out) :: pole
  integer i, j, k

  ntssoft = 0.
  if(present(pole)) pole = 0.
  do i=1,pp%n
    do j=1,pp%n
      do k=1,pp%n
        if(i.eq.j) cycle
        if((pp%combo.ne."*").and.(combonts(pp%vec(i),pp%vec(j),pp%vec(k),mx=0).ne.pp%combo)) cycle
        softfin = softfunc(pp%vec(i), pp%vec(j), kk, softsin)
        ntssoft = ntssoft &
          + pp%vec(k)%effcharge * pp%vec(j)%effcharge * pp%vec(i)%effcharge**2 &
             * ( s(pp%vec(i), pp%vec(k)) /s(kk, pp%vec(i))/s(kk, pp%vec(k))  &
                -s(pp%vec(j), pp%vec(k)) /s(kk, pp%vec(j))/s(kk, pp%vec(k))) &
             * 4 * softfin
        if(present(pole)) pole = pole &
          + pp%vec(k)%effcharge * pp%vec(j)%effcharge * pp%vec(i)%effcharge**2 &
             * ( s(pp%vec(i), pp%vec(k)) /s(kk, pp%vec(i))/s(kk, pp%vec(k))  &
                -s(pp%vec(j), pp%vec(k)) /s(kk, pp%vec(j))/s(kk, pp%vec(k))) &
             * 4 * softsin
      enddo
    enddo
  enddo

  END FUNCTION NTSSOFT

  FUNCTION NTSSOFTx(pp, kk, pole)
  type(particles), intent(in) :: pp
  real(kind=prec), intent(in) :: kk(4)
  real(kind=prec) :: ntssoftx, softfin, softsin
  real(kind=prec), optional, intent(out) :: pole
  integer i, j, k

  ntssoftx = 0.
  if(present(pole)) pole = 0.
  do i=1,pp%n
    do j=1,pp%n
      do k=1,pp%n
        if(i.eq.j) cycle
        if((pp%combo.ne."*").and.(combonts(pp%vec(i),pp%vec(j),pp%vec(k),mx=1).ne.pp%combo)) cycle
        softfin = softfunc(pp%vec(i), pp%vec(j), kk, softsin)
        ntssoftx = ntssoftx &
          + pp%vec(k)%effcharge * pp%vec(j)%effcharge * pp%vec(i)%effcharge**2 &
             * ( s(pp%vec(i), pp%vec(k)) /s(kk, pp%vec(i))/s(kk, pp%vec(k))  &
                -s(pp%vec(j), pp%vec(k)) /s(kk, pp%vec(j))/s(kk, pp%vec(k))) &
             * 4 * softfin
        pole = pole &
          + pp%vec(k)%effcharge * pp%vec(j)%effcharge * pp%vec(i)%effcharge**2 &
             * ( s(pp%vec(i), pp%vec(k)) /s(kk, pp%vec(i))/s(kk, pp%vec(k))  &
                -s(pp%vec(j), pp%vec(k)) /s(kk, pp%vec(j))/s(kk, pp%vec(k))) &
             * 4 * softsin
      enddo
    enddo
  enddo

  END FUNCTION NTSSOFTx

!=====================  end soft integrals ====================!


!===================== running of effective em coupling ====================!


  !!! WARNING: Only for space-like momenta q2 < 0 !!!

  !The effective value of the fine structure constant alphaQED at energy
  !E is alphaQED(E)=alphaQED(0)/(1-deltalph)


  !!! leptonic !!!

  !! 1-loop

  FUNCTION DELTALPH_L_0(q2,ml,img)
  real (kind=prec), intent(in) :: q2, ml
  real (kind=prec), optional :: img
  real (kind=prec) :: deltalph_l_0
  real (kind=prec) :: z

  z = q2/ml**2

  if(abs(z) > 1e10) then
    deltalph_l_0 = alpha*(-5 + 3*log(abs(z)))/9/pi
    return
  endif

  if(abs(z)<1E-1) then
    deltalph_l_0 = -alpha/pi*z*(46558512 + 4988412*z + 739024*z**2 + 125970*z**3&
                 + 23256*z**4 + 4522*z**5 + 912*z**6 + 189*z**7)/6.9837768e8
    if(present(img)) img = 0._prec
    return
  end if

  if(present(img)) then
    deltalph_l_0 = -alpha*(12+5*z+3*(2+z)*discbsmm_full(q2,ml,img))/(9*pi*z)
    img = -alpha*3*(2+z)*img/(9*pi*z)
  else
    deltalph_l_0 = -alpha*(12+5*z+3*(2+z)*discbsmm_full(q2,ml))/(9*pi*z)
  end if

  END FUNCTION DELTALPH_L_0

  FUNCTION DELTALPH_L_0_CPLX(q2,ml)
  real (kind=prec), intent(in) :: q2, ml
  complex (kind=prec) :: deltalph_l_0_cplx
  real (kind=prec) :: deltalph_re, deltalph_im

  deltalph_re = deltalph_l_0(q2,ml,deltalph_im)

  deltalph_l_0_cplx = deltalph_re + imag*deltalph_im

  END FUNCTION DELTALPH_L_0_CPLX


  !! 2-loop

  FUNCTION DELTALPH_F(x)
  real (kind=prec), intent(in) :: x
  real (kind=prec) :: deltalph_f

  deltalph_f = 6*Li3(x)-4*Li2(x)*Log(abs(x))-Log(abs(x))**2*Log(1-x)

  if(x<0) then
    deltalph_f = deltalph_f + pi**2*Log(1-x)
  end if
  END FUNCTION DELTALPH_F

  FUNCTION DELTALPH_G(x)
  real (kind=prec), intent(in) :: x
  real (kind=prec) :: deltalph_g

  deltalph_g = 2*Li2(x)+2*Log(abs(x))*Log(1-x)+x*Log(abs(x))**2/(1-x)

  if(x<0) then
    deltalph_g = deltalph_g - pi**2*x/(1-x)
  end if
  END FUNCTION DELTALPH_G


  FUNCTION DELTALPH_L_1(q2,ml,img)
  real (kind=prec), intent(in) :: q2, ml
  real (kind=prec), optional :: img
  real (kind=prec) :: deltalph_l_1, x, z, beta, img_j, img_dj
  real (kind=prec), parameter :: zeta3 = 1.2020569031595942854

  z = q2/ml**2
  if(abs(z)<1E-1) then
    deltalph_l_1 = -alpha**2/pi**2*((41*z)/162. + (449*z**2)/10800. + (62479*z**3)/7.938e6&
                 + (25993*z**4)/1.63296e7 + (6756019*z**5)/2.0170458e10&
                 + (338452951*z**6)/4.6749358656e12)
    return
  end if

  beta = sqrt(1-4/z)
  x = 2/(2-z-beta*z)
  deltalph_l_1 = 52*z + 5*z**2 - 4*(-4 + z**2)*(6*zeta3 - 2*deltalph_f(x)&
               + deltalph_f(x**2)) + 8*beta*z*(2 + z)*(deltalph_g(x) - deltalph_g(x**2))&
               + 6*beta*z*(6 + z)*log(abs(x)) + 4*(-1 + 4*z)*log(abs(x))**2
  if(x<0) then
    deltalph_l_1 = deltalph_l_1 - pi**2*4*(-1 + 4*z)
  end if

  deltalph_l_1 = -alpha**2*deltalph_l_1/24/pi**2/z**2
  deltalph_l_1 = deltalph_l_1

  if(present(img)) then
    img = 0._prec
    if(q2>4*ml**2) then
      img_j = -4*(2*Li2(x**2)-2*Li2(x)+2*log(abs(x))*log(1-x**2)-log(abs(x))*log(1-x))
      img_dj = 2*beta*(2*log(1-x**2)-log(1-x)+x*(3*x-1)/(1-x**2)*log(abs(x)))
      img = 2*img_j*(4-z**2)-4*img_dj*z*(2+z)+3*z*(6+z)*beta-4*(1-4*z)*log(abs(x))
      img = -img/12/pi/z**2
    end if
  end if
  END FUNCTION DELTALPH_L_1


  FUNCTION DELTALPH_L_1_CPLX(q2,ml)
  real (kind=prec), intent(in) :: q2, ml
  complex (kind=prec) :: deltalph_l_1_cplx
  real (kind=prec) :: deltalph_re, deltalph_im

  deltalph_re = deltalph_l_1(q2,ml,deltalph_im)

  deltalph_l_1_cplx = deltalph_re + imag*deltalph_im

  END FUNCTION DELTALPH_L_1_CPLX


  FUNCTION DELTALPH_H(q2)
  real (kind=prec), intent(in) :: q2
  real (kind=prec) :: deltalph_h
  real (kind=prec) :: e,der,errdersta,errdersys, deg,errdegsta,errdegsys

  e = sign(0.001*sqrt(abs(q2)),q2)
  call hadr5x(e,sw2,der,errdersta,errdersys,deg,errdegsta,errdegsys)
  deltalph_h = alpha*137.035999_prec*der

  END FUNCTION DELTALPH_H


  FUNCTION DELTALPH_H_CPLX(q2)
  real (kind=prec), intent(in) :: q2
  complex (kind=prec) :: deltalph_h_cplx
  real (kind=prec) :: e
  complex (kind=prec) :: der,errdersta,errdersys,deg,errdegsta,errdegsys

  e = sign(0.001*sqrt(abs(q2)),q2)
  call chadr5x(e,sw2,der,errdersta,errdersys,deg,errdegsta,errdegsys)
  ! LEPTONflag = 'had'
  ! der = gggvapx(1e-6*q2, cerror, cerrorsta, cerrorsys)
  deltalph_h_cplx = alpha*137.035999_prec*der

  END FUNCTION DELTALPH_H_CPLX


  FUNCTION DELTALPH_0_TOT(q2)
  real (kind=prec), intent(in) :: q2
  real (kind=prec) :: deltalph_0_tot
  real (kind=prec), parameter :: alpha_true = 1/137.035999084_prec / alpha
  logical, save :: warning

  deltalph_0_tot = 0._prec

  if(nel==1) deltalph_0_tot = deltalph_0_tot + deltalph_l_0(q2,mel)
  if(nmu==1) deltalph_0_tot = deltalph_0_tot + deltalph_l_0(q2,mmu)
  if(ntau==1) deltalph_0_tot = deltalph_0_tot + deltalph_l_0(q2,mtau)

  if((.not.warning).and.((nel==2).or.(nmu==2).or.(ntau==2)&
                     .or.(nel==-1).or.(nmu==-1).or.(ntau==-1))) then
    warning = .true.
    write(*,'(A)')char(27)//'[33m[WARN]'//char(27)//'[0m'//&
      'This combination of flavours is for testing purposes only, continue at your own risk'
  endif

  if(nel==2) deltalph_0_tot = deltalph_0_tot + deltalph_l_1(q2,mel)
  if(nmu==2) deltalph_0_tot = deltalph_0_tot + deltalph_l_1(q2,mmu)
  if(ntau==2) deltalph_0_tot = deltalph_0_tot + deltalph_l_1(q2,mtau)

  if(nel==-1) deltalph_0_tot = deltalph_0_tot + (1/(1-alpha_true*deltalph_l_0(q2,mel)) - 1)
  if(nmu==-1) deltalph_0_tot = deltalph_0_tot + (1/(1-alpha_true*deltalph_l_0(q2,mmu)) - 1)
  if(ntau==-1) deltalph_0_tot = deltalph_0_tot+ (1/(1-alpha_true*deltalph_l_0(q2,mtau)) - 1)

  !!! Todo: need to switch to alpha qed for q2 < -4e12 and q2 > 3e14 !!!
  if(nhad==1 .and. q2 > -4e12 .and. q2 < 3e14) deltalph_0_tot = deltalph_0_tot + deltalph_h(q2)

  END FUNCTION DELTALPH_0_TOT

  FUNCTION DELTALPH_0_TOT_CPLX(q2)
  real (kind=prec), intent(in) :: q2
  complex (kind=prec) :: deltalph_0_tot_cplx

  deltalph_0_tot_cplx = 0._prec + imag*0._prec

  if(nel==1) deltalph_0_tot_cplx = deltalph_0_tot_cplx + deltalph_l_0_cplx(q2,mel)
  if(nmu==1) deltalph_0_tot_cplx = deltalph_0_tot_cplx + deltalph_l_0_cplx(q2,mmu)
  if(ntau==1) deltalph_0_tot_cplx = deltalph_0_tot_cplx + deltalph_l_0_cplx(q2,mtau)
  if(nhad==1) deltalph_0_tot_cplx = deltalph_0_tot_cplx + deltalph_h_cplx(q2)

  END FUNCTION DELTALPH_0_TOT_CPLX


  FUNCTION DELTALPH_L_1_TOT(q2)
  real (kind=prec), intent(in) :: q2
  real (kind=prec) :: deltalph_l_1_tot

  deltalph_l_1_tot = 0._prec

  if(nel==1) deltalph_l_1_tot = deltalph_l_1_tot + deltalph_l_1(q2,mel)
  if(nmu==1) deltalph_l_1_tot = deltalph_l_1_tot + deltalph_l_1(q2,mmu)
  if(ntau==1) deltalph_l_1_tot = deltalph_l_1_tot + deltalph_l_1(q2,mtau)

  END FUNCTION DELTALPH_L_1_TOT

  FUNCTION DELTALPH_L_1_TOT_CPLX(q2)
  real (kind=prec), intent(in) :: q2
  complex (kind=prec) :: deltalph_l_1_tot_cplx

  deltalph_l_1_tot_cplx = 0._prec

  if(nel==1) deltalph_l_1_tot_cplx = deltalph_l_1_tot_cplx + deltalph_l_1_cplx(q2,mel)
  if(nmu==1) deltalph_l_1_tot_cplx = deltalph_l_1_tot_cplx + deltalph_l_1_cplx(q2,mmu)
  if(ntau==1) deltalph_l_1_tot_cplx = deltalph_l_1_tot_cplx + deltalph_l_1_cplx(q2,mtau)

  END FUNCTION DELTALPH_L_1_TOT_CPLX

  FUNCTION DELTALPH2_H_CPLX(q2)
  real (kind=prec), intent(in) :: q2
  complex (kind=prec) :: deltalph2_h_cplx
  real (kind=prec) :: e
  complex (kind=prec) :: cerror, cerrorsta, cerrorsys, der
  complex (kind=prec) :: cegvapx
  external cegvapx
  real(kind=prec) :: st2,als,mtop
  character(len=3) LEPTONflag
  integer iLEP,iwarnings
  common /lepton/iLEP,iwarnings,LEPTONflag
  common /parm/st2,als,mtop
  st2 = sw2

  LEPTONflag = 'had'
  der = cegvapx(1e-6*q2, cerror, cerrorsta, cerrorsys)
  deltalph2_h_cplx = alpha*137.035999_prec*der
  END FUNCTION DELTALPH2_H_CPLX


  FUNCTION DELTALPH2_0_TOT_CPLX(q2)
    ! This calculates Pi_{3\gamma}
  real (kind=prec), intent(in) :: q2
  complex (kind=prec) :: deltalph2_0_tot_cplx

  deltalph2_0_tot_cplx = 0._prec + imag*0._prec

  if(nel==1) deltalph2_0_tot_cplx = deltalph2_0_tot_cplx + deltalph_l_0_cplx(q2,mel)/4
  if(nmu==1) deltalph2_0_tot_cplx = deltalph2_0_tot_cplx + deltalph_l_0_cplx(q2,mmu)/4
  if(ntau==1) deltalph2_0_tot_cplx = deltalph2_0_tot_cplx + deltalph_l_0_cplx(q2,mtau)/4
  if(nhad==1) deltalph2_0_tot_cplx = deltalph2_0_tot_cplx + deltalph2_h_cplx(q2)
  END FUNCTION DELTALPH2_0_TOT_CPLX

  FUNCTION DELTALPH2P_0_TOT(q2)
    ! This calculates the corrections factor for Pi2 and Pi3
  real (kind=prec), intent(in) :: q2
  real (kind=prec) :: deltalph2p_0_tot(2)
  real (kind=prec), parameter :: replQ = -6.88
  real (kind=prec) :: repl
  integer :: nL, nU, nD
  real(kind=prec) :: SW, CW
  SW = sqrt(sw2)
  CW = sqrt(1-SW2)

  deltalph2p_0_tot = 0
  repl = 0.
  nL = 0
  nU = 0
  nD = 0

  if(nel==1) then
    repl = repl - (1-4*sw2)/3. * log(mz/mel)
    nL = nL + 1
  endif
  if(nmu==1) then
    repl = repl - (1-4*sw2)/3. * log(mz/mmu)
    nL = nL + 1
  endif
  if(ntau==1) then
    repl = repl - (1-4*sw2)/3. * log(mz/mtau)
    nL = nL + 1
  endif
  if(nhad==1) then
    repl = repl + replQ
    nU = 2
    nD = 3
  endif

  deltalph2p_0_tot(1) =  5./ 18. * (1-4*sw2)/sw/cw * nl &
                      +  5./ 81. * (3-8*sw2)/sw/cw * nu &
                      +  5./162. * (3-4*sw2)/sw/cw * nd &
                      + repl               /sw/cw


  deltalph2p_0_tot(2) = (1- 2*sw2+ 4*sw2**2)/  6./cw**2/sw2 * nl &
                      + (9-24*sw2+32*sw2**2)/108./cw**2/sw2 * nu &
                      + (9-12*sw2+ 8*sw2**2)/108./cw**2/sw2 * nd

  deltalph2p_0_tot = alpha/2/pi * deltalph2p_0_tot
  END FUNCTION DELTALPH2P_0_TOT


!===================== end running of effective em coupling ====================!



!===================== hyperspherical kernel form factors ====================!


  !!! kernel functions as defined in 1808.08233       !!!
  !!! note: different sign convention for vp function !!!


  FUNCTION F1_KERNEL(xprec,yprec)
  real (kind=prec), intent(in) :: xprec, yprec
  real (kind=prec) :: f1_kernel
  real (kind=precnf) :: x, y, f1_kernel_precnf

  x = real(xprec,kind=precnf)
  y = real(yprec,kind=precnf)

  if (1-x < 1e-6 .and. 1-x < 1./y/10.) then
    f1_kernel_precnf = y/6._precnf - ((-1 + x)*y*(22 + y))/24._precnf&
            + ((-1 + x)**2*y*(-70 + 33*y + y**2))/60._precnf&
            - ((-1 + x)**3*y*(280 - 242*y + 52*y**2 + y**3))/120._precnf&
            + ((-1 + x)**4*y*(-910 + 2051*y - 1051*y**2 + 155*y**3 + 2*y**4))/420._precnf
    f1_kernel = real(f1_kernel_precnf, kind=prec)
    return
  endif

  f1_kernel_precnf = (4 - 4*x**2 + 3*x**3)/(4.*(1 - x)*x) + ((2 - x)*((6*x**2)/((-1 + x)*(4 - y)**2) &
          &+ (4 - 6*x + x**2)/(2.*(-1 + x)*(4 - y)) + (((2*(-1 + x + x**2))/((1 - x)*x) - (&
          &12*x**3)/((1 - x)**2*(4 - y)**2) + (x*(-8 + 8*x + x**2))/((1 - x)**2*(4 - y)) + &
          &(4 - y)/x)*ATanh(((1 - x)*Sqrt((-4 + y)*y))/(-4 + 2*x + y - x*y)))/Sqrt((-4 + &
          &y)*y)))/(1 - x)
  f1_kernel = real(f1_kernel_precnf, kind=prec)

  END FUNCTION

  FUNCTION F2_KERNEL(xprec,yprec)
  real (kind=prec), intent(in) :: xprec, yprec
  real (kind=prec) :: f2_kernel
  real (kind=precnf) :: x, y, f2_kernel_precnf

  x = real(xprec,kind=precnf)
  y = real(yprec,kind=precnf)

  if (1-x < 1e-6 .and. 1-x < 1./y/10.) then
    f2_kernel_precnf = ((-1 + x)*(-6 + y))/6._precnf&
            - ((-1 + x)**2*(-5 + y)*y)/5._precnf&
            + ((-1 + x)**3*y*(20 - 18*y + 3*y**2))/15._precnf&
            + ((-1 + x)**4*y*(35 - 63*y + 30*y**2 - 4*y**3))/21._precnf&
            + ((-1 + x)**5*y*(280 - 812*y + 702*y**2 - 230*y**3 + 25*y**4))/140._precnf&
            + ((-1 + x)**6*y*(70 - 294*y + 390*y**2 - 220*y**3 + 55*y**4 - 5*y**5))/30._precnf&
            + ((-1 + x)**7*y*(840 - 4788*y + 8910*y**2 - 7480*y**3 + 3120*y**4 - 630*y**5 + 49*y**6))/315._precnf
    f2_kernel = real(f2_kernel_precnf, kind=prec)
    return
  endif

  f2_kernel_precnf = ((2 - x)*((6*x**2)/((1 - x)*(4 - y)**2) + (2 - x)/(4 - y) + (4*((3*x**3)/((1 - x&
          &)**2*(4 - y)**2) + (2*x)/((1 - x)*(4 - y)))*ATanh(((1 - x)*Sqrt((-4 + y)*y))/(&
          &-4 + 2*x + y - x*y)))/Sqrt((-4 + y)*y)))/(1 - x)
  f2_kernel = real(f2_kernel_precnf, kind=prec)

  END FUNCTION

  FUNCTION G1_KERNEL(xprec,yprec)
  real (kind=prec), intent(in) :: xprec, yprec
  real (kind=prec) :: g1_kernel
  real (kind=precnf) :: x, y, g1_kernel_precnf

  x = real(xprec,kind=precnf)
  y = real(yprec,kind=precnf)

  if (1-x < 1e-6 .and. 1-x < 1/y/10.) then
    g1_kernel_precnf = (-6 + y)/6._precnf + (-24 - 2*y - y**2)*(x - 1) / 24._precnf &
                + ( 50*y -   9*y**2 + y**3)*(x - 1)**2 / 60._precnf &
                + ( 40*y -  94*y**2 + 20*y**3 - y**4)*(x - 1)**3 / 120._precnf &
                + (490*y - 679*y**2 + 389*y**3 - 65*y**4 + 2*y**5)*(x - 1)**4 / 420._precnf
    g1_kernel = real(g1_kernel_precnf, kind=prec)
    return
  endif

  g1_kernel_precnf = (4 - 4*x**2 + 3*x**3)/4/x/(1-x) + (x-2)*(-4 + x*(4 + x))/2 / (1-x)**2 / (y-4) &
            + (2 - x)*y*(8 + (-2 + x)*x*(8 + (-2 + x)*x) - 6*y &
                    + 2*x*(6 + (-4 + x)*x)*y + (-1 + x)**2*y**2&
              )/(-1 + x)**3/x/((-4 + y)*y)**1.5 &
              *ATanh(((-1 + x)*Sqrt((-4 + y)*y))/(4 + x*(-2 + y) - y))
  g1_kernel = real(g1_kernel_precnf, kind=prec)

  END FUNCTION

  FUNCTION G2_KERNEL(xprec,yprec)
  real (kind=prec), intent(in) :: xprec, yprec
  real (kind=prec) :: g2_kernel
  real (kind=precnf) :: x, y, g2_kernel_precnf

  x = real(xprec,kind=precnf)
  y = real(yprec,kind=precnf)

  if (1-x < 1e-6 .and. 1-x < 1/y/10.) then
    g2_kernel_precnf = 4._precnf/3._precnf + (2 - 5*y)*(x - 1) / 6 &
               + y*(-7 + 3*y)*(x - 1)**2 / 5._precnf &
               - y*(28 - 30*y + 7*y**2)*(x - 1)**3 / 15._precnf &
               + y*(-49 + 93*y - 50*y**2 + 8*y**3)*(x - 1)**4 / 21._precnf
    g2_kernel = real(g2_kernel_precnf, kind=prec)
    return
  endif

  g2_kernel_precnf = (2 - x)*(-2*y + x*(4 + x*(-2 + y) + y))/(1 - x)**2/(-4 + y)/y &
        + 4*x*((-2 + x)**3 + 2*(-2 + x)*(-1 + x)*y)/((-1 + x)**3*((-4 + y)*y)**1.5_precnf) &
            *ATanh((-1 + x)*Sqrt((-4 + y)*y)/(4 + x*(-2 + y) - y))
  g2_kernel = real(g2_kernel_precnf, kind=prec)

  END FUNCTION


  FUNCTION F1NF(z,t,m2)
  real (kind=prec),intent(in) :: z, t, m2
  real (kind=prec) :: f1nf
  real (kind=prec) :: q2, y, x

  f1nf = 0._prec

  !!! substitution (importance sampling) to remove peak at x = 1 !!!
  x = 1 - z**5
  q2 = m2*x**2/(x-1)
  y = t/m2

  f1nf = 1/pi*deltalph_0_tot(q2)*f1_kernel(x,y)
  f1nf = alpha*5*z**4*f1nf
  END FUNCTION


  FUNCTION F2NF(z,t,m2)
  real (kind=prec),intent(in) :: z, t, m2
  real (kind=prec) :: f2nf
  real (kind=prec) :: q2, y, x

  f2nf = 0._prec

  !!! substitution (importance sampling) to remove peak at x = 1 !!!
  x = 1 - z**5
  q2 = m2*x**2/(x-1)
  y = t/m2

  f2nf = 1/pi*deltalph_0_tot(q2)*f2_kernel(x,y)
  f2nf = alpha*5*z**4*f2nf
  END FUNCTION


  FUNCTION G1NF(z,t,m2)
  real (kind=prec),intent(in) :: z, t, m2
  real (kind=prec) :: g1nf
  real (kind=prec) :: q2, y, x

  g1nf = 0._prec

  !!! substitution (importance sampling) to remove peak at x = 1 !!!
  x = 1 - z**5
  q2 = m2*x**2/(x-1)
  y = t/m2

  g1nf = 1/pi*deltalph_0_tot(q2)*g1_kernel(x,y)
  g1nf = alpha*5*z**4*g1nf
  END FUNCTION


  FUNCTION G2NF(z,t,m2)
  real (kind=prec),intent(in) :: z, t, m2
  real (kind=prec) :: g2nf
  real (kind=prec) :: q2, y, x

  g2nf = 0._prec

  !!! substitution (importance sampling) to remove peak at x = 1 !!!
  x = 1 - z**5
  q2 = m2*x**2/(x-1)
  y = t/m2

  g2nf = 1/pi*deltalph_0_tot(q2)*g2_kernel(x,y)
  g2nf = alpha*5*z**4*g2nf
  END FUNCTION



!===================== end hyperspherical kernel form factors ====================!



  FUNCTION QTIL(cc,xicut,epcmf,q1)

  real (kind=prec), intent(in) :: q1(4),epcmf,xicut
  character (len=1), intent(in) :: cc
  real (kind=prec) :: qtil, e1, gamma, gammap, ccol

  e1 = q1(4)

  select case(cc)
  case("l")
    gamma = 1.5_prec
    gammap = (6.5_prec - 2*pi**2/3.)
    ccol = 1._prec
  case("q")
    gamma = 1.5_prec*Cf
    gammap = (6.5_prec - 2*pi**2/3.)*Cf
    ccol = Cf
  case("g")
    gamma = (11.*Nc - 4*Tf*5.)/6.  !!Nf -> 5
    gammap = 67.*Nc/9. - 2*pi**2*Nc/3. - 23.*Tf*5./9.   !!Nf -> 5
    ccol = Nc
  case default
    call crash("QTIL")
  end select

  qtil = gammap - gamma*log(2*delcut*e1**2/musq) +                 &
         2*ccol*log(2*e1/epcmf/xicut)*                             &
                 log(epcmf*delcut*e1*xicut/musq)

  END FUNCTION QTIL




  FUNCTION BOOST_BACK(rec,mo)   !!boosts to cms system

  real (kind=prec), intent(in):: rec(4),mo(4)
  real (kind=prec)  :: cosh_a, energy,  dot_dot,   &
                       n_vec(3), boost_back(4)

  energy = rec(4)**2 - rec(1)**2 - rec(2)**2 - rec(3)**2

  if(energy < 1.0E-16_prec) then
    throw_away = 1
    energy = 1.0E-12_prec
  else
    energy = sqrt(energy)
  end if

  cosh_a = rec(4)/energy
  n_vec = - rec(1:3)/energy  ! 1/sinh_a omitted

  dot_dot = sum(n_vec*mo(1:3))  ! \vec{n} \dot \vec{m}

  boost_back(1:3) =    &
     mo(1:3) + n_vec*(dot_dot/(cosh_a + 1) - mo(4))
  boost_back(4) = mo(4)*cosh_a - dot_dot

  END FUNCTION BOOST_BACK



  FUNCTION BOOST_RFREC(rec,mo1)   !!boosts mo to rest-frame of rec

  real (kind=prec), intent(in):: rec(4),mo1(4)
  real (kind=prec)  :: energy, beta(3), gamma, betaSQ, dot_dot, betaABS
  real (kind=prec)  :: boost_rfrec(4)

  energy = rec(4)
  beta = rec(1:3)/energy
  betaSQ = beta(1)**2 + beta(2)**2 + beta(3)**2
  betaABS = sqrt(betaSQ)
  if(betaSQ > zero) then
    beta = beta/betaABS
  else
    beta = (/0._prec,0._prec,0._prec /)
  endif
  gamma = 1./sqrt(1._prec-betaSQ)

  dot_dot = sum(beta*mo1(1:3))  ! \vec{beta} \dot \vec{m}

  boost_rfrec(1:3) =    &
     mo1(1:3) + beta*dot_dot*(gamma-1.) - beta*betaABS*gamma*mo1(4)

  boost_rfrec(4) = gamma*(mo1(4)-betaABS*dot_dot)

!!$  boost_rfrec(1:3) =    &
!!$     mo1(1:3) + beta*(dot_dot*(gamma-1.)/betaSQ - gamma*mo1(4))
!!$
!!$  boost_rfrec(4) = gamma*(mo1(4)-dot_dot)

  END FUNCTION BOOST_RFREC



  ! this used to be in phase_space but we need it here as well

  FUNCTION SQ_LAMBDA(ss,m1,m2) !! square root of \lambda(s,m1,m2)

  implicit none
  real (kind=prec) ss,m1,m2,sq_lambda

  sq_lambda = (ss - (m1+m2)**2)*(ss - (m1-m2)**2)
  sq_lambda = sqrt(sq_lambda)

  END FUNCTION SQ_LAMBDA
  FUNCTION CSQ_LAMBDA(ss,m1,m2) !! complex square root of \lambda(s,m1,m2)

  implicit none
  real (kind=prec) ss,m1,m2
  complex (kind=prec) csq_lambda

  csq_lambda = (ss - (m1+m2)**2)*(ss - (m1-m2)**2)
  csq_lambda = sqrt(csq_lambda)

  END FUNCTION CSQ_LAMBDA



  FUNCTION EULER_MAT(a,b,c)
  real (kind=prec) ::euler_mat(4,4)
  real (kind=prec), intent(in) :: a ,b ,c
  real (kind=prec) :: ca, cb, cc, sa, sb, sc

  ca = cos(a); sa = sin(a)
  cb = cos(b); sb = sqrt(1-cb**2)
  cc = cos(c); sc = sin(c)

  euler_mat(1,:) =  &
     (/  ca*cb*cc - sa*sc  ,  -(cc*sa) - ca*cb*sc     ,  ca*sb    ,  0._prec  /)
  euler_mat(2,:) =  &
     (/  cb*cc*sa + ca*sc  ,  ca*cc - cb*sa*sc        ,  sa*sb    ,  0._prec  /)
  euler_mat(3,:) =  &
     (/ -cc*sb             ,  sb*sc                   ,  cb       ,  0._prec  /)
  euler_mat(4,:) =  &
     (/  0._prec           ,  0._prec                 ,  0._prec  ,  1._prec  /)


  END FUNCTION

!======================    Package X abbr.   ======================!



!!! Collier implementation (complex valued) !!!

  FUNCTION DISCB_COLL(s,m1,m2)
  ! Package X function DiscB[s,m1,m2]
  real(kind=prec), intent(in) :: s,m1,m2
  complex(kind=prec) :: pvb, discb_coll
  real(kind=prec) :: m1sq, m2sq

  call SetDeltaUV_cll(0.) ; call SetDeltaIR_cll(0.,0.)
  call SetMuUV2_cll(musq) ; call SetMuIR2_cll(musq)
  call B0_cll(pvb, cmplx(s), cmplx(m1**2), cmplx(m2**2))

  m1sq = m1**2
  m2sq = m2**2
  discb_coll = pvb-2+(m1sq-m2sq+s)*log(m1sq/m2sq)/2/s-log(musq/m2sq)
  END FUNCTION DISCB_COLL


  FUNCTION SCALARC0_COLL(s1,s12,s2,m1,m2,m3)
  ! Package X function ScalarC0[s1,s12,s2,m1,m2,m3]
  real(kind=prec), intent(in) :: s1,s12,s2,m1,m2,m3
  complex(kind=prec) :: scalarc0_coll, pvc

  call C0_cll(pvc, &
      cmplx(s1),cmplx(s12),cmplx(s2)  ,  &
      cmplx(m1**2),cmplx(m2**2),cmplx(m3**2) )

  scalarc0_coll = pvc
  END FUNCTION SCALARC0_COLL


  FUNCTION SCALARC0IR6_COLL(s,m1,m2)
  ! Package X function ScalarC0IR6[s,m1,m2]
  real(kind=prec), intent(in) :: s, m1,m2
  complex(kind=prec) :: scalarc0ir6_coll, pvc
  real(kind=prec) :: klambda, m1sq, m2sq

  m1sq = m1**2
  m2sq = m2**2

  call SetDeltaUV_cll(0.) ; call SetDeltaIR_cll(0.,0.)
  call SetMuUV2_cll(musq) ; call SetMuIR2_cll(musq)
  call C0_cll(pvc, &
      cmplx(m1sq),cmplx(s),cmplx(m2sq)  ,  &
      cmplx(0.),cmplx(m1sq),cmplx(m2sq) )

  if(s==0) then
    scalarc0ir6_coll = 0.
    return
  end if
  klambda = m1sq**2+(m2sq-s)**2-2*m1sq*(m2sq+s)
  scalarc0ir6_coll = -0.5*s*discb_cplx(s,m1,m2)/klambda*(2*log(musq/m1sq)+log(m1sq/m2sq))
  scalarc0ir6_coll = scalarc0ir6_coll + pvc
  END FUNCTION SCALARC0IR6_COLL


  FUNCTION SCALARD0IR16_COLL(x,y,m)
  ! Package-X function ScalarD0IR16[0,0,x,y,m,m,m]
  real(kind=prec), intent(in) :: x, y, m
  complex(kind=prec) :: scalard0ir16_coll, pvd, m2, logmu

  m2 = m**2
  call SetDeltaUV_cll(0.) ; call SetDeltaIR_cll(0.,0.)
  call SetMuUV2_cll(musq) ; call SetMuIR2_cll(musq)
  call D0_cll(pvd, &
      cmplx(m2), cmplx(y), cmplx(0._prec),cmplx(x), cmplx(m2),cmplx(0._prec)  ,  &
      cmplx(0._prec),cmplx(m2),cmplx(m2),cmplx(m2))

  logmu = log(musq/m2)
  scalard0ir16_coll = pvd - discb_cplx(y,m)*logmu/(m2-x)/(4*m2-y)
  END FUNCTION SCALARD0IR16_COLL

  FUNCTION SCALARD0IR16M3_COLL(x,t,m1,m2,m3)
  ! Package-X function ScalarD0IR16[m^2,M^2,t,x,m1,m2,m3]
  real(kind=prec), intent(in) :: t, x, m1, m2, m3
  complex(kind=prec) :: scalard0ir16m3_coll, pvd, m12, m22, m32, logmu

  m12 = m1**2
  m22 = m2**2
  m32 = m3**2
  call SetDeltaUV_cll(0.) ; call SetDeltaIR_cll(0.,0.)
  call SetMuUV2_cll(musq) ; call SetMuIR2_cll(musq)
  call D0_cll(pvd, &
       cmplx(m12), cmplx(t), cmplx(m32), cmplx(x), cmplx(m12),cmplx(m32)  ,  &
       cmplx(m12), cmplx(m22), cmplx(0._prec), cmplx(m32))

  logmu = log(musq**2/m12/m32)
  scalard0ir16m3_coll = pvd - x/2 * discb_cplx(x,m1,m3) &
                                  * logmu/(m12**2+(m32-x)**2-2*m12*(m32+x))/(t-m22)
  END FUNCTION SCALARD0IR16M3_COLL

  FUNCTION SCALARD0_COLL(x,t,m1,m2,m3,m4)
  ! Package X function ScalarD0[m1^2,t,m4^2,x,m1^2,m4^2,m1,m2,m3,m4]
  real(kind=prec), intent(in) :: x, t, m1, m2, m3, m4
  complex(kind=prec) :: scalard0_coll, pvd, m12, m22, m32, m42

  m12 = m1**2
  m22 = m2**2
  m32 = m3**2
  m42 = m4**2
  call D0_cll(pvd, &
       cmplx(m12), cmplx(t), cmplx(m42), cmplx(x), cmplx(m12),cmplx(m42)  ,  &
       cmplx(m12), cmplx(m22), cmplx(m32), cmplx(m42))

  scalard0_coll = pvd
  END FUNCTION SCALARD0_COLL

  !!! One-mass !!!

  !! DiscB !!

  FUNCTION DISCBSMM_FULL(s,m,img)
  ! Re & Im of Package-X function DiscB[s,m,m]
  real(kind=prec) :: discbsmm_full
  real(kind=prec), intent(in) :: s, m
  real(kind=prec), optional :: img
  real(kind=prec) :: z, beta

  if(present(img)) img = 0._prec

  z = s/4/m**2
  if(abs(z)<1E-7_prec) then
    discbsmm_full = -2+2*z/3+4*z**2/15+16*z**3/105
    return
  end if

  beta = sqrt(abs((z-1)/z))
  if(z<0) then
    discbsmm_full = beta*log((beta-1)/(beta+1))
  elseif(z>1) then
    discbsmm_full = beta*log((1-beta)/(beta+1))
    if(present(img)) img = pi*beta
  else
    discbsmm_full = -beta*atan2(2*beta,beta**2-1)
  endif
  END FUNCTION

  ! real
  FUNCTION DISCBSMM(s,m)
  ! Package-X function Re[DiscB[s,m,m]]
  real(kind=prec) :: discbsmm
  real(kind=prec), intent(in) :: s, m

  discbsmm = discbsmm_full(s,m)
  END FUNCTION

  ! complex
  FUNCTION DISCBSMM_CPLX(s,m)
  ! Package-X function DiscB[s,m,m]
  complex(kind=prec) :: discbsmm_cplx
  real(kind=prec), intent(in) :: s, m
  real(kind=prec) :: img

  discbsmm_cplx = discbsmm_full(s,m,img)
  discbsmm_cplx = discbsmm_cplx + imag*img
  END FUNCTION


  !! ScalarC0IR6 !!

  FUNCTION SCALARC0IR6SMM_FULL(s,m,img)
  ! Re & Im of Package-X function ScalarC0IR6[s,m,m]
  real(kind=prec) :: scalarc0ir6smm_full
  real(kind=prec), intent(in) :: s, m
  real(kind=prec), optional :: img
  real(kind=prec) :: z,beta,arg1,arg2,log1,log2,polylog
  real(kind=prec) :: phi

  if(present(img)) img = 0._prec

  z = s/4/m**2
  if(abs(z)<1E-7_prec) then
    scalarc0ir6smm_full = (z+6*z**2/5+44*z**3/35+80*z**4/63)/3/m**2
    return
  end if

  beta = sqrt(abs((z-1)/z))
  if(0<z .and. z<1) then
    phi = -2*atan2(-1._prec,beta)
    scalarc0ir6smm_full = -(cl2(2*phi)-2*cl2(phi)-phi*log((1+beta**2)/4/beta**2))/(s*beta)
    return
  end if

  arg1 = (beta-1)/(1+beta); arg2 = 2*beta/(1+beta)
  log1 = log(abs(arg1)); log2 = log(arg2)
  polylog = Li2(-arg1)
  if(z<0) then
    scalarc0ir6smm_full = -(pi**2-3*log1**2+12*log1*log2+12*polylog)/(6*s*beta)
  else
    scalarc0ir6smm_full = -(4*pi**2-3*log1**2+12*log1*log2+12*polylog)/(6*s*beta)
    if(present(img)) img = (2*log1-4*log2)*pi/(2*s*beta)
  endif
  END FUNCTION

  ! real
  FUNCTION SCALARC0IR6SMM(s,m)
  ! Package-X function Re[ScalarC0IR6[s,m,m]]
  real(kind=prec) :: scalarc0ir6smm
  real(kind=prec), intent(in) :: s, m

  scalarc0ir6smm = scalarc0ir6smm_full(s,m)
  END FUNCTION

  ! complex
  FUNCTION SCALARC0IR6SMM_CPLX(s,m)
  ! Package-X function ScalarC0IR6[s,m,m]
  complex(kind=prec) :: scalarc0ir6smm_cplx
  real(kind=prec), intent(in) :: s, m
  real(kind=prec) :: img

  scalarc0ir6smm_cplx = scalarc0ir6smm_full(s,m,img)
  scalarc0ir6smm_cplx = scalarc0ir6smm_cplx + imag*img
  END FUNCTION


  !! ScalarC0 !!

  FUNCTION SCALARC0SM_FULL(s,m,img)
  ! Re & Im of Package-X function ScalarC0[m^2,m^2,s,0,m,0]
  real(kind=prec) :: scalarc0sm_full
  real(kind=prec), intent(in) :: s, m
  real(kind=prec), optional :: img
  real(kind=prec) :: z, beta, arg, logarg, log2
  real(kind=prec) :: phi

  if(present(img)) img = 0._prec

  z = s/4/m**2
  if(z>1 .AND. z-1<1E-7) then
          log2 = log(2._prec)
          scalarc0sm_full = (2*log2+1._prec/3*(1-4*log2)*(z-1))/2/m**2
          if(present(img)) img = pi/2/m**2*(-1+2._prec/3*(z-1))
          return
  endif

  beta = sqrt(abs((z-1)/z))
  if(0<z .and. z<1) then
    phi = -2*atan2(-1._prec,beta)
    scalarc0sm_full = (2*cl2(phi))/(s*beta)
    if(present(img)) img = (phi-pi)*pi/(s*beta)
    return
  end if

  arg = (beta-1)/(beta+1)
  logarg = log(abs(arg))
  if(z<0) then
    scalarc0sm_full = (4*pi**2+3*logarg**2+12*Li2(arg))/(6*s*beta)
  else
    scalarc0sm_full = (pi**2+3*logarg**2+12*Li2(arg))/(6*s*beta)
    if(present(img)) img = pi*logarg/(s*beta)
  endif
  END FUNCTION

  ! real
  FUNCTION SCALARC0SM(s,m)
  ! Package-X function Re[ScalarC0[m^2,m^2,s,0,m,0]]
  real(kind=prec) :: scalarc0sm
  real(kind=prec), intent(in) :: s, m

  scalarc0sm = scalarc0sm_full(s,m)
  END FUNCTION

  ! complex
  FUNCTION SCALARC0SM_CPLX(s,m)
  ! Package-X function ScalarC0[m^2,m^2,s,0,m,0]
  complex(kind=prec) :: scalarc0sm_cplx
  real(kind=prec), intent(in) :: s, m
  real(kind=prec) :: img

  scalarc0sm_cplx = scalarc0sm_full(s,m,img)
  scalarc0sm_cplx = scalarc0sm_cplx + imag*img
  END FUNCTION


  !! ScalarD0IR16 !!

  FUNCTION SCALARD0IR16XYM_FULL(x,y,m,img)
  ! Re & Im of Package-X function ScalarD0IR16[0,0,x,y,m,m,m]
  real(kind=prec) :: scalard0ir16xym_full
  real(kind=prec), intent(in) :: x, y, m
  real(kind=prec), optional :: img
  real(kind=prec) :: m2, beta, z, arg
  real(kind=prec) :: logarg, log1, logx, li2a, li2b, li2a_img, li2b_img
  complex(kind=prec) :: scalard0ir16_cplx

  if(present(img)) img = 0._prec

  m2 = m**2
  z = y/4/m2

  if(0<z .and. z<1) then
    scalard0ir16_cplx = scalard0ir16_coll(x,y,m)
    scalard0ir16xym_full = real(scalard0ir16_cplx)
    if(present(img)) img = aimag(scalard0ir16_cplx)
    return
  end if

  beta = sqrt((z-1)/z)
  arg = (beta-1)/(beta+1)
  logarg = log(abs(arg))
  log1 = log(4*beta/(1+beta)**2)
  logx = log(abs(m2/(m2-x)))
  li2a = li2(2/(1-beta),li2a_img)
  li2b = li2(arg**2, li2b_img)
  if(z<0) then
    scalard0ir16xym_full = pi**2+12*logx*logarg-12*log1*logarg+12*logarg**2+24*li2a-6*li2b
    scalard0ir16xym_full = scalard0ir16xym_full/6./(x-m2)/y/beta
    if(present(img) .and. x > m2) img = 12*pi*logarg/6./(x-m2)/y/beta
  else
    scalard0ir16xym_full = 11*pi**2-12*logx*logarg+12*log1*logarg-12*logarg**2-24*li2a+6*li2b
    if(x>m2) scalard0ir16xym_full = scalard0ir16xym_full + 12*pi**2
    scalard0ir16xym_full = scalard0ir16xym_full/6./(m2-x)/y/beta
    if(present(img)) then
      img = 12*pi*(2*logarg+log1-logx)
      img = img - 24*li2a_img + 6*li2b_img
      if(x>m2) img = img - 12*pi*logarg
      img = img/6./(m2-x)/y/beta
    end if
  endif
  END FUNCTION


  ! real
  FUNCTION SCALARD0IR16XYM(x,y,m)
  ! Package-X function Re[ScalarD0IR16[0,0,x,y,m,m,m]]
  real(kind=prec) :: scalard0ir16xym
  real(kind=prec), intent(in) :: x, y, m

  scalard0ir16xym = scalard0ir16xym_full(x,y,m)
  END FUNCTION

  ! complex
  ! Todo Imaginary part slightly off because of a problem
  !      in Package-X. Interface disabled
  FUNCTION SCALARD0IR16XYM_CPLX(x,y,m)
  ! Package-X function ScalarC0[ScalarD0IR16[0,0,x,y,m,m,m]]
  complex(kind=prec) :: scalard0ir16xym_cplx
  real(kind=prec), intent(in) :: x, y, m
  real(kind=prec) :: img

  scalard0ir16xym_cplx = scalard0ir16xym_full(x,y,m,img)
  scalard0ir16xym_cplx = scalard0ir16xym_cplx + imag*img
  END FUNCTION



  !!! Real-valued: Two masses !!!


  !! DiscB !!

  FUNCTION DISCBSMN_FULL(s,m1,m2,img)
  ! Re & Im of Package-X function DiscB[s,m1,m2]
  real(kind=prec) :: discbsmn_full
  real(kind=prec), intent(in) :: s, m1, m2
  real(kind=prec), optional :: img
  real(kind=prec) :: thresh1, thresh2, thresh3, v, chi, logm, m1sq, m2sq

  if(abs(m1-m2)<zero) then
    if(present(img)) then
      discbsmn_full = discbsmm_full(s,m1,img)
    else
      discbsmn_full = discbsmm_full(s,m1)
    endif
    return
  endif

  if(present(img)) img = 0._prec

  m1sq = m1**2
  m2sq = m2**2

  thresh1 = (m1-m2)**2
  thresh2 = m1**2 + m2**2
  thresh3 = (m1+m2)**2

  ! stabilisation with expansion around s=0
  if(abs(s)/(m1-m2)**2<0.1) then
    logm = 2*log(m1/m2)
    discbsmn_full = (-420 - (210*logm*(m1sq + m2sq))/(m1sq - m2sq) + (210*logm*(m1sq - m2sq))/s + (2&
                &10*(m1sq**2 - 2*logm*m1sq*m2sq - m2sq**2)*s)/(m1sq - m2sq)**3 + (70*(m1sq**3 + 9&
                &*m1sq**2*m2sq - 9*m1sq*m2sq**2 - m2sq**3 - 6*logm*m1sq*m2sq*(m1sq + m2sq))*s**2)&
                &/(m1sq - m2sq)**5 + (35*(m1sq**4 + 28*m1sq**3*m2sq - 28*m1sq*m2sq**3 - m2sq**4 -&
                & 12*logm*m1sq*m2sq*(m1sq**2 + 3*m1sq*m2sq + m2sq**2))*s**3)/(m1sq - m2sq)**7 + (&
                &7*(3*m1sq**5 + 5*(35 - 12*logm)*m1sq**4*m2sq - 60*(-5 + 6*logm)*m1sq**3*m2sq**2 &
                &- 60*(5 + 6*logm)*m1sq**2*m2sq**3 - 5*(35 + 12*logm)*m1sq*m2sq**4 - 3*m2sq**5)*s&
                &**4)/(m1sq - m2sq)**9 + (14*(m1sq**6 + (101 - 30*logm)*m1sq**5*m2sq + 25*(17 - 1&
                &2*logm)*m1sq**4*m2sq**2 - 600*logm*m1sq**3*m2sq**3 - 25*(17 + 12*logm)*m1sq**2*m&
                &2sq**4 - (101 + 30*logm)*m1sq*m2sq**5 - m2sq**6)*s**5)/(m1sq - m2sq)**11 + (2*(5&
                &*m1sq**7 + 14*(56 - 15*logm)*m1sq**6*m2sq - 210*(-28 + 15*logm)*m1sq**5*m2sq**2 &
                &- 875*(-7 + 12*logm)*m1sq**4*m2sq**3 - 875*(7 + 12*logm)*m1sq**3*m2sq**4 - 210*(&
                &28 + 15*logm)*m1sq**2*m2sq**5 - 14*(56 + 15*logm)*m1sq*m2sq**6 - 5*m2sq**7)*s**6&
                &)/(m1sq - m2sq)**13)/420.
    return
  endif

  if(s<thresh1) then
    v = sq_lambda(s,m1,m2)/(m1sq+m2sq-s)
    chi = (1-v)/(1+v)
    discbsmn_full = m1*m2*(1-chi)*log(chi)/2/(sqrt(chi)*m1-m2)/(m2*sqrt(chi)-m1)
  elseif(thresh1<s .and. s<thresh2) then
    v = sqrt(4*m1sq*m2sq/(m1sq+m2sq-s)**2-1)
    discbsmn_full = 2*m1*m2*atan(v)*v/(2*m1*m2-(m1sq+m2sq)*sqrt(1+v**2))
  elseif(thresh2<s .and. s<thresh3) then
    v = -sqrt(4*m1sq*m2sq/(m1sq+m2sq-s)**2-1)
    discbsmn_full = 2*m1*m2*(atan(v)+pi)*v/(2*m1*m2+(m1sq+m2sq)*sqrt(1+v**2))
  elseif(thresh3<s) then
    v = sq_lambda(s,m1,m2)/(m1sq+m2sq-s)
    chi = (1-v)/(1+v)
    discbsmn_full = m1*m2*(1-chi)*log(chi)/2/(m2+m1*sqrt(chi))/(m1+m2*sqrt(chi))
    if(present(img)) img = -pi*m1*m2*(1-chi)/(m2+m1*sqrt(chi))/(m1+m2*sqrt(chi))
  endif
  END FUNCTION

  ! real
  FUNCTION DISCBSMN(s,m1,m2)
  ! Package-X function Re[DiscB[s,m,m]]
  real(kind=prec) :: discbsmn
  real(kind=prec), intent(in) :: s, m1, m2

  discbsmn = discbsmn_full(s,m1,m2)
  END FUNCTION

  ! complex
  FUNCTION DISCBSMN_CPLX(s,m1,m2)
  ! Package-X function DiscB[s,m1,m2]
  complex(kind=prec) :: discbsmn_cplx
  real(kind=prec), intent(in) :: s, m1, m2
  real(kind=prec) :: img

  discbsmn_cplx = discbsmn_full(s,m1,m2,img)
  discbsmn_cplx = discbsmn_cplx + imag*img
  END FUNCTION


  !! ScalarC0IR6 !!

  FUNCTION SCALARC0IR6SMN_FULL(s,m1,m2,img)
  ! Re & Im of Package-X function ScalarC0IR6[s,m1,m2]
  real(kind=prec) :: scalarc0ir6smn_full
  real(kind=prec), intent(in) :: s, m1, m2
  real(kind=prec), optional :: img
  real(kind=prec) :: thresh1, thresh2, v, chi, mmin, mmax
  real(kind=prec) :: logm, logm1, logm2, logchi1, logchi2, li2a, li2b, li2a_img
  complex(kind=prec) :: scalarc0ir6_cplx

  if(abs(m1-m2)<zero) then
    if(present(img)) then
      scalarc0ir6smn_full = scalarc0ir6smm_full(s,m1,img)
    else
      scalarc0ir6smn_full = scalarc0ir6smm_full(s,m1)
    endif
  endif

  if(present(img)) img = 0._prec

  thresh1 = (m1-m2)**2
  thresh2 = (m1+m2)**2

  if(thresh1<s .and. s<thresh2) then
    scalarc0ir6_cplx = scalarc0ir6_coll(s,m1,m2)
    scalarc0ir6smn_full = real(scalarc0ir6_cplx)
    if(present(img)) img = aimag(scalarc0ir6_cplx)
    return
  end if

  ! stabilisation with expansion around s=0
  if(abs(s)/(m1-m2)**2<0.01) then
    logm = 2*log(m1/m2)
    scalarc0ir6smn_full = (s*(3600*(m1**2 - m2**2)**10*(-m1**2 + m2**2 + (m1**2 + m2**2)*Log(m1/m2)) + 270&
                      &0*(m1**2 - m2**2)**8*s*(-3*m1**4 + 3*m2**4 + 2*(m1**4 + 4*m1**2*m2**2 + m2**4)*L&
                      &og(m1/m2)) + 1100*(m1**2 - m2**2)**6*s**2*(-11*m1**6 - 27*m1**4*m2**2 + 27*m1**2&
                      &*m2**4 + 11*m2**6 + 6*(m1**6 + 9*m1**4*m2**2 + 9*m1**2*m2**4 + m2**6)*Log(m1/m2)&
                      &) + 625*(m1**2 - m2**2)**4*s**3*(-5*(5*m1**8 + 32*m1**6*m2**2 - 32*m1**2*m2**6 -&
                      & 5*m2**8) + 12*(m1**8 + 16*m1**6*m2**2 + 36*m1**4*m2**4 + 16*m1**2*m2**6 + m2**8&
                      &)*Log(m1/m2)) + 137*(m1**2 - m2**2)**2*s**4*(-137*m1**10 - 1625*m1**8*m2**2 - 20&
                      &00*m1**6*m2**4 + 2000*m1**4*m2**6 + 1625*m1**2*m2**8 + 137*m2**10 + 60*(m1**10 +&
                      & 25*m1**8*m2**2 + 100*m1**6*m2**4 + 100*m1**4*m2**6 + 25*m1**2*m2**8 + m2**10)*L&
                      &og(m1/m2)) + 441*s**5*(-7*(7*m1**12 + 132*m1**10*m2**2 + 375*m1**8*m2**4 - 375*m&
                      &1**4*m2**8 - 132*m1**2*m2**10 - 7*m2**12) + 20*(m1**12 + 36*m1**10*m2**2 + 225*m&
                      &1**8*m2**4 + 400*m1**6*m2**6 + 225*m1**4*m2**8 + 36*m1**2*m2**10 + m2**12)*Log(m&
                      &1/m2))))/(3600.*(m1**2 - m2**2)**13)
    return
  endif

  v = sq_lambda(s,m1,m2)/(m1**2+m2**2-s)
  chi = (1-v)/(1+v)
  mmin = min(m1,m2)*chi
  mmax = max(m1,m2)*sqrt(chi)
  logm1 = log(mmin/mmax)
  logchi1 = log(chi)
  logchi2 = log(abs(1-chi))

  ! stabilisation with expansion aroung chi=1
  if(abs(chi-1)<0.01 .and. chi>1) then
    logm2 = log(1+mmin/mmax)
    scalarc0ir6smn_full = (6*(-1 + chi)*logm1*(-((1 - 7*chi - 7*chi**2 + chi**3)*mmax**4) - 4*(1 - chi - 7&
                      &*chi**2 + chi**3)*mmax**3*mmin - 6*(-1 + 7*chi - 7*chi**2 + chi**3)*mmax**2*mmin&
                      &**2 + 4*(1 - 7*chi - chi**2 + chi**3)*mmax*mmin**3 + (1 - 7*chi - 7*chi**2 + chi&
                      &**3)*mmin**4) - (mmax + mmin)*(mmax**3*(-13 + 140*chi - 44*chi**3 + 7*chi**4 + 1&
                      &8*chi**2*(-5 + 8*Pi**2)) + mmin**3*(-7 + 44*chi - 140*chi**3 + 13*chi**4 + 18*ch&
                      &i**2*(5 + 8*Pi**2)) + 3*mmax**2*mmin*(-25 + 148*chi - 68*chi**3 + 11*chi**4 + 6*&
                      &chi**2*(-11 + 24*Pi**2)) + 3*mmax*mmin**2*(-11 + 68*chi - 148*chi**3 + 25*chi**4&
                      & + 6*chi**2*(11 + 24*Pi**2))))/(144.*(-1 + chi)*mmax*mmin*(mmax + mmin)**4)
    if(present(img)) then
      img = -((3*(-1 + 8*chi - 8*chi**3 + chi**4 + 8*chi**2*(2*logchi2 + logm1 - 2*logm2))*m&
      &max**4 + 12*(-1 + 6*chi - 8*chi**3 + chi**4 + 2*chi**2*(1 + 8*logchi2 + 4*logm1 &
      &- 8*logm2))*mmax**3*mmin + 6*(-1 + 8*chi - 24*chi**3 + 3*chi**4 + 2*chi**2*(7 + &
      &24*logchi2 + 12*logm1 - 24*logm2))*mmax**2*mmin**2 + 4*(-1 + 8*chi - 14*chi**3 +&
      & chi**4 + 6*chi**2*(1 + 8*logchi2 + 4*logm1 - 8*logm2))*mmax*mmin**3 + (-1 + 8*c&
      &hi - 8*chi**3 + chi**4 + 24*chi**2*(2*logchi2 + logm1 - 2*logm2))*mmin**4)*Pi)/(&
      &24.*(-1 + chi)*mmax*mmin*(mmax + mmin)**4)
    endif
    return
  endif

  if(s<thresh1) then
    logm2 = log(1-mmin/mmax)
    li2a = li2((mmin-mmax*chi)/(mmin-mmin*chi))
    li2b = li2((mmin-mmax*chi)/(mmax-mmax*chi))
    scalarc0ir6smn_full = logm1*(2*logm2-logm1+logchi1-2*logchi2)-2*li2a+2*li2b
    scalarc0ir6smn_full = -scalarc0ir6smn_full*chi**2/2/mmin/mmax/(1-chi)
  elseif(thresh2<s) then
    logm2 = log(1+mmin/mmax)
    li2a = li2((mmin+mmax*chi)/(mmax*chi-mmax),li2a_img)
    li2b = li2((mmin+mmax*chi)/(mmin-mmin*chi))
    scalarc0ir6smn_full = pi**2+logm1*(-logm1+2*logm2+logchi1-2*logchi2)+2*li2a-2*li2b
    scalarc0ir6smn_full = scalarc0ir6smn_full*chi**2/2/mmin/mmax/(1-chi)
    if(present(img)) then
      img = pi*(2*(logm1-logm2+logchi2)-logchi1)+2*li2a_img
      img = img*chi**2/2/mmin/mmax/(1-chi)
    endif
  endif
  END FUNCTION

  ! real
  FUNCTION SCALARC0IR6SMN(s,m1,m2)
  ! Package-X function Re[ScalarC0IR6[s,m1,m2]]
  real(kind=prec) :: scalarc0ir6smn
  real(kind=prec), intent(in) :: s, m1, m2

  scalarc0ir6smn = scalarc0ir6smn_full(s,m1,m2)
  END FUNCTION

  ! complex
  FUNCTION SCALARC0IR6SMN_CPLX(s,m1,m2)
  ! Package-X function ScalarC0IR6[s,m1,m2]
  complex(kind=prec) :: scalarc0ir6smn_cplx
  real(kind=prec), intent(in) :: s, m1, m2
  real(kind=prec) :: img

  scalarc0ir6smn_cplx = scalarc0ir6smn_full(s,m1,m2,img)
  scalarc0ir6smn_cplx = scalarc0ir6smn_cplx + imag*img
  END FUNCTION


!!! Loop Tools implementation (complex valued) !!!


  FUNCTION SCALARD0IR16MN(s2,s3,s12,s23,m1,m2,m3)
  real(kind=prec), intent(in) :: s2,s3,s12,s23,m1,m2,m3
  real(kind=prec) :: scalard0ir16mn
  complex(kind=prec) :: pv
  musq = 1.

  call SetDeltaUV_cll(0.) ; call SetDeltaIR_cll(0.,0.)
  call SetMuUV2_cll(musq) ; call SetMuIR2_cll(musq)

  call D0_cll(pv, cmplx(m1**2), cmplx(m3**2), cmplx(s3), cmplx(s2), cmplx(s23), cmplx(s12),&
      cmplx(m1**2), cmplx(0.), cmplx(m3**2), cmplx(m2**2))

  scalard0ir16mn = real(DiscB(s23,m1,m3))*s23 / SQ_LAMBDA(s23,m1,m3)**2 / (m2**2-s12)
  scalard0ir16mn = scalard0ir16mn * ( log(m1/m3) -log(m1**2/musq) )
  scalard0ir16mn = scalard0ir16mn + real(pv)
  END FUNCTION


!=====================  end Package X abbr ====================!



!======================    Former user.f95   ======================!

  FUNCTION BOOST_RF(rec,mo)   !!boosts mo to (non-unique) rest frame of rec

  real (kind=prec), intent(in):: rec(4), mo(4)
  real (kind=prec)  :: energy,  mass, dot_dot, boost_rf(4)
  integer :: throw_away

  mass = rec(4)**2 - rec(1)**2 - rec(2)**2 - rec(3)**2

  if(mass < 1.0E-16_prec) then  ! rec must not be massless
    throw_away = 1
    mass = 1.0E-12_prec
  else
    mass = sqrt(mass)
  end if

  energy = rec(4)
  dot_dot = sum(rec(1:3)*mo(1:3))

  boost_rf(4) = (energy*mo(4)-dot_dot)/mass
  boost_rf(1:3) =    &
     mo(1:3) + rec(1:3)*(dot_dot/(energy+mass) - mo(4))/mass

  END FUNCTION BOOST_RF



  FUNCTION ETA(k1)   ! pseudo-rapidity

  real (kind=prec), intent(in) :: k1(4)
  real (kind=prec):: eta
  real (kind=prec) :: kv

  kv = sqrt(k1(1)**2+k1(2)**2+k1(3)**2)

  eta = 0.5*log((kv+k1(3))/(kv-k1(3)))

  END FUNCTION ETA



  FUNCTION RAP(k1)   ! rapidity

  real (kind=prec), intent(in) :: k1(4)
  real (kind=prec):: rap

  rap = 0.5*log((k1(4)+k1(3))/(k1(4)-k1(3)))

  END FUNCTION RAP



  FUNCTION PT(q1)   ! transverse momentum

  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec):: pt

  pt = sqrt(q1(1)**2 + q1(2)**2)

  END FUNCTION PT



  FUNCTION ABSVEC(q1)   ! magnitude of 3-vector

  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec):: absvec

  absvec = sqrt(q1(1)**2 + q1(2)**2 + q1(3)**2)

  END FUNCTION ABSVEC



  FUNCTION RIJ(k1,k2)

  real (kind=prec), intent(in) :: k1(4), k2(4)
  real (kind=prec) ::  rij, yij, phiij, Dres

  Dres = 0.7_prec

  yij = 0.5*log((k1(4)+k1(3))*(k2(4)-k2(3))/(k1(4)-k1(3))/(k2(4)+k2(3)))
  phiij = k1(1)*k2(1) + k1(2)*k2(2)
  phiij = phiij/sqrt(k1(1)**2+k1(2)**2)/sqrt(k2(1)**2+k2(2)**2)
  phiij = acos(phiij)

!  phiij = atan2(k1(2),k1(1)) - atan2(k2(2),k2(1))
!  if(phiij > pi) phiij = phiij - 2*pi
!  if(phiij < -pi) phiij = phiij + 2*pi

  rij = (yij**2 + phiij**2)/Dres**2

  END FUNCTION RIJ



  FUNCTION COS_TH(k1,k2)

  real (kind=prec), intent(in) :: k1(4), k2(4)
  real (kind=prec) ::  cos_th, mag1, mag2

  mag1 = sqrt(k1(1)**2 + k1(2)**2 + k1(3)**2)
  mag2 = sqrt(k2(1)**2 + k2(2)**2 + k2(3)**2)

  if ( (mag1 < zero).or.(mag2 < zero) ) then
    cos_th = 0.
    return
  endif
  cos_th = sum(k1(1:3)*k2(1:3))/mag1/mag2

  END FUNCTION COS_TH


  FUNCTION PHI(q1)   ! -pi < azimuthal angle < pi
  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec):: phi

  if(max(abs(q1(1)),abs(q1(2))) < zero) then
    phi = 100*pi    !! throw away
  else
    phi = atan2(q1(2),q1(1))
  endif

  END FUNCTION PHI



!======================  end user.f95        ======================!




  FUNCTION FUCKOFF(VAL, ARR, MSG)
  implicit none
  real(kind=prec) :: fuckoff, val, arr(:)
  character(len=*),optional :: msg
  character(len=30) :: mmsg
  character(len=10) :: slurmid

  if (runninglab) then
    fuckoff = val
    return
  endif
  if (isnan(val)) then
    fuckoff = 0.
    if (present(msg)) then
      mmsg = msg
    else
      mmsg = "no message"
    endif
    if (pcounter(2) .lt. 10000) then
      call getenv('SLURM_JOB_ID', slurmid)
      open(unit=8,action='WRITE',form="UNFORMATTED",&
           position='append',file='bad_points'//trim(slurmid)//'.bp' )
      write(8) mmsg, which_piece, flavour, xinormcut, delcut, size(arr), &
        reshape(arr, (/ 17 /), pad=[0._prec] )
      close(unit=8)
    endif
    pcounter(2) = pcounter(2)+1
  else
    fuckoff = val
    pcounter(1) = pcounter(1)+1
  endif


  END FUNCTION FUCKOFF



                            !!!!!!!!!!!!!!!!!!!!!!!!!
                              END MODULE FUNCTIONS
                            !!!!!!!!!!!!!!!!!!!!!!!!!
