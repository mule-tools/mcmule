
                          !!!!!!!!!!!!!!!!!!!!!!
                            MODULE OLINTERFACE
                          !!!!!!!!!!!!!!!!!!!!!!

  use functions
  contains

  SUBROUTINE OPENLOOPS(process, p1, p2, p3, p4, p5, p6, p7, p8, p9, reload, tree, sin, fin, hel1, hel2)
  use openloops
  implicit none
  character(len=*), intent(in) :: process
  real(kind=prec), intent(out), optional :: tree, sin, fin
  real(kind=prec), intent(in), optional, dimension(4) :: p1, p2, p3, p4, p5, p6, p7, p8, p9
  integer, intent(in), optional :: hel1, hel2
  integer :: myhel(2)
  integer nfl
  real(kind=prec) :: vecs(0:4, 9)
  logical, intent(in), optional :: reload
  logical, save :: loaded = .false.
  character(len=50) :: processed, model
  character(len=50), save :: process_saved
  integer, save :: id(-1:1,-1:1, 0:5)
  integer ido
  real(kind=prec), save :: conv
  logical, save :: muone_mixed
  real(kind=prec) :: m2_tree, m2_loop(0:2), acc
  logical :: proclib_exists
#include "../mcmule.h"

  if(present(reload)) then
    if(reload) loaded = .false.
  endif
  if (trim(process).ne.trim(process_saved)) loaded = .false.

  if(.not.loaded) then
    process_saved = trim(process)

    inquire(file=__OPENLOOPS_BUILD_DIR//"/proclib/.", exist=proclib_exists)
    if(proclib_exists) then
      call set_parameter("install_path", __OPENLOOPS_BUILD_DIR)
    else
      inquire(file=__OPENLOOPS_INSTALL_DIR//"/proclib/.", exist=proclib_exists)
      if(proclib_exists) then
        call set_parameter("install_path", __OPENLOOPS_INSTALL_DIR)
      else
        print*, "can't find OpenLoops processes"
      endif
    endif
    call set_parameter("ew_scheme", 0)         ! alpha(0) scheme
    call set_parameter("mureg", sqrt(musq))    ! set the scale
    call set_parameter("muren", sqrt(musq))    !
    call set_parameter("mass(13)", Mm)         ! set mu mass (default is zero)
    call set_parameter("mass(11)", Me)         ! set e- mass (default is zero)
    call set_parameter("qed", 2)               ! compute in QED
    call set_parameter("photon_selfenergy", 0) ! no photon selfenergies
    call set_parameter("approx", "nofermionloops")
    call set_parameter("alpha_qed",alpha)      ! set the alpha value closest to your heart

    muone_mixed = .false.
    conv = 1._prec
    Nfl = 1
    select case(process)
      case("em2emEE")
        processed = "11 13 -> 11 13"
        model = "qed_electronic"
      case("em2emgEE")
        processed = "11 13 -> 11 13 22"
        model = "qed_electronic"
      case("em2emgMM")
        processed = "11 13 -> 11 13 22"
        model = "sm"
        Nfl = 2
        call set_parameter("approx", "nofermionloops_Qmu_6")

        if(present(sin)) call crash("openloops")
        if(present(tree)) call crash("openloops")
      case("em2emgEM13")
        block
#include "../mue/charge_config.h"
          if(Qe == Qmu) then
            processed = "11 13 -> 11 13 22"
          else
            processed = "11 -13 -> 11 -13 22"
          endif
        endblock
        model = "sm"
        Nfl = 2
        call set_parameter("approx", "nofermionloops_Qmu_5")

        if(present(sin)) call crash("openloops")
        if(present(tree)) call crash("openloops")
      case("em2emgEM22")
        block
#include "../mue/charge_config.h"
          if(Qe == Qmu) then
            processed = "11 13 -> 11 13 22"
          else
            processed = "11 -13 -> 11 -13 22"
          endif
        endblock
        model = "sm"
        Nfl = 2
        call set_parameter("approx", "nofermionloops_Qmu_4")

        if(present(sin)) call crash("openloops")
        if(present(tree)) call crash("openloops")
      case("em2emgEM31")
        block
#include "../mue/charge_config.h"
          if(Qe == Qmu) then
            processed = "11 13 -> 11 13 22"
          else
            processed = "11 -13 -> 11 -13 22"
          endif
        endblock
        model = "sm"
        Nfl = 2
        call set_parameter("approx", "nofermionloops_Qmu_3")

        if(present(sin)) call crash("openloops")
        if(present(tree)) call crash("openloops")
      case("em2emgEM")
        block
#include "../mue/charge_config.h"
          if(Qe == Qmu) then
            processed = "11 13 -> 11 13 22"
          else
            processed = "11 -13 -> 11 -13 22"
          endif
        endblock
        model = "sm"
        Nfl = 2
        muone_mixed = .true.
        if(present(sin)) call crash("openloops")
        if(present(tree)) call crash("openloops")
      case("em2emggEE")
        processed = "11 13 -> 11 13 22 22"
        model = "qed_electronic"
        conv = 2._prec
      case("em2emg")
        block
#include "../mue/charge_config.h"
          if(Qe == Qmu) then
            processed = "11 13 -> 11 13 22"
          else
            processed = "11 -13 -> 11 -13 22"
          endif
        endblock
        model = "sm"
        Nfl = 2
      case("em2emgg")
        block
#include "../mue/charge_config.h"
          if(Qe == Qmu) then
            processed = "11 13 -> 11 13 22 22"
          else
            processed = "11 -13 -> 11 -13 22 22"
          endif
        endblock
        model = "sm"
        conv = 2._prec
        Nfl = 2
      case("ee2mm")
        processed = "-11(%) 11(%) -> -13 13"
        model = "sm"
        Nfl = 2
      case("ee2mmg")
        processed = "-11(%) 11(%) -> -13 13 22"
        model = "sm"
        Nfl = 2
      case("ee2mmgEE")
        processed = "-11(%) 11(%) -> -13 13 22"
        model = "qed_electronic"
      case("ee2mmggEE")
        processed = "-11(%) 11(%) -> -13 13 22 22"
        model = "qed_electronic"
      case("ee2ee")
        processed = "11 11 -> 11 11"
        model = "sm"
        conv = 2._prec
      case("eb2eb")
        processed = "11 -11 -> 11 -11"
        model = "sm"
      case("eb2ebg")
        processed = "11 -11 -> 11 -11 22"
        model = "sm"
      case("ee2eeg")
        processed = "11 11 -> 11 11 22"
        model = "sm"
        conv = 2._prec
      case("ee2eegg")
        processed = "11 11 -> 11 11 22 22"
        model = "sm"
        conv = 4._prec
      case default
        call crash("call openloops")
    end select

    call set_parameter("model", trim(model))
    call set_parameter("nf", 0)                ! no quarks
    call set_parameter("nfl", Nfl)             ! Number of internal leptons
    loaded = .true.

    if(muone_mixed) then
      call set_parameter("approx", "nofermionloops_Qmu_3")
      id(0,0,3) = register_process(trim(processed), 11)
      call set_parameter("approx", "nofermionloops_Qmu_4")
      id(0,0,4) = register_process(trim(processed), 11)
      call set_parameter("approx", "nofermionloops_Qmu_5")
      id(0,0,5) = register_process(trim(processed), 11)
    else
      call initopenloopsWithFormat(processed, id)
    endif

    call start
  endif

  vecs = 0.
  if (present(p1)) vecs(1:4, 1) = p1
  if (present(p2)) vecs(1:4, 2) = p2
  if (present(p3)) vecs(1:4, 3) = p3
  if (present(p4)) vecs(1:4, 4) = p4
  if (present(p5)) vecs(1:4, 5) = p5
  if (present(p6)) vecs(1:4, 6) = p6
  if (present(p7)) vecs(1:4, 7) = p7
  if (present(p8)) vecs(1:4, 8) = p8
  if (present(p9)) vecs(1:4, 9) = p9

  vecs(0,:) = vecs(4,:)

  myhel = 0
  if (present(hel1)) myhel(1) = hel1
  if (present(hel2)) myhel(2) = hel2

  if(present(fin) .or. present(sin)) then
    if(muone_mixed) then
      fin = 0._prec
      do ido=3,5
        call evaluate_loop(id(myhel(1),myhel(2), ido), vecs(0:3,:), m2_tree, m2_loop(0:2), acc)
        fin = fin + m2_loop(0)
      enddo
      fin = fin * conv
    else
      call evaluate_loop(id(myhel(1),myhel(2), 0), vecs(0:3,:), m2_tree, m2_loop(0:2), acc)

      if (present(tree)) tree = conv*m2_tree
      if (present(fin)) fin = conv*m2_loop(0)
      if (present(sin)) sin = conv*m2_loop(1)
    endif
  elseif(present(tree)) then
    call evaluate_tree(id(myhel(1),myhel(2), 0), vecs(0:3,:), m2_tree)
    tree = conv*m2_tree
  endif

  END SUBROUTINE



  SUBROUTINE INITOPENLOOPSWITHFORMAT(FORMATSTRING, OUTPUT)
  use openloops
  character(len=50), intent(in) :: formatstring
  character(len=54) :: buf
  character(len=2), dimension(-1:1), parameter :: map = (/"-1", "0 ", "1 "/)
  integer, intent(out), dimension(-1:1, -1:1, 0:5) :: output
  integer i, j, i1, i2

  i1 = index(formatstring, "%", back=.false.)
  i2 = index(formatstring, "%", back=.true.)

  output = -1

  if(i1.eq.0) then
    ! no format present, just use normal string
    output(0,0,0) = register_process(trim(formatstring), 11)
  elseif(i1 .eq. i2) then
    ! only one format string
    do i=-1,1
      buf = formatstring(1:i1-1)//trim(map(i))//formatstring(i1+1:)
      output(i, 0,0) = register_process(trim(buf), 11)
    enddo
  elseif(i1 .lt. i2) then
    ! only one format string
    do i=-1,1
      do j=-1,1
        buf = formatstring(1:i1-1)//trim(map(i))//formatstring(i1+1:i2-1)//trim(map(j))//formatstring(i2+1:)
        output(i, j,0) = register_process(trim(buf), 11)
      enddo
    enddo
  endif

  END SUBROUTINE

                          !!!!!!!!!!!!!!!!!!!!!!
                          END MODULE OLINTERFACE
                          !!!!!!!!!!!!!!!!!!!!!!
