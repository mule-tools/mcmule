                          !!!!!!!!!!!!!!!!!!!!!!
                           MODULE mue_PEPEZMML
                          !!!!!!!!!!!!!!!!!!!!!!
    use functions
    implicit none
    real(kind=prec), parameter :: MH = 125e3


  contains
  PURE FUNCTION COEFFLL(Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm, &
        discu, disct, discse, discsm, c6se, c6sm, c6u, c6t, ce, cm, lmm, lmu, ls, &
        disctLs, discuLs, pol)
  real(kind=prec), intent(in) :: Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm
  real(kind=prec), intent(in) :: discu, disct, discse, discsm, c6se, c6sm, c6u, c6t, ce, cm, lmm, lmu, ls
  real(kind=prec), intent(in) :: disctLs, discuLs
  real(kind=prec) :: coeff_c6se, coeff_c6sm, coeff_c6t, coeff_c6u, coeff_ce
  real(kind=prec) :: coeff_cm, coeff_discse, coeff_discse_lmm, coeff_discse_lmu, coeff_discsm
  real(kind=prec) :: coeff_discsm_lmu, coeff_disct, coeff_disct_lmm, coeff_disct_lmu, coeff_disct_ls
  real(kind=prec) :: coeff_discu, coeff_discu_lmm, coeff_discu_lmu, coeff_discu_ls, coeff_lmm
  real(kind=prec) :: coeff_lmu, coeff_ls, coeff_unity
  logical, intent(in) :: pol
  real(kind=prec) :: coeffLL

  coeffLL = 0.
  coeff_c6se = ((2*me2 - ss)*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2 - Mm2*(s&
               &s + 2*tt)))/ss
  coeff_c6sm = ((2*Mm2 - ss)*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2 - Mm2*(s&
               &s + 2*tt)))/ss
  coeff_c6t = (2*me2**3 + (Mm2 - tt)*(2*Mm2**2 - 3*Mm2*ss + 3*ss**2 - 4*Mm2*tt + 6*ss*tt + 2*t&
              &t**2) + me2**2*(6*Mm2 - 3*(ss + 2*tt)) + me2*(6*Mm2**2 - 2*Mm2*(5*ss + 6*tt) + 3&
              &*(ss**2 + 3*ss*tt + 2*tt**2)))/(2.*ss)
  coeff_c6u = (2*me2**3 + 2*Mm2**3 - 2*(ss + tt)**3 - 3*Mm2**2*(ss + 2*tt) + 3*Mm2*(ss**2 + 3*&
              &ss*tt + 2*tt**2) + me2**2*(6*Mm2 - 3*(ss + 2*tt)) + me2*(6*Mm2**2 - 2*Mm2*(5*ss &
              &+ 6*tt) + 3*(ss**2 + 3*ss*tt + 2*tt**2)))/(2.*ss)
  coeff_ce = (-16*me2**3 - 16*me2**2*(Mm2 - 2*ss - tt) + ss**2*(-2*Mm2 + ss + 2*tt) - 4*me2*s&
             &s*(-4*Mm2 + 3*ss + 4*tt))/(16*me2 - 4*ss)
  coeff_cm = (-16*Mm2**3 - 2*me2*(8*Mm2**2 - 8*Mm2*ss + ss**2) + 16*Mm2**2*(2*ss + tt) + ss**&
             &2*(ss + 2*tt) - 4*Mm2*ss*(3*ss + 4*tt))/(16*Mm2 - 4*ss)
  coeff_discse = -(1./6.)*(16*me2**4 + 4*me2**3*(8*Mm2 + 7*ss - 8*tt) + ss**2*(-11*Mm&
                 &2**2 - 9*Mm2*ss + 2*ss**2 + 22*Mm2*tt - 2*ss*tt - 11*tt**2) + me2**2*(16*Mm2**2 &
                 &+ 9*ss**2 + 8*Mm2*(7*ss - 4*tt) - 40*ss*tt + 16*tt**2) + me2*ss*(28*Mm2**2 - ss*&
                 &*2 + 2*Mm2*(ss - 28*tt) + 38*ss*tt + 28*tt**2))/((4*me2 - ss)*ss**2)
  coeff_discse_lmm = ((2*me2 - ss)*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2 - Mm2*(s&
                     &s + 2*tt)))/((4*me2 - ss)*ss)
  coeff_discse_lmu = -(((2*me2 - ss)*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2 - Mm2*&
                     &(ss + 2*tt)))/((4*me2 - ss)*ss))
  coeff_discsm = -(1./6.)*(16*Mm2**4 + me2**2*(16*Mm2**2 + 28*Mm2*ss - 11*ss**2) + 4*&
                 &Mm2**3*(7*ss - 8*tt) + ss**2*(2*ss**2 - 2*ss*tt - 11*tt**2) + Mm2**2*(9*ss**2 - &
                 &40*ss*tt + 16*tt**2) + Mm2*ss*(-ss**2 + 38*ss*tt + 28*tt**2) + me2*(32*Mm2**3 + &
                 &2*Mm2*ss*(ss - 28*tt) + 8*Mm2**2*(7*ss - 4*tt) + ss**2*(-9*ss + 22*tt)))/((4*Mm2&
                 & - ss)*ss**2)
  coeff_discsm_lmu = -(((2*Mm2 - ss)*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2 - Mm2*&
                     &(ss + 2*tt)))/((4*Mm2 - ss)*ss))
  coeff_disct = -(1./2.)*(me2**5 + me2**4*(-3*Mm2 + ss - 3*tt) + me2**3*(2*Mm2**2 - 4*Mm2*ss + 4*Mm2&
                &*tt - 3*ss*tt) + (Mm2 - tt)*(Mm2**4 + Mm2**3*(ss - 2*tt) - 2*Mm2**2*tt*(ss + tt)&
                & - Mm2*tt*(ss**2 + 6*ss*tt - 6*tt**2) + tt**2*(4*ss**2 + 5*ss*tt - 3*tt**2)) + m&
                &e2*(-3*Mm2**4 - 4*Mm2**3*(ss - tt) + 3*Mm2**2*ss*tt + tt**2*(5*ss**2 + 11*ss*tt &
                &- 9*tt**2) + 2*Mm2*tt*(ss**2 - 16*ss*tt + 4*tt**2)) + me2**2*(2*Mm2**3 + Mm2**2*&
                &(6*ss - 2*tt) + 3*Mm2*ss*tt + tt*(-ss**2 - 4*ss*tt + 8*tt**2)))/(ss*(-4*me2*Mm2 &
                &+ (me2 + Mm2 - tt)**2)*tt)
  coeff_disct_lmm = (tt*(-6*me2**3 + me2**2*(-18*Mm2 + 5*ss + 18*tt) - (Mm2 - tt)*(6*Mm2**2 - 5*Mm2*&
                    &ss + 5*ss**2 - 12*Mm2*tt + 10*ss*tt + 6*tt**2) - me2*(18*Mm2**2 + 5*ss**2 + 15*s&
                    &s*tt + 18*tt**2 - 6*Mm2*(ss + 6*tt))))/(4.*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2)&
                    &)
  coeff_disct_lmu = (2*(me2 + Mm2 - tt)*tt*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2&
                    & - Mm2*(ss + 2*tt)))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
  coeff_disct_ls = (tt*(2*me2**3 + me2**2*(6*Mm2 - ss - 6*tt) + me2*(6*Mm2**2 + ss**2 + 2*Mm2*(ss -&
                   & 6*tt) + 3*ss*tt + 6*tt**2) + (Mm2 - tt)*(2*Mm2**2 + ss**2 + 2*ss*tt + 2*tt**2 -&
                   & Mm2*(ss + 4*tt))))/(2.*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
  coeff_discu = (2*me2**4 + 2*Mm2**4 + me2**3*(19*ss - 6*tt) + Mm2**3*(19*ss - 6*tt) - 4*ss*(ss &
                &+ tt)**2*(2*ss + 3*tt) - me2**2*(4*Mm2**2 - 93*Mm2*ss + 46*ss**2 + 10*Mm2*tt + 4&
                &8*ss*tt - 6*tt**2) + Mm2**2*(-46*ss**2 - 48*ss*tt + 6*tt**2) + Mm2*(33*ss**3 + 7&
                &6*ss**2*tt + 41*ss*tt**2 - 2*tt**3) + me2*(33*ss**3 + Mm2**2*(93*ss - 10*tt) + 7&
                &6*ss**2*tt + 41*ss*tt**2 - 2*tt**3 - 4*Mm2*(25*ss**2 + 26*ss*tt - 3*tt**2)))/(2.&
                &*ss*(me2**2 + (-Mm2 + ss + tt)**2 - 2*me2*(Mm2 + ss + tt)))
  coeff_discu_lmm = -(1./4.)*((-2*me2 - 2*Mm2 + ss + tt)*(-6*me2**3 - 6*Mm2**3 + 6*(ss + tt)**3 + Mm2**&
                    &2*(13*ss + 18*tt) + me2**2*(-18*Mm2 + 13*ss + 18*tt) - Mm2*(13*ss**2 + 31*ss*tt &
                    &+ 18*tt**2) - me2*(18*Mm2**2 - 22*Mm2*ss + 13*ss**2 - 36*Mm2*tt + 31*ss*tt + 18*&
                    &tt**2)))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2))
  coeff_discu_lmu = (2*(-2*me2 - 2*Mm2 + ss + tt)*(-me2 - Mm2 + ss + tt)*(me2**2 + Mm2**2 + me2*(2*M&
                    &m2 - ss - 2*tt) + (ss + tt)**2 - Mm2*(ss + 2*tt)))/(ss*(-4*me2*Mm2 + (me2 + Mm2 &
                    &- ss - tt)**2))
  coeff_discu_ls = ((-2*me2 - 2*Mm2 + ss + tt)*(-2*me2**3 - 2*Mm2**3 + 2*(ss + tt)**3 + Mm2**2*(5*s&
                   &s + 6*tt) + me2**2*(-6*Mm2 + 5*ss + 6*tt) - Mm2*(5*ss**2 + 11*ss*tt + 6*tt**2) -&
                   & me2*(6*Mm2**2 + 5*ss**2 + 11*ss*tt + 6*tt**2 - 6*Mm2*(ss + 2*tt))))/(2.*ss*(-4*&
                   &me2*Mm2 + (me2 + Mm2 - ss - tt)**2))
  coeff_lmm = -(1./12.)*(-24*Mm2**6 + 6*me2**5*(4*Mm2 - ss) - 6*Mm2**5*(ss - 10*tt)&
              & - 3*me2**4*(4*Mm2 - ss)*(2*Mm2 - ss + 5*tt) - me2**3*(4*Mm2 - ss)*(12*Mm2**2 + &
              &3*ss**2 + 6*Mm2*(ss - tt) + 9*ss*tt - 32*tt**2) + Mm2**4*(15*ss**2 + 21*ss*tt - &
              &16*tt**2) + ss*tt**2*(2*ss**3 + 9*ss**2*tt + 14*ss*tt**2 + 7*tt**3) - Mm2**3*(3*&
              &ss**3 + 9*ss**2*tt + 96*ss*tt**2 + 80*tt**3) + Mm2**2*tt*(-12*ss**3 + 53*ss**2*t&
              &t + 168*ss*tt**2 + 88*tt**3) - Mm2*tt*(-3*ss**4 + 18*ss**3*tt + 91*ss**2*tt**2 +&
              & 102*ss*tt**3 + 28*tt**4) + me2*(24*Mm2**5 + 6*Mm2**4*(3*ss - 4*tt) - 6*Mm2**3*(&
              &7*ss**2 - 7*ss*tt - 24*tt**2) + Mm2**2*(9*ss**3 - 9*ss**2*tt - 188*ss*tt**2 - 28&
              &0*tt**3) - ss*tt*(3*ss**3 + 10*ss**2*tt + 37*ss*tt**2 + 34*tt**3) + 2*Mm2*tt*(6*&
              &ss**3 + 51*ss**2*tt + 145*ss*tt**2 + 68*tt**3)) + me2**2*(48*Mm2**4 - 12*Mm2**3*&
              &ss + ss*tt**2*(ss + 50*tt) + 12*Mm2**2*(3*ss**2 - 3*ss*tt + 16*tt**2) - Mm2*(9*s&
              &s**3 - 9*ss**2*tt + 100*ss*tt**2 + 200*tt**3)))/((4*Mm2 - ss)*ss*tt**2*(-2*me2 -&
              & 2*Mm2 + ss + tt))
  coeff_lmu = -(1./6.)*(7*me2**2 + 14*me2*Mm2 + 7*Mm2**2 - 3*me2*ss - 3*Mm2*ss + 5&
              &*ss**2 - 14*me2*tt - 14*Mm2*tt + 10*ss*tt + 7*tt**2)/ss
  coeff_ls = -(1./2.)*(16*me2**2*Mm2 + ss**2*(-3*Mm2 + ss + tt) + me2*(16*Mm2**2 - 3*ss**2 - 16*M&
             &m2*tt))/((4*me2 - ss)*(-4*Mm2 + ss))
  coeff_unity = (-3*me2**3*(3*ss + 8*tt) - 3*Mm2**3*(3*ss + 8*tt) + Mm2*tt*(24*ss**2 - 143*ss*tt&
                & - 24*tt**2) + Mm2**2*(-9*ss**2 + 73*ss*tt + 48*tt**2) + me2**2*(-9*ss**2 + 9*Mm&
                &2*(ss - 8*tt) + 73*ss*tt + 48*tt**2) + ss*tt*(-25*ss**2 + ss*tt + 55*tt**2) + me&
                &2*(9*Mm2**2*(ss - 8*tt) + tt*(24*ss**2 - 143*ss*tt - 24*tt**2) + 2*Mm2*(9*ss**2 &
                &+ 91*ss*tt + 48*tt**2)))/(18.*ss**2*tt)
  coeffLL = 2*(c6se*coeff_c6se + c6sm*coeff_c6sm + c6t*coeff_c6t  &
          + c6u*coeff_c6u + ce*coeff_ce + cm*coeff_cm  &
          + coeff_discse*discse + coeff_discse_lmm*discse*lmm + coeff_discse_lmu*discse*lmu  &
          + coeff_discsm*discsm + coeff_discsm_lmu*discsm*lmu + coeff_disct*disct  &
          + coeff_disct_lmm*disct*lmm + coeff_disct_lmu*disct*lmu + coeff_disct_ls*disctLs  &
          + coeff_discu*discu + coeff_discu_lmm*discu*lmm + coeff_discu_lmu*discu*lmu  &
          + coeff_discu_ls*discuLs + coeff_lmm*lmm + coeff_lmu*lmu  &
          + coeff_ls*ls + coeff_unity)

  if(pol) then
    coeff_c6se = -(((2*me2 - ss)*(me2**2*snm + Mm2**2*snm - s1mm*ss - 2*s2nm*ss + 3*s3mm*ss + 3*s&
                 &3nm*ss - s1mm*tt - s2nm*tt + 2*s3mm*tt + 2*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2&
                 &*(3*s1mm - s2nm - 2*(s3mm + s3nm + snm*tt)) + me2*(s1mm + s2nm - 2*(s3mm + s3nm &
                 &- Mm2*snm + snm*tt))))/ss)
    coeff_c6sm = -(((2*Mm2 - ss)*(me2**2*snm + Mm2**2*snm - s1mm*ss - 2*s2nm*ss + 3*s3mm*ss + 3*s&
                 &3nm*ss - s1mm*tt - s2nm*tt + 2*s3mm*tt + 2*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2&
                 &*(3*s1mm - s2nm - 2*(s3mm + s3nm + snm*tt)) + me2*(s1mm + s2nm - 2*(s3mm + s3nm &
                 &- Mm2*snm + snm*tt))))/ss)
    coeff_c6t = -(1./2.)*(2*me2**5*snm + 2*Mm2**5*snm + Mm2**4*(6*s1mm - 2*s2nm - 4*s3mm - 4*s3nm + &
                &snm*ss - 10*snm*tt) + me2**4*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 2*Mm2*snm + sn&
                &m*ss - 10*snm*tt) - tt**2*(2*tt**2*(-s1mm - s2nm + 2*s3mm + 2*s3nm + snm*tt) + s&
                &s**2*(-3*s1mm - 6*s2nm + 9*s3mm + 9*s3nm + 2*snm*tt) + ss*tt*(-5*s1mm - 9*s2nm +&
                & 14*s3mm + 14*s3nm + 4*snm*tt)) + me2**3*(-4*Mm2**2*snm - snm*ss**2 + 4*Mm2*(s1m&
                &m - s2nm - 4*snm*tt) + ss*(-2*s1mm - 6*s2nm + 8*s3mm + 8*s3nm + snm*tt) + 4*tt*(&
                &-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 5*snm*tt)) + Mm2**3*(-(snm*ss**2) + ss*(-8*&
                &s2nm + 8*s3mm + 8*s3nm + snm*tt) + 4*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm + 5*sn&
                &m*tt)) + Mm2**2*(ss**2*(-2*s1mm + 2*s3mm + 2*s3nm + 3*snm*tt) - 3*ss*tt*(-3*s1mm&
                & - 7*s2nm + 10*s3mm + 10*s3nm + 3*snm*tt) - 4*tt**2*(-6*s1mm + 6*s3mm + 6*s3nm +&
                & 5*snm*tt)) + Mm2*tt*((s1mm - 6*s2nm + 5*(s3mm + s3nm))*ss**2 + 2*tt**2*(-6*s1mm&
                & - 2*s2nm + 8*s3mm + 8*s3nm + 5*snm*tt) + ss*tt*(-14*s1mm - 22*s2nm + 36*s3mm + &
                &36*s3nm + 11*snm*tt)) + me2**2*(-4*Mm2**3*snm - 2*Mm2**2*(4*s1mm - 4*s3mm - 4*s3&
                &nm + snm*ss + 6*snm*tt) + tt*(3*snm*ss**2 + 3*ss*(3*s1mm + 7*s2nm - 10*s3mm - 10&
                &*s3nm - 3*snm*tt) - 4*tt*(-3*s1mm - 3*s2nm + 6*s3mm + 6*s3nm + 5*snm*tt)) + Mm2*&
                &(snm*ss**2 + ss*(4*s1mm + 4*s2nm - 8*s3mm - 8*s3nm - snm*tt) + 4*tt*(-5*s1mm + s&
                &2nm + 4*s3mm + 4*s3nm + 9*snm*tt))) + me2*(2*Mm2**4*snm - 4*Mm2**3*(s1mm - s2nm &
                &+ 4*snm*tt) + Mm2**2*(snm*ss**2 - ss*(2*s1mm - 10*s2nm + 8*s3mm + 8*s3nm + snm*t&
                &t) + 4*tt*(-4*s1mm + 4*s3mm + 4*s3nm + 9*snm*tt)) + tt*(-3*(s1mm + 2*s2nm - 3*(s&
                &3mm + s3nm))*ss**2 + 2*tt**2*(-4*s1mm - 4*s2nm + 8*s3mm + 8*s3nm + 5*snm*tt) + s&
                &s*tt*(-12*s1mm - 24*s2nm + 36*s3mm + 36*s3nm + 11*snm*tt)) + 2*Mm2*(ss**2*(s1mm &
                &- s3mm - s3nm + snm*tt) + 2*tt**2*(7*s1mm + s2nm - 8*(s3mm + s3nm + snm*tt)) + s&
                &s*tt*(3*s1mm + 7*s2nm - 5*(2*s3mm + 2*s3nm + snm*tt)))))/(ss*(me2**2 + Mm2**2 - &
                &2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_c6u = -(1./2.)*(2*me2**5*snm + 2*Mm2**5*snm + Mm2**4*(6*s1mm - 2*s2nm - 4*s3mm - 4*s3nm + &
                &snm*ss - 10*snm*tt) + me2**4*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 2*Mm2*snm + sn&
                &m*ss - 10*snm*tt) + (ss + tt)**2*(snm*ss**3 + (s1mm + 5*s2nm - 6*(s3mm + s3nm))*&
                &ss*tt + 2*tt**2*(s1mm + s2nm - 2*s3mm - 2*s3nm - snm*tt) + ss**2*(-s1mm + s3mm +&
                & s3nm + 3*snm*tt)) + me2**3*(-4*Mm2**2*snm - 7*snm*ss**2 + 4*Mm2*(s1mm - s2nm - &
                &4*snm*tt) + ss*(-2*s1mm - 6*s2nm + 8*s3mm + 8*s3nm + snm*tt) + 4*tt*(-2*s1mm - 2&
                &*s2nm + 4*s3mm + 4*s3nm + 5*snm*tt)) + Mm2**3*(-7*snm*ss**2 + ss*(-6*s1mm - 2*s2&
                &nm + 8*s3mm + 8*s3nm + snm*tt) + 4*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm + 5*snm*&
                &tt)) - Mm2*(ss + tt)*(4*snm*ss**3 - ss*tt*(-8*s1mm - 12*s2nm + 20*s3mm + 20*s3nm&
                & + snm*tt) - 2*tt**2*(-6*s1mm - 2*s2nm + 8*s3mm + 8*s3nm + 5*snm*tt) + ss**2*(-4&
                &*s1mm + s2nm + 3*s3mm + 3*s3nm + 13*snm*tt)) + Mm2**2*(7*snm*ss**3 - 3*ss*tt*(-7&
                &*s1mm - 3*s2nm + 10*s3mm + 10*s3nm + 3*snm*tt) - 4*tt**2*(-6*s1mm + 6*s3mm + 6*s&
                &3nm + 5*snm*tt) + ss**2*(-3*s1mm + 7*s2nm - 4*s3mm - 4*s3nm + 18*snm*tt)) - me2*&
                &*2*(4*Mm2**3*snm - 7*snm*ss**3 + 3*ss**2*(s1mm - s2nm - 6*snm*tt) + 3*ss*tt*(-3*&
                &s1mm - 7*s2nm + 10*s3mm + 10*s3nm + 3*snm*tt) + 4*tt**2*(-3*s1mm - 3*s2nm + 6*s3&
                &mm + 6*s3nm + 5*snm*tt) + 2*Mm2**2*(4*s1mm - 4*s3mm - 4*s3nm + snm*ss + 6*snm*tt&
                &) + Mm2*(snm*ss**2 + ss*(2*s1mm - 10*s2nm + 8*s3mm + 8*s3nm + snm*tt) - 4*tt*(-5&
                &*s1mm + s2nm + 4*s3mm + 4*s3nm + 9*snm*tt))) + me2*(2*Mm2**4*snm - 4*Mm2**3*(s1m&
                &m - s2nm + 4*snm*tt) - Mm2**2*(snm*ss**2 + ss*(-10*s1mm + 2*s2nm + 8*s3mm + 8*s3&
                &nm + snm*tt) - 4*tt*(-4*s1mm + 4*s3mm + 4*s3nm + 9*snm*tt)) - (ss + tt)*(4*snm*s&
                &s**3 - ss*tt*(-4*s1mm - 16*s2nm + 20*s3mm + 20*s3nm + snm*tt) - 2*tt**2*(-4*s1mm&
                & - 4*s2nm + 8*s3mm + 8*s3nm + 5*snm*tt) + ss**2*(-4*s1mm - s2nm + 5*s3mm + 5*s3n&
                &m + 13*snm*tt)) + 2*Mm2*(3*snm*ss**3 + 2*tt**2*(7*s1mm + s2nm - 8*(s3mm + s3nm +&
                & snm*tt)) + ss*tt*(9*s1mm + s2nm - 5*(2*s3mm + 2*s3nm + snm*tt)) + ss**2*(-5*s1m&
                &m - 5*s2nm + 2*(5*s3mm + 5*s3nm + 7*snm*tt)))))/(ss*(me2**2 + Mm2**2 - 2*Mm2*tt &
                &- 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_ce = (16*me2**5*snm + me2**2*(16*Mm2**3*snm - 29*snm*ss**3 + 2*ss**2*(14*s1mm + 4*s2n&
               &m - 18*s3mm - 18*s3nm - 53*snm*tt) - 8*tt**2*(s1mm - 5*s2nm + 4*s3mm + 4*s3nm + &
               &2*snm*tt) + 8*Mm2**2*(3*s1mm + s2nm - 2*(2*s3mm + 2*s3nm + snm*ss + 3*snm*tt)) -&
               & 8*ss*tt*(-3*s1mm - 7*s2nm + 2*(5*s3mm + 5*s3nm + 6*snm*tt)) + 2*Mm2*(13*snm*ss*&
               &*2 + 8*tt*(-s1mm - 3*s2nm + 4*s3mm + 4*s3nm + 3*snm*tt) + 8*ss*(3*s2nm - 3*s3mm &
               &- 3*s3nm + 7*snm*tt))) - ss**2*(-2*Mm2**3*snm + Mm2**2*(-2*s1mm - 2*s2nm + 4*s3m&
               &m + 4*s3nm + 3*snm*ss + 6*snm*tt) + (ss + 2*tt)*(snm*ss**2 + tt*(-s1mm - s2nm + &
               &2*s3mm + 2*s3nm + snm*tt) + ss*(-s1mm + s3mm + s3nm + 2*snm*tt)) - Mm2*(3*snm*ss&
               &**2 + 2*tt*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 3*snm*tt) + ss*(-s1mm + s2nm + &
               &8*snm*tt))) - 4*me2**3*(4*Mm2**2*snm - 12*snm*ss**2 + 2*tt*(s1mm + 7*s2nm - 8*s3&
               &mm - 8*s3nm - 6*snm*tt) + 2*Mm2*(3*s1mm + 5*s2nm - 8*s3mm - 8*s3nm - 2*snm*ss + &
               &4*snm*tt) + ss*(7*s1mm + 7*s2nm - 2*(7*s3mm + 7*s3nm + 16*snm*tt))) + 8*me2**4*(&
               &s1mm + 3*s2nm - 2*(2*s3mm + 2*s3nm + snm*(Mm2 + 3*(ss + tt)))) + me2*(9*snm*ss**&
               &4 - 8*Mm2**3*(s1mm - s2nm + 2*snm*ss) + 8*(s1mm - s2nm)*tt**3 + 4*ss*tt**2*(s1mm&
               & - 7*s2nm + 6*s3mm + 6*s3nm + 4*snm*tt) + 2*ss**2*tt*(-7*s1mm - 9*s2nm + 16*s3mm&
               & + 16*s3nm + 22*snm*tt) + ss**3*(-9*s1mm - s2nm + 10*s3mm + 10*s3nm + 36*snm*tt)&
               & - 2*Mm2*(10*snm*ss**3 + 12*(s1mm - s2nm)*tt**2 + 24*ss*tt*(-s2nm + s3mm + s3nm &
               &+ snm*tt) - ss**2*(s1mm - 5*s2nm + 4*(s3mm + s3nm - 8*snm*tt))) + 4*Mm2**2*(5*sn&
               &m*ss**2 + 6*(s1mm - s2nm)*tt + ss*(-s1mm - 5*s2nm + 6*(s3mm + s3nm + 2*snm*tt)))&
               &))/(4.*(4*me2 - ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt&
               &)))
    coeff_cm = (16*Mm2**5*snm + 2*me2**3*snm*(8*Mm2**2 - 8*Mm2*ss + ss**2) + 16*Mm2**4*(s1mm + &
               &s2nm - 2*s3mm - 2*s3nm - 3*snm*ss - 3*snm*tt) - ss**2*(ss + 2*tt)*(snm*ss**2 + t&
               &t*(-s1mm - s2nm + 2*s3mm + 2*s3nm + snm*tt) + ss*(-s1mm + s3mm + s3nm + 2*snm*tt&
               &)) - me2**2*(16*Mm2**3*snm - 16*Mm2**2*(s1mm + s2nm - 2*s3mm - 2*s3nm - snm*ss -&
               & 3*snm*tt) + ss**2*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 3*snm*ss + 6*snm*tt) - &
               &4*Mm2*ss*(-4*s1mm - 4*s2nm + 8*s3mm + 8*s3nm + 5*snm*ss + 12*snm*tt)) + 8*Mm2**3&
               &*(6*snm*ss**2 + 2*tt*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 3*snm*tt) + ss*(-7*s1&
               &mm + s2nm + 6*s3mm + 6*s3nm + 16*snm*tt)) + Mm2*ss*(9*snm*ss**3 - 16*tt**2*(s1mm&
               & + s2nm - 2*s3mm - 2*s3nm - snm*tt) + ss**2*(-11*s1mm + s2nm + 10*s3mm + 10*s3nm&
               & + 36*snm*tt) + 2*ss*tt*(-15*s1mm - 7*s2nm + 22*(s3mm + s3nm + snm*tt))) - Mm2**&
               &2*(29*snm*ss**3 - 16*tt**2*(s1mm + s2nm - 2*s3mm - 2*s3nm - snm*tt) + ss**2*(-40&
               &*s1mm + 8*s2nm + 32*s3mm + 32*s3nm + 106*snm*tt) + 8*ss*tt*(-9*s1mm - 5*s2nm + 2&
               &*(7*s3mm + 7*s3nm + 6*snm*tt))) + me2*(-16*Mm2**4*snm - 16*Mm2**3*(2*s1mm + 2*s2&
               &nm - 4*s3mm - 4*s3nm - snm*ss + 2*snm*tt) - 2*Mm2*ss*(10*snm*ss**2 + 8*tt*(-2*s1&
               &mm - 2*s2nm + 4*s3mm + 4*s3nm + 3*snm*tt) + ss*(-11*s1mm - 3*s2nm + 14*s3mm + 14&
               &*s3nm + 32*snm*tt)) + ss**2*(3*snm*ss**2 + 2*tt*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s&
               &3nm + 3*snm*tt) + ss*(-3*s1mm - s2nm + 4*(s3mm + s3nm + 2*snm*tt))) + 2*Mm2**2*(&
               &13*snm*ss**2 + 8*tt*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 3*snm*tt) + 4*ss*(-3*s&
               &1mm + s2nm + 2*(s3mm + s3nm + 7*snm*tt)))))/(4.*(4*Mm2 - ss)*(me2**2 + Mm2**2 - &
               &2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_discse = (32*me2**4*snm + 8*me2**3*(4*s1mm + 4*s2nm - 8*s3mm - 8*s3nm + 8*Mm2*snm + 7*snm&
                   &*ss - 8*snm*tt) + ss*(6*snm*ss**3 + Mm2**2*(-6*s1mm + 6*s2nm - 22*snm*ss) + 12*M&
                   &m2*(s1mm - s2nm)*tt + 6*(-s1mm + s2nm)*tt**2 + ss*tt*(7*s1mm + 31*s2nm - 38*s3mm&
                   & - 38*s3nm - 22*snm*tt) + ss**2*(-15*s1mm - 8*s2nm + 23*s3mm + 23*s3nm - 22*snm*&
                   &tt) + Mm2*ss*(-51*s1mm + 13*s2nm + 38*s3mm + 38*s3nm + 44*snm*tt)) + 2*me2**2*(1&
                   &6*Mm2**2*snm - 11*snm*ss**2 + 8*Mm2*(6*s1mm - 2*s2nm - 4*s3mm - 4*s3nm + 7*snm*s&
                   &s - 4*snm*tt) - 16*tt*(s1mm + s2nm - 2*s3mm - 2*s3nm - snm*tt) + ss*(19*s1mm + 9&
                   &*s2nm - 4*(7*s3mm + 7*s3nm + 10*snm*tt))) - me2*ss*(-56*Mm2**2*snm + 12*snm*ss**&
                   &2 - 8*tt*(-4*s1mm - 7*s2nm + 11*s3mm + 11*s3nm + 7*snm*tt) + 4*Mm2*(-36*s1mm + 1&
                   &4*s2nm + 22*s3mm + 22*s3nm + 5*snm*ss + 28*snm*tt) + ss*(13*s1mm + 45*s2nm - 2*(&
                   &29*s3mm + 29*s3nm + 50*snm*tt))))/(12.*(4*me2 - ss)*ss**2)
    coeff_discse_lmm = -(((2*me2 - ss)*(me2**2*snm + Mm2**2*snm - s1mm*ss - 2*s2nm*ss + 3*s3mm*ss + 3*s&
                       &3nm*ss - s1mm*tt - s2nm*tt + 2*s3mm*tt + 2*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2&
                       &*(3*s1mm - s2nm - 2*(s3mm + s3nm + snm*tt)) + me2*(s1mm + s2nm - 2*(s3mm + s3nm &
                       &- Mm2*snm + snm*tt))))/((4*me2 - ss)*ss))
    coeff_discse_lmu = ((2*me2 - ss)*(me2**2*snm + Mm2**2*snm - s1mm*ss - 2*s2nm*ss + 3*s3mm*ss + 3*s3n&
                       &m*ss - s1mm*tt - s2nm*tt + 2*s3mm*tt + 2*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2*(&
                       &3*s1mm - s2nm - 2*(s3mm + s3nm + snm*tt)) + me2*(s1mm + s2nm - 2*(s3mm + s3nm - &
                       &Mm2*snm + snm*tt))))/((4*me2 - ss)*ss)
    coeff_discsm = (16*Mm2**4*snm + me2**2*snm*(16*Mm2**2 + 28*Mm2*ss - 11*ss**2) + 4*Mm2**3*snm*(7&
                   &*ss - 8*tt) - Mm2**2*(11*snm*ss**2 - 16*snm*tt**2 - 8*ss*(7*s1mm - 5*s2nm - 2*s3&
                   &mm - 2*s3nm - 5*snm*tt)) + ss**2*(3*snm*ss**2 + tt*(9*s1mm + 9*s2nm - 18*s3mm - &
                   &18*s3nm - 11*snm*tt) - ss*(s1mm + 4*s2nm - 5*s3mm - 5*s3nm + 11*snm*tt)) + me2*(&
                   &32*Mm2**3*snm + 8*Mm2**2*snm*(7*ss - 4*tt) + ss**2*(-9*s1mm - 9*s2nm + 18*s3mm +&
                   & 18*s3nm + 22*snm*tt) - 2*Mm2*ss*(-12*s1mm - 12*s2nm + 24*s3mm + 24*s3nm + 5*snm&
                   &*ss + 28*snm*tt)) + Mm2*ss*(-6*snm*ss**2 + 4*tt*(-6*s1mm - 6*s2nm + 12*s3mm + 12&
                   &*s3nm + 7*snm*tt) + ss*(-19*s1mm - 7*s2nm + 26*s3mm + 26*s3nm + 50*snm*tt)))/(6.&
                   &*(4*Mm2 - ss)*ss**2)
    coeff_discsm_lmu = ((2*Mm2 - ss)*(me2**2*snm + Mm2**2*snm - s1mm*ss - 2*s2nm*ss + 3*s3mm*ss + 3*s3n&
                       &m*ss - s1mm*tt - s2nm*tt + 2*s3mm*tt + 2*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2*(&
                       &3*s1mm - s2nm - 2*(s3mm + s3nm + snm*tt)) + me2*(s1mm + s2nm - 2*(s3mm + s3nm - &
                       &Mm2*snm + snm*tt))))/((4*Mm2 - ss)*ss)
    coeff_disct = (2*me2**5*snm + 2*Mm2**5*snm + 2*Mm2**3*(s1mm - s3mm - s3nm)*(ss - 4*tt) - 2*me2&
                  &**4*(s1mm + s2nm - 2*s3mm - 2*s3nm + 3*Mm2*snm + 3*snm*tt) + 2*me2**3*(2*Mm2**2*&
                  &snm + (-s1mm + s3mm + s3nm)*ss + 4*(s1mm + s2nm - 2*(s3mm + s3nm))*tt + 4*Mm2*(s&
                  &2nm - s3mm - s3nm + snm*tt)) + Mm2**4*(6*s1mm - 2*(s2nm + 2*s3mm + 2*s3nm + 3*sn&
                  &m*tt)) + Mm2*tt*(2*ss*tt*(s1mm - 10*s2nm + 9*s3mm + 9*s3nm - 4*snm*tt) - 6*tt**2&
                  &*(-4*s1mm + 4*s3mm + 4*s3nm + 3*snm*tt) + ss**2*(-3*s1mm + 3*s3mm + 3*s3nm + 4*s&
                  &nm*tt)) + tt**2*(ss**2*(-3*s1mm + 3*s3mm + 3*s3nm - 4*snm*tt) + 6*tt**2*(-s1mm -&
                  & s2nm + 2*s3mm + 2*s3nm + snm*tt) + ss*tt*(7*s1mm + 19*s2nm - 26*s3mm - 26*s3nm &
                  &+ 6*snm*tt)) + me2**2*(4*Mm2**3*snm + 6*Mm2*(s1mm - s3mm - s3nm)*ss + 8*Mm2*(s1m&
                  &m - 2*s2nm + s3mm + s3nm)*tt + 4*Mm2**2*(3*s1mm - 3*s2nm - snm*tt) - 16*tt**2*(s&
                  &1mm + s2nm - 2*s3mm - 2*s3nm - snm*tt) + ss*tt*(s1mm + s2nm - 2*(s3mm + s3nm - s&
                  &nm*tt))) + Mm2**2*tt*(8*tt*(-2*s1mm + s2nm + s3mm + s3nm + 2*snm*tt) + ss*(-11*s&
                  &1mm + s2nm + 2*(5*s3mm + 5*s3nm + snm*tt))) + me2*(-6*Mm2**4*snm + Mm2**2*(6*(-s&
                  &1mm + s3mm + s3nm)*ss + 8*(-s1mm + s2nm)*tt) + 8*Mm2**3*(-2*s1mm + s2nm + s3mm +&
                  & s3nm + snm*tt) - tt*(ss**2*(-3*s1mm + 3*s3mm + 3*s3nm - 4*snm*tt) + 2*ss*tt*(3*&
                  &s1mm + 10*s2nm - 13*s3mm - 13*s3nm + 4*snm*tt) + 2*tt**2*(-8*s1mm - 8*s2nm + 16*&
                  &s3mm + 16*s3nm + 9*snm*tt)) + 2*Mm2*tt*(4*tt*(-4*s1mm + s2nm + 3*s3mm + 3*s3nm +&
                  & 2*snm*tt) + ss*(5*s1mm - s2nm - 2*(2*s3mm + 2*s3nm + snm*tt)))))/(4.*ss*(-4*me2&
                  &*Mm2 + (me2 + Mm2 - tt)**2)*tt)
    coeff_disct_lmm = -(1./4.)*(tt*(-6*me2**5*snm - 6*Mm2**5*snm + Mm2**4*(-18*s1mm + 6*s2nm + 12*s3mm + &
                      &12*s3nm + snm*ss + 30*snm*tt) + me2**4*(-6*s1mm - 6*s2nm + 12*s3mm + 12*s3nm - 6&
                      &*Mm2*snm + snm*ss + 30*snm*tt) + me2**3*(12*Mm2**2*snm - snm*ss**2 + ss*(6*s1mm &
                      &+ 10*s2nm - 16*s3mm - 16*s3nm - 15*snm*tt) - 12*Mm2*(s1mm - s2nm - 4*snm*tt) - 1&
                      &2*tt*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 5*snm*tt)) + tt**2*(6*tt**2*(-s1mm - &
                      &s2nm + 2*s3mm + 2*s3nm + snm*tt) + ss**2*(-5*s1mm - 10*s2nm + 15*s3mm + 15*s3nm &
                      &+ 6*snm*tt) + ss*tt*(-11*s1mm - 15*s2nm + 26*s3mm + 26*s3nm + 12*snm*tt)) - Mm2*&
                      &*3*(snm*ss**2 + 12*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm + 5*snm*tt) + ss*(-8*s1m&
                      &m - 8*s2nm + 16*s3mm + 16*s3nm + 15*snm*tt)) - Mm2*tt*(6*tt**2*(-6*s1mm - 2*s2nm&
                      & + 8*s3mm + 8*s3nm + 5*snm*tt) + ss**2*(-9*s1mm - 10*s2nm + 19*s3mm + 19*s3nm + &
                      &8*snm*tt) + ss*tt*(-42*s1mm - 26*s2nm + 68*s3mm + 68*s3nm + 37*snm*tt)) + Mm2**2&
                      &*(ss**2*(-2*s1mm + 2*s3mm + 2*s3nm + 3*snm*tt) + 12*tt**2*(-6*s1mm + 6*s3mm + 6*&
                      &s3nm + 5*snm*tt) + ss*tt*(-39*s1mm - 19*s2nm + 58*s3mm + 58*s3nm + 39*snm*tt)) +&
                      & me2**2*(12*Mm2**3*snm + Mm2**2*(24*s1mm - 24*s3mm - 24*s3nm - 2*snm*ss + 36*snm&
                      &*tt) + Mm2*(snm*ss**2 + ss*(-4*s1mm - 12*s2nm + 16*s3mm + 16*s3nm - 17*snm*tt) -&
                      & 12*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm + 9*snm*tt)) + tt*(3*snm*ss**2 + 12*tt*&
                      &(-3*s1mm - 3*s2nm + 6*s3mm + 6*s3nm + 5*snm*tt) + ss*(-23*s1mm - 35*s2nm + 58*s3&
                      &mm + 58*s3nm + 39*snm*tt))) - me2*(6*Mm2**4*snm - 12*Mm2**3*(s1mm - s2nm + 4*snm&
                      &*tt) + Mm2**2*(-(snm*ss**2) + 12*tt*(-4*s1mm + 4*s3mm + 4*s3nm + 9*snm*tt) + ss*&
                      &(10*s1mm + 6*s2nm - 16*s3mm - 16*s3nm + 17*snm*tt)) + tt*(6*tt**2*(-4*s1mm - 4*s&
                      &2nm + 8*s3mm + 8*s3nm + 5*snm*tt) + ss**2*(-5*s1mm - 10*s2nm + 15*s3mm + 15*s3nm&
                      & + 8*snm*tt) + ss*tt*(-28*s1mm - 40*s2nm + 68*s3mm + 68*s3nm + 37*snm*tt)) - 2*M&
                      &m2*(ss**2*(s1mm - s3mm - s3nm + snm*tt) + 3*ss*tt*(-7*s1mm - 3*s2nm + 10*s3mm + &
                      &10*s3nm + 9*snm*tt) + 6*tt**2*(-7*s1mm - s2nm + 8*(s3mm + s3nm + snm*tt))))))/(s&
                      &s*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + &
                      &tt) + tt*(ss + tt)))
    coeff_disct_lmu = (-2*(me2 + Mm2 - tt)*tt*(me2**2*snm + Mm2**2*snm - s1mm*ss - 2*s2nm*ss + 3*s3mm*&
                      &ss + 3*s3nm*ss - s1mm*tt - s2nm*tt + 2*s3mm*tt + 2*s3nm*tt + snm*ss*tt + snm*tt*&
                      &*2 + Mm2*(3*s1mm - s2nm - 2*(s3mm + s3nm + snm*tt)) + me2*(s1mm + s2nm - 2*(s3mm&
                      & + s3nm - Mm2*snm + snm*tt))))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
    coeff_disct_ls = (tt*(-2*me2**5*snm - 2*Mm2**5*snm + Mm2**4*(-6*s1mm + 2*s2nm + 4*s3mm + 4*s3nm +&
                     & snm*ss + 10*snm*tt) + me2**4*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm - 2*Mm2*snm + &
                     &snm*ss + 10*snm*tt) + tt**2*(2*tt**2*(-s1mm - s2nm + 2*s3mm + 2*s3nm + snm*tt) +&
                     & ss**2*(-s1mm - 2*s2nm + 3*s3mm + 3*s3nm + 2*snm*tt) + ss*tt*(-3*s1mm - 3*s2nm +&
                     & 6*s3mm + 6*s3nm + 4*snm*tt)) + me2**3*(4*Mm2**2*snm - snm*ss**2 + ss*(2*s1mm + &
                     &2*s2nm - 4*s3mm - 4*s3nm - 7*snm*tt) - 4*Mm2*(s1mm - s2nm - 4*snm*tt) - 4*tt*(-2&
                     &*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 5*snm*tt)) - Mm2**3*(snm*ss**2 + 4*tt*(-5*s1m&
                     &m + s2nm + 4*s3mm + 4*s3nm + 5*snm*tt) + ss*(-4*s1mm + 4*s3mm + 4*s3nm + 7*snm*t&
                     &t)) - Mm2*tt*(ss**2*(-5*s1mm - 2*s2nm + 7*s3mm + 7*s3nm + 4*snm*tt) + 2*tt**2*(-&
                     &6*s1mm - 2*s2nm + 8*s3mm + 8*s3nm + 5*snm*tt) + ss*tt*(-14*s1mm - 2*s2nm + 16*s3&
                     &mm + 16*s3nm + 13*snm*tt)) + Mm2**2*(ss**2*(-2*s1mm + 2*s3mm + 2*s3nm + 3*snm*tt&
                     &) + 4*tt**2*(-6*s1mm + 6*s3mm + 6*s3nm + 5*snm*tt) + ss*tt*(-15*s1mm + s2nm + 14&
                     &*s3mm + 14*s3nm + 15*snm*tt)) + me2**2*(4*Mm2**3*snm + 2*Mm2**2*(4*s1mm - 4*s3mm&
                     & - 4*s3nm - snm*ss + 6*snm*tt) + Mm2*(snm*ss**2 + ss*(-4*s2nm + 4*s3mm + 4*s3nm &
                     &- 9*snm*tt) - 4*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm + 9*snm*tt)) + tt*(3*snm*ss&
                     &**2 + 4*tt*(-3*s1mm - 3*s2nm + 6*s3mm + 6*s3nm + 5*snm*tt) + ss*(-7*s1mm - 7*s2n&
                     &m + 14*s3mm + 14*s3nm + 15*snm*tt))) + me2*(-2*Mm2**4*snm + 4*Mm2**3*(s1mm - s2n&
                     &m + 4*snm*tt) + Mm2**2*(snm*ss**2 + ss*(-6*s1mm + 2*s2nm + 4*s3mm + 4*s3nm - 9*s&
                     &nm*tt) - 4*tt*(-4*s1mm + 4*s3mm + 4*s3nm + 9*snm*tt)) - tt*(ss**2*(-s1mm - 2*s2n&
                     &m + 3*s3mm + 3*s3nm + 4*snm*tt) + 2*tt**2*(-4*s1mm - 4*s2nm + 8*s3mm + 8*s3nm + &
                     &5*snm*tt) + ss*tt*(-8*s1mm - 8*s2nm + 16*s3mm + 16*s3nm + 13*snm*tt)) + 2*Mm2*(s&
                     &s**2*(s1mm - s3mm - s3nm + snm*tt) + ss*tt*(-9*s1mm - s2nm + 10*s3mm + 10*s3nm +&
                     & 11*snm*tt) + 2*tt**2*(-7*s1mm - s2nm + 8*(s3mm + s3nm + snm*tt))))))/(2.*ss*(-4&
                     &*me2*Mm2 + (me2 + Mm2 - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) +&
                     & tt*(ss + tt)))
    coeff_discu = -(1./2.)*(2*me2**4*snm + 2*me2**3*(s1mm + s2nm - 2*s3mm - 2*s3nm - 3*snm*tt) - me2**&
                  &2*(4*Mm2**2*snm + 9*snm*ss**2 - 2*Mm2*(9*s1mm + 5*s2nm - 14*s3mm - 14*s3nm + 8*s&
                  &nm*ss - 5*snm*tt) - ss*(12*s1mm + 24*s2nm - 36*s3mm - 36*s3nm + snm*tt) - 2*tt*(&
                  &-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 3*snm*tt)) + (Mm2 - ss - tt)*(2*Mm2**3*snm &
                  &+ 2*Mm2**2*(3*s1mm - s2nm - 2*s3mm - 2*s3nm + snm*ss - 2*snm*tt) + ss*(ss + tt)*&
                  &(-8*s1mm - 16*s2nm + 24*s3mm + 24*s3nm + 3*snm*ss + snm*tt) - Mm2*(7*snm*ss**2 +&
                  & 2*tt*(s1mm + s2nm - 2*s3mm - 2*s3nm - snm*tt) + ss*(-14*s1mm - 26*s2nm + 40*s3m&
                  &m + 40*s3nm + snm*tt))) + 2*me2*(Mm2**2*(19*s1mm - 5*s2nm - 14*s3mm - 14*s3nm + &
                  &8*snm*ss - 5*snm*tt) + (ss + tt)*(5*snm*ss**2 + tt*(s1mm + s2nm - 2*s3mm - 2*s3n&
                  &m - snm*tt) + ss*(-11*s1mm - 21*s2nm + 32*s3mm + 32*s3nm + snm*tt)) - Mm2*(11*sn&
                  &m*ss**2 + ss*(-6*s1mm - 14*s2nm + 20*s3mm + 20*s3nm + snm*tt) - 2*tt*(-5*s1mm - &
                  &3*s2nm + 8*s3mm + 8*s3nm + 3*snm*tt))))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)*&
                  &*2))
    coeff_discu_lmm = ((-2*me2 - 2*Mm2 + ss + tt)*(-6*me2**5*snm - 6*Mm2**5*snm + 3*Mm2**4*(-6*s1mm + &
                      &2*s2nm + 4*s3mm + 4*s3nm + 3*snm*ss + 10*snm*tt) + me2**4*(-6*s1mm - 6*s2nm + 12&
                      &*s3mm + 12*s3nm - 6*Mm2*snm + 9*snm*ss + 30*snm*tt) + me2**3*(12*Mm2**2*snm - 7*&
                      &snm*ss**2 + ss*(14*s1mm + 18*s2nm - 32*s3mm - 32*s3nm - 47*snm*tt) - 12*Mm2*(s1m&
                      &m - s2nm - 4*snm*tt) - 12*tt*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 5*snm*tt)) + &
                      &(ss + tt)**2*(snm*ss**3 + 6*tt**2*(-s1mm - s2nm + 2*s3mm + 2*s3nm + snm*tt) + ss&
                      &**2*(-s1mm + s3mm + s3nm + 3*snm*tt) + ss*tt*(-7*s1mm - 11*s2nm + 18*s3mm + 18*s&
                      &3nm + 8*snm*tt)) - Mm2*(ss + tt)*(4*snm*ss**3 + 6*tt**2*(-6*s1mm - 2*s2nm + 8*s3&
                      &mm + 8*s3nm + 5*snm*tt) + ss**2*(-4*s1mm + s2nm + 3*s3mm + 3*s3nm + 13*snm*tt) +&
                      & ss*tt*(-40*s1mm - 28*s2nm + 68*s3mm + 68*s3nm + 39*snm*tt)) - Mm2**3*(7*snm*ss*&
                      &*2 + 12*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm + 5*snm*tt) + ss*(-26*s1mm - 6*s2nm&
                      & + 32*s3mm + 32*s3nm + 47*snm*tt)) + Mm2**2*(7*snm*ss**3 + 12*tt**2*(-6*s1mm + 6&
                      &*s3mm + 6*s3nm + 5*snm*tt) + ss**2*(-11*s1mm - 9*s2nm + 20*s3mm + 20*s3nm + 34*s&
                      &nm*tt) + ss*tt*(-83*s1mm - 23*s2nm + 106*s3mm + 106*s3nm + 87*snm*tt)) + me2**2*&
                      &(12*Mm2**3*snm + 7*snm*ss**3 + 12*tt**2*(-3*s1mm - 3*s2nm + 6*s3mm + 6*s3nm + 5*&
                      &snm*tt) + 6*Mm2**2*(4*s1mm - 4*s3mm - 4*s3nm - 3*snm*ss + 6*snm*tt) + ss**2*(-11&
                      &*s1mm - 13*s2nm + 24*s3mm + 24*s3nm + 34*snm*tt) + ss*tt*(-47*s1mm - 59*s2nm + 1&
                      &06*s3mm + 106*s3nm + 87*snm*tt) - Mm2*(snm*ss**2 + 12*tt*(-5*s1mm + s2nm + 4*s3m&
                      &m + 4*s3nm + 9*snm*tt) + ss*(2*s1mm + 30*s2nm - 32*s3mm - 32*s3nm + 49*snm*tt)))&
                      & - me2*(6*Mm2**4*snm - 12*Mm2**3*(s1mm - s2nm + 4*snm*tt) + (ss + tt)*(4*snm*ss*&
                      &*3 + 6*tt**2*(-4*s1mm - 4*s2nm + 8*s3mm + 8*s3nm + 5*snm*tt) + ss**2*(-4*s1mm - &
                      &s2nm + 5*s3mm + 5*s3nm + 13*snm*tt) + ss*tt*(-28*s1mm - 40*s2nm + 68*s3mm + 68*s&
                      &3nm + 39*snm*tt)) + Mm2**2*(snm*ss**2 + 12*tt*(-4*s1mm + 4*s3mm + 4*s3nm + 9*snm&
                      &*tt) + ss*(38*s1mm - 6*s2nm - 32*s3mm - 32*s3nm + 49*snm*tt)) - 2*Mm2*(3*snm*ss*&
                      &*3 + ss*tt*(-39*s1mm - 7*s2nm + 46*s3mm + 46*s3nm + 59*snm*tt) + ss**2*(3*s1mm +&
                      & 11*s2nm - 14*(s3mm + s3nm - snm*tt)) + 6*tt**2*(-7*s1mm - s2nm + 8*(s3mm + s3nm&
                      & + snm*tt))))))/(4.*ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)*(me2**2 + Mm2**2 &
                      &- 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_discu_lmu = (-2*(-2*me2 - 2*Mm2 + ss + tt)*(-me2 - Mm2 + ss + tt)*(me2**2*snm + Mm2**2*snm -&
                      & s1mm*ss - 2*s2nm*ss + 3*s3mm*ss + 3*s3nm*ss - s1mm*tt - s2nm*tt + 2*s3mm*tt + 2&
                      &*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2*(3*s1mm - s2nm - 2*(s3mm + s3nm + snm*tt)&
                      &) + me2*(s1mm + s2nm - 2*(s3mm + s3nm - Mm2*snm + snm*tt))))/(ss*(-4*me2*Mm2 + (&
                      &me2 + Mm2 - ss - tt)**2))
    coeff_discu_ls = -(1./2.)*((2*me2 + 2*Mm2 - ss - tt)*(2*me2**5*snm + 2*Mm2**5*snm + Mm2**4*(6*s1mm - &
                     &2*s2nm - 4*s3mm - 4*s3nm - 5*snm*ss - 10*snm*tt) + me2**4*(2*s1mm + 2*s2nm - 4*s&
                     &3mm - 4*s3nm + 2*Mm2*snm - 5*snm*ss - 10*snm*tt) - (ss + tt)**2*(snm*ss**3 + 2*t&
                     &t**2*(-s1mm - s2nm + 2*s3mm + 2*s3nm + snm*tt) + ss**2*(-s1mm + s3mm + s3nm + 3*&
                     &snm*tt) + ss*tt*(-3*s1mm - 3*s2nm + 6*s3mm + 6*s3nm + 4*snm*tt)) + Mm2*(ss + tt)&
                     &*(4*snm*ss**3 + 2*tt**2*(-6*s1mm - 2*s2nm + 8*s3mm + 8*s3nm + 5*snm*tt) + ss**2*&
                     &(-4*s1mm + s2nm + 3*s3mm + 3*s3nm + 13*snm*tt) + ss*tt*(-16*s1mm - 8*s2nm + 24*s&
                     &3mm + 24*s3nm + 19*snm*tt)) + me2**3*(-4*Mm2**2*snm + 7*snm*ss**2 + 4*Mm2*(s1mm &
                     &- s2nm - 4*snm*tt) + 4*tt*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 5*snm*tt) + ss*(&
                     &-6*s1mm - 6*s2nm + 12*s3mm + 12*s3nm + 23*snm*tt)) + Mm2**3*(7*snm*ss**2 + 4*tt*&
                     &(-5*s1mm + s2nm + 4*s3mm + 4*s3nm + 5*snm*tt) + ss*(-10*s1mm - 2*s2nm + 12*s3mm &
                     &+ 12*s3nm + 23*snm*tt)) - Mm2**2*(7*snm*ss**3 + 4*tt**2*(-6*s1mm + 6*s3mm + 6*s3&
                     &nm + 5*snm*tt) + ss**2*(-7*s1mm - s2nm + 8*s3mm + 8*s3nm + 26*snm*tt) + ss*tt*(-&
                     &31*s1mm - 7*s2nm + 38*s3mm + 38*s3nm + 39*snm*tt)) + me2**2*(-4*Mm2**3*snm - 7*s&
                     &nm*ss**3 + ss*tt*(19*s1mm + 19*s2nm - 38*s3mm - 38*s3nm - 39*snm*tt) + 2*Mm2**2*&
                     &(-4*s1mm + 4*s3mm + 4*s3nm + 5*snm*ss - 6*snm*tt) - 4*tt**2*(-3*s1mm - 3*s2nm + &
                     &6*s3mm + 6*s3nm + 5*snm*tt) + ss**2*(7*s1mm + 5*s2nm - 2*(6*s3mm + 6*s3nm + 13*s&
                     &nm*tt)) + Mm2*(snm*ss**2 + 4*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm + 9*snm*tt) + &
                     &ss*(2*s1mm + 10*s2nm - 12*s3mm - 12*s3nm + 25*snm*tt))) + me2*(2*Mm2**4*snm - 4*&
                     &Mm2**3*(s1mm - s2nm + 4*snm*tt) + (ss + tt)*(4*snm*ss**3 + 2*tt**2*(-4*s1mm - 4*&
                     &s2nm + 8*s3mm + 8*s3nm + 5*snm*tt) + ss**2*(-4*s1mm - s2nm + 5*s3mm + 5*s3nm + 1&
                     &3*snm*tt) + ss*tt*(-12*s1mm - 12*s2nm + 24*s3mm + 24*s3nm + 19*snm*tt)) + Mm2**2&
                     &*(snm*ss**2 + 4*tt*(-4*s1mm + 4*s3mm + 4*s3nm + 9*snm*tt) + ss*(14*s1mm - 2*s2nm&
                     & - 12*s3mm - 12*s3nm + 25*snm*tt)) - 2*Mm2*(3*snm*ss**3 + 3*ss*tt*(-5*s1mm - s2n&
                     &m + 6*s3mm + 6*s3nm + 9*snm*tt) - ss**2*(s1mm - 3*s2nm + 2*(s3mm + s3nm - 7*snm*&
                     &tt)) + 2*tt**2*(-7*s1mm - s2nm + 8*(s3mm + s3nm + snm*tt))))))/(ss*(-4*me2*Mm2 +&
                     & (me2 + Mm2 - ss - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(&
                     &ss + tt)))
    coeff_lmm = -(1./24.)*(48*Mm2**6*snm + 12*me2**5*snm*(-4*Mm2 + ss) + 12*Mm2**5*(&
                &12*s1mm - 4*s2nm - 8*s3mm - 8*s3nm - 3*snm*ss - 10*snm*tt) + 6*me2**4*(4*Mm2 - s&
                &s)*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 2*Mm2*snm + snm*ss + 5*snm*tt) + ss*tt*(&
                &ss + tt)*(14*tt**2*(s1mm + s2nm - 2*s3mm - 2*s3nm - snm*tt) + ss**2*(-9*s1mm + 9&
                &*s3mm + 9*s3nm + 6*snm*tt) - ss*tt*(3*s1mm - 17*s2nm + 14*s3mm + 14*s3nm + 8*snm&
                &*tt)) + 2*Mm2**4*(3*snm*ss**2 + 4*tt*(-15*s1mm - 3*s2nm + 18*s3mm + 18*s3nm + 4*&
                &snm*tt) + 3*ss*(-10*s1mm + 6*s2nm + 4*s3mm + 4*s3nm + 13*snm*tt)) + 2*me2**3*(4*&
                &Mm2 - ss)*(12*Mm2**2*snm + tt*(-21*s1mm - 21*s2nm + 42*s3mm + 42*s3nm - 32*snm*t&
                &t) + 3*ss*(s1mm - s2nm - 2*snm*tt) + 6*Mm2*(2*s1mm - 2*s2nm - snm*(ss + tt))) - &
                &2*Mm2**3*(3*ss**2*(3*s1mm + s2nm - 4*s3mm - 4*s3nm + 2*snm*tt) - 3*ss*tt*(-31*s1&
                &mm + 9*s2nm + 22*s3mm + 22*s3nm + 20*snm*tt) - 4*tt**2*(-21*s1mm + 11*s2nm + 10*&
                &(s3mm + s3nm + 2*snm*tt))) + Mm2*tt*(-56*tt**3*(s1mm + s2nm - 2*s3mm - 2*s3nm - &
                &snm*tt) + 3*ss**3*(9*s1mm + s2nm - 2*(5*s3mm + 5*s3nm + 2*snm*tt)) + 6*ss*tt**2*&
                &(-25*s1mm - 29*s2nm + 6*(9*s3mm + 9*s3nm + 5*snm*tt)) + ss**2*tt*(-51*s1mm - 97*&
                &s2nm + 4*(37*s3mm + 37*s3nm + 28*snm*tt))) + 2*Mm2**2*(3*(s1mm - s3mm - s3nm)*ss&
                &**3 + 4*tt**3*(29*s1mm + s2nm - 30*s3mm - 30*s3nm - 22*snm*tt) - ss**2*tt*(-45*s&
                &1mm + 12*s2nm + 33*s3mm + 33*s3nm + 28*snm*tt) - ss*tt**2*(-171*s1mm + s2nm + 2*&
                &(85*s3mm + 85*s3nm + 78*snm*tt))) + me2*(-48*Mm2**5*snm + 12*Mm2**4*(-8*s1mm + 8&
                &*s2nm + 5*snm*ss + 4*snm*tt) + ss*tt*(3*ss**2*(7*s1mm - s2nm - 6*s3mm - 6*s3nm -&
                & 8*snm*tt) + 2*tt**2*(-21*s1mm - 21*s2nm + 42*s3mm + 42*s3nm + 34*snm*tt) + ss*t&
                &t*(-5*s1mm - 75*s2nm + 80*s3mm + 80*s3nm + 44*snm*tt)) - 4*Mm2*(3*(s1mm - s3mm -&
                & s3nm)*ss**3 + ss**2*tt*(18*s1mm - 9*s2nm - 9*s3mm - 9*s3nm + snm*tt) + 2*tt**3*&
                &(-21*s1mm - 21*s2nm + 42*s3mm + 42*s3nm + 34*snm*tt) + ss*tt**2*(-51*s1mm - 93*s&
                &2nm + 144*s3mm + 144*s3nm + 115*snm*tt)) + 2*Mm2**2*(3*ss**2*(5*s1mm + 3*s2nm - &
                &8*s3mm - 8*s3nm + 2*snm*tt) + 8*tt**2*(-22*s1mm + 6*s2nm + 16*s3mm + 16*s3nm + 3&
                &5*snm*tt) + ss*tt*(-9*s1mm - 57*s2nm + 66*s3mm + 66*s3nm + 140*snm*tt)) - 12*Mm2&
                &**3*(snm*ss**2 + ss*(-8*s1mm + 8*s2nm + 5*snm*tt) + 2*tt*(5*s1mm - 3*s2nm - 2*(s&
                &3mm + s3nm - 6*snm*tt)))) - 2*me2**2*(48*Mm2**4*snm + 12*Mm2**3*(8*s1mm - 8*s3mm&
                & - 8*s3nm - snm*ss) + Mm2*(ss*tt*(-81*s1mm - 9*s2nm + 90*s3mm + 90*s3nm - 88*snm&
                &*tt) + 3*ss**2*(s1mm + 3*s2nm - 4*s3mm - 4*s3nm + 2*snm*tt) - 4*tt**2*(s1mm + s2&
                &nm - 2*(s3mm + s3nm - 25*snm*tt))) + ss*(3*(-s1mm + s3mm + s3nm)*ss**2 + ss*tt*(&
                &9*s1mm + 6*s2nm - 15*s3mm - 15*s3nm - 2*snm*tt) + tt**2*(s1mm + s2nm - 2*(s3mm +&
                & s3nm - 25*snm*tt))) + 12*Mm2**2*(tt*(15*s1mm - 5*s2nm - 2*(5*s3mm + 5*s3nm - 8*&
                &snm*tt)) + ss*(s1mm - 3*s2nm + 2*(s3mm + s3nm - snm*tt)))))/((4*Mm2 - ss)*ss*tt*&
                &*2*(-2*me2 - 2*Mm2 + ss + tt))
    coeff_lmu = (7*me2**2*snm + 7*Mm2**2*snm - 5*s1mm*ss - 10*s2nm*ss + 15*s3mm*ss + 15*s3nm*ss &
                &- 5*s1mm*tt - 5*s2nm*tt + 10*s3mm*tt + 10*s3nm*tt + 7*snm*ss*tt + 7*snm*tt**2 + &
                &Mm2*(15*s1mm - 5*s2nm - 2*(5*s3mm + 5*s3nm + 7*snm*tt)) + me2*(5*s1mm + 5*s2nm -&
                & 2*(5*s3mm + 5*s3nm - 7*Mm2*snm + 7*snm*tt)))/(6.*ss)
    coeff_ls = (8*Mm2**2*(s1mm - s2nm) + 16*me2**2*Mm2*snm + ss*(2*s1mm - 2*s2nm + snm*ss)*(ss &
               &+ tt) + Mm2*((-14*s1mm + 6*s2nm + 8*(s3mm + s3nm))*ss - 3*snm*ss**2 + 8*(-s1mm +&
               & s2nm)*tt) + me2*(16*Mm2**2*snm + ss*(-2*s1mm + 2*s2nm - 3*snm*ss) + 8*Mm2*(3*s1&
               &mm + s2nm - 4*s3mm - 4*s3nm - 2*snm*tt)))/(2.*(4*me2 - ss)*(-4*Mm2 + ss))
    coeff_unity = -(1./18.)*(-3*me2**3*snm*(3*ss + 8*tt) - 3*Mm2**3*snm*(3*ss + 8*tt) +&
                  & Mm2**2*(48*snm*tt**2 + ss*(-27*s1mm + 9*s2nm + 18*s3mm + 18*s3nm + 19*snm*tt)) &
                  &+ me2**2*(-24*tt*(s1mm + s2nm - 2*s3mm - 2*s3nm + 3*Mm2*snm - 2*snm*tt) + ss*(9*&
                  &s1mm + 9*s2nm - 18*s3mm - 18*s3nm + 9*Mm2*snm + 19*snm*tt)) - Mm2*(24*snm*tt**3 &
                  &+ 9*ss**2*(s1mm - s3mm - s3nm + 18*snm*tt) + ss*tt*(-174*s1mm - 10*s2nm + 184*s3&
                  &mm + 184*s3nm + 35*snm*tt)) + ss*tt*(51*snm*ss**2 + tt*(-59*s1mm - 59*s2nm + 118&
                  &*s3mm + 118*s3nm + snm*tt) + ss*(-17*s1mm + 38*s2nm - 21*s3mm - 21*s3nm + 118*sn&
                  &m*tt)) + me2*(9*Mm2**2*snm*(ss - 8*tt) + ss*tt*(50*s1mm + 74*s2nm - 124*s3mm - 1&
                  &24*s3nm - 35*snm*tt) + 9*ss**2*(s1mm - s3mm - s3nm - 18*snm*tt) + 24*tt**2*(s1mm&
                  & + s2nm - 2*s3mm - 2*s3nm - snm*tt) + 2*Mm2*ss*(9*s1mm - 9*s2nm + 73*snm*tt) + 2&
                  &4*Mm2*tt*(-3*s1mm + s2nm + 2*(s3mm + s3nm + 2*snm*tt))))/(ss**2*tt)
    coeffLL = coeffLL + c6se*coeff_c6se + c6sm*coeff_c6sm + c6t*coeff_c6t  &
            + c6u*coeff_c6u + ce*coeff_ce + cm*coeff_cm  &
            + coeff_discse*discse + coeff_discse_lmm*discse*lmm + coeff_discse_lmu*discse*lmu  &
            + coeff_discsm*discsm + coeff_discsm_lmu*discsm*lmu + coeff_disct*disct  &
            + coeff_disct_lmm*disct*lmm + coeff_disct_lmu*disct*lmu + coeff_disct_ls*disctLs  &
            + coeff_discu*discu + coeff_discu_lmm*discu*lmm + coeff_discu_lmu*discu*lmu  &
            + coeff_discu_ls*discuLs + coeff_lmm*lmm + coeff_lmu*lmu  &
            + coeff_ls*ls + coeff_unity
    coeff_c6se = ((2*me2 - ss)*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(s2&
                 &nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*ss&
                 &)
    coeff_c6sm = ((2*Mm2 - ss)*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(s2&
                 &nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*ss&
                 &)
    coeff_c6t = (me2**6*(-4*s2nm*s3mm + 4*s1mm*s3nm) + me2**5*(10*Mm2*s2nm*s3mm + 2*Mm2*s1mm*(s2&
                &nm - 5*s3nm) + s3mm*(s2nm - 2*s3nm)*ss + s1mm*(-s2nm + s3nm)*ss + 22*(s2nm*s3mm &
                &- s1mm*s3nm)*tt) + me2**4*(-2*Mm2**2*(3*s1mm*s2nm + s2nm*s3mm - s1mm*s3nm) + Mm2&
                &*(-3*s2nm*s3mm + 2*s3mm*s3nm + s1mm*(3*s2nm + s3nm))*ss + (s1mm*s2nm - s2nm*s3mm&
                & - 4*s1mm*s3nm + 5*s3mm*s3nm)*ss**2 - 2*Mm2*(5*s1mm*s2nm + 6*s2nm*s3mm - 6*s1mm*&
                &s3nm)*tt + (4*s1mm*s2nm - 13*s2nm*s3mm + 7*s1mm*s3nm + 6*s3mm*s3nm)*ss*tt + 50*(&
                &-(s2nm*s3mm) + s1mm*s3nm)*tt**2) + (Mm2 - tt)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm&
                & + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(2*Mm2**4 + Mm2**3*(s&
                &s - 8*tt) + 2*tt**2*(ss + tt)**2 + Mm2*tt*(2*ss**2 - 7*ss*tt - 8*tt**2) + Mm2**2&
                &*(-ss**2 + 2*ss*tt + 12*tt**2)) + me2**3*(4*Mm2**3*(-3*s2nm*s3mm + s1mm*(s2nm + &
                &3*s3nm)) + (s1mm - s3mm)*s3nm*ss**3 + (-3*s1mm*s2nm + 2*s2nm*s3mm + 9*s1mm*s3nm &
                &- 11*s3mm*s3nm)*ss**2*tt - (6*s1mm*s2nm - 37*s2nm*s3mm + 33*s1mm*s3nm + 4*s3mm*s&
                &3nm)*ss*tt**2 + 60*(s2nm*s3mm - s1mm*s3nm)*tt**3 - 4*Mm2**2*(s1mm*s2nm*ss + s3mm&
                &*(-s2nm + s3nm)*ss + (-2*s1mm*s2nm + 3*s2nm*s3mm - 3*s1mm*s3nm)*tt) + Mm2*((-2*s&
                &1mm*s2nm + 2*s2nm*s3mm + 6*s1mm*s3nm - 8*s3mm*s3nm)*ss**2 + (s1mm*s2nm + 9*s2nm*&
                &s3mm - 17*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt + 4*(5*s1mm*s2nm - 9*s2nm*s3mm + 9*s1mm&
                &*s3nm)*tt**2)) + me2**2*(4*Mm2**4*(2*s2nm*s3mm + s1mm*(s2nm - 2*s3nm)) + Mm2**2*&
                &(2*(-s1mm + s3mm)*s3nm*ss**2 + (-5*s1mm*s2nm + 13*s2nm*s3mm - 17*s1mm*s3nm + 4*s&
                &3mm*s3nm)*ss*tt + 4*(3*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**2) + tt*(3*(-s&
                &1mm + s3mm)*s3nm*ss**3 + 3*(s3mm*(-2*s2nm + s3nm) + s1mm*(s2nm + s3nm))*ss**2*tt&
                & + (4*s1mm*s2nm - 43*s2nm*s3mm + 47*s1mm*s3nm - 4*s3mm*s3nm)*ss*tt**2 + 40*(-(s2&
                &nm*s3mm) + s1mm*s3nm)*tt**3) - Mm2*((s1mm - s3mm)*s3nm*ss**3 + (-3*s1mm*(s2nm + &
                &s3nm) + s3mm*(2*s2nm + s3nm))*ss**2*tt + (15*s1mm*s2nm - 26*s2nm*s3mm + 14*s1mm*&
                &s3nm + 12*s3mm*s3nm)*ss*tt**2 + 20*(-4*s2nm*s3mm + s1mm*(s2nm + 4*s3nm))*tt**3) &
                &+ 4*Mm2**3*(3*s3mm*s3nm*ss - 2*s1mm*s3nm*(ss - 2*tt) + s1mm*s2nm*(ss + tt) - s2n&
                &m*s3mm*(ss + 4*tt))) - me2*(2*Mm2**5*(-(s2nm*s3mm) + s1mm*(3*s2nm + s3nm)) + Mm2&
                &**3*(-2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm))*ss**2 + (s1mm*s2nm + 9*s2nm*s3mm + 1&
                &5*s1mm*s3nm - 24*s3mm*s3nm)*ss*tt - 4*(3*s1mm*s2nm - 11*s2nm*s3mm + 11*s1mm*s3nm&
                &)*tt**2) + Mm2**2*((s1mm - s3mm)*s3nm*ss**3 + (-4*s2nm*s3mm + 3*s1mm*(s2nm - 3*s&
                &3nm) + 13*s3mm*s3nm)*ss**2*tt + (10*s1mm*s2nm - 31*s2nm*s3mm + 19*s1mm*s3nm + 12&
                &*s3mm*s3nm)*ss*tt**2 + 4*(6*s1mm*s2nm - 19*s2nm*s3mm + 19*s1mm*s3nm)*tt**3) - Mm&
                &2*tt*(2*(s1mm - s3mm)*s3nm*ss**3 + (-7*s2nm*s3mm + 6*s3mm*s3nm + s1mm*(3*s2nm + &
                &s3nm))*ss**2*tt + (15*s1mm*s2nm - 47*s2nm*s3mm + 55*s1mm*s3nm - 8*s3mm*s3nm)*ss*&
                &tt**2 + 2*(5*s1mm*s2nm - 27*s2nm*s3mm + 27*s1mm*s3nm)*tt**3) + Mm2**4*(10*s3mm*s&
                &3nm*ss - 3*s2nm*s3mm*(ss + 2*tt) + s1mm*(3*s2nm*ss - 7*s3nm*ss - 8*s2nm*tt + 6*s&
                &3nm*tt)) + tt**3*(s1mm*(ss + tt)*(s2nm*ss + 14*s3nm*(ss + tt)) - s3mm*(s3nm*ss*(&
                &7*ss + 6*tt) + s2nm*(7*ss**2 + 22*ss*tt + 14*tt**2)))))/(2.*me2*ss*(me2**2 + Mm2&
                &**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_c6u = (me2**6*(-4*s2nm*s3mm + 4*s1mm*s3nm) + me2**5*(10*Mm2*s2nm*s3mm + 2*Mm2*s1mm*(s2&
                &nm - 5*s3nm) + s3mm*(s2nm - 2*s3nm)*ss + s1mm*(-s2nm + s3nm)*ss + 22*(s2nm*s3mm &
                &- s1mm*s3nm)*tt) + me2**4*(-2*Mm2**2*(3*s1mm*s2nm + s2nm*s3mm - s1mm*s3nm) + Mm2&
                &*(5*s1mm*s2nm - 21*s2nm*s3mm + 19*s1mm*s3nm + 2*s3mm*s3nm)*ss + (4*s1mm*s2nm + 2&
                &*s2nm*s3mm - 19*s1mm*s3nm + 17*s3mm*s3nm)*ss**2 - 2*Mm2*(5*s1mm*s2nm + 6*s2nm*s3&
                &mm - 6*s1mm*s3nm)*tt + (4*s1mm*s2nm - 13*s2nm*s3mm + 7*s1mm*s3nm + 6*s3mm*s3nm)*&
                &ss*tt + 50*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2) + (-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2n&
                &m + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(2*Mm2**5 + Mm2**4*(&
                &ss - 10*tt) - Mm2*(ss + tt)**2*(4*ss**2 + 9*ss*tt - 10*tt**2) + (ss + tt)**3*(ss&
                &**2 + 2*ss*tt - 2*tt**2) + Mm2**3*(-7*ss**2 + ss*tt + 20*tt**2) + Mm2**2*(7*ss**&
                &3 + 18*ss**2*tt - 9*ss*tt**2 - 20*tt**3)) - me2*(2*Mm2**5*(-(s2nm*s3mm) + s1mm*(&
                &3*s2nm + s3nm)) - Mm2**3*((s1mm*(s2nm - 25*s3nm) + s3mm*(s2nm + 24*s3nm))*ss**2 &
                &+ (-31*s1mm*s2nm + 9*s2nm*s3mm - 33*s1mm*s3nm + 24*s3mm*s3nm)*ss*tt + 4*(3*s1mm*&
                &s2nm - 11*s2nm*s3mm + 11*s1mm*s3nm)*tt**2) + (ss + tt)**2*((s1mm*s2nm - s2nm*s3m&
                &m - 7*s1mm*s3nm + 8*s3mm*s3nm)*ss**3 + (2*s1mm*s2nm + 4*s2nm*s3mm - 21*s1mm*s3nm&
                & + 17*s3mm*s3nm)*ss**2*tt + (s1mm*s2nm + 6*s3mm*(s2nm - s3nm))*ss*tt**2 + 14*(-(&
                &s2nm*s3mm) + s1mm*s3nm)*tt**3) + Mm2**2*((2*s2nm*s3mm - 27*s1mm*s3nm + 25*s3mm*s&
                &3nm)*ss**3 + (-32*s1mm*s2nm + 29*s2nm*s3mm - 102*s1mm*s3nm + 73*s3mm*s3nm)*ss**2&
                &*tt + (-13*s2nm*s3mm + 12*s3mm*s3nm + s1mm*(-8*s2nm + s3nm))*ss*tt**2 + 4*(6*s1m&
                &m*s2nm - 19*s2nm*s3mm + 19*s1mm*s3nm)*tt**3) + Mm2*(ss + tt)*(22*(s1mm - s3mm)*s&
                &3nm*ss**3 + (7*s1mm*s2nm - 19*s2nm*s3mm + 81*s1mm*s3nm - 62*s3mm*s3nm)*ss**2*tt &
                &+ (-3*s1mm*s2nm - 13*s2nm*s3mm + 5*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt**2 - 2*(5*s1mm&
                &*s2nm - 27*s2nm*s3mm + 27*s1mm*s3nm)*tt**3) + Mm2**4*(10*s3mm*s3nm*ss + 3*s2nm*s&
                &3mm*(ss - 2*tt) - s1mm*(11*s2nm*ss + 13*s3nm*ss + 8*s2nm*tt - 6*s3nm*tt))) + me2&
                &**3*(4*Mm2**3*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm)) + 2*Mm2**2*(s3mm*(17*s2nm*ss&
                & - 2*s3nm*ss - 6*s2nm*tt) + s1mm*(3*s2nm*ss - 15*s3nm*ss + 4*s2nm*tt + 6*s3nm*tt&
                &)) + Mm2*(8*s3mm*s3nm*ss*(3*ss + tt) + s2nm*s3mm*(-5*ss**2 + 51*ss*tt - 36*tt**2&
                &) - s1mm*s3nm*(19*ss**2 + 59*ss*tt - 36*tt**2) - 5*s1mm*s2nm*(ss**2 + ss*tt - 4*&
                &tt**2)) - 3*s1mm*(ss + tt)*(2*s2nm*ss*(ss + tt) + s3nm*(-9*ss**2 - 9*ss*tt + 20*&
                &tt**2)) + s3mm*(-(s3nm*ss*(31*ss**2 + 47*ss*tt + 4*tt**2)) + s2nm*(4*ss**3 - 7*s&
                &s**2*tt + 37*ss*tt**2 + 60*tt**3))) + me2**2*(4*Mm2**4*(2*s2nm*s3mm + s1mm*(s2nm&
                & - 2*s3nm)) - 2*Mm2**3*((11*s1mm*s2nm + 5*s2nm*s3mm + s1mm*s3nm - 6*s3mm*s3nm)*s&
                &s + 8*s2nm*s3mm*tt - 2*s1mm*(s2nm + 4*s3nm)*tt) + Mm2**2*((7*s1mm*(s2nm + s3nm) &
                &- s3mm*(5*s2nm + 2*s3nm))*ss**2 + (-33*s1mm*s2nm + 49*s2nm*s3mm - 53*s1mm*s3nm +&
                & 4*s3mm*s3nm)*ss*tt + 4*(3*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**2) - Mm2*(&
                &(-5*s2nm*s3mm + s1mm*(s2nm - 28*s3nm) + 33*s3mm*s3nm)*ss**3 + (-2*s1mm*(5*s2nm +&
                & 62*s3nm) + s3mm*(43*s2nm + 81*s3nm))*ss**2*tt + (s1mm*(9*s2nm - 16*s3nm) + 4*s3&
                &mm*(s2nm + 3*s3nm))*ss*tt**2 + 20*(-4*s2nm*s3mm + s1mm*(s2nm + 4*s3nm))*tt**3) +&
                & (ss + tt)*(s1mm*(ss + tt)*(4*s2nm*ss*(ss + tt) + s3nm*(-19*ss**2 - 33*ss*tt + 4&
                &0*tt**2)) + s3mm*(s3nm*ss*(23*ss**2 + 43*ss*tt - 4*tt**2) - s2nm*(4*ss**3 - 9*ss&
                &**2*tt + 3*ss*tt**2 + 40*tt**3)))))/(2.*me2*ss*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*m&
                &e2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_ce = (8*me2**6*(5*s2nm*s3mm + s1mm*(s2nm - 5*s3nm)) - 4*me2**5*(4*Mm2*(4*s1mm*s2nm + &
               &3*s2nm*s3mm - 3*s1mm*s3nm) + 7*s1mm*(s2nm - 5*s3nm)*ss + s3mm*(3*s2nm + 32*s3nm)&
               &*ss + 4*(2*s1mm*s2nm + 9*s2nm*s3mm - 9*s1mm*s3nm)*tt) + 4*me2**4*(20*Mm2**2*s1mm&
               &*s2nm + Mm2*(11*s1mm*s2nm + 9*s2nm*s3mm - s1mm*s3nm - 8*s3mm*s3nm)*ss + 2*(5*s1m&
               &m*s2nm - 3*s2nm*s3mm - 23*s1mm*s3nm + 26*s3mm*s3nm)*ss**2 + 16*Mm2*(-(s2nm*s3mm)&
               & + s1mm*(2*s2nm + s3nm))*tt + (19*s1mm*s2nm + 35*s2nm*s3mm - 107*s1mm*s3nm + 72*&
               &s3mm*s3nm)*ss*tt + 12*(4*s2nm*s3mm + s1mm*(s2nm - 4*s3nm))*tt**2) - ss**2*(-(Mm2&
               &*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2n&
               &m*tt))*(2*Mm2**3 - 3*Mm2**2*(ss + 2*tt) - (ss + tt)**2*(ss + 2*tt) + Mm2*(3*ss**&
               &2 + 8*ss*tt + 6*tt**2)) + 2*me2**3*(Mm2**3*(-8*s2nm*s3mm + 8*s1mm*s3nm) + (-14*s&
               &1mm*s2nm + 13*s2nm*s3mm + 67*s1mm*s3nm - 80*s3mm*s3nm)*ss**3 - 2*(17*s1mm*s2nm +&
               & 14*s2nm*s3mm - 114*s1mm*s3nm + 100*s3mm*s3nm)*ss**2*tt - 2*(17*s1mm*s2nm + 61*s&
               &2nm*s3mm - 109*s1mm*s3nm + 48*s3mm*s3nm)*ss*tt**2 - 8*(2*s1mm*s2nm + 7*s2nm*s3mm&
               & - 7*s1mm*s3nm)*tt**3 + Mm2**2*((6*s2nm*s3mm - 26*s1mm*(s2nm - s3nm) - 32*s3mm*s&
               &3nm)*ss + 8*(6*s1mm*s2nm - 5*s2nm*s3mm + 5*s1mm*s3nm)*tt) - 4*Mm2*((3*s3mm*(s2nm&
               & - 6*s3nm) + s1mm*(s2nm + 15*s3nm))*ss**2 + (17*s1mm*s2nm - 13*s2nm*s3mm + 45*s1&
               &mm*s3nm - 32*s3mm*s3nm)*ss*tt + 2*(4*s1mm*s2nm - 13*s2nm*s3mm + 13*s1mm*s3nm)*tt&
               &**2)) + me2**2*(-24*Mm2**4*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + (9*s1mm*s2nm - &
               &9*s2nm*s3mm - 56*s1mm*s3nm + 65*s3mm*s3nm)*ss**4 + (26*s1mm*s2nm + 23*s2nm*s3mm &
               &- 233*s1mm*s3nm + 210*s3mm*s3nm)*ss**3*tt + 2*(14*s1mm*s2nm + 71*s2nm*s3mm - 159&
               &*s1mm*s3nm + 88*s3mm*s3nm)*ss**2*tt**2 + 4*(5*s1mm*s2nm + 33*s2nm*s3mm - 41*s1mm&
               &*s3nm + 8*s3mm*s3nm)*ss*tt**3 + 8*(3*s2nm*s3mm + s1mm*(s2nm - 3*s3nm))*tt**4 + 4&
               &*Mm2**3*((5*s1mm*s2nm - 5*s2nm*s3mm + 13*s1mm*s3nm - 8*s3mm*s3nm)*ss + 8*(2*s1mm&
               &*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt) - 2*Mm2**2*((5*s1mm*s2nm - 13*s2nm*s3mm +&
               & 53*s1mm*s3nm - 40*s3mm*s3nm)*ss**2 + 2*(29*s1mm*s2nm - 43*s2nm*s3mm + 67*s1mm*s&
               &3nm - 24*s3mm*s3nm)*ss*tt + 24*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm))*tt**2) + Mm&
               &2*((-3*s3mm*(s2nm + 38*s3nm) + s1mm*(7*s2nm + 117*s3nm))*ss**3 + 2*(-64*s3mm*(s2&
               &nm + 2*s3nm) + 3*s1mm*(13*s2nm + 64*s3nm))*ss**2*tt + 4*(19*s1mm*(s2nm + 5*s3nm)&
               & - s3mm*(71*s2nm + 24*s3nm))*ss*tt**2 + 96*(-(s2nm*s3mm) + s1mm*s3nm)*tt**3)) - &
               &me2*ss*(-16*Mm2**4*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + 4*Mm2**3*((5*s1mm*s2nm &
               &- 5*s2nm*s3mm + 9*s1mm*s3nm - 4*s3mm*s3nm)*ss + 4*(3*s1mm*s2nm - 4*s2nm*s3mm + 4&
               &*s1mm*s3nm)*tt) - 2*Mm2**2*((-2*s3mm*(5*s2nm + 7*s3nm) + 3*s1mm*(3*s2nm + 8*s3nm&
               &))*ss**2 + 4*(8*s1mm*s2nm - 11*s2nm*s3mm + 17*s1mm*s3nm - 6*s3mm*s3nm)*ss*tt + 2&
               &4*(-2*s2nm*s3mm + s1mm*(s2nm + 2*s3nm))*tt**2) + 2*Mm2*((3*s1mm*s2nm - 3*s2nm*s3&
               &mm + 19*s1mm*s3nm - 16*s3mm*s3nm)*ss**3 + 4*(4*s1mm*s2nm - 7*s2nm*s3mm + 17*s1mm&
               &*s3nm - 10*s3mm*s3nm)*ss**2*tt + 2*(11*s1mm*s2nm - 29*s2nm*s3mm + 41*s1mm*s3nm -&
               & 12*s3mm*s3nm)*ss*tt**2 + 8*(-4*s2nm*s3mm + s1mm*(s2nm + 4*s3nm))*tt**3) + (ss +&
               & 2*tt)*(s1mm*(ss + tt)*(s2nm*ss**2 - 4*s3nm*(3*ss**2 + 5*ss*tt + 2*tt**2)) + s3m&
               &m*(s3nm*ss*(13*ss**2 + 22*ss*tt + 8*tt**2) + s2nm*(-ss**3 + 10*ss**2*tt + 20*ss*&
               &tt**2 + 8*tt**3)))))/(4.*me2*(4*me2 - ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(M&
               &m2 + tt) + tt*(ss + tt))**2)
    coeff_cm = -(1./4.)*(-4*me2**4*(s2nm*s3mm - s1mm*s3nm)*(12*Mm2**2 - 8*Mm2*ss + ss**2) + 2*me2*&
               &*3*(8*Mm2**3*(3*s1mm*s2nm + 4*s2nm*s3mm - 4*s1mm*s3nm) + ss**2*(s1mm*s2nm*ss - 5&
               &*s1mm*s3nm*ss + 5*s3mm*s3nm*ss + 7*s2nm*s3mm*tt - 7*s1mm*s3nm*tt) - 4*Mm2**2*((s&
               &1mm*s2nm + 6*s2nm*s3mm + 8*s1mm*s3nm - 14*s3mm*s3nm)*ss + 20*(-(s2nm*s3mm) + s1m&
               &m*s3nm)*tt) + Mm2*ss*((-5*s1mm*s2nm + 3*s2nm*s3mm + 33*s1mm*s3nm - 36*s3mm*s3nm)&
               &*ss + 56*(-(s2nm*s3mm) + s1mm*s3nm)*tt)) + (-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + &
               &s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(16*Mm2**5 - 48*Mm2**4*(&
               &ss + tt) - ss**2*(ss + tt)**2*(ss + 2*tt) + 16*Mm2**3*(3*ss**2 + 8*ss*tt + 3*tt*&
               &*2) + Mm2*ss*(9*ss**3 + 36*ss**2*tt + 44*ss*tt**2 + 16*tt**3) - Mm2**2*(29*ss**3&
               & + 106*ss**2*tt + 96*ss*tt**2 + 16*tt**3)) - me2**2*(80*Mm2**4*s1mm*s2nm + ss**2&
               &*(3*(s1mm*s2nm - s2nm*s3mm - 4*s1mm*s3nm + 5*s3mm*s3nm)*ss**2 + (4*s1mm*s2nm + 7&
               &*s2nm*s3mm - 29*s1mm*s3nm + 22*s3mm*s3nm)*ss*tt + 18*(s2nm*s3mm - s1mm*s3nm)*tt*&
               &*2) + 4*Mm2**2*((7*s1mm*s2nm - 12*s2nm*s3mm - 36*s1mm*s3nm + 48*s3mm*s3nm)*ss**2&
               & + 4*(-2*s1mm*(s2nm + 11*s3nm) + s3mm*(7*s2nm + 15*s3nm))*ss*tt + 48*(s2nm*s3mm &
               &- s1mm*s3nm)*tt**2) - Mm2*ss*((19*s1mm*s2nm - 23*s2nm*s3mm - 85*s1mm*s3nm + 108*&
               &s3mm*s3nm)*ss**2 + 2*(9*s1mm*s2nm + 29*s2nm*s3mm - 109*s1mm*s3nm + 80*s3mm*s3nm)&
               &*ss*tt + 144*(s2nm*s3mm - s1mm*s3nm)*tt**2) - 16*Mm2**3*(s1mm*s2nm*(2*ss - 7*tt)&
               & - 2*s1mm*s3nm*(ss + 2*tt) + s3mm*(-(s2nm*ss) + 3*s3nm*ss + 4*s2nm*tt))) + me2*(&
               &16*Mm2**5*s1mm*s2nm + 8*Mm2**4*((3*s1mm*s2nm - 2*s2nm*s3mm - 8*s1mm*s3nm + 10*s3&
               &mm*s3nm)*ss + 12*s2nm*s3mm*tt - 12*s1mm*(s2nm + s3nm)*tt) - 2*Mm2**3*((5*s1mm*s2&
               &nm + s2nm*s3mm - 69*s1mm*s3nm + 68*s3mm*s3nm)*ss**2 - 8*(8*s1mm*s2nm - 9*s2nm*s3&
               &mm + 23*s1mm*s3nm - 14*s3mm*s3nm)*ss*tt - 8*(5*s1mm*s2nm - 12*s2nm*s3mm + 12*s1m&
               &m*s3nm)*tt**2) - 2*Mm2*ss*((3*s1mm*s2nm - 3*s2nm*s3mm - 25*s1mm*s3nm + 28*s3mm*s&
               &3nm)*ss**3 + (7*s1mm*s2nm + 11*s2nm*s3mm - 91*s1mm*s3nm + 80*s3mm*s3nm)*ss**2*tt&
               & + (3*s1mm*s2nm + 55*s2nm*s3mm - 107*s1mm*s3nm + 52*s3mm*s3nm)*ss*tt**2 + 40*(s2&
               &nm*s3mm - s1mm*s3nm)*tt**3) + 2*Mm2**2*((4*s1mm*s2nm - 3*s2nm*s3mm - 66*s1mm*s3n&
               &m + 69*s3mm*s3nm)*ss**3 + (-3*s1mm*(4*s2nm + 73*s3nm) + s3mm*(43*s2nm + 176*s3nm&
               &))*ss**2*tt - 4*(5*s1mm*s2nm - 34*s2nm*s3mm + 52*s1mm*s3nm - 18*s3mm*s3nm)*ss*tt&
               &**2 + 48*(s2nm*s3mm - s1mm*s3nm)*tt**3) + ss**2*(s1mm*(ss + tt)*(s2nm*ss*(ss + 2&
               &*tt) - 2*s3nm*(3*ss**2 + 8*ss*tt + 5*tt**2)) + s3mm*(s3nm*ss*(7*ss**2 + 20*ss*tt&
               & + 14*tt**2) + s2nm*(-ss**3 + 2*ss**2*tt + 12*ss*tt**2 + 10*tt**3)))))/(me2*(4*M&
               &m2 - ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_discse = (32*me2**3*(s2nm*s3mm - s1mm*s3nm) + 2*ss**2*(11*Mm2*(-(s2nm*s3mm) + s1mm*(s2nm &
                   &+ s3nm)) - 3*s1mm*s2nm*ss - 11*s1mm*s3nm*(ss + tt) + 11*s3mm*(s3nm*ss + s2nm*tt)&
                   &) + me2*ss*(Mm2*(-74*s1mm*s2nm + 68*s2nm*s3mm - 68*s1mm*s3nm) + s3mm*(5*s2nm*ss &
                   &- 80*s3nm*ss - 68*s2nm*tt) + s1mm*(3*s2nm*ss + 75*s3nm*ss - 6*s2nm*tt + 68*s3nm*&
                   &tt)) + me2**2*(-32*Mm2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + s1mm*(6*s2nm*ss + 3&
                   &2*s3nm*tt) - 32*s3mm*(s3nm*ss + s2nm*(-ss + tt))))/(12.*me2*(4*me2 - ss)*ss**2)
    coeff_discse_lmm = ((2*me2 - ss)*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(s2&
                       &nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*(4&
                       &*me2 - ss)*ss)
    coeff_discse_lmu = -(((2*me2 - ss)*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(&
                       &s2nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*&
                       &(4*me2 - ss)*ss))
    coeff_discsm = -(1./6.)*(16*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + Mm2*ss*((-&
                   &17*s1mm*s2nm + 11*s2nm*s3mm - 39*s1mm*s3nm + 28*s3mm*s3nm)*ss - 4*(s2nm*s3mm - s&
                   &1mm*s3nm)*(4*me2 - 7*tt)) + 4*Mm2**2*((7*s1mm*s2nm - 7*s2nm*s3mm + 3*s1mm*s3nm +&
                   & 4*s3mm*s3nm)*ss - 4*(s2nm*s3mm - s1mm*s3nm)*(me2 - tt)) + ss**2*(2*me2*(s2nm*s3&
                   &mm - s1mm*s3nm) + 3*s1mm*s2nm*ss + 11*s1mm*s3nm*(ss + tt) - 11*s3mm*(s3nm*ss + s&
                   &2nm*tt)))/(me2*(4*Mm2 - ss)*ss**2)
    coeff_discsm_lmu = -(((2*Mm2 - ss)*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(&
                       &s2nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*&
                       &(4*Mm2 - ss)*ss))
    coeff_disct = (2*me2**5*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) + (s1mm - s3mm)*s3nm*ss) - &
                  &me2**4*(10*Mm2**2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + 2*Mm2*(3*(s1mm - s3mm)*s&
                  &3nm*ss + s1mm*s2nm*tt) + tt*((-(s1mm*s2nm) + s2nm*s3mm + 5*s1mm*s3nm - 6*s3mm*s3&
                  &nm)*ss + 14*(-(s2nm*s3mm) + s1mm*s3nm)*tt)) + me2**2*(-20*Mm2**4*(-(s2nm*s3mm) +&
                  & s1mm*(s2nm + s3nm)) - 8*Mm2*tt**2*(s3mm*(s2nm + 2*s3nm)*ss - s1mm*(s2nm + 3*s3n&
                  &m)*ss + 4*s2nm*s3mm*tt + s1mm*(s2nm - 4*s3nm)*tt) + 4*Mm2**3*((s1mm - s3mm)*s3nm&
                  &*ss - 6*s2nm*s3mm*tt + 3*s1mm*(s2nm + 2*s3nm)*tt) + Mm2**2*tt*((3*s1mm*s2nm - 3*&
                  &s2nm*s3mm - s1mm*s3nm + 4*s3mm*s3nm)*ss + 20*(-(s2nm*s3mm) + s1mm*s3nm)*tt) + tt&
                  &**2*(3*(s1mm*s2nm - s2nm*s3mm - s1mm*s3nm + 2*s3mm*s3nm)*ss**2 + (19*s1mm*s2nm +&
                  & 3*s2nm*s3mm - 3*s1mm*s3nm)*ss*tt + 24*(s2nm*s3mm - s1mm*s3nm)*tt**2)) + me2**3*&
                  &(20*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + Mm2*tt*((-3*s1mm*s2nm + 3*s2nm*&
                  &s3mm + 5*s1mm*s3nm - 8*s3mm*s3nm)*ss - 8*s2nm*s3mm*tt + 8*s1mm*s3nm*tt) + tt**2*&
                  &((-9*s1mm*s2nm + 7*s2nm*s3mm + s1mm*s3nm - 8*s3mm*s3nm)*ss + 36*(-(s2nm*s3mm) + &
                  &s1mm*s3nm)*tt) + 4*Mm2**2*(s1mm*s3nm*(ss - 2*tt) + s3mm*(-(s3nm*ss) + 2*s2nm*tt)&
                  &)) + me2*(10*Mm2**5*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + Mm2*tt**2*((-3*s1mm*s2&
                  &nm + 3*s2nm*s3mm - 7*s1mm*s3nm + 4*s3mm*s3nm)*ss**2 + (13*s1mm*s2nm + 11*s2nm*s3&
                  &mm - 11*s1mm*s3nm)*ss*tt + 2*(7*s1mm*s2nm + 9*s2nm*s3mm - 9*s1mm*s3nm)*tt**2) - &
                  &tt**3*((7*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*ss**2 + (15*s1mm*s2nm + 3*s2nm*&
                  &s3mm + 7*s1mm*s3nm - 10*s3mm*s3nm)*ss*tt + 4*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2) -&
                  & 2*Mm2**4*(8*s1mm*s2nm*tt + 3*s1mm*s3nm*(ss + 4*tt) - 3*s3mm*(s3nm*ss + 4*s2nm*t&
                  &t)) + Mm2**3*tt*(-8*s3mm*s3nm*ss + s2nm*s3mm*(ss + 8*tt) - s1mm*(s2nm*ss - 7*s3n&
                  &m*ss + 8*s3nm*tt)) - Mm2**2*tt**2*(8*s3mm*s3nm*ss + s2nm*s3mm*(ss + 44*tt) - s1m&
                  &m*(7*s2nm*ss + 9*s3nm*ss - 8*s2nm*tt + 44*s3nm*tt))) - 2*(Mm2 - tt)*(Mm2**5*(-(s&
                  &2nm*s3mm) + s1mm*(s2nm + s3nm)) - 2*Mm2**3*tt*((-s1mm + s3mm)*s3nm*ss + s1mm*s2n&
                  &m*tt) + Mm2**2*tt**2*(3*s1mm*(s2nm + s3nm)*ss - s3mm*(s2nm + 2*s3nm)*ss + 2*(3*s&
                  &1mm*s2nm - 4*s2nm*s3mm + 4*s1mm*s3nm)*tt) + tt**3*(ss + tt)*(2*s1mm*s2nm*ss + 3*&
                  &s1mm*s3nm*(ss + tt) - 3*s3mm*(s3nm*ss + s2nm*tt)) + Mm2**4*(s3mm*(s3nm*ss + 3*s2&
                  &nm*tt) - s1mm*(2*s2nm*tt + s3nm*(ss + 3*tt))) + Mm2*tt**2*(s3mm*(s3nm*ss*(ss + 6&
                  &*tt) + s2nm*tt*(4*ss + 9*tt)) - s1mm*(s2nm*tt*(7*ss + 3*tt) + s3nm*(ss**2 + 10*s&
                  &s*tt + 9*tt**2)))))/(4.*me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2)*tt*(me2**2 + M&
                  &m2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_disct_lmm = -(1./4.)*(tt*(-12*me2**6*(s2nm*s3mm - s1mm*s3nm) + me2**5*(6*Mm2*(5*s2nm*s3mm + s1m&
                      &m*(s2nm - 5*s3nm)) + (s1mm*s2nm - s2nm*s3mm - 9*s1mm*s3nm + 10*s3mm*s3nm)*ss + 6&
                      &6*(s2nm*s3mm - s1mm*s3nm)*tt) - me2**4*(6*Mm2**2*(3*s1mm*s2nm + s2nm*s3mm - s1mm&
                      &*s3nm) + Mm2*(3*s1mm*s2nm - 3*s2nm*s3mm - 23*s1mm*s3nm + 26*s3mm*s3nm)*ss + (s1m&
                      &m*s2nm - s2nm*s3mm - 4*s1mm*s3nm + 5*s3mm*s3nm)*ss**2 + 6*Mm2*(5*s1mm*s2nm + 6*s&
                      &2nm*s3mm - 6*s1mm*s3nm)*tt + (4*s1mm*s2nm + 19*s2nm*s3mm - 65*s1mm*s3nm + 46*s3m&
                      &m*s3nm)*ss*tt + 150*(s2nm*s3mm - s1mm*s3nm)*tt**2) + (Mm2 - tt)*(-(Mm2*s2nm*s3mm&
                      &) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(6*&
                      &Mm2**4 + 6*tt**2*(ss + tt)**2 - Mm2**3*(ss + 24*tt) - Mm2*tt*(2*ss**2 + 25*ss*tt&
                      & + 24*tt**2) + Mm2**2*(ss**2 + 14*ss*tt + 36*tt**2)) + me2**2*(12*Mm2**4*(2*s2nm&
                      &*s3mm + s1mm*(s2nm - 2*s3nm)) + 4*Mm2**3*(s3mm*(s2nm + s3nm)*ss - s1mm*(s2nm + 2&
                      &*s3nm)*ss - 12*s2nm*s3mm*tt + 3*s1mm*(s2nm + 4*s3nm)*tt) + Mm2**2*(2*(s1mm - s3m&
                      &m)*s3nm*ss**2 + (-11*s1mm*s2nm + 35*s2nm*s3mm - 47*s1mm*s3nm + 12*s3mm*s3nm)*ss*&
                      &tt + 12*(3*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**2) + tt*(3*(s1mm - s3mm)*s&
                      &3nm*ss**3 - (3*s1mm*s2nm + 10*s2nm*s3mm - 61*s1mm*s3nm + 51*s3mm*s3nm)*ss**2*tt &
                      &- (4*s1mm*s2nm + 101*s2nm*s3mm - 177*s1mm*s3nm + 76*s3mm*s3nm)*ss*tt**2 + 120*(-&
                      &(s2nm*s3mm) + s1mm*s3nm)*tt**3) + Mm2*((s1mm - s3mm)*s3nm*ss**3 + (-3*s1mm*s2nm &
                      &+ 2*s2nm*s3mm + 13*s1mm*s3nm - 15*s3mm*s3nm)*ss**2*tt + (-33*s1mm*s2nm + 70*s2nm&
                      &*s3mm - 130*s1mm*s3nm + 60*s3mm*s3nm)*ss*tt**2 - 60*(-4*s2nm*s3mm + s1mm*(s2nm +&
                      & 4*s3nm))*tt**3)) + me2**3*(12*Mm2**3*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm)) + (-&
                      &s1mm + s3mm)*s3nm*ss**3 + (3*s1mm*s2nm - 2*s2nm*s3mm - 25*s1mm*s3nm + 27*s3mm*s3&
                      &nm)*ss**2*tt + 3*(2*s1mm*s2nm + 25*s2nm*s3mm - 53*s1mm*s3nm + 28*s3mm*s3nm)*ss*t&
                      &t**2 + 180*(s2nm*s3mm - s1mm*s3nm)*tt**3 + Mm2*(2*(s1mm*s2nm - s2nm*s3mm - 3*s1m&
                      &m*s3nm + 4*s3mm*s3nm)*ss**2 + (15*s1mm*s2nm + 7*s2nm*s3mm - 31*s1mm*s3nm + 24*s3&
                      &mm*s3nm)*ss*tt + 12*(5*s1mm*s2nm - 9*s2nm*s3mm + 9*s1mm*s3nm)*tt**2) + 4*Mm2**2*&
                      &(5*s3mm*s3nm*ss - s2nm*s3mm*(ss + 9*tt) + s1mm*(-4*s3nm*ss + 9*s3nm*tt + s2nm*(s&
                      &s + 6*tt)))) + me2*(6*Mm2**5*(s2nm*s3mm - s1mm*(3*s2nm + s3nm)) + Mm2**4*((3*s1m&
                      &m*s2nm - 3*s2nm*s3mm + 17*s1mm*s3nm - 14*s3mm*s3nm)*ss + 6*(4*s1mm*s2nm + 3*s2nm&
                      &*s3mm - 3*s1mm*s3nm)*tt) - Mm2**3*(2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm))*ss**2 +&
                      & (15*s1mm*s2nm + 7*s2nm*s3mm + s1mm*s3nm - 8*s3mm*s3nm)*ss*tt - 12*(3*s1mm*s2nm &
                      &- 11*s2nm*s3mm + 11*s1mm*s3nm)*tt**2) + Mm2**2*((s1mm - s3mm)*s3nm*ss**3 + (3*s1&
                      &mm*s2nm - 4*s2nm*s3mm + 7*s1mm*s3nm - 3*s3mm*s3nm)*ss**2*tt + (-22*s1mm*s2nm + 8&
                      &1*s2nm*s3mm - 141*s1mm*s3nm + 60*s3mm*s3nm)*ss*tt**2 - 12*(6*s1mm*s2nm - 19*s2nm&
                      &*s3mm + 19*s1mm*s3nm)*tt**3) + Mm2*tt*(2*(-s1mm + s3mm)*s3nm*ss**3 + (5*s1mm*s2n&
                      &m - 17*s2nm*s3mm + 55*s1mm*s3nm - 38*s3mm*s3nm)*ss**2*tt + (33*s1mm*s2nm - 129*s&
                      &2nm*s3mm + 217*s1mm*s3nm - 88*s3mm*s3nm)*ss*tt**2 + 6*(5*s1mm*s2nm - 27*s2nm*s3m&
                      &m + 27*s1mm*s3nm)*tt**3) + tt**2*(-(s1mm*(ss + tt)*(-(s2nm*ss*tt) + s3nm*(8*ss**&
                      &2 + 50*ss*tt + 42*tt**2))) + s3mm*(s3nm*ss*(8*ss**2 + 41*ss*tt + 34*tt**2) + s2n&
                      &m*tt*(17*ss**2 + 58*ss*tt + 42*tt**2))))))/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt&
                      &)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_disct_lmu = (2*(me2 + Mm2 - tt)*tt*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2&
                      &*s1mm*(s2nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt)&
                      &)/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
    coeff_disct_ls = (tt*(me2**6*(-4*s2nm*s3mm + 4*s1mm*s3nm) + me2**5*(2*Mm2*(5*s2nm*s3mm + s1mm*(s2&
                     &nm - 5*s3nm)) + (s1mm*s2nm - s2nm*s3mm - 5*s1mm*s3nm + 6*s3mm*s3nm)*ss + 22*(s2n&
                     &m*s3mm - s1mm*s3nm)*tt) - me2**4*(2*Mm2**2*(3*s1mm*s2nm + s2nm*s3mm - s1mm*s3nm)&
                     & + Mm2*(3*s1mm*s2nm - 3*s2nm*s3mm - 11*s1mm*s3nm + 14*s3mm*s3nm)*ss + (s1mm*s2nm&
                     & - s2nm*s3mm - 4*s1mm*s3nm + 5*s3mm*s3nm)*ss**2 + 2*Mm2*(5*s1mm*s2nm + 6*s2nm*s3&
                     &mm - 6*s1mm*s3nm)*tt + (4*s1mm*s2nm + 3*s2nm*s3mm - 29*s1mm*s3nm + 26*s3mm*s3nm)&
                     &*ss*tt + 50*(s2nm*s3mm - s1mm*s3nm)*tt**2) + (Mm2 - tt)*(-(Mm2*s2nm*s3mm) + Mm2*&
                     &s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(2*Mm2**4 +&
                     & 2*tt**2*(ss + tt)**2 - Mm2**3*(ss + 8*tt) - Mm2*tt*(2*ss**2 + 9*ss*tt + 8*tt**2&
                     &) + Mm2**2*(ss**2 + 6*ss*tt + 12*tt**2)) + me2**2*(4*Mm2**4*(2*s2nm*s3mm + s1mm*&
                     &(s2nm - 2*s3nm)) - 4*Mm2**3*(s1mm*s2nm*ss + s3mm*(-s2nm + s3nm)*ss + 4*s2nm*s3mm&
                     &*tt - s1mm*(s2nm + 4*s3nm)*tt) + Mm2**2*(2*(s1mm - s3mm)*s3nm*ss**2 + (s3mm*(11*&
                     &s2nm + 4*s3nm) - 3*s1mm*(s2nm + 5*s3nm))*ss*tt + 4*(3*s1mm*s2nm - 8*s2nm*s3mm + &
                     &8*s1mm*s3nm)*tt**2) + tt*(3*(s1mm - s3mm)*s3nm*ss**3 - (3*s1mm*s2nm + 2*s2nm*s3m&
                     &m - 29*s1mm*s3nm + 27*s3mm*s3nm)*ss**2*tt - (4*s1mm*s2nm + 29*s2nm*s3mm - 65*s1m&
                     &m*s3nm + 36*s3mm*s3nm)*ss*tt**2 + 40*(-(s2nm*s3mm) + s1mm*s3nm)*tt**3) + Mm2*((s&
                     &1mm - s3mm)*s3nm*ss**3 + (-3*s1mm*s2nm + 2*s2nm*s3mm + 5*s1mm*s3nm - 7*s3mm*s3nm&
                     &)*ss**2*tt + (-9*s1mm*s2nm + 22*s2nm*s3mm - 58*s1mm*s3nm + 36*s3mm*s3nm)*ss*tt**&
                     &2 - 20*(-4*s2nm*s3mm + s1mm*(s2nm + 4*s3nm))*tt**3)) + me2**3*(4*Mm2**3*(-3*s2nm&
                     &*s3mm + s1mm*(s2nm + 3*s3nm)) + (-s1mm + s3mm)*s3nm*ss**3 + (3*s1mm*s2nm - 2*s2n&
                     &m*s3mm - 17*s1mm*s3nm + 19*s3mm*s3nm)*ss**2*tt + (6*s1mm*s2nm + 19*s2nm*s3mm - 6&
                     &3*s1mm*s3nm + 44*s3mm*s3nm)*ss*tt**2 + 60*(s2nm*s3mm - s1mm*s3nm)*tt**3 + Mm2*(2&
                     &*(s1mm*s2nm - s2nm*s3mm - 3*s1mm*s3nm + 4*s3mm*s3nm)*ss**2 + (7*s1mm*s2nm - s2nm&
                     &*s3mm - 7*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt + 4*(5*s1mm*s2nm - 9*s2nm*s3mm + 9*s1mm&
                     &*s3nm)*tt**2) + 4*Mm2**2*(3*s3mm*s3nm*ss - s2nm*s3mm*(ss + 3*tt) + s1mm*(-2*s3nm&
                     &*ss + 3*s3nm*tt + s2nm*(ss + 2*tt)))) + me2*(2*Mm2**5*(s2nm*s3mm - s1mm*(3*s2nm &
                     &+ s3nm)) + Mm2**3*(-2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm))*ss**2 + (-7*s1mm*s2nm &
                     &+ s2nm*s3mm + 7*s1mm*s3nm - 8*s3mm*s3nm)*ss*tt + 4*(3*s1mm*s2nm - 11*s2nm*s3mm +&
                     & 11*s1mm*s3nm)*tt**2) + Mm2**2*((s1mm - s3mm)*s3nm*ss**3 + (3*s1mm*s2nm - 4*s2nm&
                     &*s3mm - s1mm*s3nm + 5*s3mm*s3nm)*ss**2*tt + (-6*s1mm*s2nm + 25*s2nm*s3mm - 61*s1&
                     &mm*s3nm + 36*s3mm*s3nm)*ss*tt**2 - 4*(6*s1mm*s2nm - 19*s2nm*s3mm + 19*s1mm*s3nm)&
                     &*tt**3) + Mm2*tt*(2*(-s1mm + s3mm)*s3nm*ss**3 + (s1mm*s2nm - 5*s2nm*s3mm + 27*s1&
                     &mm*s3nm - 22*s3mm*s3nm)*ss**2*tt + (9*s1mm*(s2nm + 9*s3nm) - s3mm*(41*s2nm + 40*&
                     &s3nm))*ss*tt**2 + 2*(5*s1mm*s2nm - 27*s2nm*s3mm + 27*s1mm*s3nm)*tt**3) + Mm2**4*&
                     &(s3mm*(-3*s2nm*ss - 2*s3nm*ss + 6*s2nm*tt) + s1mm*(3*s2nm*ss + 5*s3nm*ss + 8*s2n&
                     &m*tt - 6*s3nm*tt)) + tt**2*(-(s1mm*(ss + tt)*(-(s2nm*ss*tt) + 2*s3nm*(2*ss**2 + &
                     &9*ss*tt + 7*tt**2))) + s3mm*(s3nm*ss*(4*ss**2 + 17*ss*tt + 14*tt**2) + s2nm*tt*(&
                     &5*ss**2 + 18*ss*tt + 14*tt**2))))))/(2.*me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2&
                     &)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_discu = -(1./2.)*(-20*me2**5*(s2nm*s3mm - s1mm*s3nm) - 2*me2**4*(-5*Mm2*s2nm*s3mm + Mm2*s1mm&
                  &*(s2nm + 5*s3nm) - 3*s1mm*(s2nm - 6*s3nm)*ss - 2*s3mm*(8*s2nm + s3nm)*ss + 35*(-&
                  &(s2nm*s3mm) + s1mm*s3nm)*tt) - me2**3*(8*Mm2**2*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s&
                  &3nm)) + (s1mm*(11*s2nm - 27*s3nm) + 9*s3mm*(s2nm + 2*s3nm))*ss**2 + (19*s1mm*s2n&
                  &m + 109*s2nm*s3mm - 117*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt + 90*(s2nm*s3mm - s1mm*s3&
                  &nm)*tt**2 - 2*Mm2*(9*s3mm*(-2*s2nm + s3nm)*ss + 3*s1mm*(s2nm + 3*s3nm)*ss + (3*s&
                  &1mm*s2nm + 31*s2nm*s3mm - 31*s1mm*s3nm)*tt)) - me2*(4*Mm2**4*(2*s1mm*s2nm + 5*s2&
                  &nm*s3mm - 5*s1mm*s3nm) + 2*Mm2**3*(s3mm*(-22*s2nm + s3nm)*ss + 3*s1mm*(s2nm + 7*&
                  &s3nm)*ss + (-13*s1mm*s2nm - 29*s2nm*s3mm + 29*s1mm*s3nm)*tt) + 2*Mm2*(ss + tt)*(&
                  &(2*s1mm*s2nm + 13*s1mm*s3nm - 13*s3mm*s3nm)*ss**2 + (-15*s1mm*s2nm - 37*s2nm*s3m&
                  &m + 32*s1mm*s3nm + 5*s3mm*s3nm)*ss*tt - (19*s2nm*s3mm + s1mm*(s2nm - 19*s3nm))*t&
                  &t**2) + Mm2**2*((-11*s1mm*s2nm + 19*s2nm*s3mm - 45*s1mm*s3nm + 26*s3mm*s3nm)*ss*&
                  &*2 + 3*(11*s1mm*s2nm + 41*s2nm*s3mm - 37*s1mm*s3nm - 4*s3mm*s3nm)*ss*tt + 2*(10*&
                  &s1mm*s2nm + 33*s2nm*s3mm - 33*s1mm*s3nm)*tt**2) + (ss + tt)**2*((s1mm*s2nm - s2n&
                  &m*s3mm - 7*s1mm*s3nm + 8*s3mm*s3nm)*ss**2 + (11*s1mm*s2nm + 17*s2nm*s3mm - 17*s1&
                  &mm*s3nm)*ss*tt + 10*(s2nm*s3mm - s1mm*s3nm)*tt**2)) - (Mm2 - ss - tt)*(2*Mm2**4*&
                  &(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) - 2*Mm2**3*(s1mm*s2nm*ss + s3mm*(s2nm - s3nm&
                  &)*ss + (2*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt) + Mm2*(ss + tt)*((-(s3mm*(s&
                  &2nm + 3*s3nm)) + s1mm*(s2nm + 4*s3nm))*ss**2 + (-7*s1mm*s2nm - 4*s2nm*s3mm + 2*s&
                  &1mm*s3nm + 2*s3mm*s3nm)*ss*tt + 2*(s2nm*s3mm - s1mm*s3nm)*tt**2) + Mm2**2*((s3mm&
                  &*(3*s2nm + 2*s3nm) - s1mm*(s2nm + 5*s3nm))*ss**2 + (9*s1mm*s2nm + 3*s2nm*s3mm + &
                  &s1mm*s3nm - 4*s3mm*s3nm)*ss*tt + 2*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm))*tt**2) &
                  &- ss*(ss + tt)**2*(-2*s1mm*s2nm*tt + s1mm*s3nm*(ss + tt) - s3mm*(s3nm*ss + s2nm*&
                  &tt))) + me2**2*(4*Mm2**3*(5*s1mm*s2nm + s2nm*s3mm - s1mm*s3nm) + Mm2*((s1mm*s2nm&
                  & + 33*s2nm*s3mm + 19*s1mm*s3nm - 52*s3mm*s3nm)*ss**2 - (29*s1mm*s2nm + 121*s2nm*&
                  &s3mm - 131*s1mm*s3nm + 10*s3mm*s3nm)*ss*tt - 2*(3*s1mm*s2nm + 56*s2nm*s3mm - 56*&
                  &s1mm*s3nm)*tt**2) + (ss + tt)*((6*s1mm*s2nm - 4*s2nm*s3mm - 17*s1mm*s3nm + 21*s3&
                  &mm*s3nm)*ss**2 + (22*s1mm*s2nm + 63*s2nm*s3mm - 67*s1mm*s3nm + 4*s3mm*s3nm)*ss*t&
                  &t + 50*(s2nm*s3mm - s1mm*s3nm)*tt**2) - 2*Mm2**2*(s3mm*(20*s2nm*ss - 23*s3nm*ss &
                  &- 37*s2nm*tt) + s1mm*(5*s2nm*ss + 3*s3nm*ss - 13*s2nm*tt + 37*s3nm*tt))))/(me2*s&
                  &s*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(M&
                  &m2 + tt) + tt*(ss + tt)))
    coeff_discu_lmm = -(1./4.)*((2*me2 + 2*Mm2 - ss - tt)*(-12*me2**6*(s2nm*s3mm - s1mm*s3nm) + me2**5*(3&
                      &0*Mm2*s2nm*s3mm + 6*Mm2*s1mm*(s2nm - 5*s3nm) + s1mm*(s2nm - 25*s3nm)*ss + 5*s3mm&
                      &*(3*s2nm + 2*s3nm)*ss + 66*(s2nm*s3mm - s1mm*s3nm)*tt) - me2**4*(6*Mm2**2*(3*s1m&
                      &m*s2nm + s2nm*s3mm - s1mm*s3nm) + Mm2*(13*s1mm*s2nm + 35*s2nm*s3mm - 61*s1mm*s3n&
                      &m + 26*s3mm*s3nm)*ss + (4*s1mm*s2nm + 2*s2nm*s3mm - 27*s1mm*s3nm + 25*s3mm*s3nm)&
                      &*ss**2 + 6*Mm2*(5*s1mm*s2nm + 6*s2nm*s3mm - 6*s1mm*s3nm)*tt + (4*s1mm*s2nm + 91*&
                      &s2nm*s3mm - 137*s1mm*s3nm + 46*s3mm*s3nm)*ss*tt + 150*(s2nm*s3mm - s1mm*s3nm)*tt&
                      &**2) + (-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(&
                      &s3nm*ss + s2nm*tt))*(6*Mm2**5 - 3*Mm2**4*(3*ss + 10*tt) - (ss + tt)**3*(ss**2 + &
                      &2*ss*tt + 6*tt**2) + Mm2*(ss + tt)**2*(4*ss**2 + 9*ss*tt + 30*tt**2) + Mm2**3*(7&
                      &*ss**2 + 47*ss*tt + 60*tt**2) - Mm2**2*(7*ss**3 + 34*ss**2*tt + 87*ss*tt**2 + 60&
                      &*tt**3)) + me2**3*(12*Mm2**3*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm)) + (6*s1mm*s2n&
                      &m - 4*s2nm*s3mm - 27*s1mm*s3nm + 31*s3mm*s3nm)*ss**3 + (12*s1mm*s2nm + 39*s2nm*s&
                      &3mm - 134*s1mm*s3nm + 95*s3mm*s3nm)*ss**2*tt + (6*s1mm*s2nm + 203*s2nm*s3mm - 28&
                      &7*s1mm*s3nm + 84*s3mm*s3nm)*ss*tt**2 + 180*(s2nm*s3mm - s1mm*s3nm)*tt**3 + Mm2*(&
                      &(5*s1mm*s2nm + 5*s2nm*s3mm - 13*s1mm*s3nm + 8*s3mm*s3nm)*ss**2 + (53*s1mm*(s2nm &
                      &- s3nm) + s3mm*(29*s2nm + 24*s3nm))*ss*tt + 12*(5*s1mm*s2nm - 9*s2nm*s3mm + 9*s1&
                      &mm*s3nm)*tt**2) + Mm2**2*(20*s3mm*s3nm*ss + 6*s2nm*s3mm*(5*ss - 6*tt) + s1mm*(26&
                      &*s2nm*ss - 50*s3nm*ss + 24*s2nm*tt + 36*s3nm*tt))) + me2*(6*Mm2**5*(s2nm*s3mm - &
                      &s1mm*(3*s2nm + s3nm)) - Mm2**3*((s3mm*(s2nm - 8*s3nm) + s1mm*(s2nm + 7*s3nm))*ss&
                      &**2 + (s3mm*(25*s2nm - 8*s3nm) + 17*s1mm*(s2nm - s3nm))*ss*tt - 12*(3*s1mm*s2nm &
                      &- 11*s2nm*s3mm + 11*s1mm*s3nm)*tt**2) + Mm2**2*((2*s2nm*s3mm - 27*s1mm*s3nm + 25&
                      &*s3mm*s3nm)*ss**3 + (29*s2nm*s3mm - 54*s1mm*s3nm + 25*s3mm*s3nm)*ss**2*tt - 3*(2&
                      &4*s1mm*s2nm - 65*s2nm*s3mm + 85*s1mm*s3nm - 20*s3mm*s3nm)*ss*tt**2 - 12*(6*s1mm*&
                      &s2nm - 19*s2nm*s3mm + 19*s1mm*s3nm)*tt**3) + Mm2**4*(s3mm*(-13*s2nm*ss - 14*s3nm&
                      &*ss + 18*s2nm*tt) + 3*s1mm*(7*s2nm*ss + 9*s3nm*ss + 8*s2nm*tt - 6*s3nm*tt)) + Mm&
                      &2*(ss + tt)*(s1mm*(ss + tt)*(s2nm*tt*(7*ss + 30*tt) + s3nm*(22*ss**2 + 27*ss*tt &
                      &+ 162*tt**2)) - s3mm*(2*s3nm*ss*(11*ss**2 + 15*ss*tt + 44*tt**2) + s2nm*tt*(19*s&
                      &s**2 + 101*ss*tt + 162*tt**2))) + (ss + tt)**2*(s1mm*(ss + tt)*(s2nm*ss*(ss + tt&
                      &) - 7*s3nm*(ss**2 + 2*ss*tt + 6*tt**2)) + s3mm*(s3nm*ss*(8*ss**2 + 17*ss*tt + 34&
                      &*tt**2) + s2nm*(-ss**3 + 4*ss**2*tt + 22*ss*tt**2 + 42*tt**3)))) + me2**2*(12*Mm&
                      &2**4*(2*s2nm*s3mm + s1mm*(s2nm - 2*s3nm)) + Mm2**2*((-7*s1mm*s2nm + 5*s2nm*s3mm &
                      &+ 41*s1mm*s3nm - 46*s3mm*s3nm)*ss**2 + (-15*s1mm*s2nm + 47*s2nm*s3mm - 59*s1mm*s&
                      &3nm + 12*s3mm*s3nm)*ss*tt + 12*(3*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**2) &
                      &+ Mm2**3*(4*s3mm*s3nm*ss - 6*s2nm*s3mm*(ss + 8*tt) + s1mm*(-26*s2nm*ss + 2*s3nm*&
                      &ss + 12*s2nm*tt + 48*s3nm*tt)) + Mm2*(s1mm*(ss + tt)*(s2nm*(ss**2 - 27*ss*tt - 6&
                      &0*tt**2) - 4*s3nm*(7*ss**2 + 60*tt**2)) + s3mm*(3*s3nm*ss*(11*ss**2 + 11*ss*tt +&
                      & 20*tt**2) - 5*s2nm*(ss**3 + ss**2*tt - 36*ss*tt**2 - 48*tt**3))) - (ss + tt)*(s&
                      &1mm*(ss + tt)*(4*s2nm*ss*(ss + tt) - s3nm*(19*ss**2 + 49*ss*tt + 120*tt**2)) + s&
                      &3mm*(s3nm*ss*(23*ss**2 + 59*ss*tt + 76*tt**2) + s2nm*(-4*ss**3 + 9*ss**2*tt + 93&
                      &*ss*tt**2 + 120*tt**3))))))/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)*(me2&
                      &**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_discu_lmu = (2*(me2 + Mm2 - ss - tt)*(2*me2 + 2*Mm2 - ss - tt)*(-2*me2*s2nm*s3mm - Mm2*s2nm*&
                      &s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + &
                      &s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2))
    coeff_discu_ls = ((2*me2 + 2*Mm2 - ss - tt)*(me2**6*(-4*s2nm*s3mm + 4*s1mm*s3nm) + me2**5*(2*Mm2*&
                     &(5*s2nm*s3mm + s1mm*(s2nm - 5*s3nm)) + (s1mm*s2nm + 7*s2nm*s3mm - 13*s1mm*s3nm +&
                     & 6*s3mm*s3nm)*ss + 22*(s2nm*s3mm - s1mm*s3nm)*tt) - me2**4*(2*Mm2**2*(3*s1mm*s2n&
                     &m + s2nm*s3mm - s1mm*s3nm) + (4*s1mm*s2nm + 2*s2nm*s3mm - 23*s1mm*s3nm + 21*s3mm&
                     &*s3nm)*ss**2 + (4*s1mm*s2nm + 39*s2nm*s3mm - 65*s1mm*s3nm + 26*s3mm*s3nm)*ss*tt &
                     &+ 50*(s2nm*s3mm - s1mm*s3nm)*tt**2 + Mm2*(3*s1mm*(3*s2nm - 7*s3nm)*ss + 7*s3mm*(&
                     &s2nm + 2*s3nm)*ss + 2*(5*s1mm*s2nm + 6*s2nm*s3mm - 6*s1mm*s3nm)*tt)) + (-(Mm2*s2&
                     &nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*t&
                     &t))*(2*Mm2**5 - 5*Mm2**4*(ss + 2*tt) - (ss + tt)**3*(ss**2 + 2*ss*tt + 2*tt**2) &
                     &+ Mm2*(ss + tt)**2*(4*ss**2 + 9*ss*tt + 10*tt**2) + Mm2**3*(7*ss**2 + 23*ss*tt +&
                     & 20*tt**2) - Mm2**2*(7*ss**3 + 26*ss**2*tt + 39*ss*tt**2 + 20*tt**3)) + me2**3*(&
                     &4*Mm2**3*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm)) + 2*Mm2**2*(6*s3mm*s3nm*ss - s2nm&
                     &*s3mm*(ss + 6*tt) + s1mm*(5*s2nm*ss - 5*s3nm*ss + 4*s2nm*tt + 6*s3nm*tt)) + Mm2*&
                     &(8*s3mm*s3nm*ss*(-ss + tt) + s2nm*s3mm*(5*ss**2 - 11*ss*tt - 36*tt**2) + 3*s1mm*&
                     &s3nm*(ss**2 + ss*tt + 12*tt**2) + s1mm*s2nm*(5*ss**2 + 29*ss*tt + 20*tt**2)) + s&
                     &1mm*(ss + tt)*(6*s2nm*ss*(ss + tt) - s3nm*(27*ss**2 + 67*ss*tt + 60*tt**2)) + s3&
                     &mm*(s3nm*ss*(31*ss**2 + 71*ss*tt + 44*tt**2) + s2nm*(-4*ss**3 + 23*ss**2*tt + 83&
                     &*ss*tt**2 + 60*tt**3))) + me2*(2*Mm2**5*(s2nm*s3mm - s1mm*(3*s2nm + s3nm)) - Mm2&
                     &**3*((s1mm*(s2nm - 9*s3nm) + s3mm*(s2nm + 8*s3nm))*ss**2 + (-7*s1mm*s2nm + 17*s2&
                     &nm*s3mm - 25*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt - 4*(3*s1mm*s2nm - 11*s2nm*s3mm + 11&
                     &*s1mm*s3nm)*tt**2) + Mm2**2*((2*s2nm*s3mm - 27*s1mm*s3nm + 25*s3mm*s3nm)*ss**3 +&
                     & (-16*s1mm*s2nm + 29*s2nm*s3mm - 78*s1mm*s3nm + 49*s3mm*s3nm)*ss**2*tt + (-40*s1&
                     &mm*s2nm + 91*s2nm*s3mm - 127*s1mm*s3nm + 36*s3mm*s3nm)*ss*tt**2 - 4*(6*s1mm*s2nm&
                     & - 19*s2nm*s3mm + 19*s1mm*s3nm)*tt**3) + Mm2**4*(s3mm*(-5*s2nm*ss - 2*s3nm*ss + &
                     &6*s2nm*tt) + s1mm*(5*s2nm*ss + 7*s3nm*ss + 8*s2nm*tt - 6*s3nm*tt)) + Mm2*(ss + t&
                     &t)*(s1mm*(ss + tt)*(s2nm*tt*(7*ss + 10*tt) + s3nm*(22*ss**2 + 43*ss*tt + 54*tt**&
                     &2)) - s3mm*(2*s3nm*ss*(11*ss**2 + 23*ss*tt + 20*tt**2) + s2nm*tt*(19*ss**2 + 57*&
                     &ss*tt + 54*tt**2))) + (ss + tt)**2*(s1mm*(ss + tt)*(s2nm*ss*(ss + tt) - 7*s3nm*(&
                     &ss**2 + 2*ss*tt + 2*tt**2)) + s3mm*(s3nm*ss*(8*ss**2 + 17*ss*tt + 14*tt**2) + s2&
                     &nm*(-ss**3 + 4*ss**2*tt + 14*ss*tt**2 + 14*tt**3)))) + me2**2*(4*Mm2**4*(2*s2nm*&
                     &s3mm + s1mm*(s2nm - 2*s3nm)) - 2*Mm2**3*((s1mm*s2nm - s2nm*s3mm - s1mm*s3nm + 2*&
                     &s3mm*s3nm)*ss + 8*s2nm*s3mm*tt - 2*s1mm*(s2nm + 4*s3nm)*tt) + Mm2**2*((-7*s1mm*s&
                     &2nm + 5*s2nm*s3mm + 17*s1mm*s3nm - 22*s3mm*s3nm)*ss**2 + (9*s1mm*s2nm - s2nm*s3m&
                     &m - 3*s1mm*s3nm + 4*s3mm*s3nm)*ss*tt + 4*(3*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3n&
                     &m)*tt**2) - (ss + tt)*(s1mm*(ss + tt)*(4*s2nm*ss*(ss + tt) - s3nm*(19*ss**2 + 41&
                     &*ss*tt + 40*tt**2)) + s3mm*(s3nm*ss*(23*ss**2 + 51*ss*tt + 36*tt**2) + s2nm*(-4*&
                     &ss**3 + 9*ss**2*tt + 45*ss*tt**2 + 40*tt**3))) + Mm2*(s1mm*(ss + tt)*(s2nm*(ss**&
                     &2 - 19*ss*tt - 20*tt**2) - 4*s3nm*(7*ss**2 + 12*ss*tt + 20*tt**2)) + s3mm*(3*s3n&
                     &m*ss*(11*ss**2 + 19*ss*tt + 12*tt**2) + s2nm*(-5*ss**3 + 19*ss**2*tt + 92*ss*tt*&
                     &*2 + 80*tt**3))))))/(2.*me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)*(me2**2 +&
                     & Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_lmm = (12*me2**5*(4*Mm2 - ss)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) + (s1mm - s3m&
                &m)*s3nm*ss) - 2*me2**4*(4*Mm2 - ss)*(18*Mm2**2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm&
                &)) + 3*(s1mm - s3mm)*s3nm*ss**2 - 3*(s1mm*s2nm - s2nm*s3mm - 4*s1mm*s3nm + 5*s3m&
                &m*s3nm)*ss*tt + 38*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2 + 3*Mm2*(-(s3mm*(s2nm + 2*s3&
                &nm)*ss) + s1mm*(s2nm + 3*s3nm)*ss + s2nm*s3mm*tt + s1mm*(s2nm - s3nm)*tt)) + 2*(&
                &-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss &
                &+ s2nm*tt))*(24*Mm2**6 + ss*(3*ss - 7*tt)*tt**2*(ss + tt)**2 - 6*Mm2**5*(3*ss + &
                &10*tt) + Mm2**4*(3*ss**2 + 39*ss*tt + 16*tt**2) - 4*Mm2**2*tt**2*(7*ss**2 + 39*s&
                &s*tt + 22*tt**2) + Mm2**3*(-6*ss**2*tt + 60*ss*tt**2 + 80*tt**3) + Mm2*(-6*ss**3&
                &*tt**2 + 56*ss**2*tt**3 + 90*ss*tt**4 + 28*tt**5)) + me2**3*(96*Mm2**4*(-(s2nm*s&
                &3mm) + s1mm*(s2nm + s3nm)) - 8*Mm2**2*(3*(s1mm*s2nm - s2nm*s3mm - 2*s1mm*s3nm + &
                &3*s3mm*s3nm)*ss**2 + 3*(s1mm*s2nm - s2nm*s3mm - s1mm*s3nm + 2*s3mm*s3nm)*ss*tt +&
                & (38*s1mm*s2nm + 9*s2nm*s3mm - 9*s1mm*s3nm)*tt**2) + ss*tt*(3*(s1mm*s2nm - s2nm*&
                &s3mm - 3*s1mm*s3nm + 4*s3mm*s3nm)*ss**2 + (15*s1mm*s2nm - 13*s2nm*s3mm - 63*s1mm&
                &*s3nm + 76*s3mm*s3nm)*ss*tt + 218*(s2nm*s3mm - s1mm*s3nm)*tt**2) - 2*Mm2*(6*(s1m&
                &m - s3mm)*s3nm*ss**3 + 3*(s1mm*(s2nm - 6*s3nm) + 6*s3mm*s3nm)*ss**2*tt + (-8*s1m&
                &m*s2nm + 13*s2nm*s3mm - 165*s1mm*s3nm + 152*s3mm*s3nm)*ss*tt**2 + 436*(s2nm*s3mm&
                & - s1mm*s3nm)*tt**3) + 24*Mm2**3*(3*s1mm*s2nm*ss - s1mm*s3nm*(ss + 4*tt) + s3mm*&
                &(-3*s2nm*ss + 4*s3nm*ss + 4*s2nm*tt))) + me2*(-144*Mm2**6*(-(s2nm*s3mm) + s1mm*(&
                &s2nm + s3nm)) + 12*Mm2**5*((11*s1mm*s2nm - 11*s2nm*s3mm + 15*s1mm*s3nm - 4*s3mm*&
                &s3nm)*ss + 8*(2*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt) - 4*Mm2**4*(3*(2*s1mm&
                &*s2nm - 2*s2nm*s3mm + 7*s1mm*s3nm - 5*s3mm*s3nm)*ss**2 + 6*(5*s1mm*s2nm - 9*s2nm&
                &*s3mm + 11*s1mm*s3nm - 2*s3mm*s3nm)*ss*tt + 2*(46*s1mm*s2nm - 57*s2nm*s3mm + 57*&
                &s1mm*s3nm)*tt**2) + 2*Mm2*tt**2*((-24*s1mm*s2nm + 21*s2nm*s3mm - 23*s1mm*s3nm + &
                &2*s3mm*s3nm)*ss**3 + 2*(5*s1mm*s2nm - 35*s2nm*s3mm + 166*s1mm*s3nm - 131*s3mm*s3&
                &nm)*ss**2*tt + (34*s1mm*s2nm - 383*s2nm*s3mm + 543*s1mm*s3nm - 160*s3mm*s3nm)*ss&
                &*tt**2 + 188*(-(s2nm*s3mm) + s1mm*s3nm)*tt**3) + 2*Mm2**3*(6*(s1mm - s3mm)*s3nm*&
                &ss**3 + 3*(-2*s3mm*(2*s2nm + 5*s3nm) + s1mm*(s2nm + 14*s3nm))*ss**2*tt + (80*s1m&
                &m*s2nm - 143*s2nm*s3mm + 327*s1mm*s3nm - 184*s3mm*s3nm)*ss*tt**2 + 36*(8*s1mm*s2&
                &nm - 13*s2nm*s3mm + 13*s1mm*s3nm)*tt**3) + Mm2**2*tt*(3*(s1mm*s2nm - s2nm*s3mm -&
                & 3*s1mm*s3nm + 4*s3mm*s3nm)*ss**3 + (79*s1mm*s2nm - 53*s2nm*s3mm - 215*s1mm*s3nm&
                & + 268*s3mm*s3nm)*ss**2*tt + 2*(-232*s1mm*s2nm + 429*s2nm*s3mm - 741*s1mm*s3nm +&
                & 312*s3mm*s3nm)*ss*tt**2 - 40*(8*s1mm*s2nm - 25*s2nm*s3mm + 25*s1mm*s3nm)*tt**3)&
                & + ss*tt**2*(ss + tt)*(8*s3mm*s3nm*ss*(-3*ss + 10*tt) + s1mm*(ss + tt)*(3*s2nm*s&
                &s + 27*s3nm*ss - 94*s3nm*tt) + s2nm*s3mm*(-3*ss**2 - 13*ss*tt + 94*tt**2))) + 2*&
                &me2**2*(48*Mm2**5*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) - 12*Mm2**4*((7*s1mm*s2nm &
                &- 7*s2nm*s3mm + 3*s1mm*s3nm + 4*s3mm*s3nm)*ss - 2*s2nm*s3mm*tt + 2*s1mm*(s2nm + &
                &s3nm)*tt) + 2*Mm2**3*(3*(-3*s2nm*s3mm + 2*s3mm*s3nm + s1mm*(3*s2nm + s3nm))*ss**&
                &2 + 9*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm))*ss*tt - 2*(16*s1mm*s2nm - 29*s2nm*s3&
                &mm + 29*s1mm*s3nm)*tt**2) + Mm2**2*tt*(3*(3*s1mm*s2nm - 8*s1mm*s3nm + 8*s3mm*s3n&
                &m)*ss**2 + (44*s1mm*s2nm + 23*s2nm*s3mm + 41*s1mm*s3nm - 64*s3mm*s3nm)*ss*tt + 8&
                &*(34*s1mm*s2nm - 63*s2nm*s3mm + 63*s1mm*s3nm)*tt**2) + Mm2*tt*((-3*s1mm*s2nm + 3&
                &*s2nm*s3mm + 3*s1mm*s3nm - 6*s3mm*s3nm)*ss**3 + (29*s1mm*s2nm - 37*s2nm*s3mm - 1&
                &9*s1mm*s3nm + 56*s3mm*s3nm)*ss**2*tt + 2*(-22*s1mm*s2nm + 199*s2nm*s3mm - 323*s1&
                &mm*s3nm + 124*s3mm*s3nm)*ss*tt**2 + 444*(s2nm*s3mm - s1mm*s3nm)*tt**3) - ss*tt**&
                &2*(-2*s3mm*s3nm*ss*(ss - 31*tt) + s1mm*(ss + tt)*(6*s2nm*ss + 11*s3nm*ss - 111*s&
                &3nm*tt) + s2nm*s3mm*(-9*ss**2 + 38*ss*tt + 111*tt**2))))/(24.*me2*(4*Mm2 - ss)*s&
                &s*tt**2*(-2*me2 - 2*Mm2 + ss + tt)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt&
                &) + tt*(ss + tt)))
    coeff_lmu = (10*me2*s2nm*s3mm + 7*Mm2*s2nm*s3mm - 10*me2*s1mm*s3nm - 7*Mm2*s1mm*(s2nm + s3nm&
                &) + 7*s1mm*s3nm*ss - 7*s3mm*s3nm*ss - 7*s2nm*s3mm*tt + 7*s1mm*s3nm*tt)/(6.*me2*s&
                &s)
    coeff_ls = (me2**3*(8*Mm2*s2nm*s3mm - 8*Mm2*s1mm*(s2nm + s3nm) + 2*(s2nm*s3mm + s1mm*(s2nm &
               &- s3nm))*ss) + ss**2*(-3*Mm2 + ss + tt)*(Mm2*s2nm*s3mm - Mm2*s1mm*(s2nm + s3nm) &
               &+ s1mm*s3nm*(ss + tt) - s3mm*(s3nm*ss + s2nm*tt)) + me2**2*(16*Mm2**2*(-(s2nm*s3&
               &mm) + s1mm*(s2nm + s3nm)) + ss*(s3mm*(3*s2nm*ss - 12*s3nm*ss - 4*s2nm*tt) + s1mm&
               &*(-3*s2nm*ss + 9*s3nm*ss - 4*s2nm*tt + 4*s3nm*tt)) + 4*Mm2*(s3mm*(-3*s2nm*ss + 8&
               &*s3nm*ss - 4*s2nm*tt) + s1mm*(s2nm*ss - 5*s3nm*ss + 4*s2nm*tt + 4*s3nm*tt))) + m&
               &e2*(-8*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + 2*Mm2**2*(s2nm*s3mm*(5*ss - &
               &8*tt) + s1mm*(-3*s2nm*ss - 5*s3nm*ss + 8*s2nm*tt + 8*s3nm*tt)) + 4*Mm2*(-5*s3mm*&
               &s3nm*ss**2 - s2nm*s3mm*(ss - 2*tt)*tt - s1mm*s2nm*tt*(3*ss + 2*tt) + s1mm*s3nm*(&
               &5*ss**2 + ss*tt - 2*tt**2)) + ss*(s1mm*(ss + tt)*(-2*s3nm*(3*ss + tt) + s2nm*(ss&
               & + 2*tt)) + s3mm*(s3nm*ss*(7*ss + 4*tt) + s2nm*(-ss**2 + 4*ss*tt + 2*tt**2)))))/&
               &(2.*me2*(4*me2 - ss)*(-4*Mm2 + ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt&
               &) + tt*(ss + tt)))
    coeff_unity = -(1./18.)*(-24*me2**2*(s2nm*s3mm - s1mm*s3nm)*tt + 3*Mm2**2*(-(s2nm*s&
                  &3mm) + s1mm*(s2nm + s3nm))*(3*ss + 8*tt) + Mm2*(9*(-s1mm + s3mm)*s3nm*ss**2 + 2*&
                  &(76*s1mm*s2nm + 5*s2nm*s3mm - 17*s1mm*s3nm + 12*s3mm*s3nm)*ss*tt + 24*(s2nm*s3mm&
                  & - s1mm*s3nm)*tt**2) - ss*tt*(-(s1mm*s3nm*(ss + tt)) + 3*s1mm*s2nm*(17*ss + 39*t&
                  &t) + s3mm*(s3nm*ss + s2nm*tt)) + me2*(9*(-s1mm + s3mm)*s3nm*ss**2 + (117*s1mm*s2&
                  &nm - 107*s2nm*s3mm + 191*s1mm*s3nm - 84*s3mm*s3nm)*ss*tt + 24*(s2nm*s3mm - s1mm*&
                  &s3nm)*tt**2 + Mm2*(9*s2nm*s3mm*ss - 9*s1mm*(s2nm + s3nm)*ss + 24*(-2*s2nm*s3mm +&
                  & s1mm*(s2nm + 2*s3nm))*tt)))/(me2*ss**2*tt)
    coeffLL = coeffLL + c6se*coeff_c6se + c6sm*coeff_c6sm + c6t*coeff_c6t  &
            + c6u*coeff_c6u + ce*coeff_ce + cm*coeff_cm  &
            + coeff_discse*discse + coeff_discse_lmm*discse*lmm + coeff_discse_lmu*discse*lmu  &
            + coeff_discsm*discsm + coeff_discsm_lmu*discsm*lmu + coeff_disct*disct  &
            + coeff_disct_lmm*disct*lmm + coeff_disct_lmu*disct*lmu + coeff_disct_ls*disctLs  &
            + coeff_discu*discu + coeff_discu_lmm*discu*lmm + coeff_discu_lmu*discu*lmu  &
            + coeff_discu_ls*discuLs + coeff_lmm*lmm + coeff_lmu*lmu  &
            + coeff_ls*ls + coeff_unity
  endif

  END FUNCTION COEFFLL


  PURE FUNCTION COEFFLR(Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm, &
        discu, disct, discse, discsm, c6se, c6sm, c6u, c6t, ce, cm, lmm, lmu, ls, &
        disctLs, discuLs, pol)
  real(kind=prec), intent(in) :: Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm
  real(kind=prec), intent(in) :: discu, disct, discse, discsm, c6se, c6sm, c6u, c6t, ce, cm, lmm, lmu, ls
  real(kind=prec), intent(in) :: disctLs, discuLs
  real(kind=prec) :: coeff_c6se, coeff_c6sm, coeff_c6t, coeff_c6u, coeff_ce
  real(kind=prec) :: coeff_cm, coeff_discse, coeff_discse_lmm, coeff_discse_lmu, coeff_discsm
  real(kind=prec) :: coeff_discsm_lmu, coeff_disct, coeff_disct_lmm, coeff_disct_lmu, coeff_disct_ls
  real(kind=prec) :: coeff_discu, coeff_discu_lmm, coeff_discu_lmu, coeff_discu_ls, coeff_lmm
  real(kind=prec) :: coeff_lmu, coeff_ls, coeff_unity
  logical, intent(in) :: pol
  real(kind=prec) :: coeffLR

  coeffLR = 0.
  coeff_c6se = (2*(2*me2 - ss)*(me2**2 + Mm2**2 + Mm2*(ss - 2*tt) + me2*(2*Mm2 + ss - 2*tt) + t&
               &t**2))/ss
  coeff_c6sm = (2*(2*Mm2 - ss)*(me2**2 + Mm2**2 + Mm2*(ss - 2*tt) + me2*(2*Mm2 + ss - 2*tt) + t&
               &t**2))/ss
  coeff_c6t = (2*me2**3 + 3*me2**2*(2*Mm2 + ss - 2*tt) + me2*(6*Mm2**2 + 2*Mm2*(5*ss - 6*tt) -&
              & 3*(ss - 2*tt)*tt) + (Mm2 - tt)*(2*Mm2**2 + 3*Mm2*ss - 4*Mm2*tt + 2*tt**2))/ss
  coeff_c6u = (2*me2**3 + 2*Mm2**3 + ss**3 + 3*Mm2**2*(ss - 2*tt) + 3*me2**2*(2*Mm2 + ss - 2*t&
              &t) + 3*ss**2*tt - 2*tt**3 - 3*Mm2*(2*ss**2 + ss*tt - 2*tt**2) + me2*(6*Mm2**2 - &
              &6*ss**2 + 2*Mm2*(5*ss - 6*tt) - 3*ss*tt + 6*tt**2))/ss
  coeff_ce = (-16*me2**3 - 16*me2**2*(Mm2 - ss - tt) - 8*me2*ss*(-2*Mm2 + ss + 2*tt) + ss**2*&
             &(-2*Mm2 + ss + 2*tt))/(8*me2 - 2*ss)
  coeff_cm = (-16*Mm2**3 - 2*me2*(8*Mm2**2 - 8*Mm2*ss + ss**2) + 16*Mm2**2*(ss + tt) - 8*Mm2*&
             &ss*(ss + 2*tt) + ss**2*(ss + 2*tt))/(8*Mm2 - 2*ss)
  coeff_discse = (-8*me2**3 + me2**2*(-16*Mm2 + 7*ss + 16*tt) + ss*(3*Mm2**2 + 2*ss**2 + 6*ss*tt &
                 &+ 3*tt**2 - 3*Mm2*(ss + 2*tt)) - me2*(8*Mm2**2 + 9*ss**2 + 18*ss*tt + 8*tt**2 - &
                 &2*Mm2*(3*ss + 8*tt)))/((4*me2 - ss)*ss)
  coeff_discse_lmm = (2*(2*me2 - ss)*(me2**2 + Mm2**2 + Mm2*(ss - 2*tt) + me2*(2*Mm2 + ss - 2*tt) + t&
                     &t**2))/((4*me2 - ss)*ss)
  coeff_discse_lmu = (-2*(2*me2 - ss)*(me2**2 + Mm2**2 + Mm2*(ss - 2*tt) + me2*(2*Mm2 + ss - 2*tt) + &
                     &tt**2))/((4*me2 - ss)*ss)
  coeff_discsm = (-8*Mm2**3 + me2**2*(-8*Mm2 + 3*ss) + Mm2**2*(7*ss + 16*tt) + ss*(2*ss**2 + 6*ss&
                 &*tt + 3*tt**2) - Mm2*(9*ss**2 + 18*ss*tt + 8*tt**2) + me2*(-16*Mm2**2 - 3*ss*(ss&
                 & + 2*tt) + 2*Mm2*(3*ss + 8*tt)))/((4*Mm2 - ss)*ss)
  coeff_discsm_lmu = (-2*(2*Mm2 - ss)*(me2**2 + Mm2**2 + Mm2*(ss - 2*tt) + me2*(2*Mm2 + ss - 2*tt) + &
                     &tt**2))/((4*Mm2 - ss)*ss)
  coeff_disct = (2*me2**4 + 3*me2**3*(ss - 2*tt) - me2**2*(4*Mm2**2 + 3*Mm2*ss + 10*Mm2*tt + 16*&
                &ss*tt - 6*tt**2) + (Mm2 - tt)*(2*Mm2**3 + Mm2**2*(3*ss - 4*tt) + Mm2*tt*(-13*ss &
                &+ 2*tt) + 4*ss*tt*(ss + 3*tt)) + me2*(4*Mm2*tt*(-10*ss + 3*tt) - Mm2**2*(3*ss + &
                &10*tt) + tt*(4*ss**2 + 25*ss*tt - 2*tt**2)))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)*&
                &*2))
  coeff_disct_lmm = -(1./2.)*(tt*(6*me2**3 + me2**2*(18*Mm2 + 5*ss - 18*tt) + (Mm2 - tt)*(6*Mm2**2 + 5*M&
                    &m2*ss - 12*Mm2*tt + 6*tt**2) + me2*(18*Mm2**2 + 6*Mm2*(ss - 6*tt) + tt*(-5*ss + &
                    &18*tt))))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
  coeff_disct_lmu = (4*(me2 + Mm2 - tt)*tt*(me2**2 + Mm2**2 + Mm2*(ss - 2*tt) + me2*(2*Mm2 + ss - 2*&
                    &tt) + tt**2))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
  coeff_disct_ls = (tt*(2*me2**3 + me2**2*(6*Mm2 + ss - 6*tt) + (Mm2 - tt)*(2*Mm2**2 + Mm2*(ss - 4*&
                   &tt) + 2*tt**2) + me2*(6*Mm2**2 + tt*(-ss + 6*tt) - 2*Mm2*(ss + 6*tt))))/(ss*(-4*&
                   &me2*Mm2 + (me2 + Mm2 - tt)**2))
  coeff_discu = (-11*me2**5 - 11*Mm2**5 + Mm2**4*(58*ss + 45*tt) + me2**4*(-15*Mm2 + 58*ss + 45*&
                &tt) + (ss + tt)**3*(4*ss**2 + 11*ss*tt + 3*tt**2) - Mm2*(ss + tt)**2*(31*ss**2 +&
                & 71*ss*tt + 21*tt**2) - Mm2**3*(105*ss**2 + 191*ss*tt + 72*tt**2) + Mm2**2*(85*s&
                &s**3 + 255*ss**2*tt + 226*ss*tt**2 + 56*tt**3) + me2**3*(26*Mm2**2 - 105*ss**2 -&
                & 191*ss*tt - 72*tt**2 + 4*Mm2*(58*ss + 25*tt)) + me2**2*(26*Mm2**3 + 85*ss**3 + &
                &255*ss**2*tt + 226*ss*tt**2 + 56*tt**3 + Mm2**2*(316*ss + 94*tt) - Mm2*(359*ss**&
                &2 + 561*ss*tt + 168*tt**2)) - me2*(15*Mm2**4 - 4*Mm2**3*(58*ss + 25*tt) + (ss + &
                &tt)**2*(31*ss**2 + 71*ss*tt + 21*tt**2) + Mm2**2*(359*ss**2 + 561*ss*tt + 168*tt&
                &**2) - 2*Mm2*(95*ss**3 + 269*ss**2*tt + 226*ss*tt**2 + 52*tt**3)))/(ss*(-4*me2*M&
                &m2 + (me2 + Mm2 - ss - tt)**2)*(-2*me2 - 2*Mm2 + ss + tt))
  coeff_discu_lmm = -(1./2.)*((-2*me2 - 2*Mm2 + ss + tt)*(-6*me2**3 - 6*Mm2**3 + ss**3 + 3*ss**2*tt + 8*&
                    &ss*tt**2 + 6*tt**3 + 3*Mm2**2*(ss + 6*tt) + 3*me2**2*(-6*Mm2 + ss + 6*tt) + Mm2*&
                    &(2*ss**2 - 11*ss*tt - 18*tt**2) + me2*(-18*Mm2**2 + 10*Mm2*ss + 2*ss**2 + 36*Mm2&
                    &*tt - 11*ss*tt - 18*tt**2)))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2))
  coeff_discu_lmu = (4*(-2*me2 - 2*Mm2 + ss + tt)*(-me2 - Mm2 + ss + tt)*(me2**2 + Mm2**2 + Mm2*(ss &
                    &- 2*tt) + me2*(2*Mm2 + ss - 2*tt) + tt**2))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - &
                    &tt)**2))
  coeff_discu_ls = ((-2*me2 - 2*Mm2 + ss + tt)*(-2*me2**3 - 2*Mm2**3 + ss**3 + 3*ss**2*tt + 4*ss*tt&
                   &**2 + 2*tt**3 + 3*Mm2**2*(ss + 2*tt) + me2**2*(-6*Mm2 + 3*ss + 6*tt) - Mm2*(2*ss&
                   &**2 + 7*ss*tt + 6*tt**2) - me2*(6*Mm2**2 + 2*ss**2 + 7*ss*tt + 6*tt**2 - 2*Mm2*(&
                   &5*ss + 6*tt))))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2))
  coeff_lmm = (32*Mm2**6 + 8*me2**5*(-4*Mm2 + ss) + 4*Mm2**5*(2*ss - 43*tt) - me2**4*(4*Mm2 - &
              &ss)*(24*Mm2 + 4*ss - 3*tt) + ss*tt**2*(ss + tt)**2*(ss + 5*tt) + Mm2**4*(-44*ss*&
              &*2 + 55*ss*tt + 336*tt**2) + Mm2**3*(22*ss**3 + 77*ss**2*tt - 224*ss*tt**2 - 304&
              &*tt**3) + Mm2*tt*(6*ss**4 + ss**3*tt - 53*ss**2*tt**2 - 68*ss*tt**3 - 20*tt**4) &
              &+ Mm2**2*(-3*ss**4 - 42*ss**3*tt + 31*ss**2*tt**2 + 216*ss*tt**3 + 128*tt**4) - &
              &me2**3*(64*Mm2**3 + 8*Mm2**2*(2*ss + 19*tt) - 6*Mm2*(8*ss**2 - ss*tt + 24*tt**2)&
              & + ss*(10*ss**2 - 3*ss*tt + 36*tt**2)) + me2*(96*Mm2**5 + 8*Mm2**4*(ss - 61*tt) &
              &+ Mm2**3*(-48*ss**2 + 70*ss*tt + 800*tt**2) + Mm2**2*(10*ss**3 + 173*ss**2*tt - &
              &376*ss*tt**2 - 520*tt**3) + ss*tt*(6*ss**3 - ss**2*tt - 35*ss*tt**2 - 28*tt**3) &
              &+ 2*Mm2*tt*(-35*ss**3 + 8*ss**2*tt + 115*ss*tt**2 + 56*tt**3)) + me2**2*(64*Mm2*&
              &*4 - 16*Mm2**3*(ss + 30*tt) + Mm2**2*(40*ss**2 + 76*ss*tt + 640*tt**2) + ss*(3*s&
              &s**3 - 12*ss**2*tt + 25*ss*tt**2 + 54*tt**3) - Mm2*(22*ss**3 - 83*ss**2*tt + 196&
              &*ss*tt**2 + 216*tt**3)))/(2.*(4*Mm2 - ss)*ss*tt*(-2*me2 - 2*Mm2 + ss + tt)**2)
  coeff_lmu = (5*(me2**2 + Mm2**2 + Mm2*(ss - 2*tt) + me2*(2*Mm2 + ss - 2*tt) + tt**2))/ss
  coeff_ls = -((16*me2**2*Mm2 + ss**2*(Mm2 + tt) + me2*(16*Mm2**2 + ss**2 - 16*Mm2*(ss + tt))&
             &)/((4*me2 - ss)*(-4*Mm2 + ss)))
  coeff_unity = (13*me2**3 + 13*Mm2**3 + me2**2*(67*Mm2 - 14*ss - 33*tt) - (ss + tt)**2*(4*ss + &
                &7*tt) - Mm2**2*(14*ss + 33*tt) + Mm2*(11*ss**2 + 38*ss*tt + 27*tt**2) + me2*(67*&
                &Mm2**2 + 11*ss**2 + 38*ss*tt + 27*tt**2 - 6*Mm2*(6*ss + 13*tt)))/(ss*(-2*me2 - 2&
                &*Mm2 + ss + tt))
  coeffLR = 2*(c6se*coeff_c6se + c6sm*coeff_c6sm + c6t*coeff_c6t  &
          + c6u*coeff_c6u + ce*coeff_ce + cm*coeff_cm  &
          + coeff_discse*discse + coeff_discse_lmm*discse*lmm + coeff_discse_lmu*discse*lmu  &
          + coeff_discsm*discsm + coeff_discsm_lmu*discsm*lmu + coeff_disct*disct  &
          + coeff_disct_lmm*disct*lmm + coeff_disct_lmu*disct*lmu + coeff_disct_ls*disctLs  &
          + coeff_discu*discu + coeff_discu_lmm*discu*lmm + coeff_discu_lmu*discu*lmu  &
          + coeff_discu_ls*discuLs + coeff_lmm*lmm + coeff_lmu*lmu  &
          + coeff_ls*ls + coeff_unity)

  if(pol) then
    coeff_c6se = (-2*snm*(2*me2 - ss)*(me2**2 + Mm2**2 + 2*me2*(Mm2 - tt) - 2*Mm2*tt + tt*(ss + t&
                 &t)))/ss
    coeff_c6sm = (-2*snm*(2*Mm2 - ss)*(me2**2 + Mm2**2 + 2*me2*(Mm2 - tt) - 2*Mm2*tt + tt*(ss + t&
                 &t)))/ss
    coeff_c6t = -((snm*(me2 + Mm2 - tt)*(2*me2**4 + 2*Mm2**4 + 4*Mm2**2*tt*(2*ss + 3*tt) - me2**&
                &3*(ss + 8*tt) - Mm2**3*(ss + 8*tt) - Mm2*tt**2*(13*ss + 8*tt) + tt**2*(3*ss**2 +&
                & 6*ss*tt + 2*tt**2) + me2**2*(-4*Mm2**2 + Mm2*(ss - 8*tt) + 4*tt*(2*ss + 3*tt)) &
                &+ me2*(Mm2**2*(ss - 8*tt) + 16*Mm2*tt**2 - tt**2*(13*ss + 8*tt))))/(ss*(me2**2 +&
                & Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))))
    coeff_c6u = -((snm*(2*me2**5 + 2*Mm2**5 + me2**4*(2*Mm2 - ss - 10*tt) - 2*tt**2*(ss + tt)**3&
                & - Mm2**4*(ss + 10*tt) + Mm2**3*tt*(9*ss + 20*tt) + Mm2*(ss + tt)**2*(ss**2 - ss&
                &*tt + 10*tt**2) - Mm2**2*(2*ss**3 + 3*ss**2*tt + 21*ss*tt**2 + 20*tt**3) + me2**&
                &3*(-4*Mm2**2 - 16*Mm2*tt + tt*(9*ss + 20*tt)) - me2**2*(4*Mm2**3 + 2*ss**3 - 2*M&
                &m2**2*(ss - 6*tt) + 3*ss**2*tt + 21*ss*tt**2 + 20*tt**3 - Mm2*(8*ss**2 + 7*ss*tt&
                & + 36*tt**2)) + me2*(2*Mm2**4 - 16*Mm2**3*tt + (ss + tt)**2*(ss**2 - ss*tt + 10*&
                &tt**2) + Mm2**2*(8*ss**2 + 7*ss*tt + 36*tt**2) - 2*Mm2*(4*ss**3 + ss**2*tt + 13*&
                &ss*tt**2 + 16*tt**3))))/(ss*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*&
                &(ss + tt))))
    coeff_ce = (snm*(16*me2**5 - 16*me2**4*(Mm2 + 3*tt) - 4*me2**3*(4*Mm2**2 - 12*Mm2*ss + 3*ss&
               &**2 + 8*Mm2*tt - 8*ss*tt - 12*tt**2) - ss**2*(-2*Mm2**3 - Mm2**2*(ss - 6*tt) + t&
               &t**2*(ss + 2*tt) + Mm2*(ss**2 - 6*tt**2)) - me2*ss*(16*Mm2**3 + ss**3 + 8*Mm2**2&
               &*(ss - 6*tt) - 16*ss*tt**2 - 16*tt**3 + Mm2*(-12*ss**2 + 8*ss*tt + 48*tt**2)) + &
               &me2**2*(16*Mm2**3 + 7*ss**3 + 16*Mm2**2*(2*ss - 3*tt) - 2*ss**2*tt - 48*ss*tt**2&
               & - 16*tt**3 + Mm2*(-46*ss**2 + 16*ss*tt + 48*tt**2))))/(2.*(4*me2 - ss)*(me2**2 &
               &+ Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_cm = (snm*(16*Mm2**5 + 2*me2**3*(8*Mm2**2 - 8*Mm2*ss + ss**2) + me2**2*(-16*Mm2**3 - &
               &8*Mm2*ss*(ss - 6*tt) + ss**2*(ss - 6*tt) + 16*Mm2**2*(2*ss - 3*tt)) - 48*Mm2**4*&
               &tt - ss**2*tt**2*(ss + 2*tt) + Mm2**3*(-12*ss**2 + 32*ss*tt + 48*tt**2) + Mm2**2&
               &*(7*ss**3 - 2*ss**2*tt - 48*ss*tt**2 - 16*tt**3) + Mm2*ss*(-ss**3 + 16*ss*tt**2 &
               &+ 16*tt**3) - me2*(16*Mm2**4 + ss**4 - 6*ss**2*tt**2 + Mm2**3*(-48*ss + 32*tt) +&
               & 2*Mm2**2*(23*ss**2 - 8*ss*tt - 24*tt**2) + 4*Mm2*ss*(-3*ss**2 + 2*ss*tt + 12*tt&
               &**2))))/(2.*(4*Mm2 - ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss&
               & + tt)))
    coeff_discse = (snm*(8*me2**3 + me2**2*(16*Mm2 - 3*ss - 16*tt) + ss*(-3*Mm2**2 + ss**2 + 6*Mm2*&
                   &tt - 3*ss*tt - 3*tt**2) + 2*me2*(4*Mm2**2 - ss**2 + 7*ss*tt + 4*tt**2 - Mm2*(ss &
                   &+ 8*tt))))/((4*me2 - ss)*ss)
    coeff_discse_lmm = (-2*snm*(2*me2 - ss)*(me2**2 + Mm2**2 + 2*me2*(Mm2 - tt) - 2*Mm2*tt + tt*(ss + t&
                       &t)))/((4*me2 - ss)*ss)
    coeff_discse_lmu = (2*snm*(2*me2 - ss)*(me2**2 + Mm2**2 + 2*me2*(Mm2 - tt) - 2*Mm2*tt + tt*(ss + tt&
                       &)))/((4*me2 - ss)*ss)
    coeff_discsm = (snm*(8*Mm2**3 + me2**2*(8*Mm2 - 3*ss) - Mm2**2*(3*ss + 16*tt) - 2*Mm2*(ss**2 - &
                   &7*ss*tt - 4*tt**2) + ss*(ss**2 - 3*ss*tt - 3*tt**2) + 2*me2*(8*Mm2**2 + 3*ss*tt &
                   &- Mm2*(ss + 8*tt))))/((4*Mm2 - ss)*ss)
    coeff_discsm_lmu = (2*snm*(2*Mm2 - ss)*(me2**2 + Mm2**2 + 2*me2*(Mm2 - tt) - 2*Mm2*tt + tt*(ss + tt&
                       &)))/((4*Mm2 - ss)*ss)
    coeff_disct = (snm*(-2*me2**4 + 6*me2**3*tt + 2*me2*tt*(5*Mm2**2 + ss**2 + Mm2*(ss - 6*tt) + t&
                  &t**2) - (Mm2 - tt)*(2*Mm2**3 - 4*Mm2**2*tt + ss*tt*(-2*ss + tt) + Mm2*tt*(ss + 2&
                  &*tt)) + me2**2*(4*Mm2**2 + 10*Mm2*tt - tt*(ss + 6*tt))))/(ss*(-4*me2*Mm2 + (me2 &
                  &+ Mm2 - tt)**2))
    coeff_disct_lmm = (snm*(me2 + Mm2 - tt)*tt*(6*me2**4 + 6*Mm2**4 + me2**3*(ss - 24*tt) + Mm2**3*(ss&
                      & - 24*tt) + 4*Mm2**2*tt*(2*ss + 9*tt) - Mm2*tt**2*(19*ss + 24*tt) + tt**2*(5*ss*&
                      &*2 + 10*ss*tt + 6*tt**2) - me2**2*(12*Mm2**2 - 4*tt*(2*ss + 9*tt) + Mm2*(ss + 24&
                      &*tt)) - me2*(-48*Mm2*tt**2 + Mm2**2*(ss + 24*tt) + tt**2*(19*ss + 24*tt))))/(2.*&
                      &ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 +&
                      & tt) + tt*(ss + tt)))
    coeff_disct_lmu = (-4*snm*(me2 + Mm2 - tt)*tt*(me2**2 + Mm2**2 + 2*me2*(Mm2 - tt) - 2*Mm2*tt + tt*&
                      &(ss + tt)))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
    coeff_disct_ls = -((snm*(me2 + Mm2 - tt)*tt*(2*me2**4 + 2*Mm2**4 + me2**3*(ss - 8*tt) + Mm2**3*(s&
                     &s - 8*tt) + 12*Mm2**2*tt**2 - Mm2*tt**2*(3*ss + 8*tt) + tt**2*(ss**2 + 2*ss*tt +&
                     & 2*tt**2) - me2**2*(4*Mm2**2 - 12*tt**2 + Mm2*(ss + 8*tt)) - me2*(-16*Mm2*tt**2 &
                     &+ Mm2**2*(ss + 8*tt) + tt**2*(3*ss + 8*tt))))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)&
                     &**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))))
    coeff_discu = (snm*(11*me2**5 + 11*Mm2**5 + 5*me2**4*(3*Mm2 - 5*ss - 9*tt) - 5*Mm2**4*(5*ss + &
                  &9*tt) - 2*me2**3*(13*Mm2**2 + 18*Mm2*ss - 6*ss**2 + 50*Mm2*tt - 46*ss*tt - 36*tt&
                  &**2) - Mm2*(ss + tt)**2*(9*ss**2 - 22*ss*tt - 21*tt**2) + (ss + tt)**3*(2*ss**2 &
                  &- 3*ss*tt - 3*tt**2) + 4*Mm2**3*(3*ss**2 + 23*ss*tt + 18*tt**2) + Mm2**2*(9*ss**&
                  &3 - 54*ss**2*tt - 119*ss*tt**2 - 56*tt**3) - me2**2*(26*Mm2**3 - 9*ss**3 + 54*ss&
                  &**2*tt + 119*ss*tt**2 + 56*tt**3 + Mm2**2*(6*ss + 94*tt) - 4*Mm2*(ss**2 + 49*ss*&
                  &tt + 42*tt**2)) + me2*(15*Mm2**4 - 4*Mm2**3*(9*ss + 25*tt) - (ss + tt)**2*(9*ss*&
                  &*2 - 22*ss*tt - 21*tt**2) + 4*Mm2**2*(ss**2 + 49*ss*tt + 42*tt**2) + 2*Mm2*(11*s&
                  &s**3 - 46*ss**2*tt - 109*ss*tt**2 - 52*tt**3))))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - &
                  &ss - tt)**2)*(-2*me2 - 2*Mm2 + ss + tt))
    coeff_discu_lmm = (snm*(-2*me2 - 2*Mm2 + ss + tt)*(-6*me2**5 - 6*Mm2**5 + 6*tt**2*(ss + tt)**3 - 3&
                      &*Mm2**3*tt*(13*ss + 20*tt) + Mm2**4*(7*ss + 30*tt) + me2**4*(-6*Mm2 + 7*ss + 30*&
                      &tt) + Mm2*(ss + tt)**2*(ss**2 - ss*tt - 30*tt**2) + Mm2**2*(-2*ss**3 + 13*ss**2*&
                      &tt + 75*ss*tt**2 + 60*tt**3) + 3*me2**3*(4*Mm2**2 + 16*Mm2*tt - tt*(13*ss + 20*t&
                      &t)) + me2**2*(12*Mm2**3 - 2*ss**3 + 13*ss**2*tt + 75*ss*tt**2 + 60*tt**3 + Mm2**&
                      &2*(-14*ss + 36*tt) + Mm2*(8*ss**2 - 41*ss*tt - 108*tt**2)) + me2*(-6*Mm2**4 + 48&
                      &*Mm2**3*tt + Mm2**2*(8*ss**2 - 41*ss*tt - 108*tt**2) + (ss + tt)**2*(ss**2 - ss*&
                      &tt - 30*tt**2) - 2*Mm2*(4*ss**3 + ss**2*tt - 51*ss*tt**2 - 48*tt**3))))/(2.*ss*(&
                      &-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 &
                      &+ tt) + tt*(ss + tt)))
    coeff_discu_lmu = (-4*snm*(-2*me2 - 2*Mm2 + ss + tt)*(-me2 - Mm2 + ss + tt)*(me2**2 + Mm2**2 + 2*m&
                      &e2*(Mm2 - tt) - 2*Mm2*tt + tt*(ss + tt)))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt&
                      &)**2))
    coeff_discu_ls = -((snm*(-2*me2 - 2*Mm2 + ss + tt)*(-2*me2**5 - 2*Mm2**5 + 2*tt**2*(ss + tt)**3 -&
                     & 5*Mm2**3*tt*(3*ss + 4*tt) + Mm2**4*(3*ss + 10*tt) + me2**4*(-2*Mm2 + 3*ss + 10*&
                     &tt) + Mm2*(ss + tt)**2*(ss**2 - ss*tt - 10*tt**2) + Mm2**2*(-2*ss**3 + 5*ss**2*t&
                     &t + 27*ss*tt**2 + 20*tt**3) + me2**3*(4*Mm2**2 + 16*Mm2*tt - 5*tt*(3*ss + 4*tt))&
                     & + me2**2*(4*Mm2**3 - 2*ss**3 - 6*Mm2**2*(ss - 2*tt) + 5*ss**2*tt + 27*ss*tt**2 &
                     &+ 20*tt**3 + Mm2*(8*ss**2 - 17*ss*tt - 36*tt**2)) + me2*(-2*Mm2**4 + 16*Mm2**3*t&
                     &t + Mm2**2*(8*ss**2 - 17*ss*tt - 36*tt**2) + (ss + tt)**2*(ss**2 - ss*tt - 10*tt&
                     &**2) + Mm2*(-8*ss**3 - 2*ss**2*tt + 38*ss*tt**2 + 32*tt**3))))/(ss*(-4*me2*Mm2 +&
                     & (me2 + Mm2 - ss - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(&
                     &ss + tt))))
    coeff_lmm = (snm*(-32*Mm2**6 + 8*me2**5*(4*Mm2 - ss) + me2**4*(4*Mm2 - ss)*(24*Mm2 - 8*ss - &
                &3*tt) - ss*tt**2*(ss + tt)**2*(6*ss + 5*tt) + 4*Mm2**5*(10*ss + 43*tt) - Mm2**4*&
                &(16*ss**2 + 227*ss*tt + 336*tt**2) + 4*Mm2*tt**2*(13*ss**3 + 30*ss**2*tt + 22*ss&
                &*tt**2 + 5*tt**3) - 4*Mm2**2*tt*(4*ss**3 + 54*ss**2*tt + 81*ss*tt**2 + 32*tt**3)&
                & + 2*Mm2**3*(ss**3 + 51*ss**2*tt + 218*ss*tt**2 + 152*tt**3) + 2*me2**3*(32*Mm2*&
                &*3 + Mm2**2*(-40*ss + 76*tt) + 3*Mm2*(4*ss**2 + 3*ss*tt - 24*tt**2) - ss*(ss**2 &
                &+ 3*ss*tt - 18*tt**2)) - 2*me2**2*(32*Mm2**4 - 8*Mm2**3*(ss + 30*tt) - 4*Mm2**2*&
                &(ss**2 - 26*ss*tt - 80*tt**2) + ss*tt*(-3*ss**2 + 26*ss*tt + 27*tt**2) + Mm2*(ss&
                &**3 + 13*ss**2*tt - 152*ss*tt**2 - 108*tt**3)) + 2*me2*(-48*Mm2**5 + 4*Mm2**4*(1&
                &1*ss + 61*tt) - Mm2**3*(12*ss**2 + 209*ss*tt + 400*tt**2) + ss*tt*(-ss**3 + 14*s&
                &s**2*tt + 29*ss*tt**2 + 14*tt**3) - Mm2*tt*(-7*ss**3 + 102*ss**2*tt + 161*ss*tt*&
                &*2 + 56*tt**3) + Mm2**2*(ss**3 + 37*ss**2*tt + 348*ss*tt**2 + 260*tt**3))))/(2.*&
                &(4*Mm2 - ss)*ss*tt*(-2*me2 - 2*Mm2 + ss + tt)**2)
    coeff_lmu = (-5*snm*(me2**2 + Mm2**2 + 2*me2*(Mm2 - tt) - 2*Mm2*tt + tt*(ss + tt)))/ss
    coeff_ls = (snm*(16*me2**2*Mm2 + ss**2*(Mm2 + tt) + me2*(16*Mm2**2 + ss**2 - 16*Mm2*(ss + t&
               &t))))/((4*me2 - ss)*(-4*Mm2 + ss))
    coeff_unity = -((snm*(me2**3 + Mm2**3 - 4*ss**3 + me2**2*(47*Mm2 - 23*ss - 3*tt) - 12*ss**2*tt&
                  & - 9*ss*tt**2 - tt**3 - Mm2**2*(23*ss + 3*tt) + Mm2*(19*ss**2 + 28*ss*tt + 3*tt*&
                  &*2) + me2*(47*Mm2**2 - 66*Mm2*ss + 19*ss**2 - 26*Mm2*tt + 28*ss*tt + 3*tt**2)))/&
                  &(ss*(-2*me2 - 2*Mm2 + ss + tt)))
    coeffLR = coeffLR + c6se*coeff_c6se + c6sm*coeff_c6sm + c6t*coeff_c6t  &
            + c6u*coeff_c6u + ce*coeff_ce + cm*coeff_cm  &
            + coeff_discse*discse + coeff_discse_lmm*discse*lmm + coeff_discse_lmu*discse*lmu  &
            + coeff_discsm*discsm + coeff_discsm_lmu*discsm*lmu + coeff_disct*disct  &
            + coeff_disct_lmm*disct*lmm + coeff_disct_lmu*disct*lmu + coeff_disct_ls*disctLs  &
            + coeff_discu*discu + coeff_discu_lmm*discu*lmm + coeff_discu_lmu*discu*lmu  &
            + coeff_discu_ls*discuLs + coeff_lmm*lmm + coeff_lmu*lmu  &
            + coeff_ls*ls + coeff_unity
    coeff_c6se = (2*(2*me2 - ss)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt)&
                 & + s3mm*(s3nm*ss + s2nm*tt)))/(me2*ss)
    coeff_c6sm = (2*(2*Mm2 - ss)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt)&
                 & + s3mm*(s3nm*ss + s2nm*tt)))/(me2*ss)
    coeff_c6t = (me2**5*(-2*Mm2*s2nm*s3mm + 2*Mm2*s1mm*(s2nm + s3nm) + (s1mm*s2nm - s2nm*s3mm - &
                &5*s1mm*s3nm + 6*s3mm*s3nm)*ss + 2*(s2nm*s3mm - s1mm*s3nm)*tt) + (Mm2 - tt)*(-(Mm&
                &2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2&
                &nm*tt))*(2*Mm2**4 + 4*Mm2**2*tt*(2*ss + 3*tt) - Mm2**3*(ss + 8*tt) - Mm2*tt**2*(&
                &13*ss + 8*tt) + tt**2*(3*ss**2 + 6*ss*tt + 2*tt**2)) + me2**2*(4*Mm2**4*(-(s2nm*&
                &s3mm) + s1mm*(s2nm + s3nm)) + 2*Mm2**3*(-(s3mm*(s2nm + 2*s3nm)*ss) + s1mm*(s2nm &
                &+ 3*s3nm)*ss + 2*s1mm*s2nm*tt) + tt**2*(21*(s1mm - s3mm)*s3nm*ss**2 - (4*s1mm*s2&
                &nm + 17*s2nm*s3mm - 53*s1mm*s3nm + 36*s3mm*s3nm)*ss*tt + 20*(-(s2nm*s3mm) + s1mm&
                &*s3nm)*tt**2) + Mm2**2*(2*(-s1mm + s3mm)*s3nm*ss**2 + (s3mm*(3*s2nm + 4*s3nm) - &
                &s1mm*(s2nm + 7*s3nm))*ss*tt + 4*(3*s1mm*s2nm - 2*s2nm*s3mm + 2*s1mm*s3nm)*tt**2)&
                & + Mm2*tt*(9*(s1mm - s3mm)*s3nm*ss**2 + (-9*s1mm*s2nm + 8*s2nm*s3mm - 44*s1mm*s3&
                &nm + 36*s3mm*s3nm)*ss*tt - 4*(5*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**2)) +&
                & me2*(-6*Mm2**5*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + Mm2**3*tt*((-7*s1mm*s2nm +&
                & 7*s2nm*s3mm + s1mm*s3nm - 8*s3mm*s3nm)*ss - 4*s2nm*s3mm*tt + 4*s1mm*(3*s2nm + s&
                &3nm)*tt) + Mm2**4*(-(s3mm*(s2nm + 2*s3nm)*ss) + s1mm*(s2nm + 3*s3nm)*ss + 2*(4*s&
                &1mm*s2nm - 7*s2nm*s3mm + 7*s1mm*s3nm)*tt) + Mm2**2*tt*((s1mm - s3mm)*s3nm*ss**2 &
                &+ (-8*s1mm*s2nm + 7*s2nm*s3mm - 43*s1mm*s3nm + 36*s3mm*s3nm)*ss*tt - 12*(2*s1mm*&
                &s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt**2) + Mm2*tt**2*((-(s3mm*(s2nm + 18*s3nm)) &
                &+ s1mm*(s2nm + 19*s3nm))*ss**2 + (13*s1mm*s2nm - 31*s2nm*s3mm + 71*s1mm*s3nm - 4&
                &0*s3mm*s3nm)*ss*tt + 2*(5*s1mm*s2nm - 17*s2nm*s3mm + 17*s1mm*s3nm)*tt**2) + tt**&
                &2*(3*(-s1mm + s3mm)*s3nm*ss**3 + (3*s2nm*s3mm - 22*s1mm*s3nm + 19*s3mm*s3nm)*ss*&
                &*2*tt + (s1mm*(s2nm - 32*s3nm) + 2*s3mm*(9*s2nm + 7*s3nm))*ss*tt**2 + 10*(s2nm*s&
                &3mm - s1mm*s3nm)*tt**3)) + me2**3*(4*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) &
                &- 2*Mm2**2*(-(s3mm*(s2nm + 6*s3nm)*ss) + s1mm*(s2nm + 7*s3nm)*ss + 2*s2nm*s3mm*t&
                &t - 2*s1mm*(2*s2nm + s3nm)*tt) + tt*(9*(-s1mm + s3mm)*s3nm*ss**2 + (6*s1mm*s2nm &
                &+ 3*s2nm*s3mm - 47*s1mm*s3nm + 44*s3mm*s3nm)*ss*tt + 20*(s2nm*s3mm - s1mm*s3nm)*&
                &tt**2) + Mm2*tt*(8*s3mm*s3nm*ss - 3*s2nm*s3mm*(ss + 4*tt) + s1mm*(3*s2nm*ss - 5*&
                &s3nm*ss + 20*s2nm*tt + 12*s3nm*tt))) - me2**4*(6*Mm2**2*(-(s2nm*s3mm) + s1mm*(s2&
                &nm + s3nm)) + 4*s1mm*s2nm*ss*tt + s2nm*s3mm*tt*(-3*ss + 10*tt) + s3mm*s3nm*ss*(s&
                &s + 26*tt) - s1mm*s3nm*(ss**2 + 23*ss*tt + 10*tt**2) + Mm2*(14*s3mm*s3nm*ss - s2&
                &nm*s3mm*(ss + 4*tt) + s1mm*(-13*s3nm*ss + 4*s3nm*tt + s2nm*(ss + 10*tt)))))/(me2&
                &*ss*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_c6u = (me2**5*(-2*Mm2*s2nm*s3mm + 2*Mm2*s1mm*(s2nm + s3nm) + (s1mm*s2nm - s2nm*s3mm - &
                &5*s1mm*s3nm + 6*s3mm*s3nm)*ss + 2*(s2nm*s3mm - s1mm*s3nm)*tt) + (-(Mm2*s2nm*s3mm&
                &) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(2*&
                &Mm2**5 - 2*tt**2*(ss + tt)**3 - Mm2**4*(ss + 10*tt) + Mm2**3*tt*(9*ss + 20*tt) +&
                & Mm2*(ss + tt)**2*(ss**2 - ss*tt + 10*tt**2) - Mm2**2*(2*ss**3 + 3*ss**2*tt + 21&
                &*ss*tt**2 + 20*tt**3)) + me2**3*(4*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + &
                &3*(s1mm*s2nm - s2nm*s3mm - 3*s1mm*s3nm + 4*s3mm*s3nm)*ss**3 + 9*(s1mm*s2nm - s2n&
                &m*s3mm - 4*s1mm*s3nm + 5*s3mm*s3nm)*ss**2*tt + (6*s1mm*s2nm + 3*s2nm*s3mm - 47*s&
                &1mm*s3nm + 44*s3mm*s3nm)*ss*tt**2 + 20*(s2nm*s3mm - s1mm*s3nm)*tt**3 + Mm2*((-5*&
                &s1mm*s2nm + s2nm*s3mm + 7*s1mm*s3nm - 8*s3mm*s3nm)*ss**2 + (-9*s2nm*s3mm + 8*s3m&
                &m*s3nm + s1mm*(9*s2nm + s3nm))*ss*tt + 4*(5*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3n&
                &m)*tt**2) + 4*Mm2**2*(3*s3mm*s3nm*ss + s1mm*s3nm*(ss + tt) - s2nm*s3mm*(4*ss + t&
                &t) + s1mm*s2nm*(3*ss + 2*tt))) - me2**4*(6*Mm2**2*(-(s2nm*s3mm) + s1mm*(s2nm + s&
                &3nm)) + (3*s1mm*s2nm - 3*s2nm*s3mm - 10*s1mm*s3nm + 13*s3mm*s3nm)*ss**2 + (4*s1m&
                &m*s2nm - 3*s2nm*s3mm - 23*s1mm*s3nm + 26*s3mm*s3nm)*ss*tt + 10*(s2nm*s3mm - s1mm&
                &*s3nm)*tt**2 + Mm2*(s3mm*(-7*s2nm*ss + 14*s3nm*ss - 4*s2nm*tt) + s1mm*(3*s2nm*ss&
                & - 7*s3nm*ss + 10*s2nm*tt + 4*s3nm*tt))) + me2*(-6*Mm2**5*(-(s2nm*s3mm) + s1mm*(&
                &s2nm + s3nm)) + Mm2**3*((5*s1mm*s2nm - 9*s2nm*s3mm + 9*s1mm*s3nm)*ss**2 - (25*s1&
                &mm*s2nm - 25*s2nm*s3mm + 17*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt + 4*(-(s2nm*s3mm) + s&
                &1mm*(3*s2nm + s3nm))*tt**2) + Mm2**2*(5*(s3mm*(s2nm + 4*s3nm) - s1mm*(s2nm + 5*s&
                &3nm))*ss**3 + (17*s1mm*s2nm + 3*s2nm*s3mm - 14*s1mm*s3nm + 11*s3mm*s3nm)*ss**2*t&
                &t - (2*s1mm*s2nm + 11*s2nm*s3mm + 25*s1mm*s3nm - 36*s3mm*s3nm)*ss*tt**2 - 12*(2*&
                &s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt**3) + Mm2**4*(-2*s3mm*s3nm*ss - 7*s2nm&
                &*s3mm*(ss + 2*tt) + s1mm*(11*s2nm*ss + 9*s3nm*ss + 8*s2nm*tt + 14*s3nm*tt)) + (s&
                &s + tt)**2*(-(s1mm*(ss + tt)*(-(s2nm*ss*tt) + s3nm*(ss**2 + 2*ss*tt + 10*tt**2))&
                &) + s3mm*(2*s2nm*tt**2*(-ss + 5*tt) + s3nm*ss*(ss**2 + 3*ss*tt + 14*tt**2))) + M&
                &m2*(ss + tt)*(-(s3mm*(2*s3nm*ss*(6*ss**2 + ss*tt + 20*tt**2) + s2nm*tt*(7*ss**2 &
                &- 9*ss*tt + 34*tt**2))) + s1mm*(ss + tt)*(5*s2nm*tt*(-ss + 2*tt) + s3nm*(12*ss**&
                &2 - 3*ss*tt + 34*tt**2)))) + me2**2*(4*Mm2**4*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)&
                &) + Mm2**2*((3*s1mm*s2nm + 5*s2nm*s3mm + 37*s1mm*s3nm - 42*s3mm*s3nm)*ss**2 + (-&
                &21*s1mm*s2nm - 9*s2nm*s3mm + 5*s1mm*s3nm + 4*s3mm*s3nm)*ss*tt + 4*(3*s1mm*s2nm -&
                & 2*s2nm*s3mm + 2*s1mm*s3nm)*tt**2) + Mm2*(4*(s1mm*s2nm - s2nm*s3mm - 7*s1mm*s3nm&
                & + 8*s3mm*s3nm)*ss**3 + (9*s1mm*s2nm + 11*s2nm*s3mm - 34*s1mm*s3nm + 23*s3mm*s3n&
                &m)*ss**2*tt + (-15*s1mm*s2nm + 2*s2nm*s3mm - 38*s1mm*s3nm + 36*s3mm*s3nm)*ss*tt*&
                &*2 - 4*(5*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**3) - 4*Mm2**3*(s3mm*(-4*s2n&
                &m + s3nm)*ss + s1mm*(5*s2nm*ss + 3*s3nm*ss - s2nm*tt)) - (ss + tt)*(s1mm*(ss + t&
                &t)*(s2nm*ss*(ss + 4*tt) - s3nm*(5*ss**2 + 13*ss*tt + 20*tt**2)) + s3mm*(3*s3nm*s&
                &s*(2*ss**2 + 7*ss*tt + 12*tt**2) - s2nm*(ss**3 + 3*ss**2*tt + 3*ss*tt**2 - 20*tt&
                &**3)))))/(me2*ss*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))*&
                &*2)
    coeff_ce = -(1./2.)*(8*me2**6*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) - 4*me2**5*(4*Mm2*(-(s2nm*s3m&
               &m) + s1mm*s3nm) + (5*s1mm*s2nm - 5*s2nm*s3mm - 11*s1mm*s3nm + 16*s3mm*s3nm)*ss +&
               & 4*(2*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt) + ss**2*(-(Mm2*s2nm*s3mm) + Mm2&
               &*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(2*Mm2**3 &
               &+ Mm2**2*(ss - 6*tt) - tt**2*(ss + 2*tt) - Mm2*(ss**2 - 6*tt**2)) + me2**2*(8*Mm&
               &2**4*(5*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm) + (s1mm*s2nm - s2nm*s3mm - 10*s1m&
               &m*s3nm + 11*s3mm*s3nm)*ss**4 + (-3*s2nm*s3mm + 5*s1mm*(2*s2nm - 7*s3nm) + 38*s3m&
               &m*s3nm)*ss**3*tt + 2*(8*s1mm*s2nm - 9*s2nm*s3mm + 17*s1mm*s3nm - 8*s3mm*s3nm)*ss&
               &**2*tt**2 + 4*(3*s1mm*s2nm - 15*s2nm*s3mm + 23*s1mm*s3nm - 8*s3mm*s3nm)*ss*tt**3&
               & + 8*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm))*tt**4 + 4*Mm2**3*((7*s1mm*s2nm - 13*s&
               &2nm*s3mm + 5*s1mm*s3nm + 8*s3mm*s3nm)*ss - 8*(4*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm&
               &*s3nm)*tt) + 2*Mm2**2*((-17*s1mm*s2nm + 13*s2nm*s3mm - 53*s1mm*s3nm + 40*s3mm*s3&
               &nm)*ss**2 + 2*(s3mm*(11*s2nm - 24*s3nm) + 13*s1mm*(s2nm + s3nm))*ss*tt + 72*(-(s&
               &2nm*s3mm) + s1mm*(s2nm + s3nm))*tt**2) - Mm2*((3*s1mm*s2nm - 3*s2nm*s3mm - 83*s1&
               &mm*s3nm + 86*s3mm*s3nm)*ss**3 + 2*(15*s1mm*s2nm + 24*s2nm*s3mm - 56*s1mm*s3nm + &
               &32*s3mm*s3nm)*ss**2*tt + 4*(23*s1mm*s2nm - 17*s2nm*s3mm + 41*s1mm*s3nm - 24*s3mm&
               &*s3nm)*ss*tt**2 + 32*(2*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt**3)) + me2*ss*&
               &(-16*Mm2**4*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + (s1mm - s3mm)*s3nm*ss**4 - (s1&
               &mm*(s2nm - 4*s3nm) + 4*s3mm*s3nm)*ss**3*tt + 2*(s3mm*(s2nm + 4*s3nm) - s1mm*(s2n&
               &m + 5*s3nm))*ss**2*tt**2 + 16*(s2nm*s3mm + (-2*s1mm + s3mm)*s3nm)*ss*tt**3 + 16*&
               &(s2nm*s3mm - s1mm*s3nm)*tt**4 + 4*Mm2**3*((-2*s1mm*s2nm + 3*s2nm*s3mm + s1mm*s3n&
               &m - 4*s3mm*s3nm)*ss + 4*(3*s1mm*s2nm - 4*s2nm*s3mm + 4*s1mm*s3nm)*tt) + 2*Mm2**2&
               &*((5*s1mm*s2nm - 5*s2nm*s3mm + 13*s1mm*s3nm - 8*s3mm*s3nm)*ss**2 - 4*(s3mm*(s2nm&
               & - 6*s3nm) + s1mm*(s2nm + 5*s3nm))*ss*tt - 24*(-2*s2nm*s3mm + s1mm*(s2nm + 2*s3n&
               &m))*tt**2) - 4*Mm2*(4*(s1mm - s3mm)*s3nm*ss**3 - (s1mm*s2nm + 3*s2nm*s3mm - 5*s1&
               &mm*s3nm + 2*s3mm*s3nm)*ss**2*tt + (-4*s1mm*s2nm + 5*s2nm*s3mm - 17*s1mm*s3nm + 1&
               &2*s3mm*s3nm)*ss*tt**2 - 4*(-4*s2nm*s3mm + s1mm*(s2nm + 4*s3nm))*tt**3)) + 4*me2*&
               &*4*(4*Mm2**2*(-2*s2nm*s3mm + s1mm*(s2nm + 2*s3nm)) + 5*(s1mm*s2nm - s2nm*s3mm - &
               &3*s1mm*s3nm + 4*s3mm*s3nm)*ss**2 + (13*s1mm*s2nm - 13*s2nm*s3mm - 11*s1mm*s3nm +&
               & 24*s3mm*s3nm)*ss*tt + 12*(-2*s2nm*s3mm + s1mm*(s2nm + 2*s3nm))*tt**2 + Mm2*(s3m&
               &m*(s2nm - 24*s3nm)*ss + s1mm*(-7*s2nm*ss + 23*s3nm*ss - 16*s2nm*tt))) - 4*me2**3&
               &*(4*Mm2**3*(4*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm) + (2*s1mm*s2nm - 2*s2nm*s3m&
               &m - 9*s1mm*s3nm + 11*s3mm*s3nm)*ss**3 + (9*s1mm*s2nm - 6*s2nm*s3mm - 22*s1mm*s3n&
               &m + 28*s3mm*s3nm)*ss**2*tt + (11*s1mm*s2nm - 19*s2nm*s3mm + 19*s1mm*s3nm)*ss*tt*&
               &*2 + 4*(2*s1mm*s2nm - 5*s2nm*s3mm + 5*s1mm*s3nm)*tt**3 - Mm2*((-4*s3mm*(s2nm - 1&
               &1*s3nm) + 5*s1mm*(s2nm - 8*s3nm))*ss**2 + 2*(9*s1mm*s2nm + 5*s2nm*s3mm - 21*s1mm&
               &*s3nm + 16*s3mm*s3nm)*ss*tt + 4*(8*s1mm*s2nm - 7*s2nm*s3mm + 7*s1mm*s3nm)*tt**2)&
               & + Mm2**2*(s3mm*(-3*s2nm*ss + 32*s3nm*ss + 4*s2nm*tt) - s1mm*(9*s2nm*ss + 29*s3n&
               &m*ss - 8*s2nm*tt + 4*s3nm*tt))))/(me2*(4*me2 - ss)*(me2**2 + Mm2**2 - 2*Mm2*tt -&
               & 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_cm = -(1./2.)*(16*me2**4*Mm2**2*(s2nm*s3mm - s1mm*s3nm) + (-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s&
               &2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(16*Mm2**5 - 48*Mm&
               &2**4*tt - ss**2*tt**2*(ss + 2*tt) + Mm2**3*(-12*ss**2 + 32*ss*tt + 48*tt**2) + M&
               &m2**2*(7*ss**3 - 2*ss**2*tt - 48*ss*tt**2 - 16*tt**3) + Mm2*ss*(-ss**3 + 16*ss*t&
               &t**2 + 16*tt**3)) - 2*me2**3*(8*Mm2**3*s1mm*s2nm + 4*Mm2**2*(3*s1mm*s2nm*ss - 10&
               &*s1mm*s3nm*ss + 10*s3mm*s3nm*ss + 4*s2nm*s3mm*tt - 4*s1mm*s3nm*tt) + Mm2*ss*(5*s&
               &3mm*(s2nm - 4*s3nm)*ss + s1mm*(-7*s2nm + 15*s3nm)*ss + 8*(s2nm*s3mm - s1mm*s3nm)&
               &*tt) + ss**2*(s1mm*s2nm*ss + 3*s3mm*s3nm*ss + s1mm*s3nm*(-2*ss + tt) - s2nm*s3mm&
               &*(ss + tt))) - me2*(16*Mm2**5*(3*s1mm*s2nm - 4*s2nm*s3mm + 4*s1mm*s3nm) + 2*Mm2*&
               &*3*((13*s1mm*s2nm - 15*s2nm*s3mm + 83*s1mm*s3nm - 68*s3mm*s3nm)*ss**2 - 8*(7*s2n&
               &m*s3mm - 17*s1mm*s3nm + 10*s3mm*s3nm)*ss*tt - 8*s1mm*s2nm*tt**2) + 2*Mm2*ss*(8*(&
               &s1mm - s3mm)*s3nm*ss**3 - (5*s1mm*s2nm + 3*s2nm*s3mm - 23*s1mm*s3nm + 20*s3mm*s3&
               &nm)*ss**2*tt - (9*s1mm*s2nm - 11*s2nm*s3mm + 7*s1mm*s3nm + 4*s3mm*s3nm)*ss*tt**2&
               & + 24*(s2nm*s3mm - s1mm*s3nm)*tt**3) + ss**2*((-s1mm + s3mm)*s3nm*ss**3 + (s1mm*&
               &(s2nm - 4*s3nm) + 4*s3mm*s3nm)*ss**2*tt + 2*(s1mm*s2nm + s3mm*(-s2nm + s3nm))*ss&
               &*tt**2 + 6*(-(s2nm*s3mm) + s1mm*s3nm)*tt**3) + Mm2**2*((-4*s1mm*s2nm + 4*s2nm*s3&
               &mm - 82*s1mm*s3nm + 78*s3mm*s3nm)*ss**3 + 2*(16*s1mm*s2nm + 23*s2nm*s3mm - 103*s&
               &1mm*s3nm + 80*s3mm*s3nm)*ss**2*tt + 8*(7*s1mm*s2nm - 4*s2nm*s3mm - 2*s1mm*s3nm +&
               & 6*s3mm*s3nm)*ss*tt**2 + 32*(-(s2nm*s3mm) + s1mm*s3nm)*tt**3) - 8*Mm2**4*(-2*s3m&
               &m*(4*s2nm*ss + 7*s3nm*ss + 6*s2nm*tt) + s1mm*(5*s2nm*ss + 22*s3nm*ss + 4*s2nm*tt&
               & + 12*s3nm*tt))) + me2**2*(16*Mm2**4*(3*s1mm*s2nm - 4*s2nm*s3mm + 4*s1mm*s3nm) +&
               & 16*Mm2**3*((-(s1mm*s2nm) + 4*s2nm*s3mm + s1mm*s3nm - 5*s3mm*s3nm)*ss - 4*s2nm*s&
               &3mm*tt + s1mm*(s2nm + 4*s3nm)*tt) + Mm2*ss*((-9*s1mm*s2nm + 9*s2nm*s3mm + 39*s1m&
               &m*s3nm - 48*s3mm*s3nm)*ss**2 + 2*(-15*s1mm*s2nm + 7*s2nm*s3mm + 25*s1mm*s3nm - 3&
               &2*s3mm*s3nm)*ss*tt + 48*(s2nm*s3mm - s1mm*s3nm)*tt**2) + 8*Mm2**2*ss*(-4*s2nm*s3&
               &mm*(ss - tt) + 18*s3mm*s3nm*(ss + tt) + s1mm*(3*s2nm*ss - 14*s3nm*ss + 8*s2nm*tt&
               & - 22*s3nm*tt)) + ss**2*(5*s3mm*s3nm*ss*(ss + 2*tt) + s1mm*s2nm*ss*(ss + 4*tt) +&
               & s1mm*s3nm*(-4*ss**2 - 7*ss*tt + 6*tt**2) - s2nm*s3mm*(ss**2 + 3*ss*tt + 6*tt**2&
               &))))/(me2*(4*Mm2 - ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss +&
               & tt))**2)
    coeff_discse = (-2*me2**2*(-16*s2nm*s3mm + s1mm*(s2nm + 16*s3nm)) - 2*ss*(3*Mm2*s2nm*s3mm - 3*M&
                   &m2*s1mm*(s2nm + s3nm) + s1mm*s2nm*ss + 3*s1mm*s3nm*(ss + tt) - 3*s3mm*(s3nm*ss +&
                   & s2nm*tt)) + me2*(Mm2*(-26*s1mm*s2nm + 20*s2nm*s3mm - 20*s1mm*s3nm) - s3mm*(11*s&
                   &2nm*ss + 24*s3nm*ss + 20*s2nm*tt) + s1mm*(3*s2nm*ss + 35*s3nm*ss + 2*s2nm*tt + 2&
                   &0*s3nm*tt)))/(2.*me2*(4*me2 - ss)*ss)
    coeff_discse_lmm = (2*(2*me2 - ss)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt)&
                       & + s3mm*(s3nm*ss + s2nm*tt)))/(me2*(4*me2 - ss)*ss)
    coeff_discse_lmu = (-2*(2*me2 - ss)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt&
                       &) + s3mm*(s3nm*ss + s2nm*tt)))/(me2*(4*me2 - ss)*ss)
    coeff_discsm = -((8*Mm2**2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + Mm2*((-5*s1mm*s2nm + 3*s2nm*s3&
                   &mm - 11*s1mm*s3nm + 8*s3mm*s3nm)*ss - 4*(s2nm*s3mm - s1mm*s3nm)*(3*me2 - 2*tt)) &
                   &+ ss*(-3*s3mm*s3nm*ss + s1mm*(s2nm + 3*s3nm)*ss + 3*(s2nm*s3mm - s1mm*s3nm)*(2*m&
                   &e2 - tt)))/(me2*(4*Mm2 - ss)*ss))
    coeff_discsm_lmu = (-2*(2*Mm2 - ss)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt&
                       &) + s3mm*(s3nm*ss + s2nm*tt)))/(me2*(4*Mm2 - ss)*ss)
    coeff_disct = (2*me2**4*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) + 2*(-s1mm + s3mm)*s3nm*ss &
                  &- 5*s2nm*s3mm*tt + 5*s1mm*s3nm*tt) - me2**3*(8*Mm2**2*(-(s2nm*s3mm) + s1mm*(s2nm&
                  & + s3nm)) - 2*Mm2*(5*(s1mm - s3mm)*s3nm*ss + (-3*s1mm*s2nm + 7*s2nm*s3mm - 7*s1m&
                  &m*s3nm)*tt) + tt*(s1mm*(s2nm - 9*s3nm)*ss + s3mm*(s2nm + 8*s3nm)*ss + 30*(-(s2nm&
                  &*s3mm) + s1mm*s3nm)*tt)) + me2**2*(12*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm))&
                  & + Mm2*tt*((5*s1mm*s2nm - 3*s2nm*s3mm - 7*s1mm*s3nm + 10*s3mm*s3nm)*ss + 2*(3*s1&
                  &mm*s2nm + 8*s2nm*s3mm - 8*s1mm*s3nm)*tt) + tt*(3*(-s1mm + s3mm)*s3nm*ss**2 + (4*&
                  &s1mm*s2nm - 9*s2nm*s3mm + 5*s1mm*s3nm + 4*s3mm*s3nm)*ss*tt + 30*(-(s2nm*s3mm) + &
                  &s1mm*s3nm)*tt**2) - 2*Mm2**2*(-3*s1mm*s2nm*tt + 5*s1mm*s3nm*(ss + tt) - 5*s3mm*(&
                  &s3nm*ss + s2nm*tt))) + me2*(-8*Mm2**4*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + Mm2*&
                  &*3*(6*(s1mm - s3mm)*s3nm*ss + 2*(3*s1mm*s2nm - 11*s2nm*s3mm + 11*s1mm*s3nm)*tt) &
                  &+ 2*Mm2*tt*(2*(s1mm - s3mm)*s3nm*ss**2 + (s3mm*(-6*s2nm + s3nm) + s1mm*(s2nm + 5&
                  &*s3nm))*ss*tt - (13*s2nm*s3mm + s1mm*(s2nm - 13*s3nm))*tt**2) - tt**2*(2*(s1mm*(&
                  &s2nm - s3nm) + s3mm*s3nm)*ss**2 + (5*s1mm*s2nm - 11*s2nm*s3mm + 11*s1mm*s3nm)*ss&
                  &*tt + 10*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2) + Mm2**2*tt*(4*s3mm*s3nm*ss + 5*s2nm*&
                  &s3mm*(ss + 6*tt) - s1mm*(3*s2nm*ss + 9*s3nm*ss - 4*s2nm*tt + 30*s3nm*tt))) + (Mm&
                  &2 - tt)*(2*Mm2**4*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) - Mm2**2*tt*(s1mm*(s2nm - &
                  &5*s3nm)*ss + s3mm*(s2nm + 4*s3nm)*ss + 6*s2nm*s3mm*tt - 2*s1mm*(s2nm + 3*s3nm)*t&
                  &t) - 2*Mm2**3*((s1mm - s3mm)*s3nm*ss + (2*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)&
                  &*tt) - ss*tt**2*(s1mm*(2*s2nm + s3nm)*(ss + tt) - s3mm*(s3nm*ss + s2nm*tt)) + Mm&
                  &2*tt*(5*s1mm*s2nm*ss*tt + 2*s2nm*s3mm*tt**2 + s3mm*s3nm*ss*(ss + 2*tt) - s1mm*s3&
                  &nm*(ss**2 + 2*ss*tt + 2*tt**2))))/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2)*(me&
                  &2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_disct_lmm = -(1./2.)*(tt*(me2**5*(-6*Mm2*s2nm*s3mm + 6*Mm2*s1mm*(s2nm + s3nm) + s3mm*(s2nm + 2*s&
                      &3nm)*ss - s1mm*(s2nm + 3*s3nm)*ss + 6*(s2nm*s3mm - s1mm*s3nm)*tt) - me2**4*(18*M&
                      &m2**2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + (s1mm - s3mm)*s3nm*ss**2 + (-4*s1mm*&
                      &s2nm + 3*s2nm*s3mm - 17*s1mm*s3nm + 14*s3mm*s3nm)*ss*tt + 30*(s2nm*s3mm - s1mm*s&
                      &3nm)*tt**2 + Mm2*(s3mm*(s2nm + 10*s3nm)*ss - s1mm*(s2nm + 11*s3nm)*ss + 6*(5*s1m&
                      &m*s2nm - 2*s2nm*s3mm + 2*s1mm*s3nm)*tt)) + (Mm2 - tt)*(-(Mm2*s2nm*s3mm) + Mm2*s1&
                      &mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(6*Mm2**4 + M&
                      &m2**3*(ss - 24*tt) + 4*Mm2**2*tt*(2*ss + 9*tt) - Mm2*tt**2*(19*ss + 24*tt) + tt*&
                      &*2*(5*ss**2 + 10*ss*tt + 6*tt**2)) + me2**2*(12*Mm2**4*(-(s2nm*s3mm) + s1mm*(s2n&
                      &m + s3nm)) + Mm2**3*(2*s3mm*(s2nm + 10*s3nm)*ss - 2*s1mm*(s2nm + 11*s3nm)*ss + 1&
                      &2*s1mm*s2nm*tt) + tt**2*(27*(s1mm - s3mm)*s3nm*ss**2 + (4*s1mm*s2nm - 31*s2nm*s3&
                      &mm + 75*s1mm*s3nm - 44*s3mm*s3nm)*ss*tt + 60*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2) +&
                      & Mm2**2*(2*(s1mm - s3mm)*s3nm*ss**2 + (-5*s1mm*(3*s2nm + 5*s3nm) + s3mm*(13*s2nm&
                      & + 12*s3nm))*ss*tt + 12*(3*s1mm*s2nm - 2*s2nm*s3mm + 2*s1mm*s3nm)*tt**2) + Mm2*t&
                      &t*(7*(s1mm - s3mm)*s3nm*ss**2 + 3*(-13*s1mm*s2nm + 8*s2nm*s3mm - 12*s1mm*s3nm + &
                      &4*s3mm*s3nm)*ss*tt - 12*(5*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**2)) + me2*&
                      &(-18*Mm2**5*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + Mm2**4*((-(s1mm*s2nm) + s2nm*s&
                      &3mm + 21*s1mm*s3nm - 22*s3mm*s3nm)*ss + 6*(4*s1mm*s2nm - 7*s2nm*s3mm + 7*s1mm*s3&
                      &nm)*tt) + Mm2**3*tt*((-9*s1mm*s2nm + 9*s2nm*s3mm - 49*s1mm*s3nm + 40*s3mm*s3nm)*&
                      &ss + 12*(-(s2nm*s3mm) + s1mm*(3*s2nm + s3nm))*tt) + 3*Mm2**2*tt*(5*(s1mm - s3mm)&
                      &*s3nm*ss**2 + (-8*s1mm*s2nm + 3*s2nm*s3mm - 7*s1mm*s3nm + 4*s3mm*s3nm)*ss*tt - 1&
                      &2*(2*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt**2) + Mm2*tt**2*(7*(-(s3mm*(s2nm &
                      &+ 2*s3nm)) + s1mm*(s2nm + 3*s3nm))*ss**2 + 7*(5*s1mm*(s2nm + 3*s3nm) - s3mm*(7*s&
                      &2nm + 8*s3nm))*ss*tt + 6*(5*s1mm*s2nm - 17*s2nm*s3mm + 17*s1mm*s3nm)*tt**2) + tt&
                      &**2*(5*(-s1mm + s3mm)*s3nm*ss**3 + (5*s2nm*s3mm - 34*s1mm*s3nm + 29*s3mm*s3nm)*s&
                      &s**2*tt + (30*s2nm*s3mm + 26*s3mm*s3nm - s1mm*(s2nm + 56*s3nm))*ss*tt**2 + 30*(s&
                      &2nm*s3mm - s1mm*s3nm)*tt**3)) + me2**3*(12*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s&
                      &3nm)) + Mm2*tt*((13*s1mm*s2nm - 13*s2nm*s3mm - 11*s1mm*s3nm + 24*s3mm*s3nm)*ss +&
                      & 12*(5*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt) + tt*(7*(-s1mm + s3mm)*s3nm*ss&
                      &**2 + (-6*s1mm*s2nm + 13*s2nm*s3mm - 49*s1mm*s3nm + 36*s3mm*s3nm)*ss*tt + 60*(s2&
                      &nm*s3mm - s1mm*s3nm)*tt**2) + 2*Mm2**2*(2*s3mm*s3nm*ss - s2nm*s3mm*(ss + 6*tt) +&
                      & s1mm*(s2nm*ss - s3nm*ss + 12*s2nm*tt + 6*s3nm*tt)))))/(me2*ss*(-4*me2*Mm2 + (me&
                      &2 + Mm2 - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))&
                      &**2)
    coeff_disct_lmu = (4*(me2 + Mm2 - tt)*tt*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(s&
                      &s + tt) + s3mm*(s3nm*ss + s2nm*tt)))/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
    coeff_disct_ls = (tt*(me2**5*(-2*Mm2*s2nm*s3mm + 2*Mm2*s1mm*(s2nm + s3nm) + s3mm*(s2nm - 2*s3nm)*&
                     &ss + s1mm*(-s2nm + s3nm)*ss + 2*(s2nm*s3mm - s1mm*s3nm)*tt) - me2**4*(6*Mm2**2*(&
                     &-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + (s1mm - s3mm)*s3nm*ss**2 + (-4*s1mm*s2nm + &
                     &3*s2nm*s3mm + 3*s1mm*s3nm - 6*s3mm*s3nm)*ss*tt + 10*(s2nm*s3mm - s1mm*s3nm)*tt**&
                     &2 + Mm2*(s3mm*(s2nm - 2*s3nm)*ss + s1mm*(-s2nm + s3nm)*ss + 2*(5*s1mm*s2nm - 2*s&
                     &2nm*s3mm + 2*s1mm*s3nm)*tt)) + (Mm2 - tt)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s&
                     &3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(2*Mm2**4 + Mm2**3*(ss - &
                     &8*tt) + 12*Mm2**2*tt**2 - Mm2*tt**2*(3*ss + 8*tt) + tt**2*(ss**2 + 2*ss*tt + 2*t&
                     &t**2)) + me2*(-6*Mm2**5*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + Mm2**3*tt*(s3mm*(s&
                     &2nm + 24*s3nm)*ss - s1mm*(s2nm + 25*s3nm)*ss - 4*s2nm*s3mm*tt + 4*s1mm*(3*s2nm +&
                     & s3nm)*tt) + Mm2**4*((-(s1mm*s2nm) + s2nm*s3mm + 9*s1mm*s3nm - 10*s3mm*s3nm)*ss &
                     &+ 2*(4*s1mm*s2nm - 7*s2nm*s3mm + 7*s1mm*s3nm)*tt) + Mm2**2*tt*(7*(s1mm - s3mm)*s&
                     &3nm*ss**2 + (-8*s1mm*s2nm + s2nm*s3mm + 11*s1mm*s3nm - 12*s3mm*s3nm)*ss*tt - 12*&
                     &(2*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt**2) + Mm2*tt**2*((-3*s2nm*s3mm + 2*&
                     &s3mm*s3nm + s1mm*(3*s2nm + s3nm))*ss**2 + (11*s1mm*s2nm - 9*s2nm*s3mm + 17*s1mm*&
                     &s3nm - 8*s3mm*s3nm)*ss*tt + 2*(5*s1mm*s2nm - 17*s2nm*s3mm + 17*s1mm*s3nm)*tt**2)&
                     & + tt**2*((-s1mm + s3mm)*s3nm*ss**3 + (s2nm*s3mm - 6*s1mm*s3nm + 5*s3mm*s3nm)*ss&
                     &**2*tt + (6*s3mm*(s2nm + s3nm) - s1mm*(s2nm + 12*s3nm))*ss*tt**2 + 10*(s2nm*s3mm&
                     & - s1mm*s3nm)*tt**3)) + me2**3*(4*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + 2&
                     &*Mm2**2*(-(s3mm*(s2nm + 2*s3nm)*ss) + s1mm*(s2nm + 3*s3nm)*ss - 2*s2nm*s3mm*tt +&
                     & 2*s1mm*(2*s2nm + s3nm)*tt) + Mm2*tt*((5*s1mm*s2nm - 5*s2nm*s3mm - 3*s1mm*s3nm +&
                     & 8*s3mm*s3nm)*ss + 4*(5*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt) - tt*(6*s1mm*&
                     &s2nm*ss*tt + s3mm*(ss + 4*tt)*(s3nm*ss - 5*s2nm*tt) + s1mm*s3nm*(-ss**2 + ss*tt &
                     &+ 20*tt**2))) + me2**2*(4*Mm2**4*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) - 2*Mm2**3*&
                     &(-(s3mm*(s2nm + 6*s3nm)*ss) + s1mm*(s2nm + 7*s3nm)*ss - 2*s1mm*s2nm*tt) + tt**2*&
                     &(3*(s1mm - s3mm)*s3nm*ss**2 + (4*s1mm*s2nm - 7*s2nm*s3mm + 11*s1mm*s3nm - 4*s3mm&
                     &*s3nm)*ss*tt + 20*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2) + Mm2**2*(2*(s1mm - s3mm)*s3&
                     &nm*ss**2 + (-7*s1mm*s2nm + 5*s2nm*s3mm - 9*s1mm*s3nm + 4*s3mm*s3nm)*ss*tt + 4*(3&
                     &*s1mm*s2nm - 2*s2nm*s3mm + 2*s1mm*s3nm)*tt**2) + Mm2*tt*(s3mm*s3nm*ss*(ss - 12*t&
                     &t) + 8*s2nm*s3mm*tt*(ss + 4*tt) - 5*s1mm*s2nm*tt*(3*ss + 4*tt) - s1mm*s3nm*(ss**&
                     &2 - 4*ss*tt + 32*tt**2)))))/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2)*(me2**2 +&
                     & Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_discu = -(1./2.)*(-104*me2**6*(s2nm*s3mm - s1mm*s3nm) + 2*me2**5*(Mm2*(17*s1mm*s2nm - 57*s2n&
                  &m*s3mm + 57*s1mm*s3nm) + (-3*s1mm*s2nm + 98*s2nm*s3mm - 125*s1mm*s3nm + 27*s3mm*&
                  &s3nm)*ss + 220*(s2nm*s3mm - s1mm*s3nm)*tt) + me2**4*(Mm2**2*(46*s1mm*s2nm + 226*&
                  &s2nm*s3mm - 226*s1mm*s3nm) + (13*s1mm*(s2nm + 17*s3nm) - s3mm*(107*s2nm + 114*s3&
                  &nm))*ss**2 + (33*s1mm*s2nm - 793*s2nm*s3mm + 979*s1mm*s3nm - 186*s3mm*s3nm)*ss*t&
                  &t + 746*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2 + 2*Mm2*s3mm*(3*s2nm*ss + 79*s3nm*ss + &
                  &452*s2nm*tt) - 2*Mm2*s1mm*(19*s2nm*ss + 82*s3nm*ss + 65*s2nm*tt + 452*s3nm*tt)) &
                  &+ me2**2*(-4*Mm2**4*(9*s1mm*s2nm + 17*s2nm*s3mm - 17*s1mm*s3nm) + (ss + tt)**2*(&
                  &2*(s3mm*(s2nm - 11*s3nm) + s1mm*(s2nm + 10*s3nm))*ss**2 + (59*s1mm*s2nm - 171*s2&
                  &nm*s3mm + 331*s1mm*s3nm - 160*s3mm*s3nm)*ss*tt + 296*(-(s2nm*s3mm) + s1mm*s3nm)*&
                  &tt**2) + 4*Mm2*(ss + tt)*((6*s1mm*s2nm - 13*s2nm*s3mm - 13*s1mm*s3nm + 26*s3mm*s&
                  &3nm)*ss**2 + (-70*s1mm*s2nm + 219*s2nm*s3mm - 355*s1mm*s3nm + 136*s3mm*s3nm)*ss*&
                  &tt - 2*(17*s1mm*s2nm - 162*s2nm*s3mm + 162*s1mm*s3nm)*tt**2) + Mm2**2*((-81*s1mm&
                  &*s2nm + 135*s2nm*s3mm + 5*s1mm*s3nm - 140*s3mm*s3nm)*ss**2 + (459*s1mm*s2nm - 13&
                  &85*s2nm*s3mm + 1861*s1mm*s3nm - 476*s3mm*s3nm)*ss*tt + 4*(108*s1mm*s2nm - 443*s2&
                  &nm*s3mm + 443*s1mm*s3nm)*tt**2) + 4*Mm2**3*(s3mm*(-6*s2nm*ss + 19*s3nm*ss + 202*&
                  &s2nm*tt) + s1mm*(30*s2nm*ss - 13*s3nm*ss - 69*s2nm*tt - 202*s3nm*tt))) + me2**3*&
                  &(-4*Mm2**3*(27*s1mm*s2nm - 43*s2nm*s3mm + 43*s1mm*s3nm) - (ss + tt)*((9*s1mm*s2n&
                  &m - 13*s2nm*s3mm + 93*s1mm*s3nm - 80*s3mm*s3nm)*ss**2 + (65*s1mm*s2nm - 513*s2nm&
                  &*s3mm + 761*s1mm*s3nm - 248*s3mm*s3nm)*ss*tt + 644*(-(s2nm*s3mm) + s1mm*s3nm)*tt&
                  &**2) + Mm2*((-7*s1mm*s2nm + 137*s2nm*s3mm + 95*s1mm*s3nm - 232*s3mm*s3nm)*ss**2 &
                  &+ (253*s1mm*s2nm - 1379*s2nm*s3mm + 1867*s1mm*s3nm - 488*s3mm*s3nm)*ss*tt + 16*(&
                  &12*s1mm*s2nm - 107*s2nm*s3mm + 107*s1mm*s3nm)*tt**2) + 4*Mm2**2*(s3mm*(-107*s2nm&
                  &*ss + 35*s3nm*ss + 202*s2nm*tt) + 2*s1mm*(5*s2nm*ss + 36*s3nm*ss - 36*s2nm*tt - &
                  &101*s3nm*tt))) + me2*(6*Mm2**5*(7*s1mm*s2nm - 15*s2nm*s3mm + 15*s1mm*s3nm) - (ss&
                  & + tt)**3*(2*(s1mm - s3mm)*s3nm*ss**2 + (25*s1mm*s2nm - 23*s2nm*s3mm + 73*s1mm*s&
                  &3nm - 50*s3mm*s3nm)*ss*tt + 68*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2) + Mm2**3*((3*s1&
                  &mm*s2nm - 125*s2nm*s3mm + 229*s1mm*s3nm - 104*s3mm*s3nm)*ss**2 + (479*s1mm*s2nm &
                  &- 1009*s2nm*s3mm + 1305*s1mm*s3nm - 296*s3mm*s3nm)*ss*tt + 16*(24*s1mm*s2nm - 65&
                  &*s2nm*s3mm + 65*s1mm*s3nm)*tt**2) - Mm2*(ss + tt)**2*(4*(2*s1mm*s2nm - 5*s1mm*s3&
                  &nm + 5*s3mm*s3nm)*ss**2 + (-161*s1mm*s2nm + 207*s2nm*s3mm - 463*s1mm*s3nm + 256*&
                  &s3mm*s3nm)*ss*tt - 2*(23*s1mm*s2nm - 211*s2nm*s3mm + 211*s1mm*s3nm)*tt**2) + Mm2&
                  &**2*(ss + tt)*((19*s1mm*s2nm + 25*s2nm*s3mm - 89*s1mm*s3nm + 64*s3mm*s3nm)*ss**2&
                  & + (-405*s1mm*s2nm + 669*s2nm*s3mm - 1109*s1mm*s3nm + 440*s3mm*s3nm)*ss*tt - 4*(&
                  &58*s1mm*s2nm - 243*s2nm*s3mm + 243*s1mm*s3nm)*tt**2) + Mm2**4*(62*s3mm*s3nm*ss +&
                  & 8*s2nm*s3mm*(25*ss + 64*tt) - 2*s1mm*(25*s2nm*ss + 131*s3nm*ss + 120*s2nm*tt + &
                  &256*s3nm*tt))) + 2*(Mm2 - ss - tt)*(11*Mm2**5*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)&
                  &) + 2*Mm2**3*((7*s1mm*s2nm - 3*s2nm*s3mm + 10*s1mm*s3nm - 7*s3mm*s3nm)*ss**2 + (&
                  &34*s1mm*s2nm - 29*s2nm*s3mm + 46*s1mm*s3nm - 17*s3mm*s3nm)*ss*tt + (19*s1mm*s2nm&
                  & - 36*s2nm*s3mm + 36*s1mm*s3nm)*tt**2) - tt*(ss + tt)**3*(2*s1mm*s2nm*ss + 3*s1m&
                  &m*s3nm*(ss + tt) - 3*s3mm*(s3nm*ss + s2nm*tt)) + Mm2**4*(s3mm*(14*s2nm*ss + 11*s&
                  &3nm*ss + 45*s2nm*tt) - s1mm*(22*s2nm*ss + 25*s3nm*ss + 34*s2nm*tt + 45*s3nm*tt))&
                  & - Mm2**2*(ss + tt)*(-2*s3mm*s3nm*ss*(3*ss + 19*tt) + 3*s1mm*s2nm*(ss**2 + 15*ss&
                  &*tt + 6*tt**2) + 7*s1mm*s3nm*(ss**2 + 9*ss*tt + 8*tt**2) - s2nm*s3mm*(ss**2 + 25&
                  &*ss*tt + 56*tt**2)) + Mm2*(ss + tt)**2*(-(s3mm*(s3nm*ss*(ss + 18*tt) + s2nm*tt*(&
                  &4*ss + 21*tt))) + s1mm*(3*s2nm*tt*(5*ss + tt) + s3nm*(ss**2 + 22*ss*tt + 21*tt**&
                  &2)))))/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)*(-2*me2 - 2*Mm2 + ss + tt&
                  &)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_discu_lmm = ((2*me2 + 2*Mm2 - ss - tt)*(me2**5*(6*Mm2*s2nm*s3mm - 6*Mm2*s1mm*(s2nm + s3nm) -&
                      & s3mm*(s2nm + 2*s3nm)*ss + s1mm*(s2nm + 3*s3nm)*ss - 6*s2nm*s3mm*tt + 6*s1mm*s3n&
                      &m*tt) - (-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*&
                      &(s3nm*ss + s2nm*tt))*(6*Mm2**5 - 6*tt**2*(ss + tt)**3 + 3*Mm2**3*tt*(13*ss + 20*&
                      &tt) - Mm2**4*(7*ss + 30*tt) - Mm2*(ss + tt)**2*(ss**2 - ss*tt - 30*tt**2) + Mm2*&
                      &*2*(2*ss**3 - 13*ss**2*tt - 75*ss*tt**2 - 60*tt**3)) - me2**3*(12*Mm2**3*(-(s2nm&
                      &*s3mm) + s1mm*(s2nm + s3nm)) + Mm2*((5*s1mm*s2nm - s2nm*s3mm - 39*s1mm*s3nm + 40&
                      &*s3mm*s3nm)*ss**2 + (39*s1mm*s2nm - 7*s2nm*s3mm - 17*s1mm*s3nm + 24*s3mm*s3nm)*s&
                      &s*tt + 12*(5*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt**2) + 4*Mm2**2*(3*s1mm*s3&
                      &nm*(ss + tt) + s1mm*s2nm*(5*ss + 6*tt) + s3mm*(-4*s2nm*ss + s3nm*ss - 3*s2nm*tt)&
                      &) - 3*s1mm*(ss + tt)*(s2nm*ss*(ss + 2*tt) + s3nm*(-3*ss**2 + 7*ss*tt + 20*tt**2)&
                      &) + 3*s3mm*(s3nm*ss*(-4*ss**2 + ss*tt + 12*tt**2) + s2nm*(ss**3 + 3*ss**2*tt + 1&
                      &5*ss*tt**2 + 20*tt**3))) + me2**4*(18*Mm2**2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm))&
                      & + (-3*s1mm*s2nm + 3*s2nm*s3mm + 2*s1mm*s3nm - 5*s3mm*s3nm)*ss**2 + (-4*s1mm*s2n&
                      &m + 11*s2nm*s3mm - 25*s1mm*s3nm + 14*s3mm*s3nm)*ss*tt + 30*(s2nm*s3mm - s1mm*s3n&
                      &m)*tt**2 + Mm2*(10*s3mm*s3nm*ss - s2nm*s3mm*(ss + 12*tt) + s1mm*(-9*s3nm*ss + 12&
                      &*s3nm*tt + 5*s2nm*(ss + 6*tt)))) + me2*(18*Mm2**5*(-(s2nm*s3mm) + s1mm*(s2nm + s&
                      &3nm)) + Mm2**3*((5*s1mm*s2nm - 9*s2nm*s3mm + 41*s1mm*s3nm - 32*s3mm*s3nm)*ss**2 &
                      &+ (23*s1mm*s2nm - 55*s2nm*s3mm + 95*s1mm*s3nm - 40*s3mm*s3nm)*ss*tt + 12*(s2nm*s&
                      &3mm - s1mm*(3*s2nm + s3nm))*tt**2) + Mm2**2*(5*(s3mm*(s2nm + 4*s3nm) - s1mm*(s2n&
                      &m + 5*s3nm))*ss**3 + (-15*s1mm*s2nm + 35*s2nm*s3mm - 94*s1mm*s3nm + 59*s3mm*s3nm&
                      &)*ss**2*tt + (-3*s3mm*(9*s2nm + 4*s3nm) + s1mm*(62*s2nm + 39*s3nm))*ss*tt**2 + 3&
                      &6*(2*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt**3) + Mm2**4*(s3mm*(25*s2nm*ss + &
                      &22*s3nm*ss + 42*s2nm*tt) - s1mm*(21*s2nm*ss + 47*s3nm*ss + 24*s2nm*tt + 42*s3nm*&
                      &tt)) - (ss + tt)**2*(s1mm*(ss + tt)*(-(s2nm*ss*tt) + s3nm*(ss**2 + 2*ss*tt - 30*&
                      &tt**2)) + s3mm*(2*s2nm*tt**2*(ss + 15*tt) - s3nm*ss*(ss**2 + 3*ss*tt - 26*tt**2)&
                      &)) + Mm2*(ss + tt)*(s1mm*(ss + tt)*(-5*s2nm*tt*(ss + 6*tt) + s3nm*(12*ss**2 + 29&
                      &*ss*tt - 102*tt**2)) + s3mm*(-2*s3nm*ss*(6*ss**2 + 17*ss*tt - 28*tt**2) + s2nm*t&
                      &t*(-7*ss**2 + 17*ss*tt + 102*tt**2)))) + me2**2*(-12*Mm2**4*(-(s2nm*s3mm) + s1mm&
                      &*(s2nm + s3nm)) + 4*Mm2**3*((7*s1mm*s2nm - 8*s2nm*s3mm + 13*s1mm*s3nm - 5*s3mm*s&
                      &3nm)*ss - 3*s1mm*s2nm*tt) + Mm2**2*((3*s1mm*s2nm + 5*s2nm*s3mm - 11*s1mm*s3nm + &
                      &6*s3mm*s3nm)*ss**2 + 3*(9*s1mm*s2nm - 3*s2nm*s3mm + 7*s1mm*s3nm - 4*s3mm*s3nm)*s&
                      &s*tt - 12*(3*s1mm*s2nm - 2*s2nm*s3mm + 2*s1mm*s3nm)*tt**2) + Mm2*(4*(s1mm*s2nm -&
                      & s2nm*s3mm - 7*s1mm*s3nm + 8*s3mm*s3nm)*ss**3 + (25*s1mm*s2nm - 5*s2nm*s3mm - 66&
                      &*s1mm*s3nm + 71*s3mm*s3nm)*ss**2*tt + (81*s1mm*s2nm - 46*s2nm*s3mm + 58*s1mm*s3n&
                      &m - 12*s3mm*s3nm)*ss*tt**2 + 12*(5*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**3)&
                      & - (ss + tt)*(s1mm*(ss + tt)*(s2nm*ss*(ss + 4*tt) + s3nm*(-5*ss**2 + 3*ss*tt + 6&
                      &0*tt**2)) - s3mm*(s3nm*ss*(-6*ss**2 - 5*ss*tt + 44*tt**2) + s2nm*(ss**3 + 3*ss**&
                      &2*tt + 19*ss*tt**2 + 60*tt**3))))))/(2.*me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - t&
                      &t)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_discu_lmu = (4*(me2 + Mm2 - ss - tt)*(2*me2 + 2*Mm2 - ss - tt)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*&
                      &(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt)))/(me2*ss*(me2**2&
                      & + (-Mm2 + ss + tt)**2 - 2*me2*(Mm2 + ss + tt)))
    coeff_discu_ls = -(((2*me2 + 2*Mm2 - ss - tt)*(-(me2**5*(-2*Mm2*s2nm*s3mm + 2*Mm2*s1mm*(s2nm + s3&
                     &nm) + s3mm*(s2nm - 2*s3nm)*ss + s1mm*(-s2nm + s3nm)*ss + 2*(s2nm*s3mm - s1mm*s3n&
                     &m)*tt)) - (-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3m&
                     &m*(s3nm*ss + s2nm*tt))*(2*Mm2**5 - 2*tt**2*(ss + tt)**3 + 5*Mm2**3*tt*(3*ss + 4*&
                     &tt) - Mm2**4*(3*ss + 10*tt) - Mm2*(ss + tt)**2*(ss**2 - ss*tt - 10*tt**2) + Mm2*&
                     &*2*(2*ss**3 - 5*ss**2*tt - 27*ss*tt**2 - 20*tt**3)) - me2**3*(4*Mm2**3*(-(s2nm*s&
                     &3mm) + s1mm*(s2nm + s3nm)) - 3*(s1mm*s2nm - s2nm*s3mm - 3*s1mm*s3nm + 4*s3mm*s3n&
                     &m)*ss**3 - 3*(3*s1mm*s2nm - 3*s2nm*s3mm - 4*s1mm*s3nm + 7*s3mm*s3nm)*ss**2*tt - &
                     &(6*s1mm*s2nm - 21*s2nm*s3mm + 17*s1mm*s3nm + 4*s3mm*s3nm)*ss*tt**2 + 20*(s2nm*s3&
                     &mm - s1mm*s3nm)*tt**3 + Mm2*((5*s1mm*s2nm - s2nm*s3mm - 23*s1mm*s3nm + 24*s3mm*s&
                     &3nm)*ss**2 + (15*s1mm*s2nm + s2nm*s3mm - 9*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt + 4*(5&
                     &*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt**2) + 4*Mm2**2*(s1mm*s3nm*(ss + tt) +&
                     & s1mm*s2nm*(ss + 2*tt) - s3mm*(s3nm*ss + s2nm*tt))) + me2**4*(6*Mm2**2*(-(s2nm*s&
                     &3mm) + s1mm*(s2nm + s3nm)) + (-3*s1mm*s2nm + 3*s2nm*s3mm + 6*s1mm*s3nm - 9*s3mm*&
                     &s3nm)*ss**2 - (-7*s2nm*s3mm + 6*s3mm*s3nm + s1mm*(4*s2nm + s3nm))*ss*tt + 10*(s2&
                     &nm*s3mm - s1mm*s3nm)*tt**2 + Mm2*(s3mm*(3*s2nm*ss - 2*s3nm*ss - 4*s2nm*tt) + s1m&
                     &m*(s2nm*ss - s3nm*ss + 10*s2nm*tt + 4*s3nm*tt))) + me2*(6*Mm2**5*(-(s2nm*s3mm) +&
                     & s1mm*(s2nm + s3nm)) + Mm2**3*((5*s1mm*(s2nm + 5*s3nm) - s3mm*(9*s2nm + 16*s3nm)&
                     &)*ss**2 - (s1mm*(s2nm - 39*s3nm) + 3*s3mm*(5*s2nm + 8*s3nm))*ss*tt + 4*(s2nm*s3m&
                     &m - s1mm*(3*s2nm + s3nm))*tt**2) + Mm2**2*(5*(s3mm*(s2nm + 4*s3nm) - s1mm*(s2nm &
                     &+ 5*s3nm))*ss**3 + (s1mm*s2nm + 19*s2nm*s3mm - 54*s1mm*s3nm + 35*s3mm*s3nm)*ss**&
                     &2*tt + (30*s1mm*s2nm - 19*s2nm*s3mm + 7*s1mm*s3nm + 12*s3mm*s3nm)*ss*tt**2 + 12*&
                     &(2*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt**3) + Mm2**4*(s3mm*(9*s2nm*ss + 10*&
                     &s3nm*ss + 14*s2nm*tt) - s1mm*(5*s2nm*ss + 19*s3nm*ss + 8*s2nm*tt + 14*s3nm*tt)) &
                     &- (ss + tt)**2*(s1mm*(ss + tt)*(-(s2nm*ss*tt) + s3nm*(ss**2 + 2*ss*tt - 10*tt**2&
                     &)) + s3mm*(2*s2nm*tt**2*(ss + 5*tt) - s3nm*ss*(ss**2 + 3*ss*tt - 6*tt**2))) + Mm&
                     &2*(ss + tt)*(s1mm*(ss + tt)*(-5*s2nm*tt*(ss + 2*tt) + s3nm*(12*ss**2 + 13*ss*tt &
                     &- 34*tt**2)) + s3mm*(-2*s3nm*ss*(6*ss**2 + 9*ss*tt - 4*tt**2) + s2nm*tt*(-7*ss**&
                     &2 + 13*ss*tt + 34*tt**2)))) + me2**2*(-4*Mm2**4*(-(s2nm*s3mm) + s1mm*(s2nm + s3n&
                     &m)) + 4*Mm2**3*((s1mm*s2nm - 2*s2nm*s3mm + 5*s1mm*s3nm - 3*s3mm*s3nm)*ss - s1mm*&
                     &s2nm*tt) + Mm2**2*((3*s1mm*s2nm + 5*s2nm*s3mm + 13*s1mm*s3nm - 18*s3mm*s3nm)*ss*&
                     &*2 + (3*s1mm*s2nm - 9*s2nm*s3mm + 13*s1mm*s3nm - 4*s3mm*s3nm)*ss*tt - 4*(3*s1mm*&
                     &s2nm - 2*s2nm*s3mm + 2*s1mm*s3nm)*tt**2) + Mm2*(4*(s1mm*s2nm - s2nm*s3mm - 7*s1m&
                     &m*s3nm + 8*s3mm*s3nm)*ss**3 + (17*s1mm*s2nm + 3*s2nm*s3mm - 50*s1mm*s3nm + 47*s3&
                     &mm*s3nm)*ss**2*tt + (33*s1mm*s2nm - 22*s2nm*s3mm + 10*s1mm*s3nm + 12*s3mm*s3nm)*&
                     &ss*tt**2 + 4*(5*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**3) - (ss + tt)*(s1mm*&
                     &(ss + tt)*(s2nm*ss*(ss + 4*tt) - 5*s3nm*(ss**2 + ss*tt - 4*tt**2)) - s3mm*(s3nm*&
                     &ss*(-6*ss**2 - 13*ss*tt + 4*tt**2) + s2nm*(ss**3 + 3*ss**2*tt + 11*ss*tt**2 + 20&
                     &*tt**3))))))/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)*(me2**2 + Mm2**2 - &
                     &2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2))
    coeff_lmm = -(1./4.)*(8*me2**5*(4*Mm2 - ss)*(-2*Mm2*s2nm*s3mm + 2*Mm2*s1mm*(s2nm + s3nm) + 4*(-&
                &s1mm + s3mm)*s3nm*ss + 3*(s2nm*s3mm - s1mm*s3nm)*tt) - 2*(-(Mm2*s2nm*s3mm) + Mm2&
                &*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(32*Mm2**6&
                & + ss*tt**2*(ss + tt)**2*(6*ss + 5*tt) - 4*Mm2**5*(10*ss + 43*tt) + Mm2**4*(16*s&
                &s**2 + 227*ss*tt + 336*tt**2) - 4*Mm2*tt**2*(13*ss**3 + 30*ss**2*tt + 22*ss*tt**&
                &2 + 5*tt**3) + 4*Mm2**2*tt*(4*ss**3 + 54*ss**2*tt + 81*ss*tt**2 + 32*tt**3) - 2*&
                &Mm2**3*(ss**3 + 51*ss**2*tt + 218*ss*tt**2 + 152*tt**3)) - me2**3*(128*Mm2**4*(-&
                &(s2nm*s3mm) + s1mm*(s2nm + s3nm)) - 32*Mm2**3*((5*s1mm*s2nm - 5*s2nm*s3mm + 9*s1&
                &mm*s3nm - 4*s3mm*s3nm)*ss + s2nm*s3mm*tt - s1mm*(2*s2nm + s3nm)*tt) + 8*Mm2**2*(&
                &2*(-(s3mm*(s2nm + 6*s3nm)) + s1mm*(s2nm + 7*s3nm))*ss**2 + (-31*s1mm*s2nm + 5*s2&
                &nm*s3mm - 39*s1mm*s3nm + 34*s3mm*s3nm)*ss*tt + (22*s1mm*s2nm - 15*s2nm*s3mm + 15&
                &*s1mm*s3nm)*tt**2) + 2*Mm2*(2*(-(s3mm*(s2nm + 4*s3nm)) + s1mm*(s2nm + 5*s3nm))*s&
                &s**3 + (19*s1mm*(s2nm + 12*s3nm) - 2*s3mm*(13*s2nm + 101*s3nm))*ss**2*tt + (-32*&
                &s1mm*s2nm - 139*s2nm*s3mm + 99*s1mm*s3nm + 40*s3mm*s3nm)*ss*tt**2 + 84*(s2nm*s3m&
                &m - s1mm*s3nm)*tt**3) + ss*(8*(-s1mm + s3mm)*s3nm*ss**3 + (s1mm*s2nm + 3*s2nm*s3&
                &mm - 71*s1mm*s3nm + 68*s3mm*s3nm)*ss**2*tt + (5*s1mm*s2nm + 29*s2nm*s3mm - 9*s1m&
                &m*s3nm - 20*s3mm*s3nm)*ss*tt**2 + 42*(-(s2nm*s3mm) + s1mm*s3nm)*tt**3)) + 2*me2*&
                &*2*(64*Mm2**5*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) - 8*Mm2**4*(-2*s2nm*s3mm*ss + &
                &2*s1mm*(s2nm + s3nm)*ss + 3*(7*s1mm*s2nm - 9*s2nm*s3mm + 9*s1mm*s3nm)*tt) - 2*Mm&
                &2**3*(4*(3*s1mm*s2nm - 3*s2nm*s3mm + 7*s1mm*s3nm - 4*s3mm*s3nm)*ss**2 + (-139*s1&
                &mm*s2nm + 147*s2nm*s3mm - 179*s1mm*s3nm + 32*s3mm*s3nm)*ss*tt + 6*(24*s1mm*s2nm &
                &- 11*s2nm*s3mm + 11*s1mm*s3nm)*tt**2) + ss*tt*(-18*(s1mm - s3mm)*s3nm*ss**3 + (7&
                &*s3mm*(s2nm - 4*s3nm) + s1mm*(2*s2nm + 21*s3nm))*ss**2*tt + 2*(s1mm*s2nm - 22*s2&
                &nm*s3mm + 43*s1mm*s3nm - 21*s3mm*s3nm)*ss*tt**2 + 47*(-(s2nm*s3mm) + s1mm*s3nm)*&
                &tt**3) + Mm2**2*((6*s1mm*s2nm - 6*s2nm*s3mm + 38*s1mm*s3nm - 32*s3mm*s3nm)*ss**3&
                & + (-151*s1mm*s2nm + 120*s2nm*s3mm - 368*s1mm*s3nm + 248*s3mm*s3nm)*ss**2*tt + (&
                &148*s1mm*s2nm + 99*s2nm*s3mm + 189*s1mm*s3nm - 288*s3mm*s3nm)*ss*tt**2 + 8*(22*s&
                &1mm*s2nm - 63*s2nm*s3mm + 63*s1mm*s3nm)*tt**3) - Mm2*(6*(s1mm - s3mm)*s3nm*ss**4&
                & + (-17*s1mm*s2nm + 15*s2nm*s3mm - 169*s1mm*s3nm + 154*s3mm*s3nm)*ss**3*tt + (43&
                &*s1mm*s2nm + 101*s2nm*s3mm + 19*s1mm*s3nm - 120*s3mm*s3nm)*ss**2*tt**2 + 2*(26*s&
                &1mm*s2nm - 99*s2nm*s3mm + 183*s1mm*s3nm - 84*s3mm*s3nm)*ss*tt**3 + 188*(-(s2nm*s&
                &3mm) + s1mm*s3nm)*tt**4)) - 2*me2**4*(32*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s3n&
                &m)) + Mm2*(8*(s3mm*(s2nm + 9*s3nm) - s1mm*(s2nm + 10*s3nm))*ss**2 + (-5*s1mm*s2n&
                &m + 61*s2nm*s3mm - 169*s1mm*s3nm + 108*s3mm*s3nm)*ss*tt + 64*(s2nm*s3mm - s1mm*s&
                &3nm)*tt**2) + ss*(16*(s1mm - s3mm)*s3nm*ss**2 - (s1mm*s2nm + 10*s2nm*s3mm - 37*s&
                &1mm*s3nm + 27*s3mm*s3nm)*ss*tt + 16*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2) + 4*Mm2**2&
                &*(s3mm*(-6*s2nm*ss - 8*s3nm*ss + 11*s2nm*tt) + s1mm*(6*s2nm*ss + 14*s3nm*ss + 9*&
                &s2nm*tt - 11*s3nm*tt))) + me2*(64*Mm2**6*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) - 1&
                &6*Mm2**5*(-9*s2nm*s3mm*ss + 9*s1mm*(s2nm + s3nm)*ss + 4*s2nm*s3mm*tt - 4*s1mm*(2&
                &*s2nm + s3nm)*tt) + 8*Mm2**4*(2*(5*s1mm*s2nm - 5*s2nm*s3mm + 9*s1mm*s3nm - 4*s3m&
                &m*s3nm)*ss**2 + (19*s1mm*s2nm + 34*s3mm*(-s2nm + s3nm))*ss*tt + (-110*s1mm*s2nm &
                &+ 129*s2nm*s3mm - 129*s1mm*s3nm)*tt**2) - 2*Mm2**3*(6*(-(s3mm*(s2nm + 4*s3nm)) +&
                & s1mm*(s2nm + 5*s3nm))*ss**3 + (-2*s3mm*(77*s2nm + 3*s3nm) + 5*s1mm*(25*s2nm + 3&
                &2*s3nm))*ss**2*tt + (-408*s1mm*s2nm + 359*s2nm*s3mm - 847*s1mm*s3nm + 488*s3mm*s&
                &3nm)*ss*tt**2 - 52*(8*s1mm*s2nm - 17*s2nm*s3mm + 17*s1mm*s3nm)*tt**3) + 2*Mm2*tt&
                &*((-4*s1mm*s2nm + 4*s2nm*s3mm - 50*s1mm*s3nm + 46*s3mm*s3nm)*ss**4 + (26*s1mm*s2&
                &nm + 13*s2nm*s3mm + 119*s1mm*s3nm - 132*s3mm*s3nm)*ss**3*tt + 2*(29*s1mm*s2nm - &
                &102*s2nm*s3mm + 239*s1mm*s3nm - 137*s3mm*s3nm)*ss**2*tt**2 + (28*s1mm*s2nm - 313&
                &*s2nm*s3mm + 417*s1mm*s3nm - 104*s3mm*s3nm)*ss*tt**3 + 108*(-(s2nm*s3mm) + s1mm*&
                &s3nm)*tt**4) + Mm2**2*(8*(s1mm - s3mm)*s3nm*ss**4 + (95*s1mm*s2nm - 99*s2nm*s3mm&
                & + 311*s1mm*s3nm - 212*s3mm*s3nm)*ss**3*tt + (-277*s1mm*s2nm + 75*s2nm*s3mm - 92&
                &7*s1mm*s3nm + 852*s3mm*s3nm)*ss**2*tt**2 + 2*(-7*s1mm*(40*s2nm + 163*s3nm) + s3m&
                &m*(717*s2nm + 424*s3nm))*ss*tt**3 - 8*(26*s1mm*s2nm - 135*s2nm*s3mm + 135*s1mm*s&
                &3nm)*tt**4) + ss*tt*(ss + tt)*(s1mm*(ss + tt)*(-(s2nm*ss*tt) + s3nm*(8*ss**2 - 5&
                &7*ss*tt - 54*tt**2)) + s3mm*(s2nm*tt*(-3*ss**2 + 59*ss*tt + 54*tt**2) + s3nm*(-8&
                &*ss**3 + 52*ss**2*tt + 52*ss*tt**2)))))/(me2*(4*Mm2 - ss)*ss*tt*(-2*me2 - 2*Mm2 &
                &+ ss + tt)**2*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_lmu = (5*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm&
                &*ss + s2nm*tt)))/(me2*ss)
    coeff_ls = -((me2**3*(-8*Mm2*(s2nm*s3mm + s1mm*(s2nm - s3nm)) + 2*(-(s2nm*s3mm) + s1mm*(s2n&
               &m + s3nm))*ss) + ss**2*(Mm2 + tt)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s&
               &1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt)) + me2**2*(16*Mm2**2*(s2nm*s3mm + &
               &s1mm*(s2nm - s3nm)) + 4*Mm2*((s1mm*s2nm - s2nm*s3mm - 7*s1mm*s3nm + 8*s3mm*s3nm)&
               &*ss + 4*(s2nm*s3mm + s1mm*(s2nm - s3nm))*tt) + ss*((-(s1mm*s2nm) + s2nm*s3mm + 3&
               &*s1mm*s3nm - 4*s3mm*s3nm)*ss + 4*s2nm*s3mm*tt - 4*s1mm*(s2nm + s3nm)*tt)) + me2*&
               &(-8*Mm2**3*(s2nm*s3mm + s1mm*(s2nm - s3nm)) - 2*Mm2**2*(3*(-(s2nm*s3mm) + s1mm*(&
               &s2nm + s3nm))*ss - 8*(s2nm*s3mm + s1mm*(s2nm - s3nm))*tt) + 4*Mm2*(3*(s1mm - s3m&
               &m)*s3nm*ss**2 - 3*(s2nm*s3mm + s1mm*(s2nm - s3nm))*ss*tt - 2*(s2nm*s3mm + s1mm*(&
               &s2nm - s3nm))*tt**2) + ss*((-s1mm + s3mm)*s3nm*ss**2 + (-4*s3mm*s3nm + s1mm*(s2n&
               &m + 4*s3nm))*ss*tt + 2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm))*tt**2)))/(me2*(4*me2 &
               &- ss)*(-4*Mm2 + ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt&
               &))))
    coeff_unity = -((4*me2**3*(s2nm*s3mm - s1mm*s3nm) + me2**2*(8*Mm2*s2nm*s3mm - 8*Mm2*s1mm*(s2nm&
                  & + s3nm) + 12*s1mm*s2nm*ss + 6*s2nm*s3mm*ss + 26*s1mm*s3nm*ss - 32*s3mm*s3nm*ss &
                  &- 6*s2nm*s3mm*tt + 6*s1mm*s3nm*tt) + ss*(Mm2**2*(21*s1mm*s2nm + s2nm*s3mm - s1mm&
                  &*s3nm) - Mm2*(s3mm*s3nm*ss - 2*s1mm*s3nm*(ss + tt) + s2nm*s3mm*(ss + 2*tt) + 6*s&
                  &1mm*s2nm*(3*ss + 4*tt)) + (ss + tt)*(-(s1mm*s3nm*(ss + tt)) + s1mm*s2nm*(4*ss + &
                  &7*tt) + s3mm*(s3nm*ss + s2nm*tt))) - me2*(4*Mm2**2*(-(s2nm*s3mm) + s1mm*(2*s2nm &
                  &+ s3nm)) + 2*s1mm*s2nm*ss*(7*ss + 10*tt) - s3mm*s3nm*ss*(15*ss + 16*tt) + s2nm*s&
                  &3mm*(5*ss**2 + 4*ss*tt - 2*tt**2) + 2*s1mm*s3nm*(5*ss**2 + 6*ss*tt + tt**2) - Mm&
                  &2*(s3mm*(5*s2nm*ss - 32*s3nm*ss - 6*s2nm*tt) + s1mm*(35*s2nm*ss + 27*s3nm*ss + 4&
                  &*s2nm*tt + 6*s3nm*tt))))/(me2*ss**2*(-2*me2 - 2*Mm2 + ss + tt)))
    coeffLR = coeffLR + c6se*coeff_c6se + c6sm*coeff_c6sm + c6t*coeff_c6t  &
            + c6u*coeff_c6u + ce*coeff_ce + cm*coeff_cm  &
            + coeff_discse*discse + coeff_discse_lmm*discse*lmm + coeff_discse_lmu*discse*lmu  &
            + coeff_discsm*discsm + coeff_discsm_lmu*discsm*lmu + coeff_disct*disct  &
            + coeff_disct_lmm*disct*lmm + coeff_disct_lmu*disct*lmu + coeff_disct_ls*disctLs  &
            + coeff_discu*discu + coeff_discu_lmm*discu*lmm + coeff_discu_lmu*discu*lmu  &
            + coeff_discu_ls*discuLs + coeff_lmm*lmm + coeff_lmu*lmu  &
            + coeff_ls*ls + coeff_unity
  endif

  END FUNCTION COEFFLR


  PURE FUNCTION COEFFRR(Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm, &
        discu, disct, discse, discsm, c6se, c6sm, c6u, c6t, ce, cm, lmm, lmu, ls, &
        disctLs, discuLs, pol)
  real(kind=prec), intent(in) :: Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm
  real(kind=prec), intent(in) :: discu, disct, discse, discsm, c6se, c6sm, c6u, c6t, ce, cm, lmm, lmu, ls
  real(kind=prec), intent(in) :: disctLs, discuLs
  real(kind=prec) :: coeff_c6se, coeff_c6sm, coeff_c6t, coeff_c6u, coeff_ce
  real(kind=prec) :: coeff_cm, coeff_discse, coeff_discse_lmm, coeff_discse_lmu, coeff_discsm
  real(kind=prec) :: coeff_discsm_lmu, coeff_disct, coeff_disct_lmm, coeff_disct_lmu, coeff_disct_ls
  real(kind=prec) :: coeff_discu, coeff_discu_lmm, coeff_discu_lmu, coeff_discu_ls, coeff_lmm
  real(kind=prec) :: coeff_lmu, coeff_ls, coeff_unity
  logical, intent(in) :: pol
  real(kind=prec) :: coeffRR

  coeffRR = 0.
  coeff_c6se = ((2*me2 - ss)*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2 - Mm2*(s&
               &s + 2*tt)))/ss
  coeff_c6sm = ((2*Mm2 - ss)*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2 - Mm2*(s&
               &s + 2*tt)))/ss
  coeff_c6t = (2*me2**3 + (Mm2 - tt)*(2*Mm2**2 - 3*Mm2*ss + 3*ss**2 - 4*Mm2*tt + 6*ss*tt + 2*t&
              &t**2) + me2**2*(6*Mm2 - 3*(ss + 2*tt)) + me2*(6*Mm2**2 - 2*Mm2*(5*ss + 6*tt) + 3&
              &*(ss**2 + 3*ss*tt + 2*tt**2)))/(2.*ss)
  coeff_c6u = (2*me2**3 + 2*Mm2**3 - 2*(ss + tt)**3 - 3*Mm2**2*(ss + 2*tt) + 3*Mm2*(ss**2 + 3*&
              &ss*tt + 2*tt**2) + me2**2*(6*Mm2 - 3*(ss + 2*tt)) + me2*(6*Mm2**2 - 2*Mm2*(5*ss &
              &+ 6*tt) + 3*(ss**2 + 3*ss*tt + 2*tt**2)))/(2.*ss)
  coeff_ce = (-16*me2**3 - 16*me2**2*(Mm2 - 2*ss - tt) + ss**2*(-2*Mm2 + ss + 2*tt) - 4*me2*s&
             &s*(-4*Mm2 + 3*ss + 4*tt))/(16*me2 - 4*ss)
  coeff_cm = (-16*Mm2**3 - 2*me2*(8*Mm2**2 - 8*Mm2*ss + ss**2) + 16*Mm2**2*(2*ss + tt) + ss**&
             &2*(ss + 2*tt) - 4*Mm2*ss*(3*ss + 4*tt))/(16*Mm2 - 4*ss)
  coeff_discse = -(1./6.)*(16*me2**4 + 4*me2**3*(8*Mm2 + 7*ss - 8*tt) + ss**2*(-11*Mm&
                 &2**2 - 9*Mm2*ss + 2*ss**2 + 22*Mm2*tt - 2*ss*tt - 11*tt**2) + me2**2*(16*Mm2**2 &
                 &+ 9*ss**2 + 8*Mm2*(7*ss - 4*tt) - 40*ss*tt + 16*tt**2) + me2*ss*(28*Mm2**2 - ss*&
                 &*2 + 2*Mm2*(ss - 28*tt) + 38*ss*tt + 28*tt**2))/((4*me2 - ss)*ss**2)
  coeff_discse_lmm = ((2*me2 - ss)*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2 - Mm2*(s&
                     &s + 2*tt)))/((4*me2 - ss)*ss)
  coeff_discse_lmu = -(((2*me2 - ss)*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2 - Mm2*&
                     &(ss + 2*tt)))/((4*me2 - ss)*ss))
  coeff_discsm = -(1./6.)*(16*Mm2**4 + me2**2*(16*Mm2**2 + 28*Mm2*ss - 11*ss**2) + 4*&
                 &Mm2**3*(7*ss - 8*tt) + ss**2*(2*ss**2 - 2*ss*tt - 11*tt**2) + Mm2**2*(9*ss**2 - &
                 &40*ss*tt + 16*tt**2) + Mm2*ss*(-ss**2 + 38*ss*tt + 28*tt**2) + me2*(32*Mm2**3 + &
                 &2*Mm2*ss*(ss - 28*tt) + 8*Mm2**2*(7*ss - 4*tt) + ss**2*(-9*ss + 22*tt)))/((4*Mm2&
                 & - ss)*ss**2)
  coeff_discsm_lmu = -(((2*Mm2 - ss)*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2 - Mm2*&
                     &(ss + 2*tt)))/((4*Mm2 - ss)*ss))
  coeff_disct = -(1./2.)*(me2**5 + me2**4*(-3*Mm2 + ss - 3*tt) + me2**3*(2*Mm2**2 - 4*Mm2*ss + 4*Mm2&
                &*tt - 3*ss*tt) + (Mm2 - tt)*(Mm2**4 + Mm2**3*(ss - 2*tt) - 2*Mm2**2*tt*(ss + tt)&
                & - Mm2*tt*(ss**2 + 6*ss*tt - 6*tt**2) + tt**2*(4*ss**2 + 5*ss*tt - 3*tt**2)) + m&
                &e2*(-3*Mm2**4 - 4*Mm2**3*(ss - tt) + 3*Mm2**2*ss*tt + tt**2*(5*ss**2 + 11*ss*tt &
                &- 9*tt**2) + 2*Mm2*tt*(ss**2 - 16*ss*tt + 4*tt**2)) + me2**2*(2*Mm2**3 + Mm2**2*&
                &(6*ss - 2*tt) + 3*Mm2*ss*tt + tt*(-ss**2 - 4*ss*tt + 8*tt**2)))/(ss*(-4*me2*Mm2 &
                &+ (me2 + Mm2 - tt)**2)*tt)
  coeff_disct_lmm = (tt*(-6*me2**3 + me2**2*(-18*Mm2 + 5*ss + 18*tt) - (Mm2 - tt)*(6*Mm2**2 - 5*Mm2*&
                    &ss + 5*ss**2 - 12*Mm2*tt + 10*ss*tt + 6*tt**2) - me2*(18*Mm2**2 + 5*ss**2 + 15*s&
                    &s*tt + 18*tt**2 - 6*Mm2*(ss + 6*tt))))/(4.*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2)&
                    &)
  coeff_disct_lmu = (2*(me2 + Mm2 - tt)*tt*(me2**2 + Mm2**2 + me2*(2*Mm2 - ss - 2*tt) + (ss + tt)**2&
                    & - Mm2*(ss + 2*tt)))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
  coeff_disct_ls = (tt*(2*me2**3 + me2**2*(6*Mm2 - ss - 6*tt) + me2*(6*Mm2**2 + ss**2 + 2*Mm2*(ss -&
                   & 6*tt) + 3*ss*tt + 6*tt**2) + (Mm2 - tt)*(2*Mm2**2 + ss**2 + 2*ss*tt + 2*tt**2 -&
                   & Mm2*(ss + 4*tt))))/(2.*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
  coeff_discu = (2*me2**4 + 2*Mm2**4 + me2**3*(19*ss - 6*tt) + Mm2**3*(19*ss - 6*tt) - 4*ss*(ss &
                &+ tt)**2*(2*ss + 3*tt) - me2**2*(4*Mm2**2 - 93*Mm2*ss + 46*ss**2 + 10*Mm2*tt + 4&
                &8*ss*tt - 6*tt**2) + Mm2**2*(-46*ss**2 - 48*ss*tt + 6*tt**2) + Mm2*(33*ss**3 + 7&
                &6*ss**2*tt + 41*ss*tt**2 - 2*tt**3) + me2*(33*ss**3 + Mm2**2*(93*ss - 10*tt) + 7&
                &6*ss**2*tt + 41*ss*tt**2 - 2*tt**3 - 4*Mm2*(25*ss**2 + 26*ss*tt - 3*tt**2)))/(2.&
                &*ss*(me2**2 + (-Mm2 + ss + tt)**2 - 2*me2*(Mm2 + ss + tt)))
  coeff_discu_lmm = -(1./4.)*((-2*me2 - 2*Mm2 + ss + tt)*(-6*me2**3 - 6*Mm2**3 + 6*(ss + tt)**3 + Mm2**&
                    &2*(13*ss + 18*tt) + me2**2*(-18*Mm2 + 13*ss + 18*tt) - Mm2*(13*ss**2 + 31*ss*tt &
                    &+ 18*tt**2) - me2*(18*Mm2**2 - 22*Mm2*ss + 13*ss**2 - 36*Mm2*tt + 31*ss*tt + 18*&
                    &tt**2)))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2))
  coeff_discu_lmu = (2*(-2*me2 - 2*Mm2 + ss + tt)*(-me2 - Mm2 + ss + tt)*(me2**2 + Mm2**2 + me2*(2*M&
                    &m2 - ss - 2*tt) + (ss + tt)**2 - Mm2*(ss + 2*tt)))/(ss*(-4*me2*Mm2 + (me2 + Mm2 &
                    &- ss - tt)**2))
  coeff_discu_ls = ((-2*me2 - 2*Mm2 + ss + tt)*(-2*me2**3 - 2*Mm2**3 + 2*(ss + tt)**3 + Mm2**2*(5*s&
                   &s + 6*tt) + me2**2*(-6*Mm2 + 5*ss + 6*tt) - Mm2*(5*ss**2 + 11*ss*tt + 6*tt**2) -&
                   & me2*(6*Mm2**2 + 5*ss**2 + 11*ss*tt + 6*tt**2 - 6*Mm2*(ss + 2*tt))))/(2.*ss*(-4*&
                   &me2*Mm2 + (me2 + Mm2 - ss - tt)**2))
  coeff_lmm = -(1./12.)*(-24*Mm2**6 + 6*me2**5*(4*Mm2 - ss) - 6*Mm2**5*(ss - 10*tt)&
              & - 3*me2**4*(4*Mm2 - ss)*(2*Mm2 - ss + 5*tt) - me2**3*(4*Mm2 - ss)*(12*Mm2**2 + &
              &3*ss**2 + 6*Mm2*(ss - tt) + 9*ss*tt - 32*tt**2) + Mm2**4*(15*ss**2 + 21*ss*tt - &
              &16*tt**2) + ss*tt**2*(2*ss**3 + 9*ss**2*tt + 14*ss*tt**2 + 7*tt**3) - Mm2**3*(3*&
              &ss**3 + 9*ss**2*tt + 96*ss*tt**2 + 80*tt**3) + Mm2**2*tt*(-12*ss**3 + 53*ss**2*t&
              &t + 168*ss*tt**2 + 88*tt**3) - Mm2*tt*(-3*ss**4 + 18*ss**3*tt + 91*ss**2*tt**2 +&
              & 102*ss*tt**3 + 28*tt**4) + me2*(24*Mm2**5 + 6*Mm2**4*(3*ss - 4*tt) - 6*Mm2**3*(&
              &7*ss**2 - 7*ss*tt - 24*tt**2) + Mm2**2*(9*ss**3 - 9*ss**2*tt - 188*ss*tt**2 - 28&
              &0*tt**3) - ss*tt*(3*ss**3 + 10*ss**2*tt + 37*ss*tt**2 + 34*tt**3) + 2*Mm2*tt*(6*&
              &ss**3 + 51*ss**2*tt + 145*ss*tt**2 + 68*tt**3)) + me2**2*(48*Mm2**4 - 12*Mm2**3*&
              &ss + ss*tt**2*(ss + 50*tt) + 12*Mm2**2*(3*ss**2 - 3*ss*tt + 16*tt**2) - Mm2*(9*s&
              &s**3 - 9*ss**2*tt + 100*ss*tt**2 + 200*tt**3)))/((4*Mm2 - ss)*ss*tt**2*(-2*me2 -&
              & 2*Mm2 + ss + tt))
  coeff_lmu = -(1./6.)*(7*me2**2 + 14*me2*Mm2 + 7*Mm2**2 - 3*me2*ss - 3*Mm2*ss + 5&
              &*ss**2 - 14*me2*tt - 14*Mm2*tt + 10*ss*tt + 7*tt**2)/ss
  coeff_ls = -(1./2.)*(16*me2**2*Mm2 + ss**2*(-3*Mm2 + ss + tt) + me2*(16*Mm2**2 - 3*ss**2 - 16*M&
             &m2*tt))/((4*me2 - ss)*(-4*Mm2 + ss))
  coeff_unity = (-3*me2**3*(3*ss + 8*tt) - 3*Mm2**3*(3*ss + 8*tt) + Mm2*tt*(24*ss**2 - 143*ss*tt&
                & - 24*tt**2) + Mm2**2*(-9*ss**2 + 73*ss*tt + 48*tt**2) + me2**2*(-9*ss**2 + 9*Mm&
                &2*(ss - 8*tt) + 73*ss*tt + 48*tt**2) + ss*tt*(-25*ss**2 + ss*tt + 55*tt**2) + me&
                &2*(9*Mm2**2*(ss - 8*tt) + tt*(24*ss**2 - 143*ss*tt - 24*tt**2) + 2*Mm2*(9*ss**2 &
                &+ 91*ss*tt + 48*tt**2)))/(18.*ss**2*tt)
  coeffRR = 2*(c6se*coeff_c6se + c6sm*coeff_c6sm + c6t*coeff_c6t  &
          + c6u*coeff_c6u + ce*coeff_ce + cm*coeff_cm  &
          + coeff_discse*discse + coeff_discse_lmm*discse*lmm + coeff_discse_lmu*discse*lmu  &
          + coeff_discsm*discsm + coeff_discsm_lmu*discsm*lmu + coeff_disct*disct  &
          + coeff_disct_lmm*disct*lmm + coeff_disct_lmu*disct*lmu + coeff_disct_ls*disctLs  &
          + coeff_discu*discu + coeff_discu_lmm*discu*lmm + coeff_discu_lmu*discu*lmu  &
          + coeff_discu_ls*discuLs + coeff_lmm*lmm + coeff_lmu*lmu  &
          + coeff_ls*ls + coeff_unity)

  if(pol) then
    coeff_c6se = -(((2*me2 - ss)*(me2**2*snm + Mm2**2*snm + s1mm*ss + 2*s2nm*ss - 3*s3mm*ss - 3*s&
                 &3nm*ss + s1mm*tt + s2nm*tt - 2*s3mm*tt - 2*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2&
                 &*(-3*s1mm + s2nm + 2*(s3mm + s3nm - snm*tt)) - me2*(s1mm + s2nm - 2*(s3mm + s3nm&
                 & + Mm2*snm - snm*tt))))/ss)
    coeff_c6sm = -(((2*Mm2 - ss)*(me2**2*snm + Mm2**2*snm + s1mm*ss + 2*s2nm*ss - 3*s3mm*ss - 3*s&
                 &3nm*ss + s1mm*tt + s2nm*tt - 2*s3mm*tt - 2*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2&
                 &*(-3*s1mm + s2nm + 2*(s3mm + s3nm - snm*tt)) - me2*(s1mm + s2nm - 2*(s3mm + s3nm&
                 & + Mm2*snm - snm*tt))))/ss)
    coeff_c6t = (-2*me2**5*snm - 2*Mm2**5*snm + Mm2**4*(6*s1mm - 2*s2nm - 4*s3mm - 4*s3nm - snm*&
                &ss + 10*snm*tt) + me2**4*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm - 2*Mm2*snm - snm*ss&
                & + 10*snm*tt) + Mm2**3*(snm*ss**2 + 4*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm - 5*s&
                &nm*tt) + ss*(-8*s2nm + 8*s3mm + 8*s3nm - snm*tt)) + tt**2*(2*tt**2*(s1mm + s2nm &
                &- 2*s3mm - 2*s3nm + snm*tt) + ss**2*(3*s1mm + 6*s2nm - 9*s3mm - 9*s3nm + 2*snm*t&
                &t) + ss*tt*(5*s1mm + 9*s2nm - 14*s3mm - 14*s3nm + 4*snm*tt)) + Mm2*tt*((s1mm - 6&
                &*s2nm + 5*(s3mm + s3nm))*ss**2 + ss*tt*(-14*s1mm - 22*s2nm + 36*s3mm + 36*s3nm -&
                & 11*snm*tt) - 2*tt**2*(6*s1mm + 2*s2nm - 8*s3mm - 8*s3nm + 5*snm*tt)) + Mm2**2*(&
                &ss**2*(-2*s1mm + 2*s3mm + 2*s3nm - 3*snm*tt) + 3*ss*tt*(3*s1mm + 7*s2nm - 10*s3m&
                &m - 10*s3nm + 3*snm*tt) + 4*tt**2*(6*s1mm - 6*s3mm - 6*s3nm + 5*snm*tt)) + me2**&
                &3*(4*Mm2**2*snm + snm*ss**2 - ss*(2*s1mm + 6*s2nm - 8*s3mm - 8*s3nm + snm*tt) + &
                &4*Mm2*(s1mm - s2nm + 4*snm*tt) - 4*tt*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 5*snm&
                &*tt)) + me2**2*(4*Mm2**3*snm + 2*Mm2**2*(-4*s1mm + 4*s3mm + 4*s3nm + snm*ss + 6*&
                &snm*tt) + Mm2*(-(snm*ss**2) + 4*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm - 9*snm*tt)&
                & + ss*(4*s1mm + 4*s2nm - 8*s3mm - 8*s3nm + snm*tt)) + tt*(-3*snm*ss**2 + 3*ss*(3&
                &*s1mm + 7*s2nm - 10*s3mm - 10*s3nm + 3*snm*tt) + 4*tt*(3*s1mm + 3*s2nm - 6*s3mm &
                &- 6*s3nm + 5*snm*tt))) - me2*(2*Mm2**4*snm + 4*Mm2**3*(s1mm - s2nm - 4*snm*tt) +&
                & Mm2**2*(snm*ss**2 + ss*(2*s1mm - 10*s2nm + 8*s3mm + 8*s3nm - snm*tt) + 4*tt*(4*&
                &s1mm - 4*s3mm - 4*s3nm + 9*snm*tt)) + tt*(3*(s1mm + 2*s2nm - 3*(s3mm + s3nm))*ss&
                &**2 + 2*tt**2*(4*s1mm + 4*s2nm - 8*s3mm - 8*s3nm + 5*snm*tt) + ss*tt*(12*s1mm + &
                &24*s2nm - 36*s3mm - 36*s3nm + 11*snm*tt)) - 2*Mm2*(ss**2*(s1mm - s3mm - s3nm - s&
                &nm*tt) + 2*tt**2*(7*s1mm + s2nm - 8*(s3mm + s3nm - snm*tt)) + ss*tt*(3*s1mm + 7*&
                &s2nm + 5*(-2*s3mm - 2*s3nm + snm*tt)))))/(2.*ss*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*&
                &me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_c6u = -(1./2.)*(2*me2**5*snm + 2*Mm2**5*snm + Mm2**4*(-6*s1mm + 2*s2nm + 4*s3mm + 4*s3nm +&
                & snm*ss - 10*snm*tt) + me2**4*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 2*Mm2*snm + &
                &snm*ss - 10*snm*tt) + (ss + tt)**2*(snm*ss**3 - (s1mm + 5*s2nm - 6*(s3mm + s3nm)&
                &)*ss*tt - 2*tt**2*(s1mm + s2nm - 2*s3mm - 2*s3nm + snm*tt) + ss**2*(s1mm - s3mm &
                &- s3nm + 3*snm*tt)) + Mm2**3*(-7*snm*ss**2 + ss*(6*s1mm + 2*s2nm - 8*s3mm - 8*s3&
                &nm + snm*tt) + 4*tt*(5*s1mm - s2nm - 4*s3mm - 4*s3nm + 5*snm*tt)) + me2**3*(-4*M&
                &m2**2*snm - 7*snm*ss**2 + ss*(2*s1mm + 6*s2nm - 8*s3mm - 8*s3nm + snm*tt) - 4*Mm&
                &2*(s1mm - s2nm + 4*snm*tt) + 4*tt*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 5*snm*tt)&
                &) - Mm2*(ss + tt)*(4*snm*ss**3 - ss*tt*(8*s1mm + 12*s2nm - 20*s3mm - 20*s3nm + s&
                &nm*tt) - 2*tt**2*(6*s1mm + 2*s2nm - 8*s3mm - 8*s3nm + 5*snm*tt) + ss**2*(4*s1mm &
                &- s2nm - 3*s3mm - 3*s3nm + 13*snm*tt)) + Mm2**2*(7*snm*ss**3 - 3*ss*tt*(7*s1mm +&
                & 3*s2nm - 10*s3mm - 10*s3nm + 3*snm*tt) - 4*tt**2*(6*s1mm - 6*s3mm - 6*s3nm + 5*&
                &snm*tt) + ss**2*(3*s1mm - 7*s2nm + 4*s3mm + 4*s3nm + 18*snm*tt)) - me2**2*(4*Mm2&
                &**3*snm - 7*snm*ss**3 + 3*ss*tt*(3*s1mm + 7*s2nm - 10*s3mm - 10*s3nm + 3*snm*tt)&
                & + 4*tt**2*(3*s1mm + 3*s2nm - 6*s3mm - 6*s3nm + 5*snm*tt) - 3*ss**2*(s1mm - s2nm&
                & + 6*snm*tt) + 2*Mm2**2*(-4*s1mm + 4*s3mm + 4*s3nm + snm*ss + 6*snm*tt) + Mm2*(s&
                &nm*ss**2 + 4*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm - 9*snm*tt) + ss*(-2*s1mm + 10&
                &*s2nm - 8*s3mm - 8*s3nm + snm*tt))) + me2*(2*Mm2**4*snm + 4*Mm2**3*(s1mm - s2nm &
                &- 4*snm*tt) - Mm2**2*(snm*ss**2 + ss*(10*s1mm - 2*s2nm - 8*s3mm - 8*s3nm + snm*t&
                &t) - 4*tt*(4*s1mm - 4*s3mm - 4*s3nm + 9*snm*tt)) - (ss + tt)*(4*snm*ss**3 - ss*t&
                &t*(4*s1mm + 16*s2nm - 20*s3mm - 20*s3nm + snm*tt) - 2*tt**2*(4*s1mm + 4*s2nm - 8&
                &*s3mm - 8*s3nm + 5*snm*tt) + ss**2*(4*s1mm + s2nm - 5*s3mm - 5*s3nm + 13*snm*tt)&
                &) + 2*Mm2*(3*snm*ss**3 + ss**2*(5*s1mm + 5*s2nm - 2*(5*s3mm + 5*s3nm - 7*snm*tt)&
                &) - 2*tt**2*(7*s1mm + s2nm - 8*(s3mm + s3nm - snm*tt)) - ss*tt*(9*s1mm + s2nm + &
                &5*(-2*s3mm - 2*s3nm + snm*tt)))))/(ss*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 +&
                & tt) + tt*(ss + tt)))
    coeff_ce = (16*me2**5*snm - 8*me2**4*(s1mm + 3*s2nm - 4*s3mm - 4*s3nm + 2*Mm2*snm + 6*snm*s&
               &s + 6*snm*tt) - 4*me2**3*(4*Mm2**2*snm - 12*snm*ss**2 + ss*(-7*s1mm - 7*s2nm + 1&
               &4*s3mm + 14*s3nm - 32*snm*tt) - 2*Mm2*(3*s1mm + 5*s2nm - 8*s3mm - 8*s3nm + 2*snm&
               &*ss - 4*snm*tt) - 2*tt*(s1mm + 7*s2nm - 8*s3mm - 8*s3nm + 6*snm*tt)) + me2**2*(1&
               &6*Mm2**3*snm - 29*snm*ss**3 + 8*tt**2*(s1mm - 5*s2nm + 4*s3mm + 4*s3nm - 2*snm*t&
               &t) - 8*Mm2**2*(3*s1mm + s2nm - 4*s3mm - 4*s3nm + 2*snm*ss + 6*snm*tt) - 2*ss**2*&
               &(14*s1mm + 4*s2nm - 18*s3mm - 18*s3nm + 53*snm*tt) - 8*ss*tt*(3*s1mm + 7*s2nm - &
               &2*(5*s3mm + 5*s3nm - 6*snm*tt)) + 2*Mm2*(13*snm*ss**2 - 8*ss*(3*s2nm - 3*s3mm - &
               &3*s3nm - 7*snm*tt) + 8*tt*(s1mm + 3*s2nm - 4*s3mm - 4*s3nm + 3*snm*tt))) - ss**2&
               &*(-2*Mm2**3*snm + Mm2**2*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 3*snm*ss + 6*snm*t&
               &t) + (ss + 2*tt)*(snm*ss**2 + tt*(s1mm + s2nm - 2*s3mm - 2*s3nm + snm*tt) + ss*(&
               &s1mm - s3mm - s3nm + 2*snm*tt)) - Mm2*(3*snm*ss**2 + 2*tt*(2*s1mm + 2*s2nm - 4*s&
               &3mm - 4*s3nm + 3*snm*tt) + ss*(s1mm - s2nm + 8*snm*tt))) + me2*(9*snm*ss**4 + 8*&
               &Mm2**3*(s1mm - s2nm - 2*snm*ss) + 8*(-s1mm + s2nm)*tt**3 - 4*ss*tt**2*(s1mm - 7*&
               &s2nm + 6*s3mm + 6*s3nm - 4*snm*tt) + 2*ss**2*tt*(7*s1mm + 9*s2nm - 16*s3mm - 16*&
               &s3nm + 22*snm*tt) + ss**3*(9*s1mm + s2nm - 10*s3mm - 10*s3nm + 36*snm*tt) + 4*Mm&
               &2**2*(5*snm*ss**2 + 6*(-s1mm + s2nm)*tt + ss*(s1mm + 5*s2nm - 6*(s3mm + s3nm - 2&
               &*snm*tt))) - 2*Mm2*(10*snm*ss**3 + 12*(-s1mm + s2nm)*tt**2 + 24*ss*tt*(s2nm - s3&
               &mm - s3nm + snm*tt) + ss**2*(s1mm - 5*s2nm + 4*(s3mm + s3nm + 8*snm*tt)))))/(4.*&
               &(4*me2 - ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_cm = (16*Mm2**5*snm + 2*me2**3*snm*(8*Mm2**2 - 8*Mm2*ss + ss**2) - 16*Mm2**4*(s1mm + &
               &s2nm - 2*s3mm - 2*s3nm + 3*snm*ss + 3*snm*tt) - ss**2*(ss + 2*tt)*(snm*ss**2 + t&
               &t*(s1mm + s2nm - 2*s3mm - 2*s3nm + snm*tt) + ss*(s1mm - s3mm - s3nm + 2*snm*tt))&
               & - me2**2*(16*Mm2**3*snm + 16*Mm2**2*(s1mm + s2nm - 2*s3mm - 2*s3nm + snm*ss + 3&
               &*snm*tt) + ss**2*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 3*snm*ss + 6*snm*tt) - 4*M&
               &m2*ss*(4*s1mm + 4*s2nm - 8*s3mm - 8*s3nm + 5*snm*ss + 12*snm*tt)) + 8*Mm2**3*(6*&
               &snm*ss**2 + 2*tt*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 3*snm*tt) + ss*(7*s1mm - s&
               &2nm - 6*s3mm - 6*s3nm + 16*snm*tt)) - Mm2**2*(29*snm*ss**3 + 16*tt**2*(s1mm + s2&
               &nm - 2*s3mm - 2*s3nm + snm*tt) + 2*ss**2*(20*s1mm - 4*s2nm - 16*s3mm - 16*s3nm +&
               & 53*snm*tt) + 8*ss*tt*(9*s1mm + 5*s2nm - 2*(7*s3mm + 7*s3nm - 6*snm*tt))) + Mm2*&
               &ss*(9*snm*ss**3 + 16*tt**2*(s1mm + s2nm - 2*s3mm - 2*s3nm + snm*tt) + ss**2*(11*&
               &s1mm - s2nm - 10*s3mm - 10*s3nm + 36*snm*tt) + 2*ss*tt*(15*s1mm + 7*s2nm - 22*(s&
               &3mm + s3nm - snm*tt))) + me2*(-16*Mm2**4*snm + 16*Mm2**3*(2*s1mm + 2*s2nm - 4*s3&
               &mm - 4*s3nm + snm*ss - 2*snm*tt) - 2*Mm2*ss*(10*snm*ss**2 + 8*tt*(2*s1mm + 2*s2n&
               &m - 4*s3mm - 4*s3nm + 3*snm*tt) + ss*(11*s1mm + 3*s2nm - 14*s3mm - 14*s3nm + 32*&
               &snm*tt)) + 2*Mm2**2*(13*snm*ss**2 + 8*tt*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 3*&
               &snm*tt) + 4*ss*(3*s1mm - s2nm - 2*(s3mm + s3nm - 7*snm*tt))) + ss**2*(3*snm*ss**&
               &2 + 2*tt*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 3*snm*tt) + ss*(3*s1mm + s2nm - 4*&
               &(s3mm + s3nm - 2*snm*tt)))))/(4.*(4*Mm2 - ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me&
               &2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_discse = (32*me2**4*snm - 8*me2**3*(4*s1mm + 4*s2nm - 8*s3mm - 8*s3nm - 8*Mm2*snm - 7*snm&
                   &*ss + 8*snm*tt) + 2*me2**2*(16*Mm2**2*snm - 11*snm*ss**2 + ss*(-19*s1mm - 9*s2nm&
                   & + 28*s3mm + 28*s3nm - 40*snm*tt) + 16*tt*(s1mm + s2nm - 2*s3mm - 2*s3nm + snm*t&
                   &t) - 8*Mm2*(6*s1mm - 2*s2nm - 4*s3mm - 4*s3nm - 7*snm*ss + 4*snm*tt)) + ss*(6*sn&
                   &m*ss**3 + Mm2**2*(6*s1mm - 6*s2nm - 22*snm*ss) + 12*Mm2*(-s1mm + s2nm)*tt + 6*(s&
                   &1mm - s2nm)*tt**2 + ss**2*(15*s1mm + 8*s2nm - 23*s3mm - 23*s3nm - 22*snm*tt) + s&
                   &s*tt*(-7*s1mm - 31*s2nm + 38*s3mm + 38*s3nm - 22*snm*tt) + Mm2*ss*(51*s1mm - 13*&
                   &s2nm - 38*s3mm - 38*s3nm + 44*snm*tt)) + me2*ss*(56*Mm2**2*snm - 12*snm*ss**2 + &
                   &8*tt*(4*s1mm + 7*s2nm - 11*s3mm - 11*s3nm + 7*snm*tt) - 4*Mm2*(36*s1mm - 14*s2nm&
                   & - 22*s3mm - 22*s3nm + 5*snm*ss + 28*snm*tt) + ss*(13*s1mm + 45*s2nm - 58*s3mm -&
                   & 58*s3nm + 100*snm*tt)))/(12.*(4*me2 - ss)*ss**2)
    coeff_discse_lmm = -(((2*me2 - ss)*(me2**2*snm + Mm2**2*snm + s1mm*ss + 2*s2nm*ss - 3*s3mm*ss - 3*s&
                       &3nm*ss + s1mm*tt + s2nm*tt - 2*s3mm*tt - 2*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2&
                       &*(-3*s1mm + s2nm + 2*(s3mm + s3nm - snm*tt)) - me2*(s1mm + s2nm - 2*(s3mm + s3nm&
                       & + Mm2*snm - snm*tt))))/((4*me2 - ss)*ss))
    coeff_discse_lmu = ((2*me2 - ss)*(me2**2*snm + Mm2**2*snm + s1mm*ss + 2*s2nm*ss - 3*s3mm*ss - 3*s3n&
                       &m*ss + s1mm*tt + s2nm*tt - 2*s3mm*tt - 2*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2*(&
                       &-3*s1mm + s2nm + 2*(s3mm + s3nm - snm*tt)) - me2*(s1mm + s2nm - 2*(s3mm + s3nm +&
                       & Mm2*snm - snm*tt))))/((4*me2 - ss)*ss)
    coeff_discsm = (16*Mm2**4*snm + me2**2*snm*(16*Mm2**2 + 28*Mm2*ss - 11*ss**2) + 4*Mm2**3*snm*(7&
                   &*ss - 8*tt) + ss**2*(3*snm*ss**2 + ss*(s1mm + 4*s2nm - 5*s3mm - 5*s3nm - 11*snm*&
                   &tt) + tt*(-9*s1mm - 9*s2nm + 18*s3mm + 18*s3nm - 11*snm*tt)) + Mm2**2*(-11*snm*s&
                   &s**2 + 16*snm*tt**2 - 8*ss*(7*s1mm - 5*s2nm - 2*s3mm - 2*s3nm + 5*snm*tt)) + me2&
                   &*(32*Mm2**3*snm + 8*Mm2**2*snm*(7*ss - 4*tt) + ss**2*(9*s1mm + 9*s2nm - 18*s3mm &
                   &- 18*s3nm + 22*snm*tt) - 2*Mm2*ss*(12*s1mm + 12*s2nm - 24*s3mm - 24*s3nm + 5*snm&
                   &*ss + 28*snm*tt)) + Mm2*ss*(-6*snm*ss**2 + 4*tt*(6*s1mm + 6*s2nm - 12*s3mm - 12*&
                   &s3nm + 7*snm*tt) + ss*(19*s1mm + 7*s2nm - 26*s3mm - 26*s3nm + 50*snm*tt)))/(6.*(&
                   &4*Mm2 - ss)*ss**2)
    coeff_discsm_lmu = ((2*Mm2 - ss)*(me2**2*snm + Mm2**2*snm + s1mm*ss + 2*s2nm*ss - 3*s3mm*ss - 3*s3n&
                       &m*ss + s1mm*tt + s2nm*tt - 2*s3mm*tt - 2*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2*(&
                       &-3*s1mm + s2nm + 2*(s3mm + s3nm - snm*tt)) - me2*(s1mm + s2nm - 2*(s3mm + s3nm +&
                       & Mm2*snm - snm*tt))))/((4*Mm2 - ss)*ss)
    coeff_disct = (2*me2**5*snm + 2*Mm2**5*snm - 2*Mm2**3*(s1mm - s3mm - s3nm)*(ss - 4*tt) + Mm2**&
                  &4*(-6*s1mm + 2*s2nm + 4*s3mm + 4*s3nm - 6*snm*tt) + 2*me2**4*(s1mm + s2nm - 2*s3&
                  &mm - 2*s3nm - 3*Mm2*snm - 3*snm*tt) + 2*me2**3*(2*Mm2**2*snm + (s1mm - s3mm - s3&
                  &nm)*ss - 4*(s1mm + s2nm - 2*(s3mm + s3nm))*tt + 4*Mm2*(-s2nm + s3mm + s3nm + snm&
                  &*tt)) - Mm2*tt*(ss**2*(-3*s1mm + 3*s3mm + 3*s3nm - 4*snm*tt) + 6*tt**2*(4*s1mm -&
                  & 4*s3mm - 4*s3nm + 3*snm*tt) + 2*ss*tt*(s1mm - 10*s2nm + 9*s3mm + 9*s3nm + 4*snm&
                  &*tt)) + tt**2*(ss**2*(3*s1mm - 3*s3mm - 3*s3nm - 4*snm*tt) + 6*tt**2*(s1mm + s2n&
                  &m - 2*s3mm - 2*s3nm + snm*tt) + ss*tt*(-7*s1mm - 19*s2nm + 26*s3mm + 26*s3nm + 6&
                  &*snm*tt)) + Mm2**2*tt*(8*tt*(2*s1mm - s2nm - s3mm - s3nm + 2*snm*tt) + ss*(11*s1&
                  &mm - s2nm + 2*(-5*s3mm - 5*s3nm + snm*tt))) + me2**2*(4*Mm2**3*snm - 6*Mm2*(s1mm&
                  & - s3mm - s3nm)*ss - 8*Mm2*(s1mm - 2*s2nm + s3mm + s3nm)*tt - 4*Mm2**2*(3*s1mm -&
                  & 3*s2nm + snm*tt) + 16*tt**2*(s1mm + s2nm - 2*s3mm - 2*s3nm + snm*tt) - ss*tt*(s&
                  &1mm + s2nm - 2*(s3mm + s3nm + snm*tt))) - me2*(6*Mm2**4*snm + Mm2**2*(6*(-s1mm +&
                  & s3mm + s3nm)*ss + 8*(-s1mm + s2nm)*tt) + 8*Mm2**3*(-2*s1mm + s2nm + s3mm + s3nm&
                  & - snm*tt) + 2*Mm2*tt*(4*tt*(-4*s1mm + s2nm + 3*s3mm + 3*s3nm - 2*snm*tt) + ss*(&
                  &5*s1mm - s2nm - 4*s3mm - 4*s3nm + 2*snm*tt)) + tt*(ss**2*(3*s1mm - 3*s3mm - 3*s3&
                  &nm - 4*snm*tt) + 2*ss*tt*(-3*s1mm - 10*s2nm + 13*s3mm + 13*s3nm + 4*snm*tt) + 2*&
                  &tt**2*(8*s1mm + 8*s2nm - 16*s3mm - 16*s3nm + 9*snm*tt))))/(4.*ss*(-4*me2*Mm2 + (&
                  &me2 + Mm2 - tt)**2)*tt)
    coeff_disct_lmm = -(1./4.)*(tt*(-6*me2**5*snm - 6*Mm2**5*snm + Mm2**4*(18*s1mm - 6*s2nm - 12*s3mm - 1&
                      &2*s3nm + snm*ss + 30*snm*tt) + me2**4*(6*s1mm + 6*s2nm - 12*s3mm - 12*s3nm - 6*M&
                      &m2*snm + snm*ss + 30*snm*tt) + me2**3*(12*Mm2**2*snm - snm*ss**2 + ss*(-6*s1mm -&
                      & 10*s2nm + 16*s3mm + 16*s3nm - 15*snm*tt) + 12*Mm2*(s1mm - s2nm + 4*snm*tt) - 12&
                      &*tt*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 5*snm*tt)) + tt**2*(6*tt**2*(s1mm + s2n&
                      &m - 2*s3mm - 2*s3nm + snm*tt) + ss**2*(5*s1mm + 10*s2nm - 15*s3mm - 15*s3nm + 6*&
                      &snm*tt) + ss*tt*(11*s1mm + 15*s2nm - 26*s3mm - 26*s3nm + 12*snm*tt)) - Mm2**3*(s&
                      &nm*ss**2 + 12*tt*(5*s1mm - s2nm - 4*s3mm - 4*s3nm + 5*snm*tt) + ss*(8*s1mm + 8*s&
                      &2nm - 16*s3mm - 16*s3nm + 15*snm*tt)) - Mm2*tt*(6*tt**2*(6*s1mm + 2*s2nm - 8*s3m&
                      &m - 8*s3nm + 5*snm*tt) + ss**2*(9*s1mm + 10*s2nm - 19*s3mm - 19*s3nm + 8*snm*tt)&
                      & + ss*tt*(42*s1mm + 26*s2nm - 68*s3mm - 68*s3nm + 37*snm*tt)) + Mm2**2*(ss**2*(2&
                      &*s1mm - 2*s3mm - 2*s3nm + 3*snm*tt) + 12*tt**2*(6*s1mm - 6*s3mm - 6*s3nm + 5*snm&
                      &*tt) + ss*tt*(39*s1mm + 19*s2nm - 58*s3mm - 58*s3nm + 39*snm*tt)) + me2**2*(12*M&
                      &m2**3*snm + Mm2**2*(-24*s1mm + 24*s3mm + 24*s3nm - 2*snm*ss + 36*snm*tt) + Mm2*(&
                      &snm*ss**2 + ss*(4*s1mm + 12*s2nm - 16*s3mm - 16*s3nm - 17*snm*tt) - 12*tt*(5*s1m&
                      &m - s2nm - 4*s3mm - 4*s3nm + 9*snm*tt)) + tt*(3*snm*ss**2 + 12*tt*(3*s1mm + 3*s2&
                      &nm - 6*s3mm - 6*s3nm + 5*snm*tt) + ss*(23*s1mm + 35*s2nm - 58*s3mm - 58*s3nm + 3&
                      &9*snm*tt))) + me2*(-6*Mm2**4*snm - 12*Mm2**3*(s1mm - s2nm - 4*snm*tt) + Mm2**2*(&
                      &snm*ss**2 + ss*(10*s1mm + 6*s2nm - 16*s3mm - 16*s3nm - 17*snm*tt) - 12*tt*(4*s1m&
                      &m - 4*s3mm - 4*s3nm + 9*snm*tt)) - tt*(6*tt**2*(4*s1mm + 4*s2nm - 8*s3mm - 8*s3n&
                      &m + 5*snm*tt) + ss**2*(5*s1mm + 10*s2nm - 15*s3mm - 15*s3nm + 8*snm*tt) + ss*tt*&
                      &(28*s1mm + 40*s2nm - 68*s3mm - 68*s3nm + 37*snm*tt)) + 2*Mm2*(ss**2*(-s1mm + s3m&
                      &m + s3nm + snm*tt) + 3*ss*tt*(7*s1mm + 3*s2nm - 10*s3mm - 10*s3nm + 9*snm*tt) + &
                      &6*tt**2*(7*s1mm + s2nm - 8*(s3mm + s3nm - snm*tt))))))/(ss*(-4*me2*Mm2 + (me2 + &
                      &Mm2 - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_disct_lmu = (-2*(me2 + Mm2 - tt)*tt*(me2**2*snm + Mm2**2*snm + s1mm*ss + 2*s2nm*ss - 3*s3mm*&
                      &ss - 3*s3nm*ss + s1mm*tt + s2nm*tt - 2*s3mm*tt - 2*s3nm*tt + snm*ss*tt + snm*tt*&
                      &*2 + Mm2*(-3*s1mm + s2nm + 2*(s3mm + s3nm - snm*tt)) - me2*(s1mm + s2nm - 2*(s3m&
                      &m + s3nm + Mm2*snm - snm*tt))))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
    coeff_disct_ls = (tt*(-2*me2**5*snm - 2*Mm2**5*snm + Mm2**4*(6*s1mm - 2*s2nm - 4*s3mm - 4*s3nm + &
                     &snm*ss + 10*snm*tt) + me2**4*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm - 2*Mm2*snm + sn&
                     &m*ss + 10*snm*tt) + Mm2**3*(-(snm*ss**2) + ss*(-4*s1mm + 4*s3mm + 4*s3nm - 7*snm&
                     &*tt) + 4*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm - 5*snm*tt)) + tt**2*(2*tt**2*(s1m&
                     &m + s2nm - 2*s3mm - 2*s3nm + snm*tt) + ss**2*(s1mm + 2*s2nm - 3*s3mm - 3*s3nm + &
                     &2*snm*tt) + ss*tt*(3*s1mm + 3*s2nm - 6*s3mm - 6*s3nm + 4*snm*tt)) + me2**3*(4*Mm&
                     &2**2*snm - snm*ss**2 + ss*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm - 7*snm*tt) + 4*Mm&
                     &2*(s1mm - s2nm + 4*snm*tt) - 4*tt*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 5*snm*tt)&
                     &) - Mm2*tt*(ss**2*(5*s1mm + 2*s2nm - 7*s3mm - 7*s3nm + 4*snm*tt) + 2*tt**2*(6*s1&
                     &mm + 2*s2nm - 8*s3mm - 8*s3nm + 5*snm*tt) + ss*tt*(14*s1mm + 2*s2nm - 16*s3mm - &
                     &16*s3nm + 13*snm*tt)) + Mm2**2*(ss**2*(2*s1mm - 2*s3mm - 2*s3nm + 3*snm*tt) + 4*&
                     &tt**2*(6*s1mm - 6*s3mm - 6*s3nm + 5*snm*tt) + ss*tt*(15*s1mm - s2nm - 14*s3mm - &
                     &14*s3nm + 15*snm*tt)) + me2**2*(4*Mm2**3*snm + Mm2**2*(-8*s1mm + 8*s3mm + 8*s3nm&
                     & - 2*snm*ss + 12*snm*tt) + Mm2*(snm*ss**2 + ss*(4*s2nm - 4*s3mm - 4*s3nm - 9*snm&
                     &*tt) + 4*tt*(-5*s1mm + s2nm + 4*s3mm + 4*s3nm - 9*snm*tt)) + tt*(3*snm*ss**2 + 4&
                     &*tt*(3*s1mm + 3*s2nm - 6*s3mm - 6*s3nm + 5*snm*tt) + ss*(7*s1mm + 7*s2nm - 14*s3&
                     &mm - 14*s3nm + 15*snm*tt))) + me2*(-2*Mm2**4*snm - 4*Mm2**3*(s1mm - s2nm - 4*snm&
                     &*tt) + Mm2**2*(snm*ss**2 + ss*(6*s1mm - 2*s2nm - 4*s3mm - 4*s3nm - 9*snm*tt) - 4&
                     &*tt*(4*s1mm - 4*s3mm - 4*s3nm + 9*snm*tt)) - tt*(ss**2*(s1mm + 2*s2nm - 3*s3mm -&
                     & 3*s3nm + 4*snm*tt) + 2*tt**2*(4*s1mm + 4*s2nm - 8*s3mm - 8*s3nm + 5*snm*tt) + s&
                     &s*tt*(8*s1mm + 8*s2nm - 16*s3mm - 16*s3nm + 13*snm*tt)) + 2*Mm2*(ss**2*(-s1mm + &
                     &s3mm + s3nm + snm*tt) + ss*tt*(9*s1mm + s2nm - 10*s3mm - 10*s3nm + 11*snm*tt) + &
                     &2*tt**2*(7*s1mm + s2nm - 8*(s3mm + s3nm - snm*tt))))))/(2.*ss*(-4*me2*Mm2 + (me2&
                     & + Mm2 - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_discu = -(1./2.)*(2*me2**4*snm - 2*me2**3*(s1mm + s2nm - 2*s3mm - 2*s3nm + 3*snm*tt) + me2**&
                  &2*(-4*Mm2**2*snm - 9*snm*ss**2 + ss*(-12*s1mm - 24*s2nm + 36*s3mm + 36*s3nm + sn&
                  &m*tt) + 2*tt*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 3*snm*tt) - 2*Mm2*(9*s1mm + 5*&
                  &s2nm - 14*s3mm - 14*s3nm - 8*snm*ss + 5*snm*tt)) + (Mm2 - ss - tt)*(2*Mm2**3*snm&
                  & + 2*Mm2**2*(-3*s1mm + s2nm + 2*s3mm + 2*s3nm + snm*ss - 2*snm*tt) + ss*(ss + tt&
                  &)*(8*s1mm + 16*s2nm - 24*s3mm - 24*s3nm + 3*snm*ss + snm*tt) - Mm2*(7*snm*ss**2 &
                  &+ ss*(14*s1mm + 26*s2nm - 40*s3mm - 40*s3nm + snm*tt) - 2*tt*(s1mm + s2nm - 2*s3&
                  &mm - 2*s3nm + snm*tt))) - 2*me2*(Mm2**2*(19*s1mm - 5*s2nm - 14*s3mm - 14*s3nm - &
                  &8*snm*ss + 5*snm*tt) - (ss + tt)*(5*snm*ss**2 + ss*(11*s1mm + 21*s2nm - 32*s3mm &
                  &- 32*s3nm + snm*tt) - tt*(s1mm + s2nm - 2*s3mm - 2*s3nm + snm*tt)) + Mm2*(11*snm&
                  &*ss**2 + ss*(6*s1mm + 14*s2nm - 20*s3mm - 20*s3nm + snm*tt) - 2*tt*(5*s1mm + 3*s&
                  &2nm - 8*s3mm - 8*s3nm + 3*snm*tt))))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)&
                  &)
    coeff_discu_lmm = ((-2*me2 - 2*Mm2 + ss + tt)*(-6*me2**5*snm - 6*Mm2**5*snm + 3*Mm2**4*(6*s1mm - 2&
                      &*s2nm - 4*s3mm - 4*s3nm + 3*snm*ss + 10*snm*tt) + 3*me2**4*(2*s1mm + 2*s2nm - 4*&
                      &s3mm - 4*s3nm - 2*Mm2*snm + 3*snm*ss + 10*snm*tt) + Mm2**3*(-7*snm*ss**2 + ss*(-&
                      &26*s1mm - 6*s2nm + 32*s3mm + 32*s3nm - 47*snm*tt) + 12*tt*(-5*s1mm + s2nm + 4*s3&
                      &mm + 4*s3nm - 5*snm*tt)) + me2**3*(12*Mm2**2*snm - 7*snm*ss**2 + ss*(-14*s1mm - &
                      &18*s2nm + 32*s3mm + 32*s3nm - 47*snm*tt) + 12*Mm2*(s1mm - s2nm + 4*snm*tt) - 12*&
                      &tt*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 5*snm*tt)) + (ss + tt)**2*(snm*ss**3 + 6&
                      &*tt**2*(s1mm + s2nm - 2*s3mm - 2*s3nm + snm*tt) + ss**2*(s1mm - s3mm - s3nm + 3*&
                      &snm*tt) + ss*tt*(7*s1mm + 11*s2nm - 18*s3mm - 18*s3nm + 8*snm*tt)) - Mm2*(ss + t&
                      &t)*(4*snm*ss**3 + 6*tt**2*(6*s1mm + 2*s2nm - 8*s3mm - 8*s3nm + 5*snm*tt) + ss**2&
                      &*(4*s1mm - s2nm - 3*s3mm - 3*s3nm + 13*snm*tt) + ss*tt*(40*s1mm + 28*s2nm - 68*s&
                      &3mm - 68*s3nm + 39*snm*tt)) + Mm2**2*(7*snm*ss**3 + 12*tt**2*(6*s1mm - 6*s3mm - &
                      &6*s3nm + 5*snm*tt) + ss**2*(11*s1mm + 9*s2nm - 20*s3mm - 20*s3nm + 34*snm*tt) + &
                      &ss*tt*(83*s1mm + 23*s2nm - 106*s3mm - 106*s3nm + 87*snm*tt)) + me2**2*(12*Mm2**3&
                      &*snm + 7*snm*ss**3 - 6*Mm2**2*(4*s1mm - 4*s3mm - 4*s3nm + 3*snm*ss - 6*snm*tt) +&
                      & 12*tt**2*(3*s1mm + 3*s2nm - 6*s3mm - 6*s3nm + 5*snm*tt) + ss**2*(11*s1mm + 13*s&
                      &2nm - 24*s3mm - 24*s3nm + 34*snm*tt) + ss*tt*(47*s1mm + 59*s2nm - 106*s3mm - 106&
                      &*s3nm + 87*snm*tt) - Mm2*(snm*ss**2 + 12*tt*(5*s1mm - s2nm - 4*s3mm - 4*s3nm + 9&
                      &*snm*tt) + ss*(-2*s1mm - 30*s2nm + 32*s3mm + 32*s3nm + 49*snm*tt))) - me2*(6*Mm2&
                      &**4*snm + 12*Mm2**3*(s1mm - s2nm - 4*snm*tt) + (ss + tt)*(4*snm*ss**3 + 6*tt**2*&
                      &(4*s1mm + 4*s2nm - 8*s3mm - 8*s3nm + 5*snm*tt) + ss**2*(4*s1mm + s2nm - 5*s3mm -&
                      & 5*s3nm + 13*snm*tt) + ss*tt*(28*s1mm + 40*s2nm - 68*s3mm - 68*s3nm + 39*snm*tt)&
                      &) + Mm2**2*(snm*ss**2 + 12*tt*(4*s1mm - 4*s3mm - 4*s3nm + 9*snm*tt) + ss*(-38*s1&
                      &mm + 6*s2nm + 32*s3mm + 32*s3nm + 49*snm*tt)) - 2*Mm2*(3*snm*ss**3 + ss*tt*(39*s&
                      &1mm + 7*s2nm - 46*s3mm - 46*s3nm + 59*snm*tt) + 6*tt**2*(7*s1mm + s2nm - 8*(s3mm&
                      & + s3nm - snm*tt)) + ss**2*(-3*s1mm - 11*s2nm + 14*(s3mm + s3nm + snm*tt))))))/(&
                      &4.*ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me&
                      &2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_discu_lmu = (-2*(-2*me2 - 2*Mm2 + ss + tt)*(-me2 - Mm2 + ss + tt)*(me2**2*snm + Mm2**2*snm +&
                      & s1mm*ss + 2*s2nm*ss - 3*s3mm*ss - 3*s3nm*ss + s1mm*tt + s2nm*tt - 2*s3mm*tt - 2&
                      &*s3nm*tt + snm*ss*tt + snm*tt**2 + Mm2*(-3*s1mm + s2nm + 2*(s3mm + s3nm - snm*tt&
                      &)) - me2*(s1mm + s2nm - 2*(s3mm + s3nm + Mm2*snm - snm*tt))))/(ss*(-4*me2*Mm2 + &
                      &(me2 + Mm2 - ss - tt)**2))
    coeff_discu_ls = -(1./2.)*((-2*me2 - 2*Mm2 + ss + tt)*(-2*me2**5*snm - 2*Mm2**5*snm + Mm2**4*(6*s1mm &
                     &- 2*s2nm - 4*s3mm - 4*s3nm + 5*snm*ss + 10*snm*tt) + me2**4*(2*s1mm + 2*s2nm - 4&
                     &*s3mm - 4*s3nm - 2*Mm2*snm + 5*snm*ss + 10*snm*tt) + Mm2**3*(-7*snm*ss**2 + ss*(&
                     &-10*s1mm - 2*s2nm + 12*s3mm + 12*s3nm - 23*snm*tt) + 4*tt*(-5*s1mm + s2nm + 4*s3&
                     &mm + 4*s3nm - 5*snm*tt)) + (ss + tt)**2*(snm*ss**3 + 2*tt**2*(s1mm + s2nm - 2*s3&
                     &mm - 2*s3nm + snm*tt) + ss**2*(s1mm - s3mm - s3nm + 3*snm*tt) + ss*tt*(3*s1mm + &
                     &3*s2nm - 6*s3mm - 6*s3nm + 4*snm*tt)) + me2**3*(4*Mm2**2*snm - 7*snm*ss**2 + ss*&
                     &(-6*s1mm - 6*s2nm + 12*s3mm + 12*s3nm - 23*snm*tt) + 4*Mm2*(s1mm - s2nm + 4*snm*&
                     &tt) - 4*tt*(2*s1mm + 2*s2nm - 4*s3mm - 4*s3nm + 5*snm*tt)) - Mm2*(ss + tt)*(4*sn&
                     &m*ss**3 + 2*tt**2*(6*s1mm + 2*s2nm - 8*s3mm - 8*s3nm + 5*snm*tt) + ss**2*(4*s1mm&
                     & - s2nm - 3*s3mm - 3*s3nm + 13*snm*tt) + ss*tt*(16*s1mm + 8*s2nm - 24*s3mm - 24*&
                     &s3nm + 19*snm*tt)) + Mm2**2*(7*snm*ss**3 + 4*tt**2*(6*s1mm - 6*s3mm - 6*s3nm + 5&
                     &*snm*tt) + ss**2*(7*s1mm + s2nm - 8*s3mm - 8*s3nm + 26*snm*tt) + ss*tt*(31*s1mm &
                     &+ 7*s2nm - 38*s3mm - 38*s3nm + 39*snm*tt)) + me2**2*(4*Mm2**3*snm + 7*snm*ss**3 &
                     &+ 4*tt**2*(3*s1mm + 3*s2nm - 6*s3mm - 6*s3nm + 5*snm*tt) + Mm2**2*(-8*s1mm + 8*s&
                     &3mm + 8*s3nm - 10*snm*ss + 12*snm*tt) + ss**2*(7*s1mm + 5*s2nm - 12*s3mm - 12*s3&
                     &nm + 26*snm*tt) + ss*tt*(19*s1mm + 19*s2nm - 38*s3mm - 38*s3nm + 39*snm*tt) - Mm&
                     &2*(snm*ss**2 + 4*tt*(5*s1mm - s2nm - 4*s3mm - 4*s3nm + 9*snm*tt) + ss*(-2*s1mm -&
                     & 10*s2nm + 12*s3mm + 12*s3nm + 25*snm*tt))) - me2*(2*Mm2**4*snm + 4*Mm2**3*(s1mm&
                     & - s2nm - 4*snm*tt) + (ss + tt)*(4*snm*ss**3 + 2*tt**2*(4*s1mm + 4*s2nm - 8*s3mm&
                     & - 8*s3nm + 5*snm*tt) + ss**2*(4*s1mm + s2nm - 5*s3mm - 5*s3nm + 13*snm*tt) + ss&
                     &*tt*(12*s1mm + 12*s2nm - 24*s3mm - 24*s3nm + 19*snm*tt)) + Mm2**2*(snm*ss**2 + 4&
                     &*tt*(4*s1mm - 4*s3mm - 4*s3nm + 9*snm*tt) + ss*(-14*s1mm + 2*s2nm + 12*s3mm + 12&
                     &*s3nm + 25*snm*tt)) - 2*Mm2*(3*snm*ss**3 + 3*ss*tt*(5*s1mm + s2nm - 6*s3mm - 6*s&
                     &3nm + 9*snm*tt) + 2*tt**2*(7*s1mm + s2nm - 8*(s3mm + s3nm - snm*tt)) + ss**2*(s1&
                     &mm - 3*s2nm + 2*(s3mm + s3nm + 7*snm*tt))))))/(ss*(-4*me2*Mm2 + (me2 + Mm2 - ss &
                     &- tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_lmm = -(1./24.)*(48*Mm2**6*snm - 12*me2**5*snm*(4*Mm2 - ss) + 6*me2**4*(4*&
                &Mm2 - ss)*(-2*s1mm - 2*s2nm + 4*s3mm + 4*s3nm + 2*Mm2*snm + snm*ss + 5*snm*tt) -&
                & 12*Mm2**5*(12*s1mm - 4*s2nm - 8*s3mm - 8*s3nm + 3*snm*ss + 10*snm*tt) + ss*tt*(&
                &ss + tt)*(ss*tt*(3*s1mm - 17*s2nm + 14*s3mm + 14*s3nm - 8*snm*tt) - 14*tt**2*(s1&
                &mm + s2nm - 2*s3mm - 2*s3nm + snm*tt) + ss**2*(9*s1mm - 9*s3mm - 9*s3nm + 6*snm*&
                &tt)) + 2*Mm2**4*(3*snm*ss**2 + 4*tt*(15*s1mm + 3*s2nm - 18*s3mm - 18*s3nm + 4*sn&
                &m*tt) + 3*ss*(10*s1mm - 6*s2nm - 4*s3mm - 4*s3nm + 13*snm*tt)) + 2*me2**3*(4*Mm2&
                & - ss)*(12*Mm2**2*snm + tt*(21*s1mm + 21*s2nm - 42*s3mm - 42*s3nm - 32*snm*tt) -&
                & 3*ss*(s1mm - s2nm + 2*snm*tt) - 6*Mm2*(2*s1mm - 2*s2nm + snm*(ss + tt))) - 2*Mm&
                &2**2*(3*(s1mm - s3mm - s3nm)*ss**3 + 4*tt**3*(29*s1mm + s2nm - 30*s3mm - 30*s3nm&
                & + 22*snm*tt) + ss**2*tt*(45*s1mm - 12*s2nm - 33*s3mm - 33*s3nm + 28*snm*tt) + s&
                &s*tt**2*(171*s1mm - s2nm - 2*(85*s3mm + 85*s3nm - 78*snm*tt))) + Mm2*tt*(56*tt**&
                &3*(s1mm + s2nm - 2*s3mm - 2*s3nm + snm*tt) - 3*ss**3*(9*s1mm + s2nm - 10*s3mm - &
                &10*s3nm + 4*snm*tt) + ss**2*tt*(51*s1mm + 97*s2nm - 4*(37*s3mm + 37*s3nm - 28*sn&
                &m*tt)) + 6*ss*tt**2*(25*s1mm + 29*s2nm - 6*(9*s3mm + 9*s3nm - 5*snm*tt))) + 2*Mm&
                &2**3*(3*ss*tt*(31*s1mm - 9*s2nm - 22*s3mm - 22*s3nm + 20*snm*tt) + 4*tt**2*(21*s&
                &1mm - 11*s2nm - 10*(s3mm + s3nm - 2*snm*tt)) + 3*ss**2*(3*s1mm + s2nm - 2*(2*s3m&
                &m + 2*s3nm + snm*tt))) - me2*(48*Mm2**5*snm - 12*Mm2**4*(8*s1mm - 8*s2nm + 5*snm&
                &*ss + 4*snm*tt) + ss*tt*(ss*tt*(-5*s1mm - 75*s2nm + 80*s3mm + 80*s3nm - 44*snm*t&
                &t) + 3*ss**2*(7*s1mm - s2nm - 6*s3mm - 6*s3nm + 8*snm*tt) - 2*tt**2*(21*s1mm + 2&
                &1*s2nm - 42*s3mm - 42*s3nm + 34*snm*tt)) - 4*Mm2*(3*(s1mm - s3mm - s3nm)*ss**3 +&
                & ss*tt**2*(-51*s1mm - 93*s2nm + 144*s3mm + 144*s3nm - 115*snm*tt) - ss**2*tt*(-1&
                &8*s1mm + 9*s2nm + 9*s3mm + 9*s3nm + snm*tt) - 2*tt**3*(21*s1mm + 21*s2nm - 42*s3&
                &mm - 42*s3nm + 34*snm*tt)) + 2*Mm2**2*(ss*tt*(-9*s1mm - 57*s2nm + 66*s3mm + 66*s&
                &3nm - 140*snm*tt) - 8*tt**2*(22*s1mm - 6*s2nm - 16*s3mm - 16*s3nm + 35*snm*tt) +&
                & 3*ss**2*(5*s1mm + 3*s2nm - 2*(4*s3mm + 4*s3nm + snm*tt))) + 12*Mm2**3*(snm*ss**&
                &2 + ss*(8*s1mm - 8*s2nm + 5*snm*tt) + 2*tt*(-5*s1mm + 3*s2nm + 2*(s3mm + s3nm + &
                &6*snm*tt)))) - 2*me2**2*(48*Mm2**4*snm - 12*Mm2**3*(8*s1mm - 8*s3mm - 8*s3nm + s&
                &nm*ss) - 12*Mm2**2*(ss*(s1mm - 3*s2nm + 2*(s3mm + s3nm + snm*tt)) - tt*(-15*s1mm&
                & + 5*s2nm + 2*(5*s3mm + 5*s3nm + 8*snm*tt))) + Mm2*(ss*tt*(81*s1mm + 9*s2nm - 90&
                &*s3mm - 90*s3nm - 88*snm*tt) - 3*ss**2*(s1mm + 3*s2nm - 2*(2*s3mm + 2*s3nm + snm&
                &*tt)) + 4*tt**2*(s1mm + s2nm - 2*(s3mm + s3nm + 25*snm*tt))) + ss*(3*(s1mm - s3m&
                &m - s3nm)*ss**2 + ss*tt*(-9*s1mm - 6*s2nm + 15*s3mm + 15*s3nm - 2*snm*tt) + tt**&
                &2*(-s1mm - s2nm + 2*(s3mm + s3nm + 25*snm*tt)))))/((4*Mm2 - ss)*ss*tt**2*(-2*me2&
                & - 2*Mm2 + ss + tt))
    coeff_lmu = (7*me2**2*snm + 7*Mm2**2*snm + 5*s1mm*ss + 10*s2nm*ss - 15*s3mm*ss - 15*s3nm*ss &
                &+ 5*s1mm*tt + 5*s2nm*tt - 10*s3mm*tt - 10*s3nm*tt + 7*snm*ss*tt + 7*snm*tt**2 + &
                &Mm2*(-15*s1mm + 5*s2nm + 2*(5*s3mm + 5*s3nm - 7*snm*tt)) + me2*(-5*s1mm - 5*s2nm&
                & + 2*(5*s3mm + 5*s3nm + 7*Mm2*snm - 7*snm*tt)))/(6.*ss)
    coeff_ls = (-8*Mm2**2*(s1mm - s2nm) + 16*me2**2*Mm2*snm + ss*(-2*s1mm + 2*s2nm + snm*ss)*(s&
               &s + tt) - Mm2*((-14*s1mm + 6*s2nm + 8*(s3mm + s3nm))*ss + 3*snm*ss**2 + 8*(-s1mm&
               & + s2nm)*tt) + me2*(16*Mm2**2*snm + ss*(2*s1mm - 2*s2nm - 3*snm*ss) - 8*Mm2*(3*s&
               &1mm + s2nm - 4*s3mm - 4*s3nm + 2*snm*tt)))/(2.*(4*me2 - ss)*(-4*Mm2 + ss))
    coeff_unity = (3*me2**3*snm*(3*ss + 8*tt) + 3*Mm2**3*snm*(3*ss + 8*tt) + Mm2**2*(-48*snm*tt**2&
                  & + ss*(-27*s1mm + 9*s2nm + 18*s3mm + 18*s3nm - 19*snm*tt)) + me2**2*(ss*(9*s1mm &
                  &+ 9*s2nm - 18*s3mm - 18*s3nm - 9*Mm2*snm - 19*snm*tt) - 24*tt*(s1mm + s2nm - 2*s&
                  &3mm - 2*s3nm - 3*Mm2*snm + 2*snm*tt)) + Mm2*(24*snm*tt**3 + 9*ss**2*(-s1mm + s3m&
                  &m + s3nm + 18*snm*tt) + ss*tt*(174*s1mm + 10*s2nm - 184*s3mm - 184*s3nm + 35*snm&
                  &*tt)) - ss*tt*(51*snm*ss**2 + tt*(59*s1mm + 59*s2nm - 118*s3mm - 118*s3nm + snm*&
                  &tt) + ss*(17*s1mm - 38*s2nm + 21*s3mm + 21*s3nm + 118*snm*tt)) + me2*(-9*Mm2**2*&
                  &snm*(ss - 8*tt) + 2*Mm2*ss*(9*s1mm - 9*s2nm - 73*snm*tt) + 24*tt**2*(s1mm + s2nm&
                  & - 2*s3mm - 2*s3nm + snm*tt) + 9*ss**2*(s1mm - s3mm - s3nm + 18*snm*tt) + ss*tt*&
                  &(50*s1mm + 74*s2nm - 124*s3mm - 124*s3nm + 35*snm*tt) + 24*Mm2*tt*(-3*s1mm + s2n&
                  &m + 2*(s3mm + s3nm - 2*snm*tt))))/(18.*ss**2*tt)
    coeffRR = coeffRR + c6se*coeff_c6se + c6sm*coeff_c6sm + c6t*coeff_c6t  &
            + c6u*coeff_c6u + ce*coeff_ce + cm*coeff_cm  &
            + coeff_discse*discse + coeff_discse_lmm*discse*lmm + coeff_discse_lmu*discse*lmu  &
            + coeff_discsm*discsm + coeff_discsm_lmu*discsm*lmu + coeff_disct*disct  &
            + coeff_disct_lmm*disct*lmm + coeff_disct_lmu*disct*lmu + coeff_disct_ls*disctLs  &
            + coeff_discu*discu + coeff_discu_lmm*discu*lmm + coeff_discu_lmu*discu*lmu  &
            + coeff_discu_ls*discuLs + coeff_lmm*lmm + coeff_lmu*lmu  &
            + coeff_ls*ls + coeff_unity
    coeff_c6se = ((2*me2 - ss)*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(s2&
                 &nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*ss&
                 &)
    coeff_c6sm = ((2*Mm2 - ss)*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(s2&
                 &nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*ss&
                 &)
    coeff_c6t = (me2**6*(-4*s2nm*s3mm + 4*s1mm*s3nm) + me2**5*(10*Mm2*s2nm*s3mm + 2*Mm2*s1mm*(s2&
                &nm - 5*s3nm) + s3mm*(s2nm - 2*s3nm)*ss + s1mm*(-s2nm + s3nm)*ss + 22*(s2nm*s3mm &
                &- s1mm*s3nm)*tt) + me2**4*(-2*Mm2**2*(3*s1mm*s2nm + s2nm*s3mm - s1mm*s3nm) + Mm2&
                &*(-3*s2nm*s3mm + 2*s3mm*s3nm + s1mm*(3*s2nm + s3nm))*ss + (s1mm*s2nm - s2nm*s3mm&
                & - 4*s1mm*s3nm + 5*s3mm*s3nm)*ss**2 - 2*Mm2*(5*s1mm*s2nm + 6*s2nm*s3mm - 6*s1mm*&
                &s3nm)*tt + (4*s1mm*s2nm - 13*s2nm*s3mm + 7*s1mm*s3nm + 6*s3mm*s3nm)*ss*tt + 50*(&
                &-(s2nm*s3mm) + s1mm*s3nm)*tt**2) + (Mm2 - tt)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm&
                & + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(2*Mm2**4 + Mm2**3*(s&
                &s - 8*tt) + 2*tt**2*(ss + tt)**2 + Mm2*tt*(2*ss**2 - 7*ss*tt - 8*tt**2) + Mm2**2&
                &*(-ss**2 + 2*ss*tt + 12*tt**2)) + me2**3*(4*Mm2**3*(-3*s2nm*s3mm + s1mm*(s2nm + &
                &3*s3nm)) + (s1mm - s3mm)*s3nm*ss**3 + (-3*s1mm*s2nm + 2*s2nm*s3mm + 9*s1mm*s3nm &
                &- 11*s3mm*s3nm)*ss**2*tt - (6*s1mm*s2nm - 37*s2nm*s3mm + 33*s1mm*s3nm + 4*s3mm*s&
                &3nm)*ss*tt**2 + 60*(s2nm*s3mm - s1mm*s3nm)*tt**3 - 4*Mm2**2*(s1mm*s2nm*ss + s3mm&
                &*(-s2nm + s3nm)*ss + (-2*s1mm*s2nm + 3*s2nm*s3mm - 3*s1mm*s3nm)*tt) + Mm2*((-2*s&
                &1mm*s2nm + 2*s2nm*s3mm + 6*s1mm*s3nm - 8*s3mm*s3nm)*ss**2 + (s1mm*s2nm + 9*s2nm*&
                &s3mm - 17*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt + 4*(5*s1mm*s2nm - 9*s2nm*s3mm + 9*s1mm&
                &*s3nm)*tt**2)) + me2**2*(4*Mm2**4*(2*s2nm*s3mm + s1mm*(s2nm - 2*s3nm)) + Mm2**2*&
                &(2*(-s1mm + s3mm)*s3nm*ss**2 + (-5*s1mm*s2nm + 13*s2nm*s3mm - 17*s1mm*s3nm + 4*s&
                &3mm*s3nm)*ss*tt + 4*(3*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**2) + tt*(3*(-s&
                &1mm + s3mm)*s3nm*ss**3 + 3*(s3mm*(-2*s2nm + s3nm) + s1mm*(s2nm + s3nm))*ss**2*tt&
                & + (4*s1mm*s2nm - 43*s2nm*s3mm + 47*s1mm*s3nm - 4*s3mm*s3nm)*ss*tt**2 + 40*(-(s2&
                &nm*s3mm) + s1mm*s3nm)*tt**3) - Mm2*((s1mm - s3mm)*s3nm*ss**3 + (-3*s1mm*(s2nm + &
                &s3nm) + s3mm*(2*s2nm + s3nm))*ss**2*tt + (15*s1mm*s2nm - 26*s2nm*s3mm + 14*s1mm*&
                &s3nm + 12*s3mm*s3nm)*ss*tt**2 + 20*(-4*s2nm*s3mm + s1mm*(s2nm + 4*s3nm))*tt**3) &
                &+ 4*Mm2**3*(3*s3mm*s3nm*ss - 2*s1mm*s3nm*(ss - 2*tt) + s1mm*s2nm*(ss + tt) - s2n&
                &m*s3mm*(ss + 4*tt))) - me2*(2*Mm2**5*(-(s2nm*s3mm) + s1mm*(3*s2nm + s3nm)) + Mm2&
                &**3*(-2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm))*ss**2 + (s1mm*s2nm + 9*s2nm*s3mm + 1&
                &5*s1mm*s3nm - 24*s3mm*s3nm)*ss*tt - 4*(3*s1mm*s2nm - 11*s2nm*s3mm + 11*s1mm*s3nm&
                &)*tt**2) + Mm2**2*((s1mm - s3mm)*s3nm*ss**3 + (-4*s2nm*s3mm + 3*s1mm*(s2nm - 3*s&
                &3nm) + 13*s3mm*s3nm)*ss**2*tt + (10*s1mm*s2nm - 31*s2nm*s3mm + 19*s1mm*s3nm + 12&
                &*s3mm*s3nm)*ss*tt**2 + 4*(6*s1mm*s2nm - 19*s2nm*s3mm + 19*s1mm*s3nm)*tt**3) - Mm&
                &2*tt*(2*(s1mm - s3mm)*s3nm*ss**3 + (-7*s2nm*s3mm + 6*s3mm*s3nm + s1mm*(3*s2nm + &
                &s3nm))*ss**2*tt + (15*s1mm*s2nm - 47*s2nm*s3mm + 55*s1mm*s3nm - 8*s3mm*s3nm)*ss*&
                &tt**2 + 2*(5*s1mm*s2nm - 27*s2nm*s3mm + 27*s1mm*s3nm)*tt**3) + Mm2**4*(10*s3mm*s&
                &3nm*ss - 3*s2nm*s3mm*(ss + 2*tt) + s1mm*(3*s2nm*ss - 7*s3nm*ss - 8*s2nm*tt + 6*s&
                &3nm*tt)) + tt**3*(s1mm*(ss + tt)*(s2nm*ss + 14*s3nm*(ss + tt)) - s3mm*(s3nm*ss*(&
                &7*ss + 6*tt) + s2nm*(7*ss**2 + 22*ss*tt + 14*tt**2)))))/(2.*me2*ss*(me2**2 + Mm2&
                &**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_c6u = (me2**6*(-4*s2nm*s3mm + 4*s1mm*s3nm) + me2**5*(10*Mm2*s2nm*s3mm + 2*Mm2*s1mm*(s2&
                &nm - 5*s3nm) + s3mm*(s2nm - 2*s3nm)*ss + s1mm*(-s2nm + s3nm)*ss + 22*(s2nm*s3mm &
                &- s1mm*s3nm)*tt) + me2**4*(-2*Mm2**2*(3*s1mm*s2nm + s2nm*s3mm - s1mm*s3nm) + Mm2&
                &*(5*s1mm*s2nm - 21*s2nm*s3mm + 19*s1mm*s3nm + 2*s3mm*s3nm)*ss + (4*s1mm*s2nm + 2&
                &*s2nm*s3mm - 19*s1mm*s3nm + 17*s3mm*s3nm)*ss**2 - 2*Mm2*(5*s1mm*s2nm + 6*s2nm*s3&
                &mm - 6*s1mm*s3nm)*tt + (4*s1mm*s2nm - 13*s2nm*s3mm + 7*s1mm*s3nm + 6*s3mm*s3nm)*&
                &ss*tt + 50*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2) + (-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2n&
                &m + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(2*Mm2**5 + Mm2**4*(&
                &ss - 10*tt) - Mm2*(ss + tt)**2*(4*ss**2 + 9*ss*tt - 10*tt**2) + (ss + tt)**3*(ss&
                &**2 + 2*ss*tt - 2*tt**2) + Mm2**3*(-7*ss**2 + ss*tt + 20*tt**2) + Mm2**2*(7*ss**&
                &3 + 18*ss**2*tt - 9*ss*tt**2 - 20*tt**3)) - me2*(2*Mm2**5*(-(s2nm*s3mm) + s1mm*(&
                &3*s2nm + s3nm)) - Mm2**3*((s1mm*(s2nm - 25*s3nm) + s3mm*(s2nm + 24*s3nm))*ss**2 &
                &+ (-31*s1mm*s2nm + 9*s2nm*s3mm - 33*s1mm*s3nm + 24*s3mm*s3nm)*ss*tt + 4*(3*s1mm*&
                &s2nm - 11*s2nm*s3mm + 11*s1mm*s3nm)*tt**2) + (ss + tt)**2*((s1mm*s2nm - s2nm*s3m&
                &m - 7*s1mm*s3nm + 8*s3mm*s3nm)*ss**3 + (2*s1mm*s2nm + 4*s2nm*s3mm - 21*s1mm*s3nm&
                & + 17*s3mm*s3nm)*ss**2*tt + (s1mm*s2nm + 6*s3mm*(s2nm - s3nm))*ss*tt**2 + 14*(-(&
                &s2nm*s3mm) + s1mm*s3nm)*tt**3) + Mm2**2*((2*s2nm*s3mm - 27*s1mm*s3nm + 25*s3mm*s&
                &3nm)*ss**3 + (-32*s1mm*s2nm + 29*s2nm*s3mm - 102*s1mm*s3nm + 73*s3mm*s3nm)*ss**2&
                &*tt + (-13*s2nm*s3mm + 12*s3mm*s3nm + s1mm*(-8*s2nm + s3nm))*ss*tt**2 + 4*(6*s1m&
                &m*s2nm - 19*s2nm*s3mm + 19*s1mm*s3nm)*tt**3) + Mm2*(ss + tt)*(22*(s1mm - s3mm)*s&
                &3nm*ss**3 + (7*s1mm*s2nm - 19*s2nm*s3mm + 81*s1mm*s3nm - 62*s3mm*s3nm)*ss**2*tt &
                &+ (-3*s1mm*s2nm - 13*s2nm*s3mm + 5*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt**2 - 2*(5*s1mm&
                &*s2nm - 27*s2nm*s3mm + 27*s1mm*s3nm)*tt**3) + Mm2**4*(10*s3mm*s3nm*ss + 3*s2nm*s&
                &3mm*(ss - 2*tt) - s1mm*(11*s2nm*ss + 13*s3nm*ss + 8*s2nm*tt - 6*s3nm*tt))) + me2&
                &**3*(4*Mm2**3*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm)) + 2*Mm2**2*(s3mm*(17*s2nm*ss&
                & - 2*s3nm*ss - 6*s2nm*tt) + s1mm*(3*s2nm*ss - 15*s3nm*ss + 4*s2nm*tt + 6*s3nm*tt&
                &)) + Mm2*(8*s3mm*s3nm*ss*(3*ss + tt) + s2nm*s3mm*(-5*ss**2 + 51*ss*tt - 36*tt**2&
                &) - s1mm*s3nm*(19*ss**2 + 59*ss*tt - 36*tt**2) - 5*s1mm*s2nm*(ss**2 + ss*tt - 4*&
                &tt**2)) - 3*s1mm*(ss + tt)*(2*s2nm*ss*(ss + tt) + s3nm*(-9*ss**2 - 9*ss*tt + 20*&
                &tt**2)) + s3mm*(-(s3nm*ss*(31*ss**2 + 47*ss*tt + 4*tt**2)) + s2nm*(4*ss**3 - 7*s&
                &s**2*tt + 37*ss*tt**2 + 60*tt**3))) + me2**2*(4*Mm2**4*(2*s2nm*s3mm + s1mm*(s2nm&
                & - 2*s3nm)) - 2*Mm2**3*((11*s1mm*s2nm + 5*s2nm*s3mm + s1mm*s3nm - 6*s3mm*s3nm)*s&
                &s + 8*s2nm*s3mm*tt - 2*s1mm*(s2nm + 4*s3nm)*tt) + Mm2**2*((7*s1mm*(s2nm + s3nm) &
                &- s3mm*(5*s2nm + 2*s3nm))*ss**2 + (-33*s1mm*s2nm + 49*s2nm*s3mm - 53*s1mm*s3nm +&
                & 4*s3mm*s3nm)*ss*tt + 4*(3*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**2) - Mm2*(&
                &(-5*s2nm*s3mm + s1mm*(s2nm - 28*s3nm) + 33*s3mm*s3nm)*ss**3 + (-2*s1mm*(5*s2nm +&
                & 62*s3nm) + s3mm*(43*s2nm + 81*s3nm))*ss**2*tt + (s1mm*(9*s2nm - 16*s3nm) + 4*s3&
                &mm*(s2nm + 3*s3nm))*ss*tt**2 + 20*(-4*s2nm*s3mm + s1mm*(s2nm + 4*s3nm))*tt**3) +&
                & (ss + tt)*(s1mm*(ss + tt)*(4*s2nm*ss*(ss + tt) + s3nm*(-19*ss**2 - 33*ss*tt + 4&
                &0*tt**2)) + s3mm*(s3nm*ss*(23*ss**2 + 43*ss*tt - 4*tt**2) - s2nm*(4*ss**3 - 9*ss&
                &**2*tt + 3*ss*tt**2 + 40*tt**3)))))/(2.*me2*ss*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*m&
                &e2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_ce = (8*me2**6*(5*s2nm*s3mm + s1mm*(s2nm - 5*s3nm)) - 4*me2**5*(4*Mm2*(4*s1mm*s2nm + &
               &3*s2nm*s3mm - 3*s1mm*s3nm) + 7*s1mm*(s2nm - 5*s3nm)*ss + s3mm*(3*s2nm + 32*s3nm)&
               &*ss + 4*(2*s1mm*s2nm + 9*s2nm*s3mm - 9*s1mm*s3nm)*tt) + 4*me2**4*(20*Mm2**2*s1mm&
               &*s2nm + Mm2*(11*s1mm*s2nm + 9*s2nm*s3mm - s1mm*s3nm - 8*s3mm*s3nm)*ss + 2*(5*s1m&
               &m*s2nm - 3*s2nm*s3mm - 23*s1mm*s3nm + 26*s3mm*s3nm)*ss**2 + 16*Mm2*(-(s2nm*s3mm)&
               & + s1mm*(2*s2nm + s3nm))*tt + (19*s1mm*s2nm + 35*s2nm*s3mm - 107*s1mm*s3nm + 72*&
               &s3mm*s3nm)*ss*tt + 12*(4*s2nm*s3mm + s1mm*(s2nm - 4*s3nm))*tt**2) - ss**2*(-(Mm2&
               &*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2n&
               &m*tt))*(2*Mm2**3 - 3*Mm2**2*(ss + 2*tt) - (ss + tt)**2*(ss + 2*tt) + Mm2*(3*ss**&
               &2 + 8*ss*tt + 6*tt**2)) + 2*me2**3*(Mm2**3*(-8*s2nm*s3mm + 8*s1mm*s3nm) + (-14*s&
               &1mm*s2nm + 13*s2nm*s3mm + 67*s1mm*s3nm - 80*s3mm*s3nm)*ss**3 - 2*(17*s1mm*s2nm +&
               & 14*s2nm*s3mm - 114*s1mm*s3nm + 100*s3mm*s3nm)*ss**2*tt - 2*(17*s1mm*s2nm + 61*s&
               &2nm*s3mm - 109*s1mm*s3nm + 48*s3mm*s3nm)*ss*tt**2 - 8*(2*s1mm*s2nm + 7*s2nm*s3mm&
               & - 7*s1mm*s3nm)*tt**3 + Mm2**2*((6*s2nm*s3mm - 26*s1mm*(s2nm - s3nm) - 32*s3mm*s&
               &3nm)*ss + 8*(6*s1mm*s2nm - 5*s2nm*s3mm + 5*s1mm*s3nm)*tt) - 4*Mm2*((3*s3mm*(s2nm&
               & - 6*s3nm) + s1mm*(s2nm + 15*s3nm))*ss**2 + (17*s1mm*s2nm - 13*s2nm*s3mm + 45*s1&
               &mm*s3nm - 32*s3mm*s3nm)*ss*tt + 2*(4*s1mm*s2nm - 13*s2nm*s3mm + 13*s1mm*s3nm)*tt&
               &**2)) + me2**2*(-24*Mm2**4*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + (9*s1mm*s2nm - &
               &9*s2nm*s3mm - 56*s1mm*s3nm + 65*s3mm*s3nm)*ss**4 + (26*s1mm*s2nm + 23*s2nm*s3mm &
               &- 233*s1mm*s3nm + 210*s3mm*s3nm)*ss**3*tt + 2*(14*s1mm*s2nm + 71*s2nm*s3mm - 159&
               &*s1mm*s3nm + 88*s3mm*s3nm)*ss**2*tt**2 + 4*(5*s1mm*s2nm + 33*s2nm*s3mm - 41*s1mm&
               &*s3nm + 8*s3mm*s3nm)*ss*tt**3 + 8*(3*s2nm*s3mm + s1mm*(s2nm - 3*s3nm))*tt**4 + 4&
               &*Mm2**3*((5*s1mm*s2nm - 5*s2nm*s3mm + 13*s1mm*s3nm - 8*s3mm*s3nm)*ss + 8*(2*s1mm&
               &*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt) - 2*Mm2**2*((5*s1mm*s2nm - 13*s2nm*s3mm +&
               & 53*s1mm*s3nm - 40*s3mm*s3nm)*ss**2 + 2*(29*s1mm*s2nm - 43*s2nm*s3mm + 67*s1mm*s&
               &3nm - 24*s3mm*s3nm)*ss*tt + 24*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm))*tt**2) + Mm&
               &2*((-3*s3mm*(s2nm + 38*s3nm) + s1mm*(7*s2nm + 117*s3nm))*ss**3 + 2*(-64*s3mm*(s2&
               &nm + 2*s3nm) + 3*s1mm*(13*s2nm + 64*s3nm))*ss**2*tt + 4*(19*s1mm*(s2nm + 5*s3nm)&
               & - s3mm*(71*s2nm + 24*s3nm))*ss*tt**2 + 96*(-(s2nm*s3mm) + s1mm*s3nm)*tt**3)) - &
               &me2*ss*(-16*Mm2**4*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + 4*Mm2**3*((5*s1mm*s2nm &
               &- 5*s2nm*s3mm + 9*s1mm*s3nm - 4*s3mm*s3nm)*ss + 4*(3*s1mm*s2nm - 4*s2nm*s3mm + 4&
               &*s1mm*s3nm)*tt) - 2*Mm2**2*((-2*s3mm*(5*s2nm + 7*s3nm) + 3*s1mm*(3*s2nm + 8*s3nm&
               &))*ss**2 + 4*(8*s1mm*s2nm - 11*s2nm*s3mm + 17*s1mm*s3nm - 6*s3mm*s3nm)*ss*tt + 2&
               &4*(-2*s2nm*s3mm + s1mm*(s2nm + 2*s3nm))*tt**2) + 2*Mm2*((3*s1mm*s2nm - 3*s2nm*s3&
               &mm + 19*s1mm*s3nm - 16*s3mm*s3nm)*ss**3 + 4*(4*s1mm*s2nm - 7*s2nm*s3mm + 17*s1mm&
               &*s3nm - 10*s3mm*s3nm)*ss**2*tt + 2*(11*s1mm*s2nm - 29*s2nm*s3mm + 41*s1mm*s3nm -&
               & 12*s3mm*s3nm)*ss*tt**2 + 8*(-4*s2nm*s3mm + s1mm*(s2nm + 4*s3nm))*tt**3) + (ss +&
               & 2*tt)*(s1mm*(ss + tt)*(s2nm*ss**2 - 4*s3nm*(3*ss**2 + 5*ss*tt + 2*tt**2)) + s3m&
               &m*(s3nm*ss*(13*ss**2 + 22*ss*tt + 8*tt**2) + s2nm*(-ss**3 + 10*ss**2*tt + 20*ss*&
               &tt**2 + 8*tt**3)))))/(4.*me2*(4*me2 - ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(M&
               &m2 + tt) + tt*(ss + tt))**2)
    coeff_cm = -(1./4.)*(-4*me2**4*(s2nm*s3mm - s1mm*s3nm)*(12*Mm2**2 - 8*Mm2*ss + ss**2) + 2*me2*&
               &*3*(8*Mm2**3*(3*s1mm*s2nm + 4*s2nm*s3mm - 4*s1mm*s3nm) + ss**2*(s1mm*s2nm*ss - 5&
               &*s1mm*s3nm*ss + 5*s3mm*s3nm*ss + 7*s2nm*s3mm*tt - 7*s1mm*s3nm*tt) - 4*Mm2**2*((s&
               &1mm*s2nm + 6*s2nm*s3mm + 8*s1mm*s3nm - 14*s3mm*s3nm)*ss + 20*(-(s2nm*s3mm) + s1m&
               &m*s3nm)*tt) + Mm2*ss*((-5*s1mm*s2nm + 3*s2nm*s3mm + 33*s1mm*s3nm - 36*s3mm*s3nm)&
               &*ss + 56*(-(s2nm*s3mm) + s1mm*s3nm)*tt)) + (-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + &
               &s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(16*Mm2**5 - 48*Mm2**4*(&
               &ss + tt) - ss**2*(ss + tt)**2*(ss + 2*tt) + 16*Mm2**3*(3*ss**2 + 8*ss*tt + 3*tt*&
               &*2) + Mm2*ss*(9*ss**3 + 36*ss**2*tt + 44*ss*tt**2 + 16*tt**3) - Mm2**2*(29*ss**3&
               & + 106*ss**2*tt + 96*ss*tt**2 + 16*tt**3)) - me2**2*(80*Mm2**4*s1mm*s2nm + ss**2&
               &*(3*(s1mm*s2nm - s2nm*s3mm - 4*s1mm*s3nm + 5*s3mm*s3nm)*ss**2 + (4*s1mm*s2nm + 7&
               &*s2nm*s3mm - 29*s1mm*s3nm + 22*s3mm*s3nm)*ss*tt + 18*(s2nm*s3mm - s1mm*s3nm)*tt*&
               &*2) + 4*Mm2**2*((7*s1mm*s2nm - 12*s2nm*s3mm - 36*s1mm*s3nm + 48*s3mm*s3nm)*ss**2&
               & + 4*(-2*s1mm*(s2nm + 11*s3nm) + s3mm*(7*s2nm + 15*s3nm))*ss*tt + 48*(s2nm*s3mm &
               &- s1mm*s3nm)*tt**2) - Mm2*ss*((19*s1mm*s2nm - 23*s2nm*s3mm - 85*s1mm*s3nm + 108*&
               &s3mm*s3nm)*ss**2 + 2*(9*s1mm*s2nm + 29*s2nm*s3mm - 109*s1mm*s3nm + 80*s3mm*s3nm)&
               &*ss*tt + 144*(s2nm*s3mm - s1mm*s3nm)*tt**2) - 16*Mm2**3*(s1mm*s2nm*(2*ss - 7*tt)&
               & - 2*s1mm*s3nm*(ss + 2*tt) + s3mm*(-(s2nm*ss) + 3*s3nm*ss + 4*s2nm*tt))) + me2*(&
               &16*Mm2**5*s1mm*s2nm + 8*Mm2**4*((3*s1mm*s2nm - 2*s2nm*s3mm - 8*s1mm*s3nm + 10*s3&
               &mm*s3nm)*ss + 12*s2nm*s3mm*tt - 12*s1mm*(s2nm + s3nm)*tt) - 2*Mm2**3*((5*s1mm*s2&
               &nm + s2nm*s3mm - 69*s1mm*s3nm + 68*s3mm*s3nm)*ss**2 - 8*(8*s1mm*s2nm - 9*s2nm*s3&
               &mm + 23*s1mm*s3nm - 14*s3mm*s3nm)*ss*tt - 8*(5*s1mm*s2nm - 12*s2nm*s3mm + 12*s1m&
               &m*s3nm)*tt**2) - 2*Mm2*ss*((3*s1mm*s2nm - 3*s2nm*s3mm - 25*s1mm*s3nm + 28*s3mm*s&
               &3nm)*ss**3 + (7*s1mm*s2nm + 11*s2nm*s3mm - 91*s1mm*s3nm + 80*s3mm*s3nm)*ss**2*tt&
               & + (3*s1mm*s2nm + 55*s2nm*s3mm - 107*s1mm*s3nm + 52*s3mm*s3nm)*ss*tt**2 + 40*(s2&
               &nm*s3mm - s1mm*s3nm)*tt**3) + 2*Mm2**2*((4*s1mm*s2nm - 3*s2nm*s3mm - 66*s1mm*s3n&
               &m + 69*s3mm*s3nm)*ss**3 + (-3*s1mm*(4*s2nm + 73*s3nm) + s3mm*(43*s2nm + 176*s3nm&
               &))*ss**2*tt - 4*(5*s1mm*s2nm - 34*s2nm*s3mm + 52*s1mm*s3nm - 18*s3mm*s3nm)*ss*tt&
               &**2 + 48*(s2nm*s3mm - s1mm*s3nm)*tt**3) + ss**2*(s1mm*(ss + tt)*(s2nm*ss*(ss + 2&
               &*tt) - 2*s3nm*(3*ss**2 + 8*ss*tt + 5*tt**2)) + s3mm*(s3nm*ss*(7*ss**2 + 20*ss*tt&
               & + 14*tt**2) + s2nm*(-ss**3 + 2*ss**2*tt + 12*ss*tt**2 + 10*tt**3)))))/(me2*(4*M&
               &m2 - ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_discse = (32*me2**3*(s2nm*s3mm - s1mm*s3nm) + 2*ss**2*(11*Mm2*(-(s2nm*s3mm) + s1mm*(s2nm &
                   &+ s3nm)) - 3*s1mm*s2nm*ss - 11*s1mm*s3nm*(ss + tt) + 11*s3mm*(s3nm*ss + s2nm*tt)&
                   &) + me2*ss*(Mm2*(-74*s1mm*s2nm + 68*s2nm*s3mm - 68*s1mm*s3nm) + s3mm*(5*s2nm*ss &
                   &- 80*s3nm*ss - 68*s2nm*tt) + s1mm*(3*s2nm*ss + 75*s3nm*ss - 6*s2nm*tt + 68*s3nm*&
                   &tt)) + me2**2*(-32*Mm2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + s1mm*(6*s2nm*ss + 3&
                   &2*s3nm*tt) - 32*s3mm*(s3nm*ss + s2nm*(-ss + tt))))/(12.*me2*(4*me2 - ss)*ss**2)
    coeff_discse_lmm = ((2*me2 - ss)*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(s2&
                       &nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*(4&
                       &*me2 - ss)*ss)
    coeff_discse_lmu = -(((2*me2 - ss)*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(&
                       &s2nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*&
                       &(4*me2 - ss)*ss))
    coeff_discsm = -(1./6.)*(16*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + Mm2*ss*((-&
                   &17*s1mm*s2nm + 11*s2nm*s3mm - 39*s1mm*s3nm + 28*s3mm*s3nm)*ss - 4*(s2nm*s3mm - s&
                   &1mm*s3nm)*(4*me2 - 7*tt)) + 4*Mm2**2*((7*s1mm*s2nm - 7*s2nm*s3mm + 3*s1mm*s3nm +&
                   & 4*s3mm*s3nm)*ss - 4*(s2nm*s3mm - s1mm*s3nm)*(me2 - tt)) + ss**2*(2*me2*(s2nm*s3&
                   &mm - s1mm*s3nm) + 3*s1mm*s2nm*ss + 11*s1mm*s3nm*(ss + tt) - 11*s3mm*(s3nm*ss + s&
                   &2nm*tt)))/(me2*(4*Mm2 - ss)*ss**2)
    coeff_discsm_lmu = -(((2*Mm2 - ss)*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(&
                       &s2nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*&
                       &(4*Mm2 - ss)*ss))
    coeff_disct = (2*me2**5*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) + (s1mm - s3mm)*s3nm*ss) - &
                  &me2**4*(10*Mm2**2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + 2*Mm2*(3*(s1mm - s3mm)*s&
                  &3nm*ss + s1mm*s2nm*tt) + tt*((-(s1mm*s2nm) + s2nm*s3mm + 5*s1mm*s3nm - 6*s3mm*s3&
                  &nm)*ss + 14*(-(s2nm*s3mm) + s1mm*s3nm)*tt)) + me2**2*(-20*Mm2**4*(-(s2nm*s3mm) +&
                  & s1mm*(s2nm + s3nm)) - 8*Mm2*tt**2*(s3mm*(s2nm + 2*s3nm)*ss - s1mm*(s2nm + 3*s3n&
                  &m)*ss + 4*s2nm*s3mm*tt + s1mm*(s2nm - 4*s3nm)*tt) + 4*Mm2**3*((s1mm - s3mm)*s3nm&
                  &*ss - 6*s2nm*s3mm*tt + 3*s1mm*(s2nm + 2*s3nm)*tt) + Mm2**2*tt*((3*s1mm*s2nm - 3*&
                  &s2nm*s3mm - s1mm*s3nm + 4*s3mm*s3nm)*ss + 20*(-(s2nm*s3mm) + s1mm*s3nm)*tt) + tt&
                  &**2*(3*(s1mm*s2nm - s2nm*s3mm - s1mm*s3nm + 2*s3mm*s3nm)*ss**2 + (19*s1mm*s2nm +&
                  & 3*s2nm*s3mm - 3*s1mm*s3nm)*ss*tt + 24*(s2nm*s3mm - s1mm*s3nm)*tt**2)) + me2**3*&
                  &(20*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + Mm2*tt*((-3*s1mm*s2nm + 3*s2nm*&
                  &s3mm + 5*s1mm*s3nm - 8*s3mm*s3nm)*ss - 8*s2nm*s3mm*tt + 8*s1mm*s3nm*tt) + tt**2*&
                  &((-9*s1mm*s2nm + 7*s2nm*s3mm + s1mm*s3nm - 8*s3mm*s3nm)*ss + 36*(-(s2nm*s3mm) + &
                  &s1mm*s3nm)*tt) + 4*Mm2**2*(s1mm*s3nm*(ss - 2*tt) + s3mm*(-(s3nm*ss) + 2*s2nm*tt)&
                  &)) + me2*(10*Mm2**5*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + Mm2*tt**2*((-3*s1mm*s2&
                  &nm + 3*s2nm*s3mm - 7*s1mm*s3nm + 4*s3mm*s3nm)*ss**2 + (13*s1mm*s2nm + 11*s2nm*s3&
                  &mm - 11*s1mm*s3nm)*ss*tt + 2*(7*s1mm*s2nm + 9*s2nm*s3mm - 9*s1mm*s3nm)*tt**2) - &
                  &tt**3*((7*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*ss**2 + (15*s1mm*s2nm + 3*s2nm*&
                  &s3mm + 7*s1mm*s3nm - 10*s3mm*s3nm)*ss*tt + 4*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2) -&
                  & 2*Mm2**4*(8*s1mm*s2nm*tt + 3*s1mm*s3nm*(ss + 4*tt) - 3*s3mm*(s3nm*ss + 4*s2nm*t&
                  &t)) + Mm2**3*tt*(-8*s3mm*s3nm*ss + s2nm*s3mm*(ss + 8*tt) - s1mm*(s2nm*ss - 7*s3n&
                  &m*ss + 8*s3nm*tt)) - Mm2**2*tt**2*(8*s3mm*s3nm*ss + s2nm*s3mm*(ss + 44*tt) - s1m&
                  &m*(7*s2nm*ss + 9*s3nm*ss - 8*s2nm*tt + 44*s3nm*tt))) - 2*(Mm2 - tt)*(Mm2**5*(-(s&
                  &2nm*s3mm) + s1mm*(s2nm + s3nm)) - 2*Mm2**3*tt*((-s1mm + s3mm)*s3nm*ss + s1mm*s2n&
                  &m*tt) + Mm2**2*tt**2*(3*s1mm*(s2nm + s3nm)*ss - s3mm*(s2nm + 2*s3nm)*ss + 2*(3*s&
                  &1mm*s2nm - 4*s2nm*s3mm + 4*s1mm*s3nm)*tt) + tt**3*(ss + tt)*(2*s1mm*s2nm*ss + 3*&
                  &s1mm*s3nm*(ss + tt) - 3*s3mm*(s3nm*ss + s2nm*tt)) + Mm2**4*(s3mm*(s3nm*ss + 3*s2&
                  &nm*tt) - s1mm*(2*s2nm*tt + s3nm*(ss + 3*tt))) + Mm2*tt**2*(s3mm*(s3nm*ss*(ss + 6&
                  &*tt) + s2nm*tt*(4*ss + 9*tt)) - s1mm*(s2nm*tt*(7*ss + 3*tt) + s3nm*(ss**2 + 10*s&
                  &s*tt + 9*tt**2)))))/(4.*me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2)*tt*(me2**2 + M&
                  &m2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt)))
    coeff_disct_lmm = -(1./4.)*(tt*(-12*me2**6*(s2nm*s3mm - s1mm*s3nm) + me2**5*(6*Mm2*(5*s2nm*s3mm + s1m&
                      &m*(s2nm - 5*s3nm)) + (s1mm*s2nm - s2nm*s3mm - 9*s1mm*s3nm + 10*s3mm*s3nm)*ss + 6&
                      &6*(s2nm*s3mm - s1mm*s3nm)*tt) - me2**4*(6*Mm2**2*(3*s1mm*s2nm + s2nm*s3mm - s1mm&
                      &*s3nm) + Mm2*(3*s1mm*s2nm - 3*s2nm*s3mm - 23*s1mm*s3nm + 26*s3mm*s3nm)*ss + (s1m&
                      &m*s2nm - s2nm*s3mm - 4*s1mm*s3nm + 5*s3mm*s3nm)*ss**2 + 6*Mm2*(5*s1mm*s2nm + 6*s&
                      &2nm*s3mm - 6*s1mm*s3nm)*tt + (4*s1mm*s2nm + 19*s2nm*s3mm - 65*s1mm*s3nm + 46*s3m&
                      &m*s3nm)*ss*tt + 150*(s2nm*s3mm - s1mm*s3nm)*tt**2) + (Mm2 - tt)*(-(Mm2*s2nm*s3mm&
                      &) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(6*&
                      &Mm2**4 + 6*tt**2*(ss + tt)**2 - Mm2**3*(ss + 24*tt) - Mm2*tt*(2*ss**2 + 25*ss*tt&
                      & + 24*tt**2) + Mm2**2*(ss**2 + 14*ss*tt + 36*tt**2)) + me2**2*(12*Mm2**4*(2*s2nm&
                      &*s3mm + s1mm*(s2nm - 2*s3nm)) + 4*Mm2**3*(s3mm*(s2nm + s3nm)*ss - s1mm*(s2nm + 2&
                      &*s3nm)*ss - 12*s2nm*s3mm*tt + 3*s1mm*(s2nm + 4*s3nm)*tt) + Mm2**2*(2*(s1mm - s3m&
                      &m)*s3nm*ss**2 + (-11*s1mm*s2nm + 35*s2nm*s3mm - 47*s1mm*s3nm + 12*s3mm*s3nm)*ss*&
                      &tt + 12*(3*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**2) + tt*(3*(s1mm - s3mm)*s&
                      &3nm*ss**3 - (3*s1mm*s2nm + 10*s2nm*s3mm - 61*s1mm*s3nm + 51*s3mm*s3nm)*ss**2*tt &
                      &- (4*s1mm*s2nm + 101*s2nm*s3mm - 177*s1mm*s3nm + 76*s3mm*s3nm)*ss*tt**2 + 120*(-&
                      &(s2nm*s3mm) + s1mm*s3nm)*tt**3) + Mm2*((s1mm - s3mm)*s3nm*ss**3 + (-3*s1mm*s2nm &
                      &+ 2*s2nm*s3mm + 13*s1mm*s3nm - 15*s3mm*s3nm)*ss**2*tt + (-33*s1mm*s2nm + 70*s2nm&
                      &*s3mm - 130*s1mm*s3nm + 60*s3mm*s3nm)*ss*tt**2 - 60*(-4*s2nm*s3mm + s1mm*(s2nm +&
                      & 4*s3nm))*tt**3)) + me2**3*(12*Mm2**3*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm)) + (-&
                      &s1mm + s3mm)*s3nm*ss**3 + (3*s1mm*s2nm - 2*s2nm*s3mm - 25*s1mm*s3nm + 27*s3mm*s3&
                      &nm)*ss**2*tt + 3*(2*s1mm*s2nm + 25*s2nm*s3mm - 53*s1mm*s3nm + 28*s3mm*s3nm)*ss*t&
                      &t**2 + 180*(s2nm*s3mm - s1mm*s3nm)*tt**3 + Mm2*(2*(s1mm*s2nm - s2nm*s3mm - 3*s1m&
                      &m*s3nm + 4*s3mm*s3nm)*ss**2 + (15*s1mm*s2nm + 7*s2nm*s3mm - 31*s1mm*s3nm + 24*s3&
                      &mm*s3nm)*ss*tt + 12*(5*s1mm*s2nm - 9*s2nm*s3mm + 9*s1mm*s3nm)*tt**2) + 4*Mm2**2*&
                      &(5*s3mm*s3nm*ss - s2nm*s3mm*(ss + 9*tt) + s1mm*(-4*s3nm*ss + 9*s3nm*tt + s2nm*(s&
                      &s + 6*tt)))) + me2*(6*Mm2**5*(s2nm*s3mm - s1mm*(3*s2nm + s3nm)) + Mm2**4*((3*s1m&
                      &m*s2nm - 3*s2nm*s3mm + 17*s1mm*s3nm - 14*s3mm*s3nm)*ss + 6*(4*s1mm*s2nm + 3*s2nm&
                      &*s3mm - 3*s1mm*s3nm)*tt) - Mm2**3*(2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm))*ss**2 +&
                      & (15*s1mm*s2nm + 7*s2nm*s3mm + s1mm*s3nm - 8*s3mm*s3nm)*ss*tt - 12*(3*s1mm*s2nm &
                      &- 11*s2nm*s3mm + 11*s1mm*s3nm)*tt**2) + Mm2**2*((s1mm - s3mm)*s3nm*ss**3 + (3*s1&
                      &mm*s2nm - 4*s2nm*s3mm + 7*s1mm*s3nm - 3*s3mm*s3nm)*ss**2*tt + (-22*s1mm*s2nm + 8&
                      &1*s2nm*s3mm - 141*s1mm*s3nm + 60*s3mm*s3nm)*ss*tt**2 - 12*(6*s1mm*s2nm - 19*s2nm&
                      &*s3mm + 19*s1mm*s3nm)*tt**3) + Mm2*tt*(2*(-s1mm + s3mm)*s3nm*ss**3 + (5*s1mm*s2n&
                      &m - 17*s2nm*s3mm + 55*s1mm*s3nm - 38*s3mm*s3nm)*ss**2*tt + (33*s1mm*s2nm - 129*s&
                      &2nm*s3mm + 217*s1mm*s3nm - 88*s3mm*s3nm)*ss*tt**2 + 6*(5*s1mm*s2nm - 27*s2nm*s3m&
                      &m + 27*s1mm*s3nm)*tt**3) + tt**2*(-(s1mm*(ss + tt)*(-(s2nm*ss*tt) + s3nm*(8*ss**&
                      &2 + 50*ss*tt + 42*tt**2))) + s3mm*(s3nm*ss*(8*ss**2 + 41*ss*tt + 34*tt**2) + s2n&
                      &m*tt*(17*ss**2 + 58*ss*tt + 42*tt**2))))))/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt&
                      &)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_disct_lmu = (2*(me2 + Mm2 - tt)*tt*(-2*me2*s2nm*s3mm - Mm2*s2nm*s3mm + 2*me2*s1mm*s3nm + Mm2&
                      &*s1mm*(s2nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + s2nm*s3mm*tt - s1mm*s3nm*tt)&
                      &)/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2))
    coeff_disct_ls = (tt*(me2**6*(-4*s2nm*s3mm + 4*s1mm*s3nm) + me2**5*(2*Mm2*(5*s2nm*s3mm + s1mm*(s2&
                     &nm - 5*s3nm)) + (s1mm*s2nm - s2nm*s3mm - 5*s1mm*s3nm + 6*s3mm*s3nm)*ss + 22*(s2n&
                     &m*s3mm - s1mm*s3nm)*tt) - me2**4*(2*Mm2**2*(3*s1mm*s2nm + s2nm*s3mm - s1mm*s3nm)&
                     & + Mm2*(3*s1mm*s2nm - 3*s2nm*s3mm - 11*s1mm*s3nm + 14*s3mm*s3nm)*ss + (s1mm*s2nm&
                     & - s2nm*s3mm - 4*s1mm*s3nm + 5*s3mm*s3nm)*ss**2 + 2*Mm2*(5*s1mm*s2nm + 6*s2nm*s3&
                     &mm - 6*s1mm*s3nm)*tt + (4*s1mm*s2nm + 3*s2nm*s3mm - 29*s1mm*s3nm + 26*s3mm*s3nm)&
                     &*ss*tt + 50*(s2nm*s3mm - s1mm*s3nm)*tt**2) + (Mm2 - tt)*(-(Mm2*s2nm*s3mm) + Mm2*&
                     &s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*tt))*(2*Mm2**4 +&
                     & 2*tt**2*(ss + tt)**2 - Mm2**3*(ss + 8*tt) - Mm2*tt*(2*ss**2 + 9*ss*tt + 8*tt**2&
                     &) + Mm2**2*(ss**2 + 6*ss*tt + 12*tt**2)) + me2**2*(4*Mm2**4*(2*s2nm*s3mm + s1mm*&
                     &(s2nm - 2*s3nm)) - 4*Mm2**3*(s1mm*s2nm*ss + s3mm*(-s2nm + s3nm)*ss + 4*s2nm*s3mm&
                     &*tt - s1mm*(s2nm + 4*s3nm)*tt) + Mm2**2*(2*(s1mm - s3mm)*s3nm*ss**2 + (s3mm*(11*&
                     &s2nm + 4*s3nm) - 3*s1mm*(s2nm + 5*s3nm))*ss*tt + 4*(3*s1mm*s2nm - 8*s2nm*s3mm + &
                     &8*s1mm*s3nm)*tt**2) + tt*(3*(s1mm - s3mm)*s3nm*ss**3 - (3*s1mm*s2nm + 2*s2nm*s3m&
                     &m - 29*s1mm*s3nm + 27*s3mm*s3nm)*ss**2*tt - (4*s1mm*s2nm + 29*s2nm*s3mm - 65*s1m&
                     &m*s3nm + 36*s3mm*s3nm)*ss*tt**2 + 40*(-(s2nm*s3mm) + s1mm*s3nm)*tt**3) + Mm2*((s&
                     &1mm - s3mm)*s3nm*ss**3 + (-3*s1mm*s2nm + 2*s2nm*s3mm + 5*s1mm*s3nm - 7*s3mm*s3nm&
                     &)*ss**2*tt + (-9*s1mm*s2nm + 22*s2nm*s3mm - 58*s1mm*s3nm + 36*s3mm*s3nm)*ss*tt**&
                     &2 - 20*(-4*s2nm*s3mm + s1mm*(s2nm + 4*s3nm))*tt**3)) + me2**3*(4*Mm2**3*(-3*s2nm&
                     &*s3mm + s1mm*(s2nm + 3*s3nm)) + (-s1mm + s3mm)*s3nm*ss**3 + (3*s1mm*s2nm - 2*s2n&
                     &m*s3mm - 17*s1mm*s3nm + 19*s3mm*s3nm)*ss**2*tt + (6*s1mm*s2nm + 19*s2nm*s3mm - 6&
                     &3*s1mm*s3nm + 44*s3mm*s3nm)*ss*tt**2 + 60*(s2nm*s3mm - s1mm*s3nm)*tt**3 + Mm2*(2&
                     &*(s1mm*s2nm - s2nm*s3mm - 3*s1mm*s3nm + 4*s3mm*s3nm)*ss**2 + (7*s1mm*s2nm - s2nm&
                     &*s3mm - 7*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt + 4*(5*s1mm*s2nm - 9*s2nm*s3mm + 9*s1mm&
                     &*s3nm)*tt**2) + 4*Mm2**2*(3*s3mm*s3nm*ss - s2nm*s3mm*(ss + 3*tt) + s1mm*(-2*s3nm&
                     &*ss + 3*s3nm*tt + s2nm*(ss + 2*tt)))) + me2*(2*Mm2**5*(s2nm*s3mm - s1mm*(3*s2nm &
                     &+ s3nm)) + Mm2**3*(-2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm))*ss**2 + (-7*s1mm*s2nm &
                     &+ s2nm*s3mm + 7*s1mm*s3nm - 8*s3mm*s3nm)*ss*tt + 4*(3*s1mm*s2nm - 11*s2nm*s3mm +&
                     & 11*s1mm*s3nm)*tt**2) + Mm2**2*((s1mm - s3mm)*s3nm*ss**3 + (3*s1mm*s2nm - 4*s2nm&
                     &*s3mm - s1mm*s3nm + 5*s3mm*s3nm)*ss**2*tt + (-6*s1mm*s2nm + 25*s2nm*s3mm - 61*s1&
                     &mm*s3nm + 36*s3mm*s3nm)*ss*tt**2 - 4*(6*s1mm*s2nm - 19*s2nm*s3mm + 19*s1mm*s3nm)&
                     &*tt**3) + Mm2*tt*(2*(-s1mm + s3mm)*s3nm*ss**3 + (s1mm*s2nm - 5*s2nm*s3mm + 27*s1&
                     &mm*s3nm - 22*s3mm*s3nm)*ss**2*tt + (9*s1mm*(s2nm + 9*s3nm) - s3mm*(41*s2nm + 40*&
                     &s3nm))*ss*tt**2 + 2*(5*s1mm*s2nm - 27*s2nm*s3mm + 27*s1mm*s3nm)*tt**3) + Mm2**4*&
                     &(s3mm*(-3*s2nm*ss - 2*s3nm*ss + 6*s2nm*tt) + s1mm*(3*s2nm*ss + 5*s3nm*ss + 8*s2n&
                     &m*tt - 6*s3nm*tt)) + tt**2*(-(s1mm*(ss + tt)*(-(s2nm*ss*tt) + 2*s3nm*(2*ss**2 + &
                     &9*ss*tt + 7*tt**2))) + s3mm*(s3nm*ss*(4*ss**2 + 17*ss*tt + 14*tt**2) + s2nm*tt*(&
                     &5*ss**2 + 18*ss*tt + 14*tt**2))))))/(2.*me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - tt)**2&
                     &)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_discu = -(1./2.)*(-20*me2**5*(s2nm*s3mm - s1mm*s3nm) - 2*me2**4*(-5*Mm2*s2nm*s3mm + Mm2*s1mm&
                  &*(s2nm + 5*s3nm) - 3*s1mm*(s2nm - 6*s3nm)*ss - 2*s3mm*(8*s2nm + s3nm)*ss + 35*(-&
                  &(s2nm*s3mm) + s1mm*s3nm)*tt) - me2**3*(8*Mm2**2*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s&
                  &3nm)) + (s1mm*(11*s2nm - 27*s3nm) + 9*s3mm*(s2nm + 2*s3nm))*ss**2 + (19*s1mm*s2n&
                  &m + 109*s2nm*s3mm - 117*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt + 90*(s2nm*s3mm - s1mm*s3&
                  &nm)*tt**2 - 2*Mm2*(9*s3mm*(-2*s2nm + s3nm)*ss + 3*s1mm*(s2nm + 3*s3nm)*ss + (3*s&
                  &1mm*s2nm + 31*s2nm*s3mm - 31*s1mm*s3nm)*tt)) - me2*(4*Mm2**4*(2*s1mm*s2nm + 5*s2&
                  &nm*s3mm - 5*s1mm*s3nm) + 2*Mm2**3*(s3mm*(-22*s2nm + s3nm)*ss + 3*s1mm*(s2nm + 7*&
                  &s3nm)*ss + (-13*s1mm*s2nm - 29*s2nm*s3mm + 29*s1mm*s3nm)*tt) + 2*Mm2*(ss + tt)*(&
                  &(2*s1mm*s2nm + 13*s1mm*s3nm - 13*s3mm*s3nm)*ss**2 + (-15*s1mm*s2nm - 37*s2nm*s3m&
                  &m + 32*s1mm*s3nm + 5*s3mm*s3nm)*ss*tt - (19*s2nm*s3mm + s1mm*(s2nm - 19*s3nm))*t&
                  &t**2) + Mm2**2*((-11*s1mm*s2nm + 19*s2nm*s3mm - 45*s1mm*s3nm + 26*s3mm*s3nm)*ss*&
                  &*2 + 3*(11*s1mm*s2nm + 41*s2nm*s3mm - 37*s1mm*s3nm - 4*s3mm*s3nm)*ss*tt + 2*(10*&
                  &s1mm*s2nm + 33*s2nm*s3mm - 33*s1mm*s3nm)*tt**2) + (ss + tt)**2*((s1mm*s2nm - s2n&
                  &m*s3mm - 7*s1mm*s3nm + 8*s3mm*s3nm)*ss**2 + (11*s1mm*s2nm + 17*s2nm*s3mm - 17*s1&
                  &mm*s3nm)*ss*tt + 10*(s2nm*s3mm - s1mm*s3nm)*tt**2)) - (Mm2 - ss - tt)*(2*Mm2**4*&
                  &(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) - 2*Mm2**3*(s1mm*s2nm*ss + s3mm*(s2nm - s3nm&
                  &)*ss + (2*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt) + Mm2*(ss + tt)*((-(s3mm*(s&
                  &2nm + 3*s3nm)) + s1mm*(s2nm + 4*s3nm))*ss**2 + (-7*s1mm*s2nm - 4*s2nm*s3mm + 2*s&
                  &1mm*s3nm + 2*s3mm*s3nm)*ss*tt + 2*(s2nm*s3mm - s1mm*s3nm)*tt**2) + Mm2**2*((s3mm&
                  &*(3*s2nm + 2*s3nm) - s1mm*(s2nm + 5*s3nm))*ss**2 + (9*s1mm*s2nm + 3*s2nm*s3mm + &
                  &s1mm*s3nm - 4*s3mm*s3nm)*ss*tt + 2*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm))*tt**2) &
                  &- ss*(ss + tt)**2*(-2*s1mm*s2nm*tt + s1mm*s3nm*(ss + tt) - s3mm*(s3nm*ss + s2nm*&
                  &tt))) + me2**2*(4*Mm2**3*(5*s1mm*s2nm + s2nm*s3mm - s1mm*s3nm) + Mm2*((s1mm*s2nm&
                  & + 33*s2nm*s3mm + 19*s1mm*s3nm - 52*s3mm*s3nm)*ss**2 - (29*s1mm*s2nm + 121*s2nm*&
                  &s3mm - 131*s1mm*s3nm + 10*s3mm*s3nm)*ss*tt - 2*(3*s1mm*s2nm + 56*s2nm*s3mm - 56*&
                  &s1mm*s3nm)*tt**2) + (ss + tt)*((6*s1mm*s2nm - 4*s2nm*s3mm - 17*s1mm*s3nm + 21*s3&
                  &mm*s3nm)*ss**2 + (22*s1mm*s2nm + 63*s2nm*s3mm - 67*s1mm*s3nm + 4*s3mm*s3nm)*ss*t&
                  &t + 50*(s2nm*s3mm - s1mm*s3nm)*tt**2) - 2*Mm2**2*(s3mm*(20*s2nm*ss - 23*s3nm*ss &
                  &- 37*s2nm*tt) + s1mm*(5*s2nm*ss + 3*s3nm*ss - 13*s2nm*tt + 37*s3nm*tt))))/(me2*s&
                  &s*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(M&
                  &m2 + tt) + tt*(ss + tt)))
    coeff_discu_lmm = -(1./4.)*((2*me2 + 2*Mm2 - ss - tt)*(-12*me2**6*(s2nm*s3mm - s1mm*s3nm) + me2**5*(3&
                      &0*Mm2*s2nm*s3mm + 6*Mm2*s1mm*(s2nm - 5*s3nm) + s1mm*(s2nm - 25*s3nm)*ss + 5*s3mm&
                      &*(3*s2nm + 2*s3nm)*ss + 66*(s2nm*s3mm - s1mm*s3nm)*tt) - me2**4*(6*Mm2**2*(3*s1m&
                      &m*s2nm + s2nm*s3mm - s1mm*s3nm) + Mm2*(13*s1mm*s2nm + 35*s2nm*s3mm - 61*s1mm*s3n&
                      &m + 26*s3mm*s3nm)*ss + (4*s1mm*s2nm + 2*s2nm*s3mm - 27*s1mm*s3nm + 25*s3mm*s3nm)&
                      &*ss**2 + 6*Mm2*(5*s1mm*s2nm + 6*s2nm*s3mm - 6*s1mm*s3nm)*tt + (4*s1mm*s2nm + 91*&
                      &s2nm*s3mm - 137*s1mm*s3nm + 46*s3mm*s3nm)*ss*tt + 150*(s2nm*s3mm - s1mm*s3nm)*tt&
                      &**2) + (-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(&
                      &s3nm*ss + s2nm*tt))*(6*Mm2**5 - 3*Mm2**4*(3*ss + 10*tt) - (ss + tt)**3*(ss**2 + &
                      &2*ss*tt + 6*tt**2) + Mm2*(ss + tt)**2*(4*ss**2 + 9*ss*tt + 30*tt**2) + Mm2**3*(7&
                      &*ss**2 + 47*ss*tt + 60*tt**2) - Mm2**2*(7*ss**3 + 34*ss**2*tt + 87*ss*tt**2 + 60&
                      &*tt**3)) + me2**3*(12*Mm2**3*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm)) + (6*s1mm*s2n&
                      &m - 4*s2nm*s3mm - 27*s1mm*s3nm + 31*s3mm*s3nm)*ss**3 + (12*s1mm*s2nm + 39*s2nm*s&
                      &3mm - 134*s1mm*s3nm + 95*s3mm*s3nm)*ss**2*tt + (6*s1mm*s2nm + 203*s2nm*s3mm - 28&
                      &7*s1mm*s3nm + 84*s3mm*s3nm)*ss*tt**2 + 180*(s2nm*s3mm - s1mm*s3nm)*tt**3 + Mm2*(&
                      &(5*s1mm*s2nm + 5*s2nm*s3mm - 13*s1mm*s3nm + 8*s3mm*s3nm)*ss**2 + (53*s1mm*(s2nm &
                      &- s3nm) + s3mm*(29*s2nm + 24*s3nm))*ss*tt + 12*(5*s1mm*s2nm - 9*s2nm*s3mm + 9*s1&
                      &mm*s3nm)*tt**2) + Mm2**2*(20*s3mm*s3nm*ss + 6*s2nm*s3mm*(5*ss - 6*tt) + s1mm*(26&
                      &*s2nm*ss - 50*s3nm*ss + 24*s2nm*tt + 36*s3nm*tt))) + me2*(6*Mm2**5*(s2nm*s3mm - &
                      &s1mm*(3*s2nm + s3nm)) - Mm2**3*((s3mm*(s2nm - 8*s3nm) + s1mm*(s2nm + 7*s3nm))*ss&
                      &**2 + (s3mm*(25*s2nm - 8*s3nm) + 17*s1mm*(s2nm - s3nm))*ss*tt - 12*(3*s1mm*s2nm &
                      &- 11*s2nm*s3mm + 11*s1mm*s3nm)*tt**2) + Mm2**2*((2*s2nm*s3mm - 27*s1mm*s3nm + 25&
                      &*s3mm*s3nm)*ss**3 + (29*s2nm*s3mm - 54*s1mm*s3nm + 25*s3mm*s3nm)*ss**2*tt - 3*(2&
                      &4*s1mm*s2nm - 65*s2nm*s3mm + 85*s1mm*s3nm - 20*s3mm*s3nm)*ss*tt**2 - 12*(6*s1mm*&
                      &s2nm - 19*s2nm*s3mm + 19*s1mm*s3nm)*tt**3) + Mm2**4*(s3mm*(-13*s2nm*ss - 14*s3nm&
                      &*ss + 18*s2nm*tt) + 3*s1mm*(7*s2nm*ss + 9*s3nm*ss + 8*s2nm*tt - 6*s3nm*tt)) + Mm&
                      &2*(ss + tt)*(s1mm*(ss + tt)*(s2nm*tt*(7*ss + 30*tt) + s3nm*(22*ss**2 + 27*ss*tt &
                      &+ 162*tt**2)) - s3mm*(2*s3nm*ss*(11*ss**2 + 15*ss*tt + 44*tt**2) + s2nm*tt*(19*s&
                      &s**2 + 101*ss*tt + 162*tt**2))) + (ss + tt)**2*(s1mm*(ss + tt)*(s2nm*ss*(ss + tt&
                      &) - 7*s3nm*(ss**2 + 2*ss*tt + 6*tt**2)) + s3mm*(s3nm*ss*(8*ss**2 + 17*ss*tt + 34&
                      &*tt**2) + s2nm*(-ss**3 + 4*ss**2*tt + 22*ss*tt**2 + 42*tt**3)))) + me2**2*(12*Mm&
                      &2**4*(2*s2nm*s3mm + s1mm*(s2nm - 2*s3nm)) + Mm2**2*((-7*s1mm*s2nm + 5*s2nm*s3mm &
                      &+ 41*s1mm*s3nm - 46*s3mm*s3nm)*ss**2 + (-15*s1mm*s2nm + 47*s2nm*s3mm - 59*s1mm*s&
                      &3nm + 12*s3mm*s3nm)*ss*tt + 12*(3*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3nm)*tt**2) &
                      &+ Mm2**3*(4*s3mm*s3nm*ss - 6*s2nm*s3mm*(ss + 8*tt) + s1mm*(-26*s2nm*ss + 2*s3nm*&
                      &ss + 12*s2nm*tt + 48*s3nm*tt)) + Mm2*(s1mm*(ss + tt)*(s2nm*(ss**2 - 27*ss*tt - 6&
                      &0*tt**2) - 4*s3nm*(7*ss**2 + 60*tt**2)) + s3mm*(3*s3nm*ss*(11*ss**2 + 11*ss*tt +&
                      & 20*tt**2) - 5*s2nm*(ss**3 + ss**2*tt - 36*ss*tt**2 - 48*tt**3))) - (ss + tt)*(s&
                      &1mm*(ss + tt)*(4*s2nm*ss*(ss + tt) - s3nm*(19*ss**2 + 49*ss*tt + 120*tt**2)) + s&
                      &3mm*(s3nm*ss*(23*ss**2 + 59*ss*tt + 76*tt**2) + s2nm*(-4*ss**3 + 9*ss**2*tt + 93&
                      &*ss*tt**2 + 120*tt**3))))))/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)*(me2&
                      &**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_discu_lmu = (2*(me2 + Mm2 - ss - tt)*(2*me2 + 2*Mm2 - ss - tt)*(-2*me2*s2nm*s3mm - Mm2*s2nm*&
                      &s3mm + 2*me2*s1mm*s3nm + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*ss + s3mm*s3nm*ss + &
                      &s2nm*s3mm*tt - s1mm*s3nm*tt))/(me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2))
    coeff_discu_ls = ((2*me2 + 2*Mm2 - ss - tt)*(me2**6*(-4*s2nm*s3mm + 4*s1mm*s3nm) + me2**5*(2*Mm2*&
                     &(5*s2nm*s3mm + s1mm*(s2nm - 5*s3nm)) + (s1mm*s2nm + 7*s2nm*s3mm - 13*s1mm*s3nm +&
                     & 6*s3mm*s3nm)*ss + 22*(s2nm*s3mm - s1mm*s3nm)*tt) - me2**4*(2*Mm2**2*(3*s1mm*s2n&
                     &m + s2nm*s3mm - s1mm*s3nm) + (4*s1mm*s2nm + 2*s2nm*s3mm - 23*s1mm*s3nm + 21*s3mm&
                     &*s3nm)*ss**2 + (4*s1mm*s2nm + 39*s2nm*s3mm - 65*s1mm*s3nm + 26*s3mm*s3nm)*ss*tt &
                     &+ 50*(s2nm*s3mm - s1mm*s3nm)*tt**2 + Mm2*(3*s1mm*(3*s2nm - 7*s3nm)*ss + 7*s3mm*(&
                     &s2nm + 2*s3nm)*ss + 2*(5*s1mm*s2nm + 6*s2nm*s3mm - 6*s1mm*s3nm)*tt)) + (-(Mm2*s2&
                     &nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss + s2nm*t&
                     &t))*(2*Mm2**5 - 5*Mm2**4*(ss + 2*tt) - (ss + tt)**3*(ss**2 + 2*ss*tt + 2*tt**2) &
                     &+ Mm2*(ss + tt)**2*(4*ss**2 + 9*ss*tt + 10*tt**2) + Mm2**3*(7*ss**2 + 23*ss*tt +&
                     & 20*tt**2) - Mm2**2*(7*ss**3 + 26*ss**2*tt + 39*ss*tt**2 + 20*tt**3)) + me2**3*(&
                     &4*Mm2**3*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm)) + 2*Mm2**2*(6*s3mm*s3nm*ss - s2nm&
                     &*s3mm*(ss + 6*tt) + s1mm*(5*s2nm*ss - 5*s3nm*ss + 4*s2nm*tt + 6*s3nm*tt)) + Mm2*&
                     &(8*s3mm*s3nm*ss*(-ss + tt) + s2nm*s3mm*(5*ss**2 - 11*ss*tt - 36*tt**2) + 3*s1mm*&
                     &s3nm*(ss**2 + ss*tt + 12*tt**2) + s1mm*s2nm*(5*ss**2 + 29*ss*tt + 20*tt**2)) + s&
                     &1mm*(ss + tt)*(6*s2nm*ss*(ss + tt) - s3nm*(27*ss**2 + 67*ss*tt + 60*tt**2)) + s3&
                     &mm*(s3nm*ss*(31*ss**2 + 71*ss*tt + 44*tt**2) + s2nm*(-4*ss**3 + 23*ss**2*tt + 83&
                     &*ss*tt**2 + 60*tt**3))) + me2*(2*Mm2**5*(s2nm*s3mm - s1mm*(3*s2nm + s3nm)) - Mm2&
                     &**3*((s1mm*(s2nm - 9*s3nm) + s3mm*(s2nm + 8*s3nm))*ss**2 + (-7*s1mm*s2nm + 17*s2&
                     &nm*s3mm - 25*s1mm*s3nm + 8*s3mm*s3nm)*ss*tt - 4*(3*s1mm*s2nm - 11*s2nm*s3mm + 11&
                     &*s1mm*s3nm)*tt**2) + Mm2**2*((2*s2nm*s3mm - 27*s1mm*s3nm + 25*s3mm*s3nm)*ss**3 +&
                     & (-16*s1mm*s2nm + 29*s2nm*s3mm - 78*s1mm*s3nm + 49*s3mm*s3nm)*ss**2*tt + (-40*s1&
                     &mm*s2nm + 91*s2nm*s3mm - 127*s1mm*s3nm + 36*s3mm*s3nm)*ss*tt**2 - 4*(6*s1mm*s2nm&
                     & - 19*s2nm*s3mm + 19*s1mm*s3nm)*tt**3) + Mm2**4*(s3mm*(-5*s2nm*ss - 2*s3nm*ss + &
                     &6*s2nm*tt) + s1mm*(5*s2nm*ss + 7*s3nm*ss + 8*s2nm*tt - 6*s3nm*tt)) + Mm2*(ss + t&
                     &t)*(s1mm*(ss + tt)*(s2nm*tt*(7*ss + 10*tt) + s3nm*(22*ss**2 + 43*ss*tt + 54*tt**&
                     &2)) - s3mm*(2*s3nm*ss*(11*ss**2 + 23*ss*tt + 20*tt**2) + s2nm*tt*(19*ss**2 + 57*&
                     &ss*tt + 54*tt**2))) + (ss + tt)**2*(s1mm*(ss + tt)*(s2nm*ss*(ss + tt) - 7*s3nm*(&
                     &ss**2 + 2*ss*tt + 2*tt**2)) + s3mm*(s3nm*ss*(8*ss**2 + 17*ss*tt + 14*tt**2) + s2&
                     &nm*(-ss**3 + 4*ss**2*tt + 14*ss*tt**2 + 14*tt**3)))) + me2**2*(4*Mm2**4*(2*s2nm*&
                     &s3mm + s1mm*(s2nm - 2*s3nm)) - 2*Mm2**3*((s1mm*s2nm - s2nm*s3mm - s1mm*s3nm + 2*&
                     &s3mm*s3nm)*ss + 8*s2nm*s3mm*tt - 2*s1mm*(s2nm + 4*s3nm)*tt) + Mm2**2*((-7*s1mm*s&
                     &2nm + 5*s2nm*s3mm + 17*s1mm*s3nm - 22*s3mm*s3nm)*ss**2 + (9*s1mm*s2nm - s2nm*s3m&
                     &m - 3*s1mm*s3nm + 4*s3mm*s3nm)*ss*tt + 4*(3*s1mm*s2nm - 8*s2nm*s3mm + 8*s1mm*s3n&
                     &m)*tt**2) - (ss + tt)*(s1mm*(ss + tt)*(4*s2nm*ss*(ss + tt) - s3nm*(19*ss**2 + 41&
                     &*ss*tt + 40*tt**2)) + s3mm*(s3nm*ss*(23*ss**2 + 51*ss*tt + 36*tt**2) + s2nm*(-4*&
                     &ss**3 + 9*ss**2*tt + 45*ss*tt**2 + 40*tt**3))) + Mm2*(s1mm*(ss + tt)*(s2nm*(ss**&
                     &2 - 19*ss*tt - 20*tt**2) - 4*s3nm*(7*ss**2 + 12*ss*tt + 20*tt**2)) + s3mm*(3*s3n&
                     &m*ss*(11*ss**2 + 19*ss*tt + 12*tt**2) + s2nm*(-5*ss**3 + 19*ss**2*tt + 92*ss*tt*&
                     &*2 + 80*tt**3))))))/(2.*me2*ss*(-4*me2*Mm2 + (me2 + Mm2 - ss - tt)**2)*(me2**2 +&
                     & Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt) + tt*(ss + tt))**2)
    coeff_lmm = (12*me2**5*(4*Mm2 - ss)*(-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) + (s1mm - s3m&
                &m)*s3nm*ss) - 2*me2**4*(4*Mm2 - ss)*(18*Mm2**2*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm&
                &)) + 3*(s1mm - s3mm)*s3nm*ss**2 - 3*(s1mm*s2nm - s2nm*s3mm - 4*s1mm*s3nm + 5*s3m&
                &m*s3nm)*ss*tt + 38*(-(s2nm*s3mm) + s1mm*s3nm)*tt**2 + 3*Mm2*(-(s3mm*(s2nm + 2*s3&
                &nm)*ss) + s1mm*(s2nm + 3*s3nm)*ss + s2nm*s3mm*tt + s1mm*(s2nm - s3nm)*tt)) + 2*(&
                &-(Mm2*s2nm*s3mm) + Mm2*s1mm*(s2nm + s3nm) - s1mm*s3nm*(ss + tt) + s3mm*(s3nm*ss &
                &+ s2nm*tt))*(24*Mm2**6 + ss*(3*ss - 7*tt)*tt**2*(ss + tt)**2 - 6*Mm2**5*(3*ss + &
                &10*tt) + Mm2**4*(3*ss**2 + 39*ss*tt + 16*tt**2) - 4*Mm2**2*tt**2*(7*ss**2 + 39*s&
                &s*tt + 22*tt**2) + Mm2**3*(-6*ss**2*tt + 60*ss*tt**2 + 80*tt**3) + Mm2*(-6*ss**3&
                &*tt**2 + 56*ss**2*tt**3 + 90*ss*tt**4 + 28*tt**5)) + me2**3*(96*Mm2**4*(-(s2nm*s&
                &3mm) + s1mm*(s2nm + s3nm)) - 8*Mm2**2*(3*(s1mm*s2nm - s2nm*s3mm - 2*s1mm*s3nm + &
                &3*s3mm*s3nm)*ss**2 + 3*(s1mm*s2nm - s2nm*s3mm - s1mm*s3nm + 2*s3mm*s3nm)*ss*tt +&
                & (38*s1mm*s2nm + 9*s2nm*s3mm - 9*s1mm*s3nm)*tt**2) + ss*tt*(3*(s1mm*s2nm - s2nm*&
                &s3mm - 3*s1mm*s3nm + 4*s3mm*s3nm)*ss**2 + (15*s1mm*s2nm - 13*s2nm*s3mm - 63*s1mm&
                &*s3nm + 76*s3mm*s3nm)*ss*tt + 218*(s2nm*s3mm - s1mm*s3nm)*tt**2) - 2*Mm2*(6*(s1m&
                &m - s3mm)*s3nm*ss**3 + 3*(s1mm*(s2nm - 6*s3nm) + 6*s3mm*s3nm)*ss**2*tt + (-8*s1m&
                &m*s2nm + 13*s2nm*s3mm - 165*s1mm*s3nm + 152*s3mm*s3nm)*ss*tt**2 + 436*(s2nm*s3mm&
                & - s1mm*s3nm)*tt**3) + 24*Mm2**3*(3*s1mm*s2nm*ss - s1mm*s3nm*(ss + 4*tt) + s3mm*&
                &(-3*s2nm*ss + 4*s3nm*ss + 4*s2nm*tt))) + me2*(-144*Mm2**6*(-(s2nm*s3mm) + s1mm*(&
                &s2nm + s3nm)) + 12*Mm2**5*((11*s1mm*s2nm - 11*s2nm*s3mm + 15*s1mm*s3nm - 4*s3mm*&
                &s3nm)*ss + 8*(2*s1mm*s2nm - 3*s2nm*s3mm + 3*s1mm*s3nm)*tt) - 4*Mm2**4*(3*(2*s1mm&
                &*s2nm - 2*s2nm*s3mm + 7*s1mm*s3nm - 5*s3mm*s3nm)*ss**2 + 6*(5*s1mm*s2nm - 9*s2nm&
                &*s3mm + 11*s1mm*s3nm - 2*s3mm*s3nm)*ss*tt + 2*(46*s1mm*s2nm - 57*s2nm*s3mm + 57*&
                &s1mm*s3nm)*tt**2) + 2*Mm2*tt**2*((-24*s1mm*s2nm + 21*s2nm*s3mm - 23*s1mm*s3nm + &
                &2*s3mm*s3nm)*ss**3 + 2*(5*s1mm*s2nm - 35*s2nm*s3mm + 166*s1mm*s3nm - 131*s3mm*s3&
                &nm)*ss**2*tt + (34*s1mm*s2nm - 383*s2nm*s3mm + 543*s1mm*s3nm - 160*s3mm*s3nm)*ss&
                &*tt**2 + 188*(-(s2nm*s3mm) + s1mm*s3nm)*tt**3) + 2*Mm2**3*(6*(s1mm - s3mm)*s3nm*&
                &ss**3 + 3*(-2*s3mm*(2*s2nm + 5*s3nm) + s1mm*(s2nm + 14*s3nm))*ss**2*tt + (80*s1m&
                &m*s2nm - 143*s2nm*s3mm + 327*s1mm*s3nm - 184*s3mm*s3nm)*ss*tt**2 + 36*(8*s1mm*s2&
                &nm - 13*s2nm*s3mm + 13*s1mm*s3nm)*tt**3) + Mm2**2*tt*(3*(s1mm*s2nm - s2nm*s3mm -&
                & 3*s1mm*s3nm + 4*s3mm*s3nm)*ss**3 + (79*s1mm*s2nm - 53*s2nm*s3mm - 215*s1mm*s3nm&
                & + 268*s3mm*s3nm)*ss**2*tt + 2*(-232*s1mm*s2nm + 429*s2nm*s3mm - 741*s1mm*s3nm +&
                & 312*s3mm*s3nm)*ss*tt**2 - 40*(8*s1mm*s2nm - 25*s2nm*s3mm + 25*s1mm*s3nm)*tt**3)&
                & + ss*tt**2*(ss + tt)*(8*s3mm*s3nm*ss*(-3*ss + 10*tt) + s1mm*(ss + tt)*(3*s2nm*s&
                &s + 27*s3nm*ss - 94*s3nm*tt) + s2nm*s3mm*(-3*ss**2 - 13*ss*tt + 94*tt**2))) + 2*&
                &me2**2*(48*Mm2**5*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) - 12*Mm2**4*((7*s1mm*s2nm &
                &- 7*s2nm*s3mm + 3*s1mm*s3nm + 4*s3mm*s3nm)*ss - 2*s2nm*s3mm*tt + 2*s1mm*(s2nm + &
                &s3nm)*tt) + 2*Mm2**3*(3*(-3*s2nm*s3mm + 2*s3mm*s3nm + s1mm*(3*s2nm + s3nm))*ss**&
                &2 + 9*(-3*s2nm*s3mm + s1mm*(s2nm + 3*s3nm))*ss*tt - 2*(16*s1mm*s2nm - 29*s2nm*s3&
                &mm + 29*s1mm*s3nm)*tt**2) + Mm2**2*tt*(3*(3*s1mm*s2nm - 8*s1mm*s3nm + 8*s3mm*s3n&
                &m)*ss**2 + (44*s1mm*s2nm + 23*s2nm*s3mm + 41*s1mm*s3nm - 64*s3mm*s3nm)*ss*tt + 8&
                &*(34*s1mm*s2nm - 63*s2nm*s3mm + 63*s1mm*s3nm)*tt**2) + Mm2*tt*((-3*s1mm*s2nm + 3&
                &*s2nm*s3mm + 3*s1mm*s3nm - 6*s3mm*s3nm)*ss**3 + (29*s1mm*s2nm - 37*s2nm*s3mm - 1&
                &9*s1mm*s3nm + 56*s3mm*s3nm)*ss**2*tt + 2*(-22*s1mm*s2nm + 199*s2nm*s3mm - 323*s1&
                &mm*s3nm + 124*s3mm*s3nm)*ss*tt**2 + 444*(s2nm*s3mm - s1mm*s3nm)*tt**3) - ss*tt**&
                &2*(-2*s3mm*s3nm*ss*(ss - 31*tt) + s1mm*(ss + tt)*(6*s2nm*ss + 11*s3nm*ss - 111*s&
                &3nm*tt) + s2nm*s3mm*(-9*ss**2 + 38*ss*tt + 111*tt**2))))/(24.*me2*(4*Mm2 - ss)*s&
                &s*tt**2*(-2*me2 - 2*Mm2 + ss + tt)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt&
                &) + tt*(ss + tt)))
    coeff_lmu = (10*me2*s2nm*s3mm + 7*Mm2*s2nm*s3mm - 10*me2*s1mm*s3nm - 7*Mm2*s1mm*(s2nm + s3nm&
                &) + 7*s1mm*s3nm*ss - 7*s3mm*s3nm*ss - 7*s2nm*s3mm*tt + 7*s1mm*s3nm*tt)/(6.*me2*s&
                &s)
    coeff_ls = (me2**3*(8*Mm2*s2nm*s3mm - 8*Mm2*s1mm*(s2nm + s3nm) + 2*(s2nm*s3mm + s1mm*(s2nm &
               &- s3nm))*ss) + ss**2*(-3*Mm2 + ss + tt)*(Mm2*s2nm*s3mm - Mm2*s1mm*(s2nm + s3nm) &
               &+ s1mm*s3nm*(ss + tt) - s3mm*(s3nm*ss + s2nm*tt)) + me2**2*(16*Mm2**2*(-(s2nm*s3&
               &mm) + s1mm*(s2nm + s3nm)) + ss*(s3mm*(3*s2nm*ss - 12*s3nm*ss - 4*s2nm*tt) + s1mm&
               &*(-3*s2nm*ss + 9*s3nm*ss - 4*s2nm*tt + 4*s3nm*tt)) + 4*Mm2*(s3mm*(-3*s2nm*ss + 8&
               &*s3nm*ss - 4*s2nm*tt) + s1mm*(s2nm*ss - 5*s3nm*ss + 4*s2nm*tt + 4*s3nm*tt))) + m&
               &e2*(-8*Mm2**3*(-(s2nm*s3mm) + s1mm*(s2nm + s3nm)) + 2*Mm2**2*(s2nm*s3mm*(5*ss - &
               &8*tt) + s1mm*(-3*s2nm*ss - 5*s3nm*ss + 8*s2nm*tt + 8*s3nm*tt)) + 4*Mm2*(-5*s3mm*&
               &s3nm*ss**2 - s2nm*s3mm*(ss - 2*tt)*tt - s1mm*s2nm*tt*(3*ss + 2*tt) + s1mm*s3nm*(&
               &5*ss**2 + ss*tt - 2*tt**2)) + ss*(s1mm*(ss + tt)*(-2*s3nm*(3*ss + tt) + s2nm*(ss&
               & + 2*tt)) + s3mm*(s3nm*ss*(7*ss + 4*tt) + s2nm*(-ss**2 + 4*ss*tt + 2*tt**2)))))/&
               &(2.*me2*(4*me2 - ss)*(-4*Mm2 + ss)*(me2**2 + Mm2**2 - 2*Mm2*tt - 2*me2*(Mm2 + tt&
               &) + tt*(ss + tt)))
    coeff_unity = -(1./18.)*(-24*me2**2*(s2nm*s3mm - s1mm*s3nm)*tt + 3*Mm2**2*(-(s2nm*s&
                  &3mm) + s1mm*(s2nm + s3nm))*(3*ss + 8*tt) + Mm2*(9*(-s1mm + s3mm)*s3nm*ss**2 + 2*&
                  &(76*s1mm*s2nm + 5*s2nm*s3mm - 17*s1mm*s3nm + 12*s3mm*s3nm)*ss*tt + 24*(s2nm*s3mm&
                  & - s1mm*s3nm)*tt**2) - ss*tt*(-(s1mm*s3nm*(ss + tt)) + 3*s1mm*s2nm*(17*ss + 39*t&
                  &t) + s3mm*(s3nm*ss + s2nm*tt)) + me2*(9*(-s1mm + s3mm)*s3nm*ss**2 + (117*s1mm*s2&
                  &nm - 107*s2nm*s3mm + 191*s1mm*s3nm - 84*s3mm*s3nm)*ss*tt + 24*(s2nm*s3mm - s1mm*&
                  &s3nm)*tt**2 + Mm2*(9*s2nm*s3mm*ss - 9*s1mm*(s2nm + s3nm)*ss + 24*(-2*s2nm*s3mm +&
                  & s1mm*(s2nm + 2*s3nm))*tt)))/(me2*ss**2*tt)
    coeffRR = coeffRR + c6se*coeff_c6se + c6sm*coeff_c6sm + c6t*coeff_c6t  &
            + c6u*coeff_c6u + ce*coeff_ce + cm*coeff_cm  &
            + coeff_discse*discse + coeff_discse_lmm*discse*lmm + coeff_discse_lmu*discse*lmu  &
            + coeff_discsm*discsm + coeff_discsm_lmu*discsm*lmu + coeff_disct*disct  &
            + coeff_disct_lmm*disct*lmm + coeff_disct_lmu*disct*lmu + coeff_disct_ls*disctLs  &
            + coeff_discu*discu + coeff_discu_lmm*discu*lmm + coeff_discu_lmu*discu*lmu  &
            + coeff_discu_ls*discuLs + coeff_lmm*lmm + coeff_lmu*lmu  &
            + coeff_ls*ls + coeff_unity
  endif

  END FUNCTION COEFFRR

  SUBROUTINE HARDCOEFF(m, Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm, &
        coeff_Cd1, coeff_Cd2, coeff_QQ2, coeff_CL_CL, coeff_CL_CR, coeff_CLe_CLe, &
        coeff_CLe_CRe, coeff_CR_CR, coeff_CRe_CRe, coeff_CL0_CL0_QQ1, coeff_CL0_CR0_QQ1, coeff_CR0_CR0_QQ1)
  real(kind=prec), intent(in) :: m, Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm
  real(kind=prec), intent(out) :: coeff_Cd1(0:0), coeff_Cd2(0:0), coeff_QQ2(0:1)
  real(kind=prec), intent(out) :: coeff_CL_CL(0:1), coeff_CL_CR(0:1), coeff_CLe_CLe(0:0)
  real(kind=prec), intent(out) :: coeff_CLe_CRe(0:0), coeff_CR_CR(0:1), coeff_CRe_CRe(0:0)
  real(kind=prec), intent(out) :: coeff_CL0_CL0_QQ1(0:1), coeff_CL0_CR0_QQ1(0:1), coeff_CR0_CR0_QQ1(0:1)


  coeff_Cd1(0) = (64*Pi**2*(s1mm*s2nm*ss - s2nm*s3mm*ss - 3*s1mm*s3nm*ss + 4*s3mm*s3nm*ss + 2*mm*&
                 &*2*(-(s2nm*s3mm) + s1mm*(2*s2nm + s3nm) - 2*me**2*(-2 + snm)*ss) + me**2*(-2*s2n&
                 &m*s3mm + 2*s1mm*s3nm + 4*ss**2) + 2*s2nm*s3mm*tt - 2*s1mm*s3nm*tt))/(me*ss)

  coeff_Cd2(0) = -256*mm*Pi**2*(-(me**2*(-2 + snm)) + ss)

  coeff_QQ2(0) = (-128*Pi**2*(mm**2*s2nm*s3mm - mm**2*s1mm*(s2nm + s3nm) + me**6*(-2 + snm) + s1m&
                 &m*s3nm*ss - s3mm*s3nm*ss + 2*me**4*(-2 + snm)*(mm**2 - tt) - s2nm*s3mm*tt + s1mm&
                 &*s3nm*tt + me**2*(s2nm*s3mm - s1mm*s3nm + mm**4*(-2 + snm) - ss**2 - 2*mm**2*(-2&
                 & + snm)*tt - 2*ss*tt + snm*ss*tt - 2*tt**2 + snm*tt**2)))/(me**2*ss**2)

  coeff_QQ2(1) = (64*Pi**2*(-((s1mm*s2nm)/me**2) + (-2 + snm)*ss))/ss

  coeff_CL_CL(0) = (32*Pi**2*(mm**2*s2nm*s3mm - mm**2*s1mm*(s2nm + s3nm) + me**6*(-2 + snm) + s1mm*&
                   &s3nm*ss - s3mm*s3nm*ss - s2nm*s3mm*tt + s1mm*s3nm*tt + me**4*(s1mm + s2nm - 2*s3&
                   &mm - 2*s3nm + 2*mm**2*(-2 + snm) + 2*ss + 4*tt - 2*snm*tt) + me**2*(2*s2nm*s3mm &
                   &- 2*s1mm*s3nm + mm**4*(-2 + snm) - s1mm*ss - 2*s2nm*ss + 3*s3mm*ss + 3*s3nm*ss -&
                   & 2*ss**2 - s1mm*tt - s2nm*tt + 2*s3mm*tt + 2*s3nm*tt - 4*ss*tt + snm*ss*tt - 2*t&
                   &t**2 + snm*tt**2 + mm**2*(3*s1mm - s2nm - 2*(s3mm + s3nm - ss - 2*tt + snm*tt)))&
                   &))/(me**2*ss)

  coeff_CL_CL(1) = (32*Pi**2*(-(me**4*(6 + snm)*ss) + s1mm*s2nm*(mm**2 - tt) + me**2*(-4*s2nm*(s3mm&
                   & - ss) + s1mm*(s2nm + 4*s3nm + 2*ss) + ss*(-6*s3mm - 6*s3nm - mm**2*(6 + snm) + &
                   &4*ss + 6*tt + snm*tt))))/(me**2*ss)

  coeff_CL_CR(0) = (64*Pi**2*(mm**2*s2nm*s3mm - mm**2*s1mm*(s2nm + s3nm) + me**6*(-2 + snm) + s1mm*&
                   &s3nm*ss - s3mm*s3nm*ss - s2nm*s3mm*tt + s1mm*s3nm*tt + 2*me**4*(mm**2*(-2 + snm)&
                   & - ss + 2*tt - snm*tt) + me**2*(mm**4*(-2 + snm) - 2*mm**2*(ss + (-2 + snm)*tt) &
                   &+ tt*(-2*tt + snm*(ss + tt)))))/(me**2*ss)

  coeff_CL_CR(1) = (64*Pi**2*(me**4*(6 + snm)*ss + s1mm*s2nm*(-mm**2 + ss + tt) - me**2*(-4*s2nm*s3&
                   &mm + s1mm*(s2nm + 4*s3nm) + ss*(-(mm**2*(6 + snm)) + (2 + snm)*ss + (6 + snm)*tt&
                   &))))/(me**2*ss)

  coeff_CLe_CLe(0) = (-128*Pi*(10*mm**2*(s2nm*s3mm - s1mm*s3nm) + 2*me**6*(-13 + 5*snm) - 3*s1mm*s2nm&
                     &*ss + 10*s1mm*s3nm*ss - 10*s3mm*s3nm*ss - 8*s1mm*s2nm*tt - 10*s2nm*s3mm*tt + 10*&
                     &s1mm*s3nm*tt + me**4*(13*s1mm + 13*s2nm - 26*s3mm - 26*s3nm + 4*mm**2*(-15 + 7*s&
                     &nm) + 12*ss - 10*snm*ss + 52*tt - 20*snm*tt) + me**2*(10*s2nm*s3mm - 6*s3mm*s3nm&
                     & + 2*mm**4*(-13 + 5*snm) - 14*s2nm*ss + 23*s3mm*ss + 23*s3nm*ss - 14*ss**2 + 3*s&
                     &nm*ss**2 + s1mm*(8*s2nm - 4*s3nm - 9*ss - 13*tt) - 13*s2nm*tt + 26*s3mm*tt + 26*&
                     &s3nm*tt - 36*ss*tt + 18*snm*ss*tt - 26*tt**2 + 10*snm*tt**2 + mm**2*(37*s1mm - 1&
                     &1*s2nm - 26*s3mm - 26*s3nm + 12*ss - 10*snm*ss + 52*tt - 20*snm*tt))))/(me**2*ss&
                     &)

  coeff_CLe_CRe(0) = (-256*Pi*(me**6*(26 - 10*snm) - 2*me**4*(2*mm**2*(-15 + 7*snm) + ss - 2*snm*ss +&
                     & 2*(13 - 5*snm)*tt) + 2*(mm**2*(3*s1mm*s2nm - 5*s2nm*s3mm + 5*s1mm*s3nm) + s1mm*&
                     &s2nm*tt - 5*s1mm*s3nm*(ss + tt) + 5*s3mm*(s3nm*ss + s2nm*tt)) + me**2*(-2*s1mm*s&
                     &2nm - 11*s2nm*s3mm + 5*s1mm*s3nm + 6*s3mm*s3nm + mm**4*(26 - 10*snm) + 9*ss**2 +&
                     & 26*ss*tt - 12*snm*ss*tt + 26*tt**2 - 10*snm*tt**2 + mm**2*((-2 + 4*snm)*ss + 4*&
                     &(-13 + 5*snm)*tt))))/(me**2*ss)

  coeff_CR_CR(0) = (32*Pi**2*(mm**2*s2nm*s3mm - mm**2*s1mm*(s2nm + s3nm) + me**6*(-2 + snm) + s1mm*&
                   &s3nm*ss - s3mm*s3nm*ss - s2nm*s3mm*tt + s1mm*s3nm*tt + me**4*(-s1mm - s2nm + 2*s&
                   &3mm + 2*s3nm + 2*mm**2*(-2 + snm) + 2*ss + 4*tt - 2*snm*tt) + me**2*(2*s2nm*s3mm&
                   & - 2*s1mm*s3nm + mm**4*(-2 + snm) + s1mm*ss + 2*s2nm*ss - 3*s3mm*ss - 3*s3nm*ss &
                   &- 2*ss**2 + s1mm*tt + s2nm*tt - 2*s3mm*tt - 2*s3nm*tt - 4*ss*tt + snm*ss*tt - 2*&
                   &tt**2 + snm*tt**2 + mm**2*(-3*s1mm + s2nm + 2*(s3mm + s3nm + ss + 2*tt - snm*tt)&
                   &))))/(me**2*ss)

  coeff_CR_CR(1) = (32*Pi**2*(-(me**4*(6 + snm)*ss) + s1mm*s2nm*(mm**2 - tt) + me**2*(s1mm*(s2nm + &
                   &4*s3nm - 2*ss) - 4*s2nm*(s3mm + ss) + ss*(6*s3mm + 6*s3nm - mm**2*(6 + snm) + 4*&
                   &ss + 6*tt + snm*tt))))/(me**2*ss)

  coeff_CRe_CRe(0) = (-128*Pi*(10*mm**2*(s2nm*s3mm - s1mm*s3nm) + 2*me**6*(-13 + 5*snm) - 3*s1mm*s2nm&
                     &*ss + 10*s1mm*s3nm*ss - 10*s3mm*s3nm*ss - 8*s1mm*s2nm*tt - 10*s2nm*s3mm*tt + 10*&
                     &s1mm*s3nm*tt + me**4*(-13*s1mm - 13*s2nm + 26*s3mm + 26*s3nm + 4*mm**2*(-15 + 7*&
                     &snm) + 12*ss - 10*snm*ss + 52*tt - 20*snm*tt) + me**2*(10*s2nm*s3mm - 6*s3mm*s3n&
                     &m + 2*mm**4*(-13 + 5*snm) + 14*s2nm*ss - 23*s3mm*ss - 23*s3nm*ss - 14*ss**2 + 3*&
                     &snm*ss**2 + 13*s2nm*tt - 26*s3mm*tt - 26*s3nm*tt - 36*ss*tt + 18*snm*ss*tt - 26*&
                     &tt**2 + 10*snm*tt**2 + s1mm*(8*s2nm - 4*s3nm + 9*ss + 13*tt) + mm**2*(-37*s1mm +&
                     & 11*s2nm + 26*s3mm + 26*s3nm + 12*ss - 10*snm*ss + 52*tt - 20*snm*tt))))/(me**2*&
                     &ss)

  coeff_CL0_CL0_QQ1(0) = (32*Pi**2*(mm**2*s2nm*s3mm - mm**2*s1mm*(s2nm + s3nm) + me**6*(-2 + snm) + s1mm*&
                         &s3nm*ss - s3mm*s3nm*ss - s2nm*s3mm*tt + s1mm*s3nm*tt + me**4*(s1mm + s2nm - 2*s3&
                         &mm - 2*s3nm + 2*mm**2*(-2 + snm) + 2*ss + 4*tt - 2*snm*tt) + me**2*(2*s2nm*s3mm &
                         &- 2*s1mm*s3nm + mm**4*(-2 + snm) - s1mm*ss - 2*s2nm*ss + 3*s3mm*ss + 3*s3nm*ss -&
                         & 2*ss**2 - s1mm*tt - s2nm*tt + 2*s3mm*tt + 2*s3nm*tt - 4*ss*tt + snm*ss*tt - 2*t&
                         &t**2 + snm*tt**2 + mm**2*(3*s1mm - s2nm - 2*(s3mm + s3nm - ss - 2*tt + snm*tt)))&
                         &))/(me**2*ss)

  coeff_CL0_CL0_QQ1(1) = (32*Pi**2*(me**4*(-6 + snm)*ss + s1mm*s2nm*(-mm**2 + ss + tt) - me**2*(2*s2nm*(s&
                         &3mm - 2*ss) + s1mm*(s2nm - 2*(s3nm + ss)) + ss*(6*s3mm + 6*s3nm - mm**2*(-6 + sn&
                         &m) - 4*ss + snm*ss - 6*tt + snm*tt))))/(me**2*ss)

  coeff_CL0_CR0_QQ1(0) = (64*Pi**2*(mm**2*s2nm*s3mm - mm**2*s1mm*(s2nm + s3nm) + me**6*(-2 + snm) + s1mm*&
                         &s3nm*ss - s3mm*s3nm*ss - s2nm*s3mm*tt + s1mm*s3nm*tt + 2*me**4*(mm**2*(-2 + snm)&
                         & - ss + 2*tt - snm*tt) + me**2*(mm**4*(-2 + snm) - 2*mm**2*(ss + (-2 + snm)*tt) &
                         &+ tt*(-2*tt + snm*(ss + tt)))))/(me**2*ss)

  coeff_CL0_CR0_QQ1(1) = (-64*Pi**2*(me**4*(-6 + snm)*ss + s1mm*s2nm*(-mm**2 + tt) + me**2*(-2*s2nm*s3mm &
                         &- s1mm*(s2nm - 2*s3nm) + ss*(mm**2*(-6 + snm) + 2*ss + 6*tt - snm*tt))))/(me**2*&
                         &ss)

  coeff_CR0_CR0_QQ1(0) = (32*Pi**2*(mm**2*s2nm*s3mm - mm**2*s1mm*(s2nm + s3nm) + me**6*(-2 + snm) + s1mm*&
                         &s3nm*ss - s3mm*s3nm*ss - s2nm*s3mm*tt + s1mm*s3nm*tt + me**4*(-s1mm - s2nm + 2*s&
                         &3mm + 2*s3nm + 2*mm**2*(-2 + snm) + 2*ss + 4*tt - 2*snm*tt) + me**2*(2*s2nm*s3mm&
                         & - 2*s1mm*s3nm + mm**4*(-2 + snm) + s1mm*ss + 2*s2nm*ss - 3*s3mm*ss - 3*s3nm*ss &
                         &- 2*ss**2 + s1mm*tt + s2nm*tt - 2*s3mm*tt - 2*s3nm*tt - 4*ss*tt + snm*ss*tt - 2*&
                         &tt**2 + snm*tt**2 + mm**2*(-3*s1mm + s2nm + 2*(s3mm + s3nm + ss + 2*tt - snm*tt)&
                         &))))/(me**2*ss)

  coeff_CR0_CR0_QQ1(1) = (32*Pi**2*(me**4*(-6 + snm)*ss + s1mm*s2nm*(-mm**2 + ss + tt) - me**2*(2*s2nm*(s&
                         &3mm + 2*ss) + s1mm*(s2nm - 2*s3nm + 2*ss) + ss*(-6*s3mm - 6*s3nm - mm**2*(-6 + s&
                         &nm) - 4*ss + snm*ss - 6*tt + snm*tt))))/(me**2*ss)
  coeff_Cd1 = coeff_Cd1 / ss
  coeff_Cd2 = coeff_Cd2 / ss
  coeff_QQ2 = coeff_QQ2 * ss

  END SUBROUTINE HARDCOEFF

  SUBROUTINE WILSONCOEFF(Me, Mm, CL, CR, CDe, CDm, CLe, CRe, QQg, QQz)
  real(kind=prec), intent(in) :: Me, Mm
  real(kind=prec), intent(out), dimension(0:1, -1:0) :: CL, CR
  real(kind=prec), intent(out), dimension(-1:0) :: QQg, QQz
  real(kind=prec), intent(out) :: CDe, CDm, CLe, CRe

  logical, save :: first = .true.
  real(kind=prec), save :: CL0, CR0, tCLe, tCRe, CD, CL1, CR1
  real(kind=prec), save :: CL1p1, CR1p1, CL1p2, CR1p2
  real(kind=prec), save :: tQQ(-1:0)
  real(kind=prec) :: SW, CW, MW

  if(first) then
    first = .true.
    SW = sqrt(SW2)
    CW = sqrt(1-SW2)
    MW = CW * MZ

    CL0 = (1 - 2*SW**2)/(2.*CW*MZ*SW)
    CR0 = -(SW/(CW*MZ))
    tCLe = (1 - 2*CW**2)/(4.*MW*SW)
    tCRe = SW/(2.*MW)
    CD = -0.020833333333333332*(-3 - 8*SW**2 + 16*SW**4)/(MW**2*Pi*SW**2)
    CL1 = ((6 - 1935*CW**2 + 47038*CW**4 - 211464*CW**6 + 363728*CW**8 - 276480*CW**10 + 7&
      &8336*CW**12)*MZ**4 + 6*(1 + 2*CW**2)*MH**4*SW**2 + 27*MH**2*MW**2*(1 - 2*SW**2))&
      &/(288.*MW**5*SW**3) + ((MH**4 - 4*MH**2*MW**2 + 12*MW**4)*DiscB(MW**2,MH,MW))/(4&
      &8.*MW**5*SW**5) + ((1 - 4*CW**2)*(1 + 20*CW**2 + 12*CW**4)*DiscB(MW**2,MW,MZ))/(&
      &48.*CW**5*MZ*SW**5) - ((MH**4 - 4*MH**2*MZ**2 + 12*MZ**4)*(1 + 2*SW**2)*DiscB(MZ&
      &**2,MH,MZ))/(48.*CW*MZ**5*SW**5) + ((99 - 66*SW**2 - 316*SW**4 + 376*SW**6 - 96*&
      &SW**8)*DiscB(MZ**2,MW,MW))/(48.*CW*MZ*SW**5) + ((6*(1 + 4*CW**2 - 8*CW**4)*MH**4&
      &*MW**2*MZ**2 - 36*MH**2*MW**4*MZ**2*(-1 + 2*SW**2) + MH**8*(1 + 3*SW**2 - 5*SW**&
      &4 + 2*SW**6) - MH**6*MZ**2*(1 + 21*SW**2 - 35*SW**4 + 14*SW**6))*Log(MZ**2/MH**2&
      &))/(96.*MW**7*(MH**2 - MZ**2)*SW**3) - ((MH**6 - 6*MH**4*MW**2 + 18*MH**2*MW**4 &
      &+ (1 + 14*CW**2 - 102*CW**4 - 22*CW**6 + 4*CW**8)*MZ**6)*Log(MZ**2/MW**2))/(96.*&
      &MW**7*SW**5)
    CR1 = (2*(1 + CW**2)*MH**4 - 9*MH**2*MW**2 + (2 - 105*CW**2 + 3296*CW**4 - (52840*CW**&
      &6)/3. + 26496*CW**8 - 13056*CW**10)*MZ**4)/(48.*MW**5*SW) + 2*SW**2*(((MH**4 - 4&
      &*MH**2*MW**2 + 12*MW**4)*DiscB(MW**2,MH,MW))/(48.*MW**5*SW**5) + ((1 - 4*CW**2)*&
      &(1 + 20*CW**2 + 12*CW**4)*DiscB(MW**2,MW,MZ))/(48.*CW**5*MZ*SW**5)) + (2*SW**2*(&
      &-0.020833333333333332*((MH**4 - 4*MH**2*MZ**2 + 12*MZ**4)*(1 + 2*SW**2)*DiscB(MZ&
      &**2,MH,MZ))/(CW*MZ**5*SW**5) + ((99 - 66*SW**2 - 316*SW**4 + 376*SW**6 - 96*SW**&
      &8)*DiscB(MZ**2,MW,MW))/(48.*CW*MZ*SW**5)))/(1 + 2*SW**2) + (((1 + CW**2 + CW**4)&
      &*MH**8 - (1 + 7*CW**2 + 7*CW**4)*MH**6*MZ**2 + 6*(1 + 4*CW**2)*MH**4*MW**2*MZ**2&
      & - 36*MH**2*MW**4*MZ**2)*Log(MZ**2/MH**2))/(48.*MW**7*(MH**2 - MZ**2)*SW) - ((MH&
      &**6 - 6*MH**4*MW**2 + 18*MH**2*MW**4 + (1 + 14*CW**2 - 102*CW**4 - 18*CW**6)*MZ*&
      &*6)*Log(MZ**2/MW**2))/(48.*MW**7*SW**3)
    CL1p1 = (5 - 10*CW**2)/(6.*CW*MZ*SW)
    CR1p1 = (5*SW)/(3.*MW)
    CL1p2 = (7 - 18*SW**2 - 144*SW**4 + 288*SW**6)/(6.*CW*MZ*SW)
    CR1p2 = (1 + 27*SW**2 - 144*SW**4 + 144*SW**6)/(3.*CW*MZ*SW)
    tQQ(-1) = (-2*(1 + 34*SW**2 - 144*SW**4 + 144*SW**6))/(3.*MW**2)
    tQQ(0) = (-3 - 24082*SW**2 + 118900*SW**4 - 191520*SW**6 + 97920*SW**8)/(180.*CW**2*MW**2&
         &)
  endif

  CL(0, 0) = CL0
  CL(1, -1) = CL1p1 + CL1p2
  CL(1, 0) = CL1 + log(musq/me/Mm) * CL1p1 + log(musq/MZ**2) * CL1p2
  CLe = tCLe

  CR(0, 0) = CR0
  CR(1, -1) = CR1p1 + CR1p2
  CR(1, 0) = CR1 + log(musq/me/Mm) * CR1p1 + log(musq/MZ**2) * CR1p2
  CRe = tCRe

  CDe = +CD * Me
  CDm = -CD * Mm

  QQg(-1) = -6
  QQg( 0) = -8.   - 6. * log(musq/me/Mm)
  QQz(-1) = tQQ(-1)
  QQz( 0) = tQQ( 0) + log(musq/MZ**2) * tQQ(-1)

  CL(1,:) = CL(1,:)/4/pi
  CR(1,:) = CR(1,:)/4/pi
  QQg = QQg/4/pi
  QQz = QQz/4/pi

  END SUBROUTINE WILSONCOEFF

                          !!!!!!!!!!!!!!!!!!!!!!
                         END MODULE mue_PEPEZMML
                          !!!!!!!!!!!!!!!!!!!!!!
