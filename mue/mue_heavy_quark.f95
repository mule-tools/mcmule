                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE mue_heavy_quark
                          !!!!!!!!!!!!!!!!!!!!!!
    use functions
    implicit none
    real(kind=prec), parameter :: zeta(2:5) = (/1.6449340668482264365, &
                                                1.2020569031595942854, &
                                                1.0823232337111381915, &
                                                1.0369277551433699263 /)
    real(kind=8) :: Hr1(-1:+1)
    real(kind=8) :: Hr2(-1:+1,-1:+1)
    real(kind=8) :: Hr3(-1:+1,-1:+1,-1:+1)
    real(kind=8) :: Hr4(-1:+1,-1:+1,-1:+1,-1:+1)
    real(kind=prec) :: xpow(1:7)
    real(kind=prec) :: xp1(1:5)
    real(kind=prec) :: xm1(1:3)
    real(kind=prec) :: xp1xm1

  contains

  SUBROUTINE INIT_HPLS(x)
  real(kind=prec), intent(in) :: x
  integer, parameter :: n1 = -1
  integer, parameter :: n2 = +1
  integer, parameter :: nw =  4
  complex(kind=16) Hc1(-1:+1)
  complex(kind=16) Hc2(-1:+1,-1:+1)
  complex(kind=16) Hc3(-1:+1,-1:+1,-1:+1)
  complex(kind=16) Hc4(-1:+1,-1:+1,-1:+1,-1:+1)

  real(kind=8) Hi1(-1:+1)
  real(kind=8) Hi2(-1:+1,-1:+1)
  real(kind=8) Hi3(-1:+1,-1:+1,-1:+1)
  real(kind=8) Hi4(-1:+1,-1:+1,-1:+1,-1:+1)

  call hplog(x, nw, Hc1, Hc2, Hc3, Hc4, &
                    Hr1, Hr2, Hr3, Hr4, &
                    Hi1, Hi2, Hi3, Hi4, n1, n2)

  xpow = x**(/1,2,3,4,5,6,7/)
  xp1 = 1._prec/(x+1)**(/1,2,3,4,5/)
  xm1 = 1._prec/(x-1)**(/1,2,3/)
  xp1xm1 = (xpow(2)+1)**2 / (xpow(2)+1)**2
  END SUBROUTINE


  FUNCTION FF1A1ES()
  ! Calculate the alpha^1 e^-1 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a1es
  ff1a1es = -1
  ff1a1es = ff1a1es + Hr1(0)*(xm1(1)*xp1(1)*(1 + xpow(2)))
  END FUNCTION FF1A1ES

  FUNCTION FF1A1EF()
  ! Calculate the alpha^1 e^0 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a1ef
  ff1a1ef = + zeta(2)*(-(xm1(1)*xp1(1)*(1 + xpow(2))))
  ff1a1ef = ff1a1ef + Hr2(0,0)*(xm1(1)*xp1(1)*(1 + xpow(2)))
  ff1a1ef = ff1a1ef + Hr1(0)*((xm1(1)*xp1(1)*(3 + 2*xpow(1) + 3*xpow(2)))/2.)
  ff1a1ef = ff1a1ef -2
  ff1a1ef = ff1a1ef + Hr2(-1,0)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))
  END FUNCTION FF1A1EF

  FUNCTION FF1A1EL()
  ! Calculate the alpha^1 e^1 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a1el
  ff1a1el = + zeta(2)*(-(xm1(1)*xp1(1)*(1 + xpow(1) + 2*xpow(2))))
  ff1a1el = ff1a1el + Hr2(-1,0)*(-(xm1(1)*xp1(1)*(3 + 2*xpow(1) + 3*xpow(2))))
  ff1a1el = ff1a1el + Hr2(0,0)*((xm1(1)*xp1(1)*(3 + 2*xpow(1) + 3*xpow(2)))/2.)
  ff1a1el = ff1a1el + Hr1(0)*((xm1(1)*xp1(1)*(7 + 2*xpow(1) + 7*xpow(2)))/2.)
  ff1a1el = ff1a1el -4
  ff1a1el = ff1a1el + Hr3(-1,0,0)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))
  ff1a1el = ff1a1el + Hr3(0,-1,0)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))
  ff1a1el = ff1a1el + zeta(3)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))
  ff1a1el = ff1a1el + Hr1(0)*zeta(2)*(-(xm1(1)*xp1(1)*(1 + xpow(2)))/2.)
  ff1a1el = ff1a1el + Hr3(0,0,0)*(xm1(1)*xp1(1)*(1 + xpow(2)))
  ff1a1el = ff1a1el + Hr1(-1)*zeta(2)*(2*xm1(1)*xp1(1)*(1 + xpow(2)))
  ff1a1el = ff1a1el + Hr3(-1,-1,0)*(4*xm1(1)*xp1(1)*(1 + xpow(2)))
  END FUNCTION FF1A1EL


  FUNCTION FF1A2ED()
  ! Calculate the alpha^2 e^-2 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a2ed
  ff1a2ed = + Hr2(0,0)*(xm1(2)*xp1(2)*(1 + xpow(2))**2)
  ff1a2ed = ff1a2ed + 0.5
  ff1a2ed = ff1a2ed + Hr1(0)*(-(xm1(1)*xp1(1)*(1 + xpow(2))))
  END FUNCTION FF1A2ED

  FUNCTION FF1A2ES()
  ! Calculate the alpha^2 e^-1 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a2es
  ff1a2es = + Hr2(-1,0)*(2*xm1(1)*xp1(1)*(1 + xpow(2)))
  ff1a2es = ff1a2es + Hr3(-1,0,0)*(-4*xm1(2)*xp1(2)*(1 + xpow(2))**2)
  ff1a2es = ff1a2es + Hr3(0,-1,0)*(-2*xm1(2)*xp1(2)*(1 + xpow(2))**2)
  ff1a2es = ff1a2es + Hr1(0)*zeta(2)*(-(xm1(2)*xp1(2)*(1 + xpow(2))**2))
  ff1a2es = ff1a2es + Hr3(0,0,0)*(3*xm1(2)*xp1(2)*(1 + xpow(2))**2)
  ff1a2es = ff1a2es + Hr2(0,0)*(2*xm1(2)*xp1(2)*(1 + xpow(2))*(2 + xpow(1) + xpow(2)))
  ff1a2es = ff1a2es + Hr1(0)*(-(xm1(1)*xp1(1)*(7 + 2*xpow(1) + 7*xpow(2)))/2.)
  ff1a2es = ff1a2es + 2
  ff1a2es = ff1a2es + zeta(2)*(xm1(1)*xp1(1)*(1 + xpow(2)))
  END FUNCTION FF1A2ES

  FUNCTION FF1A2EF()
  ! Calculate the alpha^2 e^0 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a2ef
  ff1a2ef = + Hr1(1)*zeta(3)*(4*xm1(2)*xp1(2)*(1 + xpow(2))**2)
  ff1a2ef = ff1a2ef + Hr4(-1,0,-1,0)*(8*xm1(2)*xp1(2)*(1 + xpow(2))**2)
  ff1a2ef = ff1a2ef + Hr4(1,0,1,0)*(8*xm1(2)*xp1(2)*(1 + xpow(2))**2)
  ff1a2ef = ff1a2ef + Hr4(-1,-1,0,0)*(16*xm1(2)*xp1(2)*(1 + xpow(2))**2)
  ff1a2ef = ff1a2ef + log2*zeta(2)*(-12*xp1(2)*(1 - 4*xpow(1) + xpow(2)))
  ff1a2ef = ff1a2ef + Hr1(0)*(-(xm1(1)*xp1(1)*(81 - 10*xpow(1) + 81*xpow(2)))/8.)
  ff1a2ef = ff1a2ef + Hr2(1,0)*(8*xm1(1)*xp1(3)*(2 + xpow(1) + 10*xpow(2) + xpow(3) + 2*xp&
          &ow(4)))
  ff1a2ef = ff1a2ef + Hr3(1,0,0)*(2*xp1(4)*(8 + 15*xpow(1) + 194*xpow(2) + 15*xpow(3) + 8*&
          &xpow(4)))
  ff1a2ef = ff1a2ef + Hr2(-1,0)*(-(xm1(1)*xp1(3)*(55 - 48*xpow(1) + 562*xpow(2) - 48*xpow(&
          &3) + 55*xpow(4)))/2.)
  ff1a2ef = ff1a2ef + Hr1(-1)*zeta(2)*(2*xm1(1)*xp1(4)*(-7 + 24*xpow(1) - 61*xpow(2) + 53*&
          &xpow(3) - 30*xpow(4) + 5*xpow(5)))
  ff1a2ef = ff1a2ef + Hr3(0,-1,0)*(2*xm1(1)*xp1(4)*(-6 + 11*xpow(1) - 173*xpow(2) + 181*xp&
          &ow(3) - 5*xpow(4) + 8*xpow(5)))
  ff1a2ef = ff1a2ef + zeta(2)*((xm1(1)*xp1(4)*(-39 - 7*xpow(1) + 750*xpow(2) - 1074*xpow(3&
          &) + 185*xpow(4) + 57*xpow(5)))/4.)
  ff1a2ef = ff1a2ef + Hr4(0,1,0,0)*(-4*xm1(1)*xp1(5)*(1 + xpow(1) + 30*xpow(2) - 120*xpow(&
          &3) + 30*xpow(4) + xpow(5) + xpow(6)))
  ff1a2ef = ff1a2ef + Hr1(0)*zeta(3)*(4*xm1(1)*xp1(5)*(2 + 11*xpow(1) - 9*xpow(2) + 48*xpo&
          &w(3) - 9*xpow(4) + 11*xpow(5) + 2*xpow(6)))
  ff1a2ef = ff1a2ef + Hr3(-1,0,0)*(-2*xm1(2)*xp1(4)*(5 + 9*xpow(1) + 155*xpow(2) - 202*xpo&
          &w(3) + 153*xpow(4) + 5*xpow(5) + 3*xpow(6)))
  ff1a2ef = ff1a2ef + Hr3(0,1,0)*(-2*xm1(2)*xp1(4)*(5 + 4*xpow(1) + 59*xpow(2) - 72*xpow(3&
          &) + 59*xpow(4) + 4*xpow(5) + 5*xpow(6)))
  ff1a2ef = ff1a2ef + zeta(3)*(-(xm1(2)*xp1(4)*(11 + 48*xpow(1) - 159*xpow(2) + 280*xpow(3&
          &) - 163*xpow(4) + 40*xpow(5) + 7*xpow(6))))
  ff1a2ef = ff1a2ef + Hr1(0)*zeta(2)*(-(xm1(2)*xp1(5)*xpow(1)*(-1 + 99*xpow(1) + 76*xpow(2&
          &) + 150*xpow(3) - 85*xpow(4) - xpow(5) + 18*xpow(6))))
  ff1a2ef = ff1a2ef + Hr2(0,0)*((xm1(2)*xp1(4)*(51 + 4*xpow(1) + 251*xpow(2) - 968*xpow(3)&
          & + 1521*xpow(4) - 60*xpow(5) + 225*xpow(6)))/4.)
  ff1a2ef = ff1a2ef + Hr2(1,0)*zeta(2)*(4*xm1(2)*xp1(5)*(1 + 5*xpow(1) - 19*xpow(2) + 53*x&
          &pow(3) - 39*xpow(4) + 29*xpow(5) + xpow(6) + xpow(7)))
  ff1a2ef = ff1a2ef + Hr4(1,0,0,0)*(8*xm1(2)*xp1(5)*(1 + 4*xpow(1) - 7*xpow(2) + 30*xpow(3&
          &) - 16*xpow(4) + 17*xpow(5) + 2*xpow(6) + xpow(7)))
  ff1a2ef = ff1a2ef + Hr4(0,0,1,0)*(4*xm1(2)*xp1(5)*(-1 - 3*xpow(1) - 5*xpow(2) + 49*xpow(&
          &3) - 35*xpow(4) + 15*xpow(5) + 9*xpow(6) + 3*xpow(7)))
  ff1a2ef = ff1a2ef + Hr3(0,0,0)*(-2*xm1(2)*xp1(5)*(-5 - 10*xpow(1) - 5*xpow(2) + 7*xpow(3&
          &) + 45*xpow(4) - 94*xpow(5) - 7*xpow(6) + 5*xpow(7)))
  ff1a2ef = ff1a2ef + Hr2(0,-1)*zeta(2)*(-2*xm1(2)*xp1(5)*(-7 - 15*xpow(1) - 53*xpow(2) + &
          &131*xpow(3) - 145*xpow(4) + 43*xpow(5) + 9*xpow(6) + 5*xpow(7)))
  ff1a2ef = ff1a2ef + Hr2(0,0)*zeta(2)*(2*xm1(2)*xp1(5)*(-1 - xpow(1) - 16*xpow(2) + 62*xp&
          &ow(3) - 20*xpow(4) + 46*xpow(5) + 19*xpow(6) + 7*xpow(7)))
  ff1a2ef = ff1a2ef + Hr4(0,-1,0,0)*(-2*xm1(2)*xp1(5)*(3 + 3*xpow(1) + 77*xpow(2) - 155*xp&
          &ow(3) + 225*xpow(4) - 27*xpow(5) + 27*xpow(6) + 7*xpow(7)))
  ff1a2ef = ff1a2ef + Hr4(0,0,-1,0)*(-2*xm1(2)*xp1(5)*(-1 + 3*xpow(1) - 49*xpow(2) + 351*x&
          &pow(3) - 281*xpow(4) + 99*xpow(5) + 27*xpow(6) + 11*xpow(7)))
  ff1a2ef = ff1a2ef + Hr4(0,0,0,0)*(xm1(2)*xp1(5)*(7 + 19*xpow(1) + 53*xpow(2) + 121*xpow(&
          &3) + 117*xpow(4) + 117*xpow(5) + 83*xpow(6) + 27*xpow(7)))
  ff1a2ef = ff1a2ef + zeta(4)*((xm1(2)*xp1(5)*(-59 - 113*xpow(1) - 745*xpow(2) + 2973*xpow&
          &(3) - 2119*xpow(4) + 1355*xpow(5) + 479*xpow(6) + 181*xpow(7)))/4.)
  ff1a2ef = ff1a2ef + 11.5
  ff1a2ef = ff1a2ef + Hr3(-1,-1,0)*(-4*xm1(1)*xp1(1)*(1 + xpow(2)))
  ff1a2ef = ff1a2ef + Hr4(-1,0,0,0)*(-12*xm1(2)*xp1(2)*(1 + xpow(2))**2)
  ff1a2ef = ff1a2ef + Hr4(1,0,-1,0)*(-8*xm1(2)*xp1(2)*(1 + xpow(2))**2)
  ff1a2ef = ff1a2ef + Hr4(0,-1,-1,0)*(4*xm1(2)*xp1(2)*(1 + xpow(2))**2)
  ff1a2ef = ff1a2ef + Hr2(-1,0)*zeta(2)*(4*xm1(2)*xp1(2)*(1 + xpow(2))**2)
  END FUNCTION FF1A2EF


  FUNCTION FF2A1EF()
  ! Calculate the alpha^1 e^0 coeff. of F2
  implicit none
  real(kind=prec) :: ff2a1ef
  ff2a1ef = + Hr1(0)*(2*xm1(1)*xp1(1)*xpow(1))
  END FUNCTION FF2A1EF

  FUNCTION FF2A1EL()
  ! Calculate the alpha^1 e^1 coeff. of F2
  implicit none
  real(kind=prec) :: ff2a1el
  ff2a1el = + Hr2(-1,0)*(-4*xm1(1)*xp1(1)*xpow(1))
  ff2a1el = ff2a1el + zeta(2)*(-2*xm1(1)*xp1(1)*xpow(1))
  ff2a1el = ff2a1el + Hr2(0,0)*(2*xm1(1)*xp1(1)*xpow(1))
  ff2a1el = ff2a1el + Hr1(0)*(6*xm1(1)*xp1(1)*xpow(1))
  END FUNCTION FF2A1EL


  FUNCTION FF2A2ES()
  ! Calculate the alpha^2 e^-1 coeff. of F2
  implicit none
  real(kind=prec) :: ff2a2es
  ff2a2es = + Hr1(0)*(-2*xm1(1)*xp1(1)*xpow(1))
  ff2a2es = ff2a2es + Hr2(0,0)*(4*xm1(2)*xp1(2)*xpow(1)*(1 + xpow(2)))
  END FUNCTION FF2A2ES

  FUNCTION FF2A2EF()
  ! Calculate the alpha^2 e^0 coeff. of F2
  implicit none
  real(kind=prec) :: ff2a2ef
  ff2a2ef = Hr2(0,0)*zeta(2)*(-16*xm1(3)*xp1(5)*xpow(2)*(2 - 10*xpow(1) + 9*xpow&
          &(2) - 10*xpow(3) + 2*xpow(4)))
  ff2a2ef = ff2a2ef + Hr4(0,0,-1,0)*(16*xm1(3)*xp1(5)*xpow(2)*(5 - 49*xpow(1) + 84*xpow(2)&
          & - 49*xpow(3) + 5*xpow(4)))
  ff2a2ef = ff2a2ef + Hr1(-1)*zeta(2)*(12*xm1(2)*xp1(4)*xpow(1)*(5 - 12*xpow(1) + 26*xpow(&
          &2) - 12*xpow(3) + 5*xpow(4)))
  ff2a2ef = ff2a2ef + Hr4(0,-1,0,0)*(-16*xm1(3)*xp1(5)*xpow(2)*(7 - 31*xpow(1) + 50*xpow(2&
          &) - 31*xpow(3) + 7*xpow(4)))
  ff2a2ef = ff2a2ef + zeta(2)**2*((-24*xm1(3)*xp1(5)*xpow(2)*(11 - 77*xpow(1) + 109*xpow(2&
          &) - 77*xpow(3) + 11*xpow(4)))/5.)
  ff2a2ef = ff2a2ef + Hr3(0,0,0)*(-4*xm1(2)*xp1(5)*xpow(1)*(-3 - xpow(1) + xpow(2) - 23*xp&
          &ow(3) + 46*xpow(4) + 8*xpow(5)))
  ff2a2ef = ff2a2ef + Hr1(0)*zeta(2)*(-4*xm1(2)*xp1(5)*xpow(1)*(3 - 4*xpow(1) + 22*xpow(2)&
          & - 2*xpow(3) + 43*xpow(4) + 14*xpow(5)))
  ff2a2ef = ff2a2ef + Hr1(0)*((-27*xm1(1)*xp1(1)*xpow(1))/2.)
  ff2a2ef = ff2a2ef + log2*zeta(2)*(-48*xp1(2)*xpow(1))
  ff2a2ef = ff2a2ef + Hr4(0,1,0,0)*(80*xm1(1)*xp1(5)*xpow(2)*(1 - 7*xpow(1) + xpow(2)))
  ff2a2ef = ff2a2ef + Hr3(0,1,0)*(-8*xm1(2)*xp1(4)*xpow(1)*(1 - 6*xpow(1) + xpow(2))*(1 - &
          &4*xpow(1) + xpow(2)))
  ff2a2ef = ff2a2ef + Hr2(1,0)*(32*xm1(1)*xp1(3)*xpow(1)*(1 - xpow(1) + xpow(2)))
  ff2a2ef = ff2a2ef + Hr4(1,0,0,0)*(-96*xm1(1)*xp1(5)*xpow(2)*(1 - xpow(1) + xpow(2)))
  ff2a2ef = ff2a2ef + Hr2(1,0)*zeta(2)*(-96*xm1(1)*xp1(5)*xpow(2)*(1 - xpow(1) + xpow(2)))
  ff2a2ef = ff2a2ef + Hr1(0)*zeta(3)*(112*xm1(1)*xp1(5)*xpow(2)*(1 - xpow(1) + xpow(2)))
  ff2a2ef = ff2a2ef + Hr2(0,-1)*zeta(2)*(48*xm1(3)*xp1(5)*xpow(2)*(1 - 8*xpow(1) + xpow(2)&
          &)*(1 - xpow(1) + xpow(2)))
  ff2a2ef = ff2a2ef + Hr4(0,0,0,0)*(8*xm1(3)*xp1(5)*xpow(2)*(1 - xpow(1) + xpow(2))*(1 + 4&
          &*xpow(1) + xpow(2)))
  ff2a2ef = ff2a2ef + Hr2(-1,0)*(-6*xm1(1)*xp1(3)*xpow(1)*(13 - 38*xpow(1) + 13*xpow(2)))
  ff2a2ef = ff2a2ef + Hr4(0,0,1,0)*(192*xm1(1)*xp1(5)*xpow(3))
  ff2a2ef = ff2a2ef + zeta(2)*(xm1(1)*xp1(4)*xpow(1)*(-15 - 173*xpow(1) + 323*xpow(2) + xp&
          &ow(3)))
  ff2a2ef = ff2a2ef + Hr2(0,0)*(xm1(1)*xp1(4)*xpow(1)*(-13 + 49*xpow(1) - 199*xpow(2) + 12&
          &3*xpow(3)))
  ff2a2ef = ff2a2ef + Hr3(-1,0,0)*(-4*xm1(2)*xp1(4)*xpow(1)*(1 - 64*xpow(1) + 122*xpow(2) &
          &- 64*xpow(3) + xpow(4)))
  ff2a2ef = ff2a2ef + zeta(3)*(4*xm1(2)*xp1(4)*xpow(1)*(1 - 52*xpow(1) + 62*xpow(2) - 52*x&
          &pow(3) + xpow(4)))
  ff2a2ef = ff2a2ef + Hr3(0,-1,0)*(8*xm1(2)*xp1(4)*xpow(1)*(1 - 49*xpow(1) + 92*xpow(2) - &
          &49*xpow(3) + xpow(4)))
  ff2a2ef = ff2a2ef + Hr3(1,0,0)*(-8*xm1(2)*xp1(4)*xpow(1)*(1 + 47*xpow(1) - 88*xpow(2) + &
          &47*xpow(3) + xpow(4)))
  END FUNCTION FF2A2EF

                          !!!!!!!!!!!!!!!!!!!!!!
                           END MODULE mue_heavy_quark
                          !!!!!!!!!!!!!!!!!!!!!!

