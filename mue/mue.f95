
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MUE
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use phase_space, only: ksoft, ksoftA, ksoftB
  use mue_mat_el


!!!  use amplitude_lo, only: amplitude_n_LO

  implicit none

  contains


  FUNCTION EM2EM_EE_part(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! both massive and massless electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  type(particles) :: em2em_ee_part
  em2em_ee_part = parts((/part(p1, 1, 1, 1), part(p3, 1, -1, 1)/), "e")
  END FUNCTION EM2EM_EE_part

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
  !! both massive and massless electrons
  FUNCTION EM2EM_EM_part(p1, p2, p3, p4)
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  type(particles) :: em2em_em_part
  em2em_em_part = parts((/part(p1, 1, 1, 1), part(p2, 1, 1, 2), part(p3, 1, -1, 1), part(p4, 1, -1, 2)/), "x")
  END FUNCTION EM2EM_EM_part

  FUNCTION EM2EM_MM_part(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! both massive and massless electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  type(particles) :: em2em_mm_part
  em2em_mm_part = parts((/part(p2, 1, 1, 2), part(p4, 1, -1, 2)/), "m")
  END FUNCTION EM2EM_MM_part

  FUNCTION EM2EM_part(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! both massive and massless electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  type(particles) :: em2em_part
#include "charge_config.h"
  em2em_part = parts((/part(p1, Qe, 1, 1), part(p2, Qmu, 1, 2), part(p3, Qe, -1, 1), part(p4, Qmu, -1, 2)/), "*")
  END FUNCTION EM2EM_part

  FUNCTION MP2MP_part(p1, p2, p3, p4)
    !! mu-(p1) p(p2) -> mu-(p3) p(p4)
    !! for massive (and massless) muons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  type(particles) :: mp2mp_part
  mp2mp_part = parts((/part(p1, 1, 1, 2), part(p3, 1, -1, 2)/), "m")
  END FUNCTION MP2MP_part



  FUNCTION EE2MM_EE_part(p1, p2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  type(particles) :: ee2mm_ee_part
  ee2mm_ee_part = parts((/part(p1, 1, 1), part(p2,-1, 1)/))
  END FUNCTION EE2MM_EE_part

  FUNCTION EE2MM_part(p1, p2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  type(particles) :: ee2mm_part
  ee2mm_part = parts((/part(p1, 1, 1), part(p2,-1, 1), part(p3, 1, -1), part(p4,-1, -1)/))
  END FUNCTION EE2MM_part

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!        SINGULAR    LIMITS          !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  FUNCTION EM2EMG_EM_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emg_em_s
#include "charge_config.h"

  em2emg_em_s = 2*eik(ksoft, parts((/part(p1, Qe, 1, 1), part(p2, Qmu, 1, 2), part(q1, Qe, -1, 1), part(q2, Qmu, -1, 2)/), "x")) &
                 *em2em(p1,p2,q1,q2)
  em2emg_em_s = (4.*pi*alpha)*em2emg_em_s
  END FUNCTION EM2EMG_EM_S

  FUNCTION MP2MPG_MP_S(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) g(ksoft)
    !! for massive (and massless) muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mpg_mp_s
#include "charge_config.h"

  mp2mpg_mp_s = 2*eik(ksoft, parts((/part(p1, Qmu, 1, 1), part(p2, Qp, 1, 2), part(q1, Qmu, -1, 1), part(q2, Qp, -1, 2)/), "x")) &
                 *mp2mp(p1,p2,q1,q2)
  mp2mpg_mp_s = (4.*pi*alpha)*mp2mpg_mp_s
  END FUNCTION MP2MPG_MP_S


          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!              NNLO                  !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  FUNCTION EM2EMG_AEM_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emg_aem_s
#include "charge_config.h"

  em2emg_aem_s = 2*eik(ksoft, parts((/part(p1, Qe, 1, 1), part(p2, Qmu, 1, 2), part(q1, Qe, -1, 1), part(q2, Qmu, -1, 2)/), "x")) &
                 *em2em_a(p1,p2,q1,q2)
  em2emg_aem_s = (4.*pi*alpha)*em2emg_aem_s
  END FUNCTION EM2EMG_AEM_S


  FUNCTION EM2EMGL_MIXD_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emgl_mixd_s
#include "charge_config.h"

  em2emgl_mixd_s =   2*eik(ksoft, parts((/part(p1,Qe,+1,1), part(p2,Qmu,+1,2),         &
                                          part(q1,Qe,-1,1), part(q2,Qmu,-1,2)/), "x")) &
                      * em2eml(p1,p2,q1,q2)                                            &
                   + 2*eik(ksoft, parts((/part(p2, Qmu, 1), part(q2, Qmu, -1)/)))      &
                      * (em2eml_ee(p1,p2,q1,q2) + em2eml_em(p1,p2,q1,q2))              &
                   + 2*eik(ksoft, parts((/part(p1, Qe, 1), part(q1, Qe, -1)/)))        &
                      * (em2eml_mm(p1,p2,q1,q2) + em2eml_em(p1,p2,q1,q2))
  em2emgl_mixd_s = (4.*pi*alpha)*em2emgl_mixd_s
  END FUNCTION EM2EMGL_MIXD_S

  FUNCTION EM2EMGL_E3M1_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emgl_e3m1_s
#include "charge_config.h"

  em2emgl_e3m1_s =   2*eik(ksoft, parts((/part(p1,Qe,+1,1), part(p2,Qmu,+1,2),        &
                                          part(q1,Qe,-1,1), part(q2,Qmu,-1,2)/),"x")) &
                      * em2eml_ee(p1,p2,q1,q2)                                        &
                   + 2*eik(ksoft, parts((/part(p1, Qe, 1), part(q1, Qe, -1)/)))       &
                      * em2eml_em(p1,p2,q1,q2)
  em2emgl_e3m1_s = (4.*pi*alpha)*em2emgl_e3m1_s
  END FUNCTION EM2EMGL_E3M1_S

  FUNCTION EM2EMGL_E2M2_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emgl_e2m2_s
#include "charge_config.h"

  em2emgl_e2m2_s =   2*eik(ksoft, parts((/part(p1,Qe,+1,1), part(p2,Qmu,+1,2),        &
                                          part(q1,Qe,-1,1), part(q2,Qmu,-1,2)/),"x")) &
                      * em2eml_em(p1,p2,q1,q2)                                        &
                   + 2*eik(ksoft, parts((/part(p2, Qmu, 1), part(q2, Qmu,-1)/)))      &
                      * em2eml_ee(p1,p2,q1,q2)                                        &
                   + 2*eik(ksoft, parts((/part(p1, Qe, 1), part(q1, Qe, -1)/)))       &
                      * em2eml_mm(p1,p2,q1,q2)
  em2emgl_e2m2_s = (4.*pi*alpha)*em2emgl_e2m2_s
  END FUNCTION EM2EMGL_E2M2_S

  FUNCTION EM2EMGL_E1M3_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emgl_e1m3_s
#include "charge_config.h"

  em2emgl_e1m3_s =   2*eik(ksoft, parts((/part(p1,Qe,+1,1), part(p2,Qmu,+1,2),        &
                                          part(q1,Qe,-1,1), part(q2,Qmu,-1,2)/),"x")) &
                      * em2eml_mm(p1,p2,q1,q2)                                        &
                   + 2*eik(ksoft, parts((/part(p2, Qmu, 1), part(q2, Qmu,-1)/)))      &
                      * em2eml_em(p1,p2,q1,q2)
  em2emgl_e1m3_s = (4.*pi*alpha)*em2emgl_e1m3_s
  END FUNCTION EM2EMGL_E1M3_S

  FUNCTION MP2MPGL_S(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(ksoft)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mpgl_s
#include "charge_config.h"

  mp2mpgl_s =  2*eik(ksoft, parts((/part(p1, 1, 1), part(q1, 1, -1)/))) * mp2mpl(p1,p2,q1,q2)
  mp2mpgl_s = (4.*pi*alpha)*mp2mpgl_s
  END FUNCTION MP2MPGL_S

  FUNCTION EM2EMGf_MIXD_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emgf_mixd_s
  real (kind=prec) :: Epart
  real (kind=prec) :: mat0, mat0e, mat0m, mat0x
  real (kind=prec) :: ctfine, ctfinm, ctfinx
#include "charge_config.h"

  Epart  = sqrt(sq(p1+p2))

  mat0e = 2*eik(ksoft, parts((/part(p1, 1, 1), part(q1, 1, -1)/)))
  mat0m = 2*eik(ksoft, parts((/part(p2, 1, 1), part(q2, 1, -1)/)))
  mat0x = 2*eik(ksoft, parts((/part(p1, Qe, 1, 1), part(p2, Qmu, 1, 2), part(q1, Qe, -1, 1), part(q2, Qmu, -1, 2)/), "x"))

  mat0 = em2em(p1,p2,q1,q2)
  mat0e = (4*pi*alpha)*mat0e * mat0
  mat0m = (4*pi*alpha)*mat0m * mat0
  mat0x = (4*pi*alpha)*mat0x * mat0

  ctfine = em2emeik_ee(p1,p2,q1,q2,xieik1)
  ctfinm = em2emeik_ee(p2,p1,q2,q1,xieik1)
  ctfinx = em2emeik_em(p1,p2,q1,q2,xieik1)
  !ctfinx = (alpha/2/pi) * 2*(  Ireg(xieik2,Epart,p1,q2) + Ireg(xieik2,Epart,p2,q1)  &
  !                           - Ireg(xieik2,Epart,p1,p2) - Ireg(xieik2,Epart,q1,q2))

  em2emgf_mixd_s = em2emgl_mixd_s(p1,p2,q1,q2)
  em2emgf_mixd_s = em2emgf_mixd_s + ctfinE*(mat0m+mat0x) + ctfinM*(mat0e+mat0x) + ctfinX*(mat0e+mat0m+mat0x)
  END FUNCTION EM2EMGf_MIXD_S

  FUNCTION EM2EMGf_E3M1_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emgf_e3m1_s
  real (kind=prec) :: Epart
  real (kind=prec) :: mat0, mat0e, mat0m, mat0x
  real (kind=prec) :: ctfine, ctfinm, ctfinx
#include "charge_config.h"

  Epart  = sqrt(sq(p1+p2))

  mat0e = 2*eik(ksoft, parts((/part(p1, 1, 1), part(q1, 1, -1)/)))
  mat0m = 2*eik(ksoft, parts((/part(p2, 1, 1), part(q2, 1, -1)/)))
  mat0x = 2*eik(ksoft, parts((/part(p1, Qe, 1, 1), part(p2, Qmu, 1, 2), part(q1, Qe, -1, 1), part(q2, Qmu, -1, 2)/), "x"))

  mat0 = em2em(p1,p2,q1,q2)
  mat0e = (4*pi*alpha)*mat0e * mat0
  mat0m = (4*pi*alpha)*mat0m * mat0
  mat0x = (4*pi*alpha)*mat0x * mat0

  ctfine = em2emeik_ee(p1,p2,q1,q2,xieik1)
  ctfinm = em2emeik_ee(p2,p1,q2,q1,xieik1)
  ctfinx = em2emeik_em(p1,p2,q1,q2,xieik1)

  em2emgf_e3m1_s = em2emgl_e3m1_s(p1,p2,q1,q2)
  em2emgf_e3m1_s = em2emgf_e3m1_s + ctfinE*(mat0x) + ctfinM*(0._prec) + ctfinX*(mat0e)
  END FUNCTION EM2EMGf_E3M1_S

  FUNCTION EM2EMGf_E2M2_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emgf_e2m2_s
  real (kind=prec) :: Epart
  real (kind=prec) :: mat0, mat0e, mat0m, mat0x
  real (kind=prec) :: ctfine, ctfinm, ctfinx
#include "charge_config.h"

  Epart  = sqrt(sq(p1+p2))

  mat0e = 2*eik(ksoft, parts((/part(p1, 1, 1), part(q1, 1, -1)/)))
  mat0m = 2*eik(ksoft, parts((/part(p2, 1, 1), part(q2, 1, -1)/)))
  mat0x = 2*eik(ksoft, parts((/part(p1, Qe, 1, 1), part(p2, Qmu, 1, 2), part(q1, Qe, -1, 1), part(q2, Qmu, -1, 2)/), "x"))

  mat0 = em2em(p1,p2,q1,q2)
  mat0e = (4*pi*alpha)*mat0e * mat0
  mat0m = (4*pi*alpha)*mat0m * mat0
  mat0x = (4*pi*alpha)*mat0x * mat0

  ctfine = em2emeik_ee(p1,p2,q1,q2,xieik1)
  ctfinm = em2emeik_ee(p2,p1,q2,q1,xieik1)
  ctfinx = em2emeik_em(p1,p2,q1,q2,xieik1)

  em2emgf_e2m2_s = em2emgl_e2m2_s(p1,p2,q1,q2)
  em2emgf_e2m2_s = em2emgf_e2m2_s + ctfinE*(mat0m) + ctfinM*(mat0e) + ctfinX*(mat0x)
  END FUNCTION EM2EMGf_E2M2_S

  FUNCTION EM2EMGf_E1M3_S(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2emgf_e1m3_s
  real (kind=prec) :: Epart
  real (kind=prec) :: mat0, mat0e, mat0m, mat0x
  real (kind=prec) :: ctfine, ctfinm, ctfinx
#include "charge_config.h"

  Epart  = sqrt(sq(p1+p2))

  mat0e = 2*eik(ksoft, parts((/part(p1, 1, 1), part(q1, 1, -1)/)))
  mat0m = 2*eik(ksoft, parts((/part(p2, 1, 1), part(q2, 1, -1)/)))
  mat0x = 2*eik(ksoft, parts((/part(p1, Qe, 1, 1), part(p2, Qmu, 1, 2), part(q1, Qe, -1, 1), part(q2, Qmu, -1, 2)/), "x"))

  mat0 = em2em(p1,p2,q1,q2)
  mat0e = (4*pi*alpha)*mat0e * mat0
  mat0m = (4*pi*alpha)*mat0m * mat0
  mat0x = (4*pi*alpha)*mat0x * mat0

  ctfine = em2emeik_ee(p1,p2,q1,q2,xieik1)
  ctfinm = em2emeik_ee(p2,p1,q2,q1,xieik1)
  ctfinx = em2emeik_em(p1,p2,q1,q2,xieik1)

  em2emgf_e1m3_s = em2emgl_e1m3_s(p1,p2,q1,q2)
  em2emgf_e1m3_s = em2emgf_e1m3_s + ctfinE*(0._prec) + ctfinM*(mat0x) + ctfinX*(mat0m)
  END FUNCTION EM2EMGf_E1M3_S

  FUNCTION EM2EMGG_MIXD_SH(p1, p2, q1, q2, q4)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q4(4)
  real (kind=prec) :: eiktot, eikee, eikmm, eikem
  real (kind=prec) :: em2emgg_mixd_sh
#include "charge_config.h"

  eiktot = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikee  = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmm  = 2*eik(ksoftA, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikem  = 2*(eik(ksoftA, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                  part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_mixd_sh =   eiktot         * em2emg_em(p1,p2,q1,q2,q4) &
                   + (eikee + eikem) * em2emg_mm(p1,p2,q1,q2,q4) &
                   + (eikmm + eikem) * em2emg_ee(p1,p2,q1,q2,q4)
  em2emgg_mixd_sh = (4.*pi*alpha)*em2emgg_mixd_sh

  END FUNCTION EM2EMGG_MIXD_SH

  FUNCTION EM2EMGG_E3M1_SH(p1, p2, q1, q2, q4)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q4(4)
  real (kind=prec) :: eikee, eikmm, eikem
  real (kind=prec) :: em2emgg_e3m1_sh
#include "charge_config.h"

  !eiktot = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikee  = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmm  = 2*eik(ksoftA, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikem  = 2*(eik(ksoftA, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                  part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_e3m1_sh =  (eikee  ) * em2emg_em(p1,p2,q1,q2,q4) &
                   + (0._prec) * em2emg_mm(p1,p2,q1,q2,q4) &
                   + (eikem  ) * em2emg_ee(p1,p2,q1,q2,q4)
  em2emgg_e3m1_sh = (4.*pi*alpha)*em2emgg_e3m1_sh

  END FUNCTION EM2EMGG_E3M1_SH

  FUNCTION EM2EMGG_E2M2_SH(p1, p2, q1, q2, q4)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q4(4)
  real (kind=prec) :: eikee, eikmm, eikem
  real (kind=prec) :: em2emgg_e2m2_sh
#include "charge_config.h"

  !eiktot = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikee  = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmm  = 2*eik(ksoftA, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikem  = 2*(eik(ksoftA, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                  part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_e2m2_sh =  (eikem) * em2emg_em(p1,p2,q1,q2,q4) &
                   + (eikee) * em2emg_mm(p1,p2,q1,q2,q4) &
                   + (eikmm) * em2emg_ee(p1,p2,q1,q2,q4)
  em2emgg_e2m2_sh = (4.*pi*alpha)*em2emgg_e2m2_sh

  END FUNCTION EM2EMGG_E2M2_SH

  FUNCTION EM2EMGG_E1M3_SH(p1, p2, q1, q2, q4)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q4(4)
  real (kind=prec) :: eikee, eikmm, eikem
  real (kind=prec) :: em2emgg_e1m3_sh
#include "charge_config.h"

  !eiktot = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikee  = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmm  = 2*eik(ksoftA, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikem  = 2*(eik(ksoftA, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                  part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_e1m3_sh =  (eikmm )  * em2emg_em(p1,p2,q1,q2,q4) &
                   + (eikem )  * em2emg_mm(p1,p2,q1,q2,q4) &
                   + (0._prec) * em2emg_ee(p1,p2,q1,q2,q4)
  em2emgg_e1m3_sh = (4.*pi*alpha)*em2emgg_e1m3_sh

  END FUNCTION EM2EMGG_E1M3_SH

  FUNCTION EM2EMGG_MIXD_HS(p1, p2, q1, q2, q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: eiktot, eikee, eikmm, eikem
  real (kind=prec) :: em2emgg_mixd_hs
#include "charge_config.h"

  eiktot = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikee  = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmm  = 2*eik(ksoftB, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikem  = 2*(eik(ksoftB, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                  part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_mixd_hs =   eiktot         * em2emg_em(p1,p2,q1,q2,q3) &
                   + (eikee + eikem) * em2emg_mm(p1,p2,q1,q2,q3) &
                   + (eikmm + eikem) * em2emg_ee(p1,p2,q1,q2,q3)
  em2emgg_mixd_hs = (4.*pi*alpha)*em2emgg_mixd_hs

  END FUNCTION EM2EMGG_MIXD_HS

  FUNCTION EM2EMGG_E3M1_HS(p1, p2, q1, q2, q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: eikee, eikmm, eikem
  real (kind=prec) :: em2emgg_e3m1_hs
#include "charge_config.h"

  !eiktot = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikee  = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmm  = 2*eik(ksoftB, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikem  = 2*(eik(ksoftB, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                  part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_e3m1_hs =  (eikee  ) * em2emg_em(p1,p2,q1,q2,q3) &
                   + (0._prec) * em2emg_mm(p1,p2,q1,q2,q3) &
                   + (eikem  ) * em2emg_ee(p1,p2,q1,q2,q3)
  em2emgg_e3m1_hs = (4.*pi*alpha)*em2emgg_e3m1_hs

  END FUNCTION EM2EMGG_E3M1_HS

  FUNCTION EM2EMGG_E2M2_HS(p1, p2, q1, q2, q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: eikee, eikmm, eikem
  real (kind=prec) :: em2emgg_e2m2_hs
#include "charge_config.h"

  !eiktot = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikee  = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmm  = 2*eik(ksoftB, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikem  = 2*(eik(ksoftB, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                  part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_e2m2_hs =  (eikem) * em2emg_em(p1,p2,q1,q2,q3) &
                   + (eikee) * em2emg_mm(p1,p2,q1,q2,q3) &
                   + (eikmm) * em2emg_ee(p1,p2,q1,q2,q3)
  em2emgg_e2m2_hs = (4.*pi*alpha)*em2emgg_e2m2_hs

  END FUNCTION EM2EMGG_E2M2_HS

  FUNCTION EM2EMGG_E1M3_HS(p1, p2, q1, q2, q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: eikee, eikmm, eikem
  real (kind=prec) :: em2emgg_e1m3_hs
#include "charge_config.h"

  !eiktot = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikee  = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmm  = 2*eik(ksoftB, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikem  = 2*(eik(ksoftB, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                  part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_e1m3_hs =  (eikmm )  * em2emg_em(p1,p2,q1,q2,q3) &
                   + (eikem )  * em2emg_mm(p1,p2,q1,q2,q3) &
                   + (0._prec) * em2emg_ee(p1,p2,q1,q2,q3)
  em2emgg_e1m3_hs = (4.*pi*alpha)*em2emgg_e1m3_hs

  END FUNCTION EM2EMGG_E1M3_HS

  FUNCTION EM2EMGG_MIXD_SS(p1, p2, q1, q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: eiktotA, eikeeA, eikmmA, eikemA
  real (kind=prec) :: eiktotB, eikeeB, eikmmB, eikemB
  real (kind=prec) :: em2emgg_mixd_ss
#include "charge_config.h"

  eiktotA = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikeeA  = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmmA  = 2*eik(ksoftA, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikemA  = 2*(eik(ksoftA, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                   part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))
  eiktotB = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikeeB  = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmmB  = 2*eik(ksoftB, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikemB  = 2*(eik(ksoftB, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                   part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_mixd_ss = ( eikemA*eikemB + eikeeA*eikmmB + eikeeB*eikmmA        &
                     + (eikeeA + eikmmA)*eikemB + (eikeeB + eikmmB)*eikemA  ) *em2em(p1,p2,q1,q2)
  em2emgg_mixd_ss = (4.*pi*alpha)**2*em2emgg_mixd_ss

  END FUNCTION EM2EMGG_MIXD_SS

  FUNCTION EM2EMGG_E3M1_SS(p1, p2, q1, q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: eikeeA, eikmmA, eikemA
  real (kind=prec) :: eikeeB, eikmmB, eikemB
  real (kind=prec) :: em2emgg_e3m1_ss
#include "charge_config.h"

  !eiktotA = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikeeA  = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmmA  = 2*eik(ksoftA, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikemA  = 2*(eik(ksoftA, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                   part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))
  !eiktotB = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikeeB  = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmmB  = 2*eik(ksoftB, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikemB  = 2*(eik(ksoftB, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                   part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_e3m1_ss = ( 0._prec + (eikeeA)*eikemB + (eikeeB)*eikemA ) * em2em(p1,p2,q1,q2)
  em2emgg_e3m1_ss = (4.*pi*alpha)**2*em2emgg_e3m1_ss

  END FUNCTION EM2EMGG_E3M1_SS

  FUNCTION EM2EMGG_E2M2_SS(p1, p2, q1, q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: eikeeA, eikmmA, eikemA
  real (kind=prec) :: eikeeB, eikmmB, eikemB
  real (kind=prec) :: em2emgg_e2m2_ss
#include "charge_config.h"

  !eiktotA = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikeeA  = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmmA  = 2*eik(ksoftA, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikemA  = 2*(eik(ksoftA, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                   part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))
  !eiktotB = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikeeB  = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmmB  = 2*eik(ksoftB, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikemB  = 2*(eik(ksoftB, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                   part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_e2m2_ss = ( eikemA*eikemB + eikeeA*eikmmB + eikeeB*eikmmA + 0._prec ) * em2em(p1,p2,q1,q2)
  em2emgg_e2m2_ss = (4.*pi*alpha)**2*em2emgg_e2m2_ss

  END FUNCTION EM2EMGG_E2M2_SS

  FUNCTION EM2EMGG_E1M3_SS(p1, p2, q1, q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(ksoft)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: eikeeA, eikmmA, eikemA
  real (kind=prec) :: eikeeB, eikmmB, eikemB
  real (kind=prec) :: em2emgg_e1m3_ss
#include "charge_config.h"

  !eiktotA = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikeeA  = 2*eik(ksoftA, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmmA  = 2*eik(ksoftA, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikemA  = 2*(eik(ksoftA, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                   part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))
  !eiktotB = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(p2,Qmu,+1),part(q1,Qe,-1),part(q2,Qmu,-1)/)))
  eikeeB  = 2*eik(ksoftB, parts((/part(p1,Qe,+1),part(q1,Qe,-1)/)))
  eikmmB  = 2*eik(ksoftB, parts((/part(p2,Qmu,+1),part(q2,Qmu,-1)/)))
  eikemB  = 2*(eik(ksoftB, parts((/part(p1,Qe,+1,1),part(p2,Qmu,+1,2), &
                                   part(q1,Qe,-1,1),part(q2,Qmu,-1,2)/),"x")))

  em2emgg_e1m3_ss = ( 0._prec + (eikmmA)*eikemB + (eikmmB)*eikemA ) * em2em(p1,p2,q1,q2)
  em2emgg_e1m3_ss = (4.*pi*alpha)**2*em2emgg_e1m3_ss

  END FUNCTION EM2EMGG_E1M3_SS

                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE MUE
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
