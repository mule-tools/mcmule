
                          !!!!!!!!!!!!!!!!!!!!!!
                            MODULE mue_PEPE2MM
                          !!!!!!!!!!!!!!!!!!!!!!
    use functions
    implicit none


  contains

  FUNCTION PEPE2MMGG_EEEE(p1,pol1,p2,pol2,p3,p4,p5,p6)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5) g(p6)
    !! for massive electrons & positrons
  use mue_pepe2mmgl, only: PEPE2MMGG_A1, PEPE2MMGG_A3
  implicit none
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4), p6(4), pol1(4), pol2(4)
  real (kind=prec) :: pepe2mmgg_eeee
  real (kind=prec) :: ss, tt, me2, mm2, s2n, s3n, s4n, s1m, s3m, s4m, snm
  real (kind=prec) :: s15, s35, s25, s16, s26, s36, s56, s6n, s6m

  ss = sq(p1+p2); tt = sq(p1-p3)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)
  s16 = s(p1,p6); s26 = s(p2,p6); s36 = s(p3,p6); s56 = s(p5,p6)
  me2 = sq(p1); mm2 = sq(p3);
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4); s6n = s(pol1,p6)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4); s6m = s(pol2,p6)
  snm = s(pol1,pol2)

  pepe2mmgg_eeee = pepe2mmgg_a1(me2, mm2, ss, tt, s15, s25, s35, s16, s26, s36, s56, s2n, s3n, s4n,&
                                s6n, s1m, s3m, s4m, s6m, snm)&
                 + pepe2mmgg_a3(me2, mm2, ss, tt, s15, s25, s35, s16, s26, s36, s56, s2n, s3n, s4n,&
                                s6n, s1m, s3m, s4m, s6m, snm)&
                 + pepe2mmgg_a1(me2, mm2, ss, 2*me2+2*mm2-ss+s15+s25+s16+s26-s35-s36-s56-tt, s25,&
                                s15, s35, s26, s16, s36, s56, s1m, s3m, s4m, s6m, s2n, s3n, s4n,&
                                s6n, snm)&
                 + pepe2mmgg_a3(me2, mm2, ss, 2*me2+2*mm2-ss+s15+s25+s16+s26-s35-s36-s56-tt, s25,&
                                s15, s35, s26, s16, s36, s56, s1m, s3m, s4m, s6m, s2n, s3n, s4n,&
                                s6n, snm)&
                 + pepe2mmgg_a3(me2, mm2, ss, tt, s16, s26, s36, s15, s25, s35, s56, s2n, s3n, s4n,&
                                s2n - s3n - s4n - s6n, s1m, s3m, s4m, s1m - s3m - s4m - s6m, snm)&
                 + pepe2mmgg_a3(me2, mm2, ss, 2*me2+2*mm2-ss+s15+s16+s25+s26-s35-s36-s56-tt, s26,&
                                s16, s36, s25, s15, s35, s56, s1m, s3m, s4m,s1m-s3m-s4m-s6m, s2n,&
                                s3n, s4n, s2n-s3n-s4n-s6n, snm)

  pepe2mmgg_eeee = pepe2mmgg_eeee*0.25

  END FUNCTION PEPE2MMGG_EEEE

  FUNCTION PEPE2MMGL_EEEE_PVRED(p1,pol1,p2,pol2,p3,p4,p5,sing)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons
  use mue_pepe2mmgl, only: PEPE2MMGL_CTF, PEPE2MMGL_CTS, &
                           PEPE2MMGL_TFASYM, PEPE2MMGL_BFASYM, &
                           PEPE2MMGL_BSASYM, &
                           PEPE2MMGL_BF, PEPE2MMGL_BS, &
                           PEPE2MMGL_TF, PEPE2MMGL_TS, &
                           PEPE2MMGL_TGF, PEPE2MMGL_TGS, &
                           PEPE2MMGL_SF, PEPE2MMGL_SS
  implicit none
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4), pol1(4), pol2(4)
  real (kind=prec) :: pepe2mmgl_eeee_pvred
  real (kind=prec), optional :: sing
  real (kind=prec) :: ss, tt, me2, mm2, s2n, s3n, s4n, s1m, s3m, s4m, snm
  real (kind=prec) :: me1, mu2
  real (kind=prec) :: s15, s35, s25
  real (kind=prec) :: asym123n1, asym123n2, asym124n1, asym124n2, asym125n1, asym125n2
  real (kind=prec) :: asym135n1, asym135n2, asym145n1, asym145n2, asym235n1, asym235n2
  real (kind=prec) :: asym245n1, asym245n2, asym1235, asym1245
  real (kind=prec) :: logms15, logmum, logms25, logsm
  real (kind=prec) :: logssqrt, logssqrtIm, logs125sqrt, logs125sqrtIm
  real (kind=prec) :: logssqrt2, logssqrt2Im, logs125sqrt2, logs125sqrt2Im
  real (kind=prec) :: polyms15, polyms25
  real (kind=prec) :: discb125, discb125Im, discbsm, discbsIm
  real (kind=prec) :: scalarc0s15, scalarc0s15Im, scalarc0s25, scalarc0s25Im
  real (kind=prec) :: scalarc0ms25, scalarc0ms15, scalarc0ss125, scalarc0ss125Im
  real (kind=prec) :: scalard0s25, scalard0s25Im, scalard0s15, scalard0s15Im
  real (kind=prec) :: scalarc0ir6s, scalarc0ir6sIm
  complex (kind=prec) :: pvc15, pvc25, pvd15, pvd25

  ss = sq(p1+p2); tt = sq(p1-p3); s15 = s(p1,p5); s35 = s(p3,p5); s25 = s(p2,p5)
  me2 = sq(p1); mm2 = sq(p3); me1 = sqrt(me2)
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)
  mu2 = musq

  call SetDeltaUV_cll(0.); call SetDeltaIR_cll(0.,0.)
  call SetMuUV2_cll(musq); call SetMuIR2_cll(musq)

  call C0_cll(pvc15, &
    cmplx(me2), cmplx(ss-s15-s25), cmplx(me2-s15),&
    cmplx(0._prec), cmplx(me2), cmplx(me2))
  call C0_cll(pvc25, &
    cmplx(me2), cmplx(ss-s15-s25), cmplx(me2-s25),&
    cmplx(0._prec), cmplx(me2), cmplx(me2))
  call D0_cll(pvd15, &
    cmplx(me2), cmplx(me2), cmplx(ss-s15-s25), cmplx(0._prec), cmplx(ss), &
    cmplx(me2-s15), cmplx(me2), cmplx(0._prec), cmplx(me2), cmplx(me2))
  call D0_cll(pvd25, &
    cmplx(me2), cmplx(me2), cmplx(ss-s15-s25), cmplx(0._prec), cmplx(ss), &
    cmplx(me2-s25), cmplx(me2), cmplx(0._prec), cmplx(me2), cmplx(me2))

  asym123n1 = asymtensor(p1,p2,p3,pol1)
  asym123n2 = asymtensor(p1,p2,p3,pol2)
  asym124n1 = asymtensor(p1,p2,p4,pol1)
  asym124n2 = asymtensor(p1,p2,p4,pol2)
  asym125n1 = asymtensor(p1,p2,p5,pol1)
  asym125n2 = asymtensor(p1,p2,p5,pol2)
  asym135n1 = asymtensor(p1,p3,p5,pol1)
  asym135n2 = asymtensor(p1,p3,p5,pol2)
  asym145n1 = asymtensor(p1,p4,p5,pol1)
  asym145n2 = asymtensor(p1,p4,p5,pol2)
  asym235n1 = asymtensor(p2,p3,p5,pol1)
  asym235n2 = asymtensor(p2,p3,p5,pol2)
  asym245n1 = asymtensor(p2,p4,p5,pol1)
  asym245n2 = asymtensor(p2,p4,p5,pol2)
  asym1235 = asymtensor(p1,p2,p3,p5)
  asym1245 = asymtensor(p1,p2,p4,p5)

  logms15 = log(me2/s15)
  logms25 = log(me2/s25)
  logmum = log(mu2/me2)
  logsm = log((ss-4*me2)/me2)

  polyms15 = Li2(1-me2/s15)
  polyms25 = Li2(1-me2/s25)

  discb125 = DiscB(ss-s15-s25,me1)
  discb125Im = pi*sqrt(1-4*me2/(ss-s15-s25))
  discbsm = DiscB(ss,me1)
  discbsIm = pi*sqrt(1-4*me2/ss)

  logssqrt = ss*discbsm/sqrt(ss*(-4*me2+ss))
  logssqrtIm = ss*discbsIm/sqrt(ss*(-4*me2+ss))
  logssqrt2 = logssqrt**2 - logssqrtIm**2
  logssqrt2Im = 2*logssqrt*logssqrtIm
  logs125sqrt = (ss-s15-s25)*discb125/sqrt((ss-s15-s25)*(-4*me2+ss-s15-s25))
  logs125sqrtIm = (ss-s15-s25)*discb125Im/sqrt((ss-s15-s25)*(-4*me2+ss-s15-s25))
  logs125sqrt2 = logs125sqrt**2 - logs125sqrtIm**2
  logs125sqrt2Im = 2*logs125sqrt*logs125sqrtIm

  scalarc0s15 = real(pvc15)
  scalarc0s15Im = aimag(pvc15)
  scalarc0s25 = real(pvc25)
  scalarc0s25Im = aimag(pvc25)

  scalarc0ms25 = -pi**2/(6*s25) - logms25**2/(2*s25) - polyms25/s25
  scalarc0ms15 = -pi**2/(6*s15) - logms15**2/(2*s15) - polyms15/s15

  scalarc0ss125 = logssqrt2/(2*(s15+s25)) - logs125sqrt2/(2*(s15+s25))
  scalarc0ss125Im = logssqrt2Im/(2*(s15+s25)) - logs125sqrt2Im/(2*(s15+s25))

  scalarc0ir6s = ScalarC0IR6(ss,me1)
  scalarc0ir6sIm = 1/(2*(-4*me2+ss))*(discbsm*(2*pi-ss*discbsIm/sqrt(ss*(-4*me2+ss)))+&
                   discbsIm*(-2*logsm-ss*discbsm/sqrt(ss*(-4*me2+ss))))

  scalard0s25 = real(pvd25) - discbsm*logmum/((4*me2-ss)*s25)
  scalard0s25Im = aimag(pvd25) - discbsIm*logmum/((4*me2-ss)*s25)
  scalard0s15 = real(pvd15) - discbsm*logmum/((4*me2-ss)*s15)
  scalard0s15Im = aimag(pvd15) - discbsIm*logmum/((4*me2-ss)*s15)


  if(present(sing)) then
    sing =  pepe2mmgl_ss(me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm)&
           +pepe2mmgl_ss(me2, mm2, ss, 2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt, s25, s15,&
                         s35, s1m, s3m, s4m, s2n, s3n, s4n, snm)&
           +pepe2mmgl_tgs(me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm)&
           +pepe2mmgl_tgs(me2, mm2, ss, 2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt, s25, s15,&
                          s35, s1m, s3m, s4m, s2n, s3n, s4n, snm)&
           +pepe2mmgl_ts(me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm)&
           +pepe2mmgl_ts(me2, mm2, ss, 2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt, s25, s15,&
                         s35, s1m, s3m, s4m, s2n, s3n, s4n, snm)&
           +pepe2mmgl_bs(me1, me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m,&
                         snm, discbsm)&
           +pepe2mmgl_bs(me1, me2, mm2, ss, 2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt, s25,&
                         s15, s35, s1m, s3m, s4m, s2n, s3n, s4n, snm, discbsm)&
           +pepe2mmgl_bsasym(me1, me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m,&
                             snm, discbsIm, asym123n1, asym123n2, asym124n1, asym124n2, asym125n1,&
                             asym125n2, asym135n1, asym135n2, asym145n1, asym145n2, asym235n1,&
                             asym235n2, asym245n1, asym245n2, asym1235, asym1245)&
           +pepe2mmgl_bsasym(me1, me2, mm2, ss, 2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt, s25,&
                             s15, s35, s1m, s3m, s4m, s2n, s3n, s4n, snm, discbsIm,&
                             -asym123n2, -asym123n1, -asym124n2, -asym124n1, -asym125n2,&
                             -asym125n1, asym235n2, asym235n1, asym245n2, asym245n1, asym135n2,&
                             asym135n1, asym145n2, asym145n1, -asym1235, -asym1245)&
           +pepe2mmgl_cts(me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm)

  endif

    pepe2mmgl_eeee_pvred =  pepe2mmgl_sf(me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm,&
                              logms15, logmum)&
                +pepe2mmgl_sf(me2, mm2, ss, 2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt, s25, s15,&
                              s35, s1m, s3m, s4m, s2n, s3n, s4n, snm, logms25, logmum)&
                +pepe2mmgl_tgf(me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm,&
                            logms15, logmum, polyms15)&
                +pepe2mmgl_tgf(me2, mm2, ss, 2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt, s25, s15,&
                              s35, s1m, s3m, s4m, s2n, s3n, s4n, snm, logms25, logmum, polyms25)&
                +pepe2mmgl_tf(me1, me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m,&
                              snm, logmum, logms15, discb125, scalarc0s15)&
                +pepe2mmgl_tf(me1,me2, mm2, ss, 2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt, s25, s15,&
                              s35, s1m, s3m, s4m, s2n, s3n, s4n, snm, logmum, logms25, discb125,&
                              scalarc0s25)&
                +pepe2mmgl_bf(me1, me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m,&
                              snm, logms25, discb125, logmum, discbsm, logssqrt, logssqrt2,&
                              logs125sqrt, logs125sqrt2, polyms25, scalarc0ms25, scalarc0ss125,&
                              scalarc0s25, scalarc0ir6s, scalard0s25)&
                +pepe2mmgl_bf(me1, me2, mm2, ss, 2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt, s25,&
                              s15, s35, s1m, s3m, s4m, s2n, s3n, s4n, snm, logms15, discb125,&
                              logmum, discbsm, logssqrt, logssqrt2, logs125sqrt, logs125sqrt2,&
                              polyms15, scalarc0ms15, scalarc0ss125, scalarc0s15, scalarc0ir6s,&
                              scalard0s15)&
                +pepe2mmgl_tfasym(me1, me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m,&
                                  snm, scalarc0s15Im, discb125Im, asym123n1, asym123n2, asym124n1,&
                                  asym124n2, asym125n1, asym125n2, asym135n1, asym135n2, asym145n1,&
                                  asym145n2, asym235n1, asym235n2, asym245n1, asym245n2, asym1235,&
                                  asym1245)&
                +pepe2mmgl_bfasym(me1, me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m,&
                                  snm, discbsIm, logs125sqrt2Im, logssqrt2Im, scalarc0ss125Im,&
                                  scalard0s25Im, scalarc0ir6sIm, discb125Im, scalarc0s25Im, logmum,&
                                  asym123n1, asym123n2, asym124n1, asym124n2, asym125n1, asym125n2,&
                                  asym135n1, asym135n2, asym145n1, asym145n2, asym235n1, asym235n2,&
                                  asym245n1, asym245n2, asym1235, asym1245)&
                +pepe2mmgl_tfasym(me1, me2, mm2, ss, 2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt, s25,&
                                  s15, s35, s1m, s3m, s4m, s2n, s3n, s4n, snm, scalarc0s25Im,&
                                  discb125Im, -asym123n2, -asym123n1, -asym124n2, -asym124n1,&
                                  -asym125n2, -asym125n1, asym235n2, asym235n1, asym245n2, asym245n1,&
                                  asym135n2, asym135n1, asym145n2, asym145n1, -asym1235, -asym1245)&
                +pepe2mmgl_bfasym(me1, me2, mm2, ss, 2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt, s25,&
                                  s15, s35, s1m, s3m, s4m, s2n, s3n, s4n, snm, discbsIm,&
                                  logs125sqrt2Im, logssqrt2Im, scalarc0ss125Im, scalard0s15Im,&
                                  scalarc0ir6sIm, discb125Im, scalarc0s15Im, logmum, -asym123n2,&
                                  -asym123n1, -asym124n2, -asym124n1, -asym125n2, -asym125n1,&
                                  asym235n2, asym235n1, asym245n2, asym245n1, asym135n2, asym135n1,&
                                  asym145n2, asym145n1, -asym1235, -asym1245)&
                +pepe2mmgl_ctf(me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm,&
                               logmum)

  END FUNCTION PEPE2MMGL_EEEE_PVRED


  FUNCTION PEPE2MMGL_EEEE_COLL(p1,pol1,p2,pol2,p3,p4,p5,sing)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons
  use mue_pepe2mmgl, only: PEPE2MMGL_CTF, PEPE2MMGL_CTS, &
                           PEPE2MMGL_BSASYM, &
                           PEPE2MMGL_BS, &
                           PEPE2MMGL_TS, &
                           PEPE2MMGL_TGS, &
                           PEPE2MMGL_SS
  use mue_pepe2mmgl_coll, only:  pepe2mmgl_tfasym, pepe2mmgl_bfasym, &
      pepe2mmgl_eoe, &
      pepe2mmgl_tf, pepe2mmgl_sf, pepe2mmgl_tgf, pepe2mmgl_bf
  implicit none
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4), pol1(4), pol2(4)
  real (kind=prec) :: pepe2mmgl_eeee_coll
  real (kind=prec), optional :: sing
  real (kind=prec) :: ss, tt, me2, me1, mm2, s2n, s3n, s4n, s1m, s3m, s4m, snm
  real (kind=prec) :: s15, s35, s25, ttX
  real (kind=prec) :: asym123n1, asym123n2, asym124n1, asym124n2, asym125n1, asym125n2
  real (kind=prec) :: asym135n1, asym135n2, asym145n1, asym145n2, asym235n1, asym235n2
  real (kind=prec) :: asym245n1, asym245n2, asym1235, asym1245
  real (kind=prec) :: discBsm, discBsIm, logmum

  complex(kind=prec) :: uvdump(1:24)
  complex(kind=prec) :: PVB1(4),PVB2(2),PVB3(4),PVB4(4),PVB5(4),PVB6(2),PVB7(2),PVB8(4)
  complex(kind=prec) :: PVC1(7),PVC2(7),PVC3(22),PVC4(7),PVC5(7),PVC6(7)
  complex(kind=prec) :: PVD1(24)

  ss = sq(p1+p2); tt = sq(p1-p3); s15 = s(p1,p5); s35 = s(p3,p5); s25 = s(p2,p5)
  me2 = sq(p1); mm2 = sq(p3); me1 = sqrt(me2)
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)

  asym123n1 = asymtensor(p1,p2,p3,pol1)
  asym123n2 = asymtensor(p1,p2,p3,pol2)
  asym124n1 = asymtensor(p1,p2,p4,pol1)
  asym124n2 = asymtensor(p1,p2,p4,pol2)
  asym125n1 = asymtensor(p1,p2,p5,pol1)
  asym125n2 = asymtensor(p1,p2,p5,pol2)
  asym135n1 = asymtensor(p1,p3,p5,pol1)
  asym135n2 = asymtensor(p1,p3,p5,pol2)
  asym145n1 = asymtensor(p1,p4,p5,pol1)
  asym145n2 = asymtensor(p1,p4,p5,pol2)
  asym235n1 = asymtensor(p2,p3,p5,pol1)
  asym235n2 = asymtensor(p2,p3,p5,pol2)
  asym245n1 = asymtensor(p2,p4,p5,pol1)
  asym245n2 = asymtensor(p2,p4,p5,pol2)
  asym1235 = asymtensor(p1,p2,p3,p5)
  asym1245 = asymtensor(p1,p2,p4,p5)

  discbsm = DiscB(ss,me1)
  discbsIm = pi*sqrt(1-4*me2/ss)
  logmum = log(musq/me2)

  ttX =  2*me2 + 2*mm2 - ss - s35 + s15 + s25 - tt

  if(present(sing)) then
    sing =  pepe2mmgl_ss(me2, mm2, ss, tt , s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm)&
           +pepe2mmgl_ss(me2, mm2, ss, ttX, s25, s15, s35, s1m, s3m, s4m, s2n, s3n, s4n, snm)&
           +pepe2mmgl_tgs(me2, mm2, ss, tt , s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm)&
           +pepe2mmgl_tgs(me2, mm2, ss, ttX, s25, s15, s35, s1m, s3m, s4m, s2n, s3n, s4n, snm)&
           +pepe2mmgl_ts(me2, mm2, ss, tt , s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm)&
           +pepe2mmgl_ts(me2, mm2, ss, ttX, s25, s15, s35, s1m, s3m, s4m, s2n, s3n, s4n, snm)&
           +pepe2mmgl_bs(me1, me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m,&
                         snm, discbsm)&
           +pepe2mmgl_bs(me1, me2, mm2, ss, ttX, s25,&
                         s15, s35, s1m, s3m, s4m, s2n, s3n, s4n, snm, discbsm)&
           +pepe2mmgl_bsasym(me1, me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m,&
                             snm, discbsIm, asym123n1, asym123n2, asym124n1, asym124n2, asym125n1,&
                             asym125n2, asym135n1, asym135n2, asym145n1, asym145n2, asym235n1,&
                             asym235n2, asym245n1, asym245n2, asym1235, asym1245)&
           +pepe2mmgl_bsasym(me1, me2, mm2, ss, ttX, s25,&
                             s15, s35, s1m, s3m, s4m, s2n, s3n, s4n, snm, discbsIm,&
                             -asym123n2, -asym123n1, -asym124n2, -asym124n1, -asym125n2,&
                             -asym125n1, asym235n2, asym235n1, asym245n2, asym245n1, asym135n2,&
                             asym135n1, asym145n2, asym145n1, -asym1235, -asym1245)&
           +pepe2mmgl_cts(me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm)

  endif

  call B_cll(PVB1,uvdump(:4), &
    cmplx(me2 - s15), &
    cmplx(0),cmplx(me2), 2)
  call B_cll(PVB2,uvdump(:2), &
    cmplx(me2 - s15), &
    cmplx(me2),cmplx(0), 1)
  call B_cll(PVB3,uvdump(:4), &
    cmplx(me2 - s25), &
    cmplx(0),cmplx(me2), 2)
  call B_cll(PVB4,uvdump(:4), &
    cmplx(-s15 - s25 + ss), &
    cmplx(me2),cmplx(me2), 2)
  call B_cll(PVB5,uvdump(:4), &
    cmplx(0), &
    cmplx(me2),cmplx(me2), 2)
  call B_cll(PVB6,uvdump(:2), &
    cmplx(me2), &
    cmplx(me2),cmplx(0), 1)
  call B_cll(PVB7,uvdump(:2), &
    cmplx(me2), &
    cmplx(0),cmplx(me2), 1)
  call B_cll(PVB8,uvdump(:4), &
    cmplx(ss), &
    cmplx(me2),cmplx(me2), 2)
  call C_cll(PVC1,uvdump(:7), &
    cmplx(me2),cmplx(-s15 - s25 + ss),cmplx(me2 - s15), &
    cmplx(0),cmplx(me2),cmplx(me2), 2)
  call C_cll(PVC2,uvdump(:7), &
    cmplx(me2),cmplx(-s15 - s25 + ss),cmplx(me2 - s25), &
    cmplx(0),cmplx(me2),cmplx(me2), 2)
  call C_cll(PVC3,uvdump(:22), &
    cmplx(ss),cmplx(0),cmplx(-s15 - s25 + ss), &
    cmplx(me2),cmplx(me2),cmplx(me2), 4)
  call C_cll(PVC4,uvdump(:7), &
    cmplx(me2),cmplx(me2 - s15),cmplx(0), &
    cmplx(me2),cmplx(0),cmplx(me2), 2)
  call C_cll(PVC5,uvdump(:7), &
    cmplx(me2),cmplx(0),cmplx(me2 - s25), &
    cmplx(0),cmplx(me2),cmplx(me2), 2)
  call C_cll(PVC6,uvdump(:7), &
    cmplx(me2),cmplx(ss),cmplx(me2), &
    cmplx(0),cmplx(me2),cmplx(me2), 2)
  call D_cll(PVD1,uvdump(:24), &
    cmplx(me2),cmplx(ss),cmplx(0),cmplx(me2 - s25),cmplx(me2),cmplx(-s15 - s25 + ss), &
    cmplx(0),cmplx(me2),cmplx(me2),cmplx(me2), 3)

  pepe2mmgl_eeee_coll = &
  + pepe2mmgl_tf    (me2,mm2,ss,tt,s15,s25,s35,s2n,s3n,s4n,s1m,s3m,s4m,snm, &
     realpart(PVB1),realpart(PVB2),realpart(PVB3),realpart(PVB4),realpart(PVB5),realpart(PVB6),realpart(PVB7),realpart(PVB8),&
     realpart(PVC1),realpart(PVC2),realpart(PVC3),realpart(PVC4),realpart(PVC5),realpart(PVC6),&
     realpart(PVD1)) &
  + pepe2mmgl_sf    (me2,mm2,ss,tt,s15,s25,s35,s2n,s3n,s4n,s1m,s3m,s4m,snm, &
     realpart(PVB1),realpart(PVB2),realpart(PVB3),realpart(PVB4),realpart(PVB5),realpart(PVB6),realpart(PVB7),realpart(PVB8),&
     realpart(PVC1),realpart(PVC2),realpart(PVC3),realpart(PVC4),realpart(PVC5),realpart(PVC6),&
     realpart(PVD1)) &
  + pepe2mmgl_tgf   (me2,mm2,ss,tt,s15,s25,s35,s2n,s3n,s4n,s1m,s3m,s4m,snm, &
     realpart(PVB1),realpart(PVB2),realpart(PVB3),realpart(PVB4),realpart(PVB5),realpart(PVB6),realpart(PVB7),realpart(PVB8),&
     realpart(PVC1),realpart(PVC2),realpart(PVC3),realpart(PVC4),realpart(PVC5),realpart(PVC6),&
     realpart(PVD1)) &
  + pepe2mmgl_bf    (me2,mm2,ss,tt,s15,s25,s35,s2n,s3n,s4n,s1m,s3m,s4m,snm, &
     realpart(PVB1),realpart(PVB2),realpart(PVB3),realpart(PVB4),realpart(PVB5),realpart(PVB6),realpart(PVB7),realpart(PVB8),&
     realpart(PVC1),realpart(PVC2),realpart(PVC3),realpart(PVC4),realpart(PVC5),realpart(PVC6),&
     realpart(PVD1)) &
  + pepe2mmgl_tfasym(me2,mm2,ss,tt,s15,s25,s35,s2n,s3n,s4n,s1m,s3m,s4m,snm,&
     aimag(PVB1),aimag(PVB2),aimag(PVB3),aimag(PVB4),aimag(PVB5),aimag(PVB6),aimag(PVB7),aimag(PVB8),&
     aimag(PVC1),aimag(PVC2),aimag(PVC3),aimag(PVC4),aimag(PVC5),aimag(PVC6),&
     aimag(PVD1),&
     asym123n1,asym123n2,asym124n1,asym124n2,asym125n1,asym125n2,asym135n1,asym135n2,&
     asym145n1,asym145n2,asym235n1,asym235n2,asym245n1,asym245n2,asym1235,asym1245) * me1 &
  + pepe2mmgl_bfasym(me2,mm2,ss,tt,s15,s25,s35,s2n,s3n,s4n,s1m,s3m,s4m,snm,&
     aimag(PVB1),aimag(PVB2),aimag(PVB3),aimag(PVB4),aimag(PVB5),aimag(PVB6),aimag(PVB7),aimag(PVB8),&
     aimag(PVC1),aimag(PVC2),aimag(PVC3),aimag(PVC4),aimag(PVC5),aimag(PVC6),&
     aimag(PVD1),&
     asym123n1,asym123n2,asym124n1,asym124n2,asym125n1,asym125n2,asym135n1,asym135n2,&
     asym145n1,asym145n2,asym235n1,asym235n2,asym245n1,asym245n2,asym1235,asym1245) * me1

  call B_cll(PVB2,uvdump(:2), &
    cmplx(me2 - s25), &
    cmplx(me2),cmplx(0), 1)
  call C_cll(PVC4,uvdump(:7), &
    cmplx(me2),cmplx(me2 - s25),cmplx(0), &
    cmplx(me2),cmplx(0),cmplx(me2), 2)
  call C_cll(PVC5,uvdump(:7), &
    cmplx(me2),cmplx(0),cmplx(me2 - s15), &
    cmplx(0),cmplx(me2),cmplx(me2), 2)
  call D_cll(PVD1,uvdump(:24), &
    cmplx(me2),cmplx(ss),cmplx(0),cmplx(me2 - s15),cmplx(me2),cmplx(ss - s15 - s25), &
    cmplx(0),cmplx(me2),cmplx(me2),cmplx(me2), 3)

  pepe2mmgl_eeee_coll = pepe2mmgl_eeee_coll &
  + pepe2mmgl_tf    (me2,mm2,ss,ttX,s25,s15,s35,s1m,s3m,s4m,s2n,s3n,s4n,snm, &
     realpart(PVB3),realpart(PVB2),realpart(PVB1),realpart(PVB4),realpart(PVB5),realpart(PVB6),realpart(PVB7),realpart(PVB8),&
     realpart(PVC2),realpart(PVC1),realpart(PVC3),realpart(PVC4),realpart(PVC5),realpart(PVC6),&
     realpart(PVD1)) &
  + pepe2mmgl_sf    (me2,mm2,ss,ttX,s25,s15,s35,s1m,s3m,s4m,s2n,s3n,s4n,snm, &
     realpart(PVB3),realpart(PVB2),realpart(PVB1),realpart(PVB4),realpart(PVB5),realpart(PVB6),realpart(PVB7),realpart(PVB8),&
     realpart(PVC2),realpart(PVC1),realpart(PVC3),realpart(PVC4),realpart(PVC5),realpart(PVC6),&
     realpart(PVD1)) &
  + pepe2mmgl_tgf   (me2,mm2,ss,ttX,s25,s15,s35,s1m,s3m,s4m,s2n,s3n,s4n,snm, &
     realpart(PVB3),realpart(PVB2),realpart(PVB1),realpart(PVB4),realpart(PVB5),realpart(PVB6),realpart(PVB7),realpart(PVB8),&
     realpart(PVC2),realpart(PVC1),realpart(PVC3),realpart(PVC4),realpart(PVC5),realpart(PVC6),&
     realpart(PVD1)) &
  + pepe2mmgl_bf    (me2,mm2,ss,ttX,s25,s15,s35,s1m,s3m,s4m,s2n,s3n,s4n,snm, &
     realpart(PVB3),realpart(PVB2),realpart(PVB1),realpart(PVB4),realpart(PVB5),realpart(PVB6),realpart(PVB7),realpart(PVB8),&
     realpart(PVC2),realpart(PVC1),realpart(PVC3),realpart(PVC4),realpart(PVC5),realpart(PVC6),&
     realpart(PVD1)) &
  + pepe2mmgl_tfasym(me2,mm2,ss,ttX,s25,s15,s35,s1m,s3m,s4m,s2n,s3n,s4n,snm, &
     aimag(PVB3),aimag(PVB2),aimag(PVB1),aimag(PVB4),aimag(PVB5),aimag(PVB6),aimag(PVB7),aimag(PVB8),&
     aimag(PVC2),aimag(PVC1),aimag(PVC3),aimag(PVC4),aimag(PVC5),aimag(PVC6),&
     aimag(PVD1),&
    -asym123n2,-asym123n1,-asym124n2,-asym124n1,-asym125n2,-asym125n1, asym235n2 ,asym235n1,&
     asym245n2, asym245n1, asym135n2, asym135n1, asym145n2, asym145n1,-asym1235 ,-asym1245) * me1 &
  + pepe2mmgl_bfasym(me2,mm2,ss,ttX,s25,s15,s35,s1m,s3m,s4m,s2n,s3n,s4n,snm, &
     aimag(PVB3),aimag(PVB2),aimag(PVB1),aimag(PVB4),aimag(PVB5),aimag(PVB6),aimag(PVB7),aimag(PVB8),&
     aimag(PVC2),aimag(PVC1),aimag(PVC3),aimag(PVC4),aimag(PVC5),aimag(PVC6),&
     aimag(PVD1),&
    -asym123n2,-asym123n1,-asym124n2,-asym124n1,-asym125n2,-asym125n1, asym235n2 ,asym235n1,&
     asym245n2, asym245n1, asym135n2, asym135n1, asym145n2, asym145n1,-asym1235 ,-asym1245) * me1

  pepe2mmgl_eeee_coll = pepe2mmgl_eeee_coll   &
                      + pepe2mmgl_eoe(me2, mm2, ss, tt , s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm,logmum) &
                      + pepe2mmgl_eoe(me2, mm2, ss, ttX, s25, s15, s35, s1m, s3m, s4m, s2n, s3n, s4n, snm,logmum)
  pepe2mmgl_eeee_coll = alpha**4*64*pi**2*pepe2mmgl_eeee_coll + &
                        pepe2mmgl_ctf(me2, mm2, ss, tt, s15, s25, s35, s2n, s3n, s4n, s1m, s3m, s4m, snm,+logmum)

  END FUNCTION PEPE2MMGL_EEEE_COLL



  FUNCTION PEPE2MML(p1,pol1,p2,pol2,p3,p4,sing)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), pol1(4), pol2(4)
  real(kind=prec) :: pepe2mml
  real(kind=prec), optional :: sing
  real(kind=prec) :: sing_ee, sing_mm, sing_em, sing_emc

  if (present(sing)) then
     pepe2mml =pepe2mml_ee(p1,pol1,p2,pol2,p3,p4,sing_ee)&
               +pepe2mml_mm(p1,pol1,p2,pol2,p3,p4,sing_mm)&
               +pepe2mml_em(p1,pol1,p2,pol2,p3,p4,sing_em)&
               +pepe2mml_emc(p1,pol1,p2,pol2,p3,p4,sing_emc)
     sing = sing_ee + sing_mm + sing_em + sing_emc
  else
    pepe2mml =pepe2mml_ee(p1,pol1,p2,pol2,p3,p4)&
              +pepe2mml_mm(p1,pol1,p2,pol2,p3,p4)&
              +pepe2mml_em(p1,pol1,p2,pol2,p3,p4)&
              +pepe2mml_emc(p1,pol1,p2,pol2,p3,p4)
  endif

  END FUNCTION

  FUNCTION PEPE2MML_EMC(p1,pol1,p2,pol2,p3,p4,sing_emc)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), pol1(4), pol2(4)
  real(kind=prec) :: pepe2mml_emc, sing_em
  real(kind=prec), optional :: sing_emc

  pepe2mml_emc = -pepe2mml_em(p1,pol1,p2,pol2,p4,p3,sing_em)

  if(present(sing_emc)) then
   sing_emc = -sing_em
  endif

  END FUNCTION

  FUNCTION PEPE2MML_EE_CPLX(p1,pol1,p2,pol2,p3,p4,sing_ee)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), pol1(4), pol2(4)
  complex(kind=prec) :: pepe2mml_ee_cplx
  complex(kind=prec), optional :: sing_ee
  complex(kind=prec) :: discbse, logem, scalarc0ir6se
  complex(kind=prec) :: asym_ee
  real(kind=prec) :: ss, tt, me2, mm2, mm, me1, mu2
  real(kind=prec) :: s2n,s3n,s4n,s1m,s3m,s4m,snm

  ss = sq(p1+p2); tt = sq(p1-p3)
  me2 = sq(p1); mm2=sq(p3)
  me1 = sqrt(me2); mm = sqrt(mm2)
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)
  mu2 = musq
  logem = log(mu2/me2)
  discbse = DiscB_cplx(ss,me1)
  scalarc0ir6se = ScalarC0IR6_cplx(ss,me1)

  if(present(sing_ee)) then
    sing_ee =(-16*alpha**3*pi*(-4*me2 + ss + discbse*(-2*me2 + ss))*(-2*me2**2*(-2 + snm) -&
   2*mm2**2*(-2 + snm) - s1m*s2n*ss + s2n*s3m*ss - s3n*s4m*ss + s1m*s4n*ss - s3m*s4n*ss&
   + 2*ss**2 + s2n*s3m*tt - s1m*s3n*tt - s2n*s4m*tt + s1m*s4n*tt + 4*ss*tt -&
   2*snm*ss*tt + 4*tt**2 - 2*snm*tt**2 + mm2*(s2n*(-s3m + s4m) + s1m*(2*s2n + s3n -&
   s4n) + 4*(-2 + snm)*tt) + me2*(s1m*s3n + s2n*(-s3m + s4m) - s1m*s4n - 4*mm2*(-2 +&
   snm) - 8*tt + 4*snm*tt)))/(ss**2*(-4*me2 + ss))

  endif

  asym_ee = 32*alpha**3*discbse*imag*me1*pi*(2*me2 + 2*mm2 - ss - 2*tt)/(ss**2*(-4*me2 + ss))*&
           2 * asymtensor(p1,p2,p3,pol1+pol2)

  pepe2mml_ee_cplx = (8*alpha**3*Pi*(4*me2**2 + 8*me2*mm2 + 4*mm2**2 + 2*mm2*s1m*s2n - me2*s2n*s3m -&
   mm2*s2n*s3m + me2*s1m*s3n + mm2*s1m*s3n + me2*s2n*s4m + mm2*s2n*s4m - me2*s1m*s4n -&
   mm2*s1m*s4n - 2*me2**2*snm - 4*me2*mm2*snm - 2*mm2**2*snm + s2n*s3m*ss - s3n*s4m*ss&
   + s1m*s4n*ss - s3m*s4n*ss + 4*ss**2 - snm*ss**2 + ss*(-(s1m*s2n) + (-2 + snm)*ss) -&
   8*me2*tt - 8*mm2*tt + s2n*s3m*tt - s1m*s3n*tt - s2n*s4m*tt + s1m*s4n*tt +&
   4*me2*snm*tt + 4*mm2*snm*tt + 4*ss*tt - 2*snm*ss*tt + 4*tt**2 - 2*snm*tt**2 +&
   (logem*(-4*me2 + discbse*(4*me2 - 2*ss) + ss)*(2*me2**2*(-2 + snm) + 2*mm2**2*(-2 +&
   snm) + s1m*s2n*ss - s2n*s3m*ss + s3n*s4m*ss - s1m*s4n*ss + s3m*s4n*ss - 2*ss**2 -&
   s2n*s3m*tt + s1m*s3n*tt + s2n*s4m*tt - s1m*s4n*tt - 4*ss*tt + 2*snm*ss*tt - 4*tt**2&
   + 2*snm*tt**2 + mm2*(s2n*s3m - s2n*s4m + s1m*(-2*s2n - s3n + s4n) + 8*tt - 4*snm*tt)&
   + me2*(s2n*s3m - s1m*s3n - s2n*s4m + s1m*s4n + 4*mm2*(-2 + snm) + 8*tt -&
   4*snm*tt)))/(4*me2 - ss) + (-5 - 3*logem)*(-2*me2**2*(-2 + snm) - 2*mm2**2*(-2 +&
   snm) - s1m*s2n*ss + s2n*s3m*ss - s3n*s4m*ss + s1m*s4n*ss - s3m*s4n*ss + 2*ss**2 +&
   s2n*s3m*tt - s1m*s3n*tt - s2n*s4m*tt + s1m*s4n*tt + 4*ss*tt - 2*snm*ss*tt + 4*tt**2&
   - 2*snm*tt**2 + mm2*(s2n*(-s3m + s4m) + s1m*(2*s2n + s3n - s4n) + 4*(-2 + snm)*tt) +&
   me2*(s1m*s3n + s2n*(-s3m + s4m) - s1m*s4n - 4*mm2*(-2 + snm) - 8*tt + 4*snm*tt)) -&
   2*scalarc0ir6se*(-2*me2 + ss)*(-2*me2**2*(-2 + snm) - 2*mm2**2*(-2 + snm) -&
   s1m*s2n*ss + s2n*s3m*ss - s3n*s4m*ss + s1m*s4n*ss - s3m*s4n*ss + 2*ss**2 +&
   s2n*s3m*tt - s1m*s3n*tt - s2n*s4m*tt + s1m*s4n*tt + 4*ss*tt - 2*snm*ss*tt + 4*tt**2&
   - 2*snm*tt**2 + mm2*(s2n*(-s3m + s4m) + s1m*(2*s2n + s3n - s4n) + 4*(-2 + snm)*tt) +&
   me2*(s1m*s3n + s2n*(-s3m + s4m) - s1m*s4n - 4*mm2*(-2 + snm) - 8*tt + 4*snm*tt)) +&
   (discbse*(16*me2**3*(-2 + snm) + 2*me2**2*(5*s2n*s3m - 5*s1m*s3n - 5*s2n*s4m +&
   5*s1m*s4n + 16*mm2*(-2 + snm) - 3*(-2 + snm)*ss + 32*tt - 16*snm*tt) -&
   3*ss*(2*mm2**2*(-2 + snm) - 2*ss**2 + mm2*(s2n*(s3m - s4m) + s1m*(-2*s2n - s3n +&
   s4n) - 4*(-2 + snm)*tt) + tt*(s2n*(-s3m + s4m) + s1m*(s3n - s4n) + 2*(-2 + snm)*tt)&
   + ss*(-(s2n*s3m) + s3n*s4m + s1m*(s2n - s4n) + s3m*s4n - 4*tt + 2*snm*tt)) +&
   2*me2*(8*mm2**2*(-2 + snm) - 12*ss**2 + tt*(-5*s2n*(s3m - s4m) + 5*s1m*(s3n - s4n) +&
   8*(-2 + snm)*tt) + mm2*(5*s2n*s3m - 5*s2n*s4m + s1m*(-12*s2n - 5*s3n + 5*s4n) -&
   2*(-2 + snm)*ss + 32*tt - 16*snm*tt) + ss*(6*s3n*s4m + s2n*(-7*s3m + s4m) +&
   s1m*(5*s2n + s3n - 7*s4n) + 6*s3m*s4n - 28*tt + 14*snm*tt))))/(4*me2 - ss)))/ss**2

  pepe2mml_ee_cplx = pepe2mml_ee_cplx + asym_ee

  END FUNCTION

  FUNCTION PEPE2MML_EE(p1,pol1,p2,pol2,p3,p4,sing_ee)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), pol1(4), pol2(4)
  real(kind=prec) :: pepe2mml_ee
  complex(kind=prec) :: sing
  real(kind=prec), optional :: sing_ee

  if(present(sing_ee)) then
    pepe2mml_ee = real(pepe2mml_ee_cplx(p1,pol1,p2,pol2,p3,p4,sing), kind=prec)
    sing_ee = real(sing, kind=prec)
  else
    pepe2mml_ee = real(pepe2mml_ee_cplx(p1,pol1,p2,pol2,p3,p4), kind=prec)
  endif
  END FUNCTION


  FUNCTION PEPE2MML_MM(p1,pol1,p2,pol2,p3,p4,sing_mm)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), pol1(4), pol2(4)
  real(kind=prec) :: pepe2mml_mm
  real(kind=prec), optional :: sing_mm
  real(kind=prec) :: ss, tt, me2, mm2, mm, me1, mu2
  real(kind=prec) :: s2n,s3n,s4n,s1m,s3m,s4m,snm
  real(kind=prec) :: discbs, logmm, scalarc0ir6s

  ss = sq(p1+p2); tt = sq(p1-p3)
  me2 = sq(p1); mm2=sq(p3)
  me1 = sqrt(me2); mm = sqrt(mm2)
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)
  mu2 = musq
  logmm = log(mu2/mm2)
  discbs = DiscB(ss,mm)
  scalarc0ir6s = ScalarC0IR6(ss,mm)

  if(present(sing_mm)) then
    sing_mm =(-8*alpha**3*pi*(4*mm2 - 4*discbs*mm2 - ss + 2*discbs*ss)*(4*me2**2 + 8*me2*mm2 +&
   4*mm2**2 + 2*mm2*s1m*s2n - me2*s2n*s3m - mm2*s2n*s3m + me2*s1m*s3n + mm2*s1m*s3n +&
   me2*s2n*s4m + mm2*s2n*s4m - me2*s1m*s4n - mm2*s1m*s4n - 2*me2**2*snm - 4*me2*mm2*snm&
   - 2*mm2**2*snm - s1m*s2n*ss + s2n*s3m*ss - s3n*s4m*ss + s1m*s4n*ss - s3m*s4n*ss +&
   2*ss**2 - 8*me2*tt - 8*mm2*tt + s2n*s3m*tt - s1m*s3n*tt - s2n*s4m*tt + s1m*s4n*tt +&
   4*me2*snm*tt + 4*mm2*snm*tt + 4*ss*tt - 2*snm*ss*tt + 4*tt**2 -&
   2*snm*tt**2))/(ss**2*(-4*mm2 + ss)) - (24*alpha**3*pi*(-2*me2**2*(-2 + snm) -&
   2*mm2**2*(-2 + snm) - s1m*s2n*ss + s2n*s3m*ss - s3n*s4m*ss + s1m*s4n*ss - s3m*s4n*ss&
   + 2*ss**2 + s2n*s3m*tt - s1m*s3n*tt - s2n*s4m*tt + s1m*s4n*tt + 4*ss*tt -&
   2*snm*ss*tt + 4*tt**2 - 2*snm*tt**2 + mm2*(s2n*(-s3m + s4m) + s1m*(2*s2n + s3n -&
   s4n) + 4*(-2 + snm)*tt) + me2*(s1m*s3n + s2n*(-s3m + s4m) - s1m*s4n - 4*mm2*(-2 +&
   snm) - 8*tt + 4*snm*tt)))/ss**2
  endif

  pepe2mml_mm =(16*alpha**3*(-2.5 - (3*logmm)/2.)*pi*(-2*me2**2*(-2 + snm) - 2*mm2**2*(-2 + snm) -&
   s1m*s2n*ss + s2n*s3m*ss - s3n*s4m*ss + s1m*s4n*ss - s3m*s4n*ss + 2*ss**2 +&
   s2n*s3m*tt - s1m*s3n*tt - s2n*s4m*tt + s1m*s4n*tt + 4*ss*tt - 2*snm*ss*tt + 4*tt**2&
   - 2*snm*tt**2 + mm2*(s2n*(-s3m + s4m) + s1m*(2*s2n + s3n - s4n) + 4*(-2 + snm)*tt) +&
   me2*(s1m*s3n + s2n*(-s3m + s4m) - s1m*s4n - 4*mm2*(-2 + snm) - 8*tt +&
   4*snm*tt)))/ss**2 + ((256*alpha**3*pi**3*(-(s1m*s2n) - 2*ss + snm*ss))/ss -&
   (512*alpha**3*pi**3*scalarc0ir6s*(-2*mm2 + ss)*(4*me2**2 + 8*me2*mm2 + 4*mm2**2 +&
   2*mm2*s1m*s2n - me2*s2n*s3m - mm2*s2n*s3m + me2*s1m*s3n + mm2*s1m*s3n + me2*s2n*s4m&
   + mm2*s2n*s4m - me2*s1m*s4n - mm2*s1m*s4n - 2*me2**2*snm - 4*me2*mm2*snm -&
   2*mm2**2*snm - s1m*s2n*ss + s2n*s3m*ss - s3n*s4m*ss + s1m*s4n*ss - s3m*s4n*ss +&
   2*ss**2 - 8*me2*tt - 8*mm2*tt + s2n*s3m*tt - s1m*s3n*tt - s2n*s4m*tt + s1m*s4n*tt +&
   4*me2*snm*tt + 4*mm2*snm*tt + 4*ss*tt - 2*snm*ss*tt + 4*tt**2 - 2*snm*tt**2))/ss**2&
   - (256*alpha**3*logmm*pi**3*(4*mm2 - 4*discbs*mm2 - ss + 2*discbs*ss)*(4*me2**2 +&
   8*me2*mm2 + 4*mm2**2 + 2*mm2*s1m*s2n - me2*s2n*s3m - mm2*s2n*s3m + me2*s1m*s3n +&
   mm2*s1m*s3n + me2*s2n*s4m + mm2*s2n*s4m - me2*s1m*s4n - mm2*s1m*s4n - 2*me2**2*snm -&
   4*me2*mm2*snm - 2*mm2**2*snm - s1m*s2n*ss + s2n*s3m*ss - s3n*s4m*ss + s1m*s4n*ss -&
   s3m*s4n*ss + 2*ss**2 - 8*me2*tt - 8*mm2*tt + s2n*s3m*tt - s1m*s3n*tt - s2n*s4m*tt +&
   s1m*s4n*tt + 4*me2*snm*tt + 4*mm2*snm*tt + 4*ss*tt - 2*snm*ss*tt + 4*tt**2 -&
   2*snm*tt**2))/(ss**2*(-4*mm2 + ss)) - (256*alpha**3*pi**3*(-4*me2**2 - 8*me2*mm2 -&
   4*mm2**2 - 2*mm2*s1m*s2n + me2*s2n*s3m + mm2*s2n*s3m - me2*s1m*s3n - mm2*s1m*s3n -&
   me2*s2n*s4m - mm2*s2n*s4m + me2*s1m*s4n + mm2*s1m*s4n + 2*me2**2*snm + 4*me2*mm2*snm&
   + 2*mm2**2*snm - s2n*s3m*ss + s3n*s4m*ss - s1m*s4n*ss + s3m*s4n*ss - 4*ss**2 +&
   snm*ss**2 + 8*me2*tt + 8*mm2*tt - s2n*s3m*tt + s1m*s3n*tt + s2n*s4m*tt - s1m*s4n*tt&
   - 4*me2*snm*tt - 4*mm2*snm*tt - 4*ss*tt + 2*snm*ss*tt - 4*tt**2 +&
   2*snm*tt**2))/ss**2 + (256*alpha**3*discbs*pi**3*(-32*me2**2*mm2 - 64*me2*mm2**2 -&
   32*mm2**3 - 16*mm2**2*s1m*s2n + 8*me2*mm2*s2n*s3m + 8*mm2**2*s2n*s3m -&
   8*me2*mm2*s1m*s3n - 8*mm2**2*s1m*s3n - 8*me2*mm2*s2n*s4m - 8*mm2**2*s2n*s4m +&
   8*me2*mm2*s1m*s4n + 8*mm2**2*s1m*s4n + 16*me2**2*mm2*snm + 32*me2*mm2**2*snm +&
   16*mm2**3*snm + 12*me2**2*ss + 8*me2*mm2*ss + 12*mm2**2*ss + 16*mm2*s1m*s2n*ss -&
   3*me2*s2n*s3m*ss - 13*mm2*s2n*s3m*ss + 3*me2*s1m*s3n*ss + mm2*s1m*s3n*ss +&
   2*mm2*s3m*s3n*ss + 3*me2*s2n*s4m*ss + mm2*s2n*s4m*ss + 10*mm2*s3n*s4m*ss -&
   3*me2*s1m*s4n*ss - 13*mm2*s1m*s4n*ss + 10*mm2*s3m*s4n*ss + 2*mm2*s4m*s4n*ss -&
   6*me2**2*snm*ss - 4*me2*mm2*snm*ss - 6*mm2**2*snm*ss - 24*mm2*ss**2 -&
   3*s1m*s2n*ss**2 + 3*s2n*s3m*ss**2 - 3*s3n*s4m*ss**2 + 3*s1m*s4n*ss**2 -&
   3*s3m*s4n*ss**2 + 6*ss**3 + 64*me2*mm2*tt + 64*mm2**2*tt - 8*mm2*s2n*s3m*tt +&
   8*mm2*s1m*s3n*tt + 8*mm2*s2n*s4m*tt - 8*mm2*s1m*s4n*tt - 32*me2*mm2*snm*tt -&
   32*mm2**2*snm*tt - 24*me2*ss*tt - 56*mm2*ss*tt + 3*s2n*s3m*ss*tt - 3*s1m*s3n*ss*tt -&
   3*s2n*s4m*ss*tt + 3*s1m*s4n*ss*tt + 12*me2*snm*ss*tt + 28*mm2*snm*ss*tt +&
   12*ss**2*tt - 6*snm*ss**2*tt - 32*mm2*tt**2 + 16*mm2*snm*tt**2 + 12*ss*tt**2 -&
   6*snm*ss*tt**2))/((4*mm2 - ss)*ss**2))/(32.*pi**2)

  END FUNCTION


  FUNCTION PEPE2MML_EM(p1,pol1,p2,pol2,p3,p4,sing_em)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), pol1(4), pol2(4)
  real(kind=prec) :: pepe2mml_em
  real(kind=prec), optional :: sing_em
  real(kind=prec) :: ss, tt, me2, mm2, me1, mm, mu2
  real(kind=prec) :: s2n,s3n,s4n,s1m,s3m,s4m,snm
  real(kind=prec) :: logmm,logem, logme, logse, logsm
  real(kind=prec) :: asym123n1, asym123n2, asym124n1, asym124n2
  real(kind=prec) :: asym234n1, asym234n2, asym134n1, asym134n2
  real(kind=prec) :: discbs, discbse, discbtem
  real(kind=prec) :: scalarc0esIM, scalarc0msIM
  real(kind=prec) :: scalarc0sm, scalarc0se, scalarc0ir6tme

  ss = sq(p1+p2); tt = sq(p1-p3)
  me2 = sq(p1); mm2=sq(p3)
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)
  mm=sqrt(mm2);  me1=sqrt(me2)
  mu2 = musq
  logmm = log(mu2/mm2)
  logem = log(mu2/me2)
  logme = log(me2/mm2)
  logse = log(abs(-me2/ss))
  logsm = log(abs(-mm2/ss))
  discbs = DiscB(ss,mm)
  discbse = DiscB(ss,me1)
  discbtem = DiscB(tt,me1,mm)
  scalarc0ir6tme = ScalarC0IR6(tt,me1,mm)
  scalarc0se = ScalarC0(ss,me1)
  scalarc0esIM = -(pi/(4*me2-ss))*discbse
  scalarc0sm = ScalarC0(ss,mm)
  scalarc0msIM = -(pi/(4*mm2-ss))*discbs
  asym123n1 = asymtensor(p1,p2,p3,pol1)
  asym123n2 = asymtensor(p1,p2,p3,pol2)
  asym124n1 = asymtensor(p1,p2,p4,pol1)
  asym124n2 = asymtensor(p1,p2,p4,pol2)
  asym134n1 = asymtensor(p1,p3,p4,pol1)
  asym134n2 = asymtensor(p1,p3,p4,pol2)
  asym234n1 = asymtensor(p2,p3,p4,pol1)
  asym234n2 = asymtensor(p2,p3,p4,pol2)

  pepe2mml_em = real(pepe2mml_em_eva(&
       real(ss, kind=16), real(tt, kind=16), &
       real(me2, kind=16), real(mm2, kind=16), &
       real(me1, kind=16), real(mm, kind=16), &
       real(s2n, kind=16), real(s3n, kind=16), real(s4n, kind=16), &
       real(s1m, kind=16), real(s3m, kind=16), real(s4m, kind=16), real(snm, kind=16), &
       real(logmm, kind=16),real(logem, kind=16), &
       real(logme, kind=16), real(logse, kind=16), real(logsm, kind=16), &
       real(asym123n1, kind=16), real(asym123n2, kind=16), &
       real(asym124n1, kind=16), real(asym124n2, kind=16), &
       real(asym234n1, kind=16), real(asym234n2, kind=16), &
       real(asym134n1, kind=16), real(asym134n2, kind=16), &
       real(discbs, kind=16), real(discbse, kind=16), real(discbtem, kind=16), &
       real(scalarc0esIM, kind=16), real(scalarc0msIM, kind=16), &
       real(scalarc0sm, kind=16), real(scalarc0se, kind=16), real(scalarc0ir6tme, kind=16) &
    ), kind=prec)

  if (present(sing_em)) then
  sing_em =(-8*alpha**3*discbtem*pi*(me2 + mm2 - tt)*tt*(8*me2**2*(-2 + snm) + 8*mm2**2*(-2 + snm)&
   + 3*s1m*s2n*ss - 3*s2n*s3m*ss + 2*s1m*s3n*ss - 2*s3m*s3n*ss + 3*s3n*s4m*ss -&
   3*s1m*s4n*ss + 3*s3m*s4n*ss - 8*ss**2 - 5*s2n*s3m*tt + 5*s1m*s3n*tt + 3*s2n*s4m*tt -&
   3*s1m*s4n*tt - 16*ss*tt + 8*snm*ss*tt - 16*tt**2 + 8*snm*tt**2 + mm2*(-8*s1m*s2n +&
   5*s2n*s3m - 5*s1m*s3n - 3*s2n*s4m + 3*s1m*s4n + 32*tt - 16*snm*tt) + me2*(5*s2n*s3m&
   - 5*s1m*s3n - 3*s2n*s4m + 3*s1m*s4n + 16*mm2*(-2 + snm) + 32*tt -&
   16*snm*tt)))/(ss**2*(me2**2 + (mm2 - tt)**2 - 2*me2*(mm2 + tt)))
  endif

  END FUNCTION

  FUNCTION PEPE2MML_EM_EVA(ss, tt, me2, mm2, me1, mm, &
                           s2n,s3n,s4n,s1m,s3m,s4m,snm, &
                           logmm,logem, logme, logse, logsm, &
                           asym123n1, asym123n2, asym124n1, asym124n2, &
                           asym234n1, asym234n2, asym134n1, asym134n2, &
                           discbs, discbse, discbtem, &
                           scalarc0esIM, scalarc0msIM, &
                           scalarc0sm, scalarc0se, scalarc0ir6tme)

  real(kind=16) :: pepe2mml_em_eva
  real(kind=16) :: ss, tt, me2, mm2, me1, mm
  real(kind=16) :: s2n,s3n,s4n,s1m,s3m,s4m,snm
  real(kind=16) :: logmm,logem, logme, logse, logsm
  real(kind=16) :: asym123n1, asym123n2, asym124n1, asym124n2
  real(kind=16) :: asym234n1, asym234n2, asym134n1, asym134n2
  real(kind=16) :: asym_em
  real(kind=16) :: discbs, discbse, discbtem
  real(kind=16) :: scalarc0esIM, scalarc0msIM
  real(kind=16) :: scalarc0sm, scalarc0se, scalarc0ir6tme

  asym_em =asym123n1*&
   (16*alpha**3*pi*((2*discbtem*pi*tt*(me1**5*(-2*mm2 + ss) - me1*(mm2 - tt)*(2*mm2**2 +&
   mm2*(3*ss - 2*tt) + ss*tt) + 2*me1**3*(2*mm2**2 - ss*tt + mm2*(ss + 2*tt))))/(me2**2&
   + (mm2 - tt)**2 - 2*me2*(mm2 + tt)) + (me1*ss*(8*me2**3*scalarc0esIM +&
   6*mm2**2*(2*pi + (scalarc0esIM + scalarc0msIM)*ss) + 4*me2**2*(3*pi +&
   2*mm2*(7*scalarc0esIM - 5*scalarc0msIM) + scalarc0msIM*ss - 4*scalarc0esIM*tt) +&
   mm2*(3*(scalarc0esIM - scalarc0msIM)*ss**2 - 24*pi*tt - 10*(scalarc0esIM +&
   scalarc0msIM)*ss*tt) + tt*(12*pi*(ss + tt) + ss*(5*scalarc0esIM*ss + scalarc0msIM*ss&
   + 4*scalarc0esIM*tt)) - me2*(24*mm2**2*scalarc0msIM + (-scalarc0esIM +&
   scalarc0msIM)*ss**2 + 4*(6*pi + (scalarc0esIM + scalarc0msIM)*ss)*tt -&
   8*scalarc0esIM*tt**2 + mm2*(24*pi + 38*scalarc0esIM*ss - 22*scalarc0msIM*ss +&
   8*scalarc0esIM*tt - 40*scalarc0msIM*tt))))/(4*me2 - ss)))/(ss**2*(me2**2 + mm2**2 -&
   2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss + tt)))&
   +asym123n2*&
   (64*alpha**3*me1*pi*(mm2*scalarc0msIM*(-2*me2 - 2*mm2 + ss + 2*tt) +&
   (2*discbtem*mm2*pi*tt*(me2 - mm2 + tt))/(me2**2 + (mm2 - tt)**2 - 2*me2*(mm2 + tt))&
   + (pi*(me2**2 + mm2**2 - 2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss + tt)))/(4*me2 - ss) +&
   (scalarc0esIM*(2*me2**3 + mm2*ss**2 + 4*me2**2*(3*mm2 - tt) + 2*me2*(mm2**2 + tt*(ss&
   + tt) - 2*mm2*(2*ss + tt))))/(4*me2 - ss)))/(ss*(me2**2 + mm2**2 - 2*mm2*tt -&
   2*me2*(mm2 + tt) + tt*(ss + tt)))&
   +asym124n1*&
   (32*alpha**3*me1*pi*((-2*discbtem*mm2*pi*tt*(me2**2 + mm2**2 + 2*mm2*(ss - tt) -&
   2*me2*(mm2 + ss + tt) + tt*(2*ss + tt)))/(me2**2 + (mm2 - tt)**2 - 2*me2*(mm2 + tt))&
   + (ss*(4*me2**3*scalarc0esIM + mm2**2*(2*pi - 3*scalarc0esIM*ss + 7*scalarc0msIM*ss)&
   + 2*pi*tt*(ss + tt) + 2*me2**2*(pi + mm2*(6*scalarc0esIM - 2*scalarc0msIM) -&
   4*scalarc0esIM*tt) + mm2*(-4*pi*tt + ss*(2*scalarc0esIM*ss - 2*scalarc0msIM*ss +&
   3*scalarc0esIM*tt - scalarc0msIM*tt)) + me2*(4*mm2**2*(4*scalarc0esIM -&
   7*scalarc0msIM) - 4*pi*tt + 4*scalarc0esIM*tt*(ss + tt) + mm2*(-4*pi -&
   13*scalarc0esIM*ss + 9*scalarc0msIM*ss - 20*scalarc0esIM*tt +&
   4*scalarc0msIM*tt))))/(4*me2 - ss)))/(ss**2*(me2**2 + mm2**2 - 2*mm2*tt - 2*me2*(mm2&
   + tt) + tt*(ss + tt))) &
   + asym124n2*&
   (64*alpha**3*me1*pi*((-2*discbtem*mm2*pi*tt*(-me2 + mm2 + tt))/(me2**2 + (mm2 - tt)**2 -&
   2*me2*(mm2 + tt)) + (2*me2**3*scalarc0esIM - mm2**2*(pi + 3*scalarc0esIM*ss -&
   4*scalarc0msIM*ss) - (pi + scalarc0esIM*ss)*tt*(ss + tt) + mm2*((scalarc0esIM -&
   scalarc0msIM)*ss**2 + 2*(pi + 2*scalarc0esIM*ss)*tt) - me2**2*(pi +&
   scalarc0esIM*(-4*mm2 + ss + 4*tt)) + 2*me2*(mm2**2*(5*scalarc0esIM - 8*scalarc0msIM)&
   + mm2*(pi - 2*scalarc0esIM*ss + 2*scalarc0msIM*ss - 6*scalarc0esIM*tt) + tt*(pi +&
   2*scalarc0esIM*ss + scalarc0esIM*tt)))/(4*me2 - ss)))/(ss*(me2**2 + mm2**2 -&
   2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss + tt)))&
   + asym134n1*&
   (16*alpha**3*me1*pi*(2*discbtem*pi*(4*mm2 - ss)*(me2 + mm2 + 5*ss - tt)*tt + ss*(-((me1&
   - mm)*(me1 + mm)*(4*mm2**2*(scalarc0esIM + scalarc0msIM) + 5*(-scalarc0esIM +&
   scalarc0msIM)*ss**2 + mm2*(4*pi + 19*(scalarc0esIM - scalarc0msIM)*ss) + me2*(-4*pi&
   + 4*mm2*(scalarc0esIM - 3*scalarc0msIM) + (-scalarc0esIM + scalarc0msIM)*ss))) -&
   (8*mm2**2*(scalarc0esIM + 2*scalarc0msIM) + me2*(8*pi + 24*mm2*scalarc0msIM -&
   2*scalarc0msIM*ss) + 2*mm2*(4*pi + 9*scalarc0esIM*ss + 6*scalarc0msIM*ss) - ss*(4*pi&
   + 5*(scalarc0esIM + scalarc0msIM)*ss))*tt + (4*pi + 4*mm2*(scalarc0esIM +&
   3*scalarc0msIM) - (scalarc0esIM + scalarc0msIM)*ss)*tt**2)))/((4*mm2 -&
   ss)*ss**2*(me2**2 + mm2**2 - 2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss + tt)))&
   +asym134n2*&
   (32*alpha**3*me1*pi*(2*discbtem*pi*(4*mm2 - ss)*(me2 + mm2 - tt)*tt +&
   ss*(4*mm2**3*scalarc0esIM + me2**2*(2*pi - 4*mm2*(scalarc0esIM - 2*scalarc0msIM) +&
   (scalarc0esIM - scalarc0msIM)*ss) + mm2**2*(2*pi + (-scalarc0esIM + scalarc0msIM)*ss&
   - 8*(scalarc0esIM + scalarc0msIM)*tt) + tt*(-((scalarc0esIM + scalarc0msIM)*ss*tt) +&
   2*pi*(ss + tt)) + 2*mm2*tt*(-2*pi + (scalarc0esIM + 2*scalarc0msIM)*(ss + 2*tt)) -&
   2*me2*(4*mm2**2*scalarc0msIM + (2*pi - scalarc0msIM*ss)*tt + 2*mm2*(pi +&
   4*scalarc0msIM*tt)))))/((4*mm2 - ss)*ss**2*(me2**2 + mm2**2 - 2*mm2*tt - 2*me2*(mm2&
   + tt) + tt*(ss + tt)))&
   +asym234n1*&
   (16*alpha**3*me1*pi*((2*discbtem*pi*tt*(me2**3 - me2**2*(mm2 - 4*ss + 3*tt) + (mm2 -&
   tt)*(mm2**2 - 2*(mm2 + 2*ss)*tt + tt**2) - me2*(mm2**2 + (8*ss - 3*tt)*tt +&
   2*mm2*(2*ss + tt))))/(me2**2 + (mm2 - tt)**2 - 2*me2*(mm2 + tt)) + ss*((4*pi*(me2**2&
   + mm2**2 - 2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss + tt)))/(4*mm2 - ss) +&
   (scalarc0msIM*(-28*mm2**3 + me2**2*(12*mm2 - ss) + 9*mm2**2*ss + 4*(-2*mm2 +&
   ss)**2*tt + (12*mm2 - ss)*tt**2 + me2*(-48*mm2**2 + 24*mm2*(ss - tt) + 2*ss*(-2*ss +&
   tt))))/(4*mm2 - ss) + scalarc0esIM*(3*me2**2 + 5*(mm2 - tt)**2 + me2*(8*mm2 - 4*(ss&
   + 2*tt))))))/(ss**2*(me2**2 + mm2**2 - 2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss + tt)))&
   +asym234n2*&
   (32*alpha**3*me1*pi*((2*discbtem*pi*tt*(me2**3 - me2**2*(mm2 + 3*tt) - me2*(mm2**2 -&
   3*tt**2 + 2*mm2*(-ss + tt)) + (mm2 - tt)*(mm2**2 + tt**2 - 2*mm2*(ss +&
   tt))))/(me2**2 + (mm2 - tt)**2 - 2*me2*(mm2 + tt)) + ss*(scalarc0esIM*(me2**2 +&
   4*me2*(mm2 - tt) + (3*mm2 - 2*ss - 3*tt)*(mm2 - tt)) + (2*pi*(me2**2 + mm2**2 -&
   2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss + tt)))/(4*mm2 - ss) +&
   (scalarc0msIM*(-16*mm2**3 + me2**2*(8*mm2 - ss) - ss*tt**2 + mm2**2*(13*ss + 8*tt) +&
   2*me2*(-12*mm2**2 + 2*mm2*(ss - 4*tt) + ss*tt) - 2*mm2*(ss**2 - 4*tt**2)))/(4*mm2 -&
   ss))))/(ss**2*(me2**2 + mm2**2 - 2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss + tt)))


  pepe2mml_em_eva = (4*alpha**3*pi*((-2*discbtem*logem*(me2 + mm2 - tt)*tt*(8*me2**2*(-2 + snm) +&
   8*mm2**2*(-2 + snm) + 3*s1m*s2n*ss - 3*s2n*s3m*ss + 2*s1m*s3n*ss - 2*s3m*s3n*ss +&
   3*s3n*s4m*ss - 3*s1m*s4n*ss + 3*s3m*s4n*ss - 8*ss**2 - 5*s2n*s3m*tt + 5*s1m*s3n*tt +&
   3*s2n*s4m*tt - 3*s1m*s4n*tt - 16*ss*tt + 8*snm*ss*tt - 16*tt**2 + 8*snm*tt**2 +&
   mm2*(-8*s1m*s2n + 5*s2n*s3m - 5*s1m*s3n - 3*s2n*s4m + 3*s1m*s4n + 32*tt - 16*snm*tt)&
   + me2*(5*s2n*s3m - 5*s1m*s3n - 3*s2n*s4m + 3*s1m*s4n + 16*mm2*(-2 + snm) + 32*tt -&
   16*snm*tt)))/(me2**2 + (mm2 - tt)**2 - 2*me2*(mm2 + tt)) - (2*logsm*ss*(4*mm2**2*(-2&
   + snm) - 2*s1m*s3n*ss + 2*s3m*s3n*ss - s3n*s4m*ss + s1m*s4n*ss - s3m*s4n*ss +&
   2*ss**2 + me2*(s2n*s3m - s1m*s3n + s2n*s4m - s1m*s4n + 4*mm2*(-2 + snm) + (-2 +&
   snm)*ss) - s2n*s3m*tt + s1m*s3n*tt - s2n*s4m*tt + s1m*s4n*tt + 2*ss*tt - snm*ss*tt -&
   mm2*(s1m*s3n - s2n*(5*s3m + s4m) + s1m*s4n + (2 + 3*snm)*ss - 8*tt +&
   4*snm*tt)))/(4*mm2 - ss) - (2*logme*ss*(4*mm2**5*(-2 + snm) - me2**4*(-2 +&
   snm)*(4*mm2 - ss) + ss*tt**2*((s1m - s3m)*(s2n - 3*s3n)*ss + (s1m*s2n - 2*s2n*s3m -&
   3*s1m*s3n + 4*s3m*s3n)*tt) + me2**3*(4*mm2 - ss)*(2*mm2*(-2 + snm) + 2*ss + 3*(-2 +&
   snm)*tt) - mm2**4*(-4*s2n*s3m + 4*s1m*(s2n + s3n) + (6 + snm)*ss + 12*(-2 + snm)*tt)&
   + mm2*tt*(2*ss**3 + 4*s1m*s3n*tt**2 + ss**2*(4*s1m*s3n - 4*s3m*s3n + snm*tt) +&
   ss*tt*(3*s2n*s3m - 3*s1m*(s2n - 5*s3n) - 12*s3m*s3n - 2*tt + snm*tt)) +&
   mm2**3*(2*ss**2 + 4*tt*(2*s1m*s2n - 2*s2n*s3m + 3*s1m*s3n - 6*tt + 3*snm*tt) +&
   ss*(-(s2n*s3m) - 4*s3m*s3n + s1m*(s2n + 5*s3n) + 2*tt + 7*snm*tt)) -&
   mm2**2*(4*tt**2*(-(s2n*s3m) + s1m*(s2n + 3*s3n) + (-2 + snm)*tt) + ss*tt*(3*s1m*s2n&
   - 4*s2n*s3m + 17*s1m*s3n - 12*s3m*s3n - 6*tt + 7*snm*tt) + ss**2*(s1m*s3n - s3m*s3n&
   + (10 + snm)*tt)) - me2*(8*mm2**4*(-2 + snm) - 2*mm2**3*(-4*s2n*s3m + 4*s1m*(s2n +&
   s3n) + (10 + snm)*ss - 4*tt + 2*snm*tt) + mm2**2*(6*ss**2 + 8*s2n*(-s1m + s3m)*tt +&
   ss*(-2*s2n*s3m + 2*s1m*(s2n + s3n) + (-2 + snm)*tt)) + ss*tt*(2*ss**2 +&
   ss*(-(s2n*s3m) + s1m*(s2n - s3n) + 2*s3m*s3n + snm*tt) + tt*(-4*s2n*s3m + 2*s1m*(s2n&
   - 3*s3n) + 8*s3m*s3n - 2*tt + snm*tt)) - 2*mm2*tt*(4*ss**2 + 2*tt*(-2*s1m*s3n + (-2&
   + snm)*tt) + ss*(-(s2n*s3m) + s1m*(s2n - 2*s3n) + 4*s3m*s3n + 2*snm*tt))) +&
   me2**2*(-4*mm2**2*(-(s2n*s3m) + s1m*(s2n + s3n) + 6*ss - 2*tt + snm*tt) +&
   mm2*(6*ss**2 + 4*tt*(s1m*s3n - 3*(-2 + snm)*tt) + ss*(-(s2n*s3m) + s1m*(s2n - 3*s3n)&
   + 4*s3m*s3n - 10*tt - 3*snm*tt)) + ss*(tt*(-2*s2n*s3m + s1m*(s2n - 3*s3n) +&
   4*s3m*s3n - 6*tt + 3*snm*tt) + ss*(s1m*s3n - s3m*s3n + (2 + snm)*tt)))))/((4*mm2 -&
   ss)*tt*(me2**2 + mm2**2 - 2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss + tt))) -&
   (2*logse*ss*(-4*me2**4*(-2 + snm)*(4*mm2 - ss) + me2**3*(16*mm2**2*(-2 + snm) +&
   8*mm2*(s1m*s3n + s2n*(s3m + s4m) - s1m*s4n + (2 + snm)*ss - 12*tt + 6*snm*tt) -&
   ss*((2 + 3*snm)*ss + 2*(5*s2n*s3m - 8*s3m*s3n + s2n*s4m - s1m*(2*s2n - 5*s3n + s4n)&
   - 12*tt + 6*snm*tt))) + ss*(-4*mm2**4*(-2 + snm) + mm2**3*(-4*s2n*s3m + (-10 +&
   snm)*ss + 12*(-2 + snm)*tt) + mm2**2*(2*ss**2 + 4*tt*(2*s2n*s3m - 3*(-2 + snm)*tt) +&
   ss*(-(s2n*s3m) - 4*s3m*s3n + s1m*(s2n + 6*s3n) + 30*tt - 7*snm*tt)) + ss*tt*(2*ss**2&
   + ss*(-(s1m*s2n) + s2n*s3m + 2*s1m*s3n - 2*s3m*s3n + 4*tt - snm*tt) - tt*(-3*s2n*s3m&
   + s1m*(s2n - 2*s3n) + 4*s3m*s3n - 2*tt + snm*tt)) + mm2*(ss**2*(-2*s1m*s3n +&
   2*s3m*s3n + (-14 + snm)*tt) + 4*tt**2*(-(s2n*s3m) + (-2 + snm)*tt) +&
   ss*tt*(-6*s2n*s3m + 4*s1m*(s2n - 2*s3n) + 8*s3m*s3n - 22*tt + 7*snm*tt))) +&
   me2**2*(16*mm2**3*(-2 + snm) - 16*mm2**2*(s2n*(s3m + s4m) + s1m*(s3n - s4n) +&
   2*snm*ss + 4*tt - 2*snm*tt) + mm2*((-6 + 7*snm)*ss**2 - 16*tt*(s2n*(s3m + s4m) +&
   s1m*(s3n - s4n) + 3*(-2 + snm)*tt) + 4*ss*(8*s3m*s3n + s2n*s4m + s1m*(2*s2n - 7*s3n&
   - s4n) + 6*tt - 11*snm*tt)) + ss*(2*ss**2 + 4*tt*(5*s2n*s3m - 8*s3m*s3n + s2n*s4m -&
   s1m*(2*s2n - 5*s3n + s4n) - 6*tt + 3*snm*tt) + ss*(-3*s1m*s2n + 3*s2n*s3m +&
   10*s1m*s3n - 12*s3m*s3n - 2*tt + 9*snm*tt))) + me2*(-16*mm2**4*(-2 + snm) +&
   8*mm2**3*(s1m*s3n + s2n*(s3m + s4m) - s1m*s4n + (-2 + 3*snm)*ss - 12*tt + 6*snm*tt)&
   - 2*mm2*(2*ss**3 - 4*tt**2*(s2n*(s3m + s4m) + s1m*(s3n - s4n) + 2*(-2 + snm)*tt) +&
   ss**2*(s2n*s3m + 8*s3m*s3n - s1m*(s2n + 8*s3n) - 10*tt - 11*snm*tt) +&
   2*ss*tt*(-5*s2n*s3m + 8*s3m*s3n - 3*s2n*s4m + s1m*(2*s2n - 11*s3n + 3*s4n) + 8*tt -&
   8*snm*tt)) - mm2**2*((-18 + 5*snm)*ss**2 + 16*tt*(s2n*(s3m + s4m) + s1m*(s3n - s4n)&
   + 3*(-2 + snm)*tt) + 2*ss*(-8*s3m*s3n + s2n*(-7*s3m + s4m) + s1m*(6*s2n + 13*s3n -&
   s4n) - 20*tt + 26*snm*tt)) - ss*(2*tt**2*(-8*s3m*s3n + s2n*(5*s3m + s4m) -&
   s1m*(2*s2n - 5*s3n + s4n) - 4*tt + 2*snm*tt) + ss*tt*(-16*s3m*s3n + 2*s2n*(4*s3m +&
   s4m) - 2*s1m*(2*s2n - 7*s3n + s4n) - 2*tt + 5*snm*tt) + ss**2*(2*s1m*s3n - 2*s3m*s3n&
   + 3*(2 + snm)*tt)))))/((4*me2 - ss)*(-4*mm2 + ss)*(me2**2 + mm2**2 - 2*mm2*tt -&
   2*me2*(mm2 + tt) + tt*(ss + tt))) + (2*scalarc0ir6tme*ss*(me2**5*(s1m*s3n - s2n*(s3m&
   + s4m) + s1m*s4n + 2*(2 + snm)*ss + 8*tt - 4*snm*tt) - me2**4*(4*tt*(-(s2n*s4m) +&
   s1m*s4n - 5*(-2 + snm)*tt) + mm2*(s1m*s3n - 4*s3n*s4m - 5*s2n*(s3m + s4m) + s1m*s4n&
   + 4*s3m*s4n + 6*(2 + snm)*ss + 24*tt - 12*snm*tt) + ss*(s2n*s3m - 6*s3m*s3n +&
   s3n*s4m + s3m*s4n - s1m*(s2n - 4*s3n + s4n) + 20*tt + 10*snm*tt)) +&
   me2**3*(2*tt**2*(5*s2n*s3m - 5*s1m*s3n - 3*s2n*s4m + 3*s1m*s4n + 40*tt - 20*snm*tt)&
   - 2*mm2**2*(s1m*s3n + 6*s3n*s4m + 5*s2n*(s3m + s4m) + s1m*s4n - 6*s3m*s4n - 2*(2 +&
   snm)*ss - 8*tt + 4*snm*tt) + ss*tt*(-14*s3m*s3n - s2n*s4m + 3*s3n*s4m + s1m*(-3*s2n&
   + 11*s3n - 2*s4n) + 3*s3m*s4n + 56*tt + 12*snm*tt) + 2*ss**2*(s1m*s3n - s3m*s3n + (4&
   + snm)*tt) + 4*mm2*(ss*(-2*s3m*s3n + s3n*s4m + s1m*(s3n - s4n) + s3m*s4n + 4*tt +&
   2*snm*tt) - tt*(-(s1m*s2n) + s2n*s3m + 2*s1m*s3n + 3*s2n*s4m + s3n*s4m - s3m*s4n -&
   8*tt + 4*snm*tt))) + me2**2*(2*mm2**3*(s1m*s3n + 6*s3n*s4m + 5*s2n*(s3m + s4m) +&
   s1m*s4n - 6*s3m*s4n + 2*(2 + snm)*ss + 8*tt - 4*snm*tt) + mm2**2*(4*tt*(-(s1m*s2n) +&
   2*s1m*s3n + 2*s2n*s4m - 2*s3n*s4m + 2*s3m*s4n + 4*tt - 2*snm*tt) + ss*(6*s2n*s3m -&
   4*s3m*s3n - 6*s3n*s4m - 6*s3m*s4n + s1m*(-6*s2n + 4*s3n + 6*s4n) + 8*tt + 4*snm*tt))&
   + tt*(4*tt**2*(s2n*(-5*s3m + s4m) + s1m*(5*s3n - s4n) + 10*(-2 + snm)*tt) +&
   ss*tt*(6*s3m*s3n - 3*s3n*s4m + s2n*(s3m + 2*s4m) - 3*s3m*s4n + s1m*(3*s2n - 4*s3n +&
   s4n) - 88*tt + 4*snm*tt) - ss**2*(-(s2n*s3m) - 4*s3m*s3n + s3n*s4m + s1m*(s2n +&
   4*s3n - s4n) + s3m*s4n + 24*tt + 6*snm*tt)) + mm2*(2*tt**2*(-2*s3n*s4m + 5*s2n*(s3m&
   + s4m) + 2*s3m*s4n + s1m*(-6*s2n + s3n + s4n) - 12*(-2 + snm)*tt) +&
   ss*tt*(-10*s3m*s3n + s3n*s4m + 3*s2n*(2*s3m + s4m) - 7*s3m*s4n + s1m*(-s2n + 11*s3n&
   + 4*s4n) + 8*tt + 20*snm*tt) - 2*ss**2*(s1m*s3n - s3m*s3n + (4 + snm)*tt))) + (mm2 -&
   tt)*(mm2**4*(-(s1m*s3n) + s2n*(s3m + s4m) - s1m*s4n + 2*(2 + snm)*ss + 8*tt -&
   4*snm*tt) - 2*tt**2*(ss + 2*tt)*(-2*ss**2 + ss*(s1m*s3n - s3m*s3n + (-4 + snm)*tt) +&
   tt*(-(s2n*s3m) + s1m*s3n + (-2 + snm)*tt)) - mm2*tt*(tt**2*(s2n*(13*s3m + s4m) -&
   s1m*(4*s2n + 13*s3n + s4n) - 16*(-2 + snm)*tt) + ss*tt*(10*s3m*s3n + s3n*s4m +&
   s2n*(-2*s3m + s4m) + s3m*s4n - s1m*(s2n + 9*s3n + 2*s4n) + 48*tt - 8*snm*tt) +&
   ss**2*(-(s2n*s3m) - 2*s3m*s3n + s3n*s4m + s1m*(s2n + 2*s3n - s4n) + s3m*s4n + 16*tt&
   + 4*snm*tt)) - mm2**3*(tt*(7*s2n*s3m + 3*s2n*s4m - s1m*(4*s2n + 7*s3n + 3*s4n) +&
   32*tt - 16*snm*tt) + ss*(3*s1m*s2n - 3*s2n*s3m + 2*s3m*s3n + s3n*s4m - s1m*s4n +&
   s3m*s4n + 16*tt + 8*snm*tt)) + mm2**2*(tt**2*(-8*s1m*s2n + 3*s2n*(5*s3m + s4m) -&
   3*s1m*(5*s3n + s4n) - 24*(-2 + snm)*tt) + ss*tt*(6*s1m*s2n - 7*s2n*s3m + 8*s3m*s3n +&
   s2n*s4m + 2*s3n*s4m + 2*s3m*s4n - 3*s1m*(s3n + s4n) + 40*tt + 4*snm*tt) +&
   2*ss**2*(s1m*s3n - s3m*s3n + (4 + snm)*tt))) + me2*(mm2**4*(s1m*s3n - 4*s3n*s4m -&
   5*s2n*(s3m + s4m) + s1m*s4n + 4*s3m*s4n - 6*(2 + snm)*ss - 24*tt + 12*snm*tt) +&
   4*mm2**3*(tt*(3*s2n*s3m - s1m*(s2n + 2*s3n) + s2n*s4m + 3*s3n*s4m - 3*s3m*s4n + 8*tt&
   - 4*snm*tt) + ss*(-2*s2n*s3m + 2*s3m*s3n + s3n*s4m + s1m*(2*s2n - s3n - s4n) +&
   s3m*s4n + 4*tt + 2*snm*tt)) + tt**2*(4*ss**3 + tt**2*(15*s2n*s3m - 15*s1m*s3n -&
   s2n*s4m + s1m*s4n + 40*tt - 20*snm*tt) + ss*tt*(2*s2n*s3m + 6*s3m*s3n - s1m*(s2n +&
   9*s3n) - s2n*s4m + s3n*s4m + s3m*s4n + 68*tt - 14*snm*tt) + ss**2*(-(s2n*s3m) +&
   s3n*s4m + s1m*(s2n - s4n) + s3m*s4n + 32*tt + 2*snm*tt)) +&
   2*mm2*tt*(ss*tt*(2*s3m*s3n + s2n*(s3m - 2*s4m) + s3n*s4m - 3*s3m*s4n + s1m*(s2n -&
   4*s3n + s4n) - 40*tt - 4*snm*tt) + ss**2*(-(s2n*s3m) + s3n*s4m + s1m*(s2n - s4n) +&
   s3m*s4n - 8*tt - 2*snm*tt) + 2*tt**2*(3*s1m*(s2n + 2*s3n) + s3n*s4m - s2n*(7*s3m +&
   s4m) - s3m*s4n - 24*tt + 12*snm*tt)) + mm2**2*(ss*tt*(4*s2n*s3m - 18*s3m*s3n -&
   3*s2n*s4m - 7*s3n*s4m + s3m*s4n + s1m*(-5*s2n + 13*s3n + 2*s4n) + 8*tt + 20*snm*tt)&
   - 2*ss**2*(s1m*s3n - s3m*s3n + (4 + snm)*tt) - 2*tt**2*(s1m*(4*s2n + s3n + s4n) -&
   3*(-2*s3n*s4m + s2n*(s3m + s4m) + 2*s3m*s4n + 8*tt - 4*snm*tt))))))/(me2**2 + mm2**2&
   - 2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss + tt))**2 -&
   (scalarc0se*ss*(4*me2**6*(-3*s2n*s3m + 3*s1m*s3n - 3*s2n*s4m + 4*s3n*s4m + 3*s1m*s4n&
   - 4*s3m*s4n + 8*mm2*(-2 + snm) + 2*(-2 + snm)*ss + 16*tt - 8*snm*tt) +&
   me2**5*(-96*mm2**2*(-2 + snm) - 4*(2 + 3*snm)*ss**2 + 4*mm2*(3*s1m*s3n + 5*s2n*(s3m&
   + s4m) + 3*s1m*s4n - 8*(-2 + snm)*ss + 32*tt - 16*snm*tt) + 4*tt*(7*s2n*s3m -&
   7*s1m*s3n + 15*s2n*s4m - 16*s3n*s4m - 15*s1m*s4n + 16*s3m*s4n - 80*tt + 40*snm*tt) +&
   ss*(s2n*(-11*s3m + 5*s4m) + 3*s1m*(4*s2n - 11*s3n - 3*s4n) + 8*(5*s3m*s3n + s3m*s4n&
   + 4*tt - 2*snm*tt))) + ss*(-8*mm2**6*(-2 + snm) + mm2**5*(s2n*(-5*s3m + 3*s4m) +&
   s1m*(8*s2n + 5*s3n - 3*s4n) + 48*(-2 + snm)*tt) - tt**2*(-4*ss**4 +&
   3*ss**2*tt*(2*s3n*s4m + s2n*(-3*s3m + s4m) + s1m*(2*s2n + s3n - 3*s4n) + 2*s3m*s4n +&
   6*(-2 + snm)*tt) + tt**3*(s2n*(-5*s3m + 3*s4m) + s1m*(5*s3n - 3*s4n) + 8*(-2 +&
   snm)*tt) + ss**3*(-3*s2n*s3m + 3*s3n*s4m + 3*s1m*(s2n - s4n) + 3*s3m*s4n - 16*tt +&
   6*snm*tt) + ss*tt**2*(-9*s2n*s3m - 2*s3m*s3n + 6*s2n*s4m + 3*s3n*s4m + s1m*(3*s2n +&
   8*s3n - 9*s4n) + 3*s3m*s4n - 40*tt + 20*snm*tt)) - mm2**4*(2*(-2 + snm)*ss**2 +&
   tt*(-5*s2n*(5*s3m - 3*s4m) + s1m*(32*s2n + 25*s3n - 15*s4n) + 120*(-2 + snm)*tt) +&
   ss*(-2*s3m*s3n + 3*s3n*s4m + s2n*(-2*s3m + s4m) + s1m*(3*s2n + s3n - 4*s4n) +&
   3*s3m*s4n - 40*tt + 20*snm*tt)) - mm2**2*(2*ss**3*(s1m*s3n - s3m*s3n + (-4 +&
   snm)*tt) + 2*tt**3*(5*s2n*(-5*s3m + 3*s4m) + s1m*(16*s2n + 25*s3n - 15*s4n) + 60*(-2&
   + snm)*tt) + ss**2*tt*(-13*s2n*s3m + 4*s3m*s3n + s2n*s4m + 8*s3n*s4m + s1m*(12*s2n +&
   s3n - 9*s4n) + 8*s3m*s4n - 56*tt + 28*snm*tt) + 3*ss*tt**2*(-11*s2n*s3m - 4*s3m*s3n&
   + 7*s2n*s4m + 6*s3n*s4m + s1m*(14*s2n + 9*s3n - 13*s4n) + 6*s3m*s4n - 80*tt +&
   40*snm*tt)) + mm2*tt*(tt**3*(-5*s2n*(5*s3m - 3*s4m) + s1m*(8*s2n + 25*s3n - 15*s4n)&
   + 48*(-2 + snm)*tt) + ss**3*(-(s2n*s3m) - 2*s3m*s3n + s3n*s4m + s1m*(s2n + 2*s3n -&
   s4n) + s3m*s4n - 16*tt + 4*snm*tt) + ss**2*tt*(-19*s2n*s3m + 2*s3m*s3n + 4*s2n*s4m +&
   13*s3n*s4m + s1m*(19*s2n + 4*s3n - 17*s4n) + 13*s3m*s4n - 80*tt + 40*snm*tt) +&
   ss*tt**2*(-29*s2n*s3m - 8*s3m*s3n + 19*s2n*s4m + 12*s3n*s4m + s1m*(24*s2n + 25*s3n -&
   31*s4n) + 12*s3m*s4n - 160*tt + 80*snm*tt)) + mm2**3*(2*tt**2*(5*s2n*(-5*s3m +&
   3*s4m) + s1m*(24*s2n + 25*s3n - 15*s4n) + 80*(-2 + snm)*tt) + ss**2*(3*s1m*s2n -&
   3*s2n*s3m + 2*s3m*s3n + s3n*s4m - s1m*s4n + s3m*s4n - 16*tt + 8*snm*tt) +&
   ss*tt*(-15*s2n*s3m - 8*s3m*s3n + 9*s2n*s4m + 12*s3n*s4m + s1m*(24*s2n + 11*s3n -&
   21*s4n) + 12*s3m*s4n - 160*tt + 80*snm*tt))) + me2**4*(64*mm2**3*(-2 + snm) + 2*(2 +&
   snm)*ss**3 + ss*tt*(5*s2n*(9*s3m - 7*s4m) + 24*s3n*s4m - 8*s3m*(14*s3n + 9*s4n) +&
   s1m*(-48*s2n + 91*s3n + 59*s4n) - 88*(-2 + snm)*tt) + 8*mm2**2*(-(s2n*s3m) +&
   3*s2n*s4m - 4*s3n*s4m + s1m*(4*s2n - 3*s3n - 7*s4n) + 4*s3m*s4n + 5*(-2 + snm)*ss -&
   16*tt + 8*snm*tt) - 8*tt**2*(-(s2n*s3m) + s1m*s3n + 15*s2n*s4m - 12*s3n*s4m -&
   15*s1m*s4n + 12*s3m*s4n - 80*tt + 40*snm*tt) + ss**2*(8*s2n*s3m - 50*s3m*s3n -&
   s2n*s4m + 3*s3n*s4m + s1m*(-9*s2n + 39*s3n - 2*s4n) + 3*s3m*s4n + 8*tt + 44*snm*tt)&
   + mm2*(32*(1 + snm)*ss**2 + 32*tt*(-(s2n*s4m) - 2*s3n*s4m + s1m*(s2n - 3*s3n -&
   2*s4n) + 2*s3m*s4n - 12*tt + 6*snm*tt) + ss*(s2n*(11*s3m - 13*s4m) + s1m*(-16*s2n +&
   37*s3n + 13*s4n) - 8*(5*s3n*s4m + s3m*(6*s3n + s4n) - 6*(-2 + snm)*tt)))) +&
   me2**3*(-8*mm2**3*(12*s1m*s2n - 3*s2n*s3m + 7*s1m*s3n + 9*s2n*s4m - 5*s1m*s4n) +&
   64*mm2**4*(-2 + snm) + ss**2*tt*(-23*s2n*s3m + 92*s3m*s3n + 7*s2n*s4m + 6*s3n*s4m +&
   s1m*(30*s2n - 83*s3n - 21*s4n) + 22*s3m*s4n - 80*tt - 8*snm*tt) - ss**3*(s2n*s3m -&
   18*s3m*s3n + s3n*s4m + s3m*s4n - s1m*(s2n - 16*s3n + s4n) + 32*tt + 20*snm*tt) +&
   8*tt**3*(-9*s2n*s3m + 9*s1m*s3n + 15*s2n*s4m - 8*s3n*s4m - 15*s1m*s4n + 8*s3m*s4n -&
   80*tt + 40*snm*tt) + 2*ss*tt**2*(-49*s2n*s3m + 48*s3m*s3n + 43*s2n*s4m - 20*s3n*s4m&
   + s1m*(36*s2n - 23*s3n - 67*s4n) + 68*s3m*s4n - 288*tt + 144*snm*tt) -&
   2*mm2**2*(12*(2 + snm)*ss**2 - ss*(44*s3n*s4m + 3*s2n*(s3m + s4m) + s1m*(9*s3n -&
   7*s4n) + 4*s3m*(-4*s3n + s4n) + 16*(-2 + snm)*tt) - 4*tt*(-8*s1m*s2n - 8*s3n*s4m +&
   s2n*(s3m + s4m) + 8*s3m*s4n - 9*s1m*(s3n + s4n) + 16*(-2 + snm)*tt)) - 2*mm2*(2*(4 +&
   snm)*ss**3 + ss**2*(-10*s3m*s3n + s2n*(s3m - 2*s4m) + s3n*s4m + s1m*(-3*s2n + 4*s3n&
   - 3*s4n) + 5*s3m*s4n + 16*snm*tt) + 4*tt**2*(s2n*(-5*s3m + 7*s4m) + s1m*(12*s2n -&
   7*s3n - 19*s4n) + 16*(-(s3n*s4m) + s3m*s4n + 4*(-2 + snm)*tt)) + 2*ss*tt*(s2n*(s3m -&
   s4m) + s1m*(10*s2n - 31*s3n - 13*s4n) + 4*(-5*s3n*s4m + s3m*(4*s3n + s4n) + 4*(-2 +&
   snm)*tt)))) - me2**2*(96*mm2**5*(-2 + snm) + 4*mm2**4*(11*s2n*s3m - 13*s2n*s4m -&
   4*s3n*s4m + 4*s3m*s4n + s1m*(-24*s2n - 19*s3n + 5*s4n) + 10*(-2 + snm)*ss + 112*tt -&
   56*snm*tt) + ss**3*tt*(24*s3m*s3n - 2*s3n*s4m + s2n*(s3m + s4m) - 2*s3m*s4n +&
   s1m*(2*s2n - 23*s3n + s4n) - 56*tt - 16*snm*tt) + 4*tt**4*(-17*s2n*s3m + 17*s1m*s3n&
   + 15*s2n*s4m - 4*s3n*s4m - 15*s1m*s4n + 4*s3m*s4n - 80*tt + 40*snm*tt) +&
   2*ss*tt**3*(-55*s2n*s3m + 8*s3m*s3n + 45*s2n*s4m - 4*s3n*s4m + s1m*(24*s2n + 27*s3n&
   - 65*s4n) + 44*s3m*s4n - 296*tt + 148*snm*tt) - 2*ss**4*(s1m*s3n - s3m*s3n + (4 +&
   snm)*tt) - 2*mm2**3*(16*ss**2 + ss*(-3*s2n*s3m + 24*s3m*s3n + 5*s2n*s4m - 28*s3n*s4m&
   - 12*s3m*s4n + s1m*(4*s2n - 5*s3n + 11*s4n) + 96*tt - 48*snm*tt) - 16*tt*(3*s1m*s2n&
   - 2*s2n*s3m + 5*s1m*s3n + 3*s2n*s4m + 2*s3n*s4m - 2*s3m*s4n + 4*tt - 2*snm*tt)) +&
   ss**2*tt**2*(s2n*(-71*s3m + 17*s4m) + s1m*(52*s2n - 25*s3n - 65*s4n) + 8*(6*s3m*s3n&
   + 2*s3n*s4m + 10*s3m*s4n - 34*tt + 15*snm*tt)) - 2*mm2**2*(12*ss**3 +&
   ss*tt*(13*s2n*s3m - 56*s3m*s3n + 25*s2n*s4m + 60*s3n*s4m + s1m*(32*s2n + 63*s3n -&
   5*s4n) - 20*s3m*s4n - 16*tt + 8*snm*tt) - 12*tt**2*(-3*s2n*s3m + s2n*s4m - 4*s3n*s4m&
   + s1m*(4*s2n - s3n - 5*s4n) + 4*s3m*s4n - 48*tt + 24*snm*tt) - ss**2*(4*s3n*s4m +&
   3*s2n*(3*s3m + s4m) - 4*s3m*(6*s3n + s4n) + s1m*(-6*s2n + 19*s3n + s4n) + 4*(-2 +&
   3*snm)*tt)) + mm2*(-(ss**3*(-(s2n*s3m) - 2*s3m*s3n + 3*s3n*s4m + s1m*(s2n - 3*s4n) +&
   3*s3m*s4n + 48*tt + 16*snm*tt)) - 32*tt**3*(-5*s2n*s3m + 4*s2n*s4m - 2*s3n*s4m +&
   s1m*(3*s2n + 4*s3n - 5*s4n) + 2*s3m*s4n - 34*tt + 17*snm*tt) -&
   2*ss*tt**2*(-65*s2n*s3m + 40*s3m*s3n + 15*s2n*s4m - 36*s3n*s4m + s1m*(84*s2n -&
   31*s3n - 71*s4n) + 76*s3m*s4n - 416*tt + 208*snm*tt) + ss**2*tt*(s2n*(-27*s3m + s4m)&
   + s1m*(20*s2n + 11*s3n - 17*s4n) + 8*(9*s3n*s4m + 3*s3m*s4n - 8*tt + 6*snm*tt)))) +&
   me2*(32*mm2**6*(-2 + snm) + 4*mm2**5*(5*s2n*s3m - 3*s2n*s4m + s1m*(-8*s2n - 5*s3n +&
   3*s4n) + 8*(-2 + snm)*ss + 96*tt - 48*snm*tt) + 2*mm2**3*(2*(-4 + snm)*ss**3 -&
   2*ss*tt*(8*s3n*s4m + s2n*(-9*s3m + s4m) + s1m*(18*s2n - s3n - 19*s4n) - 8*s3m*(s3n -&
   2*s4n) + 64*(-2 + snm)*tt) - 4*tt**2*(5*s2n*(-5*s3m + 3*s4m) + s1m*(24*s2n + 25*s3n&
   - 15*s4n) + 80*(-2 + snm)*tt) + ss**2*(5*s2n*s3m - 10*s3m*s3n + 2*s2n*s4m +&
   5*s3n*s4m + s1m*(-3*s2n + 4*s3n - 3*s4n) + s3m*s4n - 32*tt + 16*snm*tt)) +&
   tt*(-(ss**4*(-(s2n*s3m) - 2*s3m*s3n + s3n*s4m + s1m*(s2n + 2*s3n - s4n) + s3m*s4n +&
   24*tt + 4*snm*tt)) + 4*tt**4*(-5*s2n*s3m + 5*s1m*s3n + 3*s2n*s4m - 3*s1m*s4n - 16*tt&
   + 8*snm*tt) + ss**3*tt*(-23*s2n*s3m + 6*s3m*s3n + 17*s3n*s4m + s1m*(19*s2n - 4*s3n -&
   21*s4n) + 25*s3m*s4n - 96*tt + 36*snm*tt) + ss*tt**3*(-51*s2n*s3m - 8*s3m*s3n +&
   37*s2n*s4m + 8*s3n*s4m + s1m*(12*s2n + 47*s3n - 49*s4n) + 16*s3m*s4n - 224*tt +&
   112*snm*tt) + ss**2*tt**2*(-57*s2n*s3m + 4*s3m*s3n + 25*s2n*s4m + 26*s3n*s4m +&
   s1m*(34*s2n + 19*s3n - 59*s4n) + 42*s3m*s4n - 232*tt + 116*snm*tt)) +&
   mm2**2*(8*tt**3*(5*s2n*(-5*s3m + 3*s4m) + s1m*(16*s2n + 25*s3n - 15*s4n) + 60*(-2 +&
   snm)*tt) + ss**3*(5*s2n*s3m - 18*s3m*s3n - 3*s3n*s4m - 3*s3m*s4n + s1m*(-5*s2n +&
   16*s3n + 3*s4n) - 4*snm*tt) + ss**2*tt*(-53*s2n*s3m + 44*s3m*s3n - 15*s2n*s4m +&
   6*s3n*s4m + s1m*(30*s2n - 29*s3n - 23*s4n) + 38*s3m*s4n - 80*tt + 40*snm*tt) +&
   2*ss*tt**2*(-69*s2n*s3m - 24*s3m*s3n + 39*s2n*s4m + 24*s3n*s4m + s1m*(96*s2n +&
   45*s3n - 87*s4n) + 48*s3m*s4n - 544*tt + 272*snm*tt)) + mm2**4*(4*(-2 + snm)*ss**2 +&
   4*tt*(-5*s2n*(5*s3m - 3*s4m) + s1m*(32*s2n + 25*s3n - 15*s4n) + 120*(-2 + snm)*tt) +&
   ss*(s2n*(5*s3m - 11*s4m) - s1m*(12*s2n + 17*s3n + 9*s4n) + 8*(-(s3m*s3n) + s3n*s4m +&
   2*s3m*s4n + 4*tt - 2*snm*tt))) - 2*mm2*tt*(8*ss**4 + 2*tt**3*(-5*s2n*(5*s3m - 3*s4m)&
   + s1m*(8*s2n + 25*s3n - 15*s4n) + 48*(-2 + snm)*tt) - ss**3*(6*s3m*s3n + 5*s3n*s4m +&
   s2n*(-2*s3m + s4m) + s1m*(3*s2n - 5*s3n - 2*s4n) + s3m*s4n + 8*tt + 6*snm*tt) +&
   ss**2*tt*(-50*s2n*s3m + 14*s3m*s3n + 7*s2n*s4m + 21*s3n*s4m + s1m*(53*s2n - s3n -&
   44*s4n) + 41*s3m*s4n - 192*tt + 96*snm*tt) + 2*ss*tt**2*(s2n*(-37*s3m + 25*s4m) +&
   s1m*(30*s2n + 31*s3n - 39*s4n) + 8*(-(s3m*s3n) + s3n*s4m + 2*s3m*s4n - 26*tt +&
   13*snm*tt))))))/((4*me2 - ss)*(me2**2 + mm2**2 - 2*mm2*tt - 2*me2*(mm2 + tt) +&
   tt*(ss + tt))**2) - (scalarc0sm*ss*(8*me2**6*(-2 + snm)*(4*mm2 - ss) + 8*mm2**6*(-2&
   + snm)*(ss - 4*tt) - mm2**5*(4*(2 + 3*snm)*ss**2 + ss*(s2n*(-5*s3m + 3*s4m) +&
   s1m*(8*s2n + 5*s3n - 3*s4n) + 16*(-2 + snm)*tt) - 8*tt*(s2n*(-3*s3m + s4m) +&
   s1m*(4*s2n + 3*s3n - s4n) + 20*(-2 + snm)*tt)) + ss*tt**2*(4*ss**4 -&
   3*ss**2*tt*(2*s3n*s4m + s2n*(-3*s3m + s4m) + s1m*(2*s2n + s3n - 3*s4n) + 2*s3m*s4n +&
   6*(-2 + snm)*tt) + tt**3*(5*s2n*s3m - 5*s1m*s3n - 3*s2n*s4m + 3*s1m*s4n + 16*tt -&
   8*snm*tt) + ss**3*(-3*s1m*s2n + 3*s2n*s3m - 3*s3n*s4m + 3*s1m*s4n - 3*s3m*s4n +&
   16*tt - 6*snm*tt) - ss*tt**2*(-9*s2n*s3m - 2*s3m*s3n + 6*s2n*s4m + 3*s3n*s4m +&
   s1m*(3*s2n + 8*s3n - 9*s4n) + 3*s3m*s4n - 40*tt + 20*snm*tt)) + mm2**4*(2*(2 +&
   snm)*ss**3 - 32*tt**2*(s2n*(-3*s3m + s4m) + s1m*(3*s2n + 3*s3n - s4n) + 10*(-2 +&
   snm)*tt) - ss*tt*(8*s3n*s4m + 3*s2n*(s3m + s4m) + s1m*(8*s2n + 5*s3n - 11*s4n) +&
   8*s3m*(-2*s3n + s4n) + 88*(-2 + snm)*tt) + ss**2*(-2*s3m*s3n + 3*s3n*s4m +&
   s2n*(-14*s3m + s4m) + s1m*(15*s2n + 13*s3n - 4*s4n) + 3*s3m*s4n + 8*tt + 44*snm*tt))&
   + me2**5*(-96*mm2**2*(-2 + snm) + 8*mm2*(3*s2n*s3m - 3*s1m*s3n - s2n*s4m + s1m*s4n +&
   4*(-2 + snm)*ss + 48*tt - 24*snm*tt) + ss*(-5*s2n*s3m + 5*s1m*s3n + 3*s2n*s4m -&
   3*s1m*s4n - 96*tt + 48*snm*tt)) - mm2**3*(-16*tt**3*(s2n*(-9*s3m + 3*s4m) +&
   s1m*(6*s2n + 9*s3n - 3*s4n) + 20*(-2 + snm)*tt) + ss**2*tt*(-12*s3m*s3n - 6*s3n*s4m&
   + 3*s2n*(-5*s3m + s4m) - 6*s3m*s4n + 3*s1m*(2*s2n + 11*s3n + s4n) + 80*tt +&
   8*snm*tt) + ss**3*(-3*s2n*s3m - 10*s3m*s3n + s3n*s4m + s1m*(3*s2n + 12*s3n - s4n) +&
   s3m*s4n + 32*tt + 20*snm*tt) - 2*ss*tt**2*(s2n*(-25*s3m + 19*s4m) + s1m*(44*s2n +&
   37*s3n - 31*s4n) + 12*(s3n*s4m + s3m*(-2*s3n + s4n) + 12*(-2 + snm)*tt))) +&
   mm2*tt*(8*tt**4*(s2n*(-3*s3m + s4m) + s1m*(3*s3n - s4n) + 4*(-2 + snm)*tt) -&
   ss**4*(-(s2n*s3m) - 2*s3m*s3n + s3n*s4m + s1m*(s2n + 2*s3n - s4n) + s3m*s4n + 24*tt&
   + 4*snm*tt) + ss**3*tt*(-17*s2n*s3m + 6*s3m*s3n + 2*s2n*s4m + 17*s3n*s4m +&
   s1m*(23*s2n - 6*s3n - 19*s4n) + 17*s3m*s4n - 96*tt + 36*snm*tt) +&
   ss**2*tt**2*(-43*s2n*s3m - 12*s3m*s3n + 23*s2n*s4m + 26*s3n*s4m + s1m*(38*s2n +&
   29*s3n - 49*s4n) + 26*s3m*s4n - 232*tt + 116*snm*tt) + ss*tt**3*(s2n*(-51*s3m +&
   29*s4m) + s1m*(16*s2n + 59*s3n - 37*s4n) + 8*(s3n*s4m + s3m*(-2*s3n + s4n) + 14*(-2&
   + snm)*tt))) + mm2**2*(-32*tt**4*(s2n*(-3*s3m + s4m) + s1m*(s2n + 3*s3n - s4n) +&
   5*(-2 + snm)*tt) - 2*ss*tt**3*(-47*s2n*s3m - 24*s3m*s3n + 29*s2n*s4m + 12*s3n*s4m +&
   s1m*(44*s2n + 59*s3n - 41*s4n) + 12*s3m*s4n - 296*tt + 148*snm*tt) +&
   2*ss**4*(s1m*s3n - s3m*s3n + (4 + snm)*tt) + ss**3*tt*(s2n*(-7*s3m + s4m) +&
   s1m*(6*s2n + 13*s3n - 3*s4n) + 2*(-4*s3m*s3n + s3n*s4m + s3m*s4n + 28*tt +&
   8*snm*tt)) - ss**2*tt**2*(s2n*(-33*s3m + 15*s4m) + s1m*(60*s2n + s3n - 47*s4n) +&
   8*(4*s3n*s4m + 4*s3m*s4n - 34*tt + 15*snm*tt))) + me2**4*(64*mm2**3*(-2 + snm) -&
   8*mm2**2*(6*s2n*s3m - 6*s2n*s4m + 4*s3n*s4m - 4*s3m*s4n + s1m*(4*s2n - 6*s3n +&
   6*s4n) + 5*(-2 + snm)*ss + 56*tt - 28*snm*tt) + ss*(-2*(-2 + snm)*ss**2 +&
   ss*(4*s2n*s3m + 2*s3m*s3n + s2n*s4m - 3*s3n*s4m - 3*s3m*s4n + s1m*(-3*s2n - 3*s3n +&
   2*s4n) + 40*tt - 20*snm*tt) - 5*tt*(-5*s2n*s3m + 5*s1m*s3n + 3*s2n*s4m - 3*s1m*s4n -&
   48*tt + 24*snm*tt)) + mm2*(4*(-2 + snm)*ss**2 + 40*tt*(s2n*(-3*s3m + s4m) +&
   s1m*(3*s3n - s4n) + 12*(-2 + snm)*tt) + ss*(s2n*(5*s3m - 19*s4m) + s1m*(16*s2n +&
   3*s3n + 11*s4n) - 16*(s3m*s3n - s3n*s4m - 2*tt + snm*tt)))) + me2**2*(-96*mm2**5*(-2&
   + snm) + 8*mm2**4*(5*(-2 + snm)*ss + 2*(3*s2n*s3m + 5*s2n*s4m + 2*s3n*s4m -&
   2*s3m*s4n + s1m*(-6*s2n + 5*s3n + 3*s4n) - 8*tt + 4*snm*tt)) + 2*mm2**2*(12*ss**3 -&
   24*tt**2*(2*s3n*s4m - s2n*(3*s3m + s4m) - 2*s3m*s4n + s1m*(4*s2n + 3*s3n + s4n) +&
   12*(-2 + snm)*tt) + ss**2*(9*s2n*s3m + 3*s2n*s4m + 4*s3n*s4m - 4*s3m*s4n +&
   s1m*(-6*s2n - 5*s3n + s4n) + 8*tt - 12*snm*tt) + ss*tt*(-43*s2n*s3m + 40*s3m*s3n +&
   9*s2n*s4m - 28*s3n*s4m + s1m*(28*s2n - 41*s3n - 53*s4n) + 52*s3m*s4n - 16*tt +&
   8*snm*tt)) + ss*(-2*ss**3*(s1m*s3n - s3m*s3n + (-4 + snm)*tt) -&
   10*tt**3*(s2n*(-5*s3m + 3*s4m) + s1m*(5*s3n - 3*s4n) + 12*(-2 + snm)*tt) +&
   ss**2*tt*(7*s2n*s3m + 12*s3m*s3n + s2n*s4m - 8*s3n*s4m - 8*s3m*s4n + s1m*(-4*s2n -&
   11*s3n + 7*s4n) + 56*tt - 28*snm*tt) - 3*ss*tt**2*(6*s1m*s2n - 13*s2n*s3m -&
   4*s3m*s3n + 5*s2n*s4m + 6*s3n*s4m + 11*s1m*(s3n - s4n) + 6*s3m*s4n - 80*tt +&
   40*snm*tt)) + mm2*(80*tt**3*(s2n*(-3*s3m + s4m) + s1m*(3*s3n - s4n) + 6*(-2 +&
   snm)*tt) - ss**3*(-(s2n*s3m) + 10*s3m*s3n + 3*s3n*s4m + 3*s3m*s4n + s1m*(s2n -&
   3*(4*s3n + s4n)) + 4*snm*tt) + ss**2*tt*(2*s1m*s2n - 68*s3m*s3n + 13*s2n*(s3m - s4m)&
   + 30*s3n*s4m - 2*s3m*s4n + 7*s1m*(7*s3n + s4n) - 80*tt + 40*snm*tt) +&
   2*ss*tt**2*(-69*s2n*s3m - 48*s3m*s3n + 15*s2n*s4m + 36*s3n*s4m + s1m*(48*s2n +&
   93*s3n - 39*s4n) + 12*s3m*s4n - 544*tt + 272*snm*tt)) - 2*mm2**3*(12*(2 + snm)*ss**2&
   - 8*tt*(-6*s1m*s2n + 4*s3n*s4m + 3*s2n*(s3m + s4m) - 4*s3m*s4n + 5*s1m*(s3n + s4n) +&
   8*(-2 + snm)*tt) + ss*(s2n*(37*s3m + 21*s4m) + s1m*(-44*s2n + 47*s3n + 23*s4n) -&
   8*(5*s3m*s3n - s3n*s4m + 4*s3m*s4n - 4*tt + 2*snm*tt)))) + me2*(32*mm2**6*(-2 + snm)&
   - 8*mm2**5*(-4*s1m*s2n + 3*s2n*s3m + 3*s2n*s4m + 4*s3n*s4m - 4*s3m*s4n + 5*s1m*(s3n&
   + s4n) + 4*(-2 + snm)*ss - 16*tt + 8*snm*tt) - 2*mm2**3*(2*(4 + snm)*ss**3 +&
   ss**2*(-7*s2n*s3m + 14*s3m*s3n + 2*s2n*s4m + 5*s3n*s4m + s1m*(9*s2n - 8*s3n - 3*s4n)&
   + s3m*s4n + 16*snm*tt) + 16*tt**2*(-3*s2n*s3m + 3*s3n*s4m - 3*s3m*s4n + s1m*(3*s2n +&
   5*s3n + 2*s4n) - 32*tt + 16*snm*tt) + 2*ss*tt*(25*s2n*s3m - 24*s3m*s3n + 7*s2n*s4m +&
   18*s3n*s4m - 26*s3m*s4n + s1m*(-36*s2n + 35*s3n + 29*s4n) - 32*tt + 16*snm*tt)) +&
   mm2**4*(32*(1 + snm)*ss**2 + 16*tt*(3*s2n*s3m + s2n*s4m + 6*s3n*s4m - 6*s3m*s4n +&
   s1m*(-4*s2n + 5*s3n + 7*s4n) - 24*tt + 12*snm*tt) + ss*(27*s2n*s3m - 48*s3m*s3n +&
   19*s2n*s4m + 16*s3n*s4m - 32*s3m*s4n + s1m*(-24*s2n + 61*s3n + 21*s4n) - 96*tt +&
   48*snm*tt)) + ss*tt*(tt**3*(-5*s2n*(5*s3m - 3*s4m) + 5*s1m*(5*s3n - 3*s4n) + 48*(-2&
   + snm)*tt) + ss**3*(-(s2n*s3m) - 2*s3m*s3n + s3n*s4m + s1m*(s2n + 2*s3n - s4n) +&
   s3m*s4n - 16*tt + 4*snm*tt) + ss**2*tt*(-17*s2n*s3m - 6*s3m*s3n + 2*s2n*s4m +&
   13*s3n*s4m + s1m*(11*s2n + 10*s3n - 15*s4n) + 13*s3m*s4n - 80*tt + 40*snm*tt) +&
   ss*tt**2*(-31*s2n*s3m - 8*s3m*s3n + 17*s2n*s4m + 12*s3n*s4m + s1m*(12*s2n + 27*s3n -&
   29*s4n) + 12*s3m*s4n - 160*tt + 80*snm*tt)) - 2*mm2*tt*(8*ss**4 +&
   4*tt**3*(5*s2n*(-3*s3m + s4m) + 5*s1m*(3*s3n - s4n) + 24*(-2 + snm)*tt) -&
   ss**3*(6*s3m*s3n + s3n*s4m - s2n*(4*s3m + s4m) + s1m*(3*s2n - 7*s3n - 4*s4n) +&
   5*s3m*s4n + 8*tt + 6*snm*tt) + ss**2*tt*(-26*s2n*s3m - 26*s3m*s3n + 3*s2n*s4m +&
   29*s3n*s4m + s1m*(29*s2n + 31*s3n - 24*s4n) + 17*s3m*s4n - 192*tt + 96*snm*tt) +&
   2*ss*tt**2*(-37*s2n*s3m - 16*s3m*s3n + 17*s2n*s4m + 10*s3n*s4m + s1m*(16*s2n +&
   45*s3n - 25*s4n) + 6*s3m*s4n - 208*tt + 104*snm*tt)) +&
   mm2**2*(2*ss*tt**2*(48*s3n*s4m - s2n*(25*s3m + s4m) - 8*s3m*(7*s3n + 3*s4n) +&
   s1m*(52*s2n + 85*s3n + 5*s4n) + 208*(-2 + snm)*tt) + ss**3*(-5*s2n*s3m + 6*s3m*s3n +&
   3*s3n*s4m + s1m*(5*s2n - 4*s3n - 3*s4n) + 3*s3m*s4n + 48*tt + 16*snm*tt) +&
   16*tt**3*(2*s3n*s4m + 3*s2n*(-5*s3m + s4m) + s1m*(8*s2n + 15*s3n - 3*s4n) -&
   2*s3m*s4n - 68*tt + 34*snm*tt) + ss**2*tt*(s2n*(57*s3m + 13*s4m) + s1m*(-72*s2n +&
   79*s3n + 59*s4n) - 16*(4*s3m*s3n + 4*s3m*s4n - 4*tt + 3*snm*tt)))) +&
   me2**3*(32*mm2**3*(-3*s2n*s4m + s3n*s4m - s3m*s4n + s1m*(3*s2n - 2*s3n + s4n)) +&
   64*mm2**4*(-2 + snm) + ss*(10*tt**2*(s2n*(-5*s3m + 3*s4m) + s1m*(5*s3n - 3*s4n) +&
   16*(-2 + snm)*tt) + ss*tt*(12*s3n*s4m + 3*s2n*(-7*s3m + s4m) + s1m*(12*s2n + 17*s3n&
   - 15*s4n) + 4*s3m*(-2*s3n + 3*s4n) + 80*(-2 + snm)*tt) + ss**2*(s2n*s3m - 6*s3m*s3n&
   + s3n*s4m + s3m*s4n - s1m*(s2n - 4*s3n + s4n) - 16*tt + 8*snm*tt)) +&
   2*mm2**2*(16*ss**2 + 8*tt*(3*s2n*s3m - 7*s2n*s4m + 6*s3n*s4m - 6*s3m*s4n +&
   s1m*(8*s2n - 3*s3n + 7*s4n) - 8*tt + 4*snm*tt) + ss*(21*s2n*(s3m + s4m) +&
   s1m*(-36*s2n + 15*s3n + 7*s4n) - 8*(s3n*s4m + s3m*(s3n + 2*s4n) + 6*(-2 + snm)*tt)))&
   + 2*mm2*(2*(-4 + snm)*ss**3 - 40*tt**2*(s2n*(-3*s3m + s4m) + s1m*(3*s3n - s4n) +&
   8*(-2 + snm)*tt) + ss**2*(-11*s2n*s3m + 14*s3m*s3n - 2*s2n*s4m + s3n*s4m +&
   s1m*(9*s2n - 8*s3n - 3*s4n) + 5*s3m*s4n - 32*tt + 16*snm*tt) - 2*ss*tt*(-(s2n*(9*s3m&
   + 7*s4m)) + s1m*(16*s2n + 17*s3n - s4n) + 2*(-8*s3m*s3n + 7*s3n*s4m + s3m*s4n -&
   64*tt + 32*snm*tt))))))/((4*mm2 - ss)*(me2**2 + mm2**2 - 2*mm2*tt - 2*me2*(mm2 + tt)&
   + tt*(ss + tt))**2) + (discbtem*(2*logse*tt*(-8*me2**7*(-2 + snm) +&
   me2**6*(-5*s2n*s3m + 5*s1m*s3n + 3*s2n*s4m - 3*s1m*s4n + 8*mm2*(-2 + snm) - 112*tt +&
   56*snm*tt) + me2**5*(24*mm2**2*(-2 + snm) - 2*(-2 + snm)*ss**2 + 2*mm2*(s2n*(5*s3m -&
   3*s4m) + s1m*(4*s2n - 5*s3n + 3*s4n) + 8*(-2 + snm)*tt) - 6*tt*(s2n*(-5*s3m + 3*s4m)&
   + s1m*(5*s3n - 3*s4n) + 28*(-2 + snm)*tt) + ss*(2*s3m*s3n - 3*s3n*s4m + s2n*(4*s3m +&
   s4m) - 3*s3m*s4n + s1m*(-3*s2n - 3*s3n + 2*s4n) + 40*tt - 20*snm*tt)) +&
   me2**4*(-24*mm2**3*(-2 + snm) + mm2**2*(s2n*(5*s3m - 3*s4m) + s1m*(-24*s2n - 5*s3n +&
   3*s4n) - 56*(-2 + snm)*tt) + 5*tt**2*(s2n*(-15*s3m + 9*s4m) + 3*s1m*(5*s3n - 3*s4n)&
   + 56*(-2 + snm)*tt) + ss**2*(s2n*s3m - 6*s3m*s3n + s3n*s4m + s3m*s4n - s1m*(s2n -&
   4*s3n + s4n) - 20*tt + 10*snm*tt) + ss*tt*(-25*s2n*s3m - 10*s3m*s3n + 2*s2n*s4m +&
   15*s3n*s4m + s1m*(15*s2n + 20*s3n - 17*s4n) + 15*s3m*s4n - 200*tt + 100*snm*tt) +&
   mm2*(6*(-2 + snm)*ss**2 - 2*tt*(s2n*(5*s3m - 3*s4m) + s1m*(20*s2n - 5*s3n + 3*s4n) +&
   100*(-2 + snm)*tt) + ss*(-14*s2n*s3m - 6*s3m*s3n - 5*s2n*s4m + 5*s3n*s4m +&
   s1m*(9*s2n + 7*s3n - 8*s4n) + 13*s3m*s4n + 8*tt - 4*snm*tt))) +&
   me2**3*(-24*mm2**4*(-2 + snm) - 2*ss**3*(s1m*s3n - s3m*s3n + (-4 + snm)*tt) +&
   4*mm2**3*(s2n*(-5*s3m + 3*s4m) + s1m*(4*s2n + 5*s3n - 3*s4n) - 8*(-2 + snm)*tt) -&
   20*tt**3*(s2n*(-5*s3m + 3*s4m) + s1m*(5*s3n - 3*s4n) + 14*(-2 + snm)*tt) -&
   2*ss*tt**2*(-30*s2n*s3m - 10*s3m*s3n + 9*s2n*s4m + 15*s3n*s4m + s1m*(15*s2n + 25*s3n&
   - 24*s4n) + 15*s3m*s4n - 200*tt + 100*snm*tt) - 2*mm2**2*(2*(-2 + snm)*ss**2 +&
   2*tt*(s2n*(5*s3m - 3*s4m) + s1m*(-8*s2n - 5*s3n + 3*s4n) + 36*(-2 + snm)*tt) +&
   ss*(-8*s2n*s3m - 2*s3m*s3n - 5*s2n*s4m - 3*s3n*s4m + s1m*(3*s2n + s3n - 4*s4n) +&
   9*s3m*s4n + 24*tt - 12*snm*tt)) + ss**2*tt*(s2n*(6*s3m + s4m) + s1m*(-3*s2n - 15*s3n&
   + 8*s4n) + 9*(2*s3m*s3n - s3n*s4m - s3m*s4n + 8*tt - 4*snm*tt)) -&
   4*mm2*(tt**2*(3*s2n*(5*s3m - 3*s4m) + s1m*(-20*s2n - 15*s3n + 9*s4n) - 120*(-2 +&
   snm)*tt) + ss**2*(s3n*s4m + s1m*(s3n - s4n) + s3m*(-2*s3n + s4n) - 4*tt + 2*snm*tt)&
   - ss*tt*(4*s2n*s3m + 2*s3m*s3n + 3*s2n*s4m - 2*s3n*s4m + 3*s1m*s4n - 4*s3m*s4n -&
   56*tt + 28*snm*tt))) - (mm2 - tt)*(8*mm2**6*(-2 + snm) + mm2**5*(s2n*(5*s3m - 3*s4m)&
   + s1m*(-8*s2n - 5*s3n + 3*s4n) - 48*(-2 + snm)*tt) + tt**2*(-4*ss**4 +&
   3*ss**2*tt*(2*s3n*s4m + s2n*(-3*s3m + s4m) + s1m*(2*s2n + s3n - 3*s4n) + 2*s3m*s4n +&
   6*(-2 + snm)*tt) + tt**3*(s2n*(-5*s3m + 3*s4m) + s1m*(5*s3n - 3*s4n) + 8*(-2 +&
   snm)*tt) + ss**3*(-3*s2n*s3m + 3*s3n*s4m + 3*s1m*(s2n - s4n) + 3*s3m*s4n - 16*tt +&
   6*snm*tt) + ss*tt**2*(-9*s2n*s3m - 2*s3m*s3n + 6*s2n*s4m + 3*s3n*s4m + s1m*(3*s2n +&
   8*s3n - 9*s4n) + 3*s3m*s4n - 40*tt + 20*snm*tt)) + mm2**4*(2*(-2 + snm)*ss**2 +&
   tt*(-5*s2n*(5*s3m - 3*s4m) + s1m*(32*s2n + 25*s3n - 15*s4n) + 120*(-2 + snm)*tt) +&
   ss*(-2*s3m*s3n + 3*s3n*s4m + s2n*(-2*s3m + s4m) + s1m*(3*s2n + s3n - 4*s4n) +&
   3*s3m*s4n - 40*tt + 20*snm*tt)) + mm2**2*(2*ss**3*(s1m*s3n - s3m*s3n + (-4 +&
   snm)*tt) + 2*tt**3*(5*s2n*(-5*s3m + 3*s4m) + s1m*(16*s2n + 25*s3n - 15*s4n) + 60*(-2&
   + snm)*tt) + ss**2*tt*(-13*s2n*s3m + 4*s3m*s3n + s2n*s4m + 8*s3n*s4m + s1m*(12*s2n +&
   s3n - 9*s4n) + 8*s3m*s4n - 56*tt + 28*snm*tt) + 3*ss*tt**2*(-11*s2n*s3m - 4*s3m*s3n&
   + 7*s2n*s4m + 6*s3n*s4m + s1m*(14*s2n + 9*s3n - 13*s4n) + 6*s3m*s4n - 80*tt +&
   40*snm*tt)) - mm2*tt*(tt**3*(-5*s2n*(5*s3m - 3*s4m) + s1m*(8*s2n + 25*s3n - 15*s4n)&
   + 48*(-2 + snm)*tt) + ss**3*(-(s2n*s3m) - 2*s3m*s3n + s3n*s4m + s1m*(s2n + 2*s3n -&
   s4n) + s3m*s4n - 16*tt + 4*snm*tt) + ss**2*tt*(-19*s2n*s3m + 2*s3m*s3n + 4*s2n*s4m +&
   13*s3n*s4m + s1m*(19*s2n + 4*s3n - 17*s4n) + 13*s3m*s4n - 80*tt + 40*snm*tt) +&
   ss*tt**2*(-29*s2n*s3m - 8*s3m*s3n + 19*s2n*s4m + 12*s3n*s4m + s1m*(24*s2n + 25*s3n -&
   31*s4n) + 12*s3m*s4n - 160*tt + 80*snm*tt)) - mm2**3*(2*tt**2*(5*s2n*(-5*s3m +&
   3*s4m) + s1m*(24*s2n + 25*s3n - 15*s4n) + 80*(-2 + snm)*tt) + ss**2*(3*s1m*s2n -&
   3*s2n*s3m + 2*s3m*s3n + s3n*s4m - s1m*s4n + s3m*s4n - 16*tt + 8*snm*tt) +&
   ss*tt*(-15*s2n*s3m - 8*s3m*s3n + 9*s2n*s4m + 12*s3n*s4m + s1m*(24*s2n + 11*s3n -&
   21*s4n) + 12*s3m*s4n - 160*tt + 80*snm*tt))) + me2**2*(24*mm2**5*(-2 + snm) +&
   mm2**4*(s2n*(5*s3m - 3*s4m) + s1m*(16*s2n - 5*s3n + 3*s4n) - 56*(-2 + snm)*tt) -&
   2*mm2**3*(2*(-2 + snm)*ss**2 + 2*tt*(s2n*(5*s3m - 3*s4m) + s1m*(-4*s2n - 5*s3n +&
   3*s4n) + 36*(-2 + snm)*tt) + ss*(2*s2n*s3m - 2*s3m*s3n + 5*s2n*s4m + 9*s3n*s4m +&
   s1m*(3*s2n + 3*s3n - 2*s4n) - 3*s3m*s4n + 24*tt - 12*snm*tt)) +&
   2*mm2**2*(tt**2*(-5*s2n*(5*s3m - 3*s4m) + s1m*(24*s2n + 25*s3n - 15*s4n) + 264*(-2 +&
   snm)*tt) + ss**2*(-3*s2n*s3m + 2*s3m*s3n + 3*s3n*s4m + s1m*(3*s2n - 2*s3n - 3*s4n) +&
   3*s3m*s4n + 4*tt - 2*snm*tt) + ss*tt*(13*s2n*s3m + 2*s3m*s3n - 10*s2n*s4m + s3n*s4m&
   - 7*s3m*s4n + s1m*(-9*s2n - 16*s3n + 9*s4n) - 88*tt + 44*snm*tt)) +&
   tt*(3*tt**3*(-5*s2n*(5*s3m - 3*s4m) + 5*s1m*(5*s3n - 3*s4n) + 56*(-2 + snm)*tt) +&
   ss**3*(-(s2n*s3m) - 4*s3m*s3n + s3n*s4m + s1m*(s2n + 4*s3n - s4n) + s3m*s4n - 24*tt&
   + 6*snm*tt) + ss**2*tt*(-18*s3m*s3n + 21*s3n*s4m + s2n*(-24*s3m + s4m) + s1m*(15*s2n&
   + 21*s3n - 22*s4n) + 21*s3m*s4n - 136*tt + 68*snm*tt) + 2*ss*tt**2*(-35*s2n*s3m -&
   10*s3m*s3n + 16*s2n*s4m + 15*s3n*s4m + s1m*(15*s2n + 30*s3n - 31*s4n) + 15*s3m*s4n -&
   200*tt + 100*snm*tt)) + mm2*(2*ss**3*(s1m*s3n - s3m*s3n + (-4 + snm)*tt) -&
   4*tt**3*(-7*s2n*(5*s3m - 3*s4m) + s1m*(20*s2n + 35*s3n - 21*s4n) + 130*(-2 +&
   snm)*tt) + ss**2*tt*(6*s3m*s3n + 5*s3n*s4m - 3*s2n*(4*s3m + s4m) + s1m*(7*s2n -&
   7*s3n - 10*s4n) + 13*s3m*s4n + 56*tt - 28*snm*tt) - 2*ss*tt**2*(-24*s2n*s3m -&
   6*s3m*s3n + 17*s2n*s4m + 7*s3n*s4m + s1m*(27*s2n + 27*s3n - 20*s4n) + 11*s3m*s4n -&
   312*tt + 156*snm*tt))) + me2*(8*mm2**6*(-2 + snm) + 2*mm2**5*(s2n*(5*s3m - 3*s4m) +&
   s1m*(-12*s2n - 5*s3n + 3*s4n) + 8*(-2 + snm)*tt) + mm2**4*(6*(-2 + snm)*ss**2 +&
   2*tt*(s2n*(-5*s3m + 3*s4m) + s1m*(16*s2n + 5*s3n - 3*s4n) - 100*(-2 + snm)*tt) +&
   ss*(9*s1m*s2n - 4*s2n*s3m - 6*s3m*s3n + 5*s2n*s4m + 13*s3n*s4m + 5*s1m*(s3n - 2*s4n)&
   + 5*s3m*s4n + 8*tt - 4*snm*tt)) - 4*mm2**3*(-3*tt**2*(s2n*(-5*s3m + 3*s4m) +&
   s1m*(4*s2n + 5*s3n - 3*s4n) + 40*(-2 + snm)*tt) + ss*tt*(6*s1m*s2n - 2*s3m*s3n +&
   s2n*s4m + 6*s3n*s4m - 3*s1m*s4n + 56*tt - 28*snm*tt) + ss**2*(-2*s2n*s3m + 2*s3m*s3n&
   + s3n*s4m + s1m*(2*s2n - s3n - s4n) + s3m*s4n - 4*tt + 2*snm*tt)) +&
   2*mm2*tt*(tt**3*(s2n*(-55*s3m + 33*s4m) + s1m*(20*s2n + 55*s3n - 33*s4n) + 136*(-2 +&
   snm)*tt) + ss**3*(s2n*s3m - s3n*s4m - s3m*s4n + s1m*(-s2n + s4n) - 8*tt + 2*snm*tt)&
   + ss**2*tt*(-12*s2n*s3m - 6*s3m*s3n + 5*s2n*s4m + 5*s3n*s4m + s1m*(9*s2n + 13*s3n -&
   10*s4n) + 9*s3m*s4n - 88*tt + 44*snm*tt) + 2*ss*tt**2*(-22*s2n*s3m - 6*s3m*s3n +&
   13*s2n*s4m + 8*s3n*s4m + s1m*(18*s2n + 20*s3n - 21*s4n) + 10*s3m*s4n - 152*tt +&
   76*snm*tt)) - tt**2*(-4*ss**4 + 2*ss**3*(-2*s2n*s3m - s3m*s3n + 2*s3n*s4m +&
   s1m*(2*s2n + s3n - 2*s4n) + 2*s3m*s4n - 16*tt + 5*snm*tt) + 2*tt**3*(-15*s2n*s3m +&
   15*s1m*s3n + 9*s2n*s4m - 9*s1m*s4n - 56*tt + 28*snm*tt) + ss**2*tt*(-26*s2n*s3m -&
   6*s3m*s3n + 5*s2n*s4m + 19*s3n*s4m + s1m*(17*s2n + 13*s3n - 24*s4n) + 19*s3m*s4n -&
   116*tt + 58*snm*tt) + ss*tt**2*(-40*s2n*s3m - 10*s3m*s3n + 23*s2n*s4m + 15*s3n*s4m +&
   s1m*(15*s2n + 35*s3n - 38*s4n) + 15*s3m*s4n - 200*tt + 100*snm*tt)) +&
   mm2**2*(2*ss**3*(s1m*s3n - s3m*s3n + (-4 + snm)*tt) - 4*tt**3*(-7*s2n*(5*s3m -&
   3*s4m) + s1m*(24*s2n + 35*s3n - 21*s4n) + 130*(-2 + snm)*tt) + ss**2*tt*(-10*s2n*s3m&
   + 14*s3m*s3n + 3*s2n*s4m + 13*s3n*s4m + s1m*(11*s2n - 9*s3n - 8*s4n) + 5*s3m*s4n +&
   56*tt - 28*snm*tt) - 2*ss*tt**2*(-26*s2n*s3m - 6*s3m*s3n + 15*s2n*s4m + 3*s3n*s4m +&
   s1m*(21*s2n + 25*s3n - 22*s4n) + 15*s3m*s4n - 312*tt + 156*snm*tt)))) + 4*ss*(me2**2&
   + mm2**2 - 2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss + tt))*(-(me2**5*(-2 + snm)) +&
   me2**4*(3*mm2*(-2 + snm) + 2*ss + 5*(-2 + snm)*tt) - me2**3*(2*mm2**2*(-2 + snm) +&
   mm2*(-(s2n*s3m) + s1m*(s2n + s3n) + 8*ss - 8*tt + 4*snm*tt) + tt*(-(s2n*s3m) +&
   s1m*s3n + 2*s2n*s4m - 2*s3n*s4m - 2*s1m*s4n + 2*s3m*s4n - 20*tt + 10*snm*tt) +&
   ss*(s1m*s3n - s3m*s3n + (2 + snm)*tt)) - (mm2 - tt)*(mm2**4*(-2 + snm) -&
   mm2**3*(-(s2n*s3m) + s1m*(s2n + s3n) + 2*ss - 8*tt + 4*snm*tt) + tt**2*(-2*ss**2 +&
   ss*(s1m*s3n - s3m*s3n + (-4 + snm)*tt) + tt*(-(s2n*s3m) + s1m*s3n + (-2 + snm)*tt))&
   - mm2*tt*(2*ss**2 + 2*ss*(s1m*s3n - s3m*s3n + (-3 + snm)*tt) + tt*(-3*s2n*s3m +&
   s1m*(s2n + 3*s3n) + 4*(-2 + snm)*tt)) + mm2**2*(tt*(-3*s2n*s3m + s1m*(2*s2n + 3*s3n)&
   + 6*(-2 + snm)*tt) + ss*(s1m*s3n - s3m*s3n + snm*tt))) + me2**2*(-2*mm2**3*(-2 +&
   snm) + mm2**2*(-3*s2n*s3m + 3*s1m*(s2n + s3n) + 12*ss + 4*tt - 2*snm*tt) +&
   tt*(2*ss**2 + ss*(s1m*s2n - s2n*s3m + s3m*s3n - 6*tt + 3*snm*tt) + tt*(-3*s2n*s3m +&
   3*s1m*s3n + 4*s2n*s4m - 4*s3n*s4m - 4*s1m*s4n + 4*s3m*s4n - 20*tt + 10*snm*tt)) +&
   mm2*(tt*(-2*s2n*s3m + 4*s2n*s4m - 4*s3n*s4m + s1m*(s2n + 2*s3n - 4*s4n) + 4*s3m*s4n&
   + 12*tt - 6*snm*tt) + ss*(s1m*s3n - s3m*s3n + (2 + snm)*tt))) + me2*(3*mm2**4*(-2 +&
   snm) - mm2**3*(-3*s2n*s3m + 3*s1m*(s2n + s3n) + 8*ss - 8*tt + 4*snm*tt) -&
   tt**2*(ss*(-(s2n*s3m) + s3m*s3n + 2*s2n*s4m - 2*s3n*s4m + s1m*(s2n - 2*s4n) +&
   2*s3m*s4n - 10*tt + 3*snm*tt) + tt*(-3*s2n*s3m + 3*s1m*s3n + 2*s2n*s4m - 2*s3n*s4m -&
   2*s1m*s4n + 2*s3m*s4n - 10*tt + 5*snm*tt)) + mm2*tt*(-4*ss**2 + ss*(s2n*s3m +&
   2*s3m*s3n - s1m*(s2n + 3*s3n) - 4*tt + 2*snm*tt) + tt*(-3*s2n*s3m + 4*s2n*s4m -&
   4*s3n*s4m + s1m*(s2n + 3*s3n - 4*s4n) + 4*s3m*s4n - 24*tt + 12*snm*tt)) +&
   mm2**2*(tt*(-3*s2n*s3m - 2*s2n*s4m + 2*s3n*s4m - 2*s3m*s4n + s1m*(2*s2n + 3*s3n +&
   2*s4n) + 12*tt - 6*snm*tt) + ss*(s1m*s3n - s3m*s3n + (2 + snm)*tt)))) +&
   logme*ss*tt*(me2**5*(s1m*s3n - s2n*(s3m + s4m) + s1m*s4n + 2*(2 + snm)*ss + 8*tt -&
   4*snm*tt) - me2**4*(4*tt*(-(s2n*s4m) + s1m*s4n - 5*(-2 + snm)*tt) + mm2*(s1m*s3n -&
   4*s3n*s4m - 5*s2n*(s3m + s4m) + s1m*s4n + 4*s3m*s4n + 6*(2 + snm)*ss + 24*tt -&
   12*snm*tt) + ss*(s2n*s3m - 6*s3m*s3n + s3n*s4m + s3m*s4n - s1m*(s2n - 4*s3n + s4n) +&
   20*tt + 10*snm*tt)) + me2**3*(2*tt**2*(5*s2n*s3m - 5*s1m*s3n - 3*s2n*s4m + 3*s1m*s4n&
   + 40*tt - 20*snm*tt) - 2*mm2**2*(s1m*s3n + 6*s3n*s4m + 5*s2n*(s3m + s4m) + s1m*s4n -&
   6*s3m*s4n - 2*(2 + snm)*ss - 8*tt + 4*snm*tt) + ss*tt*(-14*s3m*s3n - s2n*s4m +&
   3*s3n*s4m + s1m*(-3*s2n + 11*s3n - 2*s4n) + 3*s3m*s4n + 56*tt + 12*snm*tt) +&
   2*ss**2*(s1m*s3n - s3m*s3n + (4 + snm)*tt) + 4*mm2*(ss*(-2*s3m*s3n + s3n*s4m +&
   s1m*(s3n - s4n) + s3m*s4n + 4*tt + 2*snm*tt) - tt*(-(s1m*s2n) + s2n*s3m + 2*s1m*s3n&
   + 3*s2n*s4m + s3n*s4m - s3m*s4n - 8*tt + 4*snm*tt))) + me2**2*(2*mm2**3*(s1m*s3n +&
   6*s3n*s4m + 5*s2n*(s3m + s4m) + s1m*s4n - 6*s3m*s4n + 2*(2 + snm)*ss + 8*tt -&
   4*snm*tt) + mm2**2*(4*tt*(-(s1m*s2n) + 2*s1m*s3n + 2*s2n*s4m - 2*s3n*s4m + 2*s3m*s4n&
   + 4*tt - 2*snm*tt) + ss*(6*s2n*s3m - 4*s3m*s3n - 6*s3n*s4m - 6*s3m*s4n + s1m*(-6*s2n&
   + 4*s3n + 6*s4n) + 8*tt + 4*snm*tt)) + tt*(4*tt**2*(s2n*(-5*s3m + s4m) + s1m*(5*s3n&
   - s4n) + 10*(-2 + snm)*tt) + ss*tt*(6*s3m*s3n - 3*s3n*s4m + s2n*(s3m + 2*s4m) -&
   3*s3m*s4n + s1m*(3*s2n - 4*s3n + s4n) - 88*tt + 4*snm*tt) - ss**2*(-(s2n*s3m) -&
   4*s3m*s3n + s3n*s4m + s1m*(s2n + 4*s3n - s4n) + s3m*s4n + 24*tt + 6*snm*tt)) +&
   mm2*(2*tt**2*(-2*s3n*s4m + 5*s2n*(s3m + s4m) + 2*s3m*s4n + s1m*(-6*s2n + s3n + s4n)&
   - 12*(-2 + snm)*tt) + ss*tt*(-10*s3m*s3n + s3n*s4m + 3*s2n*(2*s3m + s4m) - 7*s3m*s4n&
   + s1m*(-s2n + 11*s3n + 4*s4n) + 8*tt + 20*snm*tt) - 2*ss**2*(s1m*s3n - s3m*s3n + (4&
   + snm)*tt))) + (mm2 - tt)*(mm2**4*(-(s1m*s3n) + s2n*(s3m + s4m) - s1m*s4n + 2*(2 +&
   snm)*ss + 8*tt - 4*snm*tt) - 2*tt**2*(ss + 2*tt)*(-2*ss**2 + ss*(s1m*s3n - s3m*s3n +&
   (-4 + snm)*tt) + tt*(-(s2n*s3m) + s1m*s3n + (-2 + snm)*tt)) -&
   mm2*tt*(tt**2*(s2n*(13*s3m + s4m) - s1m*(4*s2n + 13*s3n + s4n) - 16*(-2 + snm)*tt) +&
   ss*tt*(10*s3m*s3n + s3n*s4m + s2n*(-2*s3m + s4m) + s3m*s4n - s1m*(s2n + 9*s3n +&
   2*s4n) + 48*tt - 8*snm*tt) + ss**2*(-(s2n*s3m) - 2*s3m*s3n + s3n*s4m + s1m*(s2n +&
   2*s3n - s4n) + s3m*s4n + 16*tt + 4*snm*tt)) - mm2**3*(tt*(7*s2n*s3m + 3*s2n*s4m -&
   s1m*(4*s2n + 7*s3n + 3*s4n) + 32*tt - 16*snm*tt) + ss*(3*s1m*s2n - 3*s2n*s3m +&
   2*s3m*s3n + s3n*s4m - s1m*s4n + s3m*s4n + 16*tt + 8*snm*tt)) +&
   mm2**2*(tt**2*(-8*s1m*s2n + 3*s2n*(5*s3m + s4m) - 3*s1m*(5*s3n + s4n) - 24*(-2 +&
   snm)*tt) + ss*tt*(6*s1m*s2n - 7*s2n*s3m + 8*s3m*s3n + s2n*s4m + 2*s3n*s4m +&
   2*s3m*s4n - 3*s1m*(s3n + s4n) + 40*tt + 4*snm*tt) + 2*ss**2*(s1m*s3n - s3m*s3n + (4&
   + snm)*tt))) + me2*(mm2**4*(s1m*s3n - 4*s3n*s4m - 5*s2n*(s3m + s4m) + s1m*s4n +&
   4*s3m*s4n - 6*(2 + snm)*ss - 24*tt + 12*snm*tt) + 4*mm2**3*(tt*(3*s2n*s3m - s1m*(s2n&
   + 2*s3n) + s2n*s4m + 3*s3n*s4m - 3*s3m*s4n + 8*tt - 4*snm*tt) + ss*(-2*s2n*s3m +&
   2*s3m*s3n + s3n*s4m + s1m*(2*s2n - s3n - s4n) + s3m*s4n + 4*tt + 2*snm*tt)) +&
   tt**2*(4*ss**3 + tt**2*(15*s2n*s3m - 15*s1m*s3n - s2n*s4m + s1m*s4n + 40*tt -&
   20*snm*tt) + ss*tt*(2*s2n*s3m + 6*s3m*s3n - s1m*(s2n + 9*s3n) - s2n*s4m + s3n*s4m +&
   s3m*s4n + 68*tt - 14*snm*tt) + ss**2*(-(s2n*s3m) + s3n*s4m + s1m*(s2n - s4n) +&
   s3m*s4n + 32*tt + 2*snm*tt)) + 2*mm2*tt*(ss*tt*(2*s3m*s3n + s2n*(s3m - 2*s4m) +&
   s3n*s4m - 3*s3m*s4n + s1m*(s2n - 4*s3n + s4n) - 40*tt - 4*snm*tt) +&
   ss**2*(-(s2n*s3m) + s3n*s4m + s1m*(s2n - s4n) + s3m*s4n - 8*tt - 2*snm*tt) +&
   2*tt**2*(3*s1m*(s2n + 2*s3n) + s3n*s4m - s2n*(7*s3m + s4m) - s3m*s4n - 24*tt +&
   12*snm*tt)) + mm2**2*(ss*tt*(4*s2n*s3m - 18*s3m*s3n - 3*s2n*s4m - 7*s3n*s4m +&
   s3m*s4n + s1m*(-5*s2n + 13*s3n + 2*s4n) + 8*tt + 20*snm*tt) - 2*ss**2*(s1m*s3n -&
   s3m*s3n + (4 + snm)*tt) - 2*tt**2*(s1m*(4*s2n + s3n + s4n) - 3*(-2*s3n*s4m +&
   s2n*(s3m + s4m) + 2*s3m*s4n + 8*tt - 4*snm*tt)))))))/((me2**2 + (mm2 - tt)**2 -&
   2*me2*(mm2 + tt))*(me2**2 + mm2**2 - 2*mm2*tt - 2*me2*(mm2 + tt) + tt*(ss +&
   tt))**2)))/ss**2 +asym_em




  END FUNCTION


  FUNCTION PEPE2MMG_EE(p1,pol1,p2,pol2,p3,p4,p5)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),pol1(4),pol2(4)
  real (kind=prec) :: ss,tt, me2, mm2, s2n, s3n, s4n, s1m, s3m, s4m, snm
  real (kind=prec) :: me1, mm1, e
  real (kind=prec) :: pepe2mmg_ee
  real (kind=prec) :: s15, s35, s25
  real (kind=prec) :: m33, m34, m44

  ss = sq(p1+p2); tt = sq(p1-p3); s15 = s(p1,p5); s35 = s(p3,p5); s25 = s(p2,p5)
  me2=sq(p1); mm2=sq(p3); me1 = sqrt(me2); mm1 = sqrt(mm2)
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)
  e = sqrt(4*pi*alpha)


  m33 = s3m*((-16*e**6*me2*s2n)/(s15**2*(s15 + s25 - ss)) + (16*e**6*me2*s3n*(-2*mm2 + s15 + s25&
   - ss))/(s15**2*(s15 + s25 - ss)**2) + (16*e**6*me2*s4n*(2*me2 + 2*s15 + s25 - 2*s35&
   - ss - 2*tt))/(s15**2*(s15 + s25 - ss)**2)) + s4m*((-16*e**6*me2*s2n)/(s15**2*(s15 +&
   s25 - ss)) + (16*e**6*me2*s4n*(-2*mm2 + s15 + s25 - ss))/(s15**2*(s15 + s25 -&
   ss)**2) - (16*e**6*me2*s3n*(2*me2 + 4*mm2 + 2*s15 + s25 - 2*s35 - ss -&
   2*tt))/(s15**2*(s15 + s25 - ss)**2)) + (32*e**6*me2*snm*(me2**2 + 2*me2*mm2 + mm2**2&
   + me2*s15 + mm2*s15 + s15**2 + s15*s25 - 2*me2*s35 - 2*mm2*s35 - 2*s15*s35 - s25*s35&
   + s35**2 - s15*ss + s35*ss - 2*me2*tt - 2*mm2*tt - 2*s15*tt - s25*tt + 2*s35*tt +&
   ss*tt + tt**2))/(s15**2*(s15 + s25 - ss)**2) - (16*e**6*(4*me2**3 + 8*me2**2*mm2 +&
   4*me2*mm2**2 + 4*me2**2*s15 + 4*me2*mm2*s15 + 7*me2*s15**2 + mm2*s15**2 + s15**3 +&
   9*me2*s15*s25 - mm2*s15*s25 + 2*s15**2*s25 + 2*me2*s25**2 + s15*s25**2 -&
   8*me2**2*s35 - 8*me2*mm2*s35 - 10*me2*s15*s35 - 2*mm2*s15*s35 - 3*s15**2*s35 -&
   4*me2*s25*s35 - 2*s15*s25*s35 + 4*me2*s35**2 + 2*s15*s35**2 - 8*me2*s15*ss -&
   s15**2*ss - 4*me2*s25*ss - s15*s25*ss + 4*me2*s35*ss + s15*s35*ss + 2*me2*ss**2 -&
   8*me2**2*tt - 8*me2*mm2*tt - 8*me2*s15*tt - s15**2*tt - 4*me2*s25*tt - s15*s25*tt +&
   8*me2*s35*tt + 2*s15*s35*tt + 4*me2*ss*tt + 4*me2*tt**2))/(s15**2*(s15 + s25 -&
   ss)**2)

  m34 = (-8*e**6*(8*me2**3 + 16*me2**2*mm2 + 8*me2*mm2**2 + 8*me2**2*s15 + 12*me2*mm2*s15 +&
   4*mm2**2*s15 + 11*me2*s15**2 + 7*mm2*s15**2 + 3*s15**3 + 4*me2*mm2*s25 +&
   4*mm2**2*s25 + 12*me2*s15*s25 + 8*mm2*s15*s25 + 4*s15**2*s25 + me2*s25**2 +&
   mm2*s25**2 + s15*s25**2 - 8*me2**2*s35 - 8*me2*mm2*s35 - 10*me2*s15*s35 -&
   6*mm2*s15*s35 - 5*s15**2*s35 - 2*me2*s25*s35 - 2*mm2*s25*s35 - 3*s15*s25*s35 +&
   2*s15*s35**2 - 4*me2**2*ss - 8*me2*mm2*ss - 4*mm2**2*ss - 14*me2*s15*ss -&
   6*mm2*s15*ss - 7*s15**2*ss - 6*me2*s25*ss - 2*mm2*s25*ss - 7*s15*s25*ss -&
   2*s25**2*ss + 8*me2*s35*ss + 4*mm2*s35*ss + 7*s15*s35*ss + 3*s25*s35*ss -&
   2*s35**2*ss + 4*me2*ss**2 + 6*s15*ss**2 + 4*s25*ss**2 - 2*s35*ss**2 - 2*ss**3 -&
   16*me2**2*tt - 16*me2*mm2*tt - 20*me2*s15*tt - 8*mm2*s15*tt - 7*s15**2*tt -&
   12*me2*s25*tt - 8*mm2*s25*tt - 8*s15*s25*tt - s25**2*tt + 8*me2*s35*tt +&
   6*s15*s35*tt + 2*s25*s35*tt + 16*me2*ss*tt + 8*mm2*ss*tt + 10*s15*ss*tt +&
   6*s25*ss*tt - 4*s35*ss*tt - 4*ss**2*tt + 8*me2*tt**2 + 4*s15*tt**2 + 4*s25*tt**2 -&
   4*ss*tt**2))/(s15*s25*(s15 + s25 - ss)**2) + (8*e**6*snm*(4*me2**3 + 8*me2**2*mm2 +&
   4*me2*mm2**2 + 4*me2**2*s15 + 6*me2*mm2*s15 + 2*mm2**2*s15 + 3*me2*s15**2 +&
   3*mm2*s15**2 + s15**3 + 2*me2*mm2*s25 + 2*mm2**2*s25 + 2*me2*s15*s25 + 6*mm2*s15*s25&
   + s15**2*s25 - me2*s25**2 + mm2*s25**2 - 4*me2**2*s35 - 4*me2*mm2*s35 -&
   4*me2*s15*s35 - 2*mm2*s15*s35 - s15**2*s35 - 2*me2*s25*s35 - 2*mm2*s25*s35 -&
   s15*s25*s35 - 2*me2**2*ss - 4*me2*mm2*ss - 2*mm2**2*ss - 3*me2*s15*ss - 3*mm2*s15*ss&
   - 2*s15**2*ss + me2*s25*ss - mm2*s25*ss - s15*s25*ss + 4*me2*s35*ss + 2*mm2*s35*ss +&
   2*s15*s35*ss + s25*s35*ss + s15*ss**2 - s35*ss**2 - 8*me2**2*tt - 8*me2*mm2*tt -&
   10*me2*s15*tt - 4*mm2*s15*tt - 3*s15**2*tt - 6*me2*s25*tt - 4*mm2*s25*tt -&
   4*s15*s25*tt - s25**2*tt + 4*me2*s35*tt + 2*s15*s35*tt + 2*s25*s35*tt + 8*me2*ss*tt&
   + 4*mm2*ss*tt + 5*s15*ss*tt + 3*s25*ss*tt - 2*s35*ss*tt - 2*ss**2*tt + 4*me2*tt**2 +&
   2*s15*tt**2 + 2*s25*tt**2 - 2*ss*tt**2))/(s15*s25*(s15 + s25 - ss)**2) +&
   s3m*((-8*e**6*s3n*(-2*mm2 + s15 + s25 - ss)*(-2*me2 + ss))/(s15*s25*(s15 + s25 -&
   ss)**2) + (8*e**6*s4n*(4*me2**2 + 3*me2*s15 + me2*s25 - 2*me2*s35 - 2*me2*ss +&
   2*mm2*ss - 4*me2*tt))/(s15*s25*(s15 + s25 - ss)**2) + (8*e**6*s2n*(3*mm2*s15 +&
   mm2*s25 - 3*mm2*ss - s15*tt - s25*tt + ss*tt))/(s15*s25*(s15 + s25 - ss)**2)) +&
   s4m*((-8*e**6*s4n*(-2*mm2 + s15 + s25 - ss)*(-2*me2 + ss))/(s15*s25*(s15 + s25 -&
   ss)**2) - (8*e**6*s3n*(4*me2**2 + 8*me2*mm2 + 3*me2*s15 + me2*s25 - 2*me2*s35 -&
   2*me2*ss - 2*mm2*ss - 4*me2*tt))/(s15*s25*(s15 + s25 - ss)**2) -&
   (8*e**6*s2n*(2*me2*s15 - mm2*s15 + s15**2 + 2*me2*s25 + mm2*s25 + s15*s25 - 2*me2*ss&
   + mm2*ss - 2*s15*ss - s25*ss + ss**2 - s15*tt - s25*tt + ss*tt))/(s15*s25*(s15 + s25&
   - ss)**2)) + s1m*((-32*e**6*mm2*s2n)/(s15*s25*(s15 + s25 - ss)) +&
   (8*e**6*s4n*(mm2*s15 + s15**2 + 3*mm2*s25 + s15*s25 - s15*s35 - s25*s35 - 3*mm2*ss -&
   s15*ss + s35*ss - s15*tt - s25*tt + ss*tt))/(s15*s25*(s15 + s25 - ss)**2) -&
   (8*e**6*s3n*(2*me2*s15 + mm2*s15 + s15**2 + 2*me2*s25 - mm2*s25 + 2*s15*s25 + s25**2&
   - s15*s35 - s25*s35 - 2*me2*ss + mm2*ss - 2*s15*ss - 2*s25*ss + s35*ss + ss**2 -&
   s15*tt - s25*tt + ss*tt))/(s15*s25*(s15 + s25 - ss)**2))

  m44 = s1m*((16*e**6*me2*s3n)/(s25**2*(-s15 - s25 + ss)) + (16*e**6*me2*s4n)/(s25**2*(-s15 -&
   s25 + ss))) + (32*e**6*me2*snm*(me2**2 + 2*me2*mm2 + mm2**2 - me2*s25 - mm2*s25 -&
   2*me2*tt - 2*mm2*tt - s15*tt + ss*tt + tt**2))/(s25**2*(-s15 - s25 + ss)**2) -&
   (16*e**6*(4*me2**3 + 8*me2**2*mm2 + 4*me2*mm2**2 + 2*me2*s15**2 - 4*me2**2*s25 -&
   4*me2*mm2*s25 + 3*me2*s15*s25 - 3*mm2*s15*s25 + me2*s25**2 - mm2*s25**2 +&
   2*me2*s25*s35 + 2*mm2*s25*s35 + s15*s25*s35 - 4*me2*s15*ss - 4*me2*s25*ss -&
   s25*s35*ss + 2*me2*ss**2 - 8*me2**2*tt - 8*me2*mm2*tt - 4*me2*s15*tt + s15*s25*tt +&
   s25**2*tt - 2*s25*s35*tt + 4*me2*ss*tt + 4*me2*tt**2))/(s25**2*(-s15 - s25 + ss)**2)&
   + s3m*((-16*e**6*me2*s3n*(2*mm2 - s15 - s25 + ss))/(s25**2*(-s15 - s25 + ss)**2) -&
   (16*e**6*me2*s4n*(-2*me2 - s15 + ss + 2*tt))/(s25**2*(-s15 - s25 + ss)**2)) +&
   s4m*((-16*e**6*me2*s4n*(2*mm2 - s15 - s25 + ss))/(s25**2*(-s15 - s25 + ss)**2) +&
   (16*e**6*me2*s3n*(-2*me2 - 4*mm2 - s15 + ss + 2*tt))/(s25**2*(-s15 - s25 + ss)**2))


  pepe2mmg_ee = (m33+2*m34+m44)*0.25
  END FUNCTION PEPE2MMG_EE



  FUNCTION PEPE2MMG(p1,pol1,p2,pol2,p3,p4,p5)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),pol1(4),pol2(4)
  real (kind=prec) :: ss,tt, me2, mm2, s2n, s3n, s4n, s1m, s3m, s4m, snm
  real (kind=prec) :: me1, mm1, e
  real (kind=prec) :: pepe2mmg
  real (kind=prec) :: s15, s35, s25
  real (kind=prec) :: m11, m12, m13, m14, m22, m23, m24, m33, m34, m44

  ss = sq(p1+p2); tt = sq(p1-p3); s15 = s(p1,p5); s35 = s(p3,p5); s25 = s(p2,p5)
  me2=sq(p1); mm2=sq(p3); me1 = sqrt(me2); mm1 = sqrt(mm2)
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)
  e = sqrt(4*pi*alpha)


  m11 = (-8*e**6*snm*(-4*me2**2*mm2 - 8*me2*mm2**2 - 4*mm2**3 - 8*me2*mm2*s15 - 8*mm2**2*s15 -&
   4*mm2*s15**2 + 4*me2*mm2*s35 + 4*mm2**2*s35 + 3*me2*s15*s35 + 5*mm2*s15*s35 +&
   s15**2*s35 + me2*s25*s35 - mm2*s25*s35 - s15*s25*s35 - 2*me2*s35**2 - s15*s35**2 +&
   4*mm2*s15*ss - 4*mm2*s35*ss - s15*s35*ss + s35**2*ss + 8*me2*mm2*tt + 8*mm2**2*tt +&
   8*mm2*s15*tt - 4*mm2*s35*tt - s15*s35*tt + s25*s35*tt - 4*mm2*ss*tt -&
   4*mm2*tt**2))/(s35**2*ss**2) - (16*e**6*(4*me2**2*mm2 + 8*me2*mm2**2 + 4*mm2**3 +&
   8*me2*mm2*s15 + 8*mm2**2*s15 + 4*mm2*s15**2 - 4*me2*mm2*s35 - 4*mm2**2*s35 -&
   3*me2*s15*s35 - 5*mm2*s15*s35 - s15**2*s35 - me2*s25*s35 + mm2*s25*s35 + s15*s25*s35&
   + 2*me2*s35**2 + s15*s35**2 - 4*mm2*s15*ss + 4*mm2*s35*ss - s25*s35*ss + 2*mm2*ss**2&
   - 8*me2*mm2*tt - 8*mm2**2*tt - 8*mm2*s15*tt + 4*mm2*s35*tt + s15*s35*tt - s25*s35*tt&
   + 4*mm2*ss*tt + 4*mm2*tt**2))/(s35**2*ss**2) + s3m*((8*e**6*s4n)/(s35*ss) -&
   (8*e**6*s2n*(-me2 - mm2 - s15 + ss + tt))/(s35*ss**2)) + s1m*((8*e**6*s2n*(-4*mm2**2&
   - s15*s35 - s25*s35 + s35*ss))/(s35**2*ss**2) + (8*e**6*s3n*(-me2 - mm2 - s15 + s35&
   + tt))/(s35*ss**2) - (8*e**6*s4n*(-4*me2*mm2 - 4*mm2**2 - 4*mm2*s15 + me2*s35 +&
   5*mm2*s35 + s15*s35 - s25*s35 - s35**2 + s35*ss + 4*mm2*tt -&
   s35*tt))/(s35**2*ss**2)) + s4m*((8*e**6*s3n)/(s35*ss) + (16*e**6*(-2*mm2 +&
   s35)*s4n)/(s35**2*ss) - (8*e**6*s2n*(4*me2*mm2 + 4*mm2**2 + 4*mm2*s15 - me2*s35 -&
   mm2*s35 - 2*s15*s35 - 4*mm2*ss + 2*s35*ss - 4*mm2*tt + s35*tt))/(s35**2*ss**2))

  m12 = (4*e**6*snm*(8*me2**2*mm2 + 16*me2*mm2**2 + 8*mm2**3 + 12*me2*mm2*s15 + 12*mm2**2*s15 +&
   me2*s15**2 + 5*mm2*s15**2 + s15**3 + 4*me2*mm2*s25 + 4*mm2**2*s25 + 4*mm2*s15*s25 -&
   me2*s25**2 - mm2*s25**2 - s15*s25**2 - 8*me2*mm2*s35 - 8*mm2**2*s35 - 2*me2*s15*s35&
   - 6*mm2*s15*s35 - 3*s15**2*s35 + 2*me2*s25*s35 + 2*mm2*s25*s35 - s15*s25*s35 +&
   2*s15*s35**2 - 4*me2**2*ss - 8*me2*mm2*ss - 4*mm2**2*ss - 2*me2*s15*ss -&
   10*mm2*s15*ss - 3*s15**2*ss + 2*me2*s25*ss - 2*mm2*s25*ss - s15*s25*ss +&
   4*me2*s35*ss + 8*mm2*s35*ss + 5*s15*s35*ss + s25*s35*ss - 2*s35**2*ss + 2*s15*ss**2&
   - 2*s35*ss**2 - 16*me2*mm2*tt - 16*mm2**2*tt - 12*mm2*s15*tt - s15**2*tt -&
   4*mm2*s25*tt + s25**2*tt + 8*mm2*s35*tt + 2*s15*s35*tt - 2*s25*s35*tt + 8*me2*ss*tt&
   + 16*mm2*ss*tt + 6*s15*ss*tt + 2*s25*ss*tt - 4*s35*ss*tt - 4*ss**2*tt + 8*mm2*tt**2&
   - 4*ss*tt**2))/((s15 + s25 - s35)*s35*ss**2) + (8*e**6*(-8*me2**2*mm2 -&
   16*me2*mm2**2 - 8*mm2**3 - 12*me2*mm2*s15 - 12*mm2**2*s15 - me2*s15**2 -&
   5*mm2*s15**2 - s15**3 - 4*me2*mm2*s25 - 4*mm2**2*s25 - 4*mm2*s15*s25 + me2*s25**2 +&
   mm2*s25**2 + s15*s25**2 + 8*me2*mm2*s35 + 8*mm2**2*s35 + 2*me2*s15*s35 +&
   6*mm2*s15*s35 + 3*s15**2*s35 - 2*me2*s25*s35 - 2*mm2*s25*s35 + s15*s25*s35 -&
   2*s15*s35**2 + 4*me2**2*ss + 8*me2*mm2*ss + 4*mm2**2*ss + 2*me2*s15*ss +&
   10*mm2*s15*ss + 3*s15**2*ss - 2*me2*s25*ss + 2*mm2*s25*ss + s15*s25*ss -&
   4*me2*s35*ss - 8*mm2*s35*ss - 5*s15*s35*ss - s25*s35*ss + 2*s35**2*ss - 4*mm2*ss**2&
   - 4*s15*ss**2 - 2*s25*ss**2 + 2*s35*ss**2 + 2*ss**3 + 16*me2*mm2*tt + 16*mm2**2*tt +&
   12*mm2*s15*tt + s15**2*tt + 4*mm2*s25*tt - s25**2*tt - 8*mm2*s35*tt - 2*s15*s35*tt +&
   2*s25*s35*tt - 8*me2*ss*tt - 16*mm2*ss*tt - 6*s15*ss*tt - 2*s25*ss*tt + 4*s35*ss*tt&
   + 4*ss**2*tt - 8*mm2*tt**2 + 4*ss*tt**2))/((s15 + s25 - s35)*s35*ss**2) +&
   s3m*((4*e**6*(4*mm2 - s15 - s25)*s4n)/((s15 + s25 - s35)*s35*ss) + (8*e**6*s3n*(-s35&
   + ss))/((s15 + s25 - s35)*s35*ss) + (4*e**6*s2n*(4*me2*mm2 + 4*mm2**2 - me2*s15 +&
   3*mm2*s15 - s15**2 - me2*s25 - mm2*s25 - s15*s25 + 2*me2*s35 + 2*mm2*s35 - 2*me2*ss&
   - 6*mm2*ss + s15*ss + s25*ss - 4*mm2*tt + s15*tt + s25*tt - 2*s35*tt +&
   2*ss*tt))/((s15 + s25 - s35)*s35*ss**2)) + s1m*((4*e**6*s2n*(-8*mm2**2 - 4*mm2*s15 +&
   s15**2 - 4*mm2*s25 + 2*s15*s25 + s25**2 + 8*mm2*ss - s15*ss - s25*ss))/((s15 + s25 -&
   s35)*s35*ss**2) - (4*e**6*s4n*(-4*me2*mm2 - 4*mm2**2 - me2*s15 - 5*mm2*s15 - s15**2&
   - me2*s25 - 5*mm2*s25 + s25**2 + 2*me2*s35 + 6*mm2*s35 + 3*s15*s35 + s25*s35 -&
   2*s35**2 + 2*me2*ss + 6*mm2*ss + s15*ss - s25*ss - 2*s35*ss + 4*mm2*tt + s15*tt +&
   s25*tt - 2*s35*tt - 2*ss*tt))/((s15 + s25 - s35)*s35*ss**2) - (4*e**6*s3n*(4*me2*mm2&
   + 4*mm2**2 - me2*s15 + 3*mm2*s15 - s15**2 - me2*s25 - mm2*s25 - s15*s25 + 2*me2*s35&
   - 2*mm2*s35 + 3*s15*s35 + 3*s25*s35 - 2*s35**2 - 2*me2*ss - 2*mm2*ss - 2*s15*ss -&
   2*s25*ss + 2*ss**2 - 4*mm2*tt + s15*tt + s25*tt - 2*s35*tt + 2*ss*tt))/((s15 + s25 -&
   s35)*s35*ss**2)) + s4m*((4*e**6*(4*mm2 - s15 - s25)*s3n)/((s15 + s25 - s35)*s35*ss)&
   + (8*e**6*s4n*(-s15 - s25 + s35 + ss))/((s15 + s25 - s35)*s35*ss) -&
   (4*e**6*s2n*(4*me2*mm2 + 4*mm2**2 + me2*s15 + mm2*s15 + 2*s15**2 + me2*s25 + mm2*s25&
   + 2*s15*s25 - 2*me2*s35 - 2*mm2*s35 - 2*s15*s35 - 2*me2*ss - 2*mm2*ss - 4*s15*ss -&
   2*s25*ss + 2*s35*ss + 2*ss**2 - 4*mm2*tt - s15*tt - s25*tt + 2*s35*tt +&
   2*ss*tt))/((s15 + s25 - s35)*s35*ss**2))

  m13 = (8*e**6*(4*me2**3 + 12*me2**2*mm2 + 12*me2*mm2**2 + 4*mm2**3 + 8*me2**2*s15 +&
   20*me2*mm2*s15 + 12*mm2**2*s15 + 6*me2*s15**2 + 10*mm2*s15**2 + 2*s15**3 +&
   3*me2*s15*s25 + 3*mm2*s15*s25 + 3*s15**2*s25 + me2*s25**2 + mm2*s25**2 + s15*s25**2&
   - 12*me2**2*s35 - 20*me2*mm2*s35 - 8*mm2**2*s35 - 15*me2*s15*s35 - 15*mm2*s15*s35 -&
   5*s15**2*s35 - 3*me2*s25*s35 - 3*mm2*s25*s35 - 2*s15*s25*s35 + 8*me2*s35**2 +&
   4*mm2*s35**2 + 3*s15*s35**2 - 6*me2*s15*ss - 6*mm2*s15*ss - 4*s15**2*ss -&
   2*me2*s25*ss - 2*mm2*s25*ss - 3*s15*s25*ss + 4*me2*s35*ss + 4*mm2*s35*ss +&
   5*s15*s35*ss + s25*s35*ss - s35**2*ss + 2*me2*ss**2 + 2*mm2*ss**2 + 2*s15*ss**2 -&
   2*s35*ss**2 - 12*me2**2*tt - 24*me2*mm2*tt - 12*mm2**2*tt - 18*me2*s15*tt -&
   22*mm2*s15*tt - 8*s15**2*tt - 2*me2*s25*tt - 2*mm2*s25*tt - 5*s15*s25*tt - s25**2*tt&
   + 20*me2*s35*tt + 16*mm2*s35*tt + 13*s15*s35*tt + 3*s25*s35*tt - 4*s35**2*tt +&
   4*me2*ss*tt + 4*mm2*ss*tt + 8*s15*ss*tt + 2*s25*ss*tt - 6*s35*ss*tt - 2*ss**2*tt +&
   12*me2*tt**2 + 12*mm2*tt**2 + 10*s15*tt**2 + 2*s25*tt**2 - 8*s35*tt**2 - 4*ss*tt**2&
   - 4*tt**3))/(s15*s35*ss*(-s15 - s25 + ss)) + (4*e**6*snm*(-4*me2**3 - 12*me2**2*mm2&
   - 12*me2*mm2**2 - 4*mm2**3 - 8*me2**2*s15 - 20*me2*mm2*s15 - 12*mm2**2*s15 -&
   5*me2*s15**2 - 9*mm2*s15**2 - s15**3 - me2*s15*s25 - mm2*s15*s25 - s15**2*s25 +&
   12*me2**2*s35 + 20*me2*mm2*s35 + 8*mm2**2*s35 + 14*me2*s15*s35 + 14*mm2*s15*s35 +&
   3*s15**2*s35 + 2*me2*s25*s35 + 2*mm2*s25*s35 - 8*me2*s35**2 - 4*mm2*s35**2 -&
   2*s15*s35**2 + 4*me2*s15*ss + 4*mm2*s15*ss + s15**2*ss - 4*me2*s35*ss - 4*mm2*s35*ss&
   - 3*s15*s35*ss + 2*s35**2*ss + 12*me2**2*tt + 24*me2*mm2*tt + 12*mm2**2*tt +&
   18*me2*s15*tt + 22*mm2*s15*tt + 7*s15**2*tt + 2*me2*s25*tt + 2*mm2*s25*tt +&
   3*s15*s25*tt - 20*me2*s35*tt - 16*mm2*s35*tt - 12*s15*s35*tt - 2*s25*s35*tt +&
   4*s35**2*tt - 4*me2*ss*tt - 4*mm2*ss*tt - 6*s15*ss*tt + 6*s35*ss*tt - 12*me2*tt**2 -&
   12*mm2*tt**2 - 10*s15*tt**2 - 2*s25*tt**2 + 8*s35*tt**2 + 4*ss*tt**2 +&
   4*tt**3))/(s15*s35*ss*(-s15 - s25 + ss)) + s3m*((-4*e**6*s4n*(2*me2*s15 + 2*me2*s25&
   - 2*me2*s35 - 2*me2*ss - 2*mm2*ss + s15*ss))/(s15*s35*ss*(-s15 - s25 + ss)) +&
   (8*e**6*s3n*(me2 + mm2 + s15 - s35 - tt))/(s15*s35*(-s15 - s25 + ss)) +&
   (4*e**6*s2n*(-2*me2**2 - 2*me2*mm2 - me2*s15 - mm2*s15 - s15**2 + 2*me2*s25 +&
   2*me2*s35 + 2*mm2*s35 - 2*me2*ss - 2*mm2*ss + s15*ss + 4*me2*tt + 2*mm2*tt +&
   3*s15*tt - 2*s35*tt - 2*tt**2))/(s15*s35*ss*(-s15 - s25 + ss))) +&
   s1m*((-4*e**6*s2n*(-4*me2*mm2 - 4*mm2**2 - 2*mm2*s15 - s15**2 + 2*mm2*s25 - s15*s25&
   + 4*mm2*s35 - 2*mm2*ss + s15*ss + 4*mm2*tt))/(s15*s35*ss*(-s15 - s25 + ss)) +&
   (4*e**6*s4n*(-2*me2**2 - 6*me2*mm2 - 4*mm2**2 - 3*me2*s15 - 5*mm2*s15 - s15**2 +&
   2*mm2*s25 - s15*s25 + 4*me2*s35 + 6*mm2*s35 + 3*s15*s35 - 2*s35**2 - 2*mm2*ss +&
   s15*ss + 4*me2*tt + 6*mm2*tt + 3*s15*tt - 4*s35*tt - 2*tt**2))/(s15*s35*ss*(-s15 -&
   s25 + ss)) - (4*e**6*s3n*(-2*me2**2 - 2*me2*mm2 - 3*me2*s15 - 3*mm2*s15 - 3*s15**2 -&
   2*mm2*s25 - 2*s15*s25 + 4*me2*s35 + 2*mm2*s35 + 5*s15*s35 + 2*s25*s35 - 2*s35**2 +&
   2*mm2*ss + 2*s15*ss - 2*s35*ss + 4*me2*tt + 2*mm2*tt + 5*s15*tt + 2*s25*tt -&
   4*s35*tt - 2*ss*tt - 2*tt**2))/(s15*s35*ss*(-s15 - s25 + ss))) +&
   s4m*((-4*e**6*s3n*(-2*me2*s15 - 2*me2*s25 + 2*me2*s35 + 2*me2*ss - 2*mm2*ss +&
   s15*ss))/(s15*s35*ss*(-s15 - s25 + ss)) - (8*e**6*s4n*(-me2 - 3*mm2 + s35 +&
   tt))/(s15*s35*(-s15 - s25 + ss)) + (4*e**6*s2n*(2*me2**2 + 6*me2*mm2 + 4*mm2**2 +&
   3*me2*s15 + 7*mm2*s15 - 2*me2*s35 - 2*mm2*s35 - 2*s15*s35 - 2*me2*ss - 6*mm2*ss +&
   2*s35*ss - 4*me2*tt - 6*mm2*tt - 3*s15*tt + 2*s35*tt + 2*ss*tt +&
   2*tt**2))/(s15*s35*ss*(-s15 - s25 + ss)))

  m14 = (-4*e**6*snm*(4*me2**3 + 12*me2**2*mm2 + 12*me2*mm2**2 + 4*mm2**3 + 8*me2**2*s15 +&
   16*me2*mm2*s15 + 8*mm2**2*s15 + 6*me2*s15**2 + 6*mm2*s15**2 + 2*s15**3 +&
   4*me2*mm2*s25 + 4*mm2**2*s25 + me2*s15*s25 + 5*mm2*s15*s25 + s15**2*s25 - me2*s25**2&
   - mm2*s25**2 - s15*s25**2 - 8*me2**2*s35 - 12*me2*mm2*s35 - 4*mm2**2*s35 -&
   8*me2*s15*s35 - 4*mm2*s15*s35 - 2*s15**2*s35 - s15*s25*s35 - 4*me2**2*ss -&
   8*me2*mm2*ss - 4*mm2**2*ss - 6*me2*s15*ss - 6*mm2*s15*ss - 4*s15**2*ss +&
   2*me2*s25*ss - 2*mm2*s25*ss - s15*s25*ss + 8*me2*s35*ss + 4*mm2*s35*ss +&
   4*s15*s35*ss + s25*s35*ss + 2*s15*ss**2 - 2*s35*ss**2 - 12*me2**2*tt - 24*me2*mm2*tt&
   - 12*mm2**2*tt - 18*me2*s15*tt - 18*mm2*s15*tt - 8*s15**2*tt - 2*me2*s25*tt -&
   6*mm2*s25*tt - 3*s15*s25*tt + s25**2*tt + 12*me2*s35*tt + 8*mm2*s35*tt +&
   6*s15*s35*tt + 12*me2*ss*tt + 12*mm2*ss*tt + 12*s15*ss*tt + 2*s25*ss*tt -&
   6*s35*ss*tt - 4*ss**2*tt + 12*me2*tt**2 + 12*mm2*tt**2 + 10*s15*tt**2 + 2*s25*tt**2&
   - 4*s35*tt**2 - 8*ss*tt**2 - 4*tt**3))/(s25*s35*ss*(-s15 - s25 + ss)) -&
   (8*e**6*(-4*me2**3 - 12*me2**2*mm2 - 12*me2*mm2**2 - 4*mm2**3 - 8*me2**2*s15 -&
   16*me2*mm2*s15 - 8*mm2**2*s15 - 7*me2*s15**2 - 7*mm2*s15**2 - 3*s15**3 -&
   4*me2*mm2*s25 - 4*mm2**2*s25 - 3*me2*s15*s25 - 7*mm2*s15*s25 - 3*s15**2*s25 +&
   8*me2**2*s35 + 12*me2*mm2*s35 + 4*mm2**2*s35 + 9*me2*s15*s35 + 5*mm2*s15*s35 +&
   4*s15**2*s35 + me2*s25*s35 + mm2*s25*s35 + 3*s15*s25*s35 - s15*s35**2 + 4*me2**2*ss&
   + 8*me2*mm2*ss + 4*mm2**2*ss + 8*me2*s15*ss + 8*mm2*s15*ss + 7*s15**2*ss +&
   4*mm2*s25*ss + 4*s15*s25*ss - 8*me2*s35*ss - 4*mm2*s35*ss - 6*s15*s35*ss -&
   2*s25*s35*ss + s35**2*ss - 2*me2*ss**2 - 2*mm2*ss**2 - 6*s15*ss**2 - 2*s25*ss**2 +&
   2*s35*ss**2 + 2*ss**3 + 12*me2**2*tt + 24*me2*mm2*tt + 12*mm2**2*tt + 18*me2*s15*tt&
   + 18*mm2*s15*tt + 9*s15**2*tt + 2*me2*s25*tt + 6*mm2*s25*tt + 5*s15*s25*tt -&
   12*me2*s35*tt - 8*mm2*s35*tt - 7*s15*s35*tt - s25*s35*tt - 12*me2*ss*tt -&
   12*mm2*ss*tt - 14*s15*ss*tt - 4*s25*ss*tt + 6*s35*ss*tt + 6*ss**2*tt - 12*me2*tt**2&
   - 12*mm2*tt**2 - 10*s15*tt**2 - 2*s25*tt**2 + 4*s35*tt**2 + 8*ss*tt**2 +&
   4*tt**3))/(s25*s35*ss*(-s15 - s25 + ss)) + s3m*((4*e**6*s4n*(-2*me2*s15 - 2*me2*s25&
   + 2*me2*s35 + 2*me2*ss - 2*mm2*ss + s25*ss))/(s25*s35*ss*(-s15 - s25 + ss)) -&
   (8*e**6*s3n*(-me2 - mm2 - s15 + ss + tt))/(s25*s35*(-s15 - s25 + ss)) -&
   (4*e**6*s2n*(2*me2**2 + 6*me2*mm2 + 4*mm2**2 + 4*mm2*s15 - 3*me2*s25 - mm2*s25 -&
   s15*s25 - 4*mm2*ss + s25*ss - 4*me2*tt - 6*mm2*tt - 2*s15*tt + s25*tt + 2*ss*tt +&
   2*tt**2))/(s25*s35*ss*(-s15 - s25 + ss))) + s4m*((4*e**6*s3n*(2*me2*s15 + 2*me2*s25&
   - 2*me2*s35 - 2*me2*ss - 2*mm2*ss + s25*ss))/(s25*s35*ss*(-s15 - s25 + ss)) -&
   (8*e**6*s4n*(-me2 + mm2 - s15 - s25 + ss + tt))/(s25*s35*(-s15 - s25 + ss)) +&
   (4*e**6*s2n*(2*me2**2 + 2*me2*mm2 + 4*me2*s15 + 2*s15**2 + me2*s25 + mm2*s25 +&
   2*s15*s25 - 4*me2*ss - 4*s15*ss - 2*s25*ss + 2*ss**2 - 4*me2*tt - 2*mm2*tt -&
   4*s15*tt - s25*tt + 4*ss*tt + 2*tt**2))/(s25*s35*ss*(-s15 - s25 + ss))) +&
   s1m*((4*e**6*s2n*(4*me2*mm2 + 4*mm2**2 + 6*mm2*s15 + 2*mm2*s25 - s15*s25 - s25**2 -&
   6*mm2*ss + s25*ss - 4*mm2*tt))/(s25*s35*ss*(-s15 - s25 + ss)) +&
   (4*e**6*s4n*(-2*me2**2 - 2*me2*mm2 - 4*me2*s15 - 2*mm2*s15 - 2*s15**2 - me2*s25 -&
   3*mm2*s25 - s15*s25 + s25**2 + 2*me2*s35 + 2*s15*s35 + s25*s35 + 2*me2*ss + 4*mm2*ss&
   + 2*s15*ss - s25*ss - 2*s35*ss + 4*me2*tt + 2*mm2*tt + 4*s15*tt + s25*tt - 2*s35*tt&
   - 2*ss*tt - 2*tt**2))/(s25*s35*ss*(-s15 - s25 + ss)) + (4*e**6*s3n*(2*me2**2 +&
   6*me2*mm2 + 4*mm2**2 + 2*me2*s15 + 6*mm2*s15 + 2*s15**2 - me2*s25 + mm2*s25 +&
   s15*s25 - 2*me2*s35 - 4*mm2*s35 - 2*s15*s35 + s25*s35 - 2*me2*ss - 4*mm2*ss -&
   4*s15*ss - 2*s25*ss + 2*s35*ss + 2*ss**2 - 4*me2*tt - 6*mm2*tt - 4*s15*tt - s25*tt +&
   2*s35*tt + 4*ss*tt + 2*tt**2))/(s25*s35*ss*(-s15 - s25 + ss)))

  m22 = s4m*((8*e**6*s3n)/((s15 + s25 - s35)*ss) - (8*e**6*s2n*(me2 + mm2 - tt))/((s15 + s25 -&
   s35)*ss**2)) - (8*e**6*snm*(-4*me2**2*mm2 - 8*me2*mm2**2 - 4*mm2**3 - 4*me2*mm2*s15&
   - 4*mm2**2*s15 - me2*s15**2 - mm2*s15**2 - s15**3 - 4*me2*mm2*s25 - 4*mm2**2*s25 -&
   2*s15**2*s25 + me2*s25**2 + mm2*s25**2 - s15*s25**2 + 4*me2*mm2*s35 + 4*mm2**2*s35 +&
   3*me2*s15*s35 + mm2*s15*s35 + 2*s15**2*s35 + me2*s25*s35 - mm2*s25*s35 +&
   2*s15*s25*s35 - 2*me2*s35**2 - s15*s35**2 + s15**2*ss + s15*s25*ss - 2*s15*s35*ss -&
   s25*s35*ss + s35**2*ss + 8*me2*mm2*tt + 8*mm2**2*tt + 4*mm2*s15*tt + s15**2*tt +&
   4*mm2*s25*tt - s25**2*tt - 4*mm2*s35*tt - s15*s35*tt + s25*s35*tt - 4*mm2*ss*tt -&
   4*mm2*tt**2))/((s15 + s25 - s35)**2*ss**2) - (16*e**6*(4*me2**2*mm2 + 8*me2*mm2**2 +&
   4*mm2**3 + 4*me2*mm2*s15 + 4*mm2**2*s15 + me2*s15**2 + mm2*s15**2 + s15**3 +&
   4*me2*mm2*s25 + 4*mm2**2*s25 + 2*s15**2*s25 - me2*s25**2 - mm2*s25**2 + s15*s25**2 -&
   4*me2*mm2*s35 - 4*mm2**2*s35 - 3*me2*s15*s35 - mm2*s15*s35 - 2*s15**2*s35 -&
   me2*s25*s35 + mm2*s25*s35 - 2*s15*s25*s35 + 2*me2*s35**2 + s15*s35**2 - s15**2*ss -&
   s15*s25*ss + s15*s35*ss + 2*mm2*ss**2 - 8*me2*mm2*tt - 8*mm2**2*tt - 4*mm2*s15*tt -&
   s15**2*tt - 4*mm2*s25*tt + s25**2*tt + 4*mm2*s35*tt + s15*s35*tt - s25*s35*tt +&
   4*mm2*ss*tt + 4*mm2*tt**2))/((s15 + s25 - s35)**2*ss**2) +&
   s1m*((8*e**6*s2n*(-4*mm2**2 - s15**2 - 2*s15*s25 - s25**2 + s15*s35 + s25*s35 +&
   s15*ss + s25*ss - s35*ss))/((s15 + s25 - s35)**2*ss**2) - (8*e**6*s4n*(-me2 - mm2 -&
   s15 - s25 + s35 + ss + tt))/((s15 + s25 - s35)*ss**2) - (8*e**6*s3n*(4*me2*mm2 +&
   4*mm2**2 - me2*s15 + 3*mm2*s15 - s15**2 - me2*s25 + 3*mm2*s25 - 3*s15*s25 - 2*s25**2&
   + me2*s35 - 3*mm2*s35 + 2*s15*s35 + 3*s25*s35 - s35**2 - 4*mm2*ss + 2*s15*ss +&
   2*s25*ss - 2*s35*ss - 4*mm2*tt + s15*tt + s25*tt - s35*tt))/((s15 + s25 -&
   s35)**2*ss**2)) + s3m*((16*e**6*(-2*mm2 + s15 + s25 - s35)*s3n)/((s15 + s25 -&
   s35)**2*ss) + (8*e**6*s4n)/((s15 + s25 - s35)*ss) - (8*e**6*s2n*(-4*me2*mm2 -&
   4*mm2**2 + me2*s15 + mm2*s15 - s15**2 + me2*s25 + mm2*s25 - s15*s25 - me2*s35 -&
   mm2*s35 + s15*s35 + s15*ss + s25*ss - s35*ss + 4*mm2*tt - s15*tt - s25*tt +&
   s35*tt))/((s15 + s25 - s35)**2*ss**2))

  m23 = (-4*e**6*snm*(4*me2**3 + 12*me2**2*mm2 + 12*me2*mm2**2 + 4*mm2**3 + 4*me2**2*s15 +&
   16*me2*mm2*s15 + 12*mm2**2*s15 + me2*s15**2 + 9*mm2*s15**2 + s15**3 + 4*me2*mm2*s25&
   + 4*mm2**2*s25 - me2*s15*s25 + 11*mm2*s15*s25 + s15**2*s25 - 2*me2*s25**2 +&
   2*mm2*s25**2 - 4*me2**2*s35 - 12*me2*mm2*s35 - 8*mm2**2*s35 - 2*me2*s15*s35 -&
   14*mm2*s15*s35 - 3*s15**2*s35 + 2*me2*s25*s35 - 6*mm2*s25*s35 - 2*s15*s25*s35 +&
   4*mm2*s35**2 + 2*s15*s35**2 - 4*me2**2*ss - 8*me2*mm2*ss - 4*mm2**2*ss -&
   2*me2*s15*ss - 10*mm2*s15*ss - 3*s15**2*ss + 2*me2*s25*ss - 2*mm2*s25*ss -&
   2*s15*s25*ss + 4*me2*s35*ss + 8*mm2*s35*ss + 5*s15*s35*ss + 2*s25*s35*ss -&
   2*s35**2*ss + 2*s15*ss**2 - 2*s35*ss**2 - 12*me2**2*tt - 24*me2*mm2*tt -&
   12*mm2**2*tt - 14*me2*s15*tt - 22*mm2*s15*tt - 7*s15**2*tt - 6*me2*s25*tt -&
   10*mm2*s25*tt - 9*s15*s25*tt - 2*s25**2*tt + 12*me2*s35*tt + 16*mm2*s35*tt +&
   12*s15*s35*tt + 6*s25*s35*tt - 4*s35**2*tt + 12*me2*ss*tt + 12*mm2*ss*tt +&
   12*s15*ss*tt + 6*s25*ss*tt - 10*s35*ss*tt - 4*ss**2*tt + 12*me2*tt**2 + 12*mm2*tt**2&
   + 10*s15*tt**2 + 6*s25*tt**2 - 8*s35*tt**2 - 8*ss*tt**2 - 4*tt**3))/(s15*(s15 + s25&
   - s35)*ss*(-s15 - s25 + ss)) - (8*e**6*(-4*me2**3 - 12*me2**2*mm2 - 12*me2*mm2**2 -&
   4*mm2**3 - 4*me2**2*s15 - 16*me2*mm2*s15 - 12*mm2**2*s15 - me2*s15**2 - 9*mm2*s15**2&
   - s15**3 - 4*me2*mm2*s25 - 4*mm2**2*s25 + me2*s15*s25 - 11*mm2*s15*s25 - s15**2*s25&
   + 2*me2*s25**2 - 2*mm2*s25**2 + 4*me2**2*s35 + 12*me2*mm2*s35 + 8*mm2**2*s35 +&
   me2*s15*s35 + 13*mm2*s15*s35 + 2*s15**2*s35 - 3*me2*s25*s35 + 5*mm2*s25*s35 +&
   s15*s25*s35 - 4*mm2*s35**2 - s15*s35**2 + 4*me2**2*ss + 8*me2*mm2*ss + 4*mm2**2*ss +&
   4*me2*s15*ss + 12*mm2*s15*ss + 5*s15**2*ss + 4*mm2*s25*ss + 6*s15*s25*ss +&
   2*s25**2*ss - 4*me2*s35*ss - 8*mm2*s35*ss - 8*s15*s35*ss - 4*s25*s35*ss +&
   3*s35**2*ss - 2*me2*ss**2 - 2*mm2*ss**2 - 6*s15*ss**2 - 4*s25*ss**2 + 4*s35*ss**2 +&
   2*ss**3 + 12*me2**2*tt + 24*me2*mm2*tt + 12*mm2**2*tt + 14*me2*s15*tt +&
   22*mm2*s15*tt + 7*s15**2*tt + 6*me2*s25*tt + 10*mm2*s25*tt + 9*s15*s25*tt +&
   2*s25**2*tt - 12*me2*s35*tt - 16*mm2*s35*tt - 11*s15*s35*tt - 5*s25*s35*tt +&
   4*s35**2*tt - 12*me2*ss*tt - 12*mm2*ss*tt - 14*s15*ss*tt - 8*s25*ss*tt +&
   10*s35*ss*tt + 6*ss**2*tt - 12*me2*tt**2 - 12*mm2*tt**2 - 10*s15*tt**2 - 6*s25*tt**2&
   + 8*s35*tt**2 + 8*ss*tt**2 + 4*tt**3))/(s15*(s15 + s25 - s35)*ss*(-s15 - s25 + ss))&
   + s3m*((4*e**6*s4n*(-2*me2*s35 + 2*me2*ss - 2*mm2*ss + s15*ss))/(s15*(s15 + s25 -&
   s35)*ss*(-s15 - s25 + ss)) - (8*e**6*s3n*(-me2 + mm2 - 2*s15 - s25 + s35 + ss +&
   tt))/(s15*(s15 + s25 - s35)*(-s15 - s25 + ss)) - (4*e**6*s2n*(2*me2**2 + 2*me2*mm2 +&
   3*me2*s15 + 5*mm2*s15 - s15**2 + 2*me2*s25 + 2*mm2*s25 - 2*me2*s35 - 2*mm2*s35 -&
   2*me2*ss - 4*mm2*ss + s15*ss - 4*me2*tt - 2*mm2*tt - 3*s15*tt - 2*s25*tt + 2*s35*tt&
   + 2*ss*tt + 2*tt**2))/(s15*(s15 + s25 - s35)*ss*(-s15 - s25 + ss))) +&
   s4m*((4*e**6*s3n*(2*me2*s35 - 2*me2*ss - 2*mm2*ss + s15*ss))/(s15*(s15 + s25 -&
   s35)*ss*(-s15 - s25 + ss)) - (8*e**6*s4n*(-me2 - mm2 - s15 - s25 + s35 + ss +&
   tt))/(s15*(s15 + s25 - s35)*(-s15 - s25 + ss)) + (4*e**6*s2n*(2*me2**2 + 6*me2*mm2 +&
   4*mm2**2 + me2*s15 + 3*mm2*s15 + 2*s15**2 + 2*mm2*s25 + 2*s15*s25 - 2*me2*s35 -&
   2*mm2*s35 - 2*s15*s35 - 2*me2*ss - 4*mm2*ss - 4*s15*ss - 2*s25*ss + 2*s35*ss +&
   2*ss**2 - 4*me2*tt - 6*mm2*tt - 3*s15*tt - 2*s25*tt + 2*s35*tt + 4*ss*tt +&
   2*tt**2))/(s15*(s15 + s25 - s35)*ss*(-s15 - s25 + ss))) +&
   s1m*((4*e**6*s2n*(4*me2*mm2 + 4*mm2**2 + 6*mm2*s15 - s15**2 + 6*mm2*s25 - s15*s25 -&
   4*mm2*s35 - 6*mm2*ss + s15*ss - 4*mm2*tt))/(s15*(s15 + s25 - s35)*ss*(-s15 - s25 +&
   ss)) + (4*e**6*s4n*(-2*me2**2 - 6*me2*mm2 - 4*mm2**2 - me2*s15 - 5*mm2*s15 - s15**2&
   - 4*mm2*s25 - s15*s25 + 4*me2*s35 + 6*mm2*s35 + 3*s15*s35 + 2*s25*s35 - 2*s35**2 +&
   4*mm2*ss + s15*ss - 2*s35*ss + 4*me2*tt + 6*mm2*tt + 3*s15*tt + 2*s25*tt - 4*s35*tt&
   - 2*ss*tt - 2*tt**2))/(s15*(s15 + s25 - s35)*ss*(-s15 - s25 + ss)) +&
   (4*e**6*s3n*(2*me2**2 + 2*me2*mm2 + 5*me2*s15 + 3*mm2*s15 + 3*s15**2 + 4*me2*s25 +&
   6*s15*s25 + 2*s25**2 - 4*me2*s35 - 2*mm2*s35 - 5*s15*s35 - 4*s25*s35 + 2*s35**2 -&
   4*me2*ss - 6*s15*ss - 4*s25*ss + 4*s35*ss + 2*ss**2 - 4*me2*tt - 2*mm2*tt - 5*s15*tt&
   - 4*s25*tt + 4*s35*tt + 4*ss*tt + 2*tt**2))/(s15*(s15 + s25 - s35)*ss*(-s15 - s25 +&
   ss)))

  m24 = (8*e**6*(4*me2**3 + 12*me2**2*mm2 + 12*me2*mm2**2 + 4*mm2**3 + 4*me2*mm2*s15 +&
   4*mm2**2*s15 - 4*me2**2*s25 + 4*mm2**2*s25 - me2*s15*s25 - mm2*s15*s25 + s15**2*s25&
   - me2*s25**2 - mm2*s25**2 + s15*s25**2 - 4*me2*mm2*s35 - 4*mm2**2*s35 + me2*s15*s35&
   + mm2*s15*s35 + s15**2*s35 + me2*s25*s35 + mm2*s25*s35 - s15*s35**2 - 2*me2*s15*ss -&
   2*mm2*s15*ss - 2*me2*s25*ss - 2*mm2*s25*ss - s15*s25*ss - s15*s35*ss - s25*s35*ss +&
   s35**2*ss + 2*me2*ss**2 + 2*mm2*ss**2 - 12*me2**2*tt - 24*me2*mm2*tt - 12*mm2**2*tt&
   - 6*me2*s15*tt - 10*mm2*s15*tt - 2*s15**2*tt + 2*me2*s25*tt - 6*mm2*s25*tt -&
   s15*s25*tt + s25**2*tt + 4*me2*s35*tt + 8*mm2*s35*tt + s15*s35*tt - s25*s35*tt +&
   4*me2*ss*tt + 4*mm2*ss*tt + 4*s15*ss*tt + 2*s25*ss*tt - 2*s35*ss*tt - 2*ss**2*tt +&
   12*me2*tt**2 + 12*mm2*tt**2 + 6*s15*tt**2 + 2*s25*tt**2 - 4*s35*tt**2 - 4*ss*tt**2 -&
   4*tt**3))/(s25*(s15 + s25 - s35)*ss*(-s15 - s25 + ss)) + (4*e**6*snm*(-4*me2**3 -&
   12*me2**2*mm2 - 12*me2*mm2**2 - 4*mm2**3 - 4*me2*mm2*s15 - 4*mm2**2*s15 +&
   4*me2**2*s25 - 4*mm2**2*s25 + me2*s15*s25 + mm2*s15*s25 - s15**2*s25 + me2*s25**2 +&
   mm2*s25**2 - s15*s25**2 + 4*me2*mm2*s35 + 4*mm2**2*s35 + s15*s25*s35 + s15*s25*ss -&
   s25*s35*ss + 12*me2**2*tt + 24*me2*mm2*tt + 12*mm2**2*tt + 6*me2*s15*tt +&
   10*mm2*s15*tt + 2*s15**2*tt - 2*me2*s25*tt + 6*mm2*s25*tt + s15*s25*tt - s25**2*tt -&
   4*me2*s35*tt - 8*mm2*s35*tt - 2*s15*s35*tt - 4*me2*ss*tt - 4*mm2*ss*tt - 2*s15*ss*tt&
   + 2*s35*ss*tt - 12*me2*tt**2 - 12*mm2*tt**2 - 6*s15*tt**2 - 2*s25*tt**2 +&
   4*s35*tt**2 + 4*ss*tt**2 + 4*tt**3))/(s25*(s15 + s25 - s35)*ss*(-s15 - s25 + ss)) +&
   s3m*((-4*e**6*s4n*(2*me2*s35 - 2*me2*ss - 2*mm2*ss + s25*ss))/(s25*(s15 + s25 -&
   s35)*ss*(-s15 - s25 + ss)) - (8*e**6*s3n*(-me2 - 3*mm2 + s25 + tt))/(s25*(s15 + s25&
   - s35)*(-s15 - s25 + ss)) + (4*e**6*s2n*(-2*me2**2 - 6*me2*mm2 - 4*mm2**2 +&
   2*mm2*s15 + me2*s25 + mm2*s25 - s15*s25 - 2*mm2*ss + s25*ss + 4*me2*tt + 6*mm2*tt -&
   s25*tt - 2*tt**2))/(s25*(s15 + s25 - s35)*ss*(-s15 - s25 + ss))) +&
   s4m*((-4*e**6*s3n*(-2*me2*s35 + 2*me2*ss - 2*mm2*ss + s25*ss))/(s25*(s15 + s25 -&
   s35)*ss*(-s15 - s25 + ss)) + (8*e**6*s4n*(me2 + mm2 - tt))/(s25*(s15 + s25 -&
   s35)*(-s15 - s25 + ss)) - (4*e**6*s2n*(-2*me2**2 - 2*me2*mm2 - 2*mm2*s15 + me2*s25 -&
   mm2*s25 + 2*mm2*ss + 4*me2*tt + 2*mm2*tt + 2*s15*tt + s25*tt - 2*ss*tt -&
   2*tt**2))/(s25*(s15 + s25 - s35)*ss*(-s15 - s25 + ss))) +&
   s1m*((-4*e**6*s2n*(-4*me2*mm2 - 4*mm2**2 + 2*mm2*s15 + 2*mm2*s25 - s15*s25 - s25**2&
   - 2*mm2*ss + s25*ss + 4*mm2*tt))/(s25*(s15 + s25 - s35)*ss*(-s15 - s25 + ss)) +&
   (4*e**6*s4n*(-2*me2**2 - 2*me2*mm2 + me2*s25 + mm2*s25 - s15*s25 - s25**2 +&
   2*me2*s35 + s25*s35 - 2*me2*ss - 2*mm2*ss + s25*ss + 4*me2*tt + 2*mm2*tt + 2*s15*tt&
   + s25*tt - 2*s35*tt - 2*tt**2))/(s25*(s15 + s25 - s35)*ss*(-s15 - s25 + ss)) +&
   (4*e**6*s3n*(2*me2**2 + 6*me2*mm2 + 4*mm2**2 + 2*me2*s15 + 4*mm2*s15 + me2*s25 +&
   5*mm2*s25 - s15*s25 - 2*s25**2 - 2*me2*s35 - 4*mm2*s35 + s25*s35 - 2*me2*ss -&
   6*mm2*ss + 2*s25*ss - 4*me2*tt - 6*mm2*tt - 2*s15*tt - s25*tt + 2*s35*tt + 2*ss*tt +&
   2*tt**2))/(s25*(s15 + s25 - s35)*ss*(-s15 - s25 + ss)))

  m33 = s3m*((-16*e**6*me2*s2n)/(s15**2*(s15 + s25 - ss)) + (16*e**6*me2*s3n*(-2*mm2 + s15 + s25&
   - ss))/(s15**2*(s15 + s25 - ss)**2) + (16*e**6*me2*s4n*(2*me2 + 2*s15 + s25 - 2*s35&
   - ss - 2*tt))/(s15**2*(s15 + s25 - ss)**2)) + s4m*((-16*e**6*me2*s2n)/(s15**2*(s15 +&
   s25 - ss)) + (16*e**6*me2*s4n*(-2*mm2 + s15 + s25 - ss))/(s15**2*(s15 + s25 -&
   ss)**2) - (16*e**6*me2*s3n*(2*me2 + 4*mm2 + 2*s15 + s25 - 2*s35 - ss -&
   2*tt))/(s15**2*(s15 + s25 - ss)**2)) + (32*e**6*me2*snm*(me2**2 + 2*me2*mm2 + mm2**2&
   + me2*s15 + mm2*s15 + s15**2 + s15*s25 - 2*me2*s35 - 2*mm2*s35 - 2*s15*s35 - s25*s35&
   + s35**2 - s15*ss + s35*ss - 2*me2*tt - 2*mm2*tt - 2*s15*tt - s25*tt + 2*s35*tt +&
   ss*tt + tt**2))/(s15**2*(s15 + s25 - ss)**2) - (16*e**6*(4*me2**3 + 8*me2**2*mm2 +&
   4*me2*mm2**2 + 4*me2**2*s15 + 4*me2*mm2*s15 + 7*me2*s15**2 + mm2*s15**2 + s15**3 +&
   9*me2*s15*s25 - mm2*s15*s25 + 2*s15**2*s25 + 2*me2*s25**2 + s15*s25**2 -&
   8*me2**2*s35 - 8*me2*mm2*s35 - 10*me2*s15*s35 - 2*mm2*s15*s35 - 3*s15**2*s35 -&
   4*me2*s25*s35 - 2*s15*s25*s35 + 4*me2*s35**2 + 2*s15*s35**2 - 8*me2*s15*ss -&
   s15**2*ss - 4*me2*s25*ss - s15*s25*ss + 4*me2*s35*ss + s15*s35*ss + 2*me2*ss**2 -&
   8*me2**2*tt - 8*me2*mm2*tt - 8*me2*s15*tt - s15**2*tt - 4*me2*s25*tt - s15*s25*tt +&
   8*me2*s35*tt + 2*s15*s35*tt + 4*me2*ss*tt + 4*me2*tt**2))/(s15**2*(s15 + s25 -&
   ss)**2)

  m34 = (-8*e**6*(8*me2**3 + 16*me2**2*mm2 + 8*me2*mm2**2 + 8*me2**2*s15 + 12*me2*mm2*s15 +&
   4*mm2**2*s15 + 11*me2*s15**2 + 7*mm2*s15**2 + 3*s15**3 + 4*me2*mm2*s25 +&
   4*mm2**2*s25 + 12*me2*s15*s25 + 8*mm2*s15*s25 + 4*s15**2*s25 + me2*s25**2 +&
   mm2*s25**2 + s15*s25**2 - 8*me2**2*s35 - 8*me2*mm2*s35 - 10*me2*s15*s35 -&
   6*mm2*s15*s35 - 5*s15**2*s35 - 2*me2*s25*s35 - 2*mm2*s25*s35 - 3*s15*s25*s35 +&
   2*s15*s35**2 - 4*me2**2*ss - 8*me2*mm2*ss - 4*mm2**2*ss - 14*me2*s15*ss -&
   6*mm2*s15*ss - 7*s15**2*ss - 6*me2*s25*ss - 2*mm2*s25*ss - 7*s15*s25*ss -&
   2*s25**2*ss + 8*me2*s35*ss + 4*mm2*s35*ss + 7*s15*s35*ss + 3*s25*s35*ss -&
   2*s35**2*ss + 4*me2*ss**2 + 6*s15*ss**2 + 4*s25*ss**2 - 2*s35*ss**2 - 2*ss**3 -&
   16*me2**2*tt - 16*me2*mm2*tt - 20*me2*s15*tt - 8*mm2*s15*tt - 7*s15**2*tt -&
   12*me2*s25*tt - 8*mm2*s25*tt - 8*s15*s25*tt - s25**2*tt + 8*me2*s35*tt +&
   6*s15*s35*tt + 2*s25*s35*tt + 16*me2*ss*tt + 8*mm2*ss*tt + 10*s15*ss*tt +&
   6*s25*ss*tt - 4*s35*ss*tt - 4*ss**2*tt + 8*me2*tt**2 + 4*s15*tt**2 + 4*s25*tt**2 -&
   4*ss*tt**2))/(s15*s25*(s15 + s25 - ss)**2) + (8*e**6*snm*(4*me2**3 + 8*me2**2*mm2 +&
   4*me2*mm2**2 + 4*me2**2*s15 + 6*me2*mm2*s15 + 2*mm2**2*s15 + 3*me2*s15**2 +&
   3*mm2*s15**2 + s15**3 + 2*me2*mm2*s25 + 2*mm2**2*s25 + 2*me2*s15*s25 + 6*mm2*s15*s25&
   + s15**2*s25 - me2*s25**2 + mm2*s25**2 - 4*me2**2*s35 - 4*me2*mm2*s35 -&
   4*me2*s15*s35 - 2*mm2*s15*s35 - s15**2*s35 - 2*me2*s25*s35 - 2*mm2*s25*s35 -&
   s15*s25*s35 - 2*me2**2*ss - 4*me2*mm2*ss - 2*mm2**2*ss - 3*me2*s15*ss - 3*mm2*s15*ss&
   - 2*s15**2*ss + me2*s25*ss - mm2*s25*ss - s15*s25*ss + 4*me2*s35*ss + 2*mm2*s35*ss +&
   2*s15*s35*ss + s25*s35*ss + s15*ss**2 - s35*ss**2 - 8*me2**2*tt - 8*me2*mm2*tt -&
   10*me2*s15*tt - 4*mm2*s15*tt - 3*s15**2*tt - 6*me2*s25*tt - 4*mm2*s25*tt -&
   4*s15*s25*tt - s25**2*tt + 4*me2*s35*tt + 2*s15*s35*tt + 2*s25*s35*tt + 8*me2*ss*tt&
   + 4*mm2*ss*tt + 5*s15*ss*tt + 3*s25*ss*tt - 2*s35*ss*tt - 2*ss**2*tt + 4*me2*tt**2 +&
   2*s15*tt**2 + 2*s25*tt**2 - 2*ss*tt**2))/(s15*s25*(s15 + s25 - ss)**2) +&
   s3m*((-8*e**6*s3n*(-2*mm2 + s15 + s25 - ss)*(-2*me2 + ss))/(s15*s25*(s15 + s25 -&
   ss)**2) + (8*e**6*s4n*(4*me2**2 + 3*me2*s15 + me2*s25 - 2*me2*s35 - 2*me2*ss +&
   2*mm2*ss - 4*me2*tt))/(s15*s25*(s15 + s25 - ss)**2) + (8*e**6*s2n*(3*mm2*s15 +&
   mm2*s25 - 3*mm2*ss - s15*tt - s25*tt + ss*tt))/(s15*s25*(s15 + s25 - ss)**2)) +&
   s4m*((-8*e**6*s4n*(-2*mm2 + s15 + s25 - ss)*(-2*me2 + ss))/(s15*s25*(s15 + s25 -&
   ss)**2) - (8*e**6*s3n*(4*me2**2 + 8*me2*mm2 + 3*me2*s15 + me2*s25 - 2*me2*s35 -&
   2*me2*ss - 2*mm2*ss - 4*me2*tt))/(s15*s25*(s15 + s25 - ss)**2) -&
   (8*e**6*s2n*(2*me2*s15 - mm2*s15 + s15**2 + 2*me2*s25 + mm2*s25 + s15*s25 - 2*me2*ss&
   + mm2*ss - 2*s15*ss - s25*ss + ss**2 - s15*tt - s25*tt + ss*tt))/(s15*s25*(s15 + s25&
   - ss)**2)) + s1m*((-32*e**6*mm2*s2n)/(s15*s25*(s15 + s25 - ss)) +&
   (8*e**6*s4n*(mm2*s15 + s15**2 + 3*mm2*s25 + s15*s25 - s15*s35 - s25*s35 - 3*mm2*ss -&
   s15*ss + s35*ss - s15*tt - s25*tt + ss*tt))/(s15*s25*(s15 + s25 - ss)**2) -&
   (8*e**6*s3n*(2*me2*s15 + mm2*s15 + s15**2 + 2*me2*s25 - mm2*s25 + 2*s15*s25 + s25**2&
   - s15*s35 - s25*s35 - 2*me2*ss + mm2*ss - 2*s15*ss - 2*s25*ss + s35*ss + ss**2 -&
   s15*tt - s25*tt + ss*tt))/(s15*s25*(s15 + s25 - ss)**2))

  m44 = s1m*((16*e**6*me2*s3n)/(s25**2*(-s15 - s25 + ss)) + (16*e**6*me2*s4n)/(s25**2*(-s15 -&
   s25 + ss))) + (32*e**6*me2*snm*(me2**2 + 2*me2*mm2 + mm2**2 - me2*s25 - mm2*s25 -&
   2*me2*tt - 2*mm2*tt - s15*tt + ss*tt + tt**2))/(s25**2*(-s15 - s25 + ss)**2) -&
   (16*e**6*(4*me2**3 + 8*me2**2*mm2 + 4*me2*mm2**2 + 2*me2*s15**2 - 4*me2**2*s25 -&
   4*me2*mm2*s25 + 3*me2*s15*s25 - 3*mm2*s15*s25 + me2*s25**2 - mm2*s25**2 +&
   2*me2*s25*s35 + 2*mm2*s25*s35 + s15*s25*s35 - 4*me2*s15*ss - 4*me2*s25*ss -&
   s25*s35*ss + 2*me2*ss**2 - 8*me2**2*tt - 8*me2*mm2*tt - 4*me2*s15*tt + s15*s25*tt +&
   s25**2*tt - 2*s25*s35*tt + 4*me2*ss*tt + 4*me2*tt**2))/(s25**2*(-s15 - s25 + ss)**2)&
   + s3m*((-16*e**6*me2*s3n*(2*mm2 - s15 - s25 + ss))/(s25**2*(-s15 - s25 + ss)**2) -&
   (16*e**6*me2*s4n*(-2*me2 - s15 + ss + 2*tt))/(s25**2*(-s15 - s25 + ss)**2)) +&
   s4m*((-16*e**6*me2*s4n*(2*mm2 - s15 - s25 + ss))/(s25**2*(-s15 - s25 + ss)**2) +&
   (16*e**6*me2*s3n*(-2*me2 - 4*mm2 - s15 + ss + 2*tt))/(s25**2*(-s15 - s25 + ss)**2))

  pepe2mmg = (m11+2*m12+2*m13+2*m14+m22+2*m23+2*m24+m33+2*m34+m44)*0.25

  END FUNCTION


  FUNCTION PEPEZMMGX(p1,pol1,p2,pol2,p3,p4,p5)
    !! e-(p1) e+(p2) -> Z -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons, expanded
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),pol1(4),pol2(4)
  real (kind=prec) :: ss,tt, me2, mm2, s2n, s3n, s4n, s1m, s3m, s4m, snm
  real (kind=prec) :: me1, mm1, e
  real (kind=prec) :: pepezmmgx
  real (kind=prec) :: s15, s35, s25
  real (kind=prec) :: m2, m5, m7, m9, m12, m14, m16, m18, m20, m23, m25, m27, m29, m31, m33, m35

  ss = sq(p1+p2); tt = sq(p1-p3); s15 = s(p1,p5); s35 = s(p3,p5); s25 = s(p2,p5)
  me2=sq(p1); mm2=sq(p4); me1 = sqrt(me2); mm1 = sqrt(mm2)
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)
  e = sqrt(4*pi*alpha)

  m2 = s3m*((e**6*s4n*(2*me2 + ss*(1 - 4*sW2)**2))/(2.*s35*ss*(-1 + sW2)*sW2) + (e**6*me1*(-1 +&
   4*sW2)*(me2 + mm2 + s15 + s35 - 2*ss - tt))/(s35*ss*(-1 + sW2)*sW2) + (e**6*s2n*(1 -&
   4*sW2)**2*(me2 + mm2 + s15 - ss - tt))/(2.*s35*ss*(-1 + sW2)*sW2)) +&
   (e**6*me1*s3n*(-1 + 4*sW2)*(me2 + mm2 + s15 - ss - tt))/(s35*ss*(-1 + sW2)*sW2) +&
   (e**6*me1*s2n*(-1 + 4*sW2)*(6*mm2**2 + me2*(2*mm2 - s35) + mm2*(2*s15 + s35 - 2*ss -&
   2*tt) + s35*(-s15 + ss + tt)))/(s35**2*ss*(-1 + sW2)*sW2) - (e**6*me1*s4n*(-1 +&
   4*sW2)*(4*mm2**2 + me2*(4*mm2 - s35) + s35*(2*s25 + ss + tt) - mm2*(-4*s15 + 5*s35 +&
   6*ss + 4*tt)))/(s35**2*ss*(-1 + sW2)*sW2) - (2*e**6*(2*me2**2*mm2*(1 - 4*sW2)**2 +&
   2*mm2**3*(1 - 4*sW2)**2 + me2*(4*mm2**2*(1 - 4*sW2)**2 + s35*(s35*(1 - 4*sW2)**2 +&
   4*s25*sW2 - 8*s25*sW2**2 + s15*(-1 + 12*sW2 - 24*sW2**2)) - 2*mm2*(ss - (1 -&
   4*sW2)**2*(2*s15 - s35 - 2*tt))) - 2*mm2**2*(ss - (1 - 4*sW2)**2*(2*s15 - s35 -&
   2*tt)) + s35*(s15**2*(4*sW2 - 8*sW2**2) + s15*s25*(1 - 4*sW2 + 8*sW2**2) - s25*ss*(1&
   - 4*sW2 + 8*sW2**2) + s25*(-1 + 4*sW2 - 8*sW2**2)*tt + 4*s15*sW2*(-1 + 2*sW2)*(s35 +&
   tt)) + mm2*(s25*s35 + 2*s15**2*(1 - 4*sW2)**2 - 4*s25*s35*sW2 + 8*s25*s35*sW2**2 +&
   2*ss**2*(1 - 4*sW2 + 8*sW2**2) + 2*s35*tt - 16*s35*sW2*tt + 32*s35*sW2**2*tt +&
   2*tt**2 - 16*sW2*tt**2 + 32*sW2**2*tt**2 - 2*s15*(s35*(1 - 10*sW2 + 20*sW2**2) +&
   2*(1 - 4*sW2)**2*tt) + ss*(-4*s15*(1 - 4*sW2 + 8*sW2**2) + s35*(3 - 16*sW2 +&
   32*sW2**2) + 4*(1 - 4*sW2 + 8*sW2**2)*tt))))/(s35**2*ss*(-1 + sW2)*sW2) +&
   s1m*(-(e**6*s2n*(4*mm2**2 + s35*(s15 + s25 - ss))*(1 - 4*sW2)**2)/(2.*s35**2*ss*(-1&
   + sW2)*sW2) - (e**6*s3n*(1 - 4*sW2)**2*(me2 + mm2 + s15 - s35 - tt))/(2.*s35*ss*(-1&
   + sW2)*sW2) - (e**6*me1*(-1 + 4*sW2)*(2*mm2**2 + me2*(-2*mm2 + s35) + s35*(s15 + s35&
   - 2*ss - tt) + mm2*(-2*s15 + s35 + 4*ss + 2*tt)))/(s35**2*ss*(-1 + sW2)*sW2) +&
   (e**6*s4n*(me2*(s35*(-3 + 8*sW2 - 16*sW2**2) + 8*mm2*(1 - 4*sW2 + 8*sW2**2)) + (1 -&
   4*sW2)**2*(4*mm2**2 + mm2*(4*s15 - 5*s35 - 4*tt) + s35*(-s15 + s25 + s35 - ss +&
   tt))))/(2.*s35**2*ss*(-1 + sW2)*sW2)) + (e**6*snm*(1 - 4*sW2)**2*(4*me2**2*mm2 +&
   4*mm2**3 + mm2*(4*s15**2 - 5*s15*s35 + s25*s35 - 8*s15*tt + 4*s35*tt + 4*tt**2 +&
   4*ss*(-s15 + s35 + tt)) + s35*(-s15**2 + (s15 - s35)*ss - s25*tt + s15*(s25 + s35 +&
   tt)) + mm2**2*(8*s15 - 4*(s35 + 2*tt)) + me2*(8*mm2**2 + s35*(-3*s15 - s25 + 2*s35)&
   + mm2*(8*s15 - 4*(s35 + 2*tt)))))/(2.*s35**2*ss*(-1 + sW2)*sW2) +&
   s4m*((e**6*s3n*(-2*me2 + ss*(1 - 4*sW2)**2))/(2.*s35*ss*(-1 + sW2)*sW2) -&
   (e**6*(2*mm2 - s35)*s4n*(1 - 4*sW2)**2)/(s35**2*(-1 + sW2)*sW2) + (e**6*me1*(-1 +&
   4*sW2)*(-4*mm2**2 + me2*(-4*mm2 + s35) + s35*(s15 - s25 + s35 - 2*ss - tt) +&
   mm2*(-4*s15 + s35 + 6*ss + 4*tt)))/(s35**2*ss*(-1 + sW2)*sW2) +&
   (e**6*s2n*(me2*(-8*mm2*(1 - 4*sW2 + 8*sW2**2) + s35*(3 - 8*sW2 + 16*sW2**2)) - (1 -&
   4*sW2)**2*(4*mm2**2 + s35*(-2*s15 + 2*ss + tt) - mm2*(-4*s15 + s35 + 4*ss +&
   4*tt))))/(2.*s35**2*ss*(-1 + sW2)*sW2))

  m5 = -((e**6*me1*s3n*(-1 + 4*sW2)*(-(s15*s35) - s25*s35 + s35**2 + 2*s15*ss + s25*ss -&
   2*ss**2 + me2*(-s35 + 2*ss) + mm2*(s35 + 6*ss) + s35*tt - 2*ss*tt))/((s15 + s25 -&
   s35)*s35*ss*(-1 + sW2)*sW2)) + (e**6*me1*s2n*(-1 + 4*sW2)*(6*mm2**2 + me2*(2*mm2 -&
   s15 - s25 + ss) + mm2*(-s15 - s25 + 2*s35 + ss - 2*tt) - (-s15 - s25 + ss)*(-s15 -&
   s25 + s35 + ss + tt)))/((s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2) + (e**6*me1*s4n*(-1&
   + 4*sW2)*(-4*mm2**2 + s15*s25 + s25**2 + s15*s35 - s35**2 + me2*(-4*mm2 + s35) +&
   s15*ss - s35*ss - ss**2 - s35*tt + mm2*(-2*s15 - 2*s25 + 3*s35 + 2*ss +&
   4*tt)))/((s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2) + s1m*(-(e**6*s2n*(4*mm2**2*(1 -&
   4*sW2)**2 + (-s15 - s25 + ss)*(s25 - 4*s15*sW2 - 4*s25*sW2 + 8*s15*sW2**2 +&
   8*s25*sW2**2) - 4*mm2*(ss*(1 - 4*sW2)**2 + 4*s25*sW2 - 8*s25*sW2**2 + s15*(-1 +&
   4*sW2 - 8*sW2**2))))/(2.*(s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2) + (e**6*me1*(-1 +&
   4*sW2)*(-2*mm2**2 + me2*(2*mm2 - s15 - s25 + ss) + mm2*(-3*s15 - 3*s25 + 3*ss -&
   2*tt) + (-s15 - s25 + ss)*(s15 + s25 - 2*s35 - tt)))/((s15 + s25 - s35)*s35*ss*(-1 +&
   sW2)*sW2) - (e**6*s3n*(2*me2**2 + 2*s15**2 + s15*s25 - 2*s15*s35 + s25*s35 -&
   4*s15*ss - 2*s25*ss + 2*s35*ss + 2*ss**2 + 4*s15**2*sW2 + 4*s15*s25*sW2 -&
   12*s15*s35*sW2 - 12*s25*s35*sW2 + 8*s35**2*sW2 + 8*s15*ss*sW2 + 8*s25*ss*sW2 -&
   8*ss**2*sW2 - 8*s15**2*sW2**2 - 8*s15*s25*sW2**2 + 24*s15*s35*sW2**2 +&
   24*s25*s35*sW2**2 - 16*s35**2*sW2**2 - 16*s15*ss*sW2**2 - 16*s25*ss*sW2**2 +&
   16*ss**2*sW2**2 + 4*mm2**2*(1 - 4*sW2 + 8*sW2**2) + me2*(4*s15 + s25 - 2*s35 +&
   4*s15*sW2 + 4*s25*sW2 - 8*s35*sW2 - 8*s15*sW2**2 - 8*s25*sW2**2 + 16*s35*sW2**2 -&
   4*ss*(1 - 2*sW2 + 4*sW2**2) + 2*mm2*(3 - 8*sW2 + 16*sW2**2) - 4*tt) - 4*s15*tt -&
   s25*tt + 2*s35*tt + 4*ss*tt - 4*s15*sW2*tt - 4*s25*sW2*tt + 8*s35*sW2*tt -&
   8*ss*sW2*tt + 8*s15*sW2**2*tt + 8*s25*sW2**2*tt - 16*s35*sW2**2*tt + 16*ss*sW2**2*tt&
   + 2*tt**2 + mm2*(s25 - 4*s35 + 4*s25*sW2 + 8*s35*sW2 - 8*s25*sW2**2 - 16*s35*sW2**2&
   + 6*s15*(1 - 2*sW2 + 4*sW2**2) - 4*ss*(1 - 2*sW2 + 4*sW2**2) - 6*tt + 16*sW2*tt -&
   32*sW2**2*tt)))/(2.*(s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2) + (e**6*s4n*(-2*me2**2&
   - s15*s25 - s25**2 + s25*s35 + s25*ss - 4*s15**2*sW2 + 4*s25**2*sW2 + 12*s15*s35*sW2&
   + 4*s25*s35*sW2 - 8*s35**2*sW2 + 4*s15*ss*sW2 - 4*s25*ss*sW2 - 8*s35*ss*sW2 +&
   8*s15**2*sW2**2 - 8*s25**2*sW2**2 - 24*s15*s35*sW2**2 - 8*s25*s35*sW2**2 +&
   16*s35**2*sW2**2 - 8*s15*ss*sW2**2 + 8*s25*ss*sW2**2 + 16*s35*ss*sW2**2 +&
   16*mm2**2*sW2*(-1 + 2*sW2) + 2*s15*tt + s25*tt - 2*s35*tt + 4*s15*sW2*tt +&
   4*s25*sW2*tt - 8*s35*sW2*tt - 8*ss*sW2*tt - 8*s15*sW2**2*tt - 8*s25*sW2**2*tt +&
   16*s35*sW2**2*tt + 16*ss*sW2**2*tt - 2*tt**2 + me2*(s25 + 2*s35 + 2*mm2*(1 -&
   4*sW2)**2 - 4*s15*sW2 - 4*s25*sW2 + 8*s35*sW2 + 8*s15*sW2**2 + 8*s25*sW2**2 -&
   16*s35*sW2**2 - 2*ss*(1 - 4*sW2 + 8*sW2**2) + 4*tt) + mm2*(s25 - 20*s15*sW2 -&
   20*s25*sW2 + 24*s35*sW2 + 40*s15*sW2**2 + 40*s25*sW2**2 - 48*s35*sW2**2 + ss*(-2 +&
   24*sW2 - 48*sW2**2) + 2*tt + 16*sW2*tt - 32*sW2**2*tt)))/(2.*(s15 + s25 -&
   s35)*s35*ss*(-1 + sW2)*sW2)) + s3m*((e**6*s4n*(-2*me2*s35 + 2*mm2*ss*(1 - 4*sW2)**2&
   - ss*(s25 - 4*s15*sW2 - 4*s25*sW2 + 8*s15*sW2**2 + 8*s25*sW2**2)))/(2.*(s15 + s25 -&
   s35)*s35*ss*(-1 + sW2)*sW2) - (e**6*s3n*(me2 + mm2 + s15 - ss - 4*s35*sW2 + 4*ss*sW2&
   + 8*s35*sW2**2 - 8*ss*sW2**2 - tt))/((s15 + s25 - s35)*s35*(-1 + sW2)*sW2) -&
   (e**6*me1*(-1 + 4*sW2)*(s15*s35 + s25*s35 + 2*s15*ss + s25*ss - s35*ss - 2*ss**2 +&
   me2*(-s35 + 2*ss) + mm2*(-3*s35 + 6*ss) + s35*tt - 2*ss*tt))/((s15 + s25 -&
   s35)*s35*ss*(-1 + sW2)*sW2) + (e**6*s2n*(2*me2**2 - s15*s25 + s25*ss + 4*s15**2*sW2&
   + 4*s15*s25*sW2 - 4*s15*ss*sW2 - 4*s25*ss*sW2 - 8*s15**2*sW2**2 - 8*s15*s25*sW2**2 +&
   8*s15*ss*sW2**2 + 8*s25*ss*sW2**2 + 4*mm2**2*(1 - 4*sW2 + 8*sW2**2) + me2*(2*s15 -&
   s25 + 4*s15*sW2 + 4*s25*sW2 - 8*s35*sW2 - 8*s15*sW2**2 - 8*s25*sW2**2 +&
   16*s35*sW2**2 - 2*ss*(1 - 4*sW2 + 8*sW2**2) + 2*mm2*(3 - 8*sW2 + 16*sW2**2) - 4*tt)&
   - 2*s15*tt + s25*tt + 2*ss*tt - 4*s15*sW2*tt - 4*s25*sW2*tt + 8*s35*sW2*tt -&
   8*ss*sW2*tt + 8*s15*sW2**2*tt + 8*s25*sW2**2*tt - 16*s35*sW2**2*tt + 16*ss*sW2**2*tt&
   + 2*tt**2 - mm2*(s25 - 4*s25*sW2 + 8*s35*sW2 + 8*s25*sW2**2 - 16*s35*sW2**2 -&
   4*s15*(1 - 3*sW2 + 6*sW2**2) + 4*ss*(1 - 6*sW2 + 12*sW2**2) + 6*tt - 16*sW2*tt +&
   32*sW2**2*tt)))/(2.*(s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2)) +&
   s4m*((e**6*s3n*(2*me2*s35 + 2*mm2*ss*(1 - 4*sW2)**2 - ss*(s25 - 4*s15*sW2 -&
   4*s25*sW2 + 8*s15*sW2**2 + 8*s25*sW2**2)))/(2.*(s15 + s25 - s35)*s35*ss*(-1 +&
   sW2)*sW2) + (e**6*s4n*(me2 + mm2 + 4*s15*sW2 + 4*s25*sW2 - 4*s35*sW2 - 4*ss*sW2 -&
   8*s15*sW2**2 - 8*s25*sW2**2 + 8*s35*sW2**2 + 8*ss*sW2**2 - tt))/((s15 + s25 -&
   s35)*s35*(-1 + sW2)*sW2) + (e**6*me1*(-1 + 4*sW2)*(-4*mm2**2 + s15*s25 + s25**2 -&
   s25*s35 + me2*(-4*mm2 + s35) + s15*ss - ss**2 - s35*tt + mm2*(-2*s15 - 2*s25 + 3*s35&
   + 2*ss + 4*tt)))/((s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2) + (e**6*s2n*(2*me2**2 +&
   8*s15**2*sW2 + 8*s15*s25*sW2 - 8*s15*s35*sW2 - 16*s15*ss*sW2 - 8*s25*ss*sW2 +&
   8*s35*ss*sW2 + 8*ss**2*sW2 - 16*s15**2*sW2**2 - 16*s15*s25*sW2**2 +&
   16*s15*s35*sW2**2 + 32*s15*ss*sW2**2 + 16*s25*ss*sW2**2 - 16*s35*ss*sW2**2 -&
   16*ss**2*sW2**2 + mm2**2*(16*sW2 - 32*sW2**2) - 2*s15*tt - s25*tt + 2*ss*tt -&
   4*s15*sW2*tt - 4*s25*sW2*tt + 8*s35*sW2*tt + 8*ss*sW2*tt + 8*s15*sW2**2*tt +&
   8*s25*sW2**2*tt - 16*s35*sW2**2*tt - 16*ss*sW2**2*tt + 2*tt**2 + mm2*(s25 +&
   4*s25*sW2 - 8*s35*sW2 - 8*s25*sW2**2 + 16*s35*sW2**2 + s15*(2 + 4*sW2 - 8*sW2**2) +&
   2*ss*(-1 - 4*sW2 + 8*sW2**2) - 2*tt - 16*sW2*tt + 32*sW2**2*tt) - me2*(2*mm2*(1 -&
   4*sW2)**2 + s25*(1 - 4*sW2 + 8*sW2**2) + 4*(-(s15*sW2) + 2*s35*sW2 + 2*ss*sW2 +&
   2*s15*sW2**2 - 4*s35*sW2**2 - 4*ss*sW2**2 + tt))))/(2.*(s15 + s25 - s35)*s35*ss*(-1&
   + sW2)*sW2)) + (e**6*snm*(-(s15**2*s25) - s15*s25**2 + s15*s25*s35 + s15*s25*ss -&
   s25*s35*ss + 4*mm2**3*(1 - 4*sW2)**2 + 2*me2**2*(2*mm2 - ss)*(1 - 4*sW2)**2 -&
   4*s15**3*sW2 + 4*s15*s25**2*sW2 + 12*s15**2*s35*sW2 + 4*s15*s25*s35*sW2 -&
   8*s15*s35**2*sW2 + 12*s15**2*ss*sW2 + 4*s15*s25*ss*sW2 - 20*s15*s35*ss*sW2 -&
   4*s25*s35*ss*sW2 + 8*s35**2*ss*sW2 - 8*s15*ss**2*sW2 + 8*s35*ss**2*sW2 +&
   8*s15**3*sW2**2 - 8*s15*s25**2*sW2**2 - 24*s15**2*s35*sW2**2 - 8*s15*s25*s35*sW2**2&
   + 16*s15*s35**2*sW2**2 - 24*s15**2*ss*sW2**2 - 8*s15*s25*ss*sW2**2 +&
   40*s15*s35*ss*sW2**2 + 8*s25*s35*ss*sW2**2 - 16*s35**2*ss*sW2**2 +&
   16*s15*ss**2*sW2**2 - 16*s35*ss**2*sW2**2 + s15*s25*tt + s25**2*tt - 2*s25*s35*tt +&
   2*s15*ss*tt - 2*ss**2*tt + 4*s15**2*sW2*tt - 4*s25**2*sW2*tt - 8*s15*s35*sW2*tt +&
   8*s25*s35*sW2*tt - 24*s15*ss*sW2*tt - 8*s25*ss*sW2*tt + 16*s35*ss*sW2*tt +&
   16*ss**2*sW2*tt - 8*s15**2*sW2**2*tt + 8*s25**2*sW2**2*tt + 16*s15*s35*sW2**2*tt -&
   16*s25*s35*sW2**2*tt + 48*s15*ss*sW2**2*tt + 16*s25*ss*sW2**2*tt -&
   32*s35*ss*sW2**2*tt - 32*ss**2*sW2**2*tt - 2*ss*tt**2 + 16*ss*sW2*tt**2 -&
   32*ss*sW2**2*tt**2 - 2*mm2**2*(1 - 4*sW2)**2*(-3*s15 - s25 + 2*s35 + ss + 4*tt) +&
   mm2*(-s25**2 + 2*s25*s35 + 4*s25**2*sW2 - 8*s25*s35*sW2 - 8*s25**2*sW2**2 +&
   16*s25*s35*sW2**2 + s15**2*(2 - 20*sW2 + 40*sW2**2) - 2*s25*tt + 4*s35*tt +&
   16*s25*sW2*tt - 32*s35*sW2*tt - 32*s25*sW2**2*tt + 64*s35*sW2**2*tt + 4*tt**2 -&
   32*sW2*tt**2 + 64*sW2**2*tt**2 + s15*(-2*s35*(1 - 12*sW2 + 24*sW2**2) + s25*(1 -&
   16*sW2 + 32*sW2**2) - 6*(1 - 4*sW2)**2*tt) - 2*ss*(s15 + s25 - s35 - 20*s15*sW2 -&
   4*s25*sW2 + 16*s35*sW2 + 40*s15*sW2**2 + 8*s25*sW2**2 - 32*s35*sW2**2 - 4*tt +&
   32*sW2*tt - 64*sW2**2*tt)) + me2*(8*mm2**2*(1 - 4*sW2)**2 + (s15 + s25 -&
   2*s35)*(4*s15*sW2*(-1 + 2*sW2) + s25*(-1 + 4*sW2 - 8*sW2**2)) - 2*mm2*(1 -&
   4*sW2)**2*(-3*s15 - s25 + 2*s35 + 2*ss + 4*tt) + 2*ss*(s25*(1 - 4*sW2 + 8*sW2**2) +&
   2*(2*s15*sW2 - 4*s35*sW2 - 4*s15*sW2**2 + 8*s35*sW2**2 + tt - 8*sW2*tt +&
   16*sW2**2*tt)))))/(2.*(s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2) + (e**6*(s15**2*s25 +&
   s15*s25**2 + s15**2*s35 - s15*s35**2 + 2*s15**2*ss + s15*s25*ss - 3*s15*s35*ss -&
   s25*s35*ss + s35**2*ss - 4*s15*ss**2 - 2*s25*ss**2 + 2*s35*ss**2 + 2*ss**3 -&
   4*mm2**3*(1 - 4*sW2)**2 - 2*me2**2*(2*mm2 - ss)*(1 - 4*sW2)**2 + 4*s15**3*sW2 -&
   4*s15*s25**2*sW2 - 12*s15**2*s35*sW2 - 4*s15*s25*s35*sW2 + 8*s15*s35**2*sW2 -&
   12*s15**2*ss*sW2 - 4*s15*s25*ss*sW2 + 20*s15*s35*ss*sW2 + 4*s25*s35*ss*sW2 -&
   8*s35**2*ss*sW2 + 16*s15*ss**2*sW2 + 8*s25*ss**2*sW2 - 8*s35*ss**2*sW2 - 8*ss**3*sW2&
   - 8*s15**3*sW2**2 + 8*s15*s25**2*sW2**2 + 24*s15**2*s35*sW2**2 +&
   8*s15*s25*s35*sW2**2 - 16*s15*s35**2*sW2**2 + 24*s15**2*ss*sW2**2 +&
   8*s15*s25*ss*sW2**2 - 40*s15*s35*ss*sW2**2 - 8*s25*s35*ss*sW2**2 +&
   16*s35**2*ss*sW2**2 - 32*s15*ss**2*sW2**2 - 16*s25*ss**2*sW2**2 +&
   16*s35*ss**2*sW2**2 + 16*ss**3*sW2**2 - s15*s25*tt - s25**2*tt - s15*s35*tt +&
   s25*s35*tt - 4*s15*ss*tt - 2*s25*ss*tt + 2*s35*ss*tt + 4*ss**2*tt - 4*s15**2*sW2*tt&
   + 4*s25**2*sW2*tt + 8*s15*s35*sW2*tt - 8*s25*s35*sW2*tt + 24*s15*ss*sW2*tt +&
   8*s25*ss*sW2*tt - 16*s35*ss*sW2*tt - 16*ss**2*sW2*tt + 8*s15**2*sW2**2*tt -&
   8*s25**2*sW2**2*tt - 16*s15*s35*sW2**2*tt + 16*s25*s35*sW2**2*tt -&
   48*s15*ss*sW2**2*tt - 16*s25*ss*sW2**2*tt + 32*s35*ss*sW2**2*tt + 32*ss**2*sW2**2*tt&
   + 2*ss*tt**2 - 16*ss*sW2*tt**2 + 32*ss*sW2**2*tt**2 + 2*mm2**2*(ss*(3 - 8*sW2 +&
   16*sW2**2) - (1 - 4*sW2)**2*(3*s15 + s25 - 2*(s35 + 2*tt))) - mm2*(-s25**2 + s25*s35&
   + 4*s25**2*sW2 - 8*s25*s35*sW2 - 8*s25**2*sW2**2 + 16*s25*s35*sW2**2 + 2*ss**2*(3 -&
   8*sW2 + 16*sW2**2) + s15*s25*(1 - 16*sW2 + 32*sW2**2) + s15**2*(2 - 20*sW2 +&
   40*sW2**2) - 2*s25*tt + 4*s35*tt + 16*s25*sW2*tt - 32*s35*sW2*tt - 32*s25*sW2**2*tt&
   + 64*s35*sW2**2*tt + 4*tt**2 - 32*sW2*tt**2 + 64*sW2**2*tt**2 - 3*s15*(1 -&
   4*sW2)**2*(s35 + 2*tt) - 2*ss*(s25*(2 - 4*sW2 + 8*sW2**2) + 4*s15*(1 - 5*sW2 +&
   10*sW2**2) - (3 - 16*sW2 + 32*sW2**2)*(s35 + 2*tt))) + me2*(s15*s25 + s25**2 +&
   s15*s35 - s25*s35 - 2*ss**2 - 8*mm2**2*(1 - 4*sW2)**2 + 4*s15**2*sW2 - 4*s25**2*sW2&
   - 8*s15*s35*sW2 + 8*s25*s35*sW2 - 8*s15**2*sW2**2 + 8*s25**2*sW2**2 +&
   16*s15*s35*sW2**2 - 16*s25*s35*sW2**2 + 2*ss*(-(s35*(1 - 4*sW2)**2) + s15*(1 - 4*sW2&
   + 8*sW2**2) - 2*(s25*(-2*sW2 + 4*sW2**2) + (1 - 4*sW2)**2*tt)) + 2*mm2*(4*ss*(1 -&
   4*sW2 + 8*sW2**2) - (1 - 4*sW2)**2*(3*s15 + s25 - 2*(s35 + 2*tt))))))/((s15 + s25 -&
   s35)*s35*ss*(-1 + sW2)*sW2)

  m7 =  (e**6*me1*s4n*(-1 + 4*sW2)*(s15*s25 + s25**2 + s15*s35 - s35**2 + 2*s15*ss + s25*ss -&
   s35*ss - 2*ss**2 + me2*(-s15 - s25 + s35 + 2*ss) + 3*mm2*(-s15 - s25 + s35 + 2*ss) +&
   s15*tt + s25*tt - s35*tt - 2*ss*tt))/((s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2) -&
   (e**6*me1*s2n*(-1 + 4*sW2)*(-2*mm2**2 + me2*(2*mm2 - s15 - s25 + ss) + mm2*(-s15 -&
   3*s25 - 2*s35 + 3*ss - 2*tt) - (-s15 - s25 + ss)*(s25 - s35 + tt)))/((s15 + s25 -&
   s35)*s35*ss*(-1 + sW2)*sW2) - (e**6*me1*s3n*(-1 + 4*sW2)*(-4*mm2**2 + s15**2 +&
   s15*s25 + me2*(-4*mm2 + s15 + s25 - s35) - s15*s35 - s25*s35 + s35**2 + s25*ss -&
   ss**2 - s15*tt - s25*tt + s35*tt + mm2*(-3*s15 + s25 + s35 + 2*ss + 4*tt)))/((s15 +&
   s25 - s35)*s35*ss*(-1 + sW2)*sW2) + (e**6*snm*(s15**3 + s15**2*s25 - 3*s15**2*s35 -&
   2*s15*s25*s35 + 2*s15*s35**2 - 3*s15**2*ss - 2*s15*s25*ss + 5*s15*s35*ss +&
   2*s25*s35*ss - 2*s35**2*ss + 2*s15*ss**2 - 2*s35*ss**2 + 4*mm2**3*(1 - 4*sW2)**2 +&
   2*me2**2*(2*mm2 - ss)*(1 - 4*sW2)**2 - 4*s15**3*sW2 + 4*s15*s25**2*sW2 +&
   12*s15**2*s35*sW2 + 4*s15*s25*s35*sW2 - 8*s15*s35**2*sW2 + 12*s15**2*ss*sW2 +&
   4*s15*s25*ss*sW2 - 20*s15*s35*ss*sW2 - 4*s25*s35*ss*sW2 + 8*s35**2*ss*sW2 -&
   8*s15*ss**2*sW2 + 8*s35*ss**2*sW2 + 8*s15**3*sW2**2 - 8*s15*s25**2*sW2**2 -&
   24*s15**2*s35*sW2**2 - 8*s15*s25*s35*sW2**2 + 16*s15*s35**2*sW2**2 -&
   24*s15**2*ss*sW2**2 - 8*s15*s25*ss*sW2**2 + 40*s15*s35*ss*sW2**2 +&
   8*s25*s35*ss*sW2**2 - 16*s35**2*ss*sW2**2 + 16*s15*ss**2*sW2**2 -&
   16*s35*ss**2*sW2**2 - s15**2*tt - s15*s25*tt + 2*s15*s35*tt + 4*s15*ss*tt +&
   2*s25*ss*tt - 4*s35*ss*tt - 2*ss**2*tt + 4*s15**2*sW2*tt - 4*s25**2*sW2*tt -&
   8*s15*s35*sW2*tt + 8*s25*s35*sW2*tt - 24*s15*ss*sW2*tt - 8*s25*ss*sW2*tt +&
   16*s35*ss*sW2*tt + 16*ss**2*sW2*tt - 8*s15**2*sW2**2*tt + 8*s25**2*sW2**2*tt +&
   16*s15*s35*sW2**2*tt - 16*s25*s35*sW2**2*tt + 48*s15*ss*sW2**2*tt +&
   16*s25*ss*sW2**2*tt - 32*s35*ss*sW2**2*tt - 32*ss**2*sW2**2*tt - 2*ss*tt**2 +&
   16*ss*sW2*tt**2 - 32*ss*sW2**2*tt**2 - 2*mm2**2*(1 - 4*sW2)**2*(-3*s15 - s25 + 2*s35&
   + ss + 4*tt) + me2*(8*mm2**2*(1 - 4*sW2)**2 - 2*s15*ss*(1 - 4*sW2 + 8*sW2**2) + (s15&
   + s25 - 2*s35)*(s15 - 4*s15*sW2 + 4*s25*sW2 + 8*s15*sW2**2 - 8*s25*sW2**2) -&
   2*mm2*(1 - 4*sW2)**2*(-3*s15 - s25 + 2*s35 + 2*ss + 4*tt) + 4*ss*(s25*(-2*sW2 +&
   4*sW2**2) + s35*(1 - 4*sW2 + 8*sW2**2) + (1 - 4*sW2)**2*tt)) + mm2*(4*s25**2*sW2 -&
   8*s25*s35*sW2 - 8*s25**2*sW2**2 + 16*s25*s35*sW2**2 + s15**2*(3 - 20*sW2 +&
   40*sW2**2) - 2*s25*tt + 4*s35*tt + 16*s25*sW2*tt - 32*s35*sW2*tt - 32*s25*sW2**2*tt&
   + 64*s35*sW2**2*tt + 4*tt**2 - 32*sW2*tt**2 + 64*sW2**2*tt**2 + s15*(-4*s35*(1 -&
   6*sW2 + 12*sW2**2) + s25*(3 - 16*sW2 + 32*sW2**2) - 6*(1 - 4*sW2)**2*tt) +&
   2*ss*(4*s25*(sW2 - 2*sW2**2) - 4*s15*(1 - 5*sW2 + 10*sW2**2) + s35*(3 - 16*sW2 +&
   32*sW2**2) + 4*(1 - 4*sW2)**2*tt))))/(2.*(s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2) +&
   s3m*((e**6*s4n*(-2*me2*(s15 + s25 - s35) + 2*mm2*ss*(1 - 4*sW2)**2 - ss*(s15 -&
   4*s15*sW2 - 4*s25*sW2 + 8*s15*sW2**2 + 8*s25*sW2**2)))/(2.*(s15 + s25 -&
   s35)*s35*ss*(-1 + sW2)*sW2) + (e**6*s3n*(me2 + mm2 + s15 - s35 + 4*s35*sW2 -&
   4*ss*sW2 - 8*s35*sW2**2 + 8*ss*sW2**2 - tt))/((s15 + s25 - s35)*s35*(-1 + sW2)*sW2)&
   - (e**6*me1*(-1 + 4*sW2)*(-4*mm2**2 + s15**2 + s15*s25 + me2*(-4*mm2 + s15 + s25 -&
   s35) - s15*ss + s35*ss - ss**2 - s15*tt - s25*tt + s35*tt + mm2*(-3*s15 + s25 + s35&
   + 2*ss + 4*tt)))/((s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2) - (e**6*s2n*(2*me2**2 +&
   s15**2 - s15*ss - 4*s15**2*sW2 - 4*s15*s25*sW2 + 4*s15*ss*sW2 + 4*s25*ss*sW2 +&
   8*s15**2*sW2**2 + 8*s15*s25*sW2**2 - 8*s15*ss*sW2**2 - 8*s25*ss*sW2**2 +&
   mm2**2*(16*sW2 - 32*sW2**2) + me2*(s15 - 2*s25 - 2*s35 - 2*mm2*(1 - 4*sW2)**2 -&
   4*s15*sW2 - 4*s25*sW2 + 8*s35*sW2 + 8*s15*sW2**2 + 8*s25*sW2**2 - 16*s35*sW2**2 +&
   2*ss*(1 - 4*sW2 + 8*sW2**2) - 4*tt) - 3*s15*tt + 2*s35*tt + 4*s15*sW2*tt +&
   4*s25*sW2*tt - 8*s35*sW2*tt + 8*ss*sW2*tt - 8*s15*sW2**2*tt - 8*s25*sW2**2*tt +&
   16*s35*sW2**2*tt - 16*ss*sW2**2*tt + 2*tt**2 + mm2*(s15*(1 + 12*sW2 - 24*sW2**2) +&
   ss*(2 - 24*sW2 + 48*sW2**2) - 2*(s35 + 2*s25*sW2 - 4*s35*sW2 - 4*s25*sW2**2 +&
   8*s35*sW2**2 + tt + 8*sW2*tt - 16*sW2**2*tt))))/(2.*(s15 + s25 - s35)*s35*ss*(-1 +&
   sW2)*sW2)) + s4m*((e**6*s3n*(2*me2*(s15 + s25 - s35) + 2*mm2*ss*(1 - 4*sW2)**2 -&
   ss*(s15 - 4*s15*sW2 - 4*s25*sW2 + 8*s15*sW2**2 + 8*s25*sW2**2)))/(2.*(s15 + s25 -&
   s35)*s35*ss*(-1 + sW2)*sW2) - (e**6*s4n*(me2 + mm2 + s15 + s25 - s35 - ss -&
   4*s15*sW2 - 4*s25*sW2 + 4*s35*sW2 + 4*ss*sW2 + 8*s15*sW2**2 + 8*s25*sW2**2 -&
   8*s35*sW2**2 - 8*ss*sW2**2 - tt))/((s15 + s25 - s35)*s35*(-1 + sW2)*sW2) +&
   (e**6*me1*(-1 + 4*sW2)*(-s15**2 - s15*s25 + s15*s35 + 3*s15*ss + 2*s25*ss - 2*s35*ss&
   - 2*ss**2 + me2*(-s15 - s25 + s35 + 2*ss) + mm2*(s15 + s25 - s35 + 6*ss) + s15*tt +&
   s25*tt - s35*tt - 2*ss*tt))/((s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2) -&
   (e**6*s2n*(2*me2**2 + 2*s15**2 + 2*s15*s25 - 2*s15*s35 - 4*s15*ss - 2*s25*ss +&
   2*s35*ss + 2*ss**2 - 8*s15**2*sW2 - 8*s15*s25*sW2 + 8*s15*s35*sW2 + 16*s15*ss*sW2 +&
   8*s25*ss*sW2 - 8*s35*ss*sW2 - 8*ss**2*sW2 + 16*s15**2*sW2**2 + 16*s15*s25*sW2**2 -&
   16*s15*s35*sW2**2 - 32*s15*ss*sW2**2 - 16*s25*ss*sW2**2 + 16*s35*ss*sW2**2 +&
   16*ss**2*sW2**2 + 4*mm2**2*(1 - 4*sW2 + 8*sW2**2) + me2*(3*s15 + 2*s25 - 2*s35 -&
   4*s15*sW2 - 4*s25*sW2 + 8*s35*sW2 + 8*s15*sW2**2 + 8*s25*sW2**2 - 16*s35*sW2**2 -&
   4*ss*(1 - 2*sW2 + 4*sW2**2) + 2*mm2*(3 - 8*sW2 + 16*sW2**2) - 4*tt) - 3*s15*tt -&
   2*s25*tt + 2*s35*tt + 4*ss*tt + 4*s15*sW2*tt + 4*s25*sW2*tt - 8*s35*sW2*tt -&
   8*ss*sW2*tt - 8*s15*sW2**2*tt - 8*s25*sW2**2*tt + 16*s35*sW2**2*tt + 16*ss*sW2**2*tt&
   + 2*tt**2 + mm2*(-4*ss*(1 - 2*sW2 + 4*sW2**2) + s15*(3 - 4*sW2 + 8*sW2**2) +&
   2*(s35*(-1 + 4*sW2 - 8*sW2**2) + s25*(1 - 2*sW2 + 4*sW2**2) + (-3 + 8*sW2 -&
   16*sW2**2)*tt))))/(2.*(s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2)) +&
   s1m*(-(e**6*s2n*(4*mm2**2*(1 - 4*sW2)**2 + (-s15 - s25 + ss)*(s15 - 4*s15*sW2 -&
   4*s25*sW2 + 8*s15*sW2**2 + 8*s25*sW2**2) - 4*mm2*(ss*(1 - 4*sW2)**2 + 4*s15*sW2 -&
   8*s15*sW2**2 + s25*(-1 + 4*sW2 - 8*sW2**2))))/(2.*(s15 + s25 - s35)*s35*ss*(-1 +&
   sW2)*sW2) - (e**6*me1*(-1 + 4*sW2)*(6*mm2**2 + me2*(2*mm2 - s15 - s25 + ss) +&
   mm2*(3*s15 + s25 - 4*s35 + ss - 2*tt) - (-s15 - s25 + ss)*(-s15 + ss + tt)))/((s15 +&
   s25 - s35)*s35*ss*(-1 + sW2)*sW2) + (e**6*s4n*(2*me2**2 + s15**2 + s15*s25 -&
   3*s15*s35 - 2*s25*s35 + 2*s35**2 - s15*ss + 2*s35*ss - 4*s15**2*sW2 + 4*s25**2*sW2 +&
   12*s15*s35*sW2 + 4*s25*s35*sW2 - 8*s35**2*sW2 + 4*s15*ss*sW2 - 4*s25*ss*sW2 -&
   8*s35*ss*sW2 + 8*s15**2*sW2**2 - 8*s25**2*sW2**2 - 24*s15*s35*sW2**2 -&
   8*s25*s35*sW2**2 + 16*s35**2*sW2**2 - 8*s15*ss*sW2**2 + 8*s25*ss*sW2**2 +&
   16*s35*ss*sW2**2 + 4*mm2**2*(1 - 4*sW2 + 8*sW2**2) + me2*(3*s15 + 2*s25 - 4*s35 -&
   4*s15*sW2 - 4*s25*sW2 + 8*s35*sW2 + 8*s15*sW2**2 + 8*s25*sW2**2 - 16*s35*sW2**2 -&
   2*ss*(1 - 4*sW2 + 8*sW2**2) + 2*mm2*(3 - 8*sW2 + 16*sW2**2) - 4*tt) - 3*s15*tt -&
   2*s25*tt + 4*s35*tt + 2*ss*tt + 4*s15*sW2*tt + 4*s25*sW2*tt - 8*s35*sW2*tt -&
   8*ss*sW2*tt - 8*s15*sW2**2*tt - 8*s25*sW2**2*tt + 16*s35*sW2**2*tt + 16*ss*sW2**2*tt&
   + 2*tt**2 + mm2*(4*s25 - 6*s35 - 20*s25*sW2 + 24*s35*sW2 + 40*s25*sW2**2 -&
   48*s35*sW2**2 + 5*s15*(1 - 4*sW2 + 8*sW2**2) - 4*ss*(1 - 6*sW2 + 12*sW2**2) - 6*tt +&
   16*sW2*tt - 32*sW2**2*tt)))/(2.*(s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2) +&
   (e**6*s3n*(2*me2**2 + 3*s15**2 + 2*s15*s25 - 5*s15*s35 - 2*s25*s35 + 2*s35**2 -&
   2*s15*ss + 2*s35*ss - 4*s15**2*sW2 - 4*s15*s25*sW2 + 12*s15*s35*sW2 + 12*s25*s35*sW2&
   - 8*s35**2*sW2 - 8*s15*ss*sW2 - 8*s25*ss*sW2 + 8*ss**2*sW2 + 8*s15**2*sW2**2 +&
   8*s15*s25*sW2**2 - 24*s15*s35*sW2**2 - 24*s25*s35*sW2**2 + 16*s35**2*sW2**2 +&
   16*s15*ss*sW2**2 + 16*s25*ss*sW2**2 - 16*ss**2*sW2**2 + mm2**2*(16*sW2 - 32*sW2**2)&
   - 5*s15*tt - 2*s25*tt + 4*s35*tt + 2*ss*tt + 4*s15*sW2*tt + 4*s25*sW2*tt -&
   8*s35*sW2*tt + 8*ss*sW2*tt - 8*s15*sW2**2*tt - 8*s25*sW2**2*tt + 16*s35*sW2**2*tt -&
   16*ss*sW2**2*tt + 2*tt**2 - me2*(2*mm2*(1 - 4*sW2)**2 + s15*(-3 + 4*sW2 - 8*sW2**2)&
   + 4*(s35 + s25*sW2 - 2*s35*sW2 + 2*ss*sW2 - 2*s25*sW2**2 + 4*s35*sW2**2 -&
   4*ss*sW2**2 + tt)) + mm2*(s15*(3 + 12*sW2 - 24*sW2**2) + 2*ss*(-1 - 4*sW2 +&
   8*sW2**2) + 2*(s25*(1 - 2*sW2 + 4*sW2**2) + s35*(-1 - 4*sW2 + 8*sW2**2) + (-1 -&
   8*sW2 + 16*sW2**2)*tt))))/(2.*(s15 + s25 - s35)*s35*ss*(-1 + sW2)*sW2)) +&
   (e**6*(s15**2*s25 + s15*s25**2 + s15**2*s35 - s15*s35**2 + 2*s15**2*ss + s15*s25*ss&
   - 3*s15*s35*ss - s25*s35*ss + s35**2*ss - 4*s15*ss**2 - 2*s25*ss**2 + 2*s35*ss**2 +&
   2*ss**3 - 4*mm2**3*(1 - 4*sW2)**2 - 2*me2**2*(2*mm2 - ss)*(1 - 4*sW2)**2 +&
   4*s15**3*sW2 - 4*s15*s25**2*sW2 - 12*s15**2*s35*sW2 - 4*s15*s25*s35*sW2 +&
   8*s15*s35**2*sW2 - 12*s15**2*ss*sW2 - 4*s15*s25*ss*sW2 + 20*s15*s35*ss*sW2 +&
   4*s25*s35*ss*sW2 - 8*s35**2*ss*sW2 + 16*s15*ss**2*sW2 + 8*s25*ss**2*sW2 -&
   8*s35*ss**2*sW2 - 8*ss**3*sW2 - 8*s15**3*sW2**2 + 8*s15*s25**2*sW2**2 +&
   24*s15**2*s35*sW2**2 + 8*s15*s25*s35*sW2**2 - 16*s15*s35**2*sW2**2 +&
   24*s15**2*ss*sW2**2 + 8*s15*s25*ss*sW2**2 - 40*s15*s35*ss*sW2**2 -&
   8*s25*s35*ss*sW2**2 + 16*s35**2*ss*sW2**2 - 32*s15*ss**2*sW2**2 -&
   16*s25*ss**2*sW2**2 + 16*s35*ss**2*sW2**2 + 16*ss**3*sW2**2 - s15*s25*tt - s25**2*tt&
   - s15*s35*tt + s25*s35*tt - 4*s15*ss*tt - 2*s25*ss*tt + 2*s35*ss*tt + 4*ss**2*tt -&
   4*s15**2*sW2*tt + 4*s25**2*sW2*tt + 8*s15*s35*sW2*tt - 8*s25*s35*sW2*tt +&
   24*s15*ss*sW2*tt + 8*s25*ss*sW2*tt - 16*s35*ss*sW2*tt - 16*ss**2*sW2*tt +&
   8*s15**2*sW2**2*tt - 8*s25**2*sW2**2*tt - 16*s15*s35*sW2**2*tt +&
   16*s25*s35*sW2**2*tt - 48*s15*ss*sW2**2*tt - 16*s25*ss*sW2**2*tt +&
   32*s35*ss*sW2**2*tt + 32*ss**2*sW2**2*tt + 2*ss*tt**2 - 16*ss*sW2*tt**2 +&
   32*ss*sW2**2*tt**2 + 2*mm2**2*(ss*(3 - 8*sW2 + 16*sW2**2) - (1 - 4*sW2)**2*(3*s15 +&
   s25 - 2*(s35 + 2*tt))) - mm2*(-s25**2 + s25*s35 + 4*s25**2*sW2 - 8*s25*s35*sW2 -&
   8*s25**2*sW2**2 + 16*s25*s35*sW2**2 + 2*ss**2*(3 - 8*sW2 + 16*sW2**2) + s15*s25*(1 -&
   16*sW2 + 32*sW2**2) + s15**2*(2 - 20*sW2 + 40*sW2**2) - 2*s25*tt + 4*s35*tt +&
   16*s25*sW2*tt - 32*s35*sW2*tt - 32*s25*sW2**2*tt + 64*s35*sW2**2*tt + 4*tt**2 -&
   32*sW2*tt**2 + 64*sW2**2*tt**2 - 3*s15*(1 - 4*sW2)**2*(s35 + 2*tt) - 2*ss*(5*s15*(1&
   - 4*sW2 + 8*sW2**2) + s25*(1 - 4*sW2 + 8*sW2**2) - (3 - 16*sW2 + 32*sW2**2)*(s35 +&
   2*tt))) + me2*(s15*s25 + s25**2 + s15*s35 - s25*s35 - 2*ss**2 - 8*mm2**2*(1 -&
   4*sW2)**2 + 4*s15**2*sW2 - 4*s25**2*sW2 - 8*s15*s35*sW2 + 8*s25*s35*sW2 -&
   8*s15**2*sW2**2 + 8*s25**2*sW2**2 + 16*s15*s35*sW2**2 - 16*s25*s35*sW2**2 +&
   2*ss*(-(s35*(1 - 4*sW2)**2) + s15*(1 - 4*sW2 + 8*sW2**2) - 2*(s25*(-2*sW2 +&
   4*sW2**2) + (1 - 4*sW2)**2*tt)) + 2*mm2*(4*ss*(1 - 4*sW2 + 8*sW2**2) - (1 -&
   4*sW2)**2*(3*s15 + s25 - 2*(s35 + 2*tt))))))/((s15 + s25 - s35)*s35*ss*(-1 +&
   sW2)*sW2)

  m9 = s4m*((e**6*s3n*(-2*me2 + ss*(1 - 4*sW2)**2))/(2.*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) -&
   (e**6*s2n*(1 - 4*sW2)**2*(me2 + mm2 - tt))/(2.*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2)&
   - (e**6*me1*(-1 + 4*sW2)*(me2 + mm2 + s15 + s25 - s35 - ss - tt))/((s15 + s25 -&
   s35)*ss*(-1 + sW2)*sW2)) - (e**6*me1*s4n*(-1 + 4*sW2)*(me2 + mm2 + 2*s15 + 2*s25 -&
   2*s35 - 2*ss - tt))/((s15 + s25 - s35)*ss*(-1 + sW2)*sW2) + (e**6*me1*s3n*(-1 +&
   4*sW2)*(4*mm2**2 + me2*(4*mm2 - s15 - s25 + s35) + mm2*(3*s15 + 3*s25 - 3*s35 - 6*ss&
   - 4*tt) + (s15 + s25 - s35)*(-s15 - 2*s25 + 2*s35 + 2*ss + tt)))/((s15 + s25 -&
   s35)**2*ss*(-1 + sW2)*sW2) + (e**6*me1*s2n*(-1 + 4*sW2)*(2*mm2**2 + me2*(-2*mm2 +&
   s15 + s25 - s35) - (s15 + s25 - s35)*(-2*s15 - 2*s25 + 2*s35 + 2*ss + tt) +&
   mm2*(-s15 - s25 + s35 + 4*ss + 2*tt)))/((s15 + s25 - s35)**2*ss*(-1 + sW2)*sW2) +&
   (e**6*snm*(1 - 4*sW2)**2*(4*me2**2*mm2 + 4*mm2**3 + me2*(8*mm2**2 + s15**2 - s25**2&
   - 3*s15*s35 - s25*s35 + 2*s35**2 + 4*mm2*(s15 + s25 - s35 - 2*tt)) + 4*mm2**2*(s15 +&
   s25 - s35 - 2*tt) + (s15 + s25 - s35)*(s15**2 + (-s15 + s35)*ss + s15*(s25 - s35 -&
   tt) + s25*tt) + mm2*(s15**2 - s25**2 + s25*(s35 - 4*tt) + 4*tt*(s35 + ss + tt) -&
   s15*(s35 + 4*tt))))/(2.*(s15 + s25 - s35)**2*ss*(-1 + sW2)*sW2) -&
   (2*e**6*(2*me2**2*mm2*(1 - 4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 + me2*(-(s25*s35) +&
   s35**2 + 4*mm2**2*(1 - 4*sW2)**2 + 4*s25**2*sW2 + 4*s25*s35*sW2 - 8*s35**2*sW2 -&
   8*s25**2*sW2**2 - 8*s25*s35*sW2**2 + 16*s35**2*sW2**2 + s15**2*(1 - 4*sW2 +&
   8*sW2**2) + s15*(s25 - 2*s35*(1 - 6*sW2 + 12*sW2**2)) - 2*mm2*(ss - (1 -&
   4*sW2)**2*(s15 + s25 - s35 - 2*tt))) - 2*mm2**2*(ss - (1 - 4*sW2)**2*(s15 + s25 -&
   s35 - 2*tt)) + (s15 + s25 - s35)*(s15**2*(1 - 4*sW2 + 8*sW2**2) - s15*ss*(1 - 4*sW2&
   + 8*sW2**2) + s15*(1 - 4*sW2 + 8*sW2**2)*(s25 - s35 - tt) + 4*s25*sW2*(-1 +&
   2*sW2)*tt) + mm2*(s15**2*(1 - 4*sW2 + 8*sW2**2) + 2*ss**2*(1 - 4*sW2 + 8*sW2**2) +&
   s15*(s25 + s35*(-1 + 4*sW2 - 8*sW2**2) - 2*(1 - 4*sW2)**2*tt) - ss*(s15 + s25 - s35&
   - 4*tt + 16*sW2*tt - 32*sW2**2*tt) + 2*(s25**2*(2*sW2 - 4*sW2**2) + s25*s35*(-2*sW2&
   + 4*sW2**2) - s25*(1 - 4*sW2)**2*tt + (1 - 4*sW2)**2*tt*(s35 + tt)))))/((s15 + s25 -&
   s35)**2*ss*(-1 + sW2)*sW2) + s1m*(-(e**6*s2n*(4*mm2**2 - (s15 + s25 - s35)*(-s15 -&
   s25 + ss))*(1 - 4*sW2)**2)/(2.*(s15 + s25 - s35)**2*ss*(-1 + sW2)*sW2) +&
   (e**6*s4n*(1 - 4*sW2)**2*(me2 + mm2 + s15 + s25 - s35 - ss - tt))/(2.*(s15 + s25 -&
   s35)*ss*(-1 + sW2)*sW2) + (e**6*me1*(-1 + 4*sW2)*(-6*mm2**2 + me2*(-2*mm2 + s15 +&
   s25 - s35) - (s15 + s25 - s35)*(-s15 - s25 + s35 + ss + tt) + mm2*(-3*s15 - 3*s25 +&
   3*s35 + 2*ss + 2*tt)))/((s15 + s25 - s35)**2*ss*(-1 + sW2)*sW2) +&
   (e**6*s3n*(me2*(-8*mm2*(1 - 4*sW2 + 8*sW2**2) + (s15 + s25 - s35)*(3 - 8*sW2 +&
   16*sW2**2)) - (1 - 4*sW2)**2*(4*mm2**2 + mm2*(3*s15 + 3*s25 - 3*s35 - 4*ss - 4*tt) +&
   (s15 + s25 - s35)*(-s15 - 2*s25 + s35 + 2*ss + tt))))/(2.*(s15 + s25 -&
   s35)**2*ss*(-1 + sW2)*sW2)) + s3m*((e**6*s4n*(2*me2 + ss*(1 - 4*sW2)**2))/(2.*(s15 +&
   s25 - s35)*ss*(-1 + sW2)*sW2) + (e**6*(-2*mm2 + s15 + s25 - s35)*s3n*(1 -&
   4*sW2)**2)/((s15 + s25 - s35)**2*(-1 + sW2)*sW2) + (e**6*me1*(-1 + 4*sW2)*(4*mm2**2&
   + me2*(4*mm2 - s15 - s25 + s35) + (s15 + s25 - s35)*(s15 + s35 + ss + tt) - mm2*(s15&
   + s25 - s35 + 6*ss + 4*tt)))/((s15 + s25 - s35)**2*ss*(-1 + sW2)*sW2) +&
   (e**6*s2n*(me2*(8*mm2*(1 - 4*sW2 + 8*sW2**2) - (s15 + s25 - s35)*(3 - 8*sW2 +&
   16*sW2**2)) + (1 - 4*sW2)**2*(4*mm2**2 + (s15 + s25 - s35)*(s15 - ss + tt) -&
   mm2*(s15 + s25 - s35 + 4*tt))))/(2.*(s15 + s25 - s35)**2*ss*(-1 + sW2)*sW2))

  m12 = (e**6*me1*s2n*(-1 + 4*sW2)*(me2**2 + 3*mm2**2 + s15**2 + 2*s15*s25 + s25**2 - 2*s15*s35&
   - s25*s35 - 2*s15*ss - 2*s25*ss + 2*s35*ss + ss**2 + mm2*(5*s15 + 2*s25 - 3*s35 -&
   5*ss - 4*tt) + me2*(4*mm2 + s15 - s35 - ss - 2*tt) - 2*s15*tt - s25*tt + s35*tt +&
   2*ss*tt + tt**2))/(s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2) - (e**6*me1*s4n*(-1 +&
   4*sW2)*(2*me2**2 + 2*mm2**2 + 2*s15**2 + 3*s15*s25 + s25**2 - 3*s15*s35 - 2*s25*s35&
   + s35**2 - 3*s15*ss - 2*s25*ss + 4*s35*ss + ss**2 + mm2*(5*s15 + 3*s25 - 4*s35 -&
   7*ss - 4*tt) + me2*(4*mm2 + 3*s15 + s25 - 4*s35 - 3*ss - 4*tt) - 4*s15*tt - 2*s25*tt&
   + 3*s35*tt + 4*ss*tt + 2*tt**2))/(s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   (e**6*me1*s3n*(-1 + 4*sW2)*(mm2*(-2*s25 + 4*ss) - (-s35 + ss)*(-s15 - s25 + s35 + ss&
   + tt)))/(s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2) - (e**6*(2*s15**3 + 3*s15**2*s25 +&
   s15*s25**2 - 3*s15**2*s35 - 2*s15*s25*s35 + s15*s35**2 - 4*s15**2*ss - 3*s15*s25*ss&
   + 5*s15*s35*ss + s25*s35*ss - s35**2*ss + 2*s15*ss**2 - 2*s35*ss**2 + 2*me2**3*(1 -&
   4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 - 8*s15**3*sW2 - 12*s15**2*s25*sW2 -&
   4*s15*s25**2*sW2 + 20*s15**2*s35*sW2 + 8*s15*s25*s35*sW2 - 12*s15*s35**2*sW2 +&
   16*s15**2*ss*sW2 + 12*s15*s25*ss*sW2 - 20*s15*s35*ss*sW2 - 4*s25*s35*ss*sW2 +&
   4*s35**2*ss*sW2 - 8*s15*ss**2*sW2 + 8*s35*ss**2*sW2 + 16*s15**3*sW2**2 +&
   24*s15**2*s25*sW2**2 + 8*s15*s25**2*sW2**2 - 40*s15**2*s35*sW2**2 -&
   16*s15*s25*s35*sW2**2 + 24*s15*s35**2*sW2**2 - 32*s15**2*ss*sW2**2 -&
   24*s15*s25*ss*sW2**2 + 40*s15*s35*ss*sW2**2 + 8*s25*s35*ss*sW2**2 -&
   8*s35**2*ss*sW2**2 + 16*s15*ss**2*sW2**2 - 16*s35*ss**2*sW2**2 - 6*s15**2*tt -&
   5*s15*s25*tt - s25**2*tt + 7*s15*s35*tt + 3*s25*s35*tt - 2*s35**2*tt + 8*s15*ss*tt +&
   2*s25*ss*tt - 6*s35*ss*tt - 2*ss**2*tt + 32*s15**2*sW2*tt + 20*s15*s25*sW2*tt +&
   4*s25**2*sW2*tt - 52*s15*s35*sW2*tt - 12*s25*s35*sW2*tt + 16*s35**2*sW2*tt -&
   32*s15*ss*sW2*tt - 8*s25*ss*sW2*tt + 24*s35*ss*sW2*tt + 8*ss**2*sW2*tt -&
   64*s15**2*sW2**2*tt - 40*s15*s25*sW2**2*tt - 8*s25**2*sW2**2*tt +&
   104*s15*s35*sW2**2*tt + 24*s25*s35*sW2**2*tt - 32*s35**2*sW2**2*tt +&
   64*s15*ss*sW2**2*tt + 16*s25*ss*sW2**2*tt - 48*s35*ss*sW2**2*tt - 16*ss**2*sW2**2*tt&
   + 6*s15*tt**2 + 2*s25*tt**2 - 4*s35*tt**2 - 4*ss*tt**2 - 40*s15*sW2*tt**2 -&
   8*s25*sW2*tt**2 + 32*s35*sW2*tt**2 + 16*ss*sW2*tt**2 + 80*s15*sW2**2*tt**2 +&
   16*s25*sW2**2*tt**2 - 64*s35*sW2**2*tt**2 - 32*ss*sW2**2*tt**2 - 2*tt**3 +&
   16*sW2*tt**3 - 32*sW2**2*tt**3 + me2**2*(5*s15 + s25 - 6*s35 - 2*ss + 6*mm2*(1 -&
   4*sW2)**2 - 32*s15*sW2 + 48*s35*sW2 + 64*s15*sW2**2 - 96*s35*sW2**2 - 6*tt +&
   48*sW2*tt - 96*sW2**2*tt) + mm2**2*(s25 - 4*s35 - 2*ss + 32*s35*sW2 - 64*s35*sW2**2&
   + s15*(7 - 48*sW2 + 96*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) + me2*(5*s15**2 +&
   4*s15*s25 + s25**2 - 8*s15*s35 - 3*s25*s35 + 4*s35**2 + 6*mm2**2*(1 - 4*sW2)**2 -&
   24*s15**2*sW2 - 12*s15*s25*sW2 - 4*s25**2*sW2 + 60*s15*s35*sW2 + 12*s25*s35*sW2 -&
   32*s35**2*sW2 + 48*s15**2*sW2**2 + 24*s15*s25*sW2**2 + 8*s25**2*sW2**2 -&
   120*s15*s35*sW2**2 - 24*s25*s35*sW2**2 + 64*s35**2*sW2**2 + 2*ss**2*(1 - 4*sW2 +&
   8*sW2**2) - 11*s15*tt - 3*s25*tt + 10*s35*tt + 72*s15*sW2*tt + 8*s25*sW2*tt -&
   80*s35*sW2*tt - 144*s15*sW2**2*tt - 16*s25*sW2**2*tt + 160*s35*sW2**2*tt + 6*tt**2 -&
   48*sW2*tt**2 + 96*sW2**2*tt**2 + ss*(5*s35 - 16*s35*sW2 + 32*s35*sW2**2 + s15*(-7 +&
   24*sW2 - 48*sW2**2) - 2*s25*(1 - 4*sW2 + 8*sW2**2) + 6*tt - 16*sW2*tt +&
   32*sW2**2*tt) - 2*mm2*(-3*s25 + 5*s35 + 2*ss - 40*s35*sW2 + 80*s35*sW2**2 - 8*s15*(1&
   - 5*sW2 + 10*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt)) + mm2*(s25**2 - 3*s25*s35 +&
   2*s35**2 - 4*s25**2*sW2 + 12*s25*s35*sW2 - 16*s35**2*sW2 + 8*s25**2*sW2**2 -&
   24*s25*s35*sW2**2 + 32*s35**2*sW2**2 + 2*ss**2*(1 - 4*sW2 + 8*sW2**2) + s15**2*(7 -&
   40*sW2 + 80*sW2**2) - 3*s25*tt + 8*s35*tt + 8*s25*sW2*tt - 64*s35*sW2*tt -&
   16*s25*sW2**2*tt + 128*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 +&
   ss*(5*s35 - 16*s35*sW2 + 32*s35*sW2**2 - 2*s25*(1 - 4*sW2 + 8*sW2**2) - 3*s15*(3 -&
   8*sW2 + 16*sW2**2) + 6*tt - 16*sW2*tt + 32*sW2**2*tt) + s15*(6*s25*(1 - 2*sW2 +&
   4*sW2**2) - 4*s35*(2 - 15*sW2 + 30*sW2**2) + (-13 + 88*sW2 -&
   176*sW2**2)*tt))))/(s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   s3m*((e**6*s4n*(4*s15*ss*sW2*(-1 + 2*sW2) + mm2*ss*(-3 + 8*sW2 - 16*sW2**2) +&
   me2*(8*mm2 - ss*(1 - 4*sW2)**2 + 2*(s15 + s25 - 4*s15*sW2 - 4*s25*sW2 + 4*s35*sW2 +&
   8*s15*sW2**2 + 8*s25*sW2**2 - 8*s35*sW2**2))))/(2.*s15*s35*(s15 + s25 - ss)*(-1 +&
   sW2)*sW2) + (e**6*s3n*(me2*(4*mm2 + s35 + ss*(-1 + 4*sW2 - 8*sW2**2)) - ss*(mm2*(1 -&
   4*sW2 + 8*sW2**2) + 4*sW2*(-1 + 2*sW2)*(s15 - s35 - tt))))/(s15*s35*(s15 + s25 -&
   ss)*(-1 + sW2)*sW2) + (e**6*me1*(-1 + 4*sW2)*(s15**2 + s15*s25 + me2*(s15 + s25 -&
   ss) - s15*ss + mm2*(3*s15 + s25 - 2*s35 + ss) - s15*tt - s25*tt +&
   s35*tt))/(s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*s2n*(-mm2**2 + me2**2*(1&
   - 4*sW2)**2 + me2*(-s15 - 2*s25 + ss*(1 - 4*sW2)**2 - 4*s15*sW2 + 8*s25*sW2 +&
   8*s35*sW2 + 8*s15*sW2**2 - 16*s25*sW2**2 - 16*s35*sW2**2 + 4*mm2*(-1 - 2*sW2 +&
   4*sW2**2) - tt + 16*sW2*tt - 32*sW2**2*tt) + mm2*(8*s35*sW2 - 16*s35*sW2**2 +&
   s15*(-3 - 4*sW2 + 8*sW2**2) + ss*(3 - 8*sW2 + 16*sW2**2) + tt + 8*sW2*tt -&
   16*sW2**2*tt) - 4*sW2*(-1 + 2*sW2)*(-s15**2 + s15*ss + 3*s15*tt - 2*tt*(s35 +&
   tt))))/(2.*s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2)) +&
   s4m*((e**6*s3n*(4*s15*ss*sW2*(-1 + 2*sW2) + mm2*ss*(-3 + 8*sW2 - 16*sW2**2) +&
   me2*(8*mm2 + ss*(1 - 4*sW2)**2 - 8*(s15 + s25 - s35)*sW2*(-1 +&
   2*sW2))))/(2.*s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*s4n*(me2*(4*mm2 - s15&
   - s25 - s35 + 4*ss*sW2 - 8*ss*sW2**2) - ss*(1 - 4*sW2 + 8*sW2**2)*(3*mm2 - s35 -&
   tt)))/(s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2) - (e**6*me1*(-1 + 4*sW2)*(2*me2**2 +&
   2*mm2**2 + s15**2 + s15*s25 - s15*s35 - s15*ss + 2*s35*ss + mm2*(2*s15 - 4*ss -&
   4*tt) + 2*me2*(2*mm2 + s15 - s35 - ss - 2*tt) - 3*s15*tt - s25*tt + s35*tt + 3*ss*tt&
   + 2*tt**2))/(s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*s2n*(mm2**2*(-3 +&
   16*sW2 - 32*sW2**2) + me2**2*(-3 + 8*sW2 - 16*sW2**2) + me2*(2*s25 + 4*s35 +&
   12*s15*sW2 - 8*s35*sW2 - 8*ss*sW2 - 24*s15*sW2**2 + 16*s35*sW2**2 + 16*ss*sW2**2 -&
   2*mm2*(5 - 12*sW2 + 24*sW2**2) + 5*tt - 16*sW2*tt + 32*sW2**2*tt) + mm2*(2*s35 -&
   8*s35*sW2 + 16*s35*sW2**2 + s15*(-6 + 28*sW2 - 56*sW2**2) + 6*ss*(1 - 4*sW2 +&
   8*sW2**2) + 5*tt - 24*sW2*tt + 48*sW2**2*tt) - 2*(ss*(1 - 4*sW2 + 8*sW2**2)*(s35 +&
   tt) + (1 - 4*sW2 + 8*sW2**2)*tt*(s35 + tt) - s15*(s35 - 4*s35*sW2 + 8*s35*sW2**2 +&
   tt - 6*sW2*tt + 12*sW2**2*tt))))/(2.*s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2)) +&
   s1m*((e**6*me1*(-1 + 4*sW2)*(me2**2 - mm2**2 + s15**2 + s15*s25 - s15*s35 - s25*s35&
   - s35**2 + mm2*(s15 + 2*(s25 + s35) - 3*ss) - s15*ss + 2*s35*ss + me2*(s15 - ss -&
   2*tt) - 2*s15*tt - s25*tt + 2*ss*tt + tt**2))/(s15*s35*(s15 + s25 - ss)*(-1 +&
   sW2)*sW2) + (e**6*s2n*(-(me2*(s15 + s25 - ss + 2*mm2*(1 - 4*sW2)**2)) - 2*mm2**2*(1&
   - 4*sW2)**2 - 4*s15*(s15 + s25 - ss)*sW2*(-1 + 2*sW2) + mm2*(3*s25 + 2*s35 -&
   8*s25*sW2 - 16*s35*sW2 + 16*s25*sW2**2 + 32*s35*sW2**2 + ss*(-3 + 8*sW2 - 16*sW2**2)&
   + s15*(3 + 8*sW2 - 16*sW2**2) + 2*tt - 16*sW2*tt + 32*sW2**2*tt)))/(2.*s15*s35*(s15&
   + s25 - ss)*(-1 + sW2)*sW2) + (e**6*s3n*(mm2**2 - me2**2*(1 - 4*sW2)**2 - mm2*(2*s25&
   + s35 - 8*s25*sW2 + 8*s35*sW2 + 16*s25*sW2**2 - 16*s35*sW2**2 - 2*ss*(1 - 4*sW2 +&
   8*sW2**2) + s15*(-1 - 12*sW2 + 24*sW2**2) + tt + 8*sW2*tt - 16*sW2**2*tt) +&
   me2*(s15*(-1 + 12*sW2 - 24*sW2**2) - 4*mm2*(1 - 2*sW2 + 4*sW2**2) + (1 - 16*sW2 +&
   32*sW2**2)*(s35 + tt)) + 4*sW2*(-1 + 2*sW2)*(s15 - s35 - tt)*(-3*s15 + 2*ss +&
   2*(-s25 + s35 + tt))))/(2.*s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   (e**6*s4n*(me2**2*(3 - 8*sW2 + 16*sW2**2) + mm2**2*(3 - 16*sW2 + 32*sW2**2) +&
   mm2*(-3*s25 - 5*s35 + 8*s25*sW2 + 24*s35*sW2 - 16*s25*sW2**2 - 48*s35*sW2**2 + ss*(3&
   - 8*sW2 + 16*sW2**2) + s15*(3 - 20*sW2 + 40*sW2**2) - 5*tt + 24*sW2*tt -&
   48*sW2**2*tt) + me2*(3*s15 + s25 - 5*s35 - ss - 12*s15*sW2 + 16*s35*sW2 +&
   24*s15*sW2**2 - 32*s35*sW2**2 + mm2*(2 - 24*sW2 + 48*sW2**2) - 5*tt + 16*sW2*tt -&
   32*sW2**2*tt) + 2*(s15**2*(-2*sW2 + 4*sW2**2) + (1 - 4*sW2 + 8*sW2**2)*(s35 + tt)**2&
   - s15*(s35 + 2*s25*sW2 - 6*s35*sW2 - 2*ss*sW2 - 4*s25*sW2**2 + 12*s35*sW2**2 +&
   4*ss*sW2**2 + tt - 6*sW2*tt + 12*sW2**2*tt))))/(2.*s15*s35*(s15 + s25 - ss)*(-1 +&
   sW2)*sW2)) + (e**6*snm*(2*me2**3*(1 - 4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 +&
   me2**2*(-s25 - 6*s35 + 6*mm2*(1 - 4*sW2)**2 + 48*s35*sW2 - 96*s35*sW2**2 + s15*(3 -&
   32*sW2 + 64*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) - mm2**2*(s25 + s15*(-5 +&
   48*sW2 - 96*sW2**2) + 2*(1 - 4*sW2)**2*(2*s35 + 3*tt)) + mm2*(2*s35**2 +&
   8*s25*s35*sW2 - 16*s35**2*sW2 - 16*s25*s35*sW2**2 + 32*s35**2*sW2**2 + s15**2*(3 -&
   36*sW2 + 72*sW2**2) + s25*tt + 8*s35*tt + 8*s25*sW2*tt - 64*s35*sW2*tt -&
   16*s25*sW2**2*tt + 128*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 +&
   ss*(s15*(-3 + 16*sW2 - 32*sW2**2) + s35*(3 - 16*sW2 + 32*sW2**2) + 2*(1 -&
   4*sW2)**2*tt) - s15*(7*s35*(1 - 4*sW2)**2 + s25*(3 + 4*sW2 - 8*sW2**2) + (9 - 88*sW2&
   + 176*sW2**2)*tt)) + me2*(s15**2 - s15*s25 - 5*s15*s35 + 2*s25*s35 + 4*s35**2 -&
   s15*ss + s35*ss + 6*mm2**2*(1 - 4*sW2)**2 - 20*s15**2*sW2 - 4*s15*s25*sW2 +&
   56*s15*s35*sW2 + 8*s25*s35*sW2 - 32*s35**2*sW2 + 16*s15*ss*sW2 - 16*s35*ss*sW2 +&
   40*s15**2*sW2**2 + 8*s15*s25*sW2**2 - 112*s15*s35*sW2**2 - 16*s25*s35*sW2**2 +&
   64*s35**2*sW2**2 - 32*s15*ss*sW2**2 + 32*s35*ss*sW2**2 - 7*s15*tt + s25*tt +&
   10*s35*tt + 2*ss*tt + 72*s15*sW2*tt + 8*s25*sW2*tt - 80*s35*sW2*tt - 16*ss*sW2*tt -&
   144*s15*sW2**2*tt - 16*s25*sW2**2*tt + 160*s35*sW2**2*tt + 32*ss*sW2**2*tt + 6*tt**2&
   - 48*sW2*tt**2 + 96*sW2**2*tt**2 + 2*mm2*(s25 + s15*(6 - 40*sW2 + 80*sW2**2) - (1 -&
   4*sW2)**2*(5*s35 + 6*tt))) - 2*(s15**3*(2*sW2 - 4*sW2**2) + tt*(s35 + tt)*(s35*(1 -&
   4*sW2)**2 + s25*(4*sW2 - 8*sW2**2) + (1 - 4*sW2)**2*tt) + s15**2*(s35 + 2*s25*sW2 -&
   6*s35*sW2 - 4*s25*sW2**2 + 12*s35*sW2**2 + tt - 14*sW2*tt + 28*sW2**2*tt) -&
   s15*(s35**2*(1 - 4*sW2 + 8*sW2**2) + 3*s35*(1 - 4*sW2)**2*tt + 2*tt*(3*s25*sW2 -&
   6*s25*sW2**2 + tt - 10*sW2*tt + 20*sW2**2*tt)) + ss*(s15**2*(-2*sW2 + 4*sW2**2) +&
   s35**2*(1 - 4*sW2 + 8*sW2**2) + 2*s35*(1 - 6*sW2 + 12*sW2**2)*tt + (1 -&
   4*sW2)**2*tt**2 - s15*(s35 - 6*s35*sW2 + 12*s35*sW2**2 + tt - 12*sW2*tt +&
   24*sW2**2*tt)))))/(2.*s15*s35*(s15 + s25 - ss)*(-1 + sW2)*sW2)

  m14 = (e**6*me1*s4n*(-1 + 4*sW2)*(2*s15*ss + 2*s25*ss - s35*ss - 2*ss**2 + me2*(-2*s15 - 2*s25&
   + s35 + 3*ss) + mm2*(-2*s15 - 4*s25 + s35 + 5*ss) + s15*tt + s25*tt -&
   2*ss*tt))/(s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2) -&
   (e**6*me1*s2n*(-1 + 4*sW2)*(me2**2 - mm2**2 + 2*s15**2 + 4*s15*s25 + 2*s25**2 -&
   5*s15*s35 - 4*s25*s35 + 2*s35**2 - 2*s15*ss - 2*s25*ss + 3*s35*ss + mm2*(-3*s25 -&
   s35 + 4*ss) + me2*(2*s15 + s25 - 3*s35 - 2*tt) - 3*s15*tt - 2*s25*tt + 3*s35*tt +&
   ss*tt + tt**2))/(s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   (e**6*me1*s3n*(-1 + 4*sW2)*(2*me2**2 + 2*mm2**2 + 3*s15**2 + 5*s15*s25 + 2*s25**2 -&
   6*s15*s35 - 4*s25*s35 + 2*s35**2 - 4*s15*ss - 3*s25*ss + 4*s35*ss + ss**2 +&
   mm2*(5*s15 - s25 - 3*s35 - 4*tt) + me2*(4*mm2 + 3*s15 + s25 - 3*s35 - 2*ss - 4*tt) -&
   5*s15*tt - 3*s25*tt + 4*s35*tt + 3*ss*tt + 2*tt**2))/(s15*(s15 + s25 - s35)*(s15 +&
   s25 - ss)*(-1 + sW2)*sW2) + s3m*(-(e**6*s4n*(mm2*ss*(1 + 8*sW2 - 16*sW2**2) +&
   s15*ss*(1 - 4*sW2 + 8*sW2**2) + me2*(-8*mm2 + ss*(1 - 4*sW2)**2 - 2*s35*(1 - 4*sW2 +&
   8*sW2**2))))/(2.*s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   (e**6*me1*(-1 + 4*sW2)*(2*me2**2 + 2*mm2**2 + mm2*(8*s15 + 2*s25 - 3*s35 - 3*ss -&
   4*tt) + me2*(4*mm2 + 2*s15 - s35 - 3*ss - 4*tt) + 2*(ss + tt)*(-2*s15 - s25 + s35 +&
   ss + tt)))/(s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   (e**6*s3n*(me2*(4*mm2 - 2*s15 - 2*s25 + s35 + ss + 4*ss*sW2 - 8*ss*sW2**2) +&
   4*ss*sW2*(-1 + 2*sW2)*(mm2 - 2*s15 - s25 + s35 + ss + tt)))/(s15*(s15 + s25 -&
   s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*s2n*(-mm2**2 - s15**2 + s15*ss +&
   me2**2*(1 - 4*sW2)**2 + 4*s15**2*sW2 - 4*s15*ss*sW2 - 8*s15**2*sW2**2 +&
   8*s15*ss*sW2**2 + s15*tt + 12*s15*sW2*tt + 8*s25*sW2*tt - 8*s35*sW2*tt - 8*ss*sW2*tt&
   - 24*s15*sW2**2*tt - 16*s25*sW2**2*tt + 16*s35*sW2**2*tt + 16*ss*sW2**2*tt -&
   8*sW2*tt**2 + 16*sW2**2*tt**2 + me2*(4*s15 + 4*s25 - 2*s35 - 12*s15*sW2 - 8*s25*sW2&
   + 8*s35*sW2 + 24*s15*sW2**2 + 16*s25*sW2**2 - 16*s35*sW2**2 + ss*(-3 + 8*sW2 -&
   16*sW2**2) + 4*mm2*(-1 - 2*sW2 + 4*sW2**2) - tt + 16*sW2*tt - 32*sW2**2*tt) +&
   mm2*(-8*s25*sW2 + 8*s35*sW2 + 16*s25*sW2**2 - 16*s35*sW2**2 + ss*(1 + 16*sW2 -&
   32*sW2**2) + s15*(-2 - 20*sW2 + 40*sW2**2) + tt + 8*sW2*tt -&
   16*sW2**2*tt)))/(2.*s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2)) +&
   (e**6*snm*(s15**3 + s15**2*s25 - 3*s15**2*s35 - 2*s15*s25*s35 + 2*s15*s35**2 -&
   3*s15**2*ss - 2*s15*s25*ss + 5*s15*s35*ss + 2*s25*s35*ss - 2*s35**2*ss + 2*s15*ss**2&
   - 2*s35*ss**2 + 2*me2**3*(1 - 4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 - 4*s15**3*sW2 -&
   4*s15**2*s25*sW2 + 12*s15**2*s35*sW2 + 8*s15*s25*s35*sW2 - 8*s15*s35**2*sW2 +&
   12*s15**2*ss*sW2 + 8*s15*s25*ss*sW2 - 20*s15*s35*ss*sW2 - 8*s25*s35*ss*sW2 +&
   8*s35**2*ss*sW2 - 8*s15*ss**2*sW2 + 8*s35*ss**2*sW2 + 8*s15**3*sW2**2 +&
   8*s15**2*s25*sW2**2 - 24*s15**2*s35*sW2**2 - 16*s15*s25*s35*sW2**2 +&
   16*s15*s35**2*sW2**2 - 24*s15**2*ss*sW2**2 - 16*s15*s25*ss*sW2**2 +&
   40*s15*s35*ss*sW2**2 + 16*s25*s35*ss*sW2**2 - 16*s35**2*ss*sW2**2 +&
   16*s15*ss**2*sW2**2 - 16*s35*ss**2*sW2**2 - 3*s15**2*tt - 3*s15*s25*tt +&
   6*s15*s35*tt + 2*s25*s35*tt - 2*s35**2*tt + 6*s15*ss*tt + 2*s25*ss*tt - 6*s35*ss*tt&
   - 2*ss**2*tt + 28*s15**2*sW2*tt + 36*s15*s25*sW2*tt + 8*s25**2*sW2*tt -&
   48*s15*s35*sW2*tt - 24*s25*s35*sW2*tt + 16*s35**2*sW2*tt - 48*s15*ss*sW2*tt -&
   24*s25*ss*sW2*tt + 40*s35*ss*sW2*tt + 16*ss**2*sW2*tt - 56*s15**2*sW2**2*tt -&
   72*s15*s25*sW2**2*tt - 16*s25**2*sW2**2*tt + 96*s15*s35*sW2**2*tt +&
   48*s25*s35*sW2**2*tt - 32*s35**2*sW2**2*tt + 96*s15*ss*sW2**2*tt +&
   48*s25*ss*sW2**2*tt - 80*s35*ss*sW2**2*tt - 32*ss**2*sW2**2*tt + 4*s15*tt**2 +&
   2*s25*tt**2 - 4*s35*tt**2 - 4*ss*tt**2 - 40*s15*sW2*tt**2 - 24*s25*sW2*tt**2 +&
   32*s35*sW2*tt**2 + 32*ss*sW2*tt**2 + 80*s15*sW2**2*tt**2 + 48*s25*sW2**2*tt**2 -&
   64*s35*sW2**2*tt**2 - 64*ss*sW2**2*tt**2 - 2*tt**3 + 16*sW2*tt**3 - 32*sW2**2*tt**3&
   + me2**2*(s15 - s25 - 2*s35 + 6*mm2*(1 - 4*sW2)**2 - 2*ss*(1 - 4*sW2)**2 -&
   16*s15*sW2 + 16*s35*sW2 + 32*s15*sW2**2 - 32*s35*sW2**2 - 6*tt + 48*sW2*tt -&
   96*sW2**2*tt) + mm2**2*(s25 - 4*s35 - 2*ss*(1 - 4*sW2)**2 - 16*s25*sW2 + 32*s35*sW2&
   + 32*s25*sW2**2 - 64*s35*sW2**2 + s15*(5 - 48*sW2 + 96*sW2**2) - 6*tt + 48*sW2*tt -&
   96*sW2**2*tt) + me2*(2*s15**2 + 2*s15*s25 - 3*s15*s35 + 6*mm2**2*(1 - 4*sW2)**2 -&
   4*s15**2*sW2 + 4*s15*s25*sW2 + 8*s25**2*sW2 + 8*s15*s35*sW2 - 8*s25*s35*sW2 +&
   8*s15**2*sW2**2 - 8*s15*s25*sW2**2 - 16*s25**2*sW2**2 - 16*s15*s35*sW2**2 +&
   16*s25*s35*sW2**2 - 5*s15*tt - s25*tt + 6*s35*tt + 56*s15*sW2*tt + 24*s25*sW2*tt -&
   48*s35*sW2*tt - 112*s15*sW2**2*tt - 48*s25*sW2**2*tt + 96*s35*sW2**2*tt + 6*tt**2 -&
   48*sW2*tt**2 + 96*sW2**2*tt**2 - 2*mm2*(-2*s25 + 3*s35 + 2*ss*(1 - 4*sW2)**2 +&
   8*s25*sW2 - 24*s35*sW2 - 16*s25*sW2**2 + 48*s35*sW2**2 + s15*(-5 + 32*sW2 -&
   64*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt) + ss*(-8*s25*sW2 + 16*s25*sW2**2 +&
   s15*(-3 + 8*sW2 - 16*sW2**2) + s35*(5 - 16*sW2 + 32*sW2**2) + 6*tt - 48*sW2*tt +&
   96*sW2**2*tt)) + mm2*(-2*s25*s35 + 2*s35**2 - 8*s25**2*sW2 + 24*s25*s35*sW2 -&
   16*s35**2*sW2 + 16*s25**2*sW2**2 - 48*s25*s35*sW2**2 + 32*s35**2*sW2**2 +&
   4*s15**2*(1 - 9*sW2 + 18*sW2**2) - 3*s25*tt + 8*s35*tt + 40*s25*sW2*tt -&
   64*s35*sW2*tt - 80*s25*sW2**2*tt + 128*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 +&
   96*sW2**2*tt**2 + ss*(8*s25*sW2 - 16*s25*sW2**2 + s15*(-7 + 40*sW2 - 80*sW2**2) +&
   s35*(5 - 32*sW2 + 64*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt) + s15*(-7*s35*(1 -&
   4*sW2)**2 + s25*(2 - 44*sW2 + 88*sW2**2) + (-9 + 88*sW2 -&
   176*sW2**2)*tt))))/(2.*s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   s1m*((e**6*s2n*(-(me2*(s15 + s25 - ss + 2*mm2*(1 - 4*sW2)**2)) - 2*mm2**2*(1 -&
   4*sW2)**2 + s15*(s15 + s25 - ss)*(1 - 4*sW2 + 8*sW2**2) + mm2*(s15 - s25 + 2*s35 +&
   ss + 24*s15*sW2 + 24*s25*sW2 - 16*s35*sW2 - 24*ss*sW2 - 48*s15*sW2**2 -&
   48*s25*sW2**2 + 32*s35*sW2**2 + 48*ss*sW2**2 + 2*tt - 16*sW2*tt +&
   32*sW2**2*tt)))/(2.*s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   (e**6*s4n*(me2**2*(3 - 8*sW2 + 16*sW2**2) + mm2**2*(3 - 16*sW2 + 32*sW2**2) +&
   mm2*(s25 - 5*s35 - 16*s25*sW2 + 24*s35*sW2 + 32*s25*sW2**2 - 48*s35*sW2**2 + ss*(-1&
   + 16*sW2 - 32*sW2**2) + 4*s15*(1 - 5*sW2 + 10*sW2**2) - 5*tt + 24*sW2*tt -&
   48*sW2**2*tt) + me2*(2*s15 + s25 - 5*s35 - ss - 4*s15*sW2 + 16*s35*sW2 +&
   8*s15*sW2**2 - 32*s35*sW2**2 + mm2*(2 - 24*sW2 + 48*sW2**2) - 5*tt + 16*sW2*tt -&
   32*sW2**2*tt) - (1 - 4*sW2 + 8*sW2**2)*(-s15 - s25 + s35 + ss + tt)*(s15 - 2*(s35 +&
   tt))))/(2.*s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2) - (e**6*me1*(-1 +&
   4*sW2)*(me2**2 + 3*mm2**2 + mm2*(5*s15 + 2*s25 - 4*s35 - 2*ss - 4*tt) - (s15 - s35 -&
   tt)*(-s15 - s25 + s35 + ss + tt) + me2*(4*mm2 + s15 - 2*(s35 + tt))))/(s15*(s15 +&
   s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*s3n*(mm2**2 + s15**2 - s15*s35 -&
   me2**2*(1 - 4*sW2)**2 + 12*s15**2*sW2 + 24*s15*s25*sW2 + 8*s25**2*sW2 -&
   20*s15*s35*sW2 - 16*s25*s35*sW2 + 8*s35**2*sW2 - 24*s15*ss*sW2 - 16*s25*ss*sW2 +&
   16*s35*ss*sW2 + 8*ss**2*sW2 - 24*s15**2*sW2**2 - 48*s15*s25*sW2**2 -&
   16*s25**2*sW2**2 + 40*s15*s35*sW2**2 + 32*s25*s35*sW2**2 - 16*s35**2*sW2**2 +&
   48*s15*ss*sW2**2 + 32*s25*ss*sW2**2 - 32*s35*ss*sW2**2 - 16*ss**2*sW2**2 - s15*tt -&
   20*s15*sW2*tt - 16*s25*sW2*tt + 16*s35*sW2*tt + 16*ss*sW2*tt + 40*s15*sW2**2*tt +&
   32*s25*sW2**2*tt - 32*s35*sW2**2*tt - 32*ss*sW2**2*tt + 8*sW2*tt**2 -&
   16*sW2**2*tt**2 + me2*(s35 + 20*s15*sW2 + 16*s25*sW2 - 16*s35*sW2 - 16*ss*sW2 -&
   40*s15*sW2**2 - 32*s25*sW2**2 + 32*s35*sW2**2 + 32*ss*sW2**2 - 4*mm2*(1 - 2*sW2 +&
   4*sW2**2) + tt - 16*sW2*tt + 32*sW2**2*tt) + mm2*(s15*(2 + 12*sW2 - 24*sW2**2) + (-1&
   - 8*sW2 + 16*sW2**2)*(s35 + tt))))/(2.*s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 +&
   sW2)*sW2)) + s4m*((e**6*s3n*(s15*ss*(-1 + 4*sW2 - 8*sW2**2) + mm2*ss*(-1 - 8*sW2 +&
   16*sW2**2) + me2*(8*mm2 + ss*(1 - 4*sW2)**2 + 2*(s15 + s25 + s35*(-1 + 4*sW2 -&
   8*sW2**2)))))/(2.*s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   (e**6*me1*(-1 + 4*sW2)*(mm2*(3*s15 + s25 - s35 + 2*ss) + me2*(-s15 - s25 + s35 +&
   2*ss) - ss*(-s15 - s25 + s35 + ss + tt)))/(s15*(s15 + s25 - s35)*(s15 + s25 -&
   ss)*(-1 + sW2)*sW2) + (e**6*s4n*(mm2*ss*(-2 + 4*sW2 - 8*sW2**2) + me2*(4*mm2 + s15 +&
   s25 - s35 + ss*(-2 + 4*sW2 - 8*sW2**2)) + ss*(1 - 4*sW2 + 8*sW2**2)*(-s15 - s25 +&
   s35 + ss + tt)))/(s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2) -&
   (e**6*s2n*(me2**2*(3 - 8*sW2 + 16*sW2**2) + mm2**2*(3 - 16*sW2 + 32*sW2**2) +&
   mm2*(2*s25 - 2*s35 - 8*s25*sW2 + 8*s35*sW2 + 16*s25*sW2**2 - 16*s35*sW2**2 - 2*ss*(3&
   - 8*sW2 + 16*sW2**2) + s15*(5 - 12*sW2 + 24*sW2**2) - 5*tt + 24*sW2*tt -&
   48*sW2**2*tt) + me2*(3*s15 + 2*s25 - 2*s35 - 4*s15*sW2 + 8*s35*sW2 + 8*s15*sW2**2 -&
   16*s35*sW2**2 - 4*ss*(1 - 2*sW2 + 4*sW2**2) + 2*mm2*(5 - 12*sW2 + 24*sW2**2) - 5*tt&
   + 16*sW2*tt - 32*sW2**2*tt) + (1 - 4*sW2 + 8*sW2**2)*(2*s15**2 + 2*ss**2 +&
   s15*(2*s25 - 2*s35 - 3*tt) - 2*ss*(2*s15 + s25 - s35 - 2*tt) + 2*tt*(-s25 + s35 +&
   tt))))/(2.*s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2)) -&
   (e**6*(2*me2**3*(1 - 4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 + me2**2*(3*s15 + s25 -&
   2*s35 + 6*mm2*(1 - 4*sW2)**2 - 16*s15*sW2 + 16*s35*sW2 + 32*s15*sW2**2 -&
   32*s35*sW2**2 - 4*ss*(1 - 4*sW2 + 8*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) +&
   mm2**2*(3*s25 - 4*s35 - 16*s25*sW2 + 32*s35*sW2 + 32*s25*sW2**2 - 64*s35*sW2**2 -&
   4*ss*(1 - 4*sW2 + 8*sW2**2) + s15*(7 - 48*sW2 + 96*sW2**2) - 6*tt + 48*sW2*tt -&
   96*sW2**2*tt) + me2*(s15**2 + s15*s25 - s15*s35 + 6*mm2**2*(1 - 4*sW2)**2 -&
   4*s15**2*sW2 + 4*s15*s25*sW2 + 8*s25**2*sW2 + 4*s15*s35*sW2 - 12*s25*s35*sW2 +&
   8*s15**2*sW2**2 - 8*s15*s25*sW2**2 - 16*s25**2*sW2**2 - 8*s15*s35*sW2**2 +&
   24*s25*s35*sW2**2 + 4*ss**2*(1 - 2*sW2 + 4*sW2**2) - 9*s15*tt - 5*s25*tt + 6*s35*tt&
   + 56*s15*sW2*tt + 24*s25*sW2*tt - 48*s35*sW2*tt - 112*s15*sW2**2*tt -&
   48*s25*sW2**2*tt + 96*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 -&
   2*mm2*(-4*s25 + 3*s35 + 8*s25*sW2 - 24*s35*sW2 - 16*s25*sW2**2 + 48*s35*sW2**2 +&
   s15*(-7 + 32*sW2 - 64*sW2**2) + 4*ss*(1 - 4*sW2 + 8*sW2**2) + 6*tt - 48*sW2*tt +&
   96*sW2**2*tt) + ss*(-4*s25 + 5*s35 - 16*s35*sW2 + 32*s35*sW2**2 + s15*(-7 + 16*sW2 -&
   32*sW2**2) + 10*tt - 48*sW2*tt + 96*sW2**2*tt)) + mm2*(2*s25**2 - 4*s25*s35 +&
   2*s35**2 - 8*s25**2*sW2 + 20*s25*s35*sW2 - 16*s35**2*sW2 + 16*s25**2*sW2**2 -&
   40*s25*s35*sW2**2 + 32*s35**2*sW2**2 + 4*ss**2*(1 - 2*sW2 + 4*sW2**2) + s15**2*(5 -&
   36*sW2 + 72*sW2**2) - 7*s25*tt + 8*s35*tt + 40*s25*sW2*tt - 64*s35*sW2*tt -&
   80*s25*sW2**2*tt + 128*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 +&
   ss*(7*s35 - 32*s35*sW2 + 64*s35*sW2**2 + s15*(-13 + 48*sW2 - 96*sW2**2) - 2*s25*(3 -&
   8*sW2 + 16*sW2**2) + 10*tt - 48*sW2*tt + 96*sW2**2*tt) + s15*(s35*(-7 + 52*sW2 -&
   104*sW2**2) + s25*(9 - 44*sW2 + 88*sW2**2) + (-13 + 88*sW2 - 176*sW2**2)*tt)) -&
   2*(s15**3*(2*sW2 - 4*sW2**2) + ss**3*(1 - 4*sW2 + 8*sW2**2) - ss**2*(1 - 4*sW2 +&
   8*sW2**2)*(3*s15 + 2*s25 - 2*s35 - 3*tt) + 2*s15**2*(-2*s35*sW2 + 4*s35*sW2**2 +&
   s25*(sW2 - 2*sW2**2) + tt - 7*sW2*tt + 14*sW2**2*tt) + s15*(s35**2*(2*sW2 -&
   4*sW2**2) + s25*s35*(-2*sW2 + 4*sW2**2) + s35*(-3 + 22*sW2 - 44*sW2**2)*tt +&
   3*s25*(1 - 6*sW2 + 12*sW2**2)*tt + (-3 + 20*sW2 - 40*sW2**2)*tt**2) + tt*(s25**2*(1&
   - 4*sW2 + 8*sW2**2) + (1 - 4*sW2)**2*(s35 + tt)**2 - 2*s25*(s35 - 5*s35*sW2 +&
   10*s35*sW2**2 + tt - 6*sW2*tt + 12*sW2**2*tt)) + ss*(s35**2 - 6*s35**2*sW2 +&
   12*s35**2*sW2**2 + s25**2*(1 - 4*sW2 + 8*sW2**2) + 2*s15**2*(1 - 5*sW2 + 10*sW2**2)&
   + 4*s35*tt - 20*s35*sW2*tt + 40*s35*sW2**2*tt + 3*tt**2 - 16*sW2*tt**2 +&
   32*sW2**2*tt**2 - 2*s25*(1 - 4*sW2 + 8*sW2**2)*(s35 + 2*tt) + s15*(s35*(-3 + 16*sW2&
   - 32*sW2**2) + 3*s25*(1 - 4*sW2 + 8*sW2**2) - 2*(3 - 14*sW2 +&
   28*sW2**2)*tt)))))/(s15*(s15 + s25 - s35)*(s15 + s25 - ss)*(-1 + sW2)*sW2)

  m16 =  -((e**6*me1*s3n*(-1 + 4*sW2)*(me2**2 - mm2**2 + 2*s15**2 + s15*s25 + mm2*(s15 + s25 -&
   s35) - 2*s15*s35 - s25*s35 + s35**2 - s15*ss + s25*ss + s35*ss - ss**2 + me2*(3*s15&
   + s25 - 3*s35 - 2*tt) - 3*s15*tt - s25*tt + 2*s35*tt + ss*tt +&
   tt**2))/(s15*s35*ss*(-1 + sW2)*sW2)) - (e**6*me1*s2n*(-1 + 4*sW2)*(4*mm2**2 +&
   me2*(4*mm2 + s15 + s25 - ss) + (-s15 - s25 + ss)*(-s15 - s25 + s35 + ss) -&
   mm2*(-3*s15 + s25 + 2*s35 + ss + 4*tt)))/(s15*s35*ss*(-1 + sW2)*sW2) +&
   (e**6*me1*s4n*(-1 + 4*sW2)*(me2**2 + 3*mm2**2 + 2*s15**2 + 3*s15*s25 + s25**2 -&
   3*s15*s35 - 2*s25*s35 + s35**2 - 3*s15*ss - 2*s25*ss + 3*s35*ss + ss**2 + me2*(4*mm2&
   + 3*s15 + 2*s25 - 3*s35 - 3*ss - 2*tt) - 2*s15*tt - s25*tt + 2*s35*tt + 2*ss*tt +&
   tt**2 - mm2*(-3*s15 + 3*s35 + 3*ss + 4*tt)))/(s15*s35*ss*(-1 + sW2)*sW2) +&
   s4m*((e**6*s3n*(2*me2**2 + mm2*ss*(1 - 4*sW2)**2 + s15*ss*(-1 + 4*sW2 - 8*sW2**2) +&
   me2*(2*mm2 - ss*(1 - 4*sW2)**2 + 2*(s25 - s35 - 4*s25*sW2 + 4*s35*sW2 + 8*s25*sW2**2&
   - 8*s35*sW2**2 + s15*(2 - 4*sW2 + 8*sW2**2) - tt))))/(2.*s15*s35*ss*(-1 + sW2)*sW2)&
   + (e**6*s4n*(mm2*ss*(1 - 12*sW2 + 24*sW2**2) + me2*(-s15 - s25 + s35 + ss - 4*ss*sW2&
   + 8*ss*sW2**2) - 4*ss*sW2*(-1 + 2*sW2)*(s35 + tt)))/(s15*s35*ss*(-1 + sW2)*sW2) +&
   (e**6*me1*(-1 + 4*sW2)*(me2**2 + 3*mm2**2 + mm2*(4*s15 + s25 - s35 - 4*tt) +&
   me2*(4*mm2 + 2*s15 + s25 - s35 - 2*ss - 2*tt) + tt*(-s15 + ss +&
   tt)))/(s15*s35*ss*(-1 + sW2)*sW2) + (e**6*s2n*(me2**2*(1 - 4*sW2)**2 + 8*s15*s35*sW2&
   - 8*s35*ss*sW2 - 16*s15*s35*sW2**2 + 16*s35*ss*sW2**2 + mm2**2*(1 - 16*sW2 +&
   32*sW2**2) - s15*tt + 12*s15*sW2*tt - 8*s35*sW2*tt - 8*ss*sW2*tt - 24*s15*sW2**2*tt&
   + 16*s35*sW2**2*tt + 16*ss*sW2**2*tt - 8*sW2*tt**2 + 16*sW2**2*tt**2 + me2*(3*s15 +&
   2*s25 - 2*s35 - 12*s15*sW2 + 8*s35*sW2 + 24*s15*sW2**2 - 16*s35*sW2**2 + 6*mm2*(1 -&
   4*sW2 + 8*sW2**2) - 2*ss*(1 - 4*sW2 + 8*sW2**2) - tt + 16*sW2*tt - 32*sW2**2*tt) -&
   mm2*(-8*s35*sW2 + 16*s35*sW2**2 + s15*(-3 + 28*sW2 - 56*sW2**2) + ss*(2 - 24*sW2 +&
   48*sW2**2) + tt - 24*sW2*tt + 48*sW2**2*tt)))/(2.*s15*s35*ss*(-1 + sW2)*sW2)) +&
   (e**6*(2*s15**3 + 3*s15**2*s25 + s15*s25**2 - 3*s15**2*s35 - 2*s15*s25*s35 +&
   s15*s35**2 - 4*s15**2*ss - 3*s15*s25*ss + 5*s15*s35*ss + s25*s35*ss - s35**2*ss +&
   2*s15*ss**2 - 2*s35*ss**2 + 2*me2**3*(1 - 4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 -&
   8*s15**3*sW2 - 12*s15**2*s25*sW2 - 4*s15*s25**2*sW2 + 20*s15**2*s35*sW2 +&
   8*s15*s25*s35*sW2 - 12*s15*s35**2*sW2 + 16*s15**2*ss*sW2 + 12*s15*s25*ss*sW2 -&
   20*s15*s35*ss*sW2 - 4*s25*s35*ss*sW2 + 4*s35**2*ss*sW2 - 8*s15*ss**2*sW2 +&
   8*s35*ss**2*sW2 + 16*s15**3*sW2**2 + 24*s15**2*s25*sW2**2 + 8*s15*s25**2*sW2**2 -&
   40*s15**2*s35*sW2**2 - 16*s15*s25*s35*sW2**2 + 24*s15*s35**2*sW2**2 -&
   32*s15**2*ss*sW2**2 - 24*s15*s25*ss*sW2**2 + 40*s15*s35*ss*sW2**2 +&
   8*s25*s35*ss*sW2**2 - 8*s35**2*ss*sW2**2 + 16*s15*ss**2*sW2**2 - 16*s35*ss**2*sW2**2&
   - 6*s15**2*tt - 5*s15*s25*tt - s25**2*tt + 7*s15*s35*tt + 3*s25*s35*tt - 2*s35**2*tt&
   + 8*s15*ss*tt + 2*s25*ss*tt - 6*s35*ss*tt - 2*ss**2*tt + 32*s15**2*sW2*tt +&
   20*s15*s25*sW2*tt + 4*s25**2*sW2*tt - 52*s15*s35*sW2*tt - 12*s25*s35*sW2*tt +&
   16*s35**2*sW2*tt - 32*s15*ss*sW2*tt - 8*s25*ss*sW2*tt + 24*s35*ss*sW2*tt +&
   8*ss**2*sW2*tt - 64*s15**2*sW2**2*tt - 40*s15*s25*sW2**2*tt - 8*s25**2*sW2**2*tt +&
   104*s15*s35*sW2**2*tt + 24*s25*s35*sW2**2*tt - 32*s35**2*sW2**2*tt +&
   64*s15*ss*sW2**2*tt + 16*s25*ss*sW2**2*tt - 48*s35*ss*sW2**2*tt - 16*ss**2*sW2**2*tt&
   + 6*s15*tt**2 + 2*s25*tt**2 - 4*s35*tt**2 - 4*ss*tt**2 - 40*s15*sW2*tt**2 -&
   8*s25*sW2*tt**2 + 32*s35*sW2*tt**2 + 16*ss*sW2*tt**2 + 80*s15*sW2**2*tt**2 +&
   16*s25*sW2**2*tt**2 - 64*s35*sW2**2*tt**2 - 32*ss*sW2**2*tt**2 - 2*tt**3 +&
   16*sW2*tt**3 - 32*sW2**2*tt**3 + me2**2*(5*s15 + s25 - 6*s35 - 2*ss + 6*mm2*(1 -&
   4*sW2)**2 - 32*s15*sW2 + 48*s35*sW2 + 64*s15*sW2**2 - 96*s35*sW2**2 - 6*tt +&
   48*sW2*tt - 96*sW2**2*tt) + mm2**2*(s25 - 4*s35 - 2*ss + 32*s35*sW2 - 64*s35*sW2**2&
   + s15*(7 - 48*sW2 + 96*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) + me2*(5*s15**2 +&
   4*s15*s25 + s25**2 - 8*s15*s35 - 3*s25*s35 + 2*s35**2 + 6*mm2**2*(1 - 4*sW2)**2 -&
   24*s15**2*sW2 - 12*s15*s25*sW2 - 4*s25**2*sW2 + 60*s15*s35*sW2 + 12*s25*s35*sW2 -&
   32*s35**2*sW2 + 48*s15**2*sW2**2 + 24*s15*s25*sW2**2 + 8*s25**2*sW2**2 -&
   120*s15*s35*sW2**2 - 24*s25*s35*sW2**2 + 64*s35**2*sW2**2 + 2*ss**2*(1 - 4*sW2 +&
   8*sW2**2) - 11*s15*tt - 3*s25*tt + 10*s35*tt + 72*s15*sW2*tt + 8*s25*sW2*tt -&
   80*s35*sW2*tt - 144*s15*sW2**2*tt - 16*s25*sW2**2*tt + 160*s35*sW2**2*tt + 6*tt**2 -&
   48*sW2*tt**2 + 96*sW2**2*tt**2 + ss*(7*s35 - 16*s35*sW2 + 32*s35*sW2**2 + s15*(-7 +&
   24*sW2 - 48*sW2**2) - 2*s25*(1 - 4*sW2 + 8*sW2**2) + 6*tt - 16*sW2*tt +&
   32*sW2**2*tt) - 2*mm2*(s25 + 5*s35 + 2*ss - 40*s35*sW2 + 80*s35*sW2**2 + s15*(-4 +&
   40*sW2 - 80*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt)) + mm2*(s25**2 - 3*s25*s35 +&
   2*s35**2 - 4*s25**2*sW2 + 12*s25*s35*sW2 - 16*s35**2*sW2 + 8*s25**2*sW2**2 -&
   24*s25*s35*sW2**2 + 32*s35**2*sW2**2 + 2*ss**2*(1 - 4*sW2 + 8*sW2**2) + s15**2*(7 -&
   40*sW2 + 80*sW2**2) - 3*s25*tt + 8*s35*tt + 8*s25*sW2*tt - 64*s35*sW2*tt -&
   16*s25*sW2**2*tt + 128*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 +&
   ss*(5*s35 - 16*s35*sW2 + 32*s35*sW2**2 + s15*(-7 + 24*sW2 - 48*sW2**2) - 2*s25*(1 -&
   4*sW2 + 8*sW2**2) + 6*tt - 16*sW2*tt + 32*sW2**2*tt) + s15*(4*s25*(1 - 3*sW2 +&
   6*sW2**2) - 4*s35*(2 - 15*sW2 + 30*sW2**2) + (-13 + 88*sW2 -&
   176*sW2**2)*tt))))/(s15*s35*ss*(-1 + sW2)*sW2) + s3m*((e**6*s4n*(-2*me2**2 +&
   mm2*ss*(1 - 4*sW2)**2 + s15*ss*(-1 + 4*sW2 - 8*sW2**2) + me2*(-2*mm2 + ss*(3 - 8*sW2&
   + 16*sW2**2) - 2*(s25 - 2*s35 - 4*s25*sW2 + 4*s35*sW2 + 8*s25*sW2**2 - 8*s35*sW2**2&
   + s15*(2 - 4*sW2 + 8*sW2**2) - tt))))/(2.*s15*s35*ss*(-1 + sW2)*sW2) + (e**6*s3n*(1&
   - 4*sW2 + 8*sW2**2)*(me2 + mm2 + s15 - s35 - tt))/(s15*s35*(-1 + sW2)*sW2) -&
   (e**6*me1*(-1 + 4*sW2)*(me2**2 - mm2**2 + 2*s15**2 + s15*s25 - s15*s35 + mm2*(s15 +&
   s25 - s35 - 3*ss) - 3*s15*ss + 2*s35*ss + me2*(3*s15 + s25 - 3*s35 - ss - 2*tt) -&
   3*s15*tt - s25*tt + 2*s35*tt + 2*ss*tt + tt**2))/(s15*s35*ss*(-1 + sW2)*sW2) -&
   (e**6*s2n*(mm2**2 + me2**2*(1 - 4*sW2)**2 + me2*(-2*s25 - 2*s35 - 4*s15*sW2 +&
   8*s25*sW2 + 8*s35*sW2 + 8*s15*sW2**2 - 16*s25*sW2**2 - 16*s35*sW2**2 + 2*mm2*(-1 -&
   4*sW2 + 8*sW2**2) + ss*(3 - 8*sW2 + 16*sW2**2) - 3*tt + 16*sW2*tt - 32*sW2**2*tt) +&
   mm2*(-2*s35 + ss*(1 - 4*sW2)**2 + 8*s35*sW2 - 16*s35*sW2**2 + s15*(2 - 4*sW2 +&
   8*sW2**2) - 3*tt + 8*sW2*tt - 16*sW2**2*tt) - (1 - 4*sW2 + 8*sW2**2)*(-s15**2 +&
   s15*ss + 3*s15*tt - 2*tt*(s35 + tt))))/(2.*s15*s35*ss*(-1 + sW2)*sW2)) -&
   (e**6*snm*(s15**3 + s15**2*s25 - s15**2*s35 - s15**2*ss + s15*s35*ss + 2*me2**3*(1 -&
   4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 - 4*s15**3*sW2 - 4*s15**2*s25*sW2 +&
   12*s15**2*s35*sW2 - 8*s15*s35**2*sW2 + 4*s15**2*ss*sW2 - 12*s15*s35*ss*sW2 +&
   8*s35**2*ss*sW2 + 8*s15**3*sW2**2 + 8*s15**2*s25*sW2**2 - 24*s15**2*s35*sW2**2 +&
   16*s15*s35**2*sW2**2 - 8*s15**2*ss*sW2**2 + 24*s15*s35*ss*sW2**2 -&
   16*s35**2*ss*sW2**2 - 5*s15**2*tt - 3*s15*s25*tt + 6*s15*s35*tt + 2*s25*s35*tt -&
   2*s35**2*tt + 4*s15*ss*tt - 2*s35*ss*tt + 28*s15**2*sW2*tt + 12*s15*s25*sW2*tt -&
   48*s15*s35*sW2*tt - 8*s25*s35*sW2*tt + 16*s35**2*sW2*tt - 24*s15*ss*sW2*tt +&
   24*s35*ss*sW2*tt - 56*s15**2*sW2**2*tt - 24*s15*s25*sW2**2*tt + 96*s15*s35*sW2**2*tt&
   + 16*s25*s35*sW2**2*tt - 32*s35**2*sW2**2*tt + 48*s15*ss*sW2**2*tt -&
   48*s35*ss*sW2**2*tt + 6*s15*tt**2 + 2*s25*tt**2 - 4*s35*tt**2 - 2*ss*tt**2 -&
   40*s15*sW2*tt**2 - 8*s25*sW2*tt**2 + 32*s35*sW2*tt**2 + 16*ss*sW2*tt**2 +&
   80*s15*sW2**2*tt**2 + 16*s25*sW2**2*tt**2 - 64*s35*sW2**2*tt**2 - 32*ss*sW2**2*tt**2&
   - 2*tt**3 + 16*sW2*tt**3 - 32*sW2**2*tt**3 + me2**2*(s25 - 6*s35 + 6*mm2*(1 -&
   4*sW2)**2 + 48*s35*sW2 - 96*s35*sW2**2 + s15*(5 - 32*sW2 + 64*sW2**2) - 6*tt +&
   48*sW2*tt - 96*sW2**2*tt) + mm2**2*(s25 + s15*(7 - 48*sW2 + 96*sW2**2) - 2*(1 -&
   4*sW2)**2*(2*s35 + 3*tt)) + mm2*(-2*s25*s35 + 2*s35**2 + 8*s25*s35*sW2 -&
   16*s35**2*sW2 - 16*s25*s35*sW2**2 + 32*s35**2*sW2**2 + 6*s15**2*(1 - 6*sW2 +&
   12*sW2**2) - 3*s25*tt + 8*s35*tt + 8*s25*sW2*tt - 64*s35*sW2*tt - 16*s25*sW2**2*tt +&
   128*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 + ss*(s15*(-1 + 16*sW2&
   - 32*sW2**2) + s35*(1 - 16*sW2 + 32*sW2**2) + 2*(1 - 4*sW2)**2*tt) + s15*(-7*s35*(1&
   - 4*sW2)**2 + s25*(2 - 4*sW2 + 8*sW2**2) + (-13 + 88*sW2 - 176*sW2**2)*tt)) +&
   me2*(4*s15**2 + 2*s15*s25 - 9*s15*s35 - 4*s25*s35 + 4*s35**2 - 3*s15*ss + 3*s35*ss +&
   6*mm2**2*(1 - 4*sW2)**2 - 20*s15**2*sW2 - 4*s15*s25*sW2 + 56*s15*s35*sW2 +&
   8*s25*s35*sW2 - 32*s35**2*sW2 + 16*s15*ss*sW2 - 16*s35*ss*sW2 + 40*s15**2*sW2**2 +&
   8*s15*s25*sW2**2 - 112*s15*s35*sW2**2 - 16*s25*s35*sW2**2 + 64*s35**2*sW2**2 -&
   32*s15*ss*sW2**2 + 32*s35*ss*sW2**2 - 11*s15*tt - 3*s25*tt + 10*s35*tt + 2*ss*tt +&
   72*s15*sW2*tt + 8*s25*sW2*tt - 80*s35*sW2*tt - 16*ss*sW2*tt - 144*s15*sW2**2*tt -&
   16*s25*sW2**2*tt + 160*s35*sW2**2*tt + 32*ss*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 +&
   96*sW2**2*tt**2 - 2*mm2*(s25 + s15*(-4 + 40*sW2 - 80*sW2**2) + (1 - 4*sW2)**2*(5*s35&
   + 6*tt)))))/(2.*s15*s35*ss*(-1 + sW2)*sW2) + s1m*((e**6*me1*(-1 + 4*sW2)*(s15**2 +&
   s15*s25 - s15*s35 - s25*s35 + s35**2 - s15*ss + me2*(-s35 + ss) - mm2*(-2*s25 + s35&
   + ss) - s15*tt - s25*tt + s35*tt))/(s15*s35*ss*(-1 + sW2)*sW2) + (e**6*s4n*(-s15**2&
   - s15*s25 + s15*s35 + s15*ss - me2**2*(1 - 4*sW2)**2 + 4*s15**2*sW2 + 4*s15*s25*sW2&
   - 12*s15*s35*sW2 + 8*s35**2*sW2 - 4*s15*ss*sW2 - 8*s15**2*sW2**2 - 8*s15*s25*sW2**2&
   + 24*s15*s35*sW2**2 - 16*s35**2*sW2**2 + 8*s15*ss*sW2**2 + mm2**2*(-1 + 16*sW2 -&
   32*sW2**2) + s15*tt - 12*s15*sW2*tt + 16*s35*sW2*tt + 24*s15*sW2**2*tt -&
   32*s35*sW2**2*tt + 8*sW2*tt**2 - 16*sW2**2*tt**2 + me2*(s25 + s35 - ss + 12*s15*sW2&
   - 16*s35*sW2 - 24*s15*sW2**2 + 32*s35*sW2**2 + mm2*(-2 + 24*sW2 - 48*sW2**2) + tt -&
   16*sW2*tt + 32*sW2**2*tt) + mm2*(s25 + s35 - ss*(1 - 4*sW2)**2 - 8*s25*sW2 -&
   24*s35*sW2 + 16*s25*sW2**2 + 48*s35*sW2**2 + s15*(-2 + 20*sW2 - 40*sW2**2) + tt -&
   24*sW2*tt + 48*sW2**2*tt)))/(2.*s15*s35*ss*(-1 + sW2)*sW2) + (e**6*s2n*(2*mm2**2*(1&
   - 4*sW2)**2 + s15*(s15 + s25 - ss)*(1 - 4*sW2 + 8*sW2**2) + me2*(-s15 - s25 + ss +&
   2*mm2*(-1 - 8*sW2 + 16*sW2**2)) + mm2*(ss*(1 - 4*sW2)**2 + s15*(3 - 8*sW2 +&
   16*sW2**2) - (1 - 4*sW2)**2*(s25 + 2*(s35 + tt)))))/(2.*s15*s35*ss*(-1 + sW2)*sW2) +&
   (e**6*s3n*(me2**2*(1 - 4*sW2)**2 + me2*(2*mm2*(1 - 4*sW2 + 8*sW2**2) + 2*s15*(1 -&
   6*sW2 + 12*sW2**2) - (3 - 16*sW2 + 32*sW2**2)*(s35 + tt)) + (mm2 + s15 - s35 -&
   tt)*(mm2 - (1 - 4*sW2 + 8*sW2**2)*(-3*s15 + 2*ss + 2*(-s25 + s35 +&
   tt)))))/(2.*s15*s35*ss*(-1 + sW2)*sW2))

  m18 = -((e**6*me1*s2n*(-1 + 4*sW2)*(4*mm2**2 - 3*s15**2 - 5*s15*s25 - 2*s25**2 + 5*s15*s35 +&
   4*s25*s35 - 2*s35**2 + 4*s15*ss + 3*s25*ss - 3*s35*ss - ss**2 + me2*(4*mm2 - 3*s15 -&
   3*s25 + 2*s35 + 2*ss) + mm2*(3*s15 + s25 - 4*ss - 4*tt) + 2*s15*tt + 2*s25*tt -&
   2*s35*tt - ss*tt))/(s15*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2)) - (e**6*me1*s3n*(-1 +&
   4*sW2)*(me2**2 - mm2**2 + 3*s15**2 + 5*s15*s25 + 2*s25**2 + mm2*(2*s15 + s25 -&
   2*s35) - 5*s15*s35 - 4*s25*s35 + 2*s35**2 - 6*s15*ss - 4*s25*ss + 4*s35*ss + 2*ss**2&
   + me2*(4*s15 + 3*s25 - 2*s35 - 4*ss - 2*tt) - 4*s15*tt - 3*s25*tt + 3*s35*tt +&
   3*ss*tt + tt**2))/(s15*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) + (e**6*me1*s4n*(-1 +&
   4*sW2)*(me2**2 + 3*mm2**2 + mm2*(-5*ss + 2*(s15 + s25 - s35 - 2*tt)) + me2*(4*mm2 -&
   ss - 2*tt) + (ss + tt)*(-s15 - s25 + s35 + ss + tt)))/(s15*(s15 + s25 - s35)*ss*(-1&
   + sW2)*sW2) + s4m*((e**6*s3n*(2*me2**2 - ss*(mm2*(1 - 4*sW2)**2 + 4*s15*sW2 -&
   8*s15*sW2**2) + me2*(2*mm2 - ss*(1 - 4*sW2)**2 + 2*(s15 + s25 - s35 - 4*s35*sW2 +&
   8*s35*sW2**2 - tt))))/(2.*s15*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) + (4*e**6*s4n*(-1&
   + 2*sW2)*(me2 + mm2 + s15 + s25 - s35 - ss - tt))/(s15*(s15 + s25 - s35)*(-1 + sW2))&
   + (e**6*me1*(-1 + 4*sW2)*(me2**2 + 3*mm2**2 + me2*(4*mm2 - 2*tt) + tt*(-s15 - s25 +&
   s35 + ss + tt) - 2*mm2*(-s15 - s25 + s35 + ss + 2*tt)))/(s15*(s15 + s25 -&
   s35)*ss*(-1 + sW2)*sW2) + (e**6*s2n*(me2**2*(-1 - 8*sW2 + 16*sW2**2) + mm2**2*(1 -&
   16*sW2 + 32*sW2**2) + me2*(-4*s15*sW2 + 8*s35*sW2 + 8*ss*sW2 + 8*s15*sW2**2 -&
   16*s35*sW2**2 - 16*ss*sW2**2 + 4*mm2*(1 - 6*sW2 + 12*sW2**2) + tt + 16*sW2*tt -&
   32*sW2**2*tt) - mm2*(12*s15*sW2 + 8*s25*sW2 - 8*s35*sW2 - 24*s15*sW2**2 -&
   16*s25*sW2**2 + 16*s35*sW2**2 + 16*ss*sW2*(-1 + 2*sW2) + tt - 24*sW2*tt +&
   48*sW2**2*tt) + 4*sW2*(-1 + 2*sW2)*(2*s15**2 + 2*ss**2 + s15*(2*s25 - 2*s35 - 3*tt)&
   - 2*ss*(2*s15 + s25 - s35 - 2*tt) + 2*tt*(-s25 + s35 + tt))))/(2.*s15*(s15 + s25 -&
   s35)*ss*(-1 + sW2)*sW2)) + s3m*(-(e**6*s4n*(2*me2**2 + ss*(mm2*(1 - 4*sW2)**2 +&
   4*s15*sW2 - 8*s15*sW2**2) + me2*(2*mm2 - 8*s35*sW2 + 16*s35*sW2**2 + ss*(-3 + 8*sW2&
   - 16*sW2**2) - 2*tt)))/(2.*s15*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) - (e**6*me1*(-1&
   + 4*sW2)*(me2**2 - mm2**2 + mm2*(3*s15 + 2*s25 - 3*ss) - 6*s15*ss - 4*s25*ss +&
   3*s35*ss + 3*ss**2 + me2*(3*s15 + 2*s25 - 5*ss - 2*tt) - 3*s15*tt - 2*s25*tt +&
   s35*tt + 4*ss*tt + tt**2))/(s15*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) -&
   (e**6*s3n*(me2*(s35 + ss*(-2 + 4*sW2 - 8*sW2**2)) + ss*(-2*s15 - s25 + s35 + ss -&
   4*mm2*sW2 + 8*s15*sW2 + 4*s25*sW2 - 4*s35*sW2 - 4*ss*sW2 + 8*mm2*sW2**2 -&
   16*s15*sW2**2 - 8*s25*sW2**2 + 8*s35*sW2**2 + 8*ss*sW2**2 + tt - 4*sW2*tt +&
   8*sW2**2*tt)))/(s15*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) - (e**6*s2n*(mm2**2 +&
   me2**2*(3 - 8*sW2 + 16*sW2**2) + mm2*(2*s25 - 2*s35 - 8*s25*sW2 + 8*s35*sW2 +&
   16*s25*sW2**2 - 16*s35*sW2**2 + ss*(-3 + 16*sW2 - 32*sW2**2) + 5*s15*(1 - 4*sW2 +&
   8*sW2**2) - 3*tt + 8*sW2*tt - 16*sW2**2*tt) - me2*(-2*s25 + 4*s35 + ss*(1 -&
   4*sW2)**2 + 8*mm2*sW2 + 8*s25*sW2 - 8*s35*sW2 - 16*mm2*sW2**2 - 16*s25*sW2**2 +&
   16*s35*sW2**2 + s15*(-5 + 12*sW2 - 24*sW2**2) + 5*tt - 16*sW2*tt + 32*sW2**2*tt) +&
   2*(s15**2*(2*sW2 - 4*sW2**2) - 2*s15*(1 - 3*sW2 + 6*sW2**2)*tt - (1 - 4*sW2 +&
   8*sW2**2)*(s25 - s35 - tt)*tt + ss*(-2*s15*sW2 + 4*s15*sW2**2 + tt - 4*sW2*tt +&
   8*sW2**2*tt))))/(2.*s15*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2)) -&
   (e**6*snm*(2*me2**3*(1 - 4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 + me2**2*(3*s15 + s25 -&
   2*s35 + 6*mm2*(1 - 4*sW2)**2 - 2*ss*(1 - 4*sW2)**2 - 16*s15*sW2 + 16*s35*sW2 +&
   32*s15*sW2**2 - 32*s35*sW2**2 - 6*tt + 48*sW2*tt - 96*sW2**2*tt) + mm2**2*(3*s25 -&
   4*s35 - 2*ss*(1 - 4*sW2)**2 - 16*s25*sW2 + 32*s35*sW2 + 32*s25*sW2**2 -&
   64*s35*sW2**2 + s15*(7 - 48*sW2 + 96*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) +&
   me2*(-s15**2 - 3*s15*s25 - 2*s25**2 + s15*s35 + 2*s25*s35 + 6*mm2**2*(1 - 4*sW2)**2&
   - 4*s15**2*sW2 + 4*s15*s25*sW2 + 8*s25**2*sW2 + 8*s15*s35*sW2 - 8*s25*s35*sW2 +&
   8*s15**2*sW2**2 - 8*s15*s25*sW2**2 - 16*s25**2*sW2**2 - 16*s15*s35*sW2**2 +&
   16*s25*s35*sW2**2 - 9*s15*tt - 5*s25*tt + 6*s35*tt + 56*s15*sW2*tt + 24*s25*sW2*tt -&
   48*s35*sW2*tt - 112*s15*sW2**2*tt - 48*s25*sW2**2*tt + 96*s35*sW2**2*tt + 6*tt**2 -&
   48*sW2*tt**2 + 96*sW2**2*tt**2 - 2*mm2*(3*s35 + 2*ss*(1 - 4*sW2)**2 + 8*s25*sW2 -&
   24*s35*sW2 - 16*s25*sW2**2 + 48*s35*sW2**2 + s15*(-3 + 32*sW2 - 64*sW2**2) + 6*tt -&
   48*sW2*tt + 96*sW2**2*tt) + ss*(-s35 - 16*s35*sW2 + 32*s35*sW2**2 + s15*(1 + 8*sW2 -&
   16*sW2**2) + 2*s25*(1 - 4*sW2 + 8*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt)) +&
   mm2*(2*s25**2 - 4*s25*s35 + 2*s35**2 - 8*s25**2*sW2 + 24*s25*s35*sW2 - 16*s35**2*sW2&
   + 16*s25**2*sW2**2 - 48*s25*s35*sW2**2 + 32*s35**2*sW2**2 + s15**2*(5 - 36*sW2 +&
   72*sW2**2) - 7*s25*tt + 8*s35*tt + 40*s25*sW2*tt - 64*s35*sW2*tt - 80*s25*sW2**2*tt&
   + 128*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 + ss*(3*s35 -&
   32*s35*sW2 + 64*s35*sW2**2 + s15*(-3 + 40*sW2 - 80*sW2**2) - 2*s25*(1 - 4*sW2 +&
   8*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt) + s15*(-7*s35*(1 - 4*sW2)**2 + s25*(7 -&
   44*sW2 + 88*sW2**2) + (-13 + 88*sW2 - 176*sW2**2)*tt)) + 2*(s15**3*(-2*sW2 +&
   4*sW2**2) + ss**2*(s35*(4*sW2 - 8*sW2**2) + s15*(-4*sW2 + 8*sW2**2) - (1 -&
   4*sW2)**2*tt) - 2*s15**2*(-3*s35*sW2 + 6*s35*sW2**2 + s25*(sW2 - 2*sW2**2) + tt -&
   7*sW2*tt + 14*sW2**2*tt) + s15*(s25*s35*(4*sW2 - 8*sW2**2) + s35**2*(-4*sW2 +&
   8*sW2**2) + 3*s35*(1 - 4*sW2)**2*tt - 3*s25*(1 - 6*sW2 + 12*sW2**2)*tt + (3 - 20*sW2&
   + 40*sW2**2)*tt**2) - tt*(s25**2*(1 - 4*sW2 + 8*sW2**2) - 2*s25*(1 - 6*sW2 +&
   12*sW2**2)*(s35 + tt) + (1 - 4*sW2)**2*(s35 + tt)**2) + ss*(4*s35**2*sW2 -&
   8*s35**2*sW2**2 + s15**2*(6*sW2 - 12*sW2**2) - 2*s35*tt + 20*s35*sW2*tt -&
   40*s35*sW2**2*tt - 2*tt**2 + 16*sW2*tt**2 - 32*sW2**2*tt**2 + s15*(10*s35*sW2*(-1 +&
   2*sW2) + s25*(4*sW2 - 8*sW2**2) + 3*(1 - 4*sW2)**2*tt) + 2*s25*(-2*s35*sW2 +&
   4*s35*sW2**2 + tt - 6*sW2*tt + 12*sW2**2*tt)))))/(2.*s15*(s15 + s25 - s35)*ss*(-1 +&
   sW2)*sW2) + (e**6*(2*me2**3*(1 - 4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 + me2**2*(3*s15&
   + s25 - 2*s35 + 6*mm2*(1 - 4*sW2)**2 - 16*s15*sW2 + 16*s35*sW2 + 32*s15*sW2**2 -&
   32*s35*sW2**2 - 4*ss*(1 - 4*sW2 + 8*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) +&
   mm2**2*(3*s25 - 4*s35 - 16*s25*sW2 + 32*s35*sW2 + 32*s25*sW2**2 - 64*s35*sW2**2 -&
   4*ss*(1 - 4*sW2 + 8*sW2**2) + s15*(7 - 48*sW2 + 96*sW2**2) - 6*tt + 48*sW2*tt -&
   96*sW2**2*tt) + me2*(-s15**2 - 3*s15*s25 - 2*s25**2 + 3*s15*s35 + 4*s25*s35 -&
   2*s35**2 + 6*mm2**2*(1 - 4*sW2)**2 - 4*s15**2*sW2 + 4*s15*s25*sW2 + 8*s25**2*sW2 +&
   4*s15*s35*sW2 - 12*s25*s35*sW2 + 8*s15**2*sW2**2 - 8*s15*s25*sW2**2 -&
   16*s25**2*sW2**2 - 8*s15*s35*sW2**2 + 24*s25*s35*sW2**2 + 4*ss**2*(1 - 2*sW2 +&
   4*sW2**2) - 9*s15*tt - 5*s25*tt + 6*s35*tt + 56*s15*sW2*tt + 24*s25*sW2*tt -&
   48*s35*sW2*tt - 112*s15*sW2**2*tt - 48*s25*sW2**2*tt + 96*s35*sW2**2*tt + 6*tt**2 -&
   48*sW2*tt**2 + 96*sW2**2*tt**2 - 2*mm2*(3*s35 + 8*s25*sW2 - 24*s35*sW2 -&
   16*s25*sW2**2 + 48*s35*sW2**2 + s15*(-3 + 32*sW2 - 64*sW2**2) + 4*ss*(1 - 4*sW2 +&
   8*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt) + ss*(-2*s25 + 3*s35 - 16*s35*sW2 +&
   32*s35*sW2**2 + s15*(-5 + 16*sW2 - 32*sW2**2) + 10*tt - 48*sW2*tt + 96*sW2**2*tt)) +&
   mm2*(2*s25**2 - 4*s25*s35 + 2*s35**2 - 8*s25**2*sW2 + 20*s25*s35*sW2 - 16*s35**2*sW2&
   + 16*s25**2*sW2**2 - 40*s25*s35*sW2**2 + 32*s35**2*sW2**2 + 4*ss**2*(1 - 2*sW2 +&
   4*sW2**2) + s15**2*(5 - 36*sW2 + 72*sW2**2) - 7*s25*tt + 8*s35*tt + 40*s25*sW2*tt -&
   64*s35*sW2*tt - 80*s25*sW2**2*tt + 128*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 +&
   96*sW2**2*tt**2 + ss*(7*s35 - 32*s35*sW2 + 64*s35*sW2**2 + s15*(-11 + 48*sW2 -&
   96*sW2**2) - 2*s25*(3 - 8*sW2 + 16*sW2**2) + 10*tt - 48*sW2*tt + 96*sW2**2*tt) +&
   s15*(s35*(-7 + 52*sW2 - 104*sW2**2) + s25*(7 - 44*sW2 + 88*sW2**2) + (-13 + 88*sW2 -&
   176*sW2**2)*tt)) - 2*(s15**3*(2*sW2 - 4*sW2**2) + ss**3*(1 - 4*sW2 + 8*sW2**2) -&
   ss**2*(1 - 4*sW2 + 8*sW2**2)*(3*s15 + 2*s25 - 2*s35 - 3*tt) + 2*s15**2*(-2*s35*sW2 +&
   4*s35*sW2**2 + s25*(sW2 - 2*sW2**2) + tt - 7*sW2*tt + 14*sW2**2*tt) +&
   s15*(s35**2*(2*sW2 - 4*sW2**2) + s25*s35*(-2*sW2 + 4*sW2**2) + s35*(-3 + 22*sW2 -&
   44*sW2**2)*tt + 3*s25*(1 - 6*sW2 + 12*sW2**2)*tt + (-3 + 20*sW2 - 40*sW2**2)*tt**2)&
   + tt*(s25**2*(1 - 4*sW2 + 8*sW2**2) + (1 - 4*sW2)**2*(s35 + tt)**2 - 2*s25*(s35 -&
   5*s35*sW2 + 10*s35*sW2**2 + tt - 6*sW2*tt + 12*sW2**2*tt)) + ss*(s35**2 -&
   6*s35**2*sW2 + 12*s35**2*sW2**2 + s25**2*(1 - 4*sW2 + 8*sW2**2) + 2*s15**2*(1 -&
   5*sW2 + 10*sW2**2) + 4*s35*tt - 20*s35*sW2*tt + 40*s35*sW2**2*tt + 3*tt**2 -&
   16*sW2*tt**2 + 32*sW2**2*tt**2 - 2*s25*(1 - 4*sW2 + 8*sW2**2)*(s35 + 2*tt) +&
   s15*(s35*(-3 + 16*sW2 - 32*sW2**2) + 3*s25*(1 - 4*sW2 + 8*sW2**2) - 2*(3 - 14*sW2 +&
   28*sW2**2)*tt)))))/(s15*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) + s1m*(-((e**6*me1*(-1&
   + 4*sW2)*(me2*(-3*s15 - 3*s25 + s35 + 2*ss) + mm2*(-3*s15 - 3*s25 + s35 + 2*ss) -&
   (-2*s15 - 2*s25 + s35 + ss)*(-s15 - s25 + s35 + ss + tt)))/(s15*(s15 + s25 -&
   s35)*ss*(-1 + sW2)*sW2)) + (e**6*s3n*(mm2**2 + me2**2*(3 - 8*sW2 + 16*sW2**2) +&
   me2*(9*s15 + 6*s25 - 5*s35 - 20*s15*sW2 - 16*s25*sW2 + 16*s35*sW2 + 40*s15*sW2**2 +&
   32*s25*sW2**2 - 32*s35*sW2**2 + 4*mm2*(1 - 2*sW2 + 4*sW2**2) - 2*ss*(3 - 8*sW2 +&
   16*sW2**2) - 5*tt + 16*sW2*tt - 32*sW2**2*tt) + mm2*(2*s25 - 3*s35 - 2*ss +&
   8*s35*sW2 - 16*s35*sW2**2 + s15*(5 - 12*sW2 + 24*sW2**2) - 3*tt + 8*sW2*tt -&
   16*sW2**2*tt) + 2*(2*s15**2*(1 - 3*sW2 + 6*sW2**2) + 3*s15*s25*(1 - 4*sW2 +&
   8*sW2**2) + ss**2*(1 - 4*sW2 + 8*sW2**2) - ss*(1 - 4*sW2 + 8*sW2**2)*(3*s15 + 2*s25&
   - 2*s35 - 2*tt) - s15*(3 - 10*sW2 + 20*sW2**2)*(s35 + tt) + (1 - 4*sW2 +&
   8*sW2**2)*(-s25 + s35 + tt)**2)))/(2.*s15*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) +&
   (e**6*s4n*(me2**2*(1 + 8*sW2 - 16*sW2**2) - me2*(-s25 + s35 + ss - 24*mm2*sW2 +&
   16*s35*sW2 + 48*mm2*sW2**2 - 32*s35*sW2**2 + s15*(-1 - 4*sW2 + 8*sW2**2) + tt +&
   16*sW2*tt - 32*sW2**2*tt) - (mm2 + s15 + s25 - s35 - ss - tt)*(mm2*(1 - 16*sW2 +&
   32*sW2**2) + 4*sW2*(-1 + 2*sW2)*(s15 - 2*(s35 + tt)))))/(2.*s15*(s15 + s25 -&
   s35)*ss*(-1 + sW2)*sW2) + (e**6*s2n*(2*mm2**2*(1 - 4*sW2)**2 - 4*s15*(s15 + s25 -&
   ss)*sW2*(-1 + 2*sW2) + me2*(-s15 - s25 + ss + 2*mm2*(-1 - 8*sW2 + 16*sW2**2)) +&
   mm2*(-3*ss*(1 - 4*sW2)**2 + s15*(5 - 24*sW2 + 48*sW2**2) + (1 - 4*sW2)**2*(3*s25 -&
   2*(s35 + tt)))))/(2.*s15*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2))

  m20 = (-2*e**6*me1*mm2*s1m*(-1 + 4*sW2))/(s15*(s15 + s25 - ss)*(-1 + sW2)*sW2) -&
   (2*e**6*me1*s2n*(-1 + 4*sW2)*(me2**2 + mm2**2 + 2*s15**2 + 3*s15*s25 + s25**2 -&
   3*s15*s35 - 2*s25*s35 + s35**2 - 3*s15*ss - 2*s25*ss + 2*s35*ss + ss**2 + me2*(2*mm2&
   + 3*s15 + 2*s25 - 2*s35 - 2*ss - 2*tt) + mm2*(2*s15 + s25 - 2*s35 - ss - 2*tt) -&
   3*s15*tt - 2*s25*tt + 2*s35*tt + 2*ss*tt + tt**2))/(s15**2*(s15 + s25 - ss)*(-1 +&
   sW2)*sW2) + (2*e**6*me1*s4n*(-1 + 4*sW2)*(2*me2**2 + mm2**2 + me2*(mm2 + 4*s15 +&
   4*s25 - 3*s35 - 4*ss - 3*tt) + mm2*(2*s15 + s25 - 2*s35 - ss - 2*tt) + (-s15 - s25 +&
   s35 + ss + tt)**2))/(s15**2*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (2*e**6*me1*s3n*(-1 +&
   4*sW2)*(mm2**2 + 2*s15**2 + 3*s15*s25 + s25**2 - 3*s15*s35 - 2*s25*s35 + s35**2 -&
   3*s15*ss - 2*s25*ss + 2*s35*ss + ss**2 + mm2*(3*s15 + s25 - 2*s35 - ss - 2*tt) -&
   3*s15*tt - 2*s25*tt + 2*s35*tt + 2*ss*tt + tt**2 - me2*(mm2 - s15 - s25 + s35 + ss +&
   tt)))/(s15**2*(s15 + s25 - ss)*(-1 + sW2)*sW2) - (2*e**6*snm*(me1 -&
   4*me1*sW2)**2*(me2**2 + mm2**2 - (s15 - s35 - tt)*(-s15 - s25 + s35 + ss + tt) +&
   mm2*(s15 - 2*(s35 + tt)) + me2*(2*mm2 + s15 - 2*(s35 + tt))))/(s15**2*(s15 + s25 -&
   ss)*(-1 + sW2)*sW2) + s4m*((e**6*me2*s2n*(me2 + mm2 + s15 + s25 - s35 - ss -&
   8*s15*sW2 - 8*s25*sW2 + 8*ss*sW2 + 16*s15*sW2**2 + 16*s25*sW2**2 - 16*ss*sW2**2 -&
   tt))/(s15**2*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*me2*s4n*(-me2 - s15 - s25 +&
   s35 + ss + 8*s15*sW2 + 8*s25*sW2 - 8*ss*sW2 - 16*s15*sW2**2 - 16*s25*sW2**2 +&
   16*ss*sW2**2 + mm2*(1 - 16*sW2 + 32*sW2**2) + tt))/(s15**2*(s15 + s25 - ss)*(-1 +&
   sW2)*sW2) + (e**6*me2*s3n*(s15 + s25 - s35 - ss - 16*s15*sW2 - 8*s25*sW2 +&
   16*s35*sW2 + 8*ss*sW2 + 32*s15*sW2**2 + 16*s25*sW2**2 - 32*s35*sW2**2 - 16*ss*sW2**2&
   + me2*(3 - 16*sW2 + 32*sW2**2) + mm2*(3 - 32*sW2 + 64*sW2**2) - tt + 16*sW2*tt -&
   32*sW2**2*tt))/(s15**2*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*me1*(-1 +&
   4*sW2)*(2*me2**2 + me2*(6*mm2 + s15 + 2*s25 - 2*s35 - 2*ss - 2*tt) + s15*(mm2 - s15&
   - s25 + s35 + ss + tt)))/(s15**2*(s15 + s25 - ss)*(-1 + sW2)*sW2)) +&
   s3m*((e**6*me2*s2n*(me2 + mm2 + 3*s15 + 2*s25 - s35 - 2*ss - 8*s15*sW2 - 8*s25*sW2 +&
   8*ss*sW2 + 16*s15*sW2**2 + 16*s25*sW2**2 - 16*ss*sW2**2 - tt))/(s15**2*(s15 + s25 -&
   ss)*(-1 + sW2)*sW2) + (e**6*me2*s3n*(-me2 - 3*s15 - 2*s25 + s35 + 2*ss + 8*s15*sW2 +&
   8*s25*sW2 - 8*ss*sW2 - 16*s15*sW2**2 - 16*s25*sW2**2 + 16*ss*sW2**2 + mm2*(1 -&
   16*sW2 + 32*sW2**2) + tt))/(s15**2*(s15 + s25 - ss)*(-1 + sW2)*sW2) -&
   (e**6*me2*s4n*(mm2 + 3*s15 + 2*s25 - 3*s35 - 2*ss - 16*s15*sW2 - 8*s25*sW2 +&
   16*s35*sW2 + 8*ss*sW2 + 32*s15*sW2**2 + 16*s25*sW2**2 - 32*s35*sW2**2 - 16*ss*sW2**2&
   + me2*(5 - 16*sW2 + 32*sW2**2) - 3*tt + 16*sW2*tt - 32*sW2**2*tt))/(s15**2*(s15 +&
   s25 - ss)*(-1 + sW2)*sW2) - (e**6*me1*(-1 + 4*sW2)*(2*me2**2 + s15*(-mm2 + 3*s15 +&
   2*s25 - 3*s35 - tt) - me2*(2*mm2 - 7*s15 - 4*s25 + 2*s35 + 4*ss +&
   2*tt)))/(s15**2*(s15 + s25 - ss)*(-1 + sW2)*sW2)) + (2*e**6*(2*me2**3*(1 - 4*sW2)**2&
   + 2*me2**2*(2*s15 + s25 - 2*s35 - ss + 2*mm2*(1 - 4*sW2)**2 - 8*s15*sW2 + 16*s35*sW2&
   + 16*s15*sW2**2 - 32*s35*sW2**2 - 2*tt + 16*sW2*tt - 32*sW2**2*tt) + s15*(s15**2 +&
   2*s15*s25 + s25**2 - 2*s15*s35 - 2*s25*s35 + s35**2 - 4*s15**2*sW2 - 8*s15*s25*sW2 -&
   4*s25**2*sW2 + 12*s15*s35*sW2 + 8*s25*s35*sW2 - 8*s35**2*sW2 + 8*s15**2*sW2**2 +&
   16*s15*s25*sW2**2 + 8*s25**2*sW2**2 - 24*s15*s35*sW2**2 - 16*s25*s35*sW2**2 +&
   16*s35**2*sW2**2 - (s15 + s25 - s35)*ss*(1 - 4*sW2 + 8*sW2**2) + mm2*(-(s35*(1 -&
   4*sW2)**2) + 4*s25*sW2 - 8*s25*sW2**2 + s15*(1 - 4*sW2 + 8*sW2**2)) - s15*tt -&
   s25*tt + s35*tt + 4*s15*sW2*tt + 4*s25*sW2*tt - 8*s35*sW2*tt - 8*s15*sW2**2*tt -&
   8*s25*sW2**2*tt + 16*s35*sW2**2*tt) + me2*(6*s15**2 + 8*s15*s25 + 2*s25**2 -&
   7*s15*s35 - 4*s25*s35 + 2*s35**2 + 2*mm2**2*(1 - 4*sW2)**2 - 28*s15**2*sW2 -&
   36*s15*s25*sW2 - 8*s25**2*sW2 + 40*s15*s35*sW2 + 16*s25*s35*sW2 - 16*s35**2*sW2 +&
   56*s15**2*sW2**2 + 72*s15*s25*sW2**2 + 16*s25**2*sW2**2 - 80*s15*s35*sW2**2 -&
   32*s25*s35*sW2**2 + 32*s35**2*sW2**2 + s15*ss*(-7 + 32*sW2 - 64*sW2**2) + 2*ss**2*(1&
   - 4*sW2 + 8*sW2**2) - 4*ss*(1 - 4*sW2 + 8*sW2**2)*(s25 - s35 - tt) - 6*s15*tt -&
   4*s25*tt + 4*s35*tt + 32*s15*sW2*tt + 16*s25*sW2*tt - 32*s35*sW2*tt -&
   64*s15*sW2**2*tt - 32*s25*sW2**2*tt + 64*s35*sW2**2*tt + 2*tt**2 - 16*sW2*tt**2 +&
   32*sW2**2*tt**2 - 2*mm2*(-s25 + 2*s35 + ss - 16*s35*sW2 + 32*s35*sW2**2 - 2*s15*(1 -&
   4*sW2 + 8*sW2**2) + 2*tt - 16*sW2*tt + 32*sW2**2*tt))))/(s15**2*(s15 + s25 - ss)*(-1&
   + sW2)*sW2)

  m23 = (e**6*me1*s3n*(-1 + 4*sW2)*(me2*(-s35 + 2*ss) + mm2*(2*s25 + s35 + 2*ss) - ss*(-s15 + ss&
   + tt)))/(s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2) - (e**6*me1*s2n*(-1 +&
   4*sW2)*(me2**2 + 3*mm2**2 + mm2*(2*s15 + s25 - 2*ss - 4*tt) + me2*(4*mm2 - s25 -&
   2*tt) + tt*(-s15 + ss + tt)))/(s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   (e**6*me1*s4n*(-1 + 4*sW2)*(2*me2**2 + 2*mm2**2 + me2*(4*mm2 + 3*s15 + s25 - 3*s35 -&
   3*ss - 4*tt) + 2*(-s15 - s25 + ss + tt)*(-s15 + s35 + ss + tt) - mm2*(-3*s15 - 5*s25&
   + s35 + 3*ss + 4*tt)))/(s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   s4m*((e**6*s3n*(mm2*ss*(1 + 8*sW2 - 16*sW2**2) + s25*ss*(1 - 4*sW2 + 8*sW2**2) -&
   me2*(8*mm2 + ss*(1 - 4*sW2)**2 - 8*(s15 + s25)*sW2*(-1 + 2*sW2) + 2*s35*(1 - 4*sW2 +&
   8*sW2**2))))/(2.*s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2) + (e**6*me1*(-1 +&
   4*sW2)*(2*me2**2 + 2*mm2**2 + s15**2 - s25**2 + s15*s35 + s25*s35 - 2*s15*ss -&
   s35*ss + ss**2 + mm2*(2*s25 - s35 - 4*tt) + me2*(4*mm2 + 2*s15 - s35 - 2*ss - 4*tt)&
   - 3*s15*tt - s25*tt + 3*ss*tt + 2*tt**2))/(s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2)&
   + (e**6*s4n*(me2*(-4*mm2 + s15 + s25 + s35 + ss*(-1 - 4*sW2 + 8*sW2**2)) -&
   4*ss*sW2*(-1 + 2*sW2)*(mm2 - s15 - s25 + ss + tt)))/(s25*s35*(-s15 - s25 + ss)*(-1 +&
   sW2)*sW2) + (e**6*s2n*(-mm2**2 + me2**2*(1 - 4*sW2)**2 - 8*s15**2*sW2 -&
   8*s15*s25*sW2 + 16*s15*ss*sW2 + 8*s25*ss*sW2 - 8*ss**2*sW2 + 16*s15**2*sW2**2 +&
   16*s15*s25*sW2**2 - 32*s15*ss*sW2**2 - 16*s25*ss*sW2**2 + 16*ss**2*sW2**2 + s25*tt +&
   16*s15*sW2*tt + 4*s25*sW2*tt - 16*ss*sW2*tt - 32*s15*sW2**2*tt - 8*s25*sW2**2*tt +&
   32*ss*sW2**2*tt - 8*sW2*tt**2 + 16*sW2**2*tt**2 + me2*(-16*s15*sW2 + 16*ss*sW2 +&
   32*s15*sW2**2 - 32*ss*sW2**2 + 4*mm2*(1 - 2*sW2 + 4*sW2**2) + s25*(-1 - 4*sW2 +&
   8*sW2**2) - tt + 16*sW2*tt - 32*sW2**2*tt) + mm2*(s25*(-1 - 4*sW2 + 8*sW2**2) + tt +&
   8*sW2*tt - 16*sW2**2*tt)))/(2.*s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2)) +&
   s3m*((e**6*s4n*(mm2*ss*(1 + 8*sW2 - 16*sW2**2) + s25*ss*(1 - 4*sW2 + 8*sW2**2) +&
   me2*(-8*mm2 + ss*(1 - 4*sW2)**2 - 2*(s15 + s25 - s35)*(1 - 4*sW2 +&
   8*sW2**2))))/(2.*s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2) + (e**6*me1*(-1 +&
   4*sW2)*(-s15**2 - s15*s25 + s15*s35 + s25*s35 + 3*s15*ss + s25*ss - s35*ss - 2*ss**2&
   + me2*(-s15 - s25 - s35 + 3*ss) + mm2*(-3*s15 - s25 - s35 + 5*ss) + s15*tt + s25*tt&
   - 2*ss*tt))/(s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2) - (e**6*s3n*(mm2*ss*(-2 +&
   4*sW2 - 8*sW2**2) + me2*(4*mm2 + s35 + ss*(-2 + 4*sW2 - 8*sW2**2)) + ss*(1 - 4*sW2 +&
   8*sW2**2)*(-s15 + ss + tt)))/(s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   (e**6*s2n*(mm2**2*(-3 + 16*sW2 - 32*sW2**2) + me2**2*(-3 + 8*sW2 - 16*sW2**2) - (1 -&
   4*sW2 + 8*sW2**2)*(-s15 + ss + tt)*(s25 + 2*tt) + me2*(-s15 + 3*s25 + ss -&
   12*s25*sW2 + 24*s25*sW2**2 + mm2*(-2 + 24*sW2 - 48*sW2**2) + 5*tt - 16*sW2*tt +&
   32*sW2**2*tt) + mm2*(-s15 + s25 + ss + 16*s15*sW2 - 4*s25*sW2 - 16*ss*sW2 -&
   32*s15*sW2**2 + 8*s25*sW2**2 + 32*ss*sW2**2 + 5*tt - 24*sW2*tt +&
   48*sW2**2*tt)))/(2.*s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2)) +&
   (e**6*snm*(s15**2*s25 + s15*s25**2 - s15*s25*s35 - s15*s25*ss + s25*s35*ss -&
   2*me2**3*(1 - 4*sW2)**2 - 2*mm2**3*(1 - 4*sW2)**2 + 8*s15**3*sW2 + 4*s15**2*s25*sW2&
   - 4*s15*s25**2*sW2 - 8*s15**2*s35*sW2 - 4*s15*s25*s35*sW2 - 16*s15**2*ss*sW2 -&
   4*s15*s25*ss*sW2 + 16*s15*s35*ss*sW2 + 4*s25*s35*ss*sW2 + 8*s15*ss**2*sW2 -&
   8*s35*ss**2*sW2 - 16*s15**3*sW2**2 - 8*s15**2*s25*sW2**2 + 8*s15*s25**2*sW2**2 +&
   16*s15**2*s35*sW2**2 + 8*s15*s25*s35*sW2**2 + 32*s15**2*ss*sW2**2 +&
   8*s15*s25*ss*sW2**2 - 32*s15*s35*ss*sW2**2 - 8*s25*s35*ss*sW2**2 -&
   16*s15*ss**2*sW2**2 + 16*s35*ss**2*sW2**2 + 2*s15**2*tt - s15*s25*tt - s25**2*tt -&
   2*s15*s35*tt + 2*s25*s35*tt - 4*s15*ss*tt + 2*s35*ss*tt + 2*ss**2*tt -&
   32*s15**2*sW2*tt - 12*s15*s25*sW2*tt + 4*s25**2*sW2*tt + 24*s15*s35*sW2*tt +&
   48*s15*ss*sW2*tt + 8*s25*ss*sW2*tt - 24*s35*ss*sW2*tt - 16*ss**2*sW2*tt +&
   64*s15**2*sW2**2*tt + 24*s15*s25*sW2**2*tt - 8*s25**2*sW2**2*tt -&
   48*s15*s35*sW2**2*tt - 96*s15*ss*sW2**2*tt - 16*s25*ss*sW2**2*tt +&
   48*s35*ss*sW2**2*tt + 32*ss**2*sW2**2*tt - 4*s15*tt**2 + 2*s35*tt**2 + 4*ss*tt**2 +&
   40*s15*sW2*tt**2 + 8*s25*sW2*tt**2 - 16*s35*sW2*tt**2 - 32*ss*sW2*tt**2 -&
   80*s15*sW2**2*tt**2 - 16*s25*sW2**2*tt**2 + 32*s35*sW2**2*tt**2 + 64*ss*sW2**2*tt**2&
   + 2*tt**3 - 16*sW2*tt**3 + 32*sW2**2*tt**3 + me2**2*(-3*s15 + s25 + 4*s35 - 6*mm2*(1&
   - 4*sW2)**2 + 2*ss*(1 - 4*sW2)**2 + 32*s15*sW2 - 32*s35*sW2 - 64*s15*sW2**2 +&
   64*s35*sW2**2 + 6*tt - 48*sW2*tt + 96*sW2**2*tt) + mm2**2*(-s25 + 2*s35 + 2*ss*(1 -&
   4*sW2)**2 + 16*s25*sW2 - 16*s35*sW2 - 32*s25*sW2**2 + 32*s35*sW2**2 + s15*(-3 +&
   32*sW2 - 64*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt) - me2*(s15**2 - 2*s15*s25 -&
   s25**2 - s15*s35 + 4*s25*s35 + 6*mm2**2*(1 - 4*sW2)**2 - 24*s15**2*sW2 -&
   4*s15*s25*sW2 + 4*s25**2*sW2 + 32*s15*s35*sW2 + 48*s15**2*sW2**2 + 8*s15*s25*sW2**2&
   - 8*s25**2*sW2**2 - 64*s15*s35*sW2**2 - 7*s15*tt + s25*tt + 6*s35*tt + 72*s15*sW2*tt&
   + 8*s25*sW2*tt - 48*s35*sW2*tt - 144*s15*sW2**2*tt - 16*s25*sW2**2*tt +&
   96*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 - 2*mm2*(-2*s25 + 3*s35&
   + 2*ss*(1 - 4*sW2)**2 + 8*s25*sW2 - 24*s35*sW2 - 16*s25*sW2**2 + 48*s35*sW2**2 +&
   s15*(-5 + 32*sW2 - 64*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt) + ss*(s35 -&
   32*s35*sW2 + 64*s35*sW2**2 + s15*(-1 + 24*sW2 - 48*sW2**2) + 2*s25*(1 - 4*sW2 +&
   8*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt)) + mm2*(s25**2 - 2*s25*s35 -&
   4*s25**2*sW2 + 8*s25**2*sW2**2 + s15**2*(-1 + 24*sW2 - 48*sW2**2) + s25*tt -&
   4*s35*tt - 24*s25*sW2*tt + 32*s35*sW2*tt + 48*s25*sW2**2*tt - 64*s35*sW2**2*tt -&
   6*tt**2 + 48*sW2*tt**2 - 96*sW2**2*tt**2 + ss*(s15 + 2*s25 - s35 - 24*s15*sW2 -&
   8*s25*sW2 + 16*s35*sW2 + 48*s15*sW2**2 + 16*s25*sW2**2 - 32*s35*sW2**2 - 6*tt +&
   48*sW2*tt - 96*sW2**2*tt) + s15*(s35 - 16*s35*sW2 + 32*s35*sW2**2 + s25*(2 + 20*sW2&
   - 40*sW2**2) + 7*tt - 72*sW2*tt + 144*sW2**2*tt))))/(2.*s25*s35*(-s15 - s25 +&
   ss)*(-1 + sW2)*sW2) + s1m*(-((e**6*me1*(-1 + 4*sW2)*(me2**2 - mm2**2 - s15*s25 -&
   s25**2 + 2*s15*s35 + s25*s35 + s25*ss - 2*s35*ss + mm2*(-4*s15 - s25 + s35 + 4*ss) +&
   me2*(-s25 + s35 - 2*tt) - s15*tt - s35*tt + ss*tt + tt**2))/(s25*s35*(-s15 - s25 +&
   ss)*(-1 + sW2)*sW2)) + (e**6*s2n*(me2*(s15 + s25 - ss + 2*mm2*(1 - 4*sW2)**2) +&
   2*mm2**2*(1 - 4*sW2)**2 + s25*(-s15 - s25 + ss)*(1 - 4*sW2 + 8*sW2**2) + mm2*(s15 -&
   3*s25 - 24*s15*sW2 - 8*s25*sW2 + 48*s15*sW2**2 + 16*s25*sW2**2 + ss*(-1 + 24*sW2 -&
   48*sW2**2) - 2*tt + 16*sW2*tt - 32*sW2**2*tt)))/(2.*s25*s35*(-s15 - s25 + ss)*(-1 +&
   sW2)*sW2) - (e**6*s4n*(-mm2**2 - s15*s25 - s25**2 + s25*s35 + s25*ss + me2**2*(1 -&
   4*sW2)**2 - 8*s15**2*sW2 - 4*s15*s25*sW2 + 4*s25**2*sW2 + 8*s15*s35*sW2 +&
   4*s25*s35*sW2 + 8*s15*ss*sW2 - 4*s25*ss*sW2 - 8*s35*ss*sW2 + 16*s15**2*sW2**2 +&
   8*s15*s25*sW2**2 - 8*s25**2*sW2**2 - 16*s15*s35*sW2**2 - 8*s25*s35*sW2**2 -&
   16*s15*ss*sW2**2 + 8*s25*ss*sW2**2 + 16*s35*ss*sW2**2 + s25*tt + 16*s15*sW2*tt +&
   4*s25*sW2*tt - 8*s35*sW2*tt - 8*ss*sW2*tt - 32*s15*sW2**2*tt - 8*s25*sW2**2*tt +&
   16*s35*sW2**2*tt + 16*ss*sW2**2*tt - 8*sW2*tt**2 + 16*sW2**2*tt**2 + me2*(3*s15 +&
   2*s25 + s35 - 16*s15*sW2 - 4*s25*sW2 + 8*s35*sW2 + 32*s15*sW2**2 + 8*s25*sW2**2 -&
   16*s35*sW2**2 + ss*(-3 + 8*sW2 - 16*sW2**2) + 4*mm2*(-1 - 2*sW2 + 4*sW2**2) - tt +&
   16*sW2*tt - 32*sW2**2*tt) + mm2*(-2*s25 + s35 - 12*s25*sW2 + 24*s25*sW2**2 + ss*(1 +&
   16*sW2 - 32*sW2**2) + s15*(-1 - 8*sW2 + 16*sW2**2) + tt + 8*sW2*tt -&
   16*sW2**2*tt)))/(2.*s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2) + (e**6*s3n*(me2**2*(3&
   - 8*sW2 + 16*sW2**2) + mm2**2*(3 - 16*sW2 + 32*sW2**2) + mm2*(3*s25 - 3*s35 -&
   4*s25*sW2 + 16*s35*sW2 + 8*s25*sW2**2 - 32*s35*sW2**2 - 2*ss*(3 - 8*sW2 + 16*sW2**2)&
   + s15*(5 - 24*sW2 + 48*sW2**2) - 5*tt + 24*sW2*tt - 48*sW2**2*tt) + me2*(5*s15 + s25&
   - 3*s35 - 8*s15*sW2 + 4*s25*sW2 + 8*s35*sW2 + 16*s15*sW2**2 - 8*s25*sW2**2 -&
   16*s35*sW2**2 - 4*ss*(1 - 2*sW2 + 4*sW2**2) + 2*mm2*(5 - 12*sW2 + 24*sW2**2) - 5*tt&
   + 16*sW2*tt - 32*sW2**2*tt) + (1 - 4*sW2 + 8*sW2**2)*(2*s15**2 + s25*s35 + 2*ss**2 +&
   s15*(s25 - 2*s35 - 4*tt) - 2*ss*(2*s15 + s25 - s35 - 2*tt) - s25*tt + 2*s35*tt +&
   2*tt**2)))/(2.*s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2)) + (e**6*(2*me2**3*(1 -&
   4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 + me2**2*(5*s15 + s25 - 4*s35 + 6*mm2*(1 -&
   4*sW2)**2 - 32*s15*sW2 + 32*s35*sW2 + 64*s15*sW2**2 - 64*s35*sW2**2 - 4*ss*(1 -&
   4*sW2 + 8*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) + mm2**2*(3*s25 - 2*s35 -&
   16*s25*sW2 + 16*s35*sW2 + 32*s25*sW2**2 - 32*s35*sW2**2 - 4*ss*(1 - 4*sW2 +&
   8*sW2**2) + s15*(5 - 32*sW2 + 64*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) +&
   me2*(5*s15**2 + 3*s15*s25 - 5*s15*s35 - 2*s25*s35 + 6*mm2**2*(1 - 4*sW2)**2 -&
   28*s15**2*sW2 - 12*s15*s25*sW2 + 36*s15*s35*sW2 + 4*s25*s35*sW2 + 56*s15**2*sW2**2 +&
   24*s15*s25*sW2**2 - 72*s15*s35*sW2**2 - 8*s25*s35*sW2**2 + 4*ss**2*(1 - 2*sW2 +&
   4*sW2**2) - 11*s15*tt - 3*s25*tt + 6*s35*tt + 72*s15*sW2*tt + 8*s25*sW2*tt -&
   48*s35*sW2*tt - 144*s15*sW2**2*tt - 16*s25*sW2**2*tt + 96*s35*sW2**2*tt + 6*tt**2 -&
   48*sW2*tt**2 + 96*sW2**2*tt**2 - 2*mm2*(-4*s25 + 3*s35 + 8*s25*sW2 - 24*s35*sW2 -&
   16*s25*sW2**2 + 48*s35*sW2**2 + s15*(-7 + 32*sW2 - 64*sW2**2) + 4*ss*(1 - 4*sW2 +&
   8*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt) + ss*(-2*s25 + 5*s35 - 32*s35*sW2 +&
   64*s35*sW2**2 + s15*(-9 + 32*sW2 - 64*sW2**2) + 10*tt - 48*sW2*tt + 96*sW2**2*tt)) +&
   mm2*(-2*s25*s35 + 4*s25*s35*sW2 - 8*s25*s35*sW2**2 + 4*ss**2*(1 - 2*sW2 + 4*sW2**2)&
   + s15**2*(5 - 28*sW2 + 56*sW2**2) - 5*s25*tt + 4*s35*tt + 24*s25*sW2*tt -&
   32*s35*sW2*tt - 48*s25*sW2**2*tt + 64*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 +&
   96*sW2**2*tt**2 + ss*(3*s35 - 16*s35*sW2 + 32*s35*sW2**2 + s15*(-9 + 32*sW2 -&
   64*sW2**2) - 2*s25*(3 - 8*sW2 + 16*sW2**2) + 10*tt - 48*sW2*tt + 96*sW2**2*tt) +&
   s15*(s35*(-3 + 20*sW2 - 40*sW2**2) + 7*s25*(1 - 4*sW2 + 8*sW2**2) + (-11 + 72*sW2 -&
   144*sW2**2)*tt)) - 2*(s15**3*(-1 + 6*sW2 - 12*sW2**2) + ss**3*(1 - 4*sW2 + 8*sW2**2)&
   - ss**2*(1 - 4*sW2 + 8*sW2**2)*(3*s15 + s25 - s35 - 3*tt) + s15**2*(s35*(1 -&
   4*sW2)**2 + s25*(-1 + 6*sW2 - 12*sW2**2) + 3*(1 - 6*sW2 + 12*sW2**2)*tt) +&
   s15*(s35**2*(2*sW2 - 4*sW2**2) + s25*s35*(1 - 6*sW2 + 12*sW2**2) + 2*s25*(1 - 5*sW2&
   + 10*sW2**2)*tt - 2*s35*(1 - 7*sW2 + 14*sW2**2)*tt + (-3 + 20*sW2 -&
   40*sW2**2)*tt**2) + tt*((1 - 4*sW2)**2*tt*(s35 + tt) - s25*(s35 - 2*s35*sW2 +&
   4*s35*sW2**2 + tt - 4*sW2*tt + 8*sW2**2*tt)) + ss*(-2*s35**2*sW2 + 4*s35**2*sW2**2 +&
   s15**2*(3 - 14*sW2 + 28*sW2**2) + 2*s35*tt - 12*s35*sW2*tt + 24*s35*sW2**2*tt +&
   3*tt**2 - 16*sW2*tt**2 + 32*sW2**2*tt**2 - s25*(1 - 4*sW2 + 8*sW2**2)*(s35 + 2*tt) +&
   2*s15*(s35*(-1 + 6*sW2 - 12*sW2**2) + s25*(1 - 4*sW2 + 8*sW2**2) + (-3 + 14*sW2 -&
   28*sW2**2)*tt)))))/(s25*s35*(-s15 - s25 + ss)*(-1 + sW2)*sW2)

  m25 = -((e**6*me1*s4n*(-1 + 4*sW2)*(-(s15*s25) - s25**2 - s15*s35 + s35**2 + s25*ss +&
   me2*(-s15 - s25 + ss) - mm2*(-s15 + s25 + 2*s35 + ss) + s35*tt))/(s25*(s15 + s25 -&
   s35)*(-s15 - s25 + ss)*(-1 + sW2)*sW2)) + (e**6*me1*s2n*(-1 + 4*sW2)*(me2**2 -&
   mm2**2 - s15*s25 - s25**2 + s25*s35 + mm2*(4*s15 + 3*s25 - 2*s35 - 3*ss) + s25*ss +&
   me2*(2*s15 + s25 - 2*s35 - ss - 2*tt) - 3*s15*tt - 2*s25*tt + 2*s35*tt + 2*ss*tt +&
   tt**2))/(s25*(s15 + s25 - s35)*(-s15 - s25 + ss)*(-1 + sW2)*sW2) - (e**6*me1*s3n*(-1&
   + 4*sW2)*(2*me2**2 + 2*mm2**2 + 2*s15**2 + 2*s15*s25 - 3*s15*s35 - s25*s35 + s35**2&
   - s15*ss + s25*ss + s35*ss + mm2*(4*s15 + 2*s25 - 4*s35 - 4*ss - 4*tt) +&
   2*me2*(2*mm2 + s15 - s35 - ss - 2*tt) - 4*s15*tt - 2*s25*tt + 3*s35*tt + 3*ss*tt +&
   2*tt**2))/(s25*(s15 + s25 - s35)*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   s3m*((e**6*s4n*(ss*(4*s25*sW2 - 8*s25*sW2**2 + mm2*(3 - 8*sW2 + 16*sW2**2)) +&
   me2*(-8*mm2 + ss*(1 - 4*sW2)**2 - 2*(s15 + s25 - 4*s35*sW2 +&
   8*s35*sW2**2))))/(2.*s25*(s15 + s25 - s35)*(-s15 - s25 + ss)*(-1 + sW2)*sW2) -&
   (e**6*s3n*(me2*(4*mm2 - 2*s15 - 2*s25 + s35 + 4*ss*sW2 - 8*ss*sW2**2) - ss*(1 -&
   4*sW2 + 8*sW2**2)*(3*mm2 - s25 - tt)))/(s25*(s15 + s25 - s35)*(-s15 - s25 + ss)*(-1&
   + sW2)*sW2) - (e**6*me1*(-1 + 4*sW2)*(2*me2**2 + 2*mm2**2 + s15**2 + s15*s25 -&
   2*s15*ss + s25*ss + ss**2 + mm2*(3*s15 + s25 - 7*ss - 4*tt) + me2*(4*mm2 + s15 - s25&
   - 3*ss - 4*tt) - 3*s15*tt - s25*tt + s35*tt + 4*ss*tt + 2*tt**2))/(s25*(s15 + s25 -&
   s35)*(-s15 - s25 + ss)*(-1 + sW2)*sW2) + (e**6*s2n*(4*s15*s25*sW2 - 4*s25*ss*sW2 -&
   8*s15*s25*sW2**2 + 8*s25*ss*sW2**2 + mm2**2*(-3 + 16*sW2 - 32*sW2**2) + me2**2*(-3 +&
   8*sW2 - 16*sW2**2) - 2*s25*tt + 4*s25*sW2*tt - 8*s25*sW2**2*tt - 2*tt**2 +&
   8*sW2*tt**2 - 16*sW2**2*tt**2 + me2*(-s15 + 2*s25 + ss - 4*s25*sW2 + 8*s25*sW2**2 +&
   mm2*(-2 + 24*sW2 - 48*sW2**2) + 5*tt - 16*sW2*tt + 32*sW2**2*tt) + mm2*(2*s25 -&
   4*s25*sW2 + 8*s25*sW2**2 + ss*(-3 + 8*sW2 - 16*sW2**2) + s15*(3 - 8*sW2 + 16*sW2**2)&
   + 5*tt - 24*sW2*tt + 48*sW2**2*tt)))/(2.*s25*(s15 + s25 - s35)*(-s15 - s25 + ss)*(-1&
   + sW2)*sW2)) + (e**6*(s15**2*s25 + s15*s25**2 + s15**2*s35 - s15*s35**2 - s15*s25*ss&
   - s15*s35*ss - s25*s35*ss + s35**2*ss + 2*me2**3*(1 - 4*sW2)**2 + 2*mm2**3*(1 -&
   4*sW2)**2 - 4*s15**2*s25*sW2 - 4*s15*s25**2*sW2 - 4*s15**2*s35*sW2 +&
   4*s15*s35**2*sW2 + 4*s15*s25*ss*sW2 + 4*s15*s35*ss*sW2 + 4*s25*s35*ss*sW2 -&
   4*s35**2*ss*sW2 + 8*s15**2*s25*sW2**2 + 8*s15*s25**2*sW2**2 + 8*s15**2*s35*sW2**2 -&
   8*s15*s35**2*sW2**2 - 8*s15*s25*ss*sW2**2 - 8*s15*s35*ss*sW2**2 -&
   8*s25*s35*ss*sW2**2 + 8*s35**2*ss*sW2**2 - 2*s15**2*tt - 3*s15*s25*tt - s25**2*tt +&
   s15*s35*tt + s25*s35*tt + 4*s15*ss*tt + 2*s25*ss*tt - 2*s35*ss*tt - 2*ss**2*tt +&
   8*s15**2*sW2*tt + 4*s15*s25*sW2*tt - 4*s25**2*sW2*tt - 4*s15*s35*sW2*tt +&
   4*s25*s35*sW2*tt - 16*s15*ss*sW2*tt - 8*s25*ss*sW2*tt + 8*s35*ss*sW2*tt +&
   8*ss**2*sW2*tt - 16*s15**2*sW2**2*tt - 8*s15*s25*sW2**2*tt + 8*s25**2*sW2**2*tt +&
   8*s15*s35*sW2**2*tt - 8*s25*s35*sW2**2*tt + 32*s15*ss*sW2**2*tt +&
   16*s25*ss*sW2**2*tt - 16*s35*ss*sW2**2*tt - 16*ss**2*sW2**2*tt + 4*s15*tt**2 +&
   2*s25*tt**2 - 2*s35*tt**2 - 4*ss*tt**2 - 24*s15*sW2*tt**2 - 8*s25*sW2*tt**2 +&
   16*s35*sW2*tt**2 + 16*ss*sW2*tt**2 + 48*s15*sW2**2*tt**2 + 16*s25*sW2**2*tt**2 -&
   32*s35*sW2**2*tt**2 - 32*ss*sW2**2*tt**2 - 2*tt**3 + 16*sW2*tt**3 - 32*sW2**2*tt**3&
   + me2**2*(s15 - s25 - 2*ss + 6*mm2*(1 - 4*sW2)**2 + 16*s25*sW2 - 32*s25*sW2**2 -&
   6*tt + 48*sW2*tt - 96*sW2**2*tt) + mm2**2*(3*s25 - 2*s35 - 2*ss - 16*s25*sW2 +&
   16*s35*sW2 + 32*s25*sW2**2 - 32*s35*sW2**2 + s15*(3 - 16*sW2 + 32*sW2**2) - 6*tt +&
   48*sW2*tt - 96*sW2**2*tt) + me2*(s15**2 + 2*s15*s25 + s25**2 - s25*s35 + 6*mm2**2*(1&
   - 4*sW2)**2 + 4*s15*s25*sW2 + 4*s25**2*sW2 - 4*s15*s35*sW2 - 4*s25*s35*sW2 -&
   8*s15*s25*sW2**2 - 8*s25**2*sW2**2 + 8*s15*s35*sW2**2 + 8*s25*s35*sW2**2 +&
   2*ss**2*(1 - 4*sW2 + 8*sW2**2) - 5*s15*tt - s25*tt + 2*s35*tt + 24*s15*sW2*tt -&
   8*s25*sW2*tt - 16*s35*sW2*tt - 48*s15*sW2**2*tt + 16*s25*sW2**2*tt +&
   32*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 + ss*(s35 + s15*(-3 +&
   8*sW2 - 16*sW2**2) - 2*s25*(1 - 4*sW2 + 8*sW2**2) + 6*tt - 16*sW2*tt + 32*sW2**2*tt)&
   - 2*mm2*(-3*s25 + s35 + 2*ss - 8*s35*sW2 + 16*s35*sW2**2 - 4*s15*(1 - 2*sW2 +&
   4*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt)) + mm2*(s15**2 + s25**2 - s25*s35 +&
   4*s25**2*sW2 - 4*s25*s35*sW2 - 8*s25**2*sW2**2 + 8*s25*s35*sW2**2 + 2*ss**2*(1 -&
   4*sW2 + 8*sW2**2) - 5*s25*tt + 4*s35*tt + 24*s25*sW2*tt - 32*s35*sW2*tt -&
   48*s25*sW2**2*tt + 64*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 +&
   ss*(s35 + s15*(-3 + 8*sW2 - 16*sW2**2) - 4*s25*(1 - 2*sW2 + 4*sW2**2) + 6*tt -&
   16*sW2*tt + 32*sW2**2*tt) + s15*(s25*(4 + 4*sW2 - 8*sW2**2) + s35*(-4*sW2 +&
   8*sW2**2) + (-7 + 40*sW2 - 80*sW2**2)*tt))))/(s25*(s15 + s25 - s35)*(-s15 - s25 +&
   ss)*(-1 + sW2)*sW2) + s4m*(-(e**6*s3n*(4*s25*ss*sW2*(-1 + 2*sW2) + me2*(8*mm2 +&
   ss*(1 - 4*sW2)**2 + 8*s35*(1 - 2*sW2)*sW2) + mm2*ss*(-3 + 8*sW2 -&
   16*sW2**2)))/(2.*s25*(s15 + s25 - s35)*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   (e**6*me1*(-1 + 4*sW2)*(mm2*(-2*s15 + 4*ss) - (-s15 - s25 + s35 + ss)*(-s15 + ss +&
   tt)))/(s25*(s15 + s25 - s35)*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   (e**6*s4n*(me2*(-4*mm2 - s15 - s25 + s35 + ss - 4*ss*sW2 + 8*ss*sW2**2) + ss*(mm2*(1&
   - 4*sW2 + 8*sW2**2) + 4*sW2*tt - 8*sW2**2*tt)))/(s25*(s15 + s25 - s35)*(-s15 - s25 +&
   ss)*(-1 + sW2)*sW2) + (e**6*s2n*(-mm2**2 + me2**2*(1 - 4*sW2)**2 + 4*sW2*(-1 +&
   2*sW2)*tt*(-2*s15 - s25 + 2*ss + 2*tt) + mm2*(-4*s25*sW2 + 8*s25*sW2**2 + 2*s15*(1 -&
   4*sW2 + 8*sW2**2) - 2*ss*(1 - 4*sW2 + 8*sW2**2) + tt + 8*sW2*tt - 16*sW2**2*tt) +&
   me2*(s25*(4*sW2 - 8*sW2**2) + 4*mm2*(1 - 2*sW2 + 4*sW2**2) + (-1 + 16*sW2 -&
   32*sW2**2)*tt)))/(2.*s25*(s15 + s25 - s35)*(-s15 - s25 + ss)*(-1 + sW2)*sW2)) -&
   (e**6*snm*(2*me2**3*(1 - 4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 + 4*s15**2*sW2*(-1 +&
   2*sW2)*(s25 - 2*tt) + me2**2*(-s15 - 3*s25 + 6*mm2*(1 - 4*sW2)**2 + 16*s25*sW2 -&
   32*s25*sW2**2 - 6*tt + 48*sW2*tt - 96*sW2**2*tt) + mm2**2*(s15*(1 - 16*sW2 +&
   32*sW2**2) + s25*(1 - 16*sW2 + 32*sW2**2) - 2*(1 - 4*sW2)**2*(s35 + 3*tt)) -&
   2*ss*(2*s15*sW2*(-1 + 2*sW2)*(s25 - 2*tt) + s25*(2*s35*sW2 - 4*s35*sW2**2 + tt) +&
   tt*(s35*(-4*sW2 + 8*sW2**2) + (1 - 4*sW2)**2*tt)) - 2*tt*(s25**2*(-1 + 2*sW2 -&
   4*sW2**2) + (1 - 4*sW2)**2*tt*(s35 + tt) + s25*(s35 + 4*sW2*tt - 8*sW2**2*tt)) +&
   2*s15*(s25**2*(-2*sW2 + 4*sW2**2) + s25*(2*s35*sW2 - 4*s35*sW2**2 + tt + 2*sW2*tt -&
   4*sW2**2*tt) + tt*(-4*s35*sW2 + 8*s35*sW2**2 + tt - 12*sW2*tt + 24*sW2**2*tt)) +&
   mm2*(-s15**2 - 2*s25**2 + 2*s25*s35 + 4*s25**2*sW2 - 8*s25**2*sW2**2 - s25*tt +&
   4*s35*tt + 24*s25*sW2*tt - 32*s35*sW2*tt - 48*s25*sW2**2*tt + 64*s35*sW2**2*tt +&
   6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 + ss*(s15 - s35 + 2*(1 - 4*sW2)**2*tt) +&
   s15*(s35 + s25*(-5 + 4*sW2 - 8*sW2**2) + (-3 + 40*sW2 - 80*sW2**2)*tt)) +&
   me2*(s15**2 + s15*s25 - s15*s35 - s15*ss + s35*ss + 6*mm2**2*(1 - 4*sW2)**2 +&
   4*s15*s25*sW2 + 4*s25**2*sW2 - 8*s15*s25*sW2**2 - 8*s25**2*sW2**2 - s15*tt +&
   3*s25*tt + 2*s35*tt + 2*ss*tt + 24*s15*sW2*tt - 8*s25*sW2*tt - 16*s35*sW2*tt -&
   16*ss*sW2*tt - 48*s15*sW2**2*tt + 16*s25*sW2**2*tt + 32*s35*sW2**2*tt +&
   32*ss*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 + 2*mm2*(s25 + 2*s15*(1 -&
   4*sW2 + 8*sW2**2) - (1 - 4*sW2)**2*(s35 + 6*tt)))))/(2.*s25*(s15 + s25 - s35)*(-s15&
   - s25 + ss)*(-1 + sW2)*sW2) + s1m*((e**6*me1*(-1 + 4*sW2)*(me2**2 + 3*mm2**2 +&
   s15**2 - s25**2 + s25*s35 - 2*s15*ss + ss**2 + me2*(4*mm2 + s15 - s35 - ss - 2*tt) -&
   2*s15*tt - s25*tt + s35*tt + 2*ss*tt + tt**2 - mm2*(-3*s15 - 2*s25 + s35 + 5*ss +&
   4*tt)))/(s25*(s15 + s25 - s35)*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   (e**6*s2n*(me2*(s15 + s25 - ss + 2*mm2*(1 - 4*sW2)**2) + 2*mm2**2*(1 - 4*sW2)**2 -&
   4*s25*(-s15 - s25 + ss)*sW2*(-1 + 2*sW2) + mm2*(-5*s25 + 8*s25*sW2 - 16*s25*sW2**2 +&
   s15*(-3 + 8*sW2 - 16*sW2**2) + ss*(3 - 8*sW2 + 16*sW2**2) - 2*tt + 16*sW2*tt -&
   32*sW2**2*tt)))/(2.*s25*(s15 + s25 - s35)*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   (e**6*s4n*(mm2**2 - me2**2*(1 - 4*sW2)**2 - mm2*(-s15 - 3*s25 + s35 + 4*s25*sW2 -&
   8*s25*sW2**2 + ss*(3 - 8*sW2 + 16*sW2**2) + tt + 8*sW2*tt - 16*sW2**2*tt) + me2*(s15&
   + s25 + s35 - ss*(1 - 4*sW2)**2 - 4*s25*sW2 - 8*s35*sW2 + 8*s25*sW2**2 +&
   16*s35*sW2**2 + mm2*(4 + 8*sW2 - 16*sW2**2) + tt - 16*sW2*tt + 32*sW2**2*tt) +&
   4*sW2*(-1 + 2*sW2)*(-(s15*s25) - s25**2 + s25*s35 + s25*ss + 2*s15*tt + s25*tt -&
   2*s35*tt - 2*tt**2)))/(2.*s25*(s15 + s25 - s35)*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   (e**6*s3n*(me2**2*(3 - 8*sW2 + 16*sW2**2) + mm2**2*(3 - 16*sW2 + 32*sW2**2) +&
   mm2*(4*s25 - 3*s35 - 20*s25*sW2 + 16*s35*sW2 + 40*s25*sW2**2 - 32*s35*sW2**2 -&
   6*ss*(1 - 4*sW2 + 8*sW2**2) + s15*(3 - 16*sW2 + 32*sW2**2) - 5*tt + 24*sW2*tt -&
   48*sW2**2*tt) + me2*(-4*s25 - s35 - 4*s25*sW2 + 8*s35*sW2 + 8*ss*sW2 + 8*s25*sW2**2&
   - 16*s35*sW2**2 - 16*ss*sW2**2 + s15*(-1 - 8*sW2 + 16*sW2**2) + 2*mm2*(5 - 12*sW2 +&
   24*sW2**2) - 5*tt + 16*sW2*tt - 32*sW2**2*tt) + 2*(-s25**2 + s25*s35 + 4*s25**2*sW2&
   - 2*s25*s35*sW2 - 8*s25**2*sW2**2 + 4*s25*s35*sW2**2 + s35*tt + 2*s25*sW2*tt -&
   4*s35*sW2*tt - 4*s25*sW2**2*tt + 8*s35*sW2**2*tt + tt**2 - 4*sW2*tt**2 +&
   8*sW2**2*tt**2 + ss*(1 - 4*sW2 + 8*sW2**2)*(s25 + tt) - s15*(s25 - 2*s25*sW2 +&
   4*s25*sW2**2 + tt - 4*sW2*tt + 8*sW2**2*tt))))/(2.*s25*(s15 + s25 - s35)*(-s15 - s25&
   + ss)*(-1 + sW2)*sW2))

  m27 = -((e**6*me1*s2n*(-1 + 4*sW2)*(2*me2**2 + 2*mm2**2 - s15*s25 - s25**2 + s25*s35 + s25*ss&
   + mm2*(6*s15 + 4*s25 - 2*s35 - 5*ss - 4*tt) + me2*(4*mm2 + 2*s15 - 2*s35 - ss -&
   4*tt) - 4*s15*tt - 2*s25*tt + 2*s35*tt + 3*ss*tt + 2*tt**2))/(s15*s25*(s15 + s25 -&
   ss)*(-1 + sW2)*sW2)) + (e**6*me1*s4n*(-1 + 4*sW2)*(4*me2**2 + 2*mm2**2 + 2*s15**2 +&
   3*s15*s25 + s25**2 - 3*s15*s35 - 2*s25*s35 + s35**2 - 4*s15*ss - 3*s25*ss + 2*s35*ss&
   + 2*ss**2 + me2*(2*mm2 + 6*s15 + 6*s25 - 3*s35 - 6*ss - 6*tt) + mm2*(4*s15 + 4*s25 -&
   3*s35 - 4*ss - 4*tt) - 4*s15*tt - 4*s25*tt + 3*s35*tt + 4*ss*tt +&
   2*tt**2))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*me1*s3n*(-1 +&
   4*sW2)*(2*mm2**2 + 2*s15**2 + 2*s15*s25 - 3*s15*s35 - s25*s35 + s35**2 + s25*ss +&
   s35*ss - ss**2 + mm2*(4*s15 + 2*s25 - 3*s35 - 2*ss - 4*tt) - 4*s15*tt - 2*s25*tt +&
   3*s35*tt + 2*ss*tt + 2*tt**2 - me2*(2*mm2 + 3*s35 - 2*ss + 2*tt)))/(s15*s25*(s15 +&
   s25 - ss)*(-1 + sW2)*sW2) + s3m*((e**6*s2n*(-(mm2*(2*s15 + s25 - 12*s15*sW2 -&
   4*s25*sW2 + 24*s15*sW2**2 + 8*s25*sW2**2 - 2*ss*(1 - 6*sW2 + 12*sW2**2))) - (-s15 -&
   s25 + ss)*(1 - 4*sW2 + 8*sW2**2)*tt))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   (e**6*s3n*(-me2**2 + ss*(-(mm2*(1 - 4*sW2)**2) - (-s15 - s25 + ss)*(1 - 4*sW2 +&
   8*sW2**2)) + me2*(-3*s15 - 2*s25 + 8*s15*sW2 + 8*s25*sW2 - 16*s15*sW2**2 -&
   16*s25*sW2**2 + ss*(3 - 8*sW2 + 16*sW2**2) + mm2*(1 - 16*sW2 + 32*sW2**2) +&
   tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) - (e**6*me1*(-1 + 4*sW2)*(2*me2**2 +&
   2*mm2*s15 + 2*s15**2 + mm2*s25 + s15*s25 + mm2*s35 - s15*s35 + 3*ss**2 + me2*(-2*mm2&
   + 8*s15 + 5*s25 + s35 - 8*ss - 2*tt) - 2*s15*tt - s25*tt - s35*tt + ss*(-5*s15 -&
   4*s25 + s35 + 2*tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) -&
   (e**6*s4n*(mm2*ss*(1 - 4*sW2)**2 + me2**2*(5 - 16*sW2 + 32*sW2**2) + me2*(mm2 +&
   2*s15 + s25 - s35 - 12*s15*sW2 - 4*s25*sW2 + 8*s35*sW2 + 24*s15*sW2**2 +&
   8*s25*sW2**2 - 16*s35*sW2**2 - 2*ss*(1 - 4*sW2 + 8*sW2**2) - 3*tt + 16*sW2*tt -&
   32*sW2**2*tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2)) + s4m*((e**6*me1*(-1 +&
   4*sW2)*(2*me2**2 + mm2*(s25 - s35 - 2*ss) + me2*(6*mm2 + s25 - s35 - 2*tt) + (-s25 +&
   s35)*tt))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*s2n*(mm2*(-s15 + ss +&
   4*s15*sW2 - 4*s25*sW2 - 4*ss*sW2 - 8*s15*sW2**2 + 8*s25*sW2**2 + 8*ss*sW2**2) +&
   4*(-s15 - s25 + ss)*sW2*(-1 + 2*sW2)*(-2*me2 - s15 + ss + tt)))/(s15*s25*(s15 + s25&
   - ss)*(-1 + sW2)*sW2) + (e**6*s4n*(-me2**2 - ss*(mm2*(1 - 4*sW2)**2 + 4*(-s15 - s25&
   + ss)*sW2*(-1 + 2*sW2)) + me2*(8*s15*sW2 + 8*s25*sW2 - 8*ss*sW2 - 16*s15*sW2**2 -&
   16*s25*sW2**2 + 16*ss*sW2**2 + mm2*(1 - 16*sW2 + 32*sW2**2) + tt)))/(s15*s25*(s15 +&
   s25 - ss)*(-1 + sW2)*sW2) + (e**6*s3n*(-(mm2*ss*(1 - 4*sW2)**2) + me2**2*(3 - 16*sW2&
   + 32*sW2**2) + me2*(s15 + s25 - s35 - ss*(1 - 4*sW2)**2 - 12*s15*sW2 - 4*s25*sW2 +&
   8*s35*sW2 + 24*s15*sW2**2 + 8*s25*sW2**2 - 16*s35*sW2**2 + mm2*(3 - 32*sW2 +&
   64*sW2**2) - tt + 16*sW2*tt - 32*sW2**2*tt)))/(s15*s25*(s15 + s25 - ss)*(-1 +&
   sW2)*sW2)) + s1m*((2*e**6*mm2*s2n*(1 - 4*sW2)**2)/(s15*s25*(-1 + sW2)*sW2) +&
   (e**6*s4n*(me2**2 + mm2*(4*s15*sW2 - 8*s15*sW2**2 + s25*(-1 + 12*sW2 - 24*sW2**2) +&
   ss*(1 - 12*sW2 + 24*sW2**2)) + me2*(mm2 - s35 - tt) + 4*(-s15 - s25 + ss)*sW2*(-1 +&
   2*sW2)*(s15 - s35 - tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*me1*(-1&
   + 4*sW2)*(3*me2*(s15 + s25 - ss) + mm2*(3*s15 + s25 - ss) + (-s15 - s25 + ss)*(-s15&
   - 2*s25 + 2*s35 + ss + tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   (e**6*s3n*(me2**2 + mm2*s15 + s15**2 + 2*s15*s25 + s25**2 - s15*s35 - s25*s35 -&
   4*mm2*s15*sW2 - 4*s15**2*sW2 + 4*mm2*s25*sW2 - 8*s15*s25*sW2 - 4*s25**2*sW2 +&
   4*s15*s35*sW2 + 4*s25*s35*sW2 + 8*mm2*s15*sW2**2 + 8*s15**2*sW2**2 -&
   8*mm2*s25*sW2**2 + 16*s15*s25*sW2**2 + 8*s25**2*sW2**2 - 8*s15*s35*sW2**2 -&
   8*s25*s35*sW2**2 + ss**2*(1 - 4*sW2 + 8*sW2**2) + me2*(mm2 + 4*s15 + 3*s25 - s35 -&
   8*s15*sW2 - 8*s25*sW2 + 16*s15*sW2**2 + 16*s25*sW2**2 + ss*(-3 + 8*sW2 - 16*sW2**2)&
   - tt) - s15*tt - s25*tt + 4*s15*sW2*tt + 4*s25*sW2*tt - 8*s15*sW2**2*tt -&
   8*s25*sW2**2*tt + ss*(s35 - 4*mm2*sW2 - 4*s35*sW2 + 8*mm2*sW2**2 + 8*s35*sW2**2 -&
   2*s15*(1 - 4*sW2 + 8*sW2**2) - 2*s25*(1 - 4*sW2 + 8*sW2**2) + tt - 4*sW2*tt +&
   8*sW2**2*tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2)) + (e**6*(2*s15**3 +&
   3*s15**2*s25 + s15*s25**2 - 3*s15**2*s35 - 2*s15*s25*s35 + s15*s35**2 - 6*s15**2*ss&
   - 7*s15*s25*ss - 2*s25**2*ss + 5*s15*s35*ss + 3*s25*s35*ss - s35**2*ss + 6*s15*ss**2&
   + 4*s25*ss**2 - 2*s35*ss**2 - 2*ss**3 + 4*me2**3*(1 - 4*sW2)**2 - 2*mm2**2*(-s15 -&
   s25 + ss)*(1 - 4*sW2)**2 - 12*s15**3*sW2 - 16*s15**2*s25*sW2 - 4*s15*s25**2*sW2 +&
   20*s15**2*s35*sW2 + 12*s15*s25*s35*sW2 - 8*s15*s35**2*sW2 + 28*s15**2*ss*sW2 +&
   28*s15*s25*ss*sW2 + 8*s25**2*ss*sW2 - 28*s15*s35*ss*sW2 - 12*s25*s35*ss*sW2 +&
   8*s35**2*ss*sW2 - 24*s15*ss**2*sW2 - 16*s25*ss**2*sW2 + 8*s35*ss**2*sW2 +&
   8*ss**3*sW2 + 24*s15**3*sW2**2 + 32*s15**2*s25*sW2**2 + 8*s15*s25**2*sW2**2 -&
   40*s15**2*s35*sW2**2 - 24*s15*s25*s35*sW2**2 + 16*s15*s35**2*sW2**2 -&
   56*s15**2*ss*sW2**2 - 56*s15*s25*ss*sW2**2 - 16*s25**2*ss*sW2**2 +&
   56*s15*s35*ss*sW2**2 + 24*s25*s35*ss*sW2**2 - 16*s35**2*ss*sW2**2 +&
   48*s15*ss**2*sW2**2 + 32*s25*ss**2*sW2**2 - 16*s35*ss**2*sW2**2 - 16*ss**3*sW2**2 -&
   4*s15**2*tt - 5*s15*s25*tt - s25**2*tt + 3*s15*s35*tt + s25*s35*tt + 8*s15*ss*tt +&
   6*s25*ss*tt - 2*s35*ss*tt - 4*ss**2*tt + 28*s15**2*sW2*tt + 32*s15*s25*sW2*tt +&
   4*s25**2*sW2*tt - 24*s15*s35*sW2*tt - 8*s25*s35*sW2*tt - 40*s15*ss*sW2*tt -&
   24*s25*ss*sW2*tt + 16*s35*ss*sW2*tt + 16*ss**2*sW2*tt - 56*s15**2*sW2**2*tt -&
   64*s15*s25*sW2**2*tt - 8*s25**2*sW2**2*tt + 48*s15*s35*sW2**2*tt +&
   16*s25*s35*sW2**2*tt + 80*s15*ss*sW2**2*tt + 48*s25*ss*sW2**2*tt -&
   32*s35*ss*sW2**2*tt - 32*ss**2*sW2**2*tt + 2*s15*tt**2 + 2*s25*tt**2 - 2*ss*tt**2 -&
   16*s15*sW2*tt**2 - 16*s25*sW2*tt**2 + 16*ss*sW2*tt**2 + 32*s15*sW2**2*tt**2 +&
   32*s25*sW2**2*tt**2 - 32*ss*sW2**2*tt**2 + 2*me2**2*(2*s25 + 4*mm2*(1 - 4*sW2)**2 +&
   ss*(-3 + 8*sW2 - 16*sW2**2) + 4*s15*(1 - 4*sW2 + 8*sW2**2) - 2*(1 - 4*sW2)**2*(s35 +&
   2*tt)) + me2*(8*s15**2 + 9*s15*s25 + s25**2 - 5*s15*s35 - s25*s35 + 4*mm2**2*(1 -&
   4*sW2)**2 - 44*s15**2*sW2 - 48*s15*s25*sW2 - 4*s25**2*sW2 + 40*s15*s35*sW2 +&
   8*s25*s35*sW2 + 88*s15**2*sW2**2 + 96*s15*s25*sW2**2 + 8*s25**2*sW2**2 -&
   80*s15*s35*sW2**2 - 16*s25*s35*sW2**2 - 14*s15*ss*(1 - 4*sW2 + 8*sW2**2) +&
   2*ss**2*(3 - 8*sW2 + 16*sW2**2) - 14*s15*tt - 10*s25*tt + 4*s35*tt + 80*s15*sW2*tt +&
   48*s25*sW2*tt - 32*s35*sW2*tt - 160*s15*sW2**2*tt - 96*s25*sW2**2*tt +&
   64*s35*sW2**2*tt + 4*tt**2 - 32*sW2*tt**2 + 64*sW2**2*tt**2 - 2*mm2*(-3*s25 + 2*s35&
   + 8*s25*sW2 - 16*s35*sW2 - 16*s25*sW2**2 + 32*s35*sW2**2 + s15*(-5 + 24*sW2 -&
   48*sW2**2) + 4*ss*(1 - 4*sW2 + 8*sW2**2) + 4*tt - 32*sW2*tt + 64*sW2**2*tt) +&
   4*ss*(s35*(1 - 4*sW2)**2 - 2*s25*(1 - 3*sW2 + 6*sW2**2) + (3 - 16*sW2 +&
   32*sW2**2)*tt)) + mm2*(2*ss**2 + s25**2*(1 - 4*sW2 + 8*sW2**2) + 4*s15**2*(1 - 7*sW2&
   + 14*sW2**2) + s15*s25*(5 - 32*sW2 + 64*sW2**2) - s25*(1 - 4*sW2)**2*(s35 + 4*tt) -&
   s15*(1 - 4*sW2)**2*(3*s35 + 4*tt) - 2*ss*(3*s15*(1 - 4*sW2 + 8*sW2**2) + s25*(2 -&
   4*sW2 + 8*sW2**2) - (1 - 4*sW2)**2*(s35 + 2*tt)))))/(s15*s25*(s15 + s25 - ss)*(-1 +&
   sW2)*sW2) + (e**6*snm*(-2*me2**3*(1 - 4*sW2)**2 + mm2**2*(-s15 - s25 + ss)*(1 -&
   4*sW2)**2 - me2**2*(1 - 4*sW2)**2*(4*mm2 + 2*s15 - 2*s35 - ss - 4*tt) + me2*(s15*s25&
   + s25**2 - s25*s35 - 2*mm2**2*(1 - 4*sW2)**2 + 12*s15**2*sW2 + 8*s15*s25*sW2 -&
   4*s25**2*sW2 - 16*s15*s35*sW2 - 8*s25*s35*sW2 - 24*s15**2*sW2**2 - 16*s15*s25*sW2**2&
   + 8*s25**2*sW2**2 + 32*s15*s35*sW2**2 + 16*s25*s35*sW2**2 + 5*s15*tt + 3*s25*tt -&
   2*s35*tt - 40*s15*sW2*tt - 24*s25*sW2*tt + 16*s35*sW2*tt + 80*s15*sW2**2*tt +&
   48*s25*sW2**2*tt - 32*s35*sW2**2*tt - 2*tt**2 + 16*sW2*tt**2 - 32*sW2**2*tt**2 +&
   mm2*(1 - 4*sW2)**2*(-3*s15 - s25 + 2*s35 + 2*ss + 4*tt) - ss*(s25 + 12*s15*sW2 -&
   4*s25*sW2 - 16*s35*sW2 - 24*s15*sW2**2 + 8*s25*sW2**2 + 32*s35*sW2**2 + 4*tt -&
   32*sW2*tt + 64*sW2**2*tt)) - (-s15 - s25 + ss)*(s15**2*(4*sW2 - 8*sW2**2) +&
   ss*(s35*(4*sW2 - 8*sW2**2) + s15*(-4*sW2 + 8*sW2**2) - (1 - 4*sW2)**2*tt) +&
   s15*(-4*s35*sW2 + 8*s35*sW2**2 + tt - 12*sW2*tt + 24*sW2**2*tt) - tt*(s25*(-1 +&
   4*sW2 - 8*sW2**2) + (1 - 4*sW2)**2*(s35 + tt))) + mm2*(s15**2*(-1 + 12*sW2 -&
   24*sW2**2) - s15*(1 - 4*sW2)**2*(3*s25 - s35 - 2*tt) + ss*(s25*(1 - 4*sW2 +&
   8*sW2**2) + s15*(1 - 12*sW2 + 24*sW2**2) - (1 - 4*sW2)**2*(s35 + 2*tt)) +&
   s25*(s25*(-1 + 4*sW2 - 8*sW2**2) + (1 - 4*sW2)**2*(s35 + 2*tt)))))/(s15*s25*(s15 +&
   s25 - ss)*(-1 + sW2)*sW2)

  m29 = (e**6*me1*s4n*(-1 + 4*sW2)*(me2**2 - mm2**2 + 2*s15**2 + 2*s15*s25 - 2*s15*s35 -&
   2*s25*s35 + mm2*(2*s15 + 3*s25 - 3*ss) - 5*s15*ss - 3*s25*ss + s35*ss + 3*ss**2 +&
   me2*(4*s15 + 3*s25 - 2*s35 - 5*ss - 2*tt) - 3*s15*tt - 2*s25*tt + s35*tt + 4*ss*tt +&
   tt**2))/(s25*s35*ss*(-1 + sW2)*sW2) + (e**6*me1*s2n*(-1 + 4*sW2)*(me2*(-2*s15 -&
   2*s25 - s35 + 2*ss) + mm2*(-2*s15 - 2*s25 - s35 + 2*ss) - (-s15 - s25 - s35 +&
   ss)*(-s15 + ss + tt)))/(s25*s35*ss*(-1 + sW2)*sW2) - (e**6*me1*s3n*(-1 +&
   4*sW2)*(me2**2 + 3*mm2**2 + 2*me2*(2*mm2 + s15 - s35 - tt) - (s15 - s35 - tt)*(-s15&
   + ss + tt) - 2*mm2*(-2*s15 + s35 + ss + 2*tt)))/(s25*s35*ss*(-1 + sW2)*sW2) +&
   s3m*(-(e**6*s4n*(2*me2**2 + ss*(mm2*(1 - 4*sW2)**2 + 4*s25*sW2 - 8*s25*sW2**2) +&
   me2*(2*mm2 + ss*(-3 + 8*sW2 - 16*sW2**2) + 2*(s15 - s35 - 4*s15*sW2 - 4*s25*sW2 +&
   4*s35*sW2 + 8*s15*sW2**2 + 8*s25*sW2**2 - 8*s35*sW2**2 - tt))))/(2.*s25*s35*ss*(-1 +&
   sW2)*sW2) + (4*e**6*s3n*(-1 + 2*sW2)*(me2 + mm2 + s15 - ss - tt))/(s25*s35*(-1 +&
   sW2)) - (e**6*me1*(-1 + 4*sW2)*(me2**2 + 3*mm2**2 + me2*(4*mm2 + 2*s15 - 2*s35 - ss&
   - 2*tt) + (-s15 + ss + tt)*(-s15 + s35 + ss + tt) - mm2*(-4*s15 + 2*s35 + 5*ss +&
   4*tt)))/(s25*s35*ss*(-1 + sW2)*sW2) + (e**6*s2n*(me2**2*(1 + 8*sW2 - 16*sW2**2) -&
   me2*(-s15 + ss - 24*mm2*sW2 + 12*s25*sW2 + 48*mm2*sW2**2 - 24*s25*sW2**2 + tt +&
   16*sW2*tt - 32*sW2**2*tt) - (mm2 + s15 - ss - tt)*(mm2*(1 - 16*sW2 + 32*sW2**2) -&
   4*sW2*(-1 + 2*sW2)*(s25 + 2*tt))))/(2.*s25*s35*ss*(-1 + sW2)*sW2)) +&
   s4m*((e**6*s3n*(2*me2**2 - ss*(mm2*(1 - 4*sW2)**2 + 4*s25*sW2 - 8*s25*sW2**2) +&
   me2*(2*mm2 - ss*(1 - 4*sW2)**2 + 2*(s15 - 4*s15*sW2 - 4*s25*sW2 + 4*s35*sW2 +&
   8*s15*sW2**2 + 8*s25*sW2**2 - 8*s35*sW2**2 - tt))))/(2.*s25*s35*ss*(-1 + sW2)*sW2) +&
   (e**6*me1*(-1 + 4*sW2)*(me2**2 - mm2**2 + s15**2 + s15*s25 - mm2*(s15 - 2*s35) +&
   s15*s35 - 3*s15*ss - 2*s25*ss - s35*ss + 2*ss**2 + me2*(3*s15 + 2*s25 - 4*ss - 2*tt)&
   - 2*s15*tt - s25*tt - s35*tt + 3*ss*tt + tt**2))/(s25*s35*ss*(-1 + sW2)*sW2) +&
   (e**6*s4n*(me2*(-s15 - s25 + s35 + ss*(2 - 4*sW2 + 8*sW2**2)) + ss*(s15 + s25 +&
   4*mm2*sW2 - 4*s15*sW2 - 4*s25*sW2 - 8*mm2*sW2**2 + 8*s15*sW2**2 + 8*s25*sW2**2 +&
   ss*(-1 + 4*sW2 - 8*sW2**2) - tt + 4*sW2*tt - 8*sW2**2*tt)))/(s25*s35*ss*(-1 +&
   sW2)*sW2) + (e**6*s2n*(mm2**2 + me2**2*(3 - 8*sW2 + 16*sW2**2) + me2*(6*s15 + 4*s25&
   - 16*s15*sW2 - 4*s25*sW2 + 32*s15*sW2**2 + 8*s25*sW2**2 + 4*mm2*(1 - 2*sW2 +&
   4*sW2**2) - 2*ss*(3 - 8*sW2 + 16*sW2**2) - 5*tt + 16*sW2*tt - 32*sW2**2*tt) +&
   mm2*(2*s15 + 2*s25 - 2*ss - 4*s25*sW2 + 8*s25*sW2**2 - 3*tt + 8*sW2*tt -&
   16*sW2**2*tt) + 2*(s15**2*(1 - 4*sW2 + 8*sW2**2) + ss**2*(1 - 4*sW2 + 8*sW2**2) +&
   s15*(1 - 4*sW2 + 8*sW2**2)*(s25 - 2*tt) - ss*(1 - 4*sW2 + 8*sW2**2)*(2*s15 + s25 -&
   2*tt) + tt*(-s25 + 2*s25*sW2 - 4*s25*sW2**2 + tt - 4*sW2*tt +&
   8*sW2**2*tt))))/(2.*s25*s35*ss*(-1 + sW2)*sW2)) + s1m*((e**6*me1*(-1 +&
   4*sW2)*(4*mm2**2 - 2*s15*s35 - s25*s35 + s15*ss + s25*ss + 2*s35*ss - ss**2 +&
   me2*(4*mm2 - s15 - s25 - 2*s35 + 2*ss) + mm2*(5*s15 + 3*s25 - 4*s35 - 4*ss - 4*tt) +&
   2*s35*tt - ss*tt))/(s25*s35*ss*(-1 + sW2)*sW2) + (e**6*s2n*(2*mm2**2*(1 - 4*sW2)**2&
   + 4*s25*(-s15 - s25 + ss)*sW2*(-1 + 2*sW2) + me2*(-s15 - s25 + ss + 2*mm2*(-1 -&
   8*sW2 + 16*sW2**2)) + mm2*(3*s25 + 3*s15*(1 - 4*sW2)**2 - 3*ss*(1 - 4*sW2)**2 -&
   8*s25*sW2 + 16*s25*sW2**2 - 2*tt + 16*sW2*tt - 32*sW2**2*tt)))/(2.*s25*s35*ss*(-1 +&
   sW2)*sW2) + (e**6*s3n*(me2**2*(-1 - 8*sW2 + 16*sW2**2) + mm2**2*(1 - 16*sW2 +&
   32*sW2**2) + me2*(s35 + 4*s25*sW2 + 8*s35*sW2 + 8*ss*sW2 - 8*s25*sW2**2 -&
   16*s35*sW2**2 - 16*ss*sW2**2 + 4*mm2*(1 - 6*sW2 + 12*sW2**2) + s15*(-1 - 8*sW2 +&
   16*sW2**2) + tt + 16*sW2*tt - 32*sW2**2*tt) - mm2*(s35 + 4*s25*sW2 - 16*s35*sW2 -&
   16*ss*sW2 - 8*s25*sW2**2 + 32*s35*sW2**2 + 32*ss*sW2**2 + s15*(-1 + 24*sW2 -&
   48*sW2**2) + tt - 24*sW2*tt + 48*sW2**2*tt) + 4*sW2*(-1 + 2*sW2)*(2*s15**2 + s25*s35&
   + 2*ss**2 + s15*(s25 - 2*s35 - 4*tt) - 2*ss*(2*s15 + s25 - s35 - 2*tt) - s25*tt +&
   2*s35*tt + 2*tt**2)))/(2.*s25*s35*ss*(-1 + sW2)*sW2) + (e**6*s4n*(-mm2**2 +&
   me2**2*(-3 + 8*sW2 - 16*sW2**2) + mm2*(-3*s25 + s35 + 12*s25*sW2 - 24*s25*sW2**2 +&
   s15*(-3 + 8*sW2 - 16*sW2**2) + ss*(3 - 16*sW2 + 32*sW2**2) + 3*tt - 8*sW2*tt +&
   16*sW2**2*tt) + me2*(-s25 + s35 + ss*(1 - 4*sW2)**2 + 8*mm2*sW2 + 4*s25*sW2 -&
   8*s35*sW2 - 16*mm2*sW2**2 - 8*s25*sW2**2 + 16*s35*sW2**2 + s15*(-3 + 16*sW2 -&
   32*sW2**2) + 5*tt - 16*sW2*tt + 32*sW2**2*tt) + 2*(s25*s35 - 2*s25**2*sW2 -&
   2*s25*s35*sW2 + 4*s25**2*sW2**2 + 4*s25*s35*sW2**2 + s15**2*(-1 + 4*sW2 - 8*sW2**2)&
   + s15*s25*(-1 + 2*sW2 - 4*sW2**2) + s25*tt - s35*tt - 2*s25*sW2*tt + 4*s35*sW2*tt +&
   4*s25*sW2**2*tt - 8*s35*sW2**2*tt - tt**2 + 4*sW2*tt**2 - 8*sW2**2*tt**2 + s15*(1 -&
   4*sW2 + 8*sW2**2)*(s35 + 2*tt) - ss*(s35 - 2*s25*sW2 - 4*s35*sW2 + 4*s25*sW2**2 +&
   8*s35*sW2**2 + s15*(-1 + 4*sW2 - 8*sW2**2) + tt - 4*sW2*tt +&
   8*sW2**2*tt))))/(2.*s25*s35*ss*(-1 + sW2)*sW2)) + (e**6*(2*me2**3*(1 - 4*sW2)**2 +&
   2*mm2**3*(1 - 4*sW2)**2 + me2**2*(5*s15 + s25 - 4*s35 + 6*mm2*(1 - 4*sW2)**2 -&
   32*s15*sW2 + 32*s35*sW2 + 64*s15*sW2**2 - 64*s35*sW2**2 - 4*ss*(1 - 4*sW2 +&
   8*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) + mm2**2*(3*s25 - 2*s35 - 16*s25*sW2 +&
   16*s35*sW2 + 32*s25*sW2**2 - 32*s35*sW2**2 - 4*ss*(1 - 4*sW2 + 8*sW2**2) + s15*(5 -&
   32*sW2 + 64*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) + me2*(5*s15**2 + 3*s15*s25 -&
   5*s15*s35 - 2*s25*s35 - 2*s35**2 + 6*mm2**2*(1 - 4*sW2)**2 - 28*s15**2*sW2 -&
   12*s15*s25*sW2 + 36*s15*s35*sW2 + 4*s25*s35*sW2 + 56*s15**2*sW2**2 +&
   24*s15*s25*sW2**2 - 72*s15*s35*sW2**2 - 8*s25*s35*sW2**2 + 4*ss**2*(1 - 2*sW2 +&
   4*sW2**2) - 11*s15*tt - 3*s25*tt + 6*s35*tt + 72*s15*sW2*tt + 8*s25*sW2*tt -&
   48*s35*sW2*tt - 144*s15*sW2**2*tt - 16*s25*sW2**2*tt + 96*s35*sW2**2*tt + 6*tt**2 -&
   48*sW2*tt**2 + 96*sW2**2*tt**2 - 2*mm2*(3*s35 + 8*s25*sW2 - 24*s35*sW2 -&
   16*s25*sW2**2 + 48*s35*sW2**2 + s15*(-3 + 32*sW2 - 64*sW2**2) + 4*ss*(1 - 4*sW2 +&
   8*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt) + ss*(-2*s25 + 7*s35 - 32*s35*sW2 +&
   64*s35*sW2**2 + s15*(-9 + 32*sW2 - 64*sW2**2) + 10*tt - 48*sW2*tt + 96*sW2**2*tt)) +&
   mm2*(-2*s25*s35 + 4*s25*s35*sW2 - 8*s25*s35*sW2**2 + 4*ss**2*(1 - 2*sW2 + 4*sW2**2)&
   + s15**2*(5 - 28*sW2 + 56*sW2**2) - 5*s25*tt + 4*s35*tt + 24*s25*sW2*tt -&
   32*s35*sW2*tt - 48*s25*sW2**2*tt + 64*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 +&
   96*sW2**2*tt**2 + ss*(3*s35 - 16*s35*sW2 + 32*s35*sW2**2 + s15*(-9 + 32*sW2 -&
   64*sW2**2) - 4*s25*(1 - 4*sW2 + 8*sW2**2) + 10*tt - 48*sW2*tt + 96*sW2**2*tt) +&
   s15*(s35*(-3 + 20*sW2 - 40*sW2**2) + s25*(5 - 28*sW2 + 56*sW2**2) + (-11 + 72*sW2 -&
   144*sW2**2)*tt)) - 2*(s15**3*(-1 + 6*sW2 - 12*sW2**2) + ss**3*(1 - 4*sW2 + 8*sW2**2)&
   - ss**2*(1 - 4*sW2 + 8*sW2**2)*(3*s15 + s25 - s35 - 3*tt) + s15**2*(s35*(1 -&
   4*sW2)**2 + s25*(-1 + 6*sW2 - 12*sW2**2) + 3*(1 - 6*sW2 + 12*sW2**2)*tt) +&
   s15*(s35**2*(2*sW2 - 4*sW2**2) + s25*s35*(1 - 6*sW2 + 12*sW2**2) + 2*s25*(1 - 5*sW2&
   + 10*sW2**2)*tt - 2*s35*(1 - 7*sW2 + 14*sW2**2)*tt + (-3 + 20*sW2 -&
   40*sW2**2)*tt**2) + tt*((1 - 4*sW2)**2*tt*(s35 + tt) - s25*(s35 - 2*s35*sW2 +&
   4*s35*sW2**2 + tt - 4*sW2*tt + 8*sW2**2*tt)) + ss*(-2*s35**2*sW2 + 4*s35**2*sW2**2 +&
   s15**2*(3 - 14*sW2 + 28*sW2**2) + 2*s35*tt - 12*s35*sW2*tt + 24*s35*sW2**2*tt +&
   3*tt**2 - 16*sW2*tt**2 + 32*sW2**2*tt**2 - s25*(1 - 4*sW2 + 8*sW2**2)*(s35 + 2*tt) +&
   2*s15*(s35*(-1 + 6*sW2 - 12*sW2**2) + s25*(1 - 4*sW2 + 8*sW2**2) + (-3 + 14*sW2 -&
   28*sW2**2)*tt)))))/(s25*s35*ss*(-1 + sW2)*sW2) - (e**6*snm*(2*me2**3*(1 - 4*sW2)**2&
   + 2*mm2**3*(1 - 4*sW2)**2 + me2**2*(5*s15 + s25 - 4*s35 + 6*mm2*(1 - 4*sW2)**2 -&
   2*ss*(1 - 4*sW2)**2 - 32*s15*sW2 + 32*s35*sW2 + 64*s15*sW2**2 - 64*s35*sW2**2 - 6*tt&
   + 48*sW2*tt - 96*sW2**2*tt) + mm2**2*(3*s25 - 2*s35 - 2*ss*(1 - 4*sW2)**2 -&
   16*s25*sW2 + 16*s35*sW2 + 32*s25*sW2**2 - 32*s35*sW2**2 + s15*(5 - 32*sW2 +&
   64*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) + me2*(5*s15**2 + 3*s15*s25 -&
   7*s15*s35 - 4*s25*s35 + 6*mm2**2*(1 - 4*sW2)**2 - 24*s15**2*sW2 - 4*s15*s25*sW2 +&
   4*s25**2*sW2 + 32*s15*s35*sW2 + 48*s15**2*sW2**2 + 8*s15*s25*sW2**2 -&
   8*s25**2*sW2**2 - 64*s15*s35*sW2**2 - 11*s15*tt - 3*s25*tt + 6*s35*tt +&
   72*s15*sW2*tt + 8*s25*sW2*tt - 48*s35*sW2*tt - 144*s15*sW2**2*tt - 16*s25*sW2**2*tt&
   + 96*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 - 2*mm2*(3*s35 +&
   2*ss*(1 - 4*sW2)**2 + 8*s25*sW2 - 24*s35*sW2 - 16*s25*sW2**2 + 48*s35*sW2**2 +&
   s15*(-3 + 32*sW2 - 64*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt) + ss*(-8*s25*sW2 +&
   16*s25*sW2**2 + s15*(-5 + 24*sW2 - 48*sW2**2) + s35*(7 - 32*sW2 + 64*sW2**2) + 6*tt&
   - 48*sW2*tt + 96*sW2**2*tt)) + mm2*(-2*s25*s35 + 4*s25**2*sW2 - 8*s25**2*sW2**2 +&
   s15**2*(5 - 24*sW2 + 48*sW2**2) - 5*s25*tt + 4*s35*tt + 24*s25*sW2*tt -&
   32*s35*sW2*tt - 48*s25*sW2**2*tt + 64*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 +&
   96*sW2**2*tt**2 + ss*(8*s25*sW2 - 16*s25*sW2**2 + s15*(-5 + 24*sW2 - 48*sW2**2) +&
   s35*(3 - 16*sW2 + 32*sW2**2) + 6*tt - 48*sW2*tt + 96*sW2**2*tt) + s15*(s35*(-3 +&
   16*sW2 - 32*sW2**2) + 5*s25*(1 - 4*sW2 + 8*sW2**2) + (-11 + 72*sW2 -&
   144*sW2**2)*tt)) + 2*(s15**3*(1 - 4*sW2 + 8*sW2**2) + ss**2*(s35*(-1 + 4*sW2 -&
   8*sW2**2) + s15*(1 - 4*sW2 + 8*sW2**2) - (1 - 4*sW2)**2*tt) + s15**2*(s35*(-1 +&
   4*sW2 - 8*sW2**2) + s25*(1 - 2*sW2 + 4*sW2**2) + (-3 + 16*sW2 - 32*sW2**2)*tt) +&
   tt*(s25**2*(-2*sW2 + 4*sW2**2) - (1 - 4*sW2)**2*tt*(s35 + tt) + s25*(s35 + tt -&
   4*sW2*tt + 8*sW2**2*tt)) + ss*(-2*s15**2*(1 - 4*sW2 + 8*sW2**2) - 2*tt*(s35*(1 -&
   6*sW2 + 12*sW2**2) + (1 - 4*sW2)**2*tt) + s25*(s35 - 2*s35*sW2 + 4*s35*sW2**2 + tt -&
   4*sW2*tt + 8*sW2**2*tt) + s15*(s25*(-1 + 2*sW2 - 4*sW2**2) + 2*s35*(1 - 4*sW2 +&
   8*sW2**2) + 4*(1 - 6*sW2 + 12*sW2**2)*tt)) + s15*(s25**2*(2*sW2 - 4*sW2**2) -&
   s25*(s35*(1 - 2*sW2 + 4*sW2**2) + 2*(1 - 3*sW2 + 6*sW2**2)*tt) + tt*(2*s35*(1 -&
   6*sW2 + 12*sW2**2) + (3 - 20*sW2 + 40*sW2**2)*tt)))))/(2.*s25*s35*ss*(-1 + sW2)*sW2)

  m31 = -((e**6*me1*s2n*(-1 + 4*sW2)*(s15*s25 + s25**2 - s25*s35 + mm2*(s15 - s25 + s35 - ss) -&
   s25*ss + me2*(-s15 - s25 + s35 + ss) - s35*tt))/(s25*(s15 + s25 - s35)*ss*(-1 +&
   sW2)*sW2)) + (e**6*me1*s4n*(-1 + 4*sW2)*(me2**2 - mm2**2 + s15*s25 + s25**2 +&
   s15*s35 - s35**2 + mm2*(s35 - 3*ss) - s25*ss + me2*(s35 - ss - 2*tt) - s15*tt -&
   s25*tt + 2*ss*tt + tt**2))/(s25*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) -&
   (e**6*me1*s3n*(-1 + 4*sW2)*(me2**2 + 3*mm2**2 + mm2*(4*s15 + 3*s25 - 3*s35 - 4*tt) +&
   me2*(4*mm2 + 2*s15 + s25 - s35 - 2*ss - 2*tt) - (s15 - s35 - tt)*(-s15 - s25 + s35 +&
   ss + tt)))/(s25*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) + (e**6*(s15**2*s25 +&
   s15*s25**2 + s15**2*s35 - s15*s35**2 - s15*s25*ss - s15*s35*ss - s25*s35*ss +&
   s35**2*ss + 2*me2**3*(1 - 4*sW2)**2 + 2*mm2**3*(1 - 4*sW2)**2 - 4*s15**2*s25*sW2 -&
   4*s15*s25**2*sW2 - 4*s15**2*s35*sW2 + 4*s15*s35**2*sW2 + 4*s15*s25*ss*sW2 +&
   4*s15*s35*ss*sW2 + 4*s25*s35*ss*sW2 - 4*s35**2*ss*sW2 + 8*s15**2*s25*sW2**2 +&
   8*s15*s25**2*sW2**2 + 8*s15**2*s35*sW2**2 - 8*s15*s35**2*sW2**2 -&
   8*s15*s25*ss*sW2**2 - 8*s15*s35*ss*sW2**2 - 8*s25*s35*ss*sW2**2 + 8*s35**2*ss*sW2**2&
   - 2*s15**2*tt - 3*s15*s25*tt - s25**2*tt + s15*s35*tt + s25*s35*tt + 4*s15*ss*tt +&
   2*s25*ss*tt - 2*s35*ss*tt - 2*ss**2*tt + 8*s15**2*sW2*tt + 4*s15*s25*sW2*tt -&
   4*s25**2*sW2*tt - 4*s15*s35*sW2*tt + 4*s25*s35*sW2*tt - 16*s15*ss*sW2*tt -&
   8*s25*ss*sW2*tt + 8*s35*ss*sW2*tt + 8*ss**2*sW2*tt - 16*s15**2*sW2**2*tt -&
   8*s15*s25*sW2**2*tt + 8*s25**2*sW2**2*tt + 8*s15*s35*sW2**2*tt - 8*s25*s35*sW2**2*tt&
   + 32*s15*ss*sW2**2*tt + 16*s25*ss*sW2**2*tt - 16*s35*ss*sW2**2*tt -&
   16*ss**2*sW2**2*tt + 4*s15*tt**2 + 2*s25*tt**2 - 2*s35*tt**2 - 4*ss*tt**2 -&
   24*s15*sW2*tt**2 - 8*s25*sW2*tt**2 + 16*s35*sW2*tt**2 + 16*ss*sW2*tt**2 +&
   48*s15*sW2**2*tt**2 + 16*s25*sW2**2*tt**2 - 32*s35*sW2**2*tt**2 - 32*ss*sW2**2*tt**2&
   - 2*tt**3 + 16*sW2*tt**3 - 32*sW2**2*tt**3 + me2**2*(s15 - s25 - 2*ss + 6*mm2*(1 -&
   4*sW2)**2 + 16*s25*sW2 - 32*s25*sW2**2 - 6*tt + 48*sW2*tt - 96*sW2**2*tt) +&
   mm2**2*(3*s25 - 2*s35 - 2*ss - 16*s25*sW2 + 16*s35*sW2 + 32*s25*sW2**2 -&
   32*s35*sW2**2 + s15*(3 - 16*sW2 + 32*sW2**2) - 6*tt + 48*sW2*tt - 96*sW2**2*tt) +&
   me2*(-s15**2 - 2*s15*s25 - s25**2 + 4*s15*s35 + 3*s25*s35 - 2*s35**2 + 6*mm2**2*(1 -&
   4*sW2)**2 + 4*s15*s25*sW2 + 4*s25**2*sW2 - 4*s15*s35*sW2 - 4*s25*s35*sW2 -&
   8*s15*s25*sW2**2 - 8*s25**2*sW2**2 + 8*s15*s35*sW2**2 + 8*s25*s35*sW2**2 +&
   2*ss**2*(1 - 4*sW2 + 8*sW2**2) - 5*s15*tt - s25*tt + 2*s35*tt + 24*s15*sW2*tt -&
   8*s25*sW2*tt - 16*s35*sW2*tt - 48*s15*sW2**2*tt + 16*s25*sW2**2*tt +&
   32*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 - ss*(s35 + s15*(1 -&
   4*sW2)**2 - 8*s25*sW2 + 16*s25*sW2**2 - 6*tt + 16*sW2*tt - 32*sW2**2*tt) -&
   2*mm2*(s25 + s35 + 2*ss + 8*s15*sW2 - 8*s35*sW2 - 16*s15*sW2**2 + 16*s35*sW2**2 +&
   6*tt - 48*sW2*tt + 96*sW2**2*tt)) + mm2*(s15**2 + s25**2 - s25*s35 + 4*s25**2*sW2 -&
   4*s25*s35*sW2 - 8*s25**2*sW2**2 + 8*s25*s35*sW2**2 + 2*ss**2*(1 - 4*sW2 + 8*sW2**2)&
   - 5*s25*tt + 4*s35*tt + 24*s25*sW2*tt - 32*s35*sW2*tt - 48*s25*sW2**2*tt +&
   64*s35*sW2**2*tt + 6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 + ss*(s35 + s15*(-3 +&
   8*sW2 - 16*sW2**2) - 2*s25*(1 - 4*sW2 + 8*sW2**2) + 6*tt - 16*sW2*tt + 32*sW2**2*tt)&
   + s15*(s25*(2 + 4*sW2 - 8*sW2**2) + s35*(-4*sW2 + 8*sW2**2) + (-7 + 40*sW2 -&
   80*sW2**2)*tt))))/(s25*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) +&
   (e**6*snm*(-(s15**2*s25) - s15*s25**2 + s15*s25*s35 + s15*s25*ss - s25*s35*ss -&
   2*me2**3*(1 - 4*sW2)**2 - 2*mm2**3*(1 - 4*sW2)**2 + 4*s15**2*s25*sW2 +&
   4*s15*s25**2*sW2 - 4*s15*s25*s35*sW2 - 4*s15*s25*ss*sW2 + 4*s25*s35*ss*sW2 -&
   8*s15**2*s25*sW2**2 - 8*s15*s25**2*sW2**2 + 8*s15*s25*s35*sW2**2 +&
   8*s15*s25*ss*sW2**2 - 8*s25*s35*ss*sW2**2 + 2*s15**2*tt + 3*s15*s25*tt + s25**2*tt -&
   2*s15*s35*tt - 2*s25*s35*tt - 2*s15*ss*tt - 2*s25*ss*tt + 2*s35*ss*tt -&
   8*s15**2*sW2*tt - 4*s15*s25*sW2*tt + 4*s25**2*sW2*tt + 8*s15*s35*sW2*tt +&
   8*s15*ss*sW2*tt - 8*s35*ss*sW2*tt + 16*s15**2*sW2**2*tt + 8*s15*s25*sW2**2*tt -&
   8*s25**2*sW2**2*tt - 16*s15*s35*sW2**2*tt - 16*s15*ss*sW2**2*tt +&
   16*s35*ss*sW2**2*tt - 4*s15*tt**2 - 2*s25*tt**2 + 2*s35*tt**2 + 2*ss*tt**2 +&
   24*s15*sW2*tt**2 + 8*s25*sW2*tt**2 - 16*s35*sW2*tt**2 - 16*ss*sW2*tt**2 -&
   48*s15*sW2**2*tt**2 - 16*s25*sW2**2*tt**2 + 32*s35*sW2**2*tt**2 + 32*ss*sW2**2*tt**2&
   + 2*tt**3 - 16*sW2*tt**3 + 32*sW2**2*tt**3 + me2**2*(-s15 + s25 - 6*mm2*(1 -&
   4*sW2)**2 - 16*s25*sW2 + 32*s25*sW2**2 + 6*tt - 48*sW2*tt + 96*sW2**2*tt) +&
   mm2**2*(s15*(-3 + 16*sW2 - 32*sW2**2) + s25*(-3 + 16*sW2 - 32*sW2**2) + 2*(1 -&
   4*sW2)**2*(s35 + 3*tt)) + me2*(s15**2 + 2*s15*s25 + s25**2 - s15*s35 - 6*mm2**2*(1 -&
   4*sW2)**2 - 4*s15*s25*sW2 - 4*s25**2*sW2 + 8*s15*s25*sW2**2 + 8*s25**2*sW2**2 +&
   5*s15*tt + s25*tt - 2*s35*tt - 24*s15*sW2*tt + 8*s25*sW2*tt + 16*s35*sW2*tt +&
   48*s15*sW2**2*tt - 16*s25*sW2**2*tt - 32*s35*sW2**2*tt - 6*tt**2 + 48*sW2*tt**2 -&
   96*sW2**2*tt**2 + ss*(-s15 + s35 - 2*(1 - 4*sW2)**2*tt) + 2*mm2*(s25 + s35*(1 -&
   4*sW2)**2 + 8*s15*sW2 - 16*s15*sW2**2 + 6*tt - 48*sW2*tt + 96*sW2**2*tt)) -&
   mm2*(s15**2 + s25**2 - 2*s25*s35 + 4*s25**2*sW2 - 8*s25**2*sW2**2 - 5*s25*tt +&
   4*s35*tt + 24*s25*sW2*tt - 32*s35*sW2*tt - 48*s25*sW2**2*tt + 64*s35*sW2**2*tt +&
   6*tt**2 - 48*sW2*tt**2 + 96*sW2**2*tt**2 + ss*(-s15 + s35 + 2*(1 - 4*sW2)**2*tt) -&
   s15*(s35 + s25*(-2 - 4*sW2 + 8*sW2**2) + (7 - 40*sW2 +&
   80*sW2**2)*tt))))/(2.*s25*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) + s1m*((e**6*me1*(-1&
   + 4*sW2)*(4*mm2**2 + me2*(4*mm2 + s15 + s25 - ss) + (-s15 - s25 + ss)*(-s35 + ss) +&
   mm2*(s15 + s25 - 2*s35 - ss - 4*tt)))/(s25*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) +&
   (e**6*s2n*(2*mm2**2*(1 - 4*sW2)**2 - s25*(-s15 - s25 + ss)*(1 - 4*sW2 + 8*sW2**2) +&
   me2*(-s15 - s25 + ss + 2*mm2*(-1 - 8*sW2 + 16*sW2**2)) + mm2*(s25 - s15*(1 -&
   4*sW2)**2 + ss*(1 - 4*sW2)**2 + 8*s25*sW2 - 16*s25*sW2**2 - 2*tt + 16*sW2*tt -&
   32*sW2**2*tt)))/(2.*s25*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) + (e**6*s3n*(s15*s25 -&
   s25*s35 + me2**2*(1 - 4*sW2)**2 + 4*s15*s25*sW2 + 8*s25**2*sW2 - 4*s25*s35*sW2 -&
   8*s25*ss*sW2 - 8*s15*s25*sW2**2 - 16*s25**2*sW2**2 + 8*s25*s35*sW2**2 +&
   16*s25*ss*sW2**2 + mm2**2*(1 - 16*sW2 + 32*sW2**2) - s25*tt + 8*s15*sW2*tt +&
   4*s25*sW2*tt - 8*s35*sW2*tt - 8*ss*sW2*tt - 16*s15*sW2**2*tt - 8*s25*sW2**2*tt +&
   16*s35*sW2**2*tt + 16*ss*sW2**2*tt - 8*sW2*tt**2 + 16*sW2**2*tt**2 + me2*(s15 + s25&
   + s35 - 8*s15*sW2 - 4*s25*sW2 + 8*s35*sW2 + 16*s15*sW2**2 + 8*s25*sW2**2 -&
   16*s35*sW2**2 + 6*mm2*(1 - 4*sW2 + 8*sW2**2) - 2*ss*(1 - 4*sW2 + 8*sW2**2) - tt +&
   16*sW2*tt - 32*sW2**2*tt) - mm2*(-3*s25 + s35 + 20*s25*sW2 - 16*s35*sW2 -&
   40*s25*sW2**2 + 32*s35*sW2**2 + s15*(-1 + 16*sW2 - 32*sW2**2) + ss*(2 - 24*sW2 +&
   48*sW2**2) + tt - 24*sW2*tt + 48*sW2**2*tt)))/(2.*s25*(s15 + s25 - s35)*ss*(-1 +&
   sW2)*sW2) + (e**6*s4n*(-mm2**2 - me2**2*(1 - 4*sW2)**2 - mm2*(s15 - s35 + ss*(1 -&
   4*sW2)**2 + 4*s25*sW2 - 8*s25*sW2**2 - 3*tt + 8*sW2*tt - 16*sW2**2*tt) + me2*(s15 +&
   2*s25 + s35 - 4*s25*sW2 - 8*s35*sW2 + 8*s25*sW2**2 + 16*s35*sW2**2 + ss*(-3 + 8*sW2&
   - 16*sW2**2) + mm2*(2 + 8*sW2 - 16*sW2**2) + 3*tt - 16*sW2*tt + 32*sW2**2*tt) + (1 -&
   4*sW2 + 8*sW2**2)*(-(s15*s25) - s25**2 + s25*s35 + s25*ss + 2*s15*tt + s25*tt -&
   2*s35*tt - 2*tt**2)))/(2.*s25*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2)) +&
   s3m*((e**6*s3n*(mm2*ss*(1 - 12*sW2 + 24*sW2**2) + me2*(-s35 + ss - 4*ss*sW2 +&
   8*ss*sW2**2) - 4*ss*sW2*(-1 + 2*sW2)*(s25 + tt)))/(s25*(s15 + s25 - s35)*ss*(-1 +&
   sW2)*sW2) - (e**6*me1*(-1 + 4*sW2)*(me2**2 + 3*mm2**2 + s15*s35 + s25*s35 - s15*ss -&
   s35*ss + ss**2 + me2*(4*mm2 + s15 + s35 - 3*ss - 2*tt) - s15*tt + 2*ss*tt + tt**2 -&
   mm2*(-s15 + s35 + 3*ss + 4*tt)))/(s25*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) +&
   (e**6*s2n*(-(s15*s25) + s25*ss - me2**2*(1 - 4*sW2)**2 + 4*s15*s25*sW2 -&
   4*s25*ss*sW2 - 8*s15*s25*sW2**2 + 8*s25*ss*sW2**2 + mm2**2*(-1 + 16*sW2 - 32*sW2**2)&
   + s25*tt + 4*s25*sW2*tt - 8*s25*sW2**2*tt + 8*sW2*tt**2 - 16*sW2**2*tt**2 + me2*(s15&
   + s25 - ss - 4*s25*sW2 + 8*s25*sW2**2 + mm2*(-2 + 24*sW2 - 48*sW2**2) + tt -&
   16*sW2*tt + 32*sW2**2*tt) + mm2*(-s25 + s15*(1 - 4*sW2)**2 - ss*(1 - 4*sW2)**2 -&
   4*s25*sW2 + 8*s25*sW2**2 + tt - 24*sW2*tt + 48*sW2**2*tt)))/(2.*s25*(s15 + s25 -&
   s35)*ss*(-1 + sW2)*sW2) + (e**6*s4n*(-2*me2**2 + mm2*ss*(1 - 4*sW2)**2 + s25*ss*(-1&
   + 4*sW2 - 8*sW2**2) + me2*(-2*mm2 + ss*(3 - 8*sW2 + 16*sW2**2) + 2*(s35*(-1 + 4*sW2&
   - 8*sW2**2) + tt))))/(2.*s25*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2)) +&
   s4m*((e**6*s3n*(2*me2**2 + mm2*ss*(1 - 4*sW2)**2 + s25*ss*(-1 + 4*sW2 - 8*sW2**2) +&
   me2*(2*mm2 - ss*(1 - 4*sW2)**2 + 2*(s15 + s25 - 4*s35*sW2 + 8*s35*sW2**2 -&
   tt))))/(2.*s25*(s15 + s25 - s35)*ss*(-1 + sW2)*sW2) + (e**6*s4n*(1 - 4*sW2 +&
   8*sW2**2)*(me2 + mm2 - tt))/(s25*(s15 + s25 - s35)*(-1 + sW2)*sW2) + (e**6*me1*(-1 +&
   4*sW2)*(me2**2 - mm2**2 + s15*s25 + s25**2 + mm2*s35 - s25*s35 + s15*ss - ss**2 +&
   me2*(s35 - 2*tt) - s15*tt - s25*tt + ss*tt + tt**2))/(s25*(s15 + s25 - s35)*ss*(-1 +&
   sW2)*sW2) + (e**6*s2n*(me2**2*(1 - 4*sW2)**2 + me2*(s25*(-1 + 4*sW2 - 8*sW2**2) +&
   2*mm2*(1 - 4*sW2 + 8*sW2**2) + (-3 + 16*sW2 - 32*sW2**2)*tt) + (mm2 - tt)*(mm2 - (1&
   - 4*sW2 + 8*sW2**2)*(-2*s15 - s25 + 2*ss + 2*tt))))/(2.*s25*(s15 + s25 - s35)*ss*(-1&
   + sW2)*sW2))

  m33 = -((e**6*me1*s3n*(-1 + 4*sW2)*(2*me2**2 + mm2*(-s25 + s35 - 2*ss) + me2*(6*mm2 + 2*s15 -&
   s25 - s35 - 2*tt) + (s25 - s35)*(-s15 + s35 + tt)))/(s15*s25*(s15 + s25 - ss)*(-1 +&
   sW2)*sW2)) - (e**6*me1*s2n*(-1 + 4*sW2)*(3*me2*(s15 + s25 - ss) + mm2*(s15 + 3*s25 -&
   ss) + (-s15 - s25 + ss)*(-s15 + s25 - s35 + ss + tt)))/(s15*s25*(s15 + s25 - ss)*(-1&
   + sW2)*sW2) + (e**6*me1*s4n*(-1 + 4*sW2)*(2*me2**2 + 2*mm2*s15 + 2*s15**2 +&
   3*mm2*s25 + 3*s15*s25 + s25**2 - mm2*s35 - 3*s15*s35 - 2*s25*s35 + s35**2 + 3*ss**2&
   - 2*s15*tt - 3*s25*tt + s35*tt + ss*(-5*s15 - 4*s25 + s35 + 2*tt) - me2*(2*mm2 -&
   8*s15 - 9*s25 + 3*s35 + 8*ss + 2*tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   s1m*((2*e**6*mm2*s2n*(1 - 4*sW2)**2)/(s15*s25*(-1 + sW2)*sW2) +&
   (e**6*s4n*(-(mm2*(s15*(1 - 4*sW2 + 8*sW2**2) + 2*s25*(1 - 6*sW2 + 12*sW2**2) -&
   2*ss*(1 - 6*sW2 + 12*sW2**2))) + (-s15 - s25 + ss)*(1 - 4*sW2 + 8*sW2**2)*(s15 - s35&
   - tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*me1*(-1 + 4*sW2)*(2*me2**2&
   + 2*mm2**2 + 2*s15**2 + 2*s15*s25 - 3*s15*s35 - 2*s25*s35 - 2*s15*ss + 3*s35*ss +&
   mm2*(6*s15 + 4*s25 - 2*s35 - 5*ss - 4*tt) + me2*(4*mm2 + 2*s15 - 2*s35 - ss - 4*tt)&
   - 4*s15*tt - 2*s25*tt + 2*s35*tt + 3*ss*tt + 2*tt**2))/(s15*s25*(s15 + s25 - ss)*(-1&
   + sW2)*sW2) + (e**6*s3n*(mm2*(-s25 + ss - 4*s15*sW2 + 4*s25*sW2 - 4*ss*sW2 +&
   8*s15*sW2**2 - 8*s25*sW2**2 + 8*ss*sW2**2) + 4*(-s15 - s25 + ss)*sW2*(-1 +&
   2*sW2)*(-2*me2 - s15 - s25 + s35 + ss + tt)))/(s15*s25*(s15 + s25 - ss)*(-1 +&
   sW2)*sW2)) - (e**6*snm*(2*me2**3*(1 - 4*sW2)**2 - mm2**2*(-s15 - s25 + ss)*(1 -&
   4*sW2)**2 + me2**2*(1 - 4*sW2)**2*(4*mm2 + 2*s15 - 2*s35 - ss - 4*tt) + (-s15 - s25&
   + ss)*(s15**2*(-1 + 4*sW2 - 8*sW2**2) + s15*s35*(1 - 4*sW2 + 8*sW2**2) + 2*s15*(1 -&
   6*sW2 + 12*sW2**2)*tt + ss*(s35*(-1 + 4*sW2 - 8*sW2**2) + s15*(1 - 4*sW2 + 8*sW2**2)&
   - (1 - 4*sW2)**2*tt) - tt*(s35*(1 - 4*sW2)**2 + s25*(4*sW2 - 8*sW2**2) + (1 -&
   4*sW2)**2*tt)) + me2*(3*s15**2 + 3*s15*s25 - 4*s15*s35 - 3*s25*s35 + 2*mm2**2*(1 -&
   4*sW2)**2 - 12*s15**2*sW2 - 8*s15*s25*sW2 + 4*s25**2*sW2 + 16*s15*s35*sW2 +&
   8*s25*s35*sW2 + 24*s15**2*sW2**2 + 16*s15*s25*sW2**2 - 8*s25**2*sW2**2 -&
   32*s15*s35*sW2**2 - 16*s25*s35*sW2**2 - 3*s15*ss*(1 - 4*sW2 + 8*sW2**2) - 5*s15*tt -&
   3*s25*tt + 2*s35*tt + 40*s15*sW2*tt + 24*s25*sW2*tt - 16*s35*sW2*tt -&
   80*s15*sW2**2*tt - 48*s25*sW2**2*tt + 32*s35*sW2**2*tt + 2*tt**2 - 16*sW2*tt**2 +&
   32*sW2**2*tt**2 - mm2*(1 - 4*sW2)**2*(-3*s15 - s25 + 2*s35 + 2*ss + 4*tt) +&
   4*ss*(s25*sW2*(-1 + 2*sW2) + s35*(1 - 4*sW2 + 8*sW2**2) + (1 - 4*sW2)**2*tt)) -&
   mm2*(-2*s15**2*(1 - 6*sW2 + 12*sW2**2) - s15*(1 - 4*sW2)**2*(3*s25 - s35 - 2*tt) +&
   s25*(s35*(1 - 4*sW2)**2 + s25*(4*sW2 - 8*sW2**2) + 2*(1 - 4*sW2)**2*tt) - ss*(s35*(1&
   - 4*sW2)**2 + s25*(4*sW2 - 8*sW2**2) - 2*s15*(1 - 6*sW2 + 12*sW2**2) + 2*(1 -&
   4*sW2)**2*tt))))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) + s3m*((e**6*s2n*(me2**2&
   + mm2*(-s15 + ss + 12*s15*sW2 + 4*s25*sW2 - 12*ss*sW2 - 24*s15*sW2**2 - 8*s25*sW2**2&
   + 24*ss*sW2**2) + me2*(mm2 - s25 - tt) - 4*(-s15 - s25 + ss)*sW2*(-1 +&
   2*sW2)*tt))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) - (e**6*me1*(-1 +&
   4*sW2)*(4*me2**2 + 2*mm2**2 + 3*s15**2 + s15*s25 - 2*s15*s35 - 5*s15*ss - 2*s25*ss +&
   2*s35*ss + 2*ss**2 + me2*(2*mm2 + 9*s15 + 3*s25 - 3*s35 - 6*ss - 6*tt) + mm2*(5*s15&
   + s25 - s35 - 4*ss - 4*tt) - 5*s15*tt - s25*tt + s35*tt + 4*ss*tt +&
   2*tt**2))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) + (e**6*s3n*(-me2**2 -&
   ss*(mm2*(1 - 4*sW2)**2 + 4*(-s15 - s25 + ss)*sW2*(-1 + 2*sW2)) + me2*(s35 - s15*(1 -&
   4*sW2)**2 + 8*s25*sW2 - 8*ss*sW2 - 16*s25*sW2**2 + 16*ss*sW2**2 + mm2*(1 - 16*sW2 +&
   32*sW2**2) + tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) - (e**6*s4n*(mm2*ss*(1&
   - 4*sW2)**2 + me2**2*(5 - 16*sW2 + 32*sW2**2) + me2*(mm2 + 3*s15 + s25 - 2*s35 -&
   12*s15*sW2 - 4*s25*sW2 + 8*s35*sW2 + 24*s15*sW2**2 + 8*s25*sW2**2 - 16*s35*sW2**2 -&
   2*ss*(1 - 4*sW2 + 8*sW2**2) - 3*tt + 16*sW2*tt - 32*sW2**2*tt)))/(s15*s25*(s15 + s25&
   - ss)*(-1 + sW2)*sW2)) + s4m*((e**6*s4n*(-me2**2 + ss*(-(mm2*(1 - 4*sW2)**2) - (-s15&
   - s25 + ss)*(1 - 4*sW2 + 8*sW2**2)) + me2*(-3*s15 - 3*s25 + s35 + 8*s15*sW2 +&
   8*s25*sW2 - 16*s15*sW2**2 - 16*s25*sW2**2 + ss*(3 - 8*sW2 + 16*sW2**2) + mm2*(1 -&
   16*sW2 + 32*sW2**2) + tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   (e**6*me1*(-1 + 4*sW2)*(-2*mm2**2 - s15**2 - s15*s25 + s15*s35 - s25*ss - s35*ss +&
   ss**2 + 3*s15*tt + s25*tt - s35*tt - 2*ss*tt - 2*tt**2 + me2*(2*mm2 + s15 + 3*s25 -&
   s35 - 2*ss + 2*tt) + mm2*(-3*s15 - s25 + s35 + 2*ss + 4*tt)))/(s15*s25*(s15 + s25 -&
   ss)*(-1 + sW2)*sW2) + (e**6*s3n*(-(mm2*ss*(1 - 4*sW2)**2) + me2**2*(3 - 16*sW2 +&
   32*sW2**2) + me2*(s15 - ss*(1 - 4*sW2)**2 - 12*s15*sW2 - 4*s25*sW2 + 8*s35*sW2 +&
   24*s15*sW2**2 + 8*s25*sW2**2 - 16*s35*sW2**2 + mm2*(3 - 32*sW2 + 64*sW2**2) - tt +&
   16*sW2*tt - 32*sW2**2*tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2) +&
   (e**6*s2n*(me2**2 + s15**2 + mm2*s25 + s15*s25 + 4*mm2*s15*sW2 - 4*s15**2*sW2 -&
   4*mm2*s25*sW2 - 4*s15*s25*sW2 - 8*mm2*s15*sW2**2 + 8*s15**2*sW2**2 +&
   8*mm2*s25*sW2**2 + 8*s15*s25*sW2**2 + ss**2*(1 - 4*sW2 + 8*sW2**2) + me2*(mm2 +&
   3*s15 + 3*s25 - 8*s15*sW2 - 8*s25*sW2 + 16*s15*sW2**2 + 16*s25*sW2**2 + ss*(-3 +&
   8*sW2 - 16*sW2**2) - tt) - s15*tt - s25*tt + 4*s15*sW2*tt + 4*s25*sW2*tt -&
   8*s15*sW2**2*tt - 8*s25*sW2**2*tt + ss*(-4*mm2*sW2 + 8*mm2*sW2**2 + s25*(-1 + 4*sW2&
   - 8*sW2**2) - 2*s15*(1 - 4*sW2 + 8*sW2**2) + tt - 4*sW2*tt +&
   8*sW2**2*tt)))/(s15*s25*(s15 + s25 - ss)*(-1 + sW2)*sW2)) + (e**6*(2*s15**3 +&
   3*s15**2*s25 + s15*s25**2 - 3*s15**2*s35 - 2*s15*s25*s35 + s15*s35**2 - 6*s15**2*ss&
   - 7*s15*s25*ss - 2*s25**2*ss + 5*s15*s35*ss + 3*s25*s35*ss - s35**2*ss + 6*s15*ss**2&
   + 4*s25*ss**2 - 2*s35*ss**2 - 2*ss**3 + 4*me2**3*(1 - 4*sW2)**2 - 2*mm2**2*(-s15 -&
   s25 + ss)*(1 - 4*sW2)**2 - 12*s15**3*sW2 - 16*s15**2*s25*sW2 - 4*s15*s25**2*sW2 +&
   20*s15**2*s35*sW2 + 12*s15*s25*s35*sW2 - 8*s15*s35**2*sW2 + 28*s15**2*ss*sW2 +&
   28*s15*s25*ss*sW2 + 8*s25**2*ss*sW2 - 28*s15*s35*ss*sW2 - 12*s25*s35*ss*sW2 +&
   8*s35**2*ss*sW2 - 24*s15*ss**2*sW2 - 16*s25*ss**2*sW2 + 8*s35*ss**2*sW2 +&
   8*ss**3*sW2 + 24*s15**3*sW2**2 + 32*s15**2*s25*sW2**2 + 8*s15*s25**2*sW2**2 -&
   40*s15**2*s35*sW2**2 - 24*s15*s25*s35*sW2**2 + 16*s15*s35**2*sW2**2 -&
   56*s15**2*ss*sW2**2 - 56*s15*s25*ss*sW2**2 - 16*s25**2*ss*sW2**2 +&
   56*s15*s35*ss*sW2**2 + 24*s25*s35*ss*sW2**2 - 16*s35**2*ss*sW2**2 +&
   48*s15*ss**2*sW2**2 + 32*s25*ss**2*sW2**2 - 16*s35*ss**2*sW2**2 - 16*ss**3*sW2**2 -&
   4*s15**2*tt - 5*s15*s25*tt - s25**2*tt + 3*s15*s35*tt + s25*s35*tt + 8*s15*ss*tt +&
   6*s25*ss*tt - 2*s35*ss*tt - 4*ss**2*tt + 28*s15**2*sW2*tt + 32*s15*s25*sW2*tt +&
   4*s25**2*sW2*tt - 24*s15*s35*sW2*tt - 8*s25*s35*sW2*tt - 40*s15*ss*sW2*tt -&
   24*s25*ss*sW2*tt + 16*s35*ss*sW2*tt + 16*ss**2*sW2*tt - 56*s15**2*sW2**2*tt -&
   64*s15*s25*sW2**2*tt - 8*s25**2*sW2**2*tt + 48*s15*s35*sW2**2*tt +&
   16*s25*s35*sW2**2*tt + 80*s15*ss*sW2**2*tt + 48*s25*ss*sW2**2*tt -&
   32*s35*ss*sW2**2*tt - 32*ss**2*sW2**2*tt + 2*s15*tt**2 + 2*s25*tt**2 - 2*ss*tt**2 -&
   16*s15*sW2*tt**2 - 16*s25*sW2*tt**2 + 16*ss*sW2*tt**2 + 32*s15*sW2**2*tt**2 +&
   32*s25*sW2**2*tt**2 - 32*ss*sW2**2*tt**2 + 2*me2**2*(2*s25 + 4*mm2*(1 - 4*sW2)**2 +&
   ss*(-3 + 8*sW2 - 16*sW2**2) + 4*s15*(1 - 4*sW2 + 8*sW2**2) - 2*(1 - 4*sW2)**2*(s35 +&
   2*tt)) + me2*(10*s15**2 + 13*s15*s25 + 3*s25**2 - 9*s15*s35 - 5*s25*s35 +&
   4*mm2**2*(1 - 4*sW2)**2 - 44*s15**2*sW2 - 48*s15*s25*sW2 - 4*s25**2*sW2 +&
   40*s15*s35*sW2 + 8*s25*s35*sW2 + 88*s15**2*sW2**2 + 96*s15*s25*sW2**2 +&
   8*s25**2*sW2**2 - 80*s15*s35*sW2**2 - 16*s25*s35*sW2**2 + 2*ss**2*(3 - 8*sW2 +&
   16*sW2**2) - 14*s15*tt - 10*s25*tt + 4*s35*tt + 80*s15*sW2*tt + 48*s25*sW2*tt -&
   32*s35*sW2*tt - 160*s15*sW2**2*tt - 96*s25*sW2**2*tt + 64*s35*sW2**2*tt + 4*tt**2 -&
   32*sW2*tt**2 + 64*sW2**2*tt**2 - 2*mm2*(-3*s25 + 2*s35 + 8*s25*sW2 - 16*s35*sW2 -&
   16*s25*sW2**2 + 32*s35*sW2**2 + s15*(-5 + 24*sW2 - 48*sW2**2) + 4*ss*(1 - 4*sW2 +&
   8*sW2**2) + 4*tt - 32*sW2*tt + 64*sW2**2*tt) - 2*ss*(-4*s35*(1 - 4*sW2 + 8*sW2**2) +&
   4*s15*(2 - 7*sW2 + 14*sW2**2) + s25*(5 - 12*sW2 + 24*sW2**2) - 2*(3 - 16*sW2 +&
   32*sW2**2)*tt)) + mm2*(2*ss**2 + s25**2*(1 - 4*sW2 + 8*sW2**2) + 4*s15**2*(1 - 7*sW2&
   + 14*sW2**2) + s15*s25*(5 - 32*sW2 + 64*sW2**2) - s25*(1 - 4*sW2)**2*(s35 + 4*tt) -&
   s15*(1 - 4*sW2)**2*(3*s35 + 4*tt) - 2*ss*(3*s15*(1 - 4*sW2 + 8*sW2**2) + s25*(2 -&
   4*sW2 + 8*sW2**2) - (1 - 4*sW2)**2*(s35 + 2*tt)))))/(s15*s25*(s15 + s25 - ss)*(-1 +&
   sW2)*sW2)

  m35 = (-2*e**6*me1*mm2*s2n*(-1 + 4*sW2))/(s25*(-s15 - s25 + ss)*(-1 + sW2)*sW2) -&
   (e**6*me1*s4n*(-1 + 4*sW2)*(2*me2**2 + me2*(-2*mm2 + 4*s15 + 5*s25 - 4*ss - 2*tt) -&
   s25*(mm2 - 2*s35 + tt)))/(s25**2*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   (e**6*me1*s3n*(-1 + 4*sW2)*(2*me2**2 + me2*(6*mm2 + 2*s15 - s25 - 2*ss - 2*tt) +&
   s25*(mm2 - s15 + ss + tt)))/(s25**2*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   (2*e**6*snm*(me1 - 4*me1*sW2)**2*(me2**2 + mm2**2 + me2*(2*mm2 - s25 - 2*tt) +&
   tt*(-s15 + ss + tt) - mm2*(s25 + 2*tt)))/(s25**2*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   s3m*((e**6*me2*s3n*(me2 + s15 - ss - 8*s15*sW2 - 8*s25*sW2 + 8*ss*sW2 +&
   16*s15*sW2**2 + 16*s25*sW2**2 - 16*ss*sW2**2 + mm2*(-1 + 16*sW2 - 32*sW2**2) -&
   tt))/(s25**2*(-s15 - s25 + ss)*(-1 + sW2)*sW2) + (e**6*me2*s4n*(mm2 + 2*s15 - 2*ss -&
   8*s15*sW2 + 8*ss*sW2 + 16*s15*sW2**2 - 16*ss*sW2**2 + me2*(5 - 16*sW2 + 32*sW2**2) -&
   3*tt + 16*sW2*tt - 32*sW2**2*tt))/(s25**2*(-s15 - s25 + ss)*(-1 + sW2)*sW2) +&
   (2*e**6*me1*(-1 + 4*sW2)*(2*me2**2 + mm2**2 + me2*(mm2 + 4*s15 + s25 - 4*ss - 3*tt)&
   + mm2*(s15 - ss - 2*tt) + (-s15 + ss + tt)**2))/(s25**2*(-s15 - s25 + ss)*(-1 +&
   sW2)*sW2)) + s1m*(-((e**6*me2*s4n*(me2 + mm2 + 2*s15 + 2*s25 - 2*ss - 8*s15*sW2 -&
   8*s25*sW2 + 8*ss*sW2 + 16*s15*sW2**2 + 16*s25*sW2**2 - 16*ss*sW2**2 -&
   tt))/(s25**2*(-s15 - s25 + ss)*(-1 + sW2)*sW2)) - (e**6*me2*s3n*(me2 + mm2 + s15 -&
   ss - 8*s15*sW2 - 8*s25*sW2 + 8*ss*sW2 + 16*s15*sW2**2 + 16*s25*sW2**2 - 16*ss*sW2**2&
   - tt))/(s25**2*(-s15 - s25 + ss)*(-1 + sW2)*sW2) - (2*e**6*me1*(-1 + 4*sW2)*(me2**2&
   + mm2**2 + me2*(2*mm2 + 2*s15 + s25 - 2*ss - 2*tt) + mm2*(s15 - ss - 2*tt) + (-s15 +&
   ss + tt)*(-s15 - s25 + ss + tt)))/(s25**2*(-s15 - s25 + ss)*(-1 + sW2)*sW2)) +&
   s4m*((e**6*me2*s4n*(me2 + 2*s15 + 2*s25 - 2*ss - 8*s15*sW2 - 8*s25*sW2 + 8*ss*sW2 +&
   16*s15*sW2**2 + 16*s25*sW2**2 - 16*ss*sW2**2 + mm2*(-1 + 16*sW2 - 32*sW2**2) -&
   tt))/(s25**2*(-s15 - s25 + ss)*(-1 + sW2)*sW2) + (e**6*me2*s3n*(-s15 + ss +&
   8*s15*sW2 - 8*ss*sW2 - 16*s15*sW2**2 + 16*ss*sW2**2 + mm2*(-3 + 32*sW2 - 64*sW2**2)&
   + me2*(-3 + 16*sW2 - 32*sW2**2) + tt - 16*sW2*tt + 32*sW2**2*tt))/(s25**2*(-s15 -&
   s25 + ss)*(-1 + sW2)*sW2) + (2*e**6*me1*(-1 + 4*sW2)*(mm2**2 + mm2*(s15 + s25 - ss -&
   2*tt) - me2*(mm2 - s15 + ss + tt) + (-s15 + ss + tt)*(-s15 - s25 + ss +&
   tt)))/(s25**2*(-s15 - s25 + ss)*(-1 + sW2)*sW2)) - (2*e**6*(2*me2**3*(1 - 4*sW2)**2&
   + 2*me2**2*(s15 - ss + 2*mm2*(1 - 4*sW2)**2 + 8*s25*sW2 - 16*s25*sW2**2 - 2*tt +&
   16*sW2*tt - 32*sW2**2*tt) - s25*(-(s15*s35) + 4*s15*s35*sW2 - 8*s15*s35*sW2**2 +&
   s35*ss*(1 - 4*sW2 + 8*sW2**2) + mm2*(-(s35*(1 - 4*sW2)**2) + 4*s25*sW2*(-1 + 2*sW2)&
   + s15*(1 - 12*sW2 + 24*sW2**2)) + s35*tt + 4*s15*sW2*tt + 4*s25*sW2*tt -&
   8*s35*sW2*tt - 8*s15*sW2**2*tt - 8*s25*sW2**2*tt + 16*s35*sW2**2*tt) + me2*(2*s15**2&
   + 3*s15*s25 + s25**2 + s25*s35 + 2*mm2**2*(1 - 4*sW2)**2 - 8*s15**2*sW2 -&
   12*s15*s25*sW2 - 4*s25**2*sW2 - 8*s25*s35*sW2 + 16*s15**2*sW2**2 + 24*s15*s25*sW2**2&
   + 8*s25**2*sW2**2 + 16*s25*s35*sW2**2 + 2*ss**2*(1 - 4*sW2 + 8*sW2**2) - 4*s15*tt -&
   2*s25*tt + 16*s15*sW2*tt - 32*s15*sW2**2*tt + 2*tt**2 - 16*sW2*tt**2 +&
   32*sW2**2*tt**2 - 2*mm2*(-s15 + ss + 8*s25*sW2*(-1 + 2*sW2) + 2*(1 - 4*sW2)**2*tt) +&
   ss*(s25*(-3 + 16*sW2 - 32*sW2**2) - 4*s15*(1 - 4*sW2 + 8*sW2**2) + 4*(1 - 4*sW2 +&
   8*sW2**2)*tt))))/(s25**2*(-s15 - s25 + ss)*(-1 + sW2)*sW2)

  pepezmmgx = (m2 + m5 + m7 + m9 + m12 + m14 + m16 + m18 + m20 + m23 + m25 + m27 + m29 &
              + m31 + m33 + m35)
  pepezmmgx = 1/(2*mZ**2)*pepezmmgx

  END FUNCTION

  FUNCTION EMZEMGX(p1,p2,p3,p4,p5)
    !! e-(p1) mu+(p2) -> Z -> e-(p3) mu+(p4)
    !! for massive electrons, expanded
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: emzemgx
  real (kind=prec) :: pol0(4)
#include "charge_config.h"

  pol0 = (/0._prec, 0._prec, 0._prec, 0._prec/)
  if(Qmu == +1) then
    emzemgx = pepezmmgx(p1,pol0,-p3,pol0,-p2,p4,p5)
  elseif(Qmu == -1) then
    emzemgx = pepezmmgx(p1,pol0,-p3,pol0,p4,-p2,p5)
  endif

  END FUNCTION



                          !!!!!!!!!!!!!!!!!!!!!!
                          END MODULE mue_PEPE2MM
                          !!!!!!!!!!!!!!!!!!!!!!
