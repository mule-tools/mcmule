
                                !!!!!!!!!!!!!!!!!!!!!!!
                                 MODULE mue_mp2mp_mono
                                !!!!!!!!!!!!!!!!!!!!!!!
  use functions
  use collier
  implicit none

  real (kind=prec)    :: me2,mm2,ss,tt,uu,s15,s25,s35
  real (kind=prec)    :: F1p,F2p,Ge,Gm,tau
  real (kind=prec)    :: F1r,F2r,Ger,Gmr,taur
  real (kind=prec)    :: logF2L2,logF2P2,logF2N2,logP2L2,logN2L2,logmuF2,logmuL2,logmuP2,logmuN2
  complex (kind=prec) :: logF2t_c,logP2t_c,logmut_c,logbeL2_c,logbeN2_c
  real (kind=prec)    :: logF2t,logP2t,logmut,logbeL2,logbeN2,log4
  complex (kind=prec) :: DB0tNN_c,DB0tLL_c,DB0sFP_c,DB0tLN_c,DB0P2LP_c,DB0F2FL_c,DB0F2FN_c
  real (kind=prec)    :: DB0tNN,DB0tLL,DB0sFP,DB0tLN,DB0P2LP,DB0F2FL,DB0F2FN,DB0sFPlogmut
  complex (kind=prec) :: C0IR6sFP_c
  real (kind=prec)    :: C0IR6sFP
  complex (kind=prec) :: C0tF2F20F0_c,C0tF2F2NF0_c,C0tF2F2NFN_c,C0tF2F2LF0_c,C0tF2F2NFL_c,C0tF2F2LFL_c
  complex (kind=prec) :: C0sF2P2FNP_c,C0sF2P2FLP_c
  complex (kind=prec) :: C0tP2P20P0_c,C0tP2P2NP0_c,C0tP2P2NPN_c,C0tP2P2LP0_c,C0tP2P2NPL_c,C0tP2P2LPL_c
  real (kind=prec)    :: C0tF2F20F0,C0tF2F2NF0,C0tF2F2NFN,C0tF2F2LF0,C0tF2F2NFL,C0tF2F2LFL
  real (kind=prec)    :: C0sF2P2FNP,C0sF2P2FLP
  real (kind=prec)    :: C0tP2P20P0,C0tP2P2NP0,C0tP2P2NPN,C0tP2P2LP0,C0tP2P2NPL,C0tP2P2LPL
  complex (kind=prec) :: D0IR6tsN_c,D0IR6tsL_c
  real (kind=prec)    :: D0IR6tsN,D0IR6tsL
  complex (kind=prec) :: D0tsNN_c,D0tsNL_c,D0tsLN_c,D0tsLL_c
  real (kind=prec)    :: D0tsNN,D0tsNL,D0tsLN,D0tsLL

  contains

  FUNCTION MP2MPL_MP_NUCM(p1, p2, p3, p4, pole)
   !! m-(p1) p(p2) -> mu-(p3) p(p4)
   !! for massive muons
    implicit none
    real (kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
    real(kind=prec), optional :: pole
    real (kind=prec) :: mp2mpl_mp_nucm
    real (kind=prec) :: mp2mpl_mp_s, mp2mpl_mp_u, pole_s, pole_u
#include "charge_config.h"

    ss = sq(p1+p2); tt = sq(p2-p4); uu = sq(p1-p4)
    me2 = sq(p1); mm2 = sq(p2)
    tau = -tt/4/mm2

    Ge = sachs_gel1(-tt); Gm = sachs_gmag1(-tt)
    F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

    call initnuc(0)
    mp2mpl_mp_s = &
             mp2mpl_mp_fin_d10d20(me2,mm2,ss,tt) &
           + mp2mpl_mp_fin_d11d20(me2,mm2,ss,tt) &
           + mp2mpl_mp_fin_d12d20(me2,mm2,ss,tt) &
           + mp2mpl_mp_fin_d13d20(me2,mm2,ss,tt) &
           + mp2mpl_mp_fin_d10d21(me2,mm2,ss,tt) &
           + mp2mpl_mp_fin_d11d21(me2,mm2,ss,tt) &
           + mp2mpl_mp_fin_d12d21(me2,mm2,ss,tt) &
           + mp2mpl_mp_fin_d10d22(me2,mm2,ss,tt) &
           + mp2mpl_mp_fin_d11d22(me2,mm2,ss,tt) &
           + mp2mpl_mp_fin_d10d23(me2,mm2,ss,tt)

    call initnuc(1)
    mp2mpl_mp_u = &
             mp2mpl_mp_fin_d10d20(me2,mm2,uu,tt) &
           + mp2mpl_mp_fin_d11d20(me2,mm2,uu,tt) &
           + mp2mpl_mp_fin_d12d20(me2,mm2,uu,tt) &
           + mp2mpl_mp_fin_d13d20(me2,mm2,uu,tt) &
           + mp2mpl_mp_fin_d10d21(me2,mm2,uu,tt) &
           + mp2mpl_mp_fin_d11d21(me2,mm2,uu,tt) &
           + mp2mpl_mp_fin_d12d21(me2,mm2,uu,tt) &
           + mp2mpl_mp_fin_d10d22(me2,mm2,uu,tt) &
           + mp2mpl_mp_fin_d11d22(me2,mm2,uu,tt) &
           + mp2mpl_mp_fin_d10d23(me2,mm2,uu,tt)

    mp2mpl_mp_nucm = alpha**3 * Qmu**3*Qp**3 * 2d0 * (mp2mpl_mp_s - mp2mpl_mp_u)

    if(present(pole)) then
       call initnuc(0)
       pole_s = &
             mp2mpl_mp_sin_d10d20(me2,mm2,ss,tt) &
           + mp2mpl_mp_sin_d11d20(me2,mm2,ss,tt) &
           + mp2mpl_mp_sin_d12d20(me2,mm2,ss,tt) &
           + mp2mpl_mp_sin_d13d20(me2,mm2,ss,tt) &
           + mp2mpl_mp_sin_d10d21(me2,mm2,ss,tt) &
           + mp2mpl_mp_sin_d11d21(me2,mm2,ss,tt) &
           + mp2mpl_mp_sin_d12d21(me2,mm2,ss,tt) &
           + mp2mpl_mp_sin_d10d22(me2,mm2,ss,tt) &
           + mp2mpl_mp_sin_d11d22(me2,mm2,ss,tt)
       call initnuc(1)
       pole_u = &
             mp2mpl_mp_sin_d10d20(me2,mm2,uu,tt) &
           + mp2mpl_mp_sin_d11d20(me2,mm2,uu,tt) &
           + mp2mpl_mp_sin_d12d20(me2,mm2,uu,tt) &
           + mp2mpl_mp_sin_d13d20(me2,mm2,uu,tt) &
           + mp2mpl_mp_sin_d10d21(me2,mm2,uu,tt) &
           + mp2mpl_mp_sin_d11d21(me2,mm2,uu,tt) &
           + mp2mpl_mp_sin_d12d21(me2,mm2,uu,tt) &
           + mp2mpl_mp_sin_d10d22(me2,mm2,uu,tt) &
           + mp2mpl_mp_sin_d11d22(me2,mm2,uu,tt)
         pole = alpha**3 * Qmu**3*Qp**3 * 2d0 * (pole_s - pole_u)
    endif

  END FUNCTION MP2MPL_MP_NUCM


  FUNCTION MYLOG(x,branchcut)
    complex (kind=prec)::mylog
    real (kind=prec)::x
    integer::branchcut

    mylog=log(abs(x))
    if(x<0) mylog=mylog+branchcut*imag*pi
  END FUNCTION

  SUBROUTINE INITNUC(x)
    implicit none
    integer :: x
    real (kind=prec) :: me, mm, mn, mn2, la, la2

    me=sqrt(me2)
    mm=sqrt(mm2)
    mn2=4*mm2
    mn=sqrt(mn2)
    la2=lambda
    la=sqrt(la2)

    !! LOGS!!
    log4=log(4d0)
    logF2L2=log(me2/la2)
    logF2P2=log(me2/mm2)
    logF2N2=log(me2/mn2)
    logP2L2=log(mm2/la2)
    logN2L2=log(mn2/la2)
    logmuF2=log(musq/me2)
    logmuL2=log(musq/la2)
    logmuP2=log(musq/mm2)
    logmuN2=log(musq/mn2)

    logF2t_c=mylog(-me2/tt,1)
    logP2t_c=mylog(-mm2/tt,1)
    logmut_c=mylog(-musq/tt,1)
    logbeL2_c=mylog(la2/(la2-tt),1)
    logbeN2_c=mylog(mn2/(mn2-tt),1)
    logF2t=real(logF2t_c)
    logP2t=real(logP2t_c)
    logmut=real(logmut_c)
    logbeL2=real(logbeL2_c)
    logbeN2=real(logbeN2_c)

    !write(*,*) "LOG :: ", logF2N2,logF2P2,logmuF2,logmuN2,logmuP2,logbeN2,logF2t,logP2t, &
    !                      logmut,logF2L2,logP2L2,logN2L2,logmuL2,logbeL2

    !! DISCB!!
    DB0tNN_c=discb_coll(tt,mn,mn)
    DB0tLL_c=discb_coll(tt,la,la)
    if(x.eq.0) DB0sFP_c=discb_coll(ss,me,mm)
    if(x.eq.1) DB0sFP_c=discb_coll(uu,me,mm)
    DB0tLN_c=discb_coll(tt,la,mn)
    DB0P2LP_c=discb_coll(mm2,la,mm)
    DB0F2FL_c=discb_coll(me2,la,me)
    DB0F2FN_c=discb_coll(me2,me,mn)
    DB0tNN=real(DB0tNN_c)
    DB0tLL=real(DB0tLL_c)
    DB0sFP=real(DB0sFP_c)
    DB0tLN=real(DB0tLN_c)
    DB0P2LP=real(DB0P2LP_c)
    DB0F2FL=real(DB0F2FL_c)
    DB0F2FN=real(DB0F2FN_c)
    DB0sFPlogmut=real(DB0sFP_c*logmut_c)

    !write(*,*) "B0 :: ", DB0tNN,DB0tLL,DB0tLN,DB0P2LP,DB0F2FL,DB0F2FN,DB0sFP,DB0sFPlogmut

    !! SCALARC0IR6!!
    if(x.eq.0) C0IR6sFP_c=scalarc0ir6_coll(ss,me,mm)
    if(x.eq.1) C0IR6sFP_c=scalarc0ir6_coll(uu,me,mm)
    C0IR6sFP=real(C0IR6sFP_c)

    !write(*,*) "C0IR6 :: ", C0IR6sFP

    !! SCALARC0!!
    C0tF2F20F0_c=scalarc0_coll(me2,me2,tt,0d0,me,0d0)
    C0tF2F2NF0_c=scalarc0_coll(me2,me2,tt,mn,me,0d0)
    C0tF2F2NFN_c=scalarc0_coll(me2,me2,tt,mn,me,mn)
    C0tF2F2LF0_c=scalarc0_coll(me2,me2,tt,la,me,0d0)
    C0tF2F2NFL_c=scalarc0_coll(me2,me2,tt,mn,me,la)
    C0tF2F2LFL_c=scalarc0_coll(me2,me2,tt,la,me,la)
    if(x.eq.0) C0sF2P2FNP_c=scalarc0_coll(me2,mm2,ss,me,mn,mm)
    if(x.eq.1) C0sF2P2FNP_c=scalarc0_coll(me2,mm2,uu,me,mn,mm)
    if(x.eq.0) C0sF2P2FLP_c=scalarc0_coll(me2,mm2,ss,me,la,mm)
    if(x.eq.1) C0sF2P2FLP_c=scalarc0_coll(me2,mm2,uu,me,la,mm)
    C0tP2P20P0_c=scalarc0_coll(mm2,mm2,tt,0d0,mm,0d0)
    C0tP2P2NP0_c=scalarc0_coll(mm2,mm2,tt,mn,mm,0d0)
    C0tP2P2NPN_c=scalarc0_coll(mm2,mm2,tt,mn,mm,mn)
    C0tP2P2LP0_c=scalarc0_coll(mm2,mm2,tt,la,mm,0d0)
    C0tP2P2NPL_c=scalarc0_coll(mm2,mm2,tt,mn,mm,la)
    C0tP2P2LPL_c=scalarc0_coll(mm2,mm2,tt,la,mm,la)
    C0tF2F20F0=real(C0tF2F20F0_c)
    C0tF2F2NF0=real(C0tF2F2NF0_c)
    C0tF2F2NFN=real(C0tF2F2NFN_c)
    C0tF2F2LF0=real(C0tF2F2LF0_c)
    C0tF2F2NFL=real(C0tF2F2NFL_c)
    C0tF2F2LFL=real(C0tF2F2LFL_c)
    C0sF2P2FNP=real(C0sF2P2FNP_c)
    C0sF2P2FLP=real(C0sF2P2FLP_c)
    C0tP2P20P0=real(C0tP2P20P0_c)
    C0tP2P2NP0=real(C0tP2P2NP0_c)
    C0tP2P2NPN=real(C0tP2P2NPN_c)
    C0tP2P2LP0=real(C0tP2P2LP0_c)
    C0tP2P2NPL=real(C0tP2P2NPL_c)
    C0tP2P2LPL=real(C0tP2P2LPL_c)

    !write(*,*) "C0 :: ",  C0tF2F20F0,C0tF2F2NF0,C0tF2F2NFN,C0tF2F2LF0,C0tF2F2NFL,C0tF2F2LFL, &
    !                      C0sF2P2FNP,C0sF2P2FLP, &
    !                      C0tP2P20P0,C0tP2P2NP0,C0tP2P2NPN,C0tP2P2LP0,C0tP2P2NPL,C0tP2P2LPL

    !! SCALARD0IR16!!
    if(x.eq.0) D0IR6tsN_c=scalard0ir16m3_coll(ss,tt,me,mn,mm)
    if(x.eq.1) D0IR6tsN_c=scalard0ir16m3_coll(uu,tt,me,mn,mm)
    if(x.eq.0) D0IR6tsL_c=scalard0ir16m3_coll(ss,tt,me,la,mm)
    if(x.eq.1) D0IR6tsL_c=scalard0ir16m3_coll(uu,tt,me,la,mm)
    D0IR6tsN=real(D0IR6tsN_c)
    D0IR6tsL=real(D0IR6tsL_c)

    !write(*,*) "D0IR16 :: ", D0IR6tsN,D0IR6tsL

    !! SCALARD0!!
    if(x.eq.0) D0tsNN_c=scalard0_coll(ss,tt,me,mn,mn,mm)
    if(x.eq.1) D0tsNN_c=scalard0_coll(uu,tt,me,mn,mn,mm)
    if(x.eq.0) D0tsNL_c=scalard0_coll(ss,tt,me,mn,la,mm)
    if(x.eq.1) D0tsNL_c=scalard0_coll(uu,tt,me,mn,la,mm)
    if(x.eq.0) D0tsLN_c=scalard0_coll(ss,tt,me,la,mn,mm)
    if(x.eq.1) D0tsLN_c=scalard0_coll(uu,tt,me,la,mn,mm)
    if(x.eq.0) D0tsLL_c=scalard0_coll(ss,tt,me,la,la,mm)
    if(x.eq.1) D0tsLL_c=scalard0_coll(uu,tt,me,la,la,mm)
    D0tsNN=real(D0tsNN_c)
    D0tsNL=real(D0tsNL_c)
    D0tsLN=real(D0tsLN_c)
    D0tsLL=real(D0tsLL_c)

    !write(*,*) "D0 :: ", D0tsNN,D0tsNL,D0tsLN,D0tsLL

  END SUBROUTINE

  FUNCTION MP2MPL_MP_FIN_D10D20(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_FIN_d10d20
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36

  tmp1 = -tt
  tmp2 = 2*me2
  tmp3 = 2*mm2
  tmp4 = -2*ss
  tmp5 = tmp1 + tmp2 + tmp3 + tmp4
  tmp6 = 1/tt
  tmp7 = -ss
  tmp8 = me2 + mm2 + tmp7
  tmp9 = 2*ss
  tmp10 = 1/tmp6 + tmp9
  tmp11 = me2**2
  tmp12 = mm2 + tmp7
  tmp13 = tmp12**2
  tmp14 = mm2 + ss
  tmp15 = -2*me2*tmp14
  tmp16 = tmp11 + tmp13 + tmp15
  tmp17 = 1/tmp16
  tmp18 = 4*mm2
  tmp19 = tmp1 + tmp18
  tmp20 = 1/tmp19
  tmp21 = mm2**2
  tmp22 = ss**2
  tmp23 = 4*tmp22
  tmp24 = tmp9/tmp6
  tmp25 = tmp6**(-2)
  tmp26 = 4*me2
  tmp27 = tmp1 + tmp26
  tmp28 = 1/tmp27
  tmp29 = -4*ss
  tmp30 = 4*tmp21
  tmp31 = -8*mm2*ss
  tmp32 = 8*tmp22
  tmp33 = (4*ss)/tmp6
  tmp34 = 1/tmp25
  tmp35 = ss/tmp6
  tmp36 = tmp25 + tmp35
  MP2MPL_MP_FIN_d10d20 = -8*logmuP2*Pi - 8*logP2t*Pi*tmp20*tmp5 - 8*logF2t*Pi*tmp28*tmp5 + 16*DB0sFP*Pi*t&
                          &mp17*tmp34*(-(tmp11*(tmp25 - 3*tmp35 - mm2/tmp6)) - tmp12*(mm2*(tmp25 - 2*tmp35)&
                          & + ss*tmp36 + tmp21/tmp6) - me2*(-2*mm2*tmp36 - tmp21/tmp6 + (3*tmp22)/tmp6) - m&
                          &e2**3/tmp6) + (8*logF2P2*Pi*(me2 - mm2 + ss)*(me2 + tmp12 + 1/tmp6)*tmp6)/ss + 8&
                          &*logmuF2*Pi*(tmp2 + tmp3 + tmp4 + 1/tmp6)*tmp6 - 8*C0tP2P20P0*Pi*tmp20*((4*tmp11&
                          &)/tmp20 + tmp3*(tmp25 + tmp32 + tmp33) + 8*me2*(2*tmp21 + mm2*tmp29 + tmp35) + t&
                          &mp30*(tmp29 + 1/tmp6) - (tmp23 + tmp24 + tmp25)/tmp6)*tmp6 - 8*C0tF2F20F0*Pi*tmp&
                          &28*(tmp2*(-16*mm2*ss + 8*tmp21 + tmp25 + tmp32 + tmp33) + 4*tmp11*(tmp18 + tmp29&
                          & + 1/tmp6) - (tmp23 + tmp24 + tmp25 + tmp30 + tmp31)/tmp6)*tmp6 - 16*DB0sFPlogmu&
                          &t*Pi*ss*tmp17*(4*tmp11 + 8*me2*tmp12 + tmp23 + tmp24 + tmp25 + tmp30 + tmp31)*tm&
                          &p34*tmp8 - 16*logmut*Pi*tmp6*tmp8 - 16*C0IR6sFP*Pi*tmp10*tmp6*tmp8 - 8*DB0sFP*lo&
                          &gF2P2*Pi*ss*tmp10*tmp17*tmp6*tmp8 - 16*DB0sFP*logmuF2*Pi*ss*tmp10*tmp17*tmp6*tmp&
                          &8

  END FUNCTION MP2MPL_MP_FIN_D10D20

  FUNCTION MP2MPL_MP_FIN_D11D20(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_FIN_d11d20
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206

  tmp1 = sqrt(lambda)**2
  tmp2 = -4*mm2
  tmp3 = tmp1 + tmp2
  tmp4 = 1/tmp3
  tmp5 = -tt
  tmp6 = -f1p
  tmp7 = 2*tmp1
  tmp8 = 3 + tmp6 + tmp7
  tmp9 = 2*me2
  tmp10 = 2*mm2
  tmp11 = -2*ss
  tmp12 = tmp10 + tmp11 + tmp5 + tmp9
  tmp13 = 1/tt
  tmp14 = -ss
  tmp15 = me2 + mm2 + tmp14
  tmp16 = me2**3
  tmp17 = me2**2
  tmp18 = mm2**2
  tmp19 = mm2**3
  tmp20 = ss**2
  tmp21 = ss**3
  tmp22 = -1 + kappa
  tmp23 = sqrt(lambda)**4
  tmp24 = 4*mm2
  tmp25 = mm2 + tmp14
  tmp26 = 4*tmp17
  tmp27 = 8*me2*tmp25
  tmp28 = 4*tmp20
  tmp29 = (2*ss)/tmp13
  tmp30 = tmp13**(-2)
  tmp31 = -tmp1
  tmp32 = tmp24 + tmp31
  tmp33 = 1/tmp32
  tmp34 = 4*tmp18
  tmp35 = -8*mm2*ss
  tmp36 = 3/tmp13
  tmp37 = tmp1/tmp13
  tmp38 = 4*me2
  tmp39 = tmp38 + tmp5
  tmp40 = 1/tmp39
  tmp41 = 1/tmp30
  tmp42 = kappa*tmp31
  tmp43 = tmp24 + tmp42
  tmp44 = 16*mm2*tmp17
  tmp45 = -3*mm2
  tmp46 = ss + 1/tmp13 + tmp45
  tmp47 = tmp30*tmp46
  tmp48 = 16*tmp18
  tmp49 = -16*mm2*ss
  tmp50 = -3*tmp30
  tmp51 = tmp48 + tmp49 + tmp50
  tmp52 = me2*tmp51
  tmp53 = tmp44 + tmp47 + tmp52
  tmp54 = tmp24 + tmp5
  tmp55 = 1/tmp54
  tmp56 = -4*ss
  tmp57 = 8*tmp20
  tmp58 = (4*ss)/tmp13
  tmp59 = kappa*tmp1
  tmp60 = tmp2 + tmp59
  tmp61 = 1/tmp13 + tmp2
  tmp62 = ss*tmp7
  tmp63 = -tmp37
  tmp64 = 8*tmp18
  tmp65 = 8*ss
  tmp66 = tmp1 + tmp65
  tmp67 = 1/me2
  tmp68 = tmp25**2
  tmp69 = mm2 + ss
  tmp70 = (-2*tmp69)/tmp67
  tmp71 = tmp17 + tmp68 + tmp70
  tmp72 = -6*ss
  tmp73 = -tmp68
  tmp74 = tmp17 + tmp73
  tmp75 = 1/tmp71
  tmp76 = 1/tmp13 + tmp31
  tmp77 = tmp31 + tmp9
  tmp78 = -(1/(tmp13*tmp67))
  tmp79 = tmp26 + tmp78
  tmp80 = 1/tmp79
  tmp81 = 4*ss
  tmp82 = 2*ss
  tmp83 = 1/tmp13 + tmp82
  tmp84 = tmp2*tmp83
  tmp85 = 2*tmp18
  tmp86 = 2*tmp16
  tmp87 = 2*tmp19
  tmp88 = 6*ss
  tmp89 = 1/tmp13 + tmp88
  tmp90 = -(tmp18*tmp89)
  tmp91 = -2*mm2
  tmp92 = tmp36 + tmp72 + tmp91
  tmp93 = tmp17*tmp92
  tmp94 = -2*tmp20
  tmp95 = tmp14/tmp13
  tmp96 = tmp30 + tmp94 + tmp95
  tmp97 = ss*tmp96
  tmp98 = 6*tmp20
  tmp99 = tmp29 + tmp30 + tmp98
  tmp100 = mm2*tmp99
  tmp101 = -6*tmp20
  tmp102 = tmp10*tmp83
  tmp103 = tmp101 + tmp102 + tmp29 + tmp30 + tmp85
  tmp104 = -(tmp103/tmp67)
  tmp105 = tmp100 + tmp104 + tmp86 + tmp87 + tmp90 + tmp93 + tmp97
  tmp106 = ss/tmp13
  tmp107 = tmp75**2
  tmp108 = 6*mm2
  tmp109 = 5*ss
  tmp110 = 10*ss
  tmp111 = tmp5 + tmp82
  tmp112 = 9/tmp13
  tmp113 = -5/tmp13
  tmp114 = -3 + tmp1
  tmp115 = tmp114/tmp13
  tmp116 = 1/tmp76
  tmp117 = tmp23*tmp26
  tmp118 = -4*kappa*tmp17*tmp23
  tmp119 = 16*tmp1*tmp19
  tmp120 = (-8*ss*tmp23)/tmp67
  tmp121 = (kappa*tmp23*tmp65)/tmp67
  tmp122 = tmp23*tmp28
  tmp123 = -4*kappa*tmp20*tmp23
  tmp124 = -4*tmp17*tmp37
  tmp125 = -6*tmp1*tmp106
  tmp126 = -4*kappa*tmp106*tmp23
  tmp127 = (8*tmp1*tmp106)/tmp67
  tmp128 = -4*tmp20*tmp37
  tmp129 = tmp1*tmp50
  tmp130 = -2*kappa*tmp23*tmp30
  tmp131 = tmp30*tmp88
  tmp132 = tmp13**(-3)
  tmp133 = 3*tmp132
  tmp134 = (f1p*tmp83)/(tmp116*tmp55)
  tmp135 = -8/tmp67
  tmp136 = tmp135 + tmp59 + tmp65 + tmp76
  tmp137 = -4*tmp1*tmp136*tmp18
  tmp138 = (-2*tmp22*tmp23)/tmp67
  tmp139 = tmp1*tmp26
  tmp140 = tmp1*tmp28
  tmp141 = -2/tmp67
  tmp142 = 3 + tmp141 + tmp7
  tmp143 = tmp142*tmp37
  tmp144 = 1 + kappa
  tmp145 = tmp1*tmp144
  tmp146 = -4/tmp67
  tmp147 = 3 + tmp145 + tmp146
  tmp148 = tmp1*tmp147
  tmp149 = tmp115 + tmp148
  tmp150 = tmp149*tmp82
  tmp151 = tmp138 + tmp139 + tmp140 + tmp143 + tmp150 + tmp50
  tmp152 = tmp151*tmp24
  tmp153 = tmp117 + tmp118 + tmp119 + tmp120 + tmp121 + tmp122 + tmp123 + tmp124 +&
                          & tmp125 + tmp126 + tmp127 + tmp128 + tmp129 + tmp130 + tmp131 + tmp133 + tmp134 &
                          &+ tmp137 + tmp152
  tmp154 = tmp67**(-5)
  tmp155 = 6/tmp13
  tmp156 = tmp67**(-4)
  tmp157 = mm2**4
  tmp158 = tmp36 + tmp82
  tmp159 = 24*ss
  tmp160 = -40*ss
  tmp161 = 9*tmp30
  tmp162 = 16*tmp20
  tmp163 = -26*tmp106
  tmp164 = -2*tmp37
  tmp165 = tmp1 + 1/tmp13
  tmp166 = tmp165 + tmp82
  tmp167 = 2*tmp20
  tmp168 = tmp166*tmp18
  tmp169 = ss*tmp165
  tmp170 = 3*tmp169
  tmp171 = tmp67**(-6)
  tmp172 = 12*mm2
  tmp173 = 1/tmp13 + tmp25 + 1/tmp67
  tmp174 = sqrt(lambda)**6
  tmp175 = -15*tmp37
  tmp176 = mm2**5
  tmp177 = kappa*tmp23
  tmp178 = 5*tmp1
  tmp179 = 12 + tmp178
  tmp180 = 36/tmp13
  tmp181 = 4*tmp1
  tmp182 = 3 + tmp1
  tmp183 = 24*tmp1
  tmp184 = 2/tmp13
  tmp185 = tmp1 + tmp184
  tmp186 = 6 + tmp1
  tmp187 = ss*tmp155
  tmp188 = 1/tmp13 + tmp7
  tmp189 = 2 + tmp1
  tmp190 = 3 + tmp7
  tmp191 = 4/tmp13
  tmp192 = -2*tmp1
  tmp193 = tmp25**4
  tmp194 = -6/tmp13
  tmp195 = 1/tmp13 + tmp192
  tmp196 = -7*tmp1
  tmp197 = 12/tmp13
  tmp198 = 80*tmp20
  tmp199 = -3*tmp1
  tmp200 = 7/tmp13
  tmp201 = tmp192 + tmp36
  tmp202 = 16*tmp157
  tmp203 = tmp1 + tmp5 + tmp81
  tmp204 = 48*tmp20
  tmp205 = 5/tmp13
  tmp206 = 6*tmp30
  MP2MPL_MP_FIN_d11d20 = -8*logmuP2*Pi*tmp13*(tmp115 + f1p/tmp13 + tmp1*tmp15) - 16*D0IR6tsN*Pi*tmp13*tmp&
                          &15*tmp22*tmp23*(20*tmp18 + tmp26 + tmp27 + tmp28 + tmp29 + tmp30)*tmp33 - 16*log&
                          &muN2*Pi*tmp22*tmp23*tmp4 + 32*logbeN2*Pi*tmp22*tmp23*tmp33*tmp40*tmp41*tmp53 + 1&
                          &6*logmuL2*Pi*tmp1*tmp4*tmp60 + (32*logbeL2*Pi*tmp1*tmp33*tmp40*tmp41*tmp43*tmp53&
                          &)/(tmp116*tmp61) - 16*D0IR6tsL*Pi*tmp1*tmp13*tmp15*tmp33*tmp43*(tmp23 + tmp26 + &
                          &tmp27 + tmp28 + tmp29 + tmp30 + tmp34 + tmp35 + tmp62) - 8*DB0sFP*logF2P2*Pi*ss*&
                          &tmp116*tmp13*tmp15*tmp153*tmp55*tmp75 - 16*DB0sFP*logmuF2*Pi*ss*tmp116*tmp13*tmp&
                          &15*tmp153*tmp55*tmp75 + (32*DB0F2FN*Pi*tmp105*tmp13*tmp22*tmp23*tmp33*tmp40*tmp7&
                          &5)/tmp67 + (32*DB0F2FL*Pi*tmp1*tmp105*tmp13*tmp33*tmp40*tmp43*tmp75)/tmp67 + 16*&
                          &logmut*Pi*tmp13*tmp15*tmp8 + 8*logF2t*Pi*tmp12*tmp40*tmp8 + 8*logP2t*Pi*tmp12*tm&
                          &p55*tmp8 + 8*C0tF2F20F0*Pi*tmp13*tmp40*(-((tmp28 + tmp29 + tmp30 + tmp34 + tmp35&
                          &)/tmp13) + tmp26*(1/tmp13 + tmp24 + tmp56) + (2*(tmp30 + tmp49 + tmp57 + tmp58 +&
                          & tmp64))/tmp67)*tmp8 + 16*DB0sFPlogmut*Pi*ss*tmp15*(tmp26 + tmp27 + tmp28 + tmp2&
                          &9 + tmp30 + tmp34 + tmp35)*tmp41*tmp75*tmp8 + 16*C0IR6sFP*Pi*tmp13*tmp15*(-tmp17&
                          &7 + tmp36 + tmp37 + tmp182*tmp82 + tmp6*tmp83) + 16*C0tF2F2NF0*Pi*tmp13*tmp22*tm&
                          &p23*tmp33*tmp40*(tmp26*(tmp5 + tmp81) + (tmp28 + tmp29 + tmp30 + tmp34 + tmp84)/&
                          &tmp13 + tmp141*(tmp30 + tmp57 + tmp58 + tmp84)) + 8*C0tP2P20P0*Pi*tmp13*tmp55*tm&
                          &p8*(-((tmp28 + tmp29 + tmp30)/tmp13) + tmp26/tmp55 + tmp34*(1/tmp13 + tmp56) + t&
                          &mp10*(tmp30 + tmp57 + tmp58) + (8*(tmp106 + mm2*tmp56 + tmp85))/tmp67) + (2*logF&
                          &2P2*Pi*tmp107*tmp13*tmp33*tmp67*((4*tmp171*(-3/tmp13 + (-12 + tmp1)*tmp14 + tmp1&
                          &72))/tmp33 - 4*tmp17*tmp25*(-12*(tmp1 + ss*(8 + tmp1) + tmp113)*tmp157 + 48*tmp1&
                          &76 + tmp1*(-9/tmp13 + tmp177 + ss*tmp179)*tmp21 + tmp19*(tmp140 + tmp175 + ss*(-&
                          &84/tmp13 + tmp183 + 7*tmp23)) + mm2*tmp20*(-4*tmp179*tmp20 + tmp155*tmp22*tmp23 &
                          &+ ss*(-24*tmp1 + tmp180 + (1 - 12*kappa)*tmp23) + 3*tmp37) + ss*tmp18*(-12*tmp10&
                          &6 - kappa*tmp174 + (19 - 20*kappa)*ss*tmp23 + tmp184*tmp22*tmp23 + (24 + 7*tmp1)&
                          &*tmp28 + 21*tmp37)) + tmp14*tmp185*tmp193*tmp23*tmp43 + 12/(tmp4*tmp67**7) + (4*&
                          &f1p*tmp173*(-mm2 + ss + 1/tmp67))/(tmp107*tmp33*tmp67) - tmp156*(32*(ss*tmp1 + 1&
                          &5/tmp13)*tmp19 + (tmp159/tmp13 + tmp175 - 3*ss*tmp23 + tmp1*tmp57)*tmp64 + ss*tm&
                          &p1*(-24*tmp106 + kappa*tmp174 + tmp177*tmp184 - 40*tmp1*tmp20 + tmp177*tmp81) + &
                          &mm2*tmp81*(40*tmp1*tmp20 + tmp174*tmp22 + (tmp192*(6 + tmp59))/tmp13 + (tmp155 +&
                          & (-7 + 5*kappa)*tmp23)*tmp81)) + tmp154*(-240*tmp19 + (tmp175 + 20*tmp114*tmp20 &
                          &+ ss*(18*tmp1 + tmp180 - 5*tmp23))*tmp24 + 12*tmp18*(5*(tmp1 + tmp191) + (-6 + t&
                          &mp1)*tmp81) + tmp1*(-10*ss*tmp114 - 18/tmp13 + tmp177)*tmp82) + (2*tmp68*(24*tmp&
                          &176 + ss*tmp18*(-((-4 + kappa)*tmp174) + tmp1*(9 + 2*tmp145)*tmp184 - 24*(4 + tm&
                          &p1)*tmp20 + ss*(-36*tmp1 + 72/tmp13 + (26 - 40*kappa)*tmp23)) + (tmp167*tmp182 +&
                          & 2*tmp177*tmp188 + ss*(tmp177 + tmp194))*tmp20*tmp31 + 6*tmp19*(ss*(-12/tmp13 + &
                          &tmp181 + tmp23) + tmp186*tmp28 + tmp63) - 2*tmp157*(3*(tmp1 - 4/tmp13) + (12 + t&
                          &mp1)*tmp81) + mm2*ss*(-(kappa*tmp174*tmp185) + 8*tmp182*tmp21 + tmp20*(-24/tmp13&
                          & + tmp183 + (18 - 8*kappa)*tmp23) + tmp106*tmp192*(9 + (-3 + kappa)*tmp7) + (8 +&
                          & kappa)*tmp174*tmp82)))/tmp67 + (120*tmp176 + tmp1*tmp167*(tmp187 + tmp177*tmp18&
                          &8 - 5*tmp190*tmp20) - 4*tmp19*(ss*tmp180 + tmp182*tmp28 + ss*tmp186*tmp31 + 15*t&
                          &mp37) + tmp18*(tmp174 + 12*tmp189*tmp20 + ss*(tmp197 + tmp199 + (9 - 10*kappa)*t&
                          &mp23) + (-9 + tmp145)*tmp37)*tmp56 + mm2*ss*(kappa*tmp174*tmp185 + 40*tmp190*tmp&
                          &21 - 12*tmp20*(tmp191 + tmp192 + (5 - 6*kappa)*tmp23) + (4*tmp174 + (-3 + (-1 + &
                          &3*kappa)*tmp1)*tmp37)*tmp56) - 2*tmp157*(15*(tmp1 - 8/tmp13) + tmp186*tmp65))*tm&
                          &p86))/ss + logP2L2*Pi*tmp107*tmp13*tmp33*tmp55*tmp60*tmp67*tmp7*(8*tmp171*(tmp19&
                          &5 + tmp24) - 2*tmp154*(48*tmp18 + 20*ss*tmp195 + (20*ss + 1/tmp13 + tmp196)*tmp2&
                          &4 + (tmp184 + tmp31)*tmp36) + (tmp1*tmp185*tmp193)/tmp55 + tmp156*(64*tmp19 + ss&
                          &*tmp191*(tmp196 + tmp197) + tmp195*tmp198 + (tmp1 + tmp194)*tmp37 + tmp24*(tmp19&
                          &8 - tmp23 + 12*tmp30 + (tmp178 + tmp184)*tmp56 + tmp63) + tmp48*(-3*tmp165 + tmp&
                          &65)) - 4*tmp17*tmp25*(24*tmp157 + mm2*(-8*tmp1*tmp106 + (32*tmp1 - 30/tmp13)*tmp&
                          &20 + tmp1*tmp206 + 40*tmp21) + ss*(3*tmp106*(tmp191 + tmp199) + 10*tmp195*tmp20 &
                          &+ tmp30*tmp7) - 2*tmp19*(8*tmp1 - 11/tmp13 + tmp81) - tmp18*(56*tmp20 + (tmp178 &
                          &+ tmp191)*tmp36 + tmp195*tmp82)) + (2*tmp68*(ss*(tmp106*(tmp178 + tmp194) + (tmp&
                          &192*tmp195)/tmp13 - 4*tmp195*tmp20) + tmp202 - 12*tmp19*tmp203 + tmp18*(32*ss*tm&
                          &p1 - 28*tmp106 + tmp204 - 4*tmp23 - 6*tmp30 - 19*tmp37) + mm2*(-16*tmp21 + (tmp1&
                          &96 + tmp205)*tmp28 + (tmp1 + tmp155)*tmp37 + (tmp206 - 8*tmp23 + tmp63)*tmp82)))&
                          &/tmp67 + (32*tmp157 + tmp34*(tmp110/tmp13 + 24*tmp20 + tmp23 - 9*tmp30 - 4*tmp37&
                          & + tmp1*tmp56) + mm2*(24*tmp20*tmp201 - 160*tmp21 + (10/tmp13 + tmp31)*tmp37 + (&
                          &2*tmp23 + tmp37 + tmp50)*tmp65) + 8*tmp19*(tmp192 + tmp200 + tmp81) + (-20*tmp19&
                          &5*tmp20 + ss*tmp194*tmp201 + tmp195*tmp37)*tmp82)*tmp86) - 8*log4*Pi*tmp107*tmp1&
                          &3*tmp22*tmp23*tmp33*(2*tmp154 - tmp156*(tmp108 + tmp110 + tmp36) + (2*(-3*tmp157&
                          & + tmp167*tmp18 + 7*tmp111*tmp19 + mm2*(-18*ss + 1/tmp13)*tmp20 + (tmp109 + tmp1&
                          &55)*tmp21))/tmp67 + (mm2*(tmp36 + tmp72) + (tmp109 + tmp36)*tmp82 + tmp85)*tmp86&
                          & + tmp68*(-(tmp158*tmp20) + tmp18*(-26*ss + tmp200) + mm2*(ss - 2/tmp13)*tmp82 +&
                          & tmp87) + 2*tmp17*(mm2*ss*(tmp113 + tmp159) - (tmp110 + tmp112)*tmp20 + tmp87 + &
                          &tmp85*tmp89)) - 8*logmuF2*Pi*tmp13*(3*mm2*tmp1 + tmp108 + ss*tmp199 + tmp36 + tm&
                          &p37 + (3*tmp189)/tmp67 + tmp72 + tmp6*(tmp10 + tmp11 + 1/tmp13 + tmp9)) + logF2L&
                          &2*Pi*tmp33*tmp43*tmp7*((4*tmp40)/tmp116 + tmp1*tmp107*tmp13*tmp67*(6*tmp16 - tmp&
                          &17*(12*ss + tmp1 + tmp172) + (2*(3*tmp18 + ss*(3*ss + tmp181) + mm2*(tmp1 + tmp7&
                          &2)))/tmp67 + tmp1*tmp73)*tmp74 - 2*tmp13*tmp15*tmp67*tmp77 - 2*tmp13*tmp173*tmp6&
                          &7*tmp77 - 4*tmp15*(tmp31 + tmp38)*tmp80 + (tmp164 + (4*tmp165)/tmp67)*tmp80 + tm&
                          &p13*tmp192*tmp67*tmp74*tmp75*(tmp5 + tmp9)) - (16*DB0sFP*Pi*tmp13*(f1p*mm2*ss*tm&
                          &p141 + (f1p*mm2*tmp141)/tmp13 - 3*tmp16 + f1p*tmp16 + 3*mm2*tmp17 + 9*ss*tmp17 -&
                          & 3*f1p*ss*tmp17 - (3*tmp17)/tmp13 + (f1p*tmp17)/tmp13 + 9*ss*tmp18 - 3*f1p*ss*tm&
                          &p18 - (3*tmp18)/tmp13 + (f1p*tmp18)/tmp13 - 3*tmp19 + f1p*tmp19 - 9*mm2*tmp20 + &
                          &3*f1p*mm2*tmp20 + 3*tmp21 + tmp20*tmp36 + mm2*tmp17*tmp6 + (tmp20*tmp6)/tmp13 + &
                          &tmp21*tmp6 + (ss*tmp108)/tmp67 + (mm2*tmp155)/tmp67 + (3*tmp18)/tmp67 - (9*tmp20&
                          &)/tmp67 + (3*f1p*tmp20)/tmp67 + (tmp18*tmp6)/tmp67))/(mm2*tmp141 + ss*tmp141 + t&
                          &mp17 + tmp18 + tmp20 + ss*tmp91) - 16*C0tP2P2NP0*Pi*tmp13*tmp22*tmp23*tmp33*(ss*&
                          &tmp135 - 4*tmp18 + tmp26 + tmp28 + tmp29 + tmp30 + tmp91/tmp13) - 16*C0tP2P2LP0*&
                          &Pi*tmp1*tmp13*tmp33*tmp55*tmp60*(tmp203*tmp34 + tmp26*tmp61 + (tmp28 + tmp29 + t&
                          &mp30 + tmp63)/tmp13 + tmp146*(tmp29 + tmp34 - mm2*tmp66) + (tmp30 + tmp57 + tmp5&
                          &8 + tmp62 + tmp63)*tmp91) - 16*C0sF2P2FNP*Pi*tmp13*tmp22*tmp23*tmp33*tmp75*(tmp1&
                          &6*(tmp24 + tmp83) - tmp17*(7*mm2*tmp111 + tmp34 + 3*ss*tmp83) + (-((tmp110 + 17/&
                          &tmp13)*tmp18) - 4*tmp19 + mm2*(tmp113 + tmp81)*tmp82 + 3*tmp20*tmp83)/tmp67 + tm&
                          &p25*(4*tmp19 + tmp18*(tmp112 + tmp72) + tmp20*tmp83 + tmp106*tmp91)) + 16*C0tF2F&
                          &2LF0*Pi*tmp1*tmp13*tmp33*tmp40*tmp60*(-((tmp28 + tmp29 + tmp30 + tmp34 + tmp35 +&
                          & tmp63)/tmp13) + tmp26*(tmp24 + tmp56 + tmp76) + (2*(tmp30 + tmp57 + tmp58 + tmp&
                          &62 + tmp63 + tmp64 + tmp66*tmp91))/tmp67) - 8*logF2N2*Pi*tmp107*tmp13*tmp22*tmp2&
                          &3*tmp33*tmp40*(tmp154*(-40*mm2 + tmp155 + tmp160) + 8*tmp171 - 2*tmp17*(28*tmp15&
                          &7 + (34/tmp13 + tmp160)*tmp19 + (18*tmp106 + tmp161 - 20*tmp20)*tmp20 + mm2*ss*(&
                          &16*tmp106 - 8*tmp20 + 15*tmp30) + (tmp187 + 10*tmp20 + 5*tmp30)*tmp34) + tmp156*&
                          &(mm2*(32*ss - 38/tmp13) + tmp163 + 64*tmp18 + tmp198 + tmp50) + (2*(28*tmp176 + &
                          &(-4*tmp20 + ss*tmp200 + tmp206)*tmp21 + mm2*tmp20*(-18*tmp106 - 28*tmp20 + 3*tmp&
                          &30) + tmp19*(tmp163 + 64*tmp20 + 19*tmp30) + tmp157*(-92*ss + tmp36) + ss*(17*tm&
                          &p106 + tmp162 + 2*tmp30)*tmp85))/tmp67 + (tmp131 + mm2*(42*tmp106 + tmp161 + tmp&
                          &162) - 8*tmp19 + (22*tmp20)/tmp13 - 40*tmp21 + (21/tmp13 + tmp65)*tmp85)*tmp86 +&
                          & tmp73*((tmp158*tmp20)/tmp13 + tmp202 - 2*tmp19*(tmp159 + tmp205) + tmp18*(tmp20&
                          &4 + tmp29 + 13*tmp30) + mm2*(tmp112 + tmp65)*tmp94)) + 16*C0sF2P2FLP*Pi*tmp1*tmp&
                          &13*tmp33*tmp60*tmp75*(tmp16*tmp166 + tmp25*(tmp168 + tmp166*tmp20 + (tmp167 + tm&
                          &p169 + tmp63)*tmp91) - tmp17*(tmp164 + mm2*tmp166 + tmp170 + tmp98) - (tmp168 + &
                          &tmp10*(tmp167 + tmp169 + tmp1*tmp184) + tmp14*(tmp164 + tmp170 + tmp98))/tmp67) &
                          &+ 32*DB0P2LP*mm2*Pi*tmp1*tmp13*tmp33*tmp43*tmp55*tmp75*(tmp25*(tmp106 + tmp167 -&
                          & tmp30 + mm2*(tmp36 + tmp56) + tmp85) + tmp86 - tmp17*(tmp10 + tmp89) + (-2*tmp1&
                          &8 + tmp83*tmp91 + tmp99)/tmp67)

  END FUNCTION MP2MPL_MP_FIN_D11D20

  FUNCTION MP2MPL_MP_FIN_D12D20(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_FIN_d12d20
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206, tmp207, tmp208, tmp209, tmp210
  real tmp211, tmp212, tmp213, tmp214, tmp215
  real tmp216, tmp217, tmp218, tmp219, tmp220
  real tmp221, tmp222, tmp223, tmp224, tmp225
  real tmp226, tmp227, tmp228, tmp229, tmp230
  real tmp231, tmp232, tmp233, tmp234, tmp235
  real tmp236, tmp237, tmp238, tmp239, tmp240
  real tmp241, tmp242, tmp243, tmp244, tmp245
  real tmp246, tmp247, tmp248, tmp249, tmp250
  real tmp251, tmp252, tmp253, tmp254, tmp255
  real tmp256, tmp257, tmp258, tmp259, tmp260
  real tmp261, tmp262, tmp263, tmp264, tmp265
  real tmp266, tmp267, tmp268, tmp269, tmp270
  real tmp271, tmp272, tmp273, tmp274, tmp275
  real tmp276, tmp277, tmp278, tmp279, tmp280
  real tmp281, tmp282, tmp283, tmp284, tmp285
  real tmp286, tmp287, tmp288, tmp289, tmp290
  real tmp291, tmp292, tmp293, tmp294, tmp295
  real tmp296, tmp297, tmp298, tmp299, tmp300
  real tmp301, tmp302, tmp303, tmp304, tmp305
  real tmp306, tmp307, tmp308, tmp309, tmp310
  real tmp311, tmp312, tmp313, tmp314, tmp315
  real tmp316, tmp317, tmp318, tmp319, tmp320
  real tmp321, tmp322, tmp323, tmp324, tmp325
  real tmp326, tmp327, tmp328, tmp329, tmp330
  real tmp331, tmp332, tmp333, tmp334, tmp335
  real tmp336, tmp337, tmp338, tmp339, tmp340
  real tmp341, tmp342, tmp343, tmp344, tmp345
  real tmp346, tmp347, tmp348, tmp349, tmp350
  real tmp351, tmp352, tmp353, tmp354, tmp355
  real tmp356, tmp357, tmp358, tmp359, tmp360
  real tmp361, tmp362, tmp363, tmp364, tmp365
  real tmp366, tmp367, tmp368, tmp369, tmp370
  real tmp371, tmp372, tmp373, tmp374, tmp375
  real tmp376, tmp377, tmp378, tmp379, tmp380
  real tmp381, tmp382, tmp383, tmp384, tmp385
  real tmp386, tmp387, tmp388, tmp389, tmp390
  real tmp391, tmp392, tmp393, tmp394, tmp395
  real tmp396, tmp397, tmp398, tmp399, tmp400
  real tmp401, tmp402, tmp403, tmp404, tmp405
  real tmp406, tmp407, tmp408, tmp409, tmp410
  real tmp411, tmp412, tmp413, tmp414, tmp415
  real tmp416, tmp417, tmp418, tmp419, tmp420
  real tmp421, tmp422, tmp423, tmp424, tmp425
  real tmp426, tmp427, tmp428, tmp429, tmp430
  real tmp431, tmp432, tmp433, tmp434, tmp435
  real tmp436, tmp437, tmp438, tmp439, tmp440
  real tmp441, tmp442, tmp443, tmp444, tmp445
  real tmp446, tmp447, tmp448, tmp449, tmp450
  real tmp451, tmp452, tmp453, tmp454, tmp455
  real tmp456, tmp457, tmp458, tmp459, tmp460
  real tmp461, tmp462, tmp463, tmp464, tmp465
  real tmp466, tmp467, tmp468, tmp469, tmp470
  real tmp471, tmp472, tmp473, tmp474, tmp475
  real tmp476, tmp477, tmp478, tmp479, tmp480
  real tmp481, tmp482, tmp483, tmp484, tmp485
  real tmp486, tmp487, tmp488, tmp489, tmp490
  real tmp491, tmp492, tmp493, tmp494, tmp495
  real tmp496, tmp497, tmp498, tmp499, tmp500
  real tmp501, tmp502, tmp503, tmp504, tmp505
  real tmp506, tmp507, tmp508, tmp509, tmp510
  real tmp511, tmp512, tmp513, tmp514, tmp515
  real tmp516, tmp517, tmp518, tmp519, tmp520
  real tmp521, tmp522, tmp523, tmp524, tmp525
  real tmp526, tmp527, tmp528, tmp529, tmp530
  real tmp531, tmp532, tmp533, tmp534, tmp535
  real tmp536, tmp537, tmp538, tmp539, tmp540
  real tmp541, tmp542, tmp543, tmp544, tmp545
  real tmp546, tmp547, tmp548, tmp549, tmp550
  real tmp551, tmp552

  tmp1 = sqrt(lambda)**2
  tmp2 = -tt
  tmp3 = 1 + tmp1
  tmp4 = -2*f1p
  tmp5 = 3 + tmp1 + tmp4
  tmp6 = 2*me2
  tmp7 = 2*mm2
  tmp8 = -2*ss
  tmp9 = tmp2 + tmp6 + tmp7 + tmp8
  tmp10 = 1/tt
  tmp11 = me2**3
  tmp12 = me2**2
  tmp13 = mm2**2
  tmp14 = mm2**3
  tmp15 = ss**2
  tmp16 = ss**3
  tmp17 = 4*mm2
  tmp18 = -ss
  tmp19 = me2 + mm2 + tmp18
  tmp20 = mm2 + tmp18
  tmp21 = 4*tmp12
  tmp22 = 8*me2*tmp20
  tmp23 = 4*tmp15
  tmp24 = (2*ss)/tmp10
  tmp25 = tmp10**(-2)
  tmp26 = -f1p
  tmp27 = 2 + tmp1 + tmp26
  tmp28 = -tmp1
  tmp29 = tmp17 + tmp28
  tmp30 = 1/tmp29
  tmp31 = sqrt(lambda)**4
  tmp32 = 4*tmp13
  tmp33 = -8*mm2*ss
  tmp34 = -1 + kappa
  tmp35 = -4*mm2
  tmp36 = tmp1 + tmp35
  tmp37 = kappa*tmp1
  tmp38 = tmp36**(-2)
  tmp39 = kappa*me2*tmp31
  tmp40 = kappa*tmp18*tmp31
  tmp41 = (-2*tmp1)/tmp10
  tmp42 = (f1p*tmp36)/tmp10
  tmp43 = 8/tmp10
  tmp44 = 3*tmp1
  tmp45 = 4 + tmp44
  tmp46 = 2 + tmp1
  tmp47 = 4*me2
  tmp48 = tmp2 + tmp47
  tmp49 = 1/tmp48
  tmp50 = 1/tmp25
  tmp51 = 16*mm2*tmp12
  tmp52 = -3*mm2
  tmp53 = ss + 1/tmp10 + tmp52
  tmp54 = tmp25*tmp53
  tmp55 = 16*tmp13
  tmp56 = -16*mm2*ss
  tmp57 = -3*tmp25
  tmp58 = tmp55 + tmp56 + tmp57
  tmp59 = me2*tmp58
  tmp60 = tmp51 + tmp54 + tmp59
  tmp61 = tmp35 + tmp37
  tmp62 = 1/tmp10 + tmp35
  tmp63 = 1/tmp62
  tmp64 = sqrt(lambda)**6
  tmp65 = -tmp37
  tmp66 = tmp17 + tmp65
  tmp67 = tmp61**2
  tmp68 = 2*tmp31
  tmp69 = 4*ss*tmp1
  tmp70 = 12*tmp15
  tmp71 = tmp34**2
  tmp72 = sqrt(lambda)**8
  tmp73 = 4*tmp11
  tmp74 = 12*tmp12*tmp20
  tmp75 = tmp17 + tmp2
  tmp76 = 1/tmp75
  tmp77 = -4*ss
  tmp78 = tmp23 + tmp24 + tmp25
  tmp79 = 8*tmp15
  tmp80 = (4*ss)/tmp10
  tmp81 = 4*ss
  tmp82 = 8*ss
  tmp83 = 2*ss*tmp1
  tmp84 = tmp28/tmp10
  tmp85 = 8*tmp13
  tmp86 = tmp1 + tmp82
  tmp87 = tmp21/tmp63
  tmp88 = tmp78 + tmp84
  tmp89 = tmp88/tmp10
  tmp90 = -3/tmp10
  tmp91 = 2*tmp13
  tmp92 = tmp1 + 1/tmp10
  tmp93 = 2*ss*tmp92
  tmp94 = 20*tmp14
  tmp95 = -20*ss*tmp13
  tmp96 = tmp23 + tmp25 + tmp31 + tmp93
  tmp97 = tmp18*tmp96
  tmp98 = 28*tmp13
  tmp99 = tmp25 + tmp31 + tmp56 + tmp70 + tmp93 + tmp98
  tmp100 = me2*tmp99
  tmp101 = tmp1*tmp43
  tmp102 = tmp101 + tmp96
  tmp103 = mm2*tmp102
  tmp104 = tmp100 + tmp103 + tmp73 + tmp74 + tmp94 + tmp95 + tmp97
  tmp105 = 2*tmp1
  tmp106 = ss/tmp10
  tmp107 = -2*tmp1
  tmp108 = 1/tmp10 + tmp107
  tmp109 = tmp108/tmp10
  tmp110 = tmp1 + tmp81
  tmp111 = tmp81*tmp92
  tmp112 = 2*ss
  tmp113 = 1/tmp10 + tmp112
  tmp114 = tmp113*tmp35
  tmp115 = tmp20**2
  tmp116 = mm2 + ss
  tmp117 = -2*me2*tmp116
  tmp118 = tmp115 + tmp117 + tmp12
  tmp119 = 1/tmp118
  tmp120 = -6*ss
  tmp121 = 3/tmp10
  tmp122 = -tmp106
  tmp123 = -2 + f1p
  tmp124 = 2*tmp11
  tmp125 = 2*tmp14
  tmp126 = 6*ss
  tmp127 = 1/tmp10 + tmp126
  tmp128 = -(tmp127*tmp13)
  tmp129 = -2*mm2
  tmp130 = tmp120 + tmp121 + tmp129
  tmp131 = tmp12*tmp130
  tmp132 = -2*tmp15
  tmp133 = tmp122 + tmp132 + tmp25
  tmp134 = ss*tmp133
  tmp135 = 6*tmp15
  tmp136 = tmp135 + tmp24 + tmp25
  tmp137 = mm2*tmp136
  tmp138 = -6*tmp15
  tmp139 = tmp113*tmp7
  tmp140 = tmp138 + tmp139 + tmp24 + tmp25 + tmp91
  tmp141 = -(me2*tmp140)
  tmp142 = tmp124 + tmp125 + tmp128 + tmp131 + tmp134 + tmp137 + tmp141
  tmp143 = tmp110 + tmp2
  tmp144 = 1/tmp10 + tmp28
  tmp145 = -8*tmp15
  tmp146 = 2/tmp10
  tmp147 = -tmp25
  tmp148 = tmp10**(-3)
  tmp149 = -8*me2
  tmp150 = 1 + kappa
  tmp151 = tmp144 + tmp149 + tmp37 + tmp82
  tmp152 = tmp1*tmp150
  tmp153 = -4*me2
  tmp154 = -2 + tmp1
  tmp155 = 1/tmp144
  tmp156 = 8*tmp12*tmp31
  tmp157 = -8*kappa*tmp12*tmp31
  tmp158 = tmp21*tmp64
  tmp159 = -4*kappa*tmp12*tmp64
  tmp160 = 16*tmp1*tmp14*tmp46
  tmp161 = -16*me2*ss*tmp31
  tmp162 = 16*ss*tmp39
  tmp163 = ss*tmp149*tmp64
  tmp164 = kappa*me2*tmp64*tmp82
  tmp165 = tmp31*tmp79
  tmp166 = kappa*tmp145*tmp31
  tmp167 = tmp23*tmp64
  tmp168 = -4*kappa*tmp15*tmp64
  tmp169 = (-8*tmp1*tmp12)/tmp10
  tmp170 = (-4*tmp12*tmp31)/tmp10
  tmp171 = (tmp1*tmp120)/tmp10
  tmp172 = -8*kappa*tmp106*tmp31
  tmp173 = ss*tmp146*tmp64
  tmp174 = -4*kappa*tmp106*tmp64
  tmp175 = 16*me2*tmp1*tmp106
  tmp176 = 8*me2*tmp106*tmp31
  tmp177 = (tmp1*tmp145)/tmp10
  tmp178 = (-4*tmp15*tmp31)/tmp10
  tmp179 = tmp1*tmp57
  tmp180 = -4*kappa*tmp25*tmp31
  tmp181 = tmp25*tmp64
  tmp182 = -2*kappa*tmp181
  tmp183 = tmp126*tmp25
  tmp184 = tmp25*tmp31*tmp8
  tmp185 = 3*tmp148
  tmp186 = -(tmp148*tmp31)
  tmp187 = -4*tmp1*tmp13*tmp151*tmp46
  tmp188 = tmp28*tmp34
  tmp189 = tmp188 + tmp6
  tmp190 = me2*tmp105*tmp189*tmp46
  tmp191 = tmp1*tmp23*tmp46
  tmp192 = 4*tmp1
  tmp193 = -2*me2*tmp46
  tmp194 = 3 + tmp192 + tmp193 + tmp31
  tmp195 = (tmp1*tmp194)/tmp10
  tmp196 = -3 + tmp31
  tmp197 = tmp196*tmp25
  tmp198 = 2*tmp152
  tmp199 = kappa*tmp31
  tmp200 = tmp153*tmp46
  tmp201 = 3 + tmp198 + tmp199 + tmp200
  tmp202 = tmp1*tmp201
  tmp203 = -3 + tmp105 + tmp68
  tmp204 = tmp203/tmp10
  tmp205 = tmp202 + tmp204
  tmp206 = tmp112*tmp205
  tmp207 = tmp190 + tmp191 + tmp195 + tmp197 + tmp206
  tmp208 = tmp17*tmp207
  tmp209 = tmp12*tmp68
  tmp210 = -2*tmp12*tmp199
  tmp211 = 8*tmp1*tmp14
  tmp212 = (tmp107*tmp12)/tmp10
  tmp213 = tmp1*tmp147
  tmp214 = tmp147*tmp199
  tmp215 = tmp1*tmp34
  tmp216 = 1/tmp10 + tmp215
  tmp217 = tmp1*tmp132*tmp216
  tmp218 = tmp107*tmp13*tmp151
  tmp219 = -(me2*tmp31*tmp34)
  tmp220 = tmp105*tmp12
  tmp221 = 2 + tmp152 + tmp153
  tmp222 = ss*tmp1*tmp221
  tmp223 = tmp105*tmp15
  tmp224 = -me2
  tmp225 = tmp224 + tmp3
  tmp226 = (tmp1*tmp225)/tmp10
  tmp227 = tmp106*tmp154
  tmp228 = tmp147 + tmp219 + tmp220 + tmp222 + tmp223 + tmp226 + tmp227
  tmp229 = tmp17*tmp228
  tmp230 = me2*tmp34*tmp68
  tmp231 = -2*me2
  tmp232 = 1 + tmp231 + tmp37
  tmp233 = tmp232*tmp84
  tmp234 = tmp230 + tmp233 + tmp25
  tmp235 = tmp112*tmp234
  tmp236 = tmp148 + tmp209 + tmp210 + tmp211 + tmp212 + tmp213 + tmp214 + tmp217 +&
                          & tmp218 + tmp229 + tmp235
  tmp237 = tmp236*tmp4
  tmp238 = tmp156 + tmp157 + tmp158 + tmp159 + tmp160 + tmp161 + tmp162 + tmp163 +&
                          & tmp164 + tmp165 + tmp166 + tmp167 + tmp168 + tmp169 + tmp170 + tmp171 + tmp172 &
                          &+ tmp173 + tmp174 + tmp175 + tmp176 + tmp177 + tmp178 + tmp179 + tmp180 + tmp181&
                          & + tmp182 + tmp183 + tmp184 + tmp185 + tmp186 + tmp187 + tmp208 + tmp237
  tmp239 = 32*tmp14
  tmp240 = 16*tmp14
  tmp241 = tmp1/tmp10
  tmp242 = -tmp199
  tmp243 = 2*tmp199
  tmp244 = tmp146 + tmp242 + tmp81
  tmp245 = 8*tmp14
  tmp246 = -2/tmp10
  tmp247 = 4*tmp14
  tmp248 = 1/mm2
  tmp249 = me2**5
  tmp250 = ss + 1/tmp10
  tmp251 = 5*ss
  tmp252 = 1/tmp10 + tmp81
  tmp253 = -13*ss
  tmp254 = me2**4
  tmp255 = tmp248**(-4)
  tmp256 = tmp46/tmp10
  tmp257 = -2 + tmp152
  tmp258 = 12*ss
  tmp259 = -2 + kappa
  tmp260 = tmp259*tmp28
  tmp261 = 8 + tmp260
  tmp262 = tmp261/tmp10
  tmp263 = 8 + tmp1
  tmp264 = tmp263/tmp10
  tmp265 = tmp248**(-5)
  tmp266 = tmp1*tmp259
  tmp267 = 80*tmp15*tmp92
  tmp268 = -2 + tmp266
  tmp269 = tmp268/tmp10
  tmp270 = 2*tmp256
  tmp271 = tmp248**(-6)
  tmp272 = -8 + tmp266
  tmp273 = 11*tmp1
  tmp274 = -3*tmp199
  tmp275 = 128*tmp271
  tmp276 = 4 + tmp266
  tmp277 = 13*tmp1
  tmp278 = -4 + tmp1
  tmp279 = 1/me2
  tmp280 = tmp119**2
  tmp281 = 20*ss
  tmp282 = 2 + kappa
  tmp283 = -46*tmp1
  tmp284 = 5*tmp152
  tmp285 = tmp1*tmp282
  tmp286 = 16 + tmp285
  tmp287 = tmp286/tmp10
  tmp288 = tmp199 + tmp287
  tmp289 = tmp288/tmp10
  tmp290 = 320*tmp16
  tmp291 = 5*tmp1
  tmp292 = 6*tmp1
  tmp293 = 4 + kappa
  tmp294 = 16 + tmp44
  tmp295 = 7*tmp1
  tmp296 = 3*kappa
  tmp297 = -5*tmp1
  tmp298 = 40*ss
  tmp299 = 9*tmp1
  tmp300 = 160*tmp15
  tmp301 = tmp146*tmp278
  tmp302 = 128*tmp16
  tmp303 = -16*tmp1
  tmp304 = 48*ss
  tmp305 = 18*tmp1
  tmp306 = tmp294/tmp10
  tmp307 = -4 + tmp285
  tmp308 = tmp307/tmp10
  tmp309 = tmp199 + tmp308
  tmp310 = 256*tmp16
  tmp311 = tmp279**(-6)
  tmp312 = tmp75 + tmp81
  tmp313 = 16*ss
  tmp314 = 24*ss
  tmp315 = 5/tmp10
  tmp316 = 5*tmp25
  tmp317 = 23/tmp10
  tmp318 = 92*ss
  tmp319 = 16*tmp15
  tmp320 = -4*tmp15
  tmp321 = 64*tmp15
  tmp322 = -40*ss
  tmp323 = 40*tmp15
  tmp324 = tmp105 + tmp244
  tmp325 = -4*tmp1
  tmp326 = -4/tmp10
  tmp327 = 4*tmp241
  tmp328 = tmp13*tmp324
  tmp329 = -6*tmp1
  tmp330 = tmp296*tmp31
  tmp331 = -2*tmp199
  tmp332 = 4/tmp10
  tmp333 = tmp1 + tmp113
  tmp334 = 2*tmp15
  tmp335 = tmp13*tmp333
  tmp336 = 2*tmp241
  tmp337 = ss*tmp92
  tmp338 = 3*tmp337
  tmp339 = tmp279**(-7)
  tmp340 = tmp1 + tmp146
  tmp341 = -5/tmp10
  tmp342 = 16*tmp1
  tmp343 = 8 + tmp291
  tmp344 = 24/tmp10
  tmp345 = -8/tmp10
  tmp346 = 4 + tmp1
  tmp347 = ss*tmp344
  tmp348 = 1/tmp10 + tmp105
  tmp349 = tmp146*tmp31
  tmp350 = -3 + kappa
  tmp351 = -10*tmp241
  tmp352 = tmp313/tmp10
  tmp353 = 8*tmp1
  tmp354 = -24/tmp10
  tmp355 = -8*tmp1
  tmp356 = tmp1 + tmp332
  tmp357 = 5*tmp356
  tmp358 = -5*tmp31
  tmp359 = tmp15*tmp353
  tmp360 = 5*kappa
  tmp361 = 2*kappa
  tmp362 = tmp329/tmp10
  tmp363 = tmp1*tmp323
  tmp364 = 4*tmp215
  tmp365 = kappa**2
  tmp366 = sqrt(lambda)**10
  tmp367 = tmp1*tmp350
  tmp368 = -3*kappa
  tmp369 = 3 + tmp105
  tmp370 = 3 + tmp1
  tmp371 = 9/tmp10
  tmp372 = -3*tmp1
  tmp373 = -10*kappa
  tmp374 = 6/tmp10
  tmp375 = 3*tmp31
  tmp376 = 3 + tmp192
  tmp377 = 40*tmp16*tmp376
  tmp378 = 3*tmp241
  tmp379 = 3 + kappa
  tmp380 = tmp248**(-7)
  tmp381 = -4*kappa*tmp181
  tmp382 = tmp1*tmp374
  tmp383 = (15*tmp31)/tmp10
  tmp384 = 4*kappa
  tmp385 = 6 + tmp291
  tmp386 = 8*kappa
  tmp387 = kappa*tmp64
  tmp388 = 12*tmp199*tmp25
  tmp389 = ss**4
  tmp390 = 16*tmp385*tmp389
  tmp391 = -(kappa*tmp72)
  tmp392 = tmp248**(-8)
  tmp393 = tmp1 + tmp345
  tmp394 = -6/tmp10
  tmp395 = 18/tmp10
  tmp396 = -5 + kappa
  tmp397 = 4 + tmp296
  tmp398 = 5 + tmp296
  tmp399 = 8*tmp25*tmp387
  tmp400 = 15*tmp31
  tmp401 = 7*tmp181*tmp365
  tmp402 = tmp325*tmp34
  tmp403 = -6 + tmp402
  tmp404 = tmp403/tmp10
  tmp405 = 3 + tmp215
  tmp406 = 7 + tmp361
  tmp407 = 19*kappa
  tmp408 = tmp1*tmp398
  tmp409 = -15*tmp1
  tmp410 = tmp1*tmp396
  tmp411 = tmp20**4
  tmp412 = -7*tmp1
  tmp413 = 12/tmp10
  tmp414 = -tmp31
  tmp415 = 80*tmp15
  tmp416 = 24*tmp15
  tmp417 = tmp1*tmp326
  tmp418 = tmp107 + tmp121
  tmp419 = 10/tmp10
  tmp420 = 6*tmp25
  tmp421 = tmp20**3
  tmp422 = tmp32*tmp340
  tmp423 = -((tmp110*tmp340)/tmp248)
  tmp424 = tmp31 + tmp336
  tmp425 = ss*tmp424
  tmp426 = tmp214 + tmp422 + tmp423 + tmp425
  tmp427 = 64*tmp14
  tmp428 = 4*tmp31
  tmp429 = 384*tmp255
  tmp430 = 6 + tmp1
  tmp431 = tmp256 + tmp329
  tmp432 = 256*tmp265
  tmp433 = -6 + tmp1
  tmp434 = tmp154/tmp10
  tmp435 = -12 + tmp367
  tmp436 = tmp435/tmp10
  tmp437 = tmp192 + tmp269
  tmp438 = 2*tmp25*tmp430
  tmp439 = 22*tmp1
  tmp440 = 32*ss
  tmp441 = 192*tmp15
  tmp442 = -12/tmp10
  tmp443 = tmp34*tmp375
  tmp444 = -20*tmp1
  tmp445 = 13*kappa
  tmp446 = -2*kappa
  tmp447 = -4 + kappa
  tmp448 = 4*tmp25*tmp430
  tmp449 = 10 + tmp1
  tmp450 = tmp449/tmp10
  tmp451 = tmp25*tmp296
  tmp452 = tmp430/tmp10
  tmp453 = -4*tmp31
  tmp454 = tmp25*tmp361
  tmp455 = kappa*tmp25
  tmp456 = 5 + tmp1
  tmp457 = 7*tmp148*tmp199
  tmp458 = 2 + tmp215
  tmp459 = tmp1*tmp419
  tmp460 = tmp420*tmp430
  tmp461 = kappa*tmp295
  tmp462 = 4*tmp455
  tmp463 = -46*tmp31
  tmp464 = 12/tmp248
  tmp465 = 32*tmp13
  tmp466 = -11/tmp10
  tmp467 = -6*tmp25
  tmp468 = 7/tmp10
  tmp469 = tmp107 + tmp468 + tmp81
  tmp470 = -8*tmp31
  tmp471 = 12*tmp25
  tmp472 = -2*tmp31
  tmp473 = 48*tmp15
  tmp474 = tmp273/tmp10
  tmp475 = -3*tmp92
  tmp476 = tmp475 + tmp82
  tmp477 = tmp1 + tmp341
  tmp478 = -40*tmp16
  tmp479 = tmp105 + tmp121
  tmp480 = tmp28 + tmp419
  tmp481 = tmp241*tmp480
  tmp482 = tmp1 + tmp374
  tmp483 = tmp241 + tmp420 + tmp68
  tmp484 = 1/tmp10 + tmp281 + tmp412
  tmp485 = tmp1*tmp420
  tmp486 = tmp1*tmp341
  tmp487 = 8*tmp31
  tmp488 = tmp1*tmp315
  tmp489 = 40*tmp16
  tmp490 = 14*tmp31
  tmp491 = -tmp452
  tmp492 = 12*tmp1
  tmp493 = 22 + tmp44
  tmp494 = -(tmp493/tmp10)
  tmp495 = 320*tmp15
  tmp496 = tmp1*tmp361
  tmp497 = -2*tmp25*tmp430
  tmp498 = -320*tmp15
  tmp499 = -24*tmp1
  tmp500 = tmp1*tmp360
  tmp501 = tmp433/tmp10
  tmp502 = 96*tmp15
  tmp503 = tmp430*tmp467
  tmp504 = tmp329 + tmp450
  tmp505 = tmp504*tmp81
  tmp506 = -80*tmp16
  tmp507 = tmp31*tmp34
  tmp508 = 768*tmp16
  tmp509 = 14 + tmp44
  tmp510 = tmp509/tmp10
  tmp511 = -30*tmp1
  tmp512 = 7*kappa
  tmp513 = kappa*tmp325
  tmp514 = -10*tmp31
  tmp515 = tmp256 + tmp325
  tmp516 = 10*kappa
  tmp517 = -6 + tmp215
  tmp518 = 2 + tmp37
  tmp519 = -12*tmp31
  tmp520 = 5 + kappa
  tmp521 = 4*tmp64
  tmp522 = 12 + tmp152
  tmp523 = 7 + tmp368
  tmp524 = (tmp31*tmp523)/tmp10
  tmp525 = 3*tmp215
  tmp526 = 14 + tmp1
  tmp527 = tmp526/tmp10
  tmp528 = tmp329 + tmp527 + tmp82
  tmp529 = -3 + tmp361
  tmp530 = -26*tmp1
  tmp531 = -320*tmp16
  tmp532 = 128*tmp380
  tmp533 = -12*tmp1
  tmp534 = 2 + tmp44
  tmp535 = tmp534/tmp10
  tmp536 = 8*tmp25*tmp430
  tmp537 = -2 + tmp296
  tmp538 = tmp1*tmp537
  tmp539 = -18*tmp1
  tmp540 = 5 + tmp215
  tmp541 = 1 + tmp360
  tmp542 = tmp1*tmp541
  tmp543 = tmp361/tmp10
  tmp544 = -28*tmp1
  tmp545 = (tmp31*tmp540)/tmp10
  tmp546 = 1 + tmp445
  tmp547 = tmp1*tmp546
  tmp548 = 160*tmp389
  tmp549 = kappa/tmp10
  tmp550 = 10 + tmp44
  tmp551 = tmp550/tmp10
  tmp552 = -4*tmp64
  MP2MPL_MP_FIN_d12d20 = -8*logmuP2*Pi*tmp10*((3 + tmp107)/tmp10 + tmp107*tmp19 + f1p*(tmp1*tmp19 + tmp43&
                          &4)) - 16*logmut*Pi*tmp10*tmp19*tmp3*tmp5 + (32*DB0F2FN*Pi*tmp10*tmp119*tmp123*tm&
                          &p142*tmp30*tmp49*tmp507)/tmp279 - 32*logbeN2*Pi*tmp27*tmp30*tmp49*tmp50*tmp507*t&
                          &mp60 + 8*D0tsLN*Pi*tmp10*tmp104*tmp34*tmp38*tmp61*tmp64 + 8*D0tsNL*Pi*tmp10*tmp1&
                          &04*tmp34*tmp38*tmp61*tmp64 + (32*DB0F2FL*Pi*tmp1*tmp10*tmp119*tmp123*tmp142*tmp3&
                          &0*tmp49*tmp66)/tmp279 - (32*logbeL2*Pi*tmp1*tmp27*tmp30*tmp49*tmp50*tmp60*tmp63*&
                          &tmp66)/tmp155 + 32*DB0tLN*Pi*tmp10*tmp34*tmp38*tmp49*tmp60*tmp63*tmp64*tmp66 + 1&
                          &6*DB0tLL*Pi*tmp10*tmp31*tmp38*tmp49*tmp60*tmp63*tmp67 + 16*DB0tNN*Pi*tmp10*tmp38&
                          &*tmp49*tmp60*tmp63*tmp71*tmp72 + D0tsLL*Pi*tmp10*tmp38*tmp470*tmp67*(-12*ss*tmp1&
                          &3 + ss*tmp147 - 4*tmp16 + tmp15*tmp246 + tmp247 + tmp15*tmp325 + tmp349 + ss*tmp&
                          &472 + (tmp24 + tmp25 + tmp68 + tmp69 + tmp70)/tmp248 + (12*tmp13 + tmp24 - (24*s&
                          &s)/tmp248 + tmp25 + tmp68 + tmp69 + tmp70)/tmp279 + tmp73 + tmp74) + DB0sFP*logm&
                          &uF2*Pi*tmp10*tmp119*tmp155*tmp19*tmp238*tmp313*tmp76 + 8*logmuF2*Pi*tmp10*(tmp12&
                          &0 + tmp121 + 6/tmp248 + tmp292/tmp248 + (6*tmp3)/tmp279 + ss*tmp329 + tmp336 + t&
                          &mp26*(tmp146 + tmp241 + ss*tmp372 + tmp45/tmp248 + tmp45/tmp279 + tmp77)) + 16*D&
                          &0IR6tsN*Pi*tmp10*tmp19*tmp27*tmp30*tmp507*(20*tmp13 + tmp21 + tmp22 + tmp78) + 1&
                          &6*C0tP2P2NP0*Pi*tmp10*tmp27*tmp30*tmp507*(-4*tmp13 + tmp21 + tmp246/tmp248 - (8*&
                          &ss)/tmp279 + tmp78) - 16*DB0sFPlogmut*Pi*ss*tmp119*tmp19*tmp3*tmp5*tmp50*(tmp21 &
                          &+ tmp22 + tmp32 + tmp33 + tmp78) - 8*D0tsNN*Pi*tmp10*tmp38*tmp71*tmp72*(36*tmp14&
                          & + (tmp24 + tmp25 + tmp320)/tmp248 - 4*tmp13*(7*ss + tmp345) + (44*tmp13 + tmp24&
                          & + tmp25 + tmp33 + tmp70)/tmp279 + tmp73 + tmp74 + tmp18*tmp78) - 16*C0tF2F2NF0*&
                          &Pi*tmp10*tmp27*tmp30*tmp49*tmp507*((tmp114 + tmp32 + tmp78)/tmp10 - (2*(tmp114 +&
                          & tmp25 + tmp79 + tmp80))/tmp279 + tmp21*(tmp2 + tmp81)) - 16*C0tF2F2NFL*Pi*tmp10&
                          &*tmp34*tmp38*tmp49*tmp61*tmp64*(tmp143*tmp21 + (tmp114 + 1/(tmp10*tmp155) + tmp2&
                          &3 + tmp24 + tmp32)/tmp10 + (2*(tmp145 + (tmp1 + tmp2)/tmp10 + tmp340*tmp8 + (2*(&
                          &tmp340 + tmp81))/tmp248))/tmp279) + logmuL2*Pi*tmp10*tmp342*tmp38*tmp61*(tmp13*t&
                          &mp325 + tmp39 + tmp40 + tmp41 + tmp42 + (tmp43 + tmp1*(tmp153 + tmp37 + tmp81))/&
                          &tmp248) + DB0sFP*logF2P2*Pi*tmp10*tmp119*tmp155*tmp19*tmp238*tmp76*tmp82 - 16*lo&
                          &gmuN2*Pi*tmp10*tmp38*tmp507*(tmp31/tmp279 + tmp13*tmp355 + tmp39 + tmp40 + tmp41&
                          & + ss*tmp414 + tmp42 + (tmp43 + tmp1*(tmp149 + tmp152 + tmp82))/tmp248) + D0IR6t&
                          &sL*Pi*tmp10*tmp19*tmp27*tmp30*tmp342*tmp66*(tmp21 + tmp22 + tmp31 + tmp32 + tmp3&
                          &3 + tmp78 + tmp83) + C0sF2P2FLP*Pi*tmp10*tmp119*tmp30*tmp303*tmp66*(-(tmp20*(tmp&
                          &15*tmp324 + (tmp145 + ss*(tmp243 + tmp325 + tmp326) + tmp327)/tmp248 + tmp328)) &
                          &+ tmp12*(tmp324/tmp248 + ss*(tmp274 + tmp292 + tmp374) + tmp417 + tmp70) + tmp11&
                          &*(tmp107 + tmp199 + tmp246 + tmp77) + (tmp328 + ss*(-12*tmp15 + tmp327 + ss*(tmp&
                          &329 + tmp330 + tmp394)) + (tmp101 + ss*(tmp192 + tmp331 + tmp332) + tmp79)/tmp24&
                          &8)/tmp279 + f1p*(tmp11*tmp333 - tmp12*(tmp135 + tmp333/tmp248 + tmp338 + tmp41) &
                          &- (tmp335 + (2*(tmp334 + tmp336 + tmp337))/tmp248 + tmp18*(tmp135 + tmp338 + tmp&
                          &41))/tmp279 + tmp20*(tmp15*tmp333 + tmp335 - (2*(tmp334 + tmp337 + tmp84))/tmp24&
                          &8))) - 16*C0sF2P2FNP*Pi*tmp10*tmp119*tmp30*tmp507*(-(tmp20*(tmp15*tmp244 + tmp24&
                          &5 - tmp13*(-18/tmp10 + tmp199 + tmp258) + (ss*(tmp243 + tmp326))/tmp248)) + tmp1&
                          &1*(tmp199 + tmp246 - 8/tmp248 + tmp77) + f1p*(tmp11*(tmp113 + tmp17) - tmp12*(3*&
                          &ss*tmp113 + (7*(tmp112 + tmp2))/tmp248 + tmp32) + tmp20*(tmp113*tmp15 + tmp247 +&
                          & (ss*tmp246)/tmp248 + tmp13*(tmp120 + tmp371)) + (-((10*ss + 17/tmp10)*tmp13) - &
                          &4*tmp14 + 3*tmp113*tmp15 + (tmp112*(tmp341 + tmp81))/tmp248)/tmp279) + (tmp245 +&
                          & tmp13*(34/tmp10 + tmp242 + tmp281) + 3*tmp15*(tmp199 + tmp246 + tmp77) + (tmp8*&
                          &(-10/tmp10 + tmp199 + tmp82))/tmp248)/tmp279 + tmp12*(3*ss*tmp244 + (28*ss - 14/&
                          &tmp10 + tmp242)/tmp248 + tmp85)) + 8*C0tF2F2NFN*Pi*tmp10*tmp38*tmp49*tmp71*tmp72&
                          &*(tmp21*tmp312 + ((-8*tmp250)/tmp248 + tmp32 + tmp78)/tmp10 + (2*(tmp145 + tmp14&
                          &7 + ss*tmp326 + tmp43/tmp248 + tmp85))/tmp279) + C0tF2F2LFL*Pi*tmp10*tmp38*tmp48&
                          &7*tmp49*tmp67*((tmp109 + tmp23 + tmp24 + tmp32 + tmp33)/tmp10 - 4*tmp12*(tmp108 &
                          &+ tmp17 + tmp77) - (2*(tmp109 + tmp111 - (4*tmp110)/tmp248 + tmp79 + tmp85))/tmp&
                          &279) - 8*C0tF2F20F0*Pi*tmp10*tmp3*tmp49*tmp5*(tmp21*(1/tmp10 + tmp17 + tmp77) - &
                          &(tmp32 + tmp33 + tmp78)/tmp10 + (2*(tmp25 + tmp56 + tmp79 + tmp80 + tmp85))/tmp2&
                          &79) + C0tF2F2LF0*Pi*tmp10*tmp27*tmp30*tmp342*tmp49*tmp61*(-4*tmp12*(tmp144 + tmp&
                          &17 + tmp77) - (2*(tmp25 + tmp79 + tmp80 + tmp83 + tmp84 + tmp85 - (2*tmp86)/tmp2&
                          &48))/tmp279 + (tmp32 + tmp33 + tmp88)/tmp10) + C0tP2P2LP0*Pi*tmp10*tmp27*tmp30*t&
                          &mp342*tmp61*tmp76*(tmp143*tmp32 - (2*(tmp25 + tmp79 + tmp80 + tmp83 + tmp84))/tm&
                          &p248 - (4*(tmp24 + tmp32 - tmp86/tmp248))/tmp279 + tmp87 + tmp89) - 8*logF2t*Pi*&
                          &tmp3*tmp49*tmp5*tmp9 - 8*logP2t*Pi*tmp3*tmp5*tmp76*tmp9 + 16*C0IR6sFP*Pi*tmp10*t&
                          &mp19*(tmp243 + tmp387 + tmp41 + f1p*(tmp242 + tmp256 + tmp112*tmp46) + tmp369*tm&
                          &p8 + tmp90) + (16*DB0sFP*Pi*tmp10*(-3*tmp11 + 9*ss*tmp12 + f1p*tmp12*tmp120 + f1&
                          &p*tmp124 + f1p*tmp125 + 9*ss*tmp13 + f1p*tmp120*tmp13 - 3*tmp14 + f1p*tmp12*tmp1&
                          &46 + f1p*tmp13*tmp146 + tmp121*tmp15 + 3*tmp16 + f1p*tmp15*tmp246 + (3*tmp12)/tm&
                          &p248 + (f1p*tmp135)/tmp248 - (9*tmp15)/tmp248 + (3*tmp13)/tmp279 + (f1p*tmp135)/&
                          &tmp279 - (9*tmp15)/tmp279 + tmp126/(tmp248*tmp279) + (f1p*tmp326)/(tmp248*tmp279&
                          &) + tmp374/(tmp248*tmp279) + tmp16*tmp4 + (tmp12*tmp4)/tmp248 + (tmp13*tmp4)/tmp&
                          &279 + (f1p*tmp77)/(tmp248*tmp279) + tmp12*tmp90 + tmp13*tmp90))/(tmp12 + tmp13 +&
                          & tmp15 - 2/(tmp248*tmp279) + tmp8/tmp248 + tmp8/tmp279) + (2*logF2P2*Pi*tmp10*tm&
                          &p248*tmp279*tmp280*tmp38*((tmp26*((-4*((-8 + tmp1)*tmp18 + tmp246 + 8/tmp248)*tm&
                          &p311)/tmp30 + (8*tmp339)/tmp30 + tmp20*tmp21*(32*tmp265 + tmp14*(tmp15*tmp192 + &
                          &ss*(-56/tmp10 + 7*tmp31 + tmp342) + tmp351) + ss*tmp13*(tmp23*(16 + tmp295) + (1&
                          &9 - 20*kappa)*ss*tmp31 + (7 + tmp188)*tmp336 + ss*tmp345 - tmp387) + tmp1*tmp16*&
                          &(tmp199 + ss*tmp343 + tmp404) - 4*tmp255*(ss*tmp294 + 2*tmp477) + (tmp15*(tmp336&
                          & + tmp320*tmp343 + ss*(tmp303 + (1 - 12*kappa)*tmp31 + tmp344) + tmp246*tmp507))&
                          &/tmp248) + ss*tmp31*tmp340*tmp411*tmp66 - (2*tmp115*(16*tmp265 - 4*tmp255*(tmp1 &
                          &+ tmp112*tmp263 + tmp326) + tmp15*tmp28*(ss*tmp199 + tmp31*tmp348*tmp361 + ss*tm&
                          &p246*tmp458 + tmp334*tmp46) + (ss*(-(tmp340*tmp387) + tmp132*(tmp355 + tmp31*(-9&
                          & + tmp384) + tmp43) + 8*tmp16*tmp46 + tmp112*((tmp329 + tmp428)/tmp10 + (8 + kap&
                          &pa)*tmp64)))/tmp248 + tmp125*(ss*(tmp353 + tmp354 + tmp375) + tmp41 + tmp346*tmp&
                          &70) + tmp13*tmp18*(ss*(24*tmp1 - 48/tmp10 + (-26 + 40*kappa)*tmp31) + tmp336*(-6&
                          & + tmp410) + tmp447*tmp64 + (8 + tmp44)*tmp79)))/tmp279 + 2*tmp249*(80*tmp14 - 4&
                          &*tmp13*(tmp126*tmp278 + tmp357) + ss*tmp1*(10*ss*tmp154 + tmp242 + tmp146*(tmp43&
                          &0 + tmp65)) + (-40*tmp15*tmp154 + 20*tmp241 + (tmp344 + tmp358 + tmp492)*tmp8)/t&
                          &mp248) - 2*tmp11*(80*tmp265 - 4*tmp14*(ss*tmp28*tmp346 + tmp347 + tmp459 + tmp23&
                          &*tmp46) + tmp1*tmp334*(-10*tmp15*tmp3 + tmp199*tmp348 + ss*tmp146*(2 + tmp525)) &
                          &+ tmp13*(tmp349 + tmp362 + ss*(tmp107 + tmp31*(9 + tmp373) + tmp43) + tmp23*tmp4&
                          &5 + tmp64)*tmp77 - 4*tmp255*(5*tmp393 + tmp346*tmp81) + (ss*(80*tmp16*tmp3 + tmp&
                          &340*tmp387 + tmp320*(-3*(-5 + 6*kappa)*tmp31 + tmp325 + tmp43) + (tmp241*(2 + tm&
                          &p367) + tmp552)*tmp81))/tmp248) + tmp254*(tmp239*(ss*tmp1 + tmp419) + ss*tmp1*(-&
                          &40*tmp1*tmp15 + tmp199*tmp340 + (tmp199 + (-1 + tmp215)*tmp332)*tmp81) + (tmp81*&
                          &((-4 + tmp266)*tmp336 + tmp363 + tmp34*tmp64 + (tmp332 + tmp31*(-7 + tmp360))*tm&
                          &p81))/tmp248 + (-3*ss*tmp31 + tmp351 + tmp352 + tmp359)*tmp85)))/(tmp248*tmp30) &
                          &+ 2*(6*(tmp1*Sqrt(1/tmp248) - 4*(1/tmp248)**1.5)**2*tmp339 + (ss*tmp31*tmp421*tm&
                          &p426*tmp66)/tmp248 + tmp12*(3*tmp16*tmp25*tmp365*tmp366 + 384*tmp392 - 96*tmp380&
                          &*(tmp105 + tmp341 + tmp112*tmp430) + (tmp15*tmp31*(tmp132*(tmp331 + (9 + tmp34*t&
                          &mp353)/tmp10) + 4*tmp16*tmp385 + tmp401 + kappa*ss*tmp471*tmp507))/tmp248 + 8*tm&
                          &p271*(ss*(72*tmp1 - 144/tmp10 + 20*tmp31) + 32*tmp15*tmp370 + tmp375 + tmp511/tm&
                          &p10) + ss*tmp107*tmp13*(-4*tmp16*(tmp146*(9 + tmp364) + tmp28*(9 + tmp1*(2 + tmp&
                          &386))) + tmp390 + tmp334*(tmp387 + tmp388 + tmp1*tmp246*tmp405) + tmp112*tmp406*&
                          &tmp455*tmp64 + tmp25*tmp365*tmp72) + ss*tmp247*(tmp132*(tmp1*(6 + tmp215)*tmp332&
                          & + tmp31*(-6 + tmp1*(-9 + tmp361))) + tmp390 - 8*tmp16*(tmp371 + tmp107*(9 + tmp&
                          &1*tmp397)) + ss*(tmp31*tmp371 + tmp391 + tmp399) - tmp259*tmp455*tmp72) + tmp255&
                          &*((9 + tmp188)*tmp349 + 96*tmp16*tmp369 - 32*tmp15*(tmp121 + tmp31*tmp350 + tmp3&
                          &72) + tmp381 + tmp391 + tmp258*(tmp382 + tmp414 + (1 + tmp446)*tmp64))*tmp77 + 2&
                          &*tmp265*(32*tmp15*((2 - 5*kappa)*tmp31 + tmp329 + tmp371) + tmp383 + 192*tmp16*t&
                          &mp46 + (((-18 + tmp215)*tmp353)/tmp10 + tmp31*(18 + tmp1*(7 + tmp384)))*tmp8)) +&
                          & (2*tmp249*(60*tmp14 + ss*tmp1*((9 - 2*tmp215)/tmp10 + tmp242 + (-3 + tmp105)*tm&
                          &p251) + ((60 - 40*tmp1)*tmp15 + 15*tmp241 + (tmp299 + tmp358 + tmp395)*tmp8)/tmp&
                          &248 - 3*tmp13*(tmp357 + (-3 + tmp1)*tmp82)))/(tmp248*tmp30) + tmp11*(-480*tmp380&
                          & + 2*tmp265*(240*tmp241 - 15*tmp31 + 32*tmp15*tmp369 + (tmp28*tmp370 + tmp371)*t&
                          &mp440) + (tmp15*(tmp120*(1/tmp10 + tmp336*tmp34) + tmp31*(tmp107 + tmp2 + 3*tmp2&
                          &5*tmp34)*tmp361 + 5*tmp15*tmp376)*tmp472)/tmp248 + tmp15*tmp365*tmp366*tmp57 + t&
                          &mp1*tmp112*tmp13*(-12*tmp15*(tmp28 + (5 - 6*kappa)*tmp31 + (1 + tmp215)*tmp332) &
                          &+ tmp377 + tmp387*(tmp340 - 2*tmp455) + tmp112*(tmp378 + tmp388 + (tmp379*tmp472&
                          &)/tmp10 - 8*tmp150*tmp64)) + tmp14*((-9*tmp31)/tmp10 + tmp106*tmp353*(3 + tmp367&
                          &) - 48*tmp15*(tmp144 + tmp31*(2 + tmp368)) + tmp377 + tmp381 + ss*tmp414*(3 + tm&
                          &p105*(7 + tmp516)) + (tmp150*tmp521)/tmp10 + 2*tmp150*tmp72)*tmp77 + 4*tmp255*(9&
                          &6*tmp16*tmp3 - (15*tmp31)/tmp10 + tmp112*(-36*tmp241 + tmp375 + tmp487/tmp10 + 5&
                          &*tmp64) + (tmp372 + tmp31*(8 + tmp373) + tmp374)*tmp79) + 16*tmp271*(15*(tmp1 + &
                          &tmp326) + tmp370*tmp82)) + tmp254*(ss*tmp25*tmp365*tmp366 + 32*tmp255*(tmp359 + &
                          &tmp409/tmp10 + ss*tmp413 + ss*tmp453) + tmp247*(160*tmp1*tmp16 + tmp319*(tmp121 &
                          &+ tmp31*(-8 + tmp360)) + tmp383 + tmp112*(tmp1*(-6 + tmp266)*tmp332 + (1 + tmp36&
                          &1)*tmp64)) + ss*tmp13*tmp325*(tmp362 + tmp1*tmp415 + tmp453/tmp10 + tmp31*tmp462&
                          & - tmp64 + ((7 - 4*kappa)*tmp31 + (-6 + tmp364)/tmp10)*tmp77) + (ss*tmp31*(tmp19&
                          &9*(tmp246 + tmp28 + 4*tmp25*tmp34) + tmp363 + (tmp199 + (-3 + tmp364)/tmp10)*tmp&
                          &77))/tmp248 + 64*tmp265*(15/tmp10 + tmp83)) - (2*tmp311*(tmp464 + tmp433*tmp8 + &
                          &tmp90))/(tmp248*tmp38) - (tmp20*(tmp147*tmp16*tmp365*tmp366 + 96*tmp392 + tmp271&
                          &*(ss*(240*tmp1 - 384/tmp10 + 64*tmp31) + (15 + tmp192)*tmp321 + tmp292*tmp393) +&
                          & (tmp15*tmp414*(tmp16*(6 + tmp192) + tmp401 + tmp15*(tmp243 + tmp404) + kappa*ss&
                          &*(tmp25*tmp34 + tmp348)*tmp428))/tmp248 - 16*tmp380*(ss*(30 + tmp192) + tmp394 +&
                          & tmp44) + tmp1*tmp13*tmp334*(8*tmp16*tmp369 + tmp387*(2*tmp25*tmp406 + tmp44) + &
                          &tmp15*(15*tmp1 + tmp345*tmp405 + tmp396*tmp453) + ss*(tmp336*(-6 + tmp408) + tmp&
                          &455*tmp487 + (16 + tmp407)*tmp64)) + tmp112*tmp255*(16*tmp16*(15 + tmp353) + tmp&
                          &399 - 16*tmp15*(tmp28*(15 + tmp1*(1 + tmp386)) + tmp413) + ((6 + tmp408)*tmp472)&
                          &/tmp10 + tmp112*(-72*tmp241 + tmp400 + tmp332*tmp507 + 2*(7 + tmp445)*tmp64) - t&
                          &mp397*tmp72) + tmp14*(16*tmp369*tmp389 + tmp15*(30*tmp31 + (tmp355*(12 + tmp410)&
                          &)/tmp10 + (56 + 44*kappa)*tmp64) - 8*tmp16*(tmp374 + tmp409 + (-7 + tmp361)*tmp6&
                          &8) + kappa*(tmp246 + tmp28 + tmp454)*tmp72 + ss*((-9 + tmp215)*tmp349 + 16*tmp45&
                          &5*tmp64 + 3*(4 + tmp360)*tmp72))*tmp8 - 2*tmp265*(96*(5 + tmp105)*tmp16 - 16*tmp&
                          &15*(tmp31*(3 + tmp373) + tmp395 + tmp409) + ss*(-96*tmp241 + tmp400 + (tmp396*tm&
                          &p487)/tmp10 + 2*(-5 + tmp361)*tmp64) + tmp31*tmp90)))/tmp279)))/ss + C0tP2P2LPL*&
                          &Pi*tmp10*tmp38*tmp487*tmp67*tmp76*((tmp109 + tmp23 + tmp24)/tmp10 - (2*(tmp109 +&
                          & tmp111 + tmp79))/tmp248 + tmp32*(tmp105 + tmp2 + tmp81) + tmp87 - (8*(tmp106 - &
                          &tmp110/tmp248 + tmp91))/tmp279) - 8*C0tP2P20P0*Pi*tmp10*tmp3*tmp5*tmp76*(tmp21/t&
                          &mp76 + tmp32*(1/tmp10 + tmp77) - tmp78/tmp10 + (2*(tmp25 + tmp79 + tmp80))/tmp24&
                          &8 + (8*(tmp106 + tmp77/tmp248 + tmp91))/tmp279) + (32*DB0P2LP*Pi*tmp1*tmp10*tmp1&
                          &19*tmp123*tmp30*tmp66*tmp76*(tmp124 + (-2*tmp13 + tmp136 - (2*tmp113)/tmp248)/tm&
                          &p279 - tmp12*(tmp127 + tmp7) + tmp20*(tmp106 + tmp147 + tmp334 + (tmp121 + tmp77&
                          &)/tmp248 + tmp91)))/tmp248 + 8*C0tP2P2NPN*Pi*tmp10*tmp38*tmp71*tmp72*tmp76*(tmp2&
                          &39 + tmp78/tmp10 - (2*(tmp316 + tmp79 + tmp80))/tmp248 + tmp87 - 4*tmp13*(tmp81 &
                          &+ tmp90) + (8*(tmp122 + tmp81/tmp248 + tmp91))/tmp279) + logP2L2*Pi*tmp10*tmp107&
                          &*tmp248*tmp279*tmp280*tmp38*tmp66*tmp76*((f1p*(8*(tmp108 + tmp17)*tmp311 - 2*tmp&
                          &249*(48*tmp13 + tmp121*(tmp146 + tmp28) + tmp108*tmp281 + (4*tmp484)/tmp248) + (&
                          &tmp1*tmp340*tmp411)/tmp76 - 4*tmp12*tmp20*(24*tmp255 - tmp13*(tmp108*tmp112 + 56&
                          &*tmp15 + tmp121*(tmp291 + tmp332)) + ss*(10*tmp108*tmp15 + tmp105*tmp25 + ss*tmp&
                          &121*(tmp332 + tmp372)) + ((32*tmp1 - 30/tmp10)*tmp15 + tmp106*tmp355 + tmp485 + &
                          &tmp489)/tmp248 - 2*tmp14*(tmp353 + tmp466 + tmp81)) + tmp124*(32*tmp255 + tmp112&
                          &*(-20*tmp108*tmp15 + tmp108*tmp241 + ss*tmp394*tmp418) + tmp32*(-9*tmp25 + tmp31&
                          & + ss*tmp325 + tmp416 + tmp417 + ss*tmp419) + tmp245*tmp469 + (-160*tmp16 + tmp4&
                          &16*tmp418 + tmp481 + (tmp241 + tmp57 + tmp68)*tmp82)/tmp248) + tmp254*(tmp241*(t&
                          &mp1 + tmp394) + ss*tmp332*(tmp412 + tmp413) + tmp108*tmp415 + tmp427 + tmp476*tm&
                          &p55 + (4*(tmp414 + tmp415 + tmp471 + (tmp146 + tmp291)*tmp77 + tmp84))/tmp248) +&
                          & (2*tmp115*(-12*tmp14*tmp143 + 16*tmp255 + ss*(tmp1*tmp108*tmp246 + tmp108*tmp32&
                          &0 + tmp106*(tmp291 + tmp394)) + tmp13*(-28*tmp106 - 19*tmp241 + tmp1*tmp440 + tm&
                          &p453 + tmp467 + tmp473) + (-16*tmp16 + tmp23*(tmp315 + tmp412) + tmp241*tmp482 +&
                          & tmp112*(tmp420 + tmp470 + tmp84))/tmp248))/tmp279))/(tmp248*tmp30) - 2*(-(tmp24&
                          &9*(tmp429 + tmp32*(tmp241*(-8 + tmp1*(-7 + tmp384)) + tmp281*tmp431 + tmp438 + t&
                          &mp490) + (tmp1*(tmp281*tmp437 + (tmp292 + tmp436 - 9*tmp507)/tmp10))/tmp248 + tm&
                          &p240*(tmp298 + tmp444 + tmp535) - 10*tmp106*tmp34*tmp64)) + (tmp1*tmp421*tmp426)&
                          &/(tmp248*tmp76) + tmp11*(256*tmp271 + 8*tmp255*(tmp487 + tmp502 + tmp503 + tmp50&
                          &5 + (tmp31 + tmp331 + tmp511)/tmp10) + 32*tmp265*tmp528 + (ss*tmp107*(ss*tmp121*&
                          &(tmp242 + tmp31 + tmp353 + tmp436) + 20*tmp15*tmp437 + tmp336*(tmp108 + tmp451 +&
                          & tmp372*tmp549)))/tmp248 + tmp106*(20*tmp15*tmp34 - 3*tmp455)*tmp64 - 2*tmp13*(8&
                          &0*tmp16*tmp431 + (tmp31*(tmp454 + tmp480))/tmp10 + (tmp1*tmp434 + tmp438 + tmp45&
                          &3)*tmp70 + (tmp1*(16 + (3 + 11*kappa)*tmp1)*tmp25 + tmp349*tmp517 - 8*tmp64)*tmp&
                          &8) - 2*tmp14*(640*tmp16 - 48*tmp15*(tmp329 + tmp452) + tmp28*(tmp241*(12 + tmp29&
                          &5*tmp34) + tmp25*(76 + tmp1*(9 + tmp360)) + tmp453) + (-20*tmp31 + tmp448 + tmp2&
                          &41*tmp518)*tmp81)) + tmp254*(tmp432 + 32*tmp255*(tmp313 + tmp355 + tmp501) + (tm&
                          &p1*(tmp323*tmp437 + ss*tmp332*(tmp295 + tmp436 - 5*tmp507) + tmp241*(tmp28 + tmp&
                          &462 + (6 + tmp513)/tmp10)))/tmp248 + ((-20*tmp15*tmp34 + tmp455)*tmp64)/tmp10 + &
                          &tmp32*((6 + tmp152)*tmp25*tmp372 + tmp323*tmp431 + tmp64 + (tmp34*tmp552 + tmp68&
                          &)/tmp10 + (5*tmp31 + tmp438 + tmp241*(-5 + tmp1*tmp529))*tmp81) + tmp245*(tmp241&
                          &*(4 + tmp1*(-4 + tmp296)) + tmp300 + tmp428 + tmp448 + (tmp412 + tmp434)*tmp82))&
                          & + 2*tmp311*(tmp427 + (2*(tmp1*tmp269 + tmp428))/tmp248 - (tmp34*tmp64)/tmp10 + &
                          &tmp431*tmp85) + (tmp20*((ss*tmp1*(4*tmp16*tmp437 + ss*tmp1*tmp332*(tmp105 - (1 +&
                          & tmp37)/tmp10 + tmp455) + tmp457 + (tmp15*(tmp436 + tmp291*tmp458))/tmp10))/tmp2&
                          &48 - 16*tmp271*(tmp353 + tmp440 + tmp491) + 4*tmp265*(120*ss*tmp1 - (44 + tmp1)*&
                          &tmp241 + tmp441 - 16*tmp106*tmp456 + tmp472 + tmp497) + tmp532 + (tmp15*(tmp132*&
                          &tmp34 + tmp455)*tmp64)/tmp10 + ss*tmp13*(16*tmp16*tmp431 + ((tmp44 + tmp146*(-4 &
                          &+ tmp461) + tmp462)*tmp472)/tmp10 + tmp23*(-18*tmp31 + tmp336 + tmp438 + tmp524)&
                          & + ss*(tmp25*(52*tmp1 + (9 - 19*kappa)*tmp31) - 32*tmp64 + (tmp463 - 3*tmp34*tmp&
                          &64)/tmp10)) + tmp255*(-512*tmp16 + ((46 + tmp215)*tmp31)/tmp10 + tmp25*(60*tmp1 &
                          &+ (3 - 17*kappa)*tmp31) + (tmp346/tmp10 + tmp412)*tmp502 + 8*tmp64 + (54*tmp241 &
                          &+ (tmp396*tmp414)/tmp10 + tmp460 + tmp463)*tmp81) + tmp14*(-32*tmp16*(-13*tmp1 +&
                          & tmp146*tmp370) + 128*tmp389 + tmp349*(tmp28 + tmp394 + tmp454) + ss*(tmp1*(-100&
                          & + (-9 + 35*kappa)*tmp1)*tmp25 + 24*tmp64 + (tmp514 - tmp34*tmp64)/tmp10) + tmp3&
                          &20*(-62*tmp31 + tmp459 + tmp460 + tmp31*tmp350*tmp90))))/tmp279 + tmp12*(-384*tm&
                          &p380 + 16*tmp271*(tmp439 + tmp440 + tmp494) + 4*tmp265*(tmp241*(82 + tmp285) - 1&
                          &6*tmp31 + tmp441 + tmp313*(tmp412 + tmp452) + tmp536) + (ss*tmp1*(20*tmp16*tmp43&
                          &7 + tmp15*tmp332*(tmp299 + tmp367/tmp10 + tmp442 + tmp443) + tmp457 + ss*tmp192*&
                          &tmp25*(-2 + kappa*tmp372 + 3*tmp549)))/tmp248 + tmp247*(tmp132*(-14*tmp31 + (tmp&
                          &414*tmp447)/tmp10 + tmp448 + tmp492/tmp10) + ss*tmp241*(28/tmp10 + tmp295 + tmp5&
                          &07 + tmp241*(3 + tmp512)) + tmp548 + tmp25*tmp31*(6 + tmp496 - tmp549) - 16*tmp1&
                          &6*(-19*tmp1 + tmp551)) + (tmp15*(-10*tmp15*tmp34 + tmp451)*tmp64)/tmp10 - 2*tmp2&
                          &55*(tmp241*(30*tmp1 + 72/tmp10 + tmp241*(6 + tmp373) + tmp443) + tmp508 - 16*tmp&
                          &15*(tmp444 + tmp510) + ((13 + tmp1)*tmp241 + tmp358 + tmp438)*tmp82) + (-(tmp148&
                          &*tmp387) + 40*tmp389*tmp431 + tmp1*tmp15*tmp246*(tmp1 + (-20 + tmp1*(-3 + tmp445&
                          &))/tmp10) + 8*tmp16*(-13*tmp31 + tmp438 + (tmp1 + tmp31*(5 + tmp446))/tmp10) + s&
                          &s*tmp25*tmp472*(4 + tmp461 + tmp543))*tmp91))) + logF2L2*Pi*tmp10*tmp105*tmp248*&
                          &tmp279*tmp280*tmp38*tmp49*tmp66*((f1p*(-32*tmp339 - tmp241*tmp340*tmp411 - 2*tmp&
                          &249*(-52*tmp106 + tmp300 + tmp1*tmp304 + tmp412/tmp10 + tmp465 + (4*(tmp313 + tm&
                          &p353 + tmp466))/tmp248 + tmp467 + tmp68) - (2*tmp115*(tmp108*tmp247 - tmp13*(tmp&
                          &108*tmp258 + tmp483) + tmp18*(tmp108*tmp23 + tmp336*tmp479 + ss*tmp483) + (-(tmp&
                          &241*tmp482) + tmp112*tmp483 + tmp108*tmp70)/tmp248))/tmp279 - 4*tmp12*(8*tmp265 &
                          &- 2*tmp255*tmp484 + tmp15*(-8*tmp16 + tmp334*(-9*tmp1 + tmp468) + tmp485 + ss*(t&
                          &mp471 + tmp487 + tmp488)) + (ss*((-40*tmp15)/tmp155 + tmp192*tmp25 + tmp489 + tm&
                          &p18*(tmp471 + tmp488 + tmp490)))/tmp248 + tmp14*(tmp415 - 8*ss*(1/tmp10 + tmp44)&
                          & + tmp471 + tmp488 + tmp68) + tmp13*(tmp485 + ss*(-12*tmp25 + tmp428 + tmp486) +&
                          & tmp506 + (tmp121 + tmp28)*tmp70)) - tmp254*((-10*tmp1 + 11/tmp10)*tmp319 + tmp2&
                          &41*(tmp28 + tmp374) + tmp427 + tmp531 + tmp469*tmp55 + (tmp470 + tmp471 + tmp474&
                          &)*tmp81 + (4*(tmp471 + tmp472 + tmp473 + tmp474 + tmp418*tmp82))/tmp248) + tmp12&
                          &4*(48*tmp255 - 8*tmp14*tmp476 + tmp112*((36*tmp15)/tmp155 + tmp241*(tmp107 + tmp&
                          &315) + tmp478 + ss*tmp374*tmp479) + (192*tmp16 + tmp145*(tmp297 + tmp468) + 8*tm&
                          &p106*tmp479 + tmp481)/tmp248 + tmp13*(-96*tmp15 + tmp413*tmp479 + tmp477*tmp82))&
                          & + 8*tmp311*(tmp281 + tmp44 + tmp464 + tmp90)))/(tmp248*tmp30) + 2*(32*(tmp28/tm&
                          &p248 + tmp32)*tmp339 - (4*tmp311*(96*tmp13 + (4*(tmp298 + tmp491))/tmp248 + tmp1&
                          &*(tmp322 + tmp329 + tmp374 + tmp1*tmp549)))/tmp248 + (tmp20*(-(tmp148*tmp15*tmp3&
                          &87) + 16*tmp271*tmp515 - 4*tmp265*(tmp313*tmp515 + (tmp192 + 2*tmp452 + tmp31*(-&
                          &9 + tmp516))/tmp10) - tmp14*(tmp349*(tmp28 + tmp246*(3 + tmp37) + tmp454) + 64*t&
                          &mp16*tmp515 + tmp23*(tmp460 + tmp519 + tmp241*(18 + tmp1*tmp520)) + 3*ss*(((10 +&
                          & tmp215)*tmp31)/tmp10 + tmp521 + tmp1*tmp25*tmp522)) + ss*tmp13*(16*tmp16*tmp515&
                          & + tmp23*(tmp438 + tmp459 + tmp519 + tmp524) + tmp349*(tmp44 + tmp462 + tmp325*t&
                          &mp549) + ss*((28 + tmp152)*tmp25*tmp44 + (tmp31*(38 + tmp525))/tmp10 + 12*tmp64)&
                          &) + (ss*tmp28*(tmp457 + 4*tmp16*(tmp325 + tmp518/tmp10) + tmp15*(tmp428 + tmp241&
                          &*tmp458 + tmp25*tmp522) + ss*tmp1*tmp332*(tmp105 + tmp455 + (3 + tmp65)/tmp10)))&
                          &/tmp248 + tmp255*(tmp1*(-36 + tmp152)*tmp25 + tmp502*tmp515 + (tmp31*tmp517)/tmp&
                          &10 + tmp521 + ((14 + (-11 + 15*kappa)*tmp1)*tmp241 + tmp453 + tmp460)*tmp81)))/t&
                          &mp279 + tmp249*(256*tmp255 + tmp240*(tmp440 + tmp492 + tmp494) + tmp32*(64*ss*tm&
                          &p1 + ss*(26 + tmp291)*tmp326 + tmp495 + tmp241*(tmp263 + tmp496) + tmp497 + tmp5&
                          &19) + tmp455*tmp521 + (tmp1*((14 + tmp215)*tmp241 + (12 + (1 + 17*kappa)*tmp1)*t&
                          &mp25 + tmp453 + tmp498 + (tmp499 + (26 + tmp500)/tmp10)*tmp81))/tmp248) + (tmp24&
                          &1*tmp421*(tmp214 + tmp14*tmp34*tmp353 + tmp425 + (tmp28*tmp340 + tmp34*tmp359 + &
                          &tmp340*tmp77)/tmp248 + tmp32*(tmp146 + tmp1*(1 + kappa*tmp77 + tmp81))))/tmp248 &
                          &+ tmp254*(tmp432 + 32*tmp255*tmp528 - 4*tmp13*(tmp290 + ((12 + tmp215)*tmp31)/tm&
                          &p10 + tmp112*((-10 + tmp152)*tmp241 - 4*tmp25*tmp430 + tmp487) + tmp145*((22 + t&
                          &mp291)/tmp10 + tmp530) + tmp1*tmp25*(6 + tmp542) - 2*tmp64) - (1/tmp10 + tmp258)&
                          &*tmp455*tmp64 + (tmp28*(tmp241*(tmp28 + tmp374 + tmp462) + tmp531 + (tmp444 + (2&
                          &2 + tmp500)/tmp10)*tmp79 + ((11 + tmp215)*tmp241 + tmp470 + tmp25*(12 + tmp547))&
                          &*tmp81))/tmp248 + tmp245*(tmp502 + (tmp353 + 4*tmp452 + tmp31*tmp529)/tmp10 + (t&
                          &mp297 + tmp452)*tmp82)) + tmp11*(-384*tmp271 + 32*tmp265*(tmp313 + tmp372 + tmp5&
                          &01) + 8*tmp255*(6*tmp31 + ((7 - 8*kappa)*tmp31)/tmp10 + tmp502 + tmp503 + tmp505&
                          & + tmp539/tmp10) + (tmp1*tmp112*(tmp336*(tmp107 + tmp451 + (5 + tmp461)/tmp10) +&
                          & tmp506 + ss*tmp121*((1 + 9*kappa)*tmp241 + tmp353 + tmp413 + tmp507) + tmp23*((&
                          &18 + tmp500)/tmp10 + tmp539)))/tmp248 + 3*ss*tmp252*tmp455*tmp64 - 2*tmp14*(tmp5&
                          &08 - 16*tmp15*(tmp303 + tmp510) + tmp241*((4 + tmp1*(-3 + tmp360))/tmp10 - 3*tmp&
                          &507 + tmp544) + (tmp448 + tmp472 + tmp241*(26 + tmp538))*tmp81) + (320*tmp389 - &
                          &16*tmp16*((18 + tmp291)/tmp10 + tmp511) + ss*tmp1*tmp246*(tmp242 + tmp303 + tmp3&
                          &1 + (8 + tmp1*(-1 + tmp512))/tmp10) + (tmp31*(tmp28 + tmp454 + (10 + tmp513)/tmp&
                          &10))/tmp10 + tmp320*(tmp460 + tmp241*(38 + tmp1 + tmp496) + tmp514))*tmp91) + tm&
                          &p12*(tmp532 - 16*tmp271*(tmp298 + tmp533 + tmp535) + (ss*tmp1*(-7*tmp148*tmp199 &
                          &+ 32*tmp389 - 4*tmp16*((14 + tmp500)/tmp10 + tmp539) + tmp320*(tmp487 + tmp241*t&
                          &mp540 + tmp25*(12 + tmp542)) + ss*tmp25*tmp533*(2 + tmp496 + tmp549)))/tmp248 + &
                          &tmp247*(tmp132*(tmp241*(28 + tmp1*tmp296) + 22*tmp31 + tmp448) + tmp548 + tmp25*&
                          &tmp31*(-6 + tmp549) - 16*tmp16*(tmp409 + tmp551) + ss*(tmp25*(28*tmp1 + (1 - 11*&
                          &kappa)*tmp31) + tmp545 + tmp552)) - tmp15*tmp455*tmp64*(tmp121 + tmp81) - 4*tmp2&
                          &55*(tmp290 + tmp145*(tmp303 + 3*tmp452) + tmp25*(tmp31*(1 + tmp368) + tmp533) + &
                          &tmp545 + 2*tmp64 + (tmp438 + tmp514 + tmp241*(3 + tmp538))*tmp81) + 4*tmp265*(-6&
                          &*tmp31 + tmp241*(22 + tmp1*(-16 + tmp407)) + tmp495 + tmp536 + (tmp412 + 2*tmp43&
                          &4)*tmp82) + (-64*ss**5 + tmp148*tmp387 + 8*tmp16*(tmp241*(15 + tmp285) + tmp438 &
                          &+ tmp472) + tmp112*tmp25*tmp31*(-4 + tmp461 + tmp543) + 8*tmp389*((14 + tmp291)/&
                          &tmp10 + tmp544) + tmp334*(tmp545 + tmp1*tmp25*(36 + tmp547) + 14*tmp64))*tmp91))&
                          &) - 4*logF2N2*Pi*tmp10*tmp279*tmp280*tmp38*tmp49*tmp507*((-32*tmp339)/tmp30 + 4*&
                          &tmp311*(160*tmp13 + tmp1*((10 + tmp152)/tmp10 + tmp322) + (8*(tmp281 + tmp297 - &
                          &tmp456/tmp10))/tmp248) + (tmp20**5*tmp353*tmp66)/(tmp10*tmp248) - tmp249*(1024*t&
                          &mp14 + tmp1*(tmp289 + ss*(42 + tmp284)*tmp332 + tmp498) + (-54/tmp10 + tmp303 + &
                          &tmp440 + tmp486)*tmp55 + (4*(-8*ss*(tmp192 + (21 + tmp291)/tmp10) + tmp495 + (54&
                          &*tmp1 - tmp306 + tmp150*tmp68)/tmp10))/tmp248) + tmp124*(448*tmp265 - 16*tmp255*&
                          &(tmp295 + tmp298 + (-26 + tmp299)/tmp10) - tmp13*(tmp302 + (10*tmp1 + (-24 + tmp&
                          &291)/tmp10)*tmp319 + ss*tmp326*((76 + tmp292)/tmp10 + tmp303 + tmp150*tmp375) + &
                          &(tmp199 + (28 + tmp285)/tmp10)*tmp378) + tmp1*tmp15*(-3*tmp289 + ss*(26 + tmp284&
                          &)*tmp326 + tmp415) + tmp247*(tmp300 - 8*ss*(tmp297 + tmp301) + ((84 + tmp299)/tm&
                          &p10 + tmp31*(4 + tmp386) + tmp530)/tmp10) + ((160*tmp16 + tmp241*(tmp199 + (38 +&
                          & tmp285)/tmp10) - 16*tmp15*(tmp1 + (13 + tmp291)/tmp10) + ss*tmp246*((48 + tmp29&
                          &9)/tmp10 + tmp499 + tmp379*tmp68))*tmp8)/tmp248) - 4*tmp12*(224*tmp271 - 8*tmp26&
                          &5*((3 + tmp273)/tmp10 + tmp295 + tmp318) + tmp13*tmp18*(224*tmp16 - tmp241*tmp30&
                          &9 + (tmp192 + (9 + tmp192)/tmp10)*tmp319 + ss*tmp146*((-20 + tmp292)/tmp10 + tmp&
                          &31*tmp398 + tmp439)) + tmp14*(-(tmp241*(tmp199 + (34 + tmp285)/tmp10)) + tmp310 &
                          &+ ss*tmp326*(tmp297 + (2 + tmp296)*tmp31 + (-4 + tmp44)/tmp10) + tmp319*(tmp355 &
                          &+ (11 + tmp44)/tmp10)) + tmp1*tmp16*(tmp122*(18 + tmp284) - tmp289 + tmp79) + (t&
                          &mp15*(-32*tmp16 + tmp241*(tmp199 + (-10 + tmp285)/tmp10) + ss*(tmp299 + tmp306 +&
                          & tmp293*tmp31)*tmp332 + ((9 + tmp291)/tmp10 + tmp295)*tmp79))/tmp248 + tmp255*(5&
                          &12*tmp15 + (tmp292 + tmp31*(7 + tmp407) + tmp332*(34 + tmp44))/tmp10 + (23*tmp1 &
                          &+ tmp146*(-5 + tmp192))*tmp82)) + (tmp115*(-16*tmp255*(tmp192 + (10 + tmp273)/tm&
                          &p10 + tmp304) + tmp432 + (tmp112*(tmp241*tmp309 + tmp319*(tmp241 + tmp348) + ss*&
                          &tmp146*(tmp305 + tmp306 - 4*tmp507)))/tmp248 - tmp13*(tmp241*(tmp199 + (40 + tmp&
                          &285)/tmp10) + tmp310 + ss*(tmp105 + (-8 + tmp292)/tmp10 + 5*tmp150*tmp31)*tmp332&
                          & - 16*tmp15*((-18 + tmp1)/tmp10 + tmp533)) - tmp15*tmp241*(tmp288 + (2 + tmp152)&
                          &*tmp81) + tmp247*(tmp441 + ((40 + tmp44)/tmp10 + tmp105*(tmp456 + tmp500))/tmp10&
                          & + (1/tmp10 + tmp292 + tmp327)*tmp82)))/tmp279 + 4*tmp254*(64*tmp255 + ss*tmp1*(&
                          &-80*tmp15 + ss*tmp146*(34 + tmp284) + tmp289) + (tmp241*(tmp199 + (22 + tmp285)/&
                          &tmp10) + tmp290 - 16*tmp15*(tmp107 + (17 + tmp291)/tmp10) + ss*tmp246*((32 + tmp&
                          &292)/tmp10 + tmp28*(58 + tmp37)))/tmp248 - 2*tmp13*(tmp321 + (44/tmp10 + tmp243 &
                          &+ tmp283 + tmp31 + tmp382)/tmp10 + (58/tmp10 + tmp325 + tmp378)*tmp81) - 8*tmp14&
                          &*(46/tmp10 + tmp105 + tmp313 + tmp84)) + (4*f1p*(4*tmp311 - 5*tmp249*tmp312 + (2&
                          &8*tmp265 + (tmp15*(-18*tmp106 - 28*tmp15 + tmp316))/tmp248 - tmp255*(tmp121 + tm&
                          &p318) + tmp112*tmp13*(11*tmp106 + tmp25 + tmp319) + tmp14*(-10*tmp106 + 17*tmp25&
                          & + tmp321) + tmp16*(8*tmp25 + tmp320 + ss*tmp371))/tmp279 - tmp115*(((ss + tmp14&
                          &6)*tmp15)/tmp10 + (ss*(-9*tmp106 + tmp145 + tmp25))/tmp248 + 8*tmp255 - tmp14*(t&
                          &mp314 + tmp315) + tmp13*(tmp106 + tmp316 + tmp416)) - tmp12*(28*tmp255 + tmp14*(&
                          &26/tmp10 + tmp322) + (ss*(tmp145 + 19*tmp25 + tmp347))/tmp248 + tmp13*(21*tmp25 &
                          &+ tmp323 + tmp352) + tmp334*(13*tmp106 - 10*tmp15 + tmp420)) + tmp254*(-21*tmp10&
                          &6 - 2*tmp25 + (-27/tmp10 + tmp313)/tmp248 + tmp323 + tmp465) + tmp11*(-8*tmp14 +&
                          & (34*tmp15)/tmp10 + (58*tmp106 + 11*tmp25 + tmp319)/tmp248 + tmp478 + tmp25*tmp8&
                          &2 + (tmp317 + tmp82)*tmp91)))/(tmp279*tmp30)) + 16*C0tP2P2NPL*Pi*tmp10*tmp34*tmp&
                          &38*tmp64*tmp66*tmp76*(tmp240 - (2*(3*tmp25 + tmp79 + tmp80 + tmp83 + tmp84))/tmp&
                          &248 + (4*(ss*tmp246 + tmp86/tmp248))/tmp279 + tmp87 + tmp89 + tmp32*tmp92) - 4*l&
                          &og4*Pi*tmp10*tmp248*tmp280*tmp38*tmp507*tmp76*(tmp254*((tmp241*(9*tmp199 - 20*ss&
                          &*tmp257 + tmp262))/tmp248 - 4*tmp13*(40*tmp337 + (tmp264 + 2*(tmp1 + tmp31*(3 + &
                          &tmp361)))/tmp10) + tmp429 + tmp240*(tmp298 + tmp431) + 10*ss*tmp549*tmp64) - 2*t&
                          &mp12*(tmp275 + (tmp15*tmp241*(tmp121*tmp272 + tmp274 + tmp257*tmp281))/tmp248 + &
                          &4*tmp255*(384*tmp15 - 8*ss*(tmp264 + tmp292) + (tmp121*tmp278 + tmp105*(-5 + tmp&
                          &1*tmp293))/tmp10) + 16*tmp265*(tmp107 + tmp304 + (10 + tmp372)/tmp10) + tmp112*t&
                          &mp13*(tmp267 + tmp241*(tmp269 + tmp331) + ss*tmp263*tmp348*tmp374) + tmp14*(-640&
                          &*tmp16 + tmp241*(-7*tmp199 + tmp121*tmp276) - 48*tmp15*((16 + tmp1)/tmp10 + tmp3&
                          &53) + ss*tmp332*(tmp270 + tmp342 + tmp31*tmp520)) - 10*tmp16*tmp549*tmp64) + (2*&
                          &tmp20*(192*tmp271 - (tmp15*tmp241*(tmp199 + 10*ss*tmp257 + tmp146*tmp272))/tmp24&
                          &8 + tmp14*(tmp241*(tmp146*(10 + tmp266) + tmp274) + tmp290 + ((29 + tmp105)/tmp1&
                          &0 + tmp277)*tmp319 + ss*tmp146*(tmp283 + tmp150*tmp31 + tmp354)) - 16*tmp265*(44&
                          &*ss + (-17 + tmp105)/tmp10 + tmp44) + tmp13*tmp18*(tmp267 + ss*tmp146*(58*tmp1 +&
                          & tmp263*tmp332 + tmp31*(5 + tmp368)) + tmp241*(tmp199 + tmp442)) + 5*tmp16*tmp54&
                          &9*tmp64 + 2*tmp255*(-416*tmp15 + (-34*tmp1 + (9 + kappa)*tmp31 + (-10 + tmp1)*tm&
                          &p332)/tmp10 + (tmp273 + tmp317)*tmp82)))/tmp279 - 4*tmp11*(64*tmp265 - 16*tmp255&
                          &*(tmp1 + tmp258 - tmp3/tmp10) + (ss*tmp241*(-10*ss*tmp257 + tmp262 + tmp31*tmp36&
                          &0))/tmp248 + 5*tmp15*tmp549*tmp64 - tmp13*(tmp267 + tmp241*(tmp269 + kappa*tmp45&
                          &3) + ss*tmp332*(tmp264 + tmp273 + tmp282*tmp68)) + tmp125*(tmp300 - (tmp105 + tm&
                          &p270 + (7 + tmp296)*tmp31)/tmp10 + ((11 + tmp1)/tmp10 + tmp44)*tmp82)) + (4*f1p*&
                          &(tmp249 + tmp115*(tmp14 + (ss*(ss + tmp2))/tmp248 - tmp15*tmp250 + tmp13*(tmp146&
                          & + tmp253)) - (1/tmp10 + 3/tmp248 + tmp251)*tmp254 + tmp12*(tmp125 + tmp132*(tmp&
                          &121 + tmp251) + 3*tmp13*tmp252 + (ss*(tmp2 + tmp314))/tmp248) - (tmp20*(3*tmp14 &
                          &+ (ss*(tmp121 + tmp253))/tmp248 + tmp13*(-11*ss + tmp315) + tmp15*(tmp251 + tmp3&
                          &32)))/tmp279 + tmp11*((1/tmp10 + tmp120)/tmp248 + tmp112*(tmp146 + tmp251) + tmp&
                          &91)))/(tmp248*tmp30*tmp76) + tmp249*(-128*tmp14 + (tmp1*tmp257*tmp332)/tmp248 - &
                          &2*tmp549*tmp64 + tmp465*tmp92) - tmp115*(-16*(104*ss + (-14 + tmp1)/tmp10 + tmp1&
                          &05)*tmp265 + tmp275 - tmp14*(tmp241*(tmp199 - (16 + tmp266)/tmp10) + tmp302 + ss&
                          &*tmp332*(tmp301 + tmp305 + tmp350*tmp414) + tmp319*(tmp105 + tmp450)) - 2*tmp16*&
                          &tmp549*tmp64 + (tmp15*tmp241*(tmp272/tmp10 + tmp330 + tmp257*tmp81))/tmp248 + 4*&
                          &tmp255*(((-16 + tmp1)/tmp10 + (-7 + tmp1)*tmp105)/tmp10 + 32*tmp15 + ((9 + tmp1)&
                          &/tmp10 + tmp277)*tmp82) + tmp112*tmp13*(tmp213*tmp276 + ss*tmp146*(tmp264 + tmp1&
                          &07*(-5 + tmp37)) + tmp319*tmp92))) - 8*logN2L2*Pi*tmp248*tmp279*tmp34*tmp38*tmp4&
                          &9*tmp50*tmp61*tmp63*tmp64*(tmp12*(tmp143*tmp239 + tmp1*tmp25*tmp252 - (4*tmp25*(&
                          &tmp1 + tmp252))/tmp248 - 128*tmp255 + (tmp106 + tmp25 + ss*tmp28)*tmp465) + (tmp&
                          &20*tmp25*tmp32)/tmp76 - 4*tmp11*(tmp239 + tmp1*tmp25 - (4*tmp25)/tmp248 + tmp85/&
                          &tmp155) + (tmp25*(tmp240 + tmp18*tmp241 - 4*tmp335 + (tmp241 + tmp93)/tmp248))/t&
                          &mp279)

  END FUNCTION MP2MPL_MP_FIN_D12D20

  FUNCTION MP2MPL_MP_FIN_D13D20(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_FIN_d13d20
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206, tmp207, tmp208, tmp209, tmp210
  real tmp211, tmp212, tmp213, tmp214, tmp215
  real tmp216, tmp217, tmp218, tmp219, tmp220
  real tmp221, tmp222, tmp223, tmp224, tmp225
  real tmp226, tmp227, tmp228, tmp229, tmp230
  real tmp231, tmp232, tmp233, tmp234, tmp235
  real tmp236, tmp237, tmp238, tmp239, tmp240
  real tmp241, tmp242, tmp243, tmp244, tmp245
  real tmp246, tmp247, tmp248, tmp249, tmp250
  real tmp251, tmp252, tmp253, tmp254, tmp255
  real tmp256, tmp257, tmp258, tmp259, tmp260
  real tmp261, tmp262, tmp263, tmp264, tmp265
  real tmp266, tmp267, tmp268, tmp269, tmp270
  real tmp271, tmp272, tmp273, tmp274, tmp275
  real tmp276, tmp277, tmp278, tmp279, tmp280
  real tmp281, tmp282, tmp283, tmp284, tmp285
  real tmp286, tmp287, tmp288, tmp289, tmp290
  real tmp291, tmp292, tmp293, tmp294, tmp295
  real tmp296, tmp297, tmp298, tmp299, tmp300
  real tmp301, tmp302, tmp303, tmp304, tmp305
  real tmp306, tmp307, tmp308, tmp309, tmp310
  real tmp311, tmp312, tmp313, tmp314, tmp315
  real tmp316, tmp317, tmp318, tmp319, tmp320
  real tmp321, tmp322, tmp323, tmp324, tmp325
  real tmp326, tmp327, tmp328, tmp329, tmp330
  real tmp331, tmp332, tmp333, tmp334, tmp335
  real tmp336, tmp337, tmp338, tmp339, tmp340
  real tmp341, tmp342, tmp343, tmp344, tmp345
  real tmp346, tmp347, tmp348, tmp349, tmp350
  real tmp351, tmp352, tmp353, tmp354, tmp355
  real tmp356, tmp357, tmp358, tmp359, tmp360
  real tmp361, tmp362, tmp363, tmp364, tmp365
  real tmp366, tmp367, tmp368, tmp369, tmp370
  real tmp371, tmp372, tmp373, tmp374, tmp375
  real tmp376, tmp377, tmp378, tmp379, tmp380
  real tmp381, tmp382, tmp383, tmp384, tmp385
  real tmp386, tmp387, tmp388, tmp389, tmp390
  real tmp391, tmp392, tmp393, tmp394, tmp395
  real tmp396, tmp397, tmp398, tmp399, tmp400
  real tmp401, tmp402, tmp403, tmp404, tmp405
  real tmp406, tmp407, tmp408

  tmp1 = -tt
  tmp2 = -1 + f1p
  tmp3 = sqrt(lambda)**2
  tmp4 = 1 + tmp3
  tmp5 = tmp4**2
  tmp6 = 2*me2
  tmp7 = 2*mm2
  tmp8 = -2*ss
  tmp9 = tmp1 + tmp6 + tmp7 + tmp8
  tmp10 = -ss
  tmp11 = me2 + mm2 + tmp10
  tmp12 = 1/tt
  tmp13 = 3*tmp3
  tmp14 = 2 + tmp13
  tmp15 = me2**2
  tmp16 = mm2**2
  tmp17 = ss**2
  tmp18 = sqrt(lambda)**4
  tmp19 = 4*mm2
  tmp20 = mm2 + tmp10
  tmp21 = 4*tmp15
  tmp22 = 8*me2*tmp20
  tmp23 = 4*tmp17
  tmp24 = (2*ss)/tmp12
  tmp25 = tmp12**(-2)
  tmp26 = -tmp3
  tmp27 = tmp19 + tmp26
  tmp28 = 1/tmp27
  tmp29 = 4*tmp16
  tmp30 = -8*mm2*ss
  tmp31 = -1 + kappa
  tmp32 = kappa*tmp26
  tmp33 = tmp19 + tmp32
  tmp34 = -4*mm2
  tmp35 = tmp3 + tmp34
  tmp36 = tmp35**(-2)
  tmp37 = 1 + kappa
  tmp38 = 4/tmp12
  tmp39 = 4*me2
  tmp40 = tmp1 + tmp39
  tmp41 = 1/tmp40
  tmp42 = 1/tmp25
  tmp43 = 16*mm2*tmp15
  tmp44 = -3*mm2
  tmp45 = ss + 1/tmp12 + tmp44
  tmp46 = tmp25*tmp45
  tmp47 = 16*tmp16
  tmp48 = -16*mm2*ss
  tmp49 = -3*tmp25
  tmp50 = tmp47 + tmp48 + tmp49
  tmp51 = me2*tmp50
  tmp52 = tmp43 + tmp46 + tmp51
  tmp53 = kappa*tmp3
  tmp54 = 1/tmp12 + tmp34
  tmp55 = 1/tmp54
  tmp56 = tmp34 + tmp53
  tmp57 = tmp56**2
  tmp58 = me2**3
  tmp59 = mm2**3
  tmp60 = ss**3
  tmp61 = 2*tmp18
  tmp62 = 4*ss*tmp3
  tmp63 = 12*tmp17
  tmp64 = tmp31**2
  tmp65 = sqrt(lambda)**8
  tmp66 = 4*tmp58
  tmp67 = 12*tmp15*tmp20
  tmp68 = tmp1 + tmp19
  tmp69 = 1/tmp68
  tmp70 = tmp23 + tmp24 + tmp25
  tmp71 = -4*ss
  tmp72 = 8*tmp17
  tmp73 = ss*tmp38
  tmp74 = 4*ss
  tmp75 = 8*ss
  tmp76 = tmp26/tmp12
  tmp77 = 2*ss*tmp3
  tmp78 = 8*tmp16
  tmp79 = tmp3 + tmp75
  tmp80 = sqrt(lambda)**6
  tmp81 = tmp21/tmp55
  tmp82 = tmp70 + tmp76
  tmp83 = tmp82/tmp12
  tmp84 = 2*tmp16
  tmp85 = 1/tmp12 + tmp3
  tmp86 = 2*ss*tmp85
  tmp87 = 20*tmp59
  tmp88 = -20*ss*tmp16
  tmp89 = tmp18 + tmp23 + tmp25 + tmp86
  tmp90 = tmp10*tmp89
  tmp91 = 28*tmp16
  tmp92 = tmp18 + tmp25 + tmp48 + tmp63 + tmp86 + tmp91
  tmp93 = me2*tmp92
  tmp94 = (8*tmp3)/tmp12
  tmp95 = tmp89 + tmp94
  tmp96 = mm2*tmp95
  tmp97 = tmp66 + tmp67 + tmp87 + tmp88 + tmp90 + tmp93 + tmp96
  tmp98 = ss/tmp12
  tmp99 = -2*tmp3
  tmp100 = 1/tmp12 + tmp99
  tmp101 = tmp100/tmp12
  tmp102 = tmp3 + tmp74
  tmp103 = tmp74*tmp85
  tmp104 = 2*ss
  tmp105 = tmp104 + 1/tmp12
  tmp106 = tmp105*tmp34
  tmp107 = tmp20**2
  tmp108 = mm2 + ss
  tmp109 = -2*me2*tmp108
  tmp110 = tmp107 + tmp109 + tmp15
  tmp111 = 1/tmp110
  tmp112 = -tmp98
  tmp113 = 2*tmp58
  tmp114 = 2*tmp59
  tmp115 = 6*ss
  tmp116 = tmp115 + 1/tmp12
  tmp117 = -(tmp116*tmp16)
  tmp118 = -2*mm2
  tmp119 = -6*ss
  tmp120 = 3/tmp12
  tmp121 = tmp118 + tmp119 + tmp120
  tmp122 = tmp121*tmp15
  tmp123 = -2*tmp17
  tmp124 = tmp112 + tmp123 + tmp25
  tmp125 = ss*tmp124
  tmp126 = 6*tmp17
  tmp127 = tmp126 + tmp24 + tmp25
  tmp128 = mm2*tmp127
  tmp129 = -6*tmp17
  tmp130 = tmp105*tmp7
  tmp131 = tmp129 + tmp130 + tmp24 + tmp25 + tmp84
  tmp132 = -(me2*tmp131)
  tmp133 = tmp113 + tmp114 + tmp117 + tmp122 + tmp125 + tmp128 + tmp132
  tmp134 = tmp1 + tmp102
  tmp135 = 1/tmp12 + tmp26
  tmp136 = -8*tmp17
  tmp137 = 2/tmp12
  tmp138 = -tmp25
  tmp139 = -(kappa*tmp18)
  tmp140 = 4*tmp59
  tmp141 = tmp105 + tmp139
  tmp142 = kappa*tmp18
  tmp143 = -4*tmp16
  tmp144 = tmp12**(-3)
  tmp145 = -8*me2
  tmp146 = -4*me2
  tmp147 = -1 + tmp3
  tmp148 = tmp147*tmp25
  tmp149 = 2*tmp3
  tmp150 = 1/tmp135
  tmp151 = tmp18*tmp21
  tmp152 = -4*tmp142*tmp15
  tmp153 = 16*tmp3*tmp59
  tmp154 = (-4*tmp15*tmp3)/tmp12
  tmp155 = tmp138*tmp3
  tmp156 = tmp18*tmp25
  tmp157 = -2*kappa*tmp156
  tmp158 = tmp144*tmp26
  tmp159 = tmp3*tmp31
  tmp160 = 1/tmp12 + tmp159
  tmp161 = -4*tmp160*tmp17*tmp3
  tmp162 = tmp135 + tmp145 + tmp53 + tmp75
  tmp163 = tmp143*tmp162*tmp3
  tmp164 = tmp146*tmp18*tmp31
  tmp165 = 2*kappa
  tmp166 = -1 + tmp165
  tmp167 = tmp166*tmp3
  tmp168 = 1 + tmp146 + tmp167
  tmp169 = (tmp168*tmp3)/tmp12
  tmp170 = tmp148 + tmp164 + tmp169
  tmp171 = tmp170*tmp8
  tmp172 = -2*me2*tmp18*tmp31
  tmp173 = tmp21*tmp3
  tmp174 = tmp23*tmp3
  tmp175 = -2*me2
  tmp176 = tmp175 + tmp4
  tmp177 = (tmp176*tmp3)/tmp12
  tmp178 = 1 + tmp146 + tmp53
  tmp179 = tmp178*tmp3
  tmp180 = -1 + tmp149
  tmp181 = tmp180/tmp12
  tmp182 = tmp179 + tmp181
  tmp183 = tmp104*tmp182
  tmp184 = tmp148 + tmp172 + tmp173 + tmp174 + tmp177 + tmp183
  tmp185 = tmp184*tmp19
  tmp186 = tmp144 + tmp151 + tmp152 + tmp153 + tmp154 + tmp155 + tmp156 + tmp157 +&
                          & tmp158 + tmp161 + tmp163 + tmp171 + tmp185
  tmp187 = 32*tmp59
  tmp188 = 16*tmp59
  tmp189 = tmp3/tmp12
  tmp190 = tmp141 + tmp3
  tmp191 = 2*tmp17
  tmp192 = tmp139 + tmp85
  tmp193 = tmp16*tmp190
  tmp194 = 2*tmp189
  tmp195 = ss*tmp192
  tmp196 = 1/mm2
  tmp197 = tmp3*tmp37
  tmp198 = tmp196**(-4)
  tmp199 = -1 + tmp197
  tmp200 = -2 + kappa
  tmp201 = 4 + tmp3
  tmp202 = tmp200*tmp3
  tmp203 = tmp196**(-5)
  tmp204 = -3*tmp3
  tmp205 = 20*ss*tmp199
  tmp206 = -4 + tmp202
  tmp207 = tmp201/tmp12
  tmp208 = 40*tmp17*tmp85
  tmp209 = -1 + tmp202
  tmp210 = tmp209/tmp12
  tmp211 = tmp137*tmp4
  tmp212 = tmp196**(-6)
  tmp213 = 4*tmp3
  tmp214 = -3*tmp142
  tmp215 = 64*tmp212
  tmp216 = tmp206/tmp12
  tmp217 = 2 + tmp202
  tmp218 = -2*tmp142
  tmp219 = -2 + tmp3
  tmp220 = 1/me2
  tmp221 = tmp111**2
  tmp222 = -5*tmp3
  tmp223 = 20*ss
  tmp224 = -5/tmp12
  tmp225 = -2*tmp189
  tmp226 = tmp220**(-5)
  tmp227 = 5*tmp3
  tmp228 = 2 + kappa
  tmp229 = tmp220**(-4)
  tmp230 = -23*tmp3
  tmp231 = tmp227*tmp37
  tmp232 = tmp228*tmp3
  tmp233 = 8 + tmp232
  tmp234 = tmp233/tmp12
  tmp235 = tmp142 + tmp234
  tmp236 = tmp235/tmp12
  tmp237 = 160*tmp60
  tmp238 = 8 + tmp13
  tmp239 = 10*tmp3
  tmp240 = 8*tmp3
  tmp241 = 9*tmp3
  tmp242 = 4 + kappa
  tmp243 = tmp242*tmp61
  tmp244 = 7*tmp3
  tmp245 = 11*tmp3
  tmp246 = 3*kappa
  tmp247 = -8*tmp3
  tmp248 = 6*tmp3
  tmp249 = 80*tmp17
  tmp250 = 64*tmp60
  tmp251 = 24*ss
  tmp252 = -2 + tmp232
  tmp253 = tmp252/tmp12
  tmp254 = tmp142 + tmp253
  tmp255 = 128*tmp60
  tmp256 = -4 + tmp248
  tmp257 = tmp256/tmp12
  tmp258 = tmp220**(-7)
  tmp259 = tmp220**(-6)
  tmp260 = tmp137 + tmp3
  tmp261 = tmp18*tmp8
  tmp262 = tmp224*tmp3
  tmp263 = 5*kappa
  tmp264 = kappa**2
  tmp265 = sqrt(lambda)**10
  tmp266 = -5*tmp18
  tmp267 = 2 + tmp3
  tmp268 = 1 + tmp149
  tmp269 = -3 + kappa
  tmp270 = 6*kappa
  tmp271 = 3 + kappa
  tmp272 = tmp196**(-7)
  tmp273 = (5*tmp18)/tmp12
  tmp274 = -5*kappa
  tmp275 = 4*kappa
  tmp276 = tmp165*tmp18
  tmp277 = 4 + tmp227
  tmp278 = tmp213*tmp31
  tmp279 = ss**4
  tmp280 = -3/tmp12
  tmp281 = -4*kappa
  tmp282 = -(kappa*tmp65)
  tmp283 = tmp196**(-8)
  tmp284 = 5 + tmp3
  tmp285 = -2/tmp12
  tmp286 = 10*kappa
  tmp287 = -2*tmp18
  tmp288 = 4 + tmp246
  tmp289 = 5 + tmp246
  tmp290 = 16*kappa*tmp25*tmp80
  tmp291 = 8*kappa
  tmp292 = 8/tmp12
  tmp293 = 7*tmp25*tmp264*tmp80
  tmp294 = -5 + kappa
  tmp295 = 7 + tmp165
  tmp296 = 19*kappa
  tmp297 = tmp289*tmp3
  tmp298 = tmp294*tmp3
  tmp299 = -20*ss
  tmp300 = tmp20**3
  tmp301 = tmp18 + tmp194
  tmp302 = ss*tmp301
  tmp303 = 16*ss
  tmp304 = 160*tmp17
  tmp305 = 1 + tmp165
  tmp306 = 3 + tmp3
  tmp307 = -160*tmp17
  tmp308 = 1/tmp12 + tmp74
  tmp309 = -3 + tmp3
  tmp310 = tmp284/tmp12
  tmp311 = 6/tmp12
  tmp312 = tmp263*tmp3
  tmp313 = tmp25*tmp281
  tmp314 = tmp100 + tmp189
  tmp315 = 6*tmp25*tmp306
  tmp316 = kappa*tmp189
  tmp317 = tmp18*tmp31
  tmp318 = tmp165*tmp3
  tmp319 = tmp25*tmp275
  tmp320 = -6*tmp18
  tmp321 = 5 + kappa
  tmp322 = 2*tmp80
  tmp323 = 6 + tmp197
  tmp324 = -3*kappa
  tmp325 = 3*tmp159
  tmp326 = 12*ss
  tmp327 = 48*tmp17
  tmp328 = tmp137*tmp306
  tmp329 = -tmp80
  tmp330 = 1 + tmp263
  tmp331 = -13*tmp3
  tmp332 = 8*tmp18
  tmp333 = -320*tmp60
  tmp334 = tmp25*tmp291
  tmp335 = 13*kappa
  tmp336 = -6*tmp3
  tmp337 = 2*tmp159
  tmp338 = 4*tmp25*tmp306
  tmp339 = -15*tmp3
  tmp340 = 5 + tmp13
  tmp341 = 5 + tmp337
  tmp342 = (tmp18*tmp341)/tmp12
  tmp343 = kappa/tmp12
  tmp344 = -9*tmp3
  tmp345 = tmp3*tmp330
  tmp346 = -14*tmp3
  tmp347 = 1 + tmp335
  tmp348 = tmp3*tmp347
  tmp349 = tmp260*tmp29
  tmp350 = -((tmp102*tmp260)/tmp196)
  tmp351 = tmp157 + tmp302 + tmp349 + tmp350
  tmp352 = -10*tmp3
  tmp353 = 3*tmp189
  tmp354 = 2*tmp25*tmp306
  tmp355 = 1/tmp12 + tmp189 + tmp204
  tmp356 = tmp269*tmp3
  tmp357 = 256*tmp203
  tmp358 = -4*tmp3
  tmp359 = -6 + tmp356
  tmp360 = tmp149 + tmp210
  tmp361 = 5*tmp18
  tmp362 = 11 + tmp13
  tmp363 = -(tmp362/tmp12)
  tmp364 = -8*tmp18
  tmp365 = 96*tmp17
  tmp366 = 8*tmp25*tmp306
  tmp367 = -7*tmp3
  tmp368 = -7*tmp142*tmp144
  tmp369 = tmp246*tmp3
  tmp370 = tmp137*tmp359
  tmp371 = 160*tmp279
  tmp372 = 7*kappa
  tmp373 = tmp137*tmp340
  tmp374 = tmp3*tmp311
  tmp375 = 384*tmp60
  tmp376 = 7 + tmp13
  tmp377 = tmp376/tmp12
  tmp378 = 5 + tmp159
  tmp379 = kappa*tmp144*tmp80
  tmp380 = tmp3*tmp372
  tmp381 = 2*tmp343
  tmp382 = 7 + tmp3
  tmp383 = tmp382/tmp12
  tmp384 = tmp204 + tmp383 + tmp74
  tmp385 = tmp25*tmp324
  tmp386 = -6*tmp25*tmp306
  tmp387 = tmp204 + tmp310
  tmp388 = tmp387*tmp74
  tmp389 = tmp359/tmp12
  tmp390 = tmp25*tmp270
  tmp391 = 320*tmp60
  tmp392 = -10*tmp18
  tmp393 = -4*tmp80
  tmp394 = -3 + tmp159
  tmp395 = 11*kappa
  tmp396 = 64*tmp272
  tmp397 = -(tmp306/tmp12)
  tmp398 = kappa*tmp25
  tmp399 = -tmp18
  tmp400 = 5 + tmp149
  tmp401 = -2*tmp25*tmp306
  tmp402 = tmp144*tmp18*tmp372
  tmp403 = tmp3 + tmp317
  tmp404 = 2*tmp398
  tmp405 = -6/tmp12
  tmp406 = tmp227/tmp12
  tmp407 = 7 + tmp324
  tmp408 = -23*tmp18
  MP2MPL_MP_FIN_d13d20 = 8*logmuP2*Pi*tmp12*tmp2*(tmp147/tmp12 + tmp11*tmp3) - 16*C0IR6sFP*Pi*tmp11*tmp12&
                          &*tmp141*tmp2*tmp4 - (32*DB0F2FN*Pi*tmp111*tmp12*tmp133*tmp2*tmp28*tmp317*tmp41)/&
                          &tmp220 - (32*DB0F2FL*Pi*tmp111*tmp12*tmp133*tmp2*tmp28*tmp3*tmp33*tmp41)/tmp220 &
                          &- 16*logmut*Pi*tmp11*tmp12*tmp2*tmp5 - 32*logbeN2*Pi*tmp2*tmp28*tmp317*tmp4*tmp4&
                          &1*tmp42*tmp52 - (32*logbeL2*Pi*tmp2*tmp28*tmp3*tmp33*tmp4*tmp41*tmp42*tmp52*tmp5&
                          &5)/tmp150 + 16*DB0tLL*Pi*tmp12*tmp18*tmp2*tmp36*tmp41*tmp52*tmp55*tmp57 + 16*DB0&
                          &tNN*Pi*tmp12*tmp2*tmp36*tmp41*tmp52*tmp55*tmp64*tmp65 + D0tsLL*Pi*tmp12*tmp2*tmp&
                          &36*tmp364*tmp57*(ss*tmp138 + tmp140 - 12*ss*tmp16 + tmp137*tmp18 + tmp261 + tmp1&
                          &7*tmp285 + tmp17*tmp358 - 4*tmp60 + (tmp24 + tmp25 + tmp61 + tmp62 + tmp63)/tmp1&
                          &96 + (12*tmp16 - (24*ss)/tmp196 + tmp24 + tmp25 + tmp61 + tmp62 + tmp63)/tmp220 &
                          &+ tmp66 + tmp67) + DB0sFP*logmuF2*Pi*tmp11*tmp111*tmp12*tmp150*tmp186*tmp2*tmp30&
                          &3*tmp4*tmp69 + 16*D0IR6tsN*Pi*tmp11*tmp12*tmp2*tmp28*tmp317*tmp4*(20*tmp16 + tmp&
                          &21 + tmp22 + tmp70) + 16*C0tP2P2NP0*Pi*tmp12*tmp2*tmp28*tmp317*tmp4*(tmp143 + tm&
                          &p21 - (8*ss)/tmp220 + tmp285/tmp196 + tmp70) - 16*DB0sFPlogmut*Pi*ss*tmp11*tmp11&
                          &1*tmp2*tmp42*tmp5*(tmp21 + tmp22 + tmp29 + tmp30 + tmp70) - 8*D0tsNN*Pi*tmp12*tm&
                          &p2*tmp36*tmp64*tmp65*((7*ss - 8/tmp12)*tmp143 + (-4*tmp17 + tmp24 + tmp25)/tmp19&
                          &6 + 36*tmp59 + (44*tmp16 + tmp24 + tmp25 + tmp30 + tmp63)/tmp220 + tmp66 + tmp67&
                          & + tmp10*tmp70) - 16*C0tF2F2NF0*Pi*tmp12*tmp2*tmp28*tmp317*tmp4*tmp41*((tmp106 +&
                          & tmp29 + tmp70)/tmp12 - (2*(tmp106 + tmp25 + tmp72 + tmp73))/tmp220 + tmp21*(tmp&
                          &1 + tmp74)) + 16*logmuL2*Pi*tmp12*tmp2*tmp3*tmp33*tmp36*(tmp16*tmp213 + tmp3*(1/&
                          &tmp12 + (ss - 1/tmp220)*tmp53) - (tmp38 + tmp3*(tmp146 + tmp53 + tmp74))/tmp196)&
                          & + DB0sFP*logF2P2*Pi*tmp11*tmp111*tmp12*tmp150*tmp186*tmp2*tmp4*tmp69*tmp75 - 16&
                          &*logmuN2*Pi*tmp12*tmp2*tmp317*tmp36*(tmp16*tmp247 + tmp18*(tmp10 + 1/tmp220)*tmp&
                          &37 + (tmp38 + tmp3*(tmp145 + tmp197 + tmp75))/tmp196 + tmp76) + 16*C0sF2P2FLP*Pi&
                          &*tmp111*tmp12*tmp2*tmp28*tmp3*tmp56*((tmp193 + ss*(tmp129 + tmp194 - 3*tmp195) +&
                          & (2*(tmp191 + tmp194 + tmp195))/tmp196)/tmp220 + tmp15*(tmp126 + 3*tmp195 + tmp1&
                          &90/tmp196 + tmp225) - tmp190*tmp58 - tmp20*(tmp17*tmp190 + tmp193 - (2*(tmp191 +&
                          & tmp195 + tmp76))/tmp196)) + 16*D0IR6tsL*Pi*tmp11*tmp12*tmp2*tmp28*tmp3*tmp33*tm&
                          &p4*(tmp18 + tmp21 + tmp22 + tmp29 + tmp30 + tmp70 + tmp77) + C0tF2F2LFL*Pi*tmp12&
                          &*tmp2*tmp332*tmp36*tmp41*tmp57*((tmp101 + tmp23 + tmp24 + tmp29 + tmp30)/tmp12 -&
                          & 4*tmp15*(tmp100 + tmp19 + tmp71) - (2*(tmp101 + tmp103 - (4*tmp102)/tmp196 + tm&
                          &p72 + tmp78))/tmp220) - 8*C0tF2F20F0*Pi*tmp12*tmp2*tmp41*tmp5*(-((tmp29 + tmp30 &
                          &+ tmp70)/tmp12) + tmp21*(1/tmp12 + tmp19 + tmp71) + (2*(tmp25 + tmp48 + tmp72 + &
                          &tmp73 + tmp78))/tmp220) + 8*logmuF2*Pi*tmp12*tmp2*(1/tmp12 + tmp189 + tmp14/tmp1&
                          &96 + ss*tmp204 + tmp14/tmp220 + tmp8) - (16*DB0sFP*Pi*tmp12*tmp2*(-3*ss*tmp15 + &
                          &tmp15/tmp12 - 3*ss*tmp16 + tmp16/tmp12 - tmp17/tmp12 - tmp15/tmp196 + (3*tmp17)/&
                          &tmp196 - tmp16/tmp220 + (3*tmp17)/tmp220 + tmp285/(tmp196*tmp220) + tmp58 + tmp5&
                          &9 - tmp60 + tmp8/(tmp196*tmp220)))/(tmp15 + tmp16 + tmp17 - 2/(tmp196*tmp220) + &
                          &tmp8/tmp196 + tmp8/tmp220) + 16*C0sF2P2FNP*Pi*tmp111*tmp12*tmp2*tmp28*tmp317*(tm&
                          &p15*(-3*ss*tmp141 + tmp143 + (-14*ss + 7/tmp12 + tmp142)/tmp196) + (tmp141 + tmp&
                          &19)*tmp58 + ((-10*ss - 17/tmp12 + tmp142)*tmp16 + 3*tmp141*tmp17 - 4*tmp59 + (tm&
                          &p104*(tmp142 + tmp224 + tmp74))/tmp196)/tmp220 + tmp20*(tmp140 - (tmp115 - 9/tmp&
                          &12 + tmp142)*tmp16 + tmp141*tmp17 + ((1/tmp12 + tmp139)*tmp8)/tmp196)) + 32*DB0t&
                          &LN*Pi*tmp12*tmp2*tmp31*tmp33*tmp36*tmp41*tmp52*tmp55*tmp80 - 16*C0tF2F2NFL*Pi*tm&
                          &p12*tmp2*tmp31*tmp36*tmp41*tmp56*(tmp134*tmp21 + (tmp106 + 1/(tmp12*tmp150) + tm&
                          &p23 + tmp24 + tmp29)/tmp12 + (2*(tmp136 + (tmp1 + tmp3)/tmp12 + (2*(tmp260 + tmp&
                          &74))/tmp196 + tmp260*tmp8))/tmp220)*tmp80 + 16*C0tF2F2LF0*Pi*tmp12*tmp2*tmp28*tm&
                          &p3*tmp4*tmp41*tmp56*(-4*tmp15*(tmp135 + tmp19 + tmp71) - (2*(tmp25 + tmp72 + tmp&
                          &73 + tmp76 + tmp77 + tmp78 - (2*tmp79)/tmp196))/tmp220 + (tmp29 + tmp30 + tmp82)&
                          &/tmp12) + 16*C0tP2P2LP0*Pi*tmp12*tmp2*tmp28*tmp3*tmp4*tmp56*tmp69*(tmp134*tmp29 &
                          &- (2*(tmp25 + tmp72 + tmp73 + tmp76 + tmp77))/tmp196 - (4*(tmp24 + tmp29 - tmp79&
                          &/tmp196))/tmp220 + tmp81 + tmp83) + 8*C0tP2P2NPN*Pi*tmp12*tmp2*tmp36*tmp64*tmp65&
                          &*tmp69*(tmp187 + tmp70/tmp12 - (2*(5*tmp25 + tmp72 + tmp73))/tmp196 + tmp143*(tm&
                          &p280 + tmp74) + tmp81 + (8*(tmp112 + tmp74/tmp196 + tmp84))/tmp220) + 16*C0tP2P2&
                          &NPL*Pi*tmp12*tmp2*tmp31*tmp33*tmp36*tmp69*tmp80*(tmp188 - (2*(3*tmp25 + tmp72 + &
                          &tmp73 + tmp76 + tmp77))/tmp196 + (4*(ss*tmp285 + tmp79/tmp196))/tmp220 + tmp81 +&
                          & tmp83 + tmp29*tmp85) - 8*logF2t*Pi*tmp2*tmp41*tmp5*tmp9 - 8*logP2t*Pi*tmp2*tmp5&
                          &*tmp69*tmp9 + 8*D0tsLN*Pi*tmp12*tmp2*tmp31*tmp36*tmp56*tmp80*tmp97 + 8*D0tsNL*Pi&
                          &*tmp12*tmp2*tmp31*tmp36*tmp56*tmp80*tmp97 + 8*C0tF2F2NFN*Pi*tmp12*tmp2*tmp36*tmp&
                          &41*tmp64*tmp65*(((-8*(ss + 1/tmp12))/tmp196 + tmp29 + tmp70)/tmp12 + tmp21*(tmp6&
                          &8 + tmp74) + (2*(tmp136 + tmp138 + tmp292/tmp196 + tmp78 - 4*tmp98))/tmp220) + C&
                          &0tP2P2LPL*Pi*tmp12*tmp2*tmp332*tmp36*tmp57*tmp69*((tmp101 + tmp23 + tmp24)/tmp12&
                          & - (2*(tmp101 + tmp103 + tmp72))/tmp196 + tmp29*(tmp1 + tmp149 + tmp74) + tmp81 &
                          &- (8*(-(tmp102/tmp196) + tmp84 + tmp98))/tmp220) - 8*C0tP2P20P0*Pi*tmp12*tmp2*tm&
                          &p5*tmp69*(tmp21/tmp69 - tmp70/tmp12 + tmp29*(1/tmp12 + tmp71) + (2*(tmp25 + tmp7&
                          &2 + tmp73))/tmp196 + (8*(tmp71/tmp196 + tmp84 + tmp98))/tmp220) - (32*DB0P2LP*Pi&
                          &*tmp111*tmp12*tmp2*tmp28*tmp3*tmp33*tmp69*(tmp113 + (tmp127 - 2*tmp16 - (2*tmp10&
                          &5)/tmp196)/tmp220 - tmp15*(tmp116 + tmp7) + tmp20*(tmp138 + tmp191 + (tmp120 + t&
                          &mp71)/tmp196 + tmp84 + tmp98)))/tmp196 - 8*logN2L2*Pi*tmp196*tmp2*tmp220*tmp31*t&
                          &mp36*tmp41*tmp42*tmp55*tmp56*tmp80*((tmp20*tmp25*tmp29)/tmp69 - 4*tmp58*(tmp187 &
                          &- (4*tmp25)/tmp196 + tmp25*tmp3 + tmp78/tmp150) + (tmp25*(tmp188 + tmp10*tmp189 &
                          &+ tmp143*(tmp105 + tmp3) + (tmp189 + tmp86)/tmp196))/tmp220 + tmp15*(tmp134*tmp1&
                          &87 - 128*tmp198 + tmp25*tmp3*tmp308 - (4*tmp25*(tmp3 + tmp308))/tmp196 + 32*tmp1&
                          &6*(tmp25 + ss*tmp26 + tmp98))) + 4*log4*Pi*tmp12*tmp196*tmp2*tmp221*tmp317*tmp36&
                          &*tmp69*(tmp66*(32*tmp203 - tmp16*(tmp208 + ss*tmp137*(2*tmp207 + 4*tmp18*tmp228 &
                          &+ tmp245) + tmp189*(tmp210 + tmp18*tmp281)) - 8*tmp198*(-(tmp268/tmp12) + tmp3 +&
                          & tmp326) + (ss*tmp189*(-10*ss*tmp199 + (4 - tmp202)/tmp12 + kappa*tmp361))/tmp19&
                          &6 + tmp114*(tmp249 - (tmp211 + tmp18*(7 + tmp246) + tmp3)/tmp12 + (tmp13 + (11 +&
                          & tmp149)/tmp12)*tmp74) + 5*tmp17*tmp343*tmp80) - (2*tmp20*(96*tmp212 - 8*tmp203*&
                          &(44*ss + tmp13 + (-17 + tmp213)/tmp12) - (tmp17*tmp189*(tmp142 + 10*ss*tmp199 + &
                          &2*tmp216))/tmp196 + 2*tmp198*(-208*tmp17 + ss*(92/tmp12 + 44*tmp3) + ((9 + kappa&
                          &)*tmp18 - 17*tmp3 + (-5 + tmp3)*tmp38)/tmp12) + tmp10*tmp16*(tmp208 + ss*tmp137*&
                          &(4*tmp207 + 29*tmp3 + tmp18*(5 + tmp324)) + tmp189*(tmp142 + tmp405)) + tmp59*(t&
                          &mp189*(tmp137*(5 + tmp202) + tmp214) + tmp237 + ss*tmp137*(-12/tmp12 + tmp230 + &
                          &tmp18*tmp37) + ((29 + tmp213)/tmp12 + 13*tmp3)*tmp72) + 5*tmp343*tmp60*tmp80))/t&
                          &mp220 + 2*tmp226*(tmp187 + (tmp199*tmp285*tmp3)/tmp196 + tmp343*tmp80 - 8*tmp16*&
                          &tmp85) + tmp229*(-192*tmp198 + (tmp189*(-9*tmp142 + tmp205 + tmp216))/tmp196 - 1&
                          &6*(tmp223 + tmp355)*tmp59 - 10*ss*tmp343*tmp80 + tmp29*((tmp207 + tmp18*(6 + tmp&
                          &275) + tmp3)/tmp12 + tmp223*tmp85)) + 2*tmp15*(tmp215 + (tmp17*tmp189*(tmp205 + &
                          &tmp214 + 3*tmp216))/tmp196 + 4*tmp198*(192*tmp17 - 8*ss*(tmp13 + tmp207) + (tmp1&
                          &20*tmp219 + tmp222 + tmp243)/tmp12) + 16*tmp203*((5 + tmp204)/tmp12 + tmp251 + t&
                          &mp26) + tmp104*tmp16*(tmp208 + tmp189*(tmp210 + tmp218) + ss*(1/tmp12 + tmp149)*&
                          &tmp201*tmp311) - 10*tmp343*tmp60*tmp80 + tmp59*(tmp189*(-7*tmp142 + tmp120*tmp21&
                          &7) - 48*tmp17*(tmp213 + (8 + tmp3)/tmp12) + tmp333 + 4*(tmp211 + tmp240 + tmp18*&
                          &tmp321)*tmp98)) + tmp107*(tmp215 - 16*tmp203*(52*ss + (-7 + tmp3)/tmp12 + tmp3) &
                          &+ (tmp17*tmp189*(tmp216 + tmp18*tmp246 + tmp199*tmp74))/tmp196 - 2*tmp343*tmp60*&
                          &tmp80 + tmp104*tmp16*(tmp155*tmp217 + ss*tmp137*(tmp207 + tmp218 + tmp227) + tmp&
                          &72*tmp85) + 4*tmp198*(16*tmp17 + (-7 + tmp149)*tmp189 + tmp25*(-8 + tmp3) + 52*s&
                          &s*tmp3 + 4*(9 + tmp149)*tmp98) - tmp59*(tmp189*(tmp142 - (8 + tmp202)/tmp12) + t&
                          &mp250 + 16*tmp17*(tmp3 + tmp310) + 4*(tmp137*tmp219 + tmp241 + tmp269*tmp399)*tm&
                          &p98))) + logP2L2*Pi*tmp12*tmp149*tmp196*tmp2*tmp220*tmp221*tmp33*tmp36*tmp69*((t&
                          &mp3*tmp300*tmp351)/(tmp196*tmp69) + 4*tmp259*(tmp187 + (tmp31*tmp329)/tmp12 + (2&
                          &*(tmp210*tmp3 + tmp61))/tmp196 + tmp355*tmp78) - 2*tmp15*(192*tmp272 - 16*tmp212&
                          &*(tmp245 + tmp303 + tmp363) + (ss*tmp3*(tmp368 + ss*tmp213*tmp25*(1 - 3*tmp343 +&
                          & tmp369) + tmp17*tmp285*(tmp241 + 6*tmp317 + tmp370) - 20*tmp360*tmp60))/tmp196 &
                          &- 2*tmp59*(2*tmp156*(3 + tmp318 - tmp343) + tmp371 + ss*tmp189*(28/tmp12 + tmp24&
                          &4 + 2*tmp317 + tmp194*(3 + tmp372)) - 4*tmp17*(-7*tmp18 + tmp338 + tmp374 + ((-4&
                          & + kappa)*tmp399)/tmp12) - 16*(-19*tmp3 + tmp373)*tmp60) + 2*tmp198*(tmp375 - 16&
                          &*tmp17*(tmp352 + tmp377) + tmp189*((36 + (6 - 10*kappa)*tmp3)/tmp12 + tmp13*tmp3&
                          &78) + ((13 + tmp149)*tmp189 + tmp266 + tmp338)*tmp74) - 4*tmp203*(tmp189*(41 + t&
                          &mp232) + tmp364 + tmp365 + tmp366 + (tmp328 + tmp367)*tmp75) + (tmp17*(10*tmp17*&
                          &tmp31 + tmp385)*tmp80)/tmp12 + (tmp17*tmp189*(tmp3 + (-20 + (-6 + 26*kappa)*tmp3&
                          &)/tmp12) - 40*tmp279*tmp355 + tmp379 + tmp104*tmp156*(2 + tmp380 + tmp381) - 4*(&
                          &-13*tmp18 + (tmp18*(10 + tmp281) + tmp3)/tmp12 + tmp338)*tmp60)*tmp84) - 2*tmp22&
                          &6*(192*tmp198 + tmp188*(1/tmp12 + tmp223 + tmp352 + tmp353) + tmp29*(7*tmp18 + t&
                          &mp189*(-4 + (-7 + tmp275)*tmp3) + tmp354 + tmp223*tmp355) + (tmp3*(tmp223*tmp360&
                          & + (tmp13 - 9*tmp317 + tmp389)/tmp12))/tmp196 - 10*tmp31*tmp80*tmp98) + tmp229*(&
                          &tmp357 + (tmp322*(-20*tmp17*tmp31 + tmp398))/tmp12 + tmp188*(tmp194 + (tmp18*(-4&
                          & + tmp246))/tmp12 + tmp249 + ss*tmp147*tmp292 - 28*ss*tmp3 + tmp338 + tmp61) + 6&
                          &4*tmp198*(tmp309/tmp12 + tmp358 + tmp75) + tmp29*((3 + tmp197)*tmp25*tmp336 + tm&
                          &p249*tmp355 + (tmp189*(-5 + (-6 + tmp275)*tmp3) + tmp338 + tmp361)*tmp74 + tmp80&
                          & + (tmp61 - 8*tmp31*tmp80)/tmp12) + (tmp3*(tmp189*((6 + kappa*tmp247)/tmp12 + tm&
                          &p26 + tmp334) + tmp249*tmp360 + 4*(tmp244 + tmp370 + tmp31*tmp392)*tmp98))/tmp19&
                          &6) + (2*tmp20*(tmp396 - 16*tmp212*(tmp213 + tmp303 + tmp397) + (ss*tmp3*(tmp402 &
                          &+ (tmp17*(tmp389 + 5*tmp403))/tmp12 + ss*tmp194*(tmp149 - (1 + tmp318)/tmp12 + t&
                          &mp404) + 4*tmp360*tmp60))/tmp196 + (tmp17*(tmp123*tmp31 + tmp398)*tmp80)/tmp12 +&
                          & tmp198*(((23 + tmp159)*tmp18)/tmp12 + tmp25*((3 - 17*kappa)*tmp18 + 30*tmp3) + &
                          &tmp327*(tmp137*tmp267 + tmp367) - 256*tmp60 + (27*tmp189 + tmp315 + (tmp294*tmp3&
                          &99)/tmp12 + tmp408)*tmp74 + 4*tmp80) + tmp59*(64*tmp279 + (tmp18*(tmp26 + tmp319&
                          & + tmp405))/tmp12 - 4*tmp17*(-31*tmp18 + tmp18*tmp269*tmp280 + tmp315 + tmp406) &
                          &- 16*((6 + tmp213)/tmp12 + tmp331)*tmp60 + ss*(tmp25*tmp3*(-50 + (-9 + 35*kappa)&
                          &*tmp3) + (tmp266 + tmp31*tmp329)/tmp12 + 12*tmp80)) + ss*tmp16*(((tmp13 + tmp334&
                          & + tmp38*(-2 + tmp380))*tmp399)/tmp12 + tmp23*(-9*tmp18 + tmp354 + (tmp3 + tmp18&
                          &*tmp407)/tmp12) + 16*tmp355*tmp60 + ss*(tmp25*((9 - 19*kappa)*tmp18 + 26*tmp3) -&
                          & 16*tmp80 + (tmp408 - 3*tmp31*tmp80)/tmp12)) + 4*tmp203*(60*ss*tmp3 - tmp189*(22&
                          & + tmp3) + tmp365 + tmp399 + tmp401 - 8*tmp400*tmp98)))/tmp220 + tmp113*(128*tmp&
                          &212 + 32*tmp203*tmp384 + 8*tmp198*(4*tmp18 + tmp327 + (tmp18 + tmp218 + tmp339)/&
                          &tmp12 + tmp386 + tmp388) - 2*tmp59*(-48*tmp17*(tmp204 + tmp306/tmp12) + tmp26*(t&
                          &mp287 + tmp25*(38 + (9 + tmp263)*tmp3) + tmp189*(6 + tmp244*tmp31)) + tmp391 + (&
                          &(tmp142 + tmp3)/tmp12 + tmp338 + tmp392)*tmp74) + tmp16*((tmp18*(-10/tmp12 + tmp&
                          &3 + tmp313))/tmp12 - 24*tmp17*(tmp147*tmp189 + tmp287 + tmp354) - 160*tmp355*tmp&
                          &60 + (tmp393 + tmp137*tmp18*tmp394 + tmp25*tmp3*(8 + tmp3*(3 + tmp395)))*tmp74) &
                          &+ (20*tmp17*tmp31 + tmp385)*tmp80*tmp98 + (ss*(20*tmp17*tmp360 + ss*tmp120*(tmp1&
                          &39 + tmp18 + tmp213 + tmp389) + tmp189*(tmp100 + tmp390 + tmp405*tmp53))*tmp99)/&
                          &tmp196)) + 4*logF2N2*Pi*tmp12*tmp2*tmp220*tmp221*tmp317*tmp36*tmp41*((16*tmp258)&
                          &/tmp28 - 4*tmp259*(80*tmp16 + (4*(tmp222 + tmp223 + tmp224 + tmp225))/tmp196 + (&
                          &(5 + tmp197)/tmp12 + tmp299)*tmp3) + (tmp20**5*tmp247*tmp33)/(tmp12*tmp196) + tm&
                          &p21*(112*tmp212 - 4*tmp203*(92*ss + tmp244 + (3 + 22*tmp3)/tmp12) + tmp198*(256*&
                          &tmp17 + ss*(-5 + tmp240)*tmp292 + 92*ss*tmp3 + (tmp13 + tmp18*(7 + tmp296) + (17&
                          & + tmp13)*tmp38)/tmp12) + (tmp17*(tmp189*(tmp142 + (-5 + tmp232)/tmp12) + ss*tmp&
                          &137*(tmp137*tmp238 + tmp241 + tmp243) + tmp23*((9 + tmp239)/tmp12 + tmp244) - 16&
                          &*tmp60))/tmp196 + (tmp23 + tmp112*(9 + tmp231) - tmp236)*tmp3*tmp60 + tmp10*tmp1&
                          &6*(-(tmp189*tmp254) + ss*tmp137*((-5 + tmp13)*tmp137 + tmp245 + tmp18*tmp289) + &
                          &112*tmp60 + (tmp213 + (9 + tmp240)/tmp12)*tmp72) + tmp59*(-(tmp189*(tmp142 + (17&
                          & + tmp232)/tmp12)) + tmp255 + ss*(tmp222 + tmp257 + tmp18*(4 + tmp270))*tmp285 +&
                          & (tmp247 + (11 + tmp248)/tmp12)*tmp72)) + tmp226*((-((27 + tmp227)/tmp12) + tmp2&
                          &47 + tmp303)*tmp47 + 512*tmp59 + (4*(tmp304 + (-(tmp238/tmp12) + 27*tmp3 + tmp37&
                          &*tmp61)/tmp12 + (tmp213 + (21 + tmp239)/tmp12)*tmp71))/tmp196 + tmp3*(tmp236 + t&
                          &mp307 + 4*(21 + tmp231)*tmp98)) - 2*tmp58*(224*tmp203 - 8*tmp198*(40*ss + tmp137&
                          &*(-13 + tmp241) + tmp244) + ((tmp189*(tmp142 + (19 + tmp232)/tmp12) + tmp136*((1&
                          &3 + tmp239)/tmp12 + tmp3) + ss*tmp285*(tmp120*tmp238 + tmp149*(-6 + tmp271*tmp3)&
                          &) + 80*tmp60)*tmp8)/tmp196 + tmp140*(tmp249 + tmp223*tmp3 + ((42 + tmp241)/tmp12&
                          & + tmp18*(4 + tmp291) + tmp331)/tmp12 - 16*tmp219*tmp98) + tmp17*tmp3*(40*tmp17 &
                          &+ tmp235*tmp280 - 4*(13 + tmp231)*tmp98) - tmp16*(16*tmp17*((-12 + tmp227)/tmp12&
                          & + tmp227) + tmp250 + (tmp142 + (14 + tmp232)/tmp12)*tmp353 - 4*(tmp247 + (38 + &
                          &tmp248)/tmp12 + 3*tmp18*tmp37)*tmp98)) - (tmp107*(128*tmp203 - 16*tmp198*(tmp149&
                          & + (5 + tmp245)/tmp12 + tmp251) + (tmp104*(tmp189*tmp254 + ss*tmp137*(tmp238/tmp&
                          &12 + tmp241 - 4*tmp317) + (1/tmp12 + tmp149 + tmp194)*tmp72))/tmp196 - tmp17*tmp&
                          &189*(tmp235 + (1 + tmp197)*tmp74) + tmp140*(tmp365 + ((20 + tmp13)/tmp12 + tmp22&
                          &7 + tmp330*tmp61)/tmp12 + tmp74*(1/tmp12 + tmp248 + tmp94)) - tmp16*(tmp189*(tmp&
                          &142 + (20 + tmp232)/tmp12) + tmp255 - 16*tmp17*((-9 + tmp3)/tmp12 + tmp336) + 4*&
                          &(tmp257 + tmp3 + tmp361*tmp37)*tmp98)))/tmp220 - 4*tmp229*(32*tmp198 + ss*(-40*t&
                          &mp17 + ss*tmp137*(17 + tmp231) + tmp236)*tmp3 - 8*tmp59*(-((-23 + tmp3)/tmp12) +&
                          & tmp79) - 2*tmp16*(32*tmp17 + ss*tmp247 + (22/tmp12 + tmp18 + tmp230 + tmp276 + &
                          &tmp374)/tmp12 + 4*(29 + tmp13)*tmp98) + (tmp189*(tmp142 + (11 + tmp232)/tmp12) +&
                          & tmp237 + ss*tmp137*(tmp238*tmp285 + tmp3*(29 + tmp53)) + tmp136*((17 + tmp239)/&
                          &tmp12 + tmp99))/tmp196)) + (2*logF2P2*Pi*tmp12*tmp196*tmp2*tmp220*tmp221*tmp36*(&
                          &4*tmp258*(-4*(1/tmp196)**1.5 + Sqrt(1/tmp196)*tmp3)**2 + (ss*tmp18*tmp300*tmp33*&
                          &tmp351)/tmp196 + (2*tmp226*((-2*(20*tmp147*tmp17 + tmp262 + ss*(12/tmp12 + tmp24&
                          &8 + tmp266)))/tmp196 + ss*(tmp139 + 10*ss*tmp147 + (6 - 2*tmp159)/tmp12)*tmp3 - &
                          &2*tmp16*(tmp219*tmp326 + 5*(tmp3 + tmp38)) + 40*tmp59))/(tmp196*tmp28) - (4*tmp2&
                          &59*(tmp10*(-4 + tmp3) + tmp68))/(tmp196*tmp36) + tmp229*(tmp104*tmp25*tmp264*tmp&
                          &265 + 128*tmp203*(5/tmp12 + ss*tmp3) + (ss*tmp18*(40*tmp17*tmp3 + tmp142*(tmp26 &
                          &+ tmp285 + 8*tmp25*tmp31) + (tmp142 + (-2 + tmp278)/tmp12)*tmp71))/tmp196 + ss*t&
                          &mp16*tmp358*(tmp249*tmp3 - (4*(tmp18 + tmp3))/tmp12 + tmp329 + tmp332*tmp398 + (&
                          &tmp18*(7 + tmp281) + (-1 + tmp159)*tmp38)*tmp71) + 64*tmp198*(tmp174 + tmp261 + &
                          &tmp262 + tmp73) + 8*tmp59*(tmp273 + 80*tmp3*tmp60 + (tmp137 + tmp18*(-8 + tmp263&
                          &))*tmp72 + ss*(tmp213*tmp216 + tmp305*tmp80))) - (2*tmp20*(32*tmp283 - 16*tmp272&
                          &*(tmp104*tmp284 + tmp285 + tmp3) + 2*tmp212*(tmp18 + tmp247/tmp12 - 8*ss*(tmp222&
                          & + tmp287 + tmp292) + 32*tmp17*tmp400) + tmp138*tmp264*tmp265*tmp60 + (tmp17*tmp&
                          &399*(tmp17*(tmp142 + (1 + tmp159)*tmp285) + tmp293 + ss*tmp276*(1/tmp12 + tmp149&
                          & + 2*tmp25*tmp31) + 2*tmp4*tmp60))/tmp196 + tmp16*tmp17*tmp3*(16*tmp4*tmp60 + tm&
                          &p123*(tmp222 + tmp278/tmp12 + tmp292 + tmp294*tmp61) + kappa*(tmp13 + 4*tmp25*tm&
                          &p295)*tmp80 + ss*(tmp194*(-4 + tmp297) + 16*tmp18*tmp398 + (16 + tmp296)*tmp80))&
                          & + ss*tmp198*(tmp290 + (tmp287*(4 + tmp297))/tmp12 - 16*tmp17*(tmp292 + tmp26*(1&
                          &0 + (1 + tmp291)*tmp3)) + 32*(5 + tmp213)*tmp60 - tmp288*tmp65 + tmp74*(-24*tmp1&
                          &89 + tmp137*tmp317 + tmp361 + (7 + tmp335)*tmp80)) + tmp10*tmp59*(tmp17*((tmp247&
                          &*(8 + tmp298))/tmp12 + 4*tmp18*(5 + tmp3*(14 + tmp395))) + 32*tmp279*tmp4 - 16*(&
                          &tmp137 + (-7 + tmp165)*tmp18 + tmp222)*tmp60 + kappa*(tmp26 + tmp285 + tmp319)*t&
                          &mp65 + ss*(tmp137*(-6 + tmp159)*tmp18 + 3*(4 + tmp263)*tmp65 + 32*tmp398*tmp80))&
                          & - 2*tmp203*(ss*tmp18*(5 + (-5 + tmp165)*tmp3) + tmp17*(-96/tmp12 + 80*tmp3 + (-&
                          &3 + tmp286)*tmp332) + tmp399/tmp12 + 32*tmp340*tmp60 + tmp213*(-8 + tmp298)*tmp9&
                          &8)))/tmp220 + 2*tmp15*(-32*(tmp149 + 3*ss*tmp201 + tmp224)*tmp272 + 128*tmp283 +&
                          & 8*tmp212*(tmp18 + 16*tmp17*tmp267 + ss*(-48/tmp12 + 10*tmp18 + 24*tmp3) + tmp35&
                          &2/tmp12) + 3*tmp25*tmp264*tmp265*tmp60 + (tmp17*tmp18*(tmp293 + tmp17*(tmp276 + &
                          &(-6 + tmp247*tmp31)/tmp12) + tmp317*tmp326*tmp398 + 2*tmp277*tmp60))/tmp196 + 2*&
                          &tmp203*(tmp273 + 16*tmp17*(tmp18*(2 + tmp274) + tmp311 + tmp358) + 32*(4 + tmp13&
                          &)*tmp60 + tmp10*(12*tmp18 - 96*tmp189 + (tmp31*tmp332)/tmp12 + (7 + tmp275)*tmp8&
                          &0)) + ss*tmp114*(16*tmp277*tmp279 + ss*(tmp282 + tmp290 + tmp18*tmp311) + 16*(tm&
                          &p248 + tmp280 + tmp18*tmp288)*tmp60 - 2*tmp200*tmp398*tmp65 + tmp17*(((4 + tmp15&
                          &9)*tmp247)/tmp12 + tmp332 + (18 + tmp281)*tmp80)) + ss*tmp16*(8*tmp277*tmp279 - &
                          &4*((6 + tmp278)/tmp12 + tmp26*(3 + (1 + tmp275)*tmp3))*tmp60 + tmp25*tmp264*tmp6&
                          &5 + ss*tmp295*tmp404*tmp80 + tmp17*((2 + tmp159)*tmp285*tmp3 + 24*tmp18*tmp398 +&
                          & kappa*tmp80))*tmp99 + tmp198*tmp8*((12*tmp18)/tmp12 + tmp282 + tmp322/tmp12 + 1&
                          &92*tmp4*tmp60 - 2*tmp343*tmp80 - 8*tmp398*tmp80 + tmp74*(12*tmp189 + tmp287 + (3&
                          & - 6*kappa)*tmp80) - 32*tmp17*(tmp137 + tmp18*tmp269 + tmp99))) - 2*tmp58*(3*tmp&
                          &17*tmp25*tmp264*tmp265 + 160*tmp272 - 4*tmp198*(ss*tmp18*(2 + tmp227) + tmp266/t&
                          &mp12 + ss*tmp292*tmp3*tmp309 + 16*tmp14*tmp60 + (tmp137 + tmp26 + tmp18*(4 + tmp&
                          &274))*tmp72) - 16*tmp212*(5*(-4/tmp12 + tmp3) + tmp267*tmp74) + ss*tmp16*tmp26*(&
                          &tmp17*(tmp240 + 12*tmp18*(-5 + tmp270) - (16*(2 + tmp325))/tmp12) + ss*tmp213*(1&
                          &/tmp12 - tmp189*tmp271 - 4*tmp18*tmp37 + 12*tmp3*tmp398) + 80*tmp268*tmp60 + kap&
                          &pa*(tmp260 + tmp313)*tmp80) - 2*tmp203*(80*tmp189 + tmp266 - 16*ss*tmp267*tmp3 +&
                          & 32*tmp17*tmp4 + 96*tmp98) + ss*tmp140*(tmp18*tmp280 + (tmp322*tmp37)/tmp12 + tm&
                          &p393*tmp398 + ss*(1 + (7 + tmp286)*tmp3)*tmp399 + 40*tmp268*tmp60 + tmp37*tmp65 &
                          &+ tmp213*(2 + tmp356)*tmp98 + tmp136*(tmp137 + (6 - 9*kappa)*tmp18 + tmp99)) + (&
                          &tmp18*tmp191*(5*tmp17*tmp268 + (1/tmp12 + tmp31*tmp353)*tmp8 + tmp142*(tmp1 + 6*&
                          &tmp25*tmp31 + tmp99)))/tmp196)))/ss + logF2L2*Pi*tmp12*tmp149*tmp196*tmp2*tmp220&
                          &*tmp221*tmp33*tmp36*tmp41*(32*tmp258*(tmp26/tmp196 + tmp29) - 8*tmp259*((tmp3*(t&
                          &mp120 + tmp204 + tmp299 + tmp316))/tmp196 + tmp29*(tmp223 + tmp397) + 48*tmp59) &
                          &+ (tmp189*tmp300*(tmp157 + tmp159*tmp188 + tmp302 + (16*tmp159*tmp17 + tmp26*tmp&
                          &260 + tmp260*tmp71)/tmp196 + tmp29*(tmp137 + tmp3*(1 - 8*kappa*ss + tmp75))))/tm&
                          &p196 + tmp229*(-8*tmp16*(((6 + tmp159)*tmp18)/tmp12 + tmp237 + tmp329 + tmp136*(&
                          &(11 + tmp227)/tmp12 + tmp331) + ss*(tmp194*(-5 + tmp197) - 8*tmp25*tmp306 + tmp3&
                          &32) + tmp25*tmp3*(3 + tmp345)) + tmp357 + 64*tmp198*tmp384 + tmp188*(tmp327 + ((&
                          &-3 + tmp165)*tmp18 + tmp213 + tmp306*tmp38)/tmp12 + (tmp222 + tmp328)*tmp74) + (&
                          &tmp26*(tmp333 + tmp189*(tmp26 + tmp311 + tmp334) + 16*tmp17*((11 + tmp312)/tmp12&
                          & + tmp352) + (tmp189*(11 + tmp337) + 2*tmp25*(6 + tmp348) + tmp364)*tmp74))/tmp1&
                          &96 - 2*(1/tmp12 + tmp326)*tmp398*tmp80) + 2*tmp226*(128*tmp198 + tmp188*(tmp248 &
                          &+ tmp303 + tmp363) + (tmp3*((7 + tmp159)*tmp189 + tmp287 + tmp25*(6 + (1 + 17*ka&
                          &ppa)*tmp3) + tmp307 + (-12*tmp3 + (13 + tmp312)/tmp12)*tmp74))/tmp196 + 4*tmp398&
                          &*tmp80 + tmp29*(32*ss*tmp3 + tmp304 + tmp189*(4 + tmp3*tmp305) + tmp320 + tmp401&
                          & - 4*(13 + tmp227)*tmp98)) + 2*tmp15*(-16*tmp212*(1/tmp12 + tmp223 + tmp336 + tm&
                          &p353) + tmp396 + (ss*tmp3*(16*tmp279 - 12*ss*tmp25*tmp3*(1 + tmp318 + tmp343) + &
                          &tmp123*(tmp332 + tmp189*tmp341 + 2*tmp25*(6 + tmp345)) + tmp368 - 4*((7 + tmp312&
                          &)/tmp12 + tmp344)*tmp60))/tmp196 + tmp114*(2*tmp156*(-3 + tmp343) - 4*tmp17*(11*&
                          &tmp18 + tmp338 + tmp189*(14 + tmp369)) + tmp371 + ss*(tmp149*tmp25*(14 + (1 - 11&
                          &*kappa)*tmp3) + tmp342 + tmp393) - 16*(tmp339 + tmp373)*tmp60) + tmp17*tmp329*tm&
                          &p398*(tmp120 + tmp74) + 4*tmp203*(-3*tmp18 + tmp189*(11 + (-16 + tmp296)*tmp3) +&
                          & tmp304 + tmp366 + (tmp367 + tmp147*tmp38)*tmp74) + (-32*ss**5 + 8*tmp279*((7 + &
                          &tmp227)/tmp12 + tmp346) + tmp379 + tmp104*tmp156*(-2 + tmp380 + tmp381) + 4*(tmp&
                          &189*(15 + 2*tmp232) + tmp287 + tmp338)*tmp60 + tmp17*(tmp342 + tmp149*tmp25*(18 &
                          &+ tmp348) + 14*tmp80))*tmp84 - 2*tmp198*(-16*tmp17*(tmp247 + tmp120*tmp306) + tm&
                          &p322 + tmp342 + tmp391 + (tmp189*(3 + (-4 + tmp270)*tmp3) + tmp338 + tmp392)*tmp&
                          &74 + tmp25*(6 + (-1 + tmp246)*tmp3)*tmp99)) - 2*tmp58*(192*tmp212 - 16*tmp203*(t&
                          &mp204 + tmp303 + tmp137*tmp309) - 8*tmp198*(3*tmp18 + ((7 - 8*kappa)*tmp18)/tmp1&
                          &2 + tmp327 + tmp344/tmp12 + tmp386 + tmp388) + tmp114*(tmp189*((2 + (-3 + tmp263&
                          &)*tmp3)/tmp12 - 3*tmp317 + tmp346) + tmp375 - 16*tmp17*(tmp247 + tmp377) + (tmp1&
                          &89*(13 + (-2 + tmp246)*tmp3) + tmp338 + tmp399)*tmp74) - 3*ss*tmp308*tmp398*tmp8&
                          &0 + tmp16*(-320*tmp279 + (tmp18*(tmp3 + tmp137*(-5 + tmp275*tmp3) + tmp313))/tmp&
                          &12 + 32*((9 + tmp227)/tmp12 + tmp339)*tmp60 + (tmp266 + tmp315 + tmp189*(19 + tm&
                          &p3 + tmp318))*tmp72 + tmp213*(tmp139 + tmp18 + tmp247 + (4 + tmp3*(-1 + tmp372))&
                          &/tmp12)*tmp98) + (ss*tmp149*(ss*tmp280*((1 + 9*kappa)*tmp189 + tmp213 + tmp311 +&
                          & tmp317) - 4*tmp17*((9 + tmp312)/tmp12 + tmp344) + 40*tmp60 - tmp189*(tmp390 + (&
                          &5 + 14*tmp53)/tmp12 + tmp99)))/tmp196) + (2*tmp20*(16*tmp212*tmp314 - 4*tmp203*(&
                          &tmp303*tmp314 + (tmp149 + tmp18*(-9 + tmp286) + tmp328)/tmp12) - tmp17*tmp379 - &
                          &(tmp250*tmp314 + (tmp18*(tmp26 + tmp285*(3 + tmp318) + tmp319))/tmp12 + tmp23*(t&
                          &mp315 + tmp320 + tmp189*(9 + tmp3*tmp321)) + 3*ss*(tmp322 + tmp25*tmp3*tmp323 + &
                          &(tmp18*tmp378)/tmp12))*tmp59 + tmp198*((-18 + tmp197)*tmp25*tmp3 + tmp322 + tmp3&
                          &14*tmp365 + (tmp18*tmp394)/tmp12 + (tmp287 + tmp189*(7 + (-11 + 15*kappa)*tmp3) &
                          &+ tmp315)*tmp74) + ss*tmp16*((tmp18*(tmp13 + tmp334 + tmp247*tmp343))/tmp12 + tm&
                          &p23*(tmp320 + tmp354 + tmp406 + (tmp18*tmp407)/tmp12) + 16*tmp314*tmp60 + ss*(tm&
                          &p13*(14 + tmp197)*tmp25 + (tmp18*(19 + tmp325))/tmp12 + 6*tmp80)) + (ss*tmp26*(t&
                          &mp402 + 4*(tmp100 + tmp316)*tmp60 + tmp17*(tmp25*tmp323 + tmp403/tmp12 + tmp61) &
                          &+ ss*tmp194*(tmp149 + tmp404 + (3 + kappa*tmp99)/tmp12)))/tmp196))/tmp220)

  END FUNCTION MP2MPL_MP_FIN_D13D20

  FUNCTION MP2MPL_MP_FIN_D10D21(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_FIN_d10d21
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129

  tmp1 = -1 + kappa
  tmp2 = sqrt(lambda)**2
  tmp3 = -ss
  tmp4 = me2**2
  tmp5 = -2*me2*mm2
  tmp6 = mm2**2
  tmp7 = 1/tt
  tmp8 = 5*me2
  tmp9 = -tmp2
  tmp10 = 4*mm2
  tmp11 = tmp10 + tmp9
  tmp12 = 1/tmp11
  tmp13 = 4/tmp7
  tmp14 = mm2 + tmp13 + tmp3 + tmp8
  tmp15 = 2*me2
  tmp16 = -(1/tmp7)
  tmp17 = sqrt(lambda)**4
  tmp18 = -2*me2*ss
  tmp19 = ss**2
  tmp20 = tmp7**(-2)
  tmp21 = mm2 + tmp3
  tmp22 = tmp21**2
  tmp23 = -tmp22
  tmp24 = tmp23 + tmp4
  tmp25 = mm2 + ss
  tmp26 = -2*me2*tmp25
  tmp27 = tmp22 + tmp26 + tmp4
  tmp28 = tmp15 + 1/tmp7
  tmp29 = 1/tmp27
  tmp30 = -2*ss
  tmp31 = ss + tmp16
  tmp32 = ss*tmp31
  tmp33 = -2*mm2
  tmp34 = tmp30 + tmp33 + 1/tmp7
  tmp35 = me2*tmp34
  tmp36 = 2*ss
  tmp37 = tmp36 + 1/tmp7
  tmp38 = -(mm2*tmp37)
  tmp39 = tmp32 + tmp35 + tmp38 + tmp4 + tmp6
  tmp40 = 2/tmp7
  tmp41 = tmp21 + tmp40 + tmp8
  tmp42 = -4*ss
  tmp43 = tmp10 + tmp16
  tmp44 = 1/tmp43
  tmp45 = 1/tmp20
  tmp46 = 2*tmp4
  tmp47 = 2*tmp6
  tmp48 = -3/tmp7
  tmp49 = tmp10 + tmp42 + tmp48
  tmp50 = me2*tmp49
  tmp51 = tmp42 + 1/tmp7
  tmp52 = mm2*tmp51
  tmp53 = 3/tmp7
  tmp54 = tmp36 + tmp53
  tmp55 = ss*tmp54
  tmp56 = tmp46 + tmp47 + tmp50 + tmp52 + tmp55
  tmp57 = me2 + tmp21
  tmp58 = 32*tmp6
  tmp59 = 1/tmp7 + tmp9
  tmp60 = 4*ss*tmp2
  tmp61 = 2*mm2
  tmp62 = ss/tmp7
  tmp63 = 4*ss
  tmp64 = tmp29**2
  tmp65 = 3*ss
  tmp66 = me2**3
  tmp67 = mm2**3
  tmp68 = me2**5
  tmp69 = me2**4
  tmp70 = ss + tmp48
  tmp71 = ss**3
  tmp72 = -2/tmp7
  tmp73 = 17/tmp7
  tmp74 = tmp2*tmp40
  tmp75 = tmp2*tmp30
  tmp76 = tmp62 + tmp74 + tmp75
  tmp77 = tmp65/tmp7
  tmp78 = tmp16*tmp19
  tmp79 = 32*ss
  tmp80 = tmp15 + tmp21
  tmp81 = tmp2/tmp7
  tmp82 = 1/tmp59
  tmp83 = (f2p*tmp16*tmp80)/(tmp44*tmp82)
  tmp84 = tmp16 + tmp2
  tmp85 = tmp20*tmp84
  tmp86 = 4*me2*tmp2
  tmp87 = tmp20 + tmp81 + tmp86
  tmp88 = tmp10*tmp87
  tmp89 = tmp85 + tmp88
  tmp90 = tmp1*tmp57*tmp89*tmp9
  tmp91 = tmp83 + tmp90
  tmp92 = tmp53 + tmp63
  tmp93 = 2*tmp62
  tmp94 = 2*tmp67
  tmp95 = -5/tmp7
  tmp96 = 2*tmp19
  tmp97 = 3*tmp20
  tmp98 = -mm2
  tmp99 = 1/me2
  tmp100 = 8*ss
  tmp101 = -3*tmp2
  tmp102 = mm2**4
  tmp103 = 12*tmp19
  tmp104 = 7*tmp17
  tmp105 = 4*tmp19
  tmp106 = 4*tmp81
  tmp107 = tmp2 + tmp40
  tmp108 = -tmp17
  tmp109 = 4*tmp2
  tmp110 = -40*ss
  tmp111 = 22*tmp62
  tmp112 = 24*tmp20
  tmp113 = -16*tmp2
  tmp114 = 24/tmp7
  tmp115 = mm2**5
  tmp116 = 24*ss
  tmp117 = 16*tmp19
  tmp118 = -8*tmp2
  tmp119 = 9/tmp7
  tmp120 = -4*tmp2
  tmp121 = -22/tmp7
  tmp122 = tmp121 + tmp2
  tmp123 = tmp122*tmp81
  tmp124 = -2*tmp2
  tmp125 = tmp20 + tmp93 + tmp96
  tmp126 = 12*tmp20
  tmp127 = 2*tmp2
  tmp128 = 11*tmp2
  tmp129 = 48*tmp71
  MP2MPL_MP_FIN_d10d21 = -8*logmut*Pi*(f2p + tmp1*tmp127) + 8*C0tF2F20F0*Pi*(f2p*tmp16 + tmp1*tmp2*tmp28)&
                          & + 16*logbeN2*Pi*tmp1*tmp12*tmp17*tmp45*tmp56 - 8*logmuN2*Pi*tmp1*tmp12*tmp14*tm&
                          &p17*tmp7 + 32*logmuL2*mm2*Pi*tmp1*tmp12*tmp14*tmp2*tmp7 + logmuP2*Pi*tmp1*tmp120&
                          &*tmp41*tmp7 + 16*C0tP2P2NP0*Pi*tmp1*tmp12*tmp17*(tmp18 + tmp19 + tmp20 + tmp4 - &
                          &5*tmp6 + tmp62 + mm2*(tmp16 + tmp63))*tmp7 - 8*C0IR6sFP*Pi*(tmp1*tmp2*tmp57 + 2*&
                          &f2p*tmp80) + (64*logbeL2*mm2*Pi*tmp1*tmp12*tmp2*tmp44*tmp45*tmp56)/tmp82 + 4*log&
                          &muF2*Pi*tmp7*(f2p*tmp40 + tmp1*tmp41*tmp9) + 16*DB0sFP*logmuF2*Pi*ss*tmp29*tmp44&
                          &*tmp7*tmp82*tmp91 + DB0sFP*logF2P2*Pi*tmp100*tmp29*tmp44*tmp7*tmp82*tmp91 - 8*lo&
                          &gP2t*Pi*tmp44*(tmp15 + tmp16 + tmp30 + tmp61)*tmp7*(f2p/tmp7 + tmp1*tmp127*(tmp1&
                          &6 + tmp21 + 1/tmp99)) + 16*DB0sFPlogmut*Pi*ss*tmp29*tmp7*(tmp1*tmp57*tmp81 + f2p&
                          &*(-4*tmp4 + (ss + tmp98)/tmp7 - (4*tmp21)/tmp99)) + 8*C0tP2P20P0*Pi*tmp44*(tmp1*&
                          &tmp124*(tmp19 + tmp20 + tmp4 + mm2*tmp48 - tmp6 + tmp62 - (2*(ss + tmp61))/tmp99&
                          &) + f2p*(tmp20 - 4*tmp6 + mm2*(tmp63 + tmp72) + (2*(-6*mm2 + 1/tmp7))/tmp99)) - &
                          &(16*DB0sFP*f2p*Pi*(tmp4 + tmp5 + tmp6 + ss*tmp98 + tmp3/tmp99))/(tmp18 + tmp19 +&
                          & ss*tmp33 + tmp4 + tmp5 + tmp6) + 8*D0IR6tsN*Pi*tmp1*tmp12*tmp17*tmp7*(32*mm2*tm&
                          &p4 + tmp21*(mm2*tmp13 + tmp20 + tmp58) + (tmp20 - 4*mm2*(tmp100 + tmp48) + tmp58&
                          &)/tmp99) + 8*log4*Pi*tmp1*tmp12*tmp17*tmp64*tmp7*((mm2*tmp37 + tmp36*(11*ss + tm&
                          &p40) + 14*tmp6)*tmp66 + 3*tmp68 - tmp69*(11*mm2 + 13*ss + 1/tmp7) + tmp22*((-13*&
                          &ss + tmp13)*tmp6 + tmp67 - tmp19*(ss + 1/tmp7) + mm2*ss*tmp70) + tmp4*(-6*tmp67 &
                          &+ 5*tmp6*(tmp63 + 1/tmp7) - 6*tmp19*(tmp65 + 1/tmp7) + mm2*ss*(tmp48 + tmp79)) -&
                          & (tmp21*((7*ss + tmp13)*tmp19 + (-5*ss + tmp119)*tmp6 + tmp67 + mm2*ss*(-19*ss +&
                          & 7/tmp7)))/tmp99) + 8*C0tF2F2NF0*Pi*tmp1*tmp12*tmp17*tmp7*(tmp20 + (2*(tmp10 + 1&
                          &/tmp7))/tmp99) - 32*C0tF2F2LF0*mm2*Pi*tmp1*tmp12*tmp2*tmp7*(tmp20 + (2*(tmp2 + 1&
                          &/tmp7))/tmp99) + 32*C0tP2P2LP0*mm2*Pi*tmp1*tmp12*tmp2*tmp44*tmp7*(tmp124*tmp19 +&
                          & tmp20*tmp36 - 2*tmp6*(-5*tmp2 + 1/tmp7) + 2/tmp7**3 + tmp46/tmp82 + tmp20*tmp9 &
                          &+ tmp96/tmp7 + tmp33*(tmp60 + tmp97) + (tmp60 - 4*tmp62 - (8*mm2)/tmp7 + tmp74)/&
                          &tmp99) + 8*C0sF2P2FNP*Pi*tmp1*tmp12*tmp17*tmp29*tmp7*((16*mm2 + tmp16)*tmp66 + t&
                          &mp4*(-24*tmp6 + mm2*(tmp110 + tmp73) + tmp77) + tmp21*(ss*tmp61*(tmp48 + tmp63) &
                          &+ 8*tmp67 + tmp6*(-16*ss + 15/tmp7) + tmp78) - (mm2*(tmp111 - 32*tmp19) + tmp19*&
                          &tmp53 + tmp6*(31/tmp7 + tmp79))/tmp99) - 32*D0IR6tsL*mm2*Pi*tmp1*tmp12*tmp2*tmp7&
                          &*(8*tmp2*tmp4 + tmp21*(2*tmp17 + tmp20 + tmp81) + (tmp20 + 8*tmp2*tmp21 + 3*tmp8&
                          &1)/tmp99) + 32*C0sF2P2FLP*mm2*Pi*tmp1*tmp12*tmp2*tmp29*tmp7*(tmp66*(tmp120 + 1/t&
                          &mp7) + tmp21*(tmp6*(tmp124 + 1/tmp7) + ss*tmp76 + tmp33*tmp76) - tmp4*(tmp106 - &
                          &10*ss*tmp2 + mm2*(-6*tmp2 + 1/tmp7) + tmp77) + (tmp16*tmp6 + ss*(ss*tmp118 + tmp&
                          &77 + 6*tmp81) + mm2*(tmp100*tmp2 + ss*tmp72 + 8*tmp81))/tmp99) + 32*DB0P2LP*mm2*&
                          &Pi*tmp1*tmp12*tmp2*tmp29*tmp44*tmp7*(4*(3*mm2 + tmp36)*tmp66 - 2*tmp69 - tmp4*(t&
                          &mp103 + tmp20 + 16*tmp6 + mm2*(20*ss + tmp72) + tmp93) + tmp21*(ss*tmp125 + tmp9&
                          &4 - 2*tmp6*(ss + tmp95) + (tmp63/tmp7 + tmp96 + tmp97)*tmp98) + (2*((tmp19 + tmp&
                          &20)*tmp61 - 2*tmp6*tmp92 + ss*(tmp105 + tmp20 + tmp93) + tmp94))/tmp99) + 8*logF&
                          &2N2*Pi*tmp1*tmp12*tmp17*tmp64*tmp7*((7*mm2*(tmp16 + tmp36) + 28*tmp6 + tmp63*(tm&
                          &p16 + tmp65))*tmp66 + 2*tmp68 + tmp69*(-12*mm2 - 8*ss + 1/tmp7) + tmp4*(-32*tmp6&
                          &7 + mm2*ss*(tmp36 + 13/tmp7) + (6*tmp19)/tmp7 - 8*tmp71 + tmp6*(tmp36 + tmp73)) &
                          &+ tmp23*(mm2*ss*(6*ss + tmp16) + 4*tmp67 + tmp47*tmp70 + tmp78) + (18*tmp102 + 2&
                          &*tmp71*(ss + tmp72) - tmp67*(14*ss + tmp73) + tmp30*tmp6*tmp92 + mm2*tmp19*(tmp3&
                          &6 + tmp95))/tmp99) - (16*DB0F2FN*Pi*tmp1*tmp12*tmp17*tmp29*tmp39*tmp7)/tmp99 + (&
                          &64*DB0F2FL*mm2*Pi*tmp1*tmp12*tmp2*tmp29*tmp39*tmp7)/tmp99 + logF2L2*mm2*Pi*tmp1*&
                          &tmp109*tmp12*tmp7*(tmp124*(tmp15 + tmp16)*tmp24*tmp29 - 4*tmp28*(tmp15 + tmp9) +&
                          & tmp2*tmp24*tmp64*(tmp2*tmp23 - (12*mm2 + 12*ss + tmp2)*tmp4 + 6*tmp66 + (2*(mm2&
                          &*(-6*ss + tmp2) + 3*tmp6 + ss*(tmp109 + tmp65)))/tmp99))*tmp99 + logP2L2*Pi*tmp1&
                          &*tmp109*tmp12*tmp44*tmp64*tmp7*(2*tmp68*(4*(60*ss - 22*tmp2 + tmp53)*tmp6 + 160*&
                          &tmp67 + mm2*(tmp126 + 240*tmp19 - 112*ss*tmp2 + 68*tmp62 + 5*tmp81) + tmp124*(30&
                          &*tmp19 + tmp20 + tmp93)) + 2*tmp66*(80*tmp115 - 4*tmp102*(tmp116 + tmp128 + 42/t&
                          &mp7) - 4*tmp67*(tmp117 + tmp17 - 18*tmp20 + (tmp118 + tmp119)*tmp36 + tmp113/tmp&
                          &7) - 12*tmp19*tmp2*(5*tmp19 + tmp20 + tmp93) + ss*tmp61*(tmp105*(tmp120 + 21/tmp&
                          &7) + ((tmp119 + tmp2)*tmp63)/tmp7 + 120*tmp71 + tmp81*(tmp127 + tmp95)) + tmp6*(&
                          &tmp123 - 8*tmp19*(-9*tmp2 + 23/tmp7) - 160*tmp71 + 16*ss*(tmp108 + tmp97))) + (t&
                          &mp107*tmp2*tmp21**4*tmp98)/tmp44 - 4*tmp21*tmp4*(8*tmp115 + tmp102*(tmp109 + tmp&
                          &110 - 66/tmp7) + tmp47*(-5*tmp2*tmp20 + tmp19*(tmp113 + 45/tmp7) + 36*tmp71) + t&
                          &mp67*(tmp111 + tmp112 + 8*tmp19 + 20*ss*tmp2 + 31*tmp81) + tmp109*tmp19*(3*tmp19&
                          & + tmp20 + tmp93) + ss*(tmp129 + tmp127*tmp20 + (tmp114 + 7*tmp2)*tmp62 + tmp19*&
                          &(tmp109 + 46/tmp7))*tmp98) + tmp69*(-320*tmp102 - 16*tmp67*(16*ss - 13*tmp2 - 9/&
                          &tmp7) - 4*tmp6*(tmp108 + tmp112 + 80*tmp19 - 44*ss*tmp2 - 8*tmp62 + 15*tmp81) + &
                          &16*ss*tmp2*(10*tmp19 + tmp20 + tmp93) + (tmp123 + tmp117*(tmp113 + 19/tmp7) + ((&
                          &tmp114 + tmp2)*tmp63)/tmp7 + 640*tmp71)*tmp98) + 8/(tmp12*tmp99**7) - (8*(-6*ss*&
                          &tmp2 + mm2*(tmp116 + tmp118 + tmp53) + 20*tmp6))/tmp99**6 - (2*tmp22*(tmp125*tmp&
                          &127*tmp19 + 4*tmp102*(tmp119 + tmp124 + tmp63) - tmp67*(ss*tmp113 + tmp126 + 4*t&
                          &mp17 + 48*tmp19 + 92*tmp62 + 27*tmp81) + tmp6*(tmp129 + (tmp126 - 8*tmp17 + tmp1&
                          &19*tmp2)*tmp36 + tmp19*(tmp120 + 76/tmp7) + (tmp2 + 8/tmp7)*tmp81) + ss*(tmp105*&
                          &(tmp127 + 5/tmp7) + tmp62*(tmp128 + 12/tmp7) + (tmp127*(tmp124 + tmp53))/tmp7 + &
                          &16*tmp71)*tmp98))/tmp99)*tmp99 + 4*logF2P2*Pi*((2*f2p*(ss + tmp98 + 1/tmp99))/ss&
                          & + tmp1*tmp12*tmp57*tmp64*tmp7*tmp9*(mm2*tmp107*tmp2*tmp21**3 + tmp66*(-6*tmp19*&
                          &tmp2 + mm2*(tmp108 + 24*tmp19 - 26*ss*tmp2) + 4*tmp6*(tmp101 + tmp63) + 24*tmp67&
                          &) + tmp68/tmp12 - 2*tmp69*(mm2*(tmp100 + tmp101) + 8*tmp6 + tmp75) + tmp4*(-16*t&
                          &mp102 + mm2*ss*(tmp104 - 16*tmp19 + 10*ss*tmp2 + tmp124/tmp7) + tmp109*tmp71 + t&
                          &mp6*(tmp117 + 3*tmp17 + tmp74 + tmp2*tmp79) + (tmp100 + 5*tmp2)*tmp94) + (tmp21*&
                          &(4*tmp102 - 3*(tmp2 + tmp63)*tmp67 + tmp6*(tmp103 - 3*tmp17 - 13*ss*tmp2 + tmp12&
                          &0/tmp7) + tmp2*tmp71 + ss*(tmp104 + tmp105 + tmp106 + 9*ss*tmp2)*tmp98))/tmp99)*&
                          &tmp99)

  END FUNCTION MP2MPL_MP_FIN_D10D21

  FUNCTION MP2MPL_MP_FIN_D11D21(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_FIN_d11d21
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206, tmp207, tmp208, tmp209, tmp210
  real tmp211, tmp212, tmp213, tmp214, tmp215
  real tmp216, tmp217, tmp218, tmp219, tmp220
  real tmp221, tmp222, tmp223, tmp224, tmp225
  real tmp226, tmp227, tmp228, tmp229, tmp230
  real tmp231, tmp232, tmp233, tmp234, tmp235
  real tmp236, tmp237, tmp238, tmp239, tmp240
  real tmp241, tmp242, tmp243, tmp244, tmp245
  real tmp246, tmp247, tmp248, tmp249, tmp250
  real tmp251, tmp252, tmp253, tmp254, tmp255
  real tmp256, tmp257, tmp258, tmp259, tmp260
  real tmp261, tmp262, tmp263, tmp264, tmp265
  real tmp266, tmp267, tmp268, tmp269, tmp270
  real tmp271, tmp272, tmp273, tmp274, tmp275
  real tmp276, tmp277, tmp278, tmp279, tmp280
  real tmp281, tmp282, tmp283, tmp284, tmp285
  real tmp286, tmp287, tmp288, tmp289, tmp290
  real tmp291, tmp292, tmp293, tmp294, tmp295
  real tmp296, tmp297, tmp298, tmp299, tmp300
  real tmp301, tmp302, tmp303, tmp304, tmp305
  real tmp306, tmp307, tmp308, tmp309, tmp310
  real tmp311, tmp312, tmp313, tmp314, tmp315
  real tmp316, tmp317, tmp318, tmp319, tmp320
  real tmp321, tmp322, tmp323, tmp324, tmp325
  real tmp326, tmp327, tmp328, tmp329, tmp330
  real tmp331, tmp332, tmp333, tmp334, tmp335
  real tmp336, tmp337, tmp338, tmp339, tmp340
  real tmp341, tmp342, tmp343, tmp344, tmp345
  real tmp346, tmp347, tmp348, tmp349, tmp350
  real tmp351, tmp352, tmp353, tmp354, tmp355
  real tmp356, tmp357, tmp358, tmp359, tmp360
  real tmp361, tmp362, tmp363, tmp364, tmp365
  real tmp366, tmp367, tmp368, tmp369, tmp370
  real tmp371, tmp372, tmp373, tmp374, tmp375
  real tmp376, tmp377, tmp378, tmp379, tmp380
  real tmp381, tmp382, tmp383, tmp384, tmp385
  real tmp386, tmp387, tmp388, tmp389, tmp390
  real tmp391, tmp392, tmp393, tmp394, tmp395
  real tmp396, tmp397, tmp398, tmp399, tmp400
  real tmp401, tmp402, tmp403, tmp404, tmp405
  real tmp406, tmp407, tmp408, tmp409, tmp410
  real tmp411, tmp412, tmp413, tmp414, tmp415
  real tmp416, tmp417, tmp418, tmp419, tmp420
  real tmp421, tmp422, tmp423, tmp424, tmp425
  real tmp426, tmp427, tmp428, tmp429, tmp430
  real tmp431, tmp432, tmp433, tmp434, tmp435
  real tmp436, tmp437, tmp438, tmp439, tmp440
  real tmp441, tmp442, tmp443, tmp444, tmp445
  real tmp446, tmp447, tmp448, tmp449, tmp450
  real tmp451, tmp452, tmp453, tmp454, tmp455
  real tmp456, tmp457, tmp458, tmp459, tmp460
  real tmp461, tmp462, tmp463, tmp464, tmp465
  real tmp466, tmp467, tmp468, tmp469, tmp470
  real tmp471, tmp472, tmp473, tmp474, tmp475
  real tmp476, tmp477, tmp478, tmp479, tmp480
  real tmp481, tmp482, tmp483, tmp484, tmp485
  real tmp486, tmp487, tmp488, tmp489, tmp490
  real tmp491, tmp492, tmp493, tmp494, tmp495
  real tmp496, tmp497, tmp498, tmp499, tmp500
  real tmp501, tmp502, tmp503, tmp504, tmp505
  real tmp506, tmp507, tmp508, tmp509, tmp510
  real tmp511, tmp512, tmp513, tmp514, tmp515
  real tmp516, tmp517, tmp518, tmp519, tmp520
  real tmp521, tmp522, tmp523, tmp524, tmp525
  real tmp526, tmp527, tmp528

  tmp1 = sqrt(lambda)**2
  tmp2 = -1 + kappa
  tmp3 = me2**2
  tmp4 = -2*me2*mm2
  tmp5 = mm2**2
  tmp6 = -4*mm2
  tmp7 = tmp1 + tmp6
  tmp8 = tmp7**(-2)
  tmp9 = 4*mm2
  tmp10 = -tt
  tmp11 = 2*me2
  tmp12 = 2 + tmp1
  tmp13 = -ss
  tmp14 = me2 + mm2 + tmp10 + tmp13
  tmp15 = 1 + tmp1
  tmp16 = tmp11 + tt
  tmp17 = 1/tt
  tmp18 = 5*me2
  tmp19 = 2/tmp17
  tmp20 = mm2 + tmp13 + tmp18 + tmp19
  tmp21 = tmp2**2
  tmp22 = sqrt(lambda)**8
  tmp23 = tmp10 + tmp9
  tmp24 = 1/tmp23
  tmp25 = -4*ss
  tmp26 = sqrt(lambda)**4
  tmp27 = -(kappa*tmp1)
  tmp28 = tmp27 + tmp9
  tmp29 = 2*tmp3
  tmp30 = 2*tmp5
  tmp31 = -3/tmp17
  tmp32 = tmp25 + tmp31 + tmp9
  tmp33 = me2*tmp32
  tmp34 = 1/tmp17 + tmp25
  tmp35 = mm2*tmp34
  tmp36 = 2*ss
  tmp37 = 3/tmp17
  tmp38 = tmp36 + tmp37
  tmp39 = ss*tmp38
  tmp40 = tmp29 + tmp30 + tmp33 + tmp35 + tmp39
  tmp41 = me2 + mm2 + tmp13
  tmp42 = mm2 + tmp13
  tmp43 = tmp17**(-2)
  tmp44 = 2*mm2
  tmp45 = -2*ss
  tmp46 = ss**2
  tmp47 = tmp11 + 1/tmp17 + tmp6
  tmp48 = me2*tmp45
  tmp49 = ss/tmp17
  tmp50 = -2*tmp1*tmp49
  tmp51 = -tmp1
  tmp52 = tmp51 + tmp9
  tmp53 = 1/tmp52
  tmp54 = (tmp1*tmp9)/tmp17
  tmp55 = -2*tmp43
  tmp56 = tmp1*tmp55
  tmp57 = kappa*tmp1*tmp43
  tmp58 = mm2**3
  tmp59 = tmp13*tmp43
  tmp60 = 4*ss
  tmp61 = tmp42**2
  tmp62 = mm2 + ss
  tmp63 = -2*me2*tmp62
  tmp64 = tmp3 + tmp61 + tmp63
  tmp65 = 1/tmp64
  tmp66 = ss + tmp10
  tmp67 = ss*tmp66
  tmp68 = -2*mm2
  tmp69 = 1/tmp17 + tmp45 + tmp68
  tmp70 = me2*tmp69
  tmp71 = 1/tmp17 + tmp36
  tmp72 = -(mm2*tmp71)
  tmp73 = tmp3 + tmp5 + tmp67 + tmp70 + tmp72
  tmp74 = -tmp5
  tmp75 = ss + tmp44
  tmp76 = -2*me2*tmp75
  tmp77 = mm2*tmp31
  tmp78 = tmp3 + tmp43 + tmp46 + tmp49 + tmp74 + tmp76 + tmp77
  tmp79 = -2 + kappa
  tmp80 = 1/tmp43
  tmp81 = 1/tmp17 + tmp51
  tmp82 = 3*mm2
  tmp83 = me2 + tmp10 + tmp13 + tmp82
  tmp84 = sqrt(lambda)**6
  tmp85 = tmp1 + tmp10
  tmp86 = kappa*tmp1*tmp85
  tmp87 = tmp79/tmp17
  tmp88 = (f1p*tmp2*tmp41*tmp51)/tmp17
  tmp89 = -(kappa*tmp26)
  tmp90 = tmp12*tmp19
  tmp91 = tmp1*tmp79
  tmp92 = 4/tmp17
  tmp93 = tmp18 + tmp42 + tmp92
  tmp94 = 4*tmp5
  tmp95 = 4 + tmp1
  tmp96 = kappa*tmp1
  tmp97 = tmp79*tmp9
  tmp98 = tmp96 + tmp97
  tmp99 = tmp43 + tmp46 + tmp49
  tmp100 = tmp1 + tmp87
  tmp101 = kappa*tmp26
  tmp102 = tmp2/tmp17
  tmp103 = tmp36*tmp43
  tmp104 = -2*tmp49
  tmp105 = 8*tmp5
  tmp106 = tmp44/tmp17
  tmp107 = 32*mm2*tmp3
  tmp108 = 32*tmp5
  tmp109 = mm2**4
  tmp110 = -3 + kappa
  tmp111 = 2*kappa
  tmp112 = -1 + tmp111
  tmp113 = -3*kappa
  tmp114 = 2 + tmp113
  tmp115 = tmp1*tmp114
  tmp116 = tmp112/tmp17
  tmp117 = 3*tmp2*tmp26
  tmp118 = tmp43*tmp79
  tmp119 = 8*ss
  tmp120 = 1 + tmp111
  tmp121 = sqrt(lambda)**3
  tmp122 = -(sqrt(lambda)/tmp17)
  tmp123 = tmp121 + tmp122
  tmp124 = tmp123**2
  tmp125 = -2*kappa
  tmp126 = 1 + tmp125
  tmp127 = 64*tmp109*tmp110
  tmp128 = mm2*tmp1*tmp126
  tmp129 = tmp128 + tmp94
  tmp130 = -32*tmp129*tmp3
  tmp131 = tmp110*tmp60
  tmp132 = tmp115 + tmp116 + tmp131
  tmp133 = -16*tmp132*tmp58
  tmp134 = kappa*tmp124*tmp13
  tmp135 = 18*kappa
  tmp136 = -31 + tmp135
  tmp137 = (tmp1*tmp136)/tmp17
  tmp138 = tmp115 + tmp116
  tmp139 = tmp138*tmp60
  tmp140 = tmp117 + tmp118 + tmp137 + tmp139
  tmp141 = tmp140*tmp94
  tmp142 = (14*tmp1)/tmp17
  tmp143 = tmp142 + tmp26 + tmp43
  tmp144 = tmp143*tmp96
  tmp145 = (tmp1*tmp120)/tmp17
  tmp146 = tmp117 + tmp118 + tmp145
  tmp147 = tmp146*tmp25
  tmp148 = tmp144 + tmp147
  tmp149 = mm2*tmp148
  tmp150 = 64*tmp110*tmp58
  tmp151 = 7*kappa
  tmp152 = -8 + tmp151
  tmp153 = tmp1*tmp152
  tmp154 = -(tmp120/tmp17)
  tmp155 = tmp119 + tmp153 + tmp154
  tmp156 = 16*tmp155*tmp5
  tmp157 = kappa*tmp124
  tmp158 = 3*kappa
  tmp159 = -1 + tmp158
  tmp160 = tmp159*tmp26
  tmp161 = tmp1*tmp119*tmp126
  tmp162 = 6*kappa
  tmp163 = -1 + tmp162
  tmp164 = (tmp1*tmp163)/tmp17
  tmp165 = tmp118 + tmp160 + tmp161 + tmp164
  tmp166 = tmp165*tmp9
  tmp167 = tmp150 + tmp156 + tmp157 + tmp166
  tmp168 = me2*tmp167
  tmp169 = tmp127 + tmp130 + tmp133 + tmp134 + tmp141 + tmp149 + tmp168
  tmp170 = tmp1*tmp119
  tmp171 = 9*tmp43
  tmp172 = tmp1/tmp17
  tmp173 = tmp1 + 1/tmp17
  tmp174 = 7*tmp1
  tmp175 = 5/tmp17
  tmp176 = tmp174 + tmp175
  tmp177 = 1/mm2
  tmp178 = me2**6
  tmp179 = me2**5
  tmp180 = me2**4
  tmp181 = me2**3
  tmp182 = tmp177**(-5)
  tmp183 = tmp177**(-6)
  tmp184 = tmp177**(-7)
  tmp185 = tmp177**(-8)
  tmp186 = ss**3
  tmp187 = ss**4
  tmp188 = ss**5
  tmp189 = ss**6
  tmp190 = tmp19*tmp46
  tmp191 = -2/tmp17
  tmp192 = 20*ss
  tmp193 = 6/tmp17
  tmp194 = 7/tmp17
  tmp195 = tmp65**2
  tmp196 = 28*tmp5
  tmp197 = tmp10 + tmp36
  tmp198 = -16*ss
  tmp199 = -9/tmp17
  tmp200 = -7/tmp17
  tmp201 = 14*ss
  tmp202 = -64*tmp58
  tmp203 = -8*ss
  tmp204 = 9/tmp17
  tmp205 = 4*tmp179
  tmp206 = tmp199 + tmp201
  tmp207 = tmp206/tmp177
  tmp208 = 6*ss*tmp197
  tmp209 = tmp196 + tmp207 + tmp208
  tmp210 = 2*tmp181*tmp209
  tmp211 = -24/tmp177
  tmp212 = tmp198 + tmp211 + tmp37
  tmp213 = tmp180*tmp212
  tmp214 = 8*tmp58
  tmp215 = tmp199 + tmp60
  tmp216 = tmp215*tmp5
  tmp217 = 3*ss
  tmp218 = tmp10 + tmp217
  tmp219 = (tmp218*tmp60)/tmp177
  tmp220 = tmp31*tmp46
  tmp221 = tmp214 + tmp216 + tmp219 + tmp220
  tmp222 = -(tmp221*tmp61)
  tmp223 = 18*tmp109
  tmp224 = tmp200 + tmp36
  tmp225 = (tmp224*tmp46)/tmp177
  tmp226 = ss + tmp31
  tmp227 = 2*tmp186*tmp226
  tmp228 = tmp25*tmp5*tmp71
  tmp229 = 15/tmp17
  tmp230 = tmp201 + tmp229
  tmp231 = -(tmp230*tmp58)
  tmp232 = tmp223 + tmp225 + tmp227 + tmp228 + tmp231
  tmp233 = tmp11*tmp232
  tmp234 = tmp203 + tmp204
  tmp235 = 2*tmp234*tmp46
  tmp236 = ss + tmp204
  tmp237 = tmp236*tmp94
  tmp238 = 17/tmp17
  tmp239 = tmp238 + tmp36
  tmp240 = (tmp239*tmp36)/tmp177
  tmp241 = tmp202 + tmp235 + tmp237 + tmp240
  tmp242 = tmp241*tmp3
  tmp243 = tmp205 + tmp210 + tmp213 + tmp222 + tmp233 + tmp242
  tmp244 = 10*ss
  tmp245 = 5*ss
  tmp246 = 18*ss
  tmp247 = tmp101*tmp85
  tmp248 = tmp43*tmp85
  tmp249 = 4*me2*tmp1
  tmp250 = tmp172 + tmp249 + tmp43
  tmp251 = (4*tmp250)/tmp177
  tmp252 = tmp248 + tmp251
  tmp253 = 1/tmp81
  tmp254 = f1p*tmp2*tmp252*tmp41*tmp51
  tmp255 = tmp1*tmp12*tmp2*tmp252*tmp41
  tmp256 = tmp1*tmp2
  tmp257 = 1/tmp17 + tmp256 + tmp6
  tmp258 = -2*tmp1*tmp257*tmp3
  tmp259 = tmp101 + tmp85
  tmp260 = tmp259/tmp17
  tmp261 = tmp15*tmp51
  tmp262 = 1/tmp17 + tmp261
  tmp263 = (4*tmp262)/tmp177
  tmp264 = tmp260 + tmp263
  tmp265 = -(tmp264*tmp42)
  tmp266 = tmp1*tmp105
  tmp267 = tmp2*tmp26*tmp36
  tmp268 = 1 + kappa
  tmp269 = tmp1*tmp268
  tmp270 = 2 + tmp269 + tmp45
  tmp271 = -(tmp172*tmp270)
  tmp272 = tmp12*tmp43
  tmp273 = -5 + kappa
  tmp274 = tmp1*tmp273
  tmp275 = -4 + tmp274 + tmp60
  tmp276 = tmp1*tmp275
  tmp277 = 3*tmp1
  tmp278 = 4 + tmp277
  tmp279 = tmp278/tmp17
  tmp280 = tmp276 + tmp279
  tmp281 = (-2*tmp280)/tmp177
  tmp282 = tmp266 + tmp267 + tmp271 + tmp272 + tmp281
  tmp283 = me2*tmp282
  tmp284 = tmp258 + tmp265 + tmp283
  tmp285 = f2p*tmp191*tmp284
  tmp286 = tmp254 + tmp255 + tmp285
  tmp287 = -6*tmp1
  tmp288 = 1/tmp17 + tmp287
  tmp289 = -2*tmp1
  tmp290 = 1/tmp17 + tmp289
  tmp291 = 2*tmp172
  tmp292 = ss*tmp289
  tmp293 = tmp291 + tmp292 + tmp49
  tmp294 = tmp1*tmp203
  tmp295 = tmp1*tmp193
  tmp296 = tmp217/tmp17
  tmp297 = 8*tmp172
  tmp298 = ss*tmp193
  tmp299 = -4*tmp1
  tmp300 = 4*tmp172
  tmp301 = 2*tmp49
  tmp302 = ss*tmp299
  tmp303 = ss*tmp101
  tmp304 = tmp300 + tmp301 + tmp302 + tmp303
  tmp305 = tmp101 + tmp19
  tmp306 = 3*tmp303
  tmp307 = tmp19 + tmp51
  tmp308 = -4*tmp49
  tmp309 = tmp1*tmp36
  tmp310 = -8*tmp172
  tmp311 = -16*tmp109
  tmp312 = 6*ss
  tmp313 = -4/tmp17
  tmp314 = 2*tmp58
  tmp315 = -(tmp46/tmp17)
  tmp316 = tmp37*tmp46
  tmp317 = 32*ss
  tmp318 = ss + 1/tmp17
  tmp319 = 2*tmp46
  tmp320 = ss*tmp92
  tmp321 = 2*tmp180
  tmp322 = tmp36 + tmp82
  tmp323 = -4*tmp181*tmp322
  tmp324 = 16*tmp5
  tmp325 = 12*tmp46
  tmp326 = tmp191 + tmp192
  tmp327 = tmp326/tmp177
  tmp328 = tmp301 + tmp324 + tmp325 + tmp327 + tmp43
  tmp329 = tmp3*tmp328
  tmp330 = tmp37 + tmp60
  tmp331 = -2*tmp330*tmp5
  tmp332 = tmp43 + tmp46
  tmp333 = (2*tmp332)/tmp177
  tmp334 = 4*tmp46
  tmp335 = tmp301 + tmp334 + tmp43
  tmp336 = ss*tmp335
  tmp337 = tmp314 + tmp331 + tmp333 + tmp336
  tmp338 = -2*me2*tmp337
  tmp339 = -5/tmp17
  tmp340 = ss + tmp339
  tmp341 = -2*tmp340*tmp5
  tmp342 = tmp301 + tmp319 + tmp43
  tmp343 = ss*tmp342
  tmp344 = 3*tmp43
  tmp345 = tmp319 + tmp320 + tmp344
  tmp346 = -(tmp345/tmp177)
  tmp347 = tmp314 + tmp341 + tmp343 + tmp346
  tmp348 = -(tmp347*tmp42)
  tmp349 = tmp321 + tmp323 + tmp329 + tmp338 + tmp348
  tmp350 = 8*tmp1*tmp3
  tmp351 = tmp277/tmp17
  tmp352 = tmp203/tmp17
  tmp353 = tmp253**(-2)
  tmp354 = -(tmp101*tmp353)
  tmp355 = 2*tmp15*tmp43
  tmp356 = tmp289*tmp46
  tmp357 = tmp17**(-3)
  tmp358 = 2*tmp357
  tmp359 = tmp1*tmp60
  tmp360 = -5*tmp1
  tmp361 = 12 + kappa
  tmp362 = kappa*tmp84
  tmp363 = 5*tmp1
  tmp364 = tmp1 + tmp19
  tmp365 = 96*tmp5
  tmp366 = 16*ss
  tmp367 = tmp287/tmp17
  tmp368 = -3*tmp1
  tmp369 = tmp368 + tmp92
  tmp370 = 4*tmp26
  tmp371 = ss*tmp368
  tmp372 = -8/tmp17
  tmp373 = ss*tmp174
  tmp374 = 16*tmp46
  tmp375 = tmp119/tmp17
  tmp376 = 2*tmp26
  tmp377 = 2*tmp1
  tmp378 = tmp42**4
  tmp379 = -tmp26
  tmp380 = 8/tmp17
  tmp381 = -2*tmp26
  tmp382 = tmp1 + tmp92
  tmp383 = tmp1 + tmp380
  tmp384 = ss*tmp383
  tmp385 = 32/tmp177
  tmp386 = tmp313 + tmp317 + tmp363 + tmp385
  tmp387 = 96*tmp46
  tmp388 = kappa/tmp17
  tmp389 = -7*tmp26
  tmp390 = 14*tmp26
  tmp391 = tmp111*tmp43
  tmp392 = (-12*ss)/tmp253
  tmp393 = -4*tmp26
  tmp394 = -6/tmp17
  tmp395 = 1/me2
  tmp396 = tmp42**3
  tmp397 = 1/tmp17 + tmp60
  tmp398 = 24*tmp58
  tmp399 = 24*tmp46
  tmp400 = tmp360/tmp17
  tmp401 = 4*tmp109
  tmp402 = tmp1 + tmp60
  tmp403 = -3*tmp26
  tmp404 = 7*tmp26
  tmp405 = tmp1*tmp244
  tmp406 = ss*tmp277
  tmp407 = -10*ss*tmp1
  tmp408 = 20/tmp17
  tmp409 = -20*tmp172
  tmp410 = tmp198/tmp17
  tmp411 = tmp102*tmp381
  tmp412 = 48*tmp49
  tmp413 = ss*tmp1
  tmp414 = -96*tmp49
  tmp415 = -tmp413
  tmp416 = tmp366/tmp17
  tmp417 = tmp1*tmp334
  tmp418 = tmp1*tmp408
  tmp419 = 1/tmp17 + tmp377
  tmp420 = -64/tmp17
  tmp421 = tmp362*tmp364
  tmp422 = tmp317/tmp17
  tmp423 = 5*kappa
  tmp424 = tmp101*tmp43
  tmp425 = tmp364*tmp94
  tmp426 = -((tmp364*tmp402)/tmp177)
  tmp427 = -9*tmp1
  tmp428 = tmp363/tmp17
  tmp429 = -(tmp362*tmp43)
  tmp430 = 4*kappa*tmp43
  tmp431 = 32*tmp46
  tmp432 = -24*tmp1*tmp46
  tmp433 = 64*tmp186
  tmp434 = 8*kappa*tmp43
  tmp435 = -8*tmp1
  tmp436 = 4*tmp1
  tmp437 = -40*ss
  tmp438 = 8*tmp46
  tmp439 = 22*tmp49
  tmp440 = 24*tmp43
  tmp441 = -16*tmp1
  tmp442 = 24/tmp17
  tmp443 = 24*ss
  tmp444 = tmp441/tmp17
  tmp445 = -22/tmp17
  tmp446 = tmp1 + tmp445
  tmp447 = tmp172*tmp446
  tmp448 = ss*tmp441
  tmp449 = 48*tmp46
  tmp450 = 12*tmp43
  tmp451 = 16*tmp186
  tmp452 = 11*tmp1
  tmp453 = 48*tmp186
  tmp454 = tmp1*tmp204
  tmp455 = (tmp1*tmp364*tmp378)/(tmp177*tmp24)
  tmp456 = tmp1 + tmp313
  tmp457 = 16/tmp17
  tmp458 = 12/tmp17
  tmp459 = 9*tmp1
  tmp460 = 32/tmp17
  tmp461 = 60/tmp17
  tmp462 = 36*tmp413
  tmp463 = 80*tmp46
  tmp464 = -24*tmp172
  tmp465 = 10*tmp172
  tmp466 = -8*tmp26
  tmp467 = 16*tmp43
  tmp468 = tmp395**(-7)
  tmp469 = tmp26 + tmp291
  tmp470 = ss*tmp469
  tmp471 = tmp424 + tmp425 + tmp426 + tmp470
  tmp472 = 1/tmp17 + tmp312
  tmp473 = tmp26 + tmp84
  tmp474 = 36*tmp172
  tmp475 = 16 + tmp158
  tmp476 = 22 + kappa
  tmp477 = 6 + kappa
  tmp478 = -3 + tmp1
  tmp479 = 3 + tmp1
  tmp480 = kappa*tmp277
  tmp481 = 8 + tmp480
  tmp482 = tmp363*tmp475
  tmp483 = 4 + tmp96
  tmp484 = -6 + kappa
  tmp485 = 60*tmp1
  tmp486 = -10 + kappa
  tmp487 = tmp467*tmp479
  tmp488 = -3 + tmp423
  tmp489 = tmp1*tmp477
  tmp490 = 20 + tmp489
  tmp491 = 4*tmp43*tmp490
  tmp492 = tmp19 + tmp217
  tmp493 = -4 + kappa
  tmp494 = tmp481/tmp17
  tmp495 = -44*tmp1
  tmp496 = 2*tmp268*tmp84
  tmp497 = tmp363*tmp476
  tmp498 = kappa*tmp376
  tmp499 = -10/tmp17
  tmp500 = 11*kappa
  tmp501 = -4*kappa*tmp43
  tmp502 = 14 + tmp423
  tmp503 = 48*tmp43*tmp479
  tmp504 = -(kappa*tmp22)
  tmp505 = tmp175 + tmp312
  tmp506 = tmp158*tmp26
  tmp507 = 6 + tmp1
  tmp508 = 44/tmp17
  tmp509 = tmp1*tmp458
  tmp510 = 2 + kappa
  tmp511 = -64*tmp1
  tmp512 = 17 + tmp459
  tmp513 = tmp512*tmp92
  tmp514 = 32*tmp43*tmp479
  tmp515 = 32*tmp1
  tmp516 = tmp497/tmp17
  tmp517 = tmp26*tmp510
  tmp518 = tmp458*tmp479
  tmp519 = 8*tmp26
  tmp520 = (tmp26*tmp502)/tmp17
  tmp521 = -8 + tmp500
  tmp522 = 9*kappa
  tmp523 = 6 + tmp522
  tmp524 = 14 + tmp158
  tmp525 = tmp26*tmp524
  tmp526 = 8 + kappa
  tmp527 = 1 + tmp158
  tmp528 = 19*kappa
  MP2MPL_MP_FIN_d11d21 = 16*logmut*Pi*(f2p*tmp15 - f1p*tmp256 + tmp12*tmp256) - 8*C0tF2F20F0*Pi*(f2p*tmp1&
                          &5*tmp191 - f1p*tmp16*tmp256 + tmp12*tmp16*tmp256) + logmuP2*Pi*tmp17*(2*tmp2*tmp&
                          &20 - f1p*tmp2*tmp20 + f2p*(tmp13 + 1/tmp177 + tmp313 + 1/tmp395))*tmp436 + 16*lo&
                          &gP2t*Pi*tmp17*tmp24*((f2p*tmp15)/tmp17 - f1p*tmp14*tmp256 + tmp12*tmp14*tmp256)*&
                          &(tmp10 + tmp11 + tmp44 + tmp45) + (32*DB0sFP*f2p*Pi*(tmp13/tmp177 + tmp3 + tmp13&
                          &/tmp395 + tmp4 + tmp5))/(tmp3 + tmp4 + tmp45/tmp177 + tmp46 + tmp48 + tmp5) + DB&
                          &0sFP*logF2P2*Pi*tmp119*tmp17*tmp24*tmp253*tmp286*tmp65 + DB0sFP*logmuF2*Pi*tmp17&
                          &*tmp24*tmp253*tmp286*tmp366*tmp65 + logF2N2*Pi*tmp17*tmp195*tmp2*tmp370*tmp53*(-&
                          &2*tmp243 + f1p*tmp243 + f2p*(2*tmp181*((tmp204 + tmp244)/tmp177 + tmp298) + tmp1&
                          &80*tmp31 - 2*tmp3*((ss*(tmp238 + tmp246))/tmp177 + (tmp204 + tmp245)*tmp30 + tmp&
                          &204*tmp46) - (2*(5/tmp177 + tmp217)*((ss*(1/tmp17 + tmp45))/tmp177 + tmp191*tmp4&
                          &6 + (tmp31 + tmp36)*tmp5))/tmp395 + tmp61*(tmp220 + (tmp192 + tmp199)*tmp5 + (4*&
                          &tmp67)/tmp177))) + 16*DB0P2LP*Pi*tmp1*tmp17*tmp24*tmp53*tmp65*((4*tmp2*tmp349)/t&
                          &mp177 - (2*f1p*tmp2*tmp349)/tmp177 + (f2p*tmp28*(tmp181 + (-3*ss + tmp10 + 1/tmp&
                          &177)*tmp3 + ((-6*ss + tmp37)/tmp177 + ss*tmp492 - 9*tmp5)/tmp395 + tmp42*(ss*tmp&
                          &318 + 7*tmp5 - (2*tmp71)/tmp177)))/tmp17) - (16*DB0F2FN*Pi*tmp17*tmp2*tmp26*tmp5&
                          &3*tmp65*((f2p*(ss + 1/tmp177 - 1/tmp395))/tmp17 - 2*tmp73 + f1p*tmp73))/tmp395 +&
                          & (16*DB0F2FL*Pi*tmp1*tmp17*tmp53*tmp65*((f2p*tmp28*(tmp13 - 1/tmp177 + 1/tmp395)&
                          &)/tmp17 - (8*tmp2*tmp73)/tmp177 + (4*f1p*tmp2*tmp73)/tmp177))/tmp395 + C0sF2P2FN&
                          &P*Pi*tmp17*tmp2*tmp519*tmp53*tmp65*(tmp101*tmp181 - (32*tmp181)/tmp177 + tmp181*&
                          &tmp19 + tmp186*tmp191 + (80*ss*tmp3)/tmp177 - (34*tmp3)/(tmp17*tmp177) + tmp311 &
                          &+ kappa*tmp186*tmp379 + (kappa*tmp3*tmp379)/tmp177 + ss*tmp3*tmp394 + (kappa*ss*&
                          &tmp381)/(tmp177*tmp395) + kappa*ss*tmp3*tmp403 + tmp451/tmp177 - (64*tmp46)/(tmp&
                          &177*tmp395) + (tmp193*tmp46)/tmp395 + (tmp46*tmp499)/tmp177 + 48*tmp3*tmp5 + (64&
                          &*ss*tmp5)/tmp395 + (62*tmp5)/(tmp17*tmp395) + (kappa*tmp379*tmp5)/tmp395 + kappa&
                          &*ss*tmp403*tmp5 - 48*tmp46*tmp5 + 42*tmp49*tmp5 + (tmp46*tmp506)/tmp177 + (tmp46&
                          &*tmp506)/tmp395 + (ss*tmp508)/(tmp177*tmp395) + 48*ss*tmp58 + tmp101*tmp58 - (30&
                          &*tmp58)/tmp17 + f1p*((tmp10 + 16/tmp177)*tmp181 + tmp3*(tmp296 + (tmp238 + tmp43&
                          &7)/tmp177 - 24*tmp5) - (tmp316 + (tmp439 - 32*tmp46)/tmp177 + (31/tmp17 + tmp317&
                          &)*tmp5)/tmp395 + tmp42*(tmp214 + tmp315 + (tmp198 + tmp229)*tmp5 + (tmp36*(tmp31&
                          & + tmp60))/tmp177)) + 2*f2p*(2*(tmp10 + 1/tmp177)*tmp181 - tmp3*(tmp30 + ss*tmp3&
                          &39 + tmp505/tmp177) - (2*(tmp190 + tmp30*(ss + tmp313) + (tmp13*(tmp217 + tmp380&
                          &))/tmp177 + tmp58))/tmp395 + tmp42*(tmp314 + tmp315 + (tmp36*(ss + tmp37))/tmp17&
                          &7 + (tmp204 + tmp60)*tmp74))) - 16*DB0tNN*Pi*tmp17*tmp21*tmp22*tmp24*tmp40*tmp8 &
                          &+ (64*DB0tLL*Pi*tmp17*tmp2*tmp24*tmp26*tmp28*tmp40*tmp8)/tmp177 + 8*C0tF2F2NFN*P&
                          &i*tmp21*tmp22*tmp47*tmp8 + C0tP2P2LPL*Pi*tmp17*tmp2*tmp24*tmp26*tmp28*tmp385*(tm&
                          &p103 + tmp190 + tmp29*tmp290 + tmp358 + (tmp170 - 8/(tmp177*tmp253) + tmp291 + t&
                          &mp308)/tmp395 + (tmp294 + tmp295 - 6*tmp43)/tmp177 + tmp368*tmp43 + tmp299*tmp46&
                          & - 2*tmp288*tmp5 + tmp50)*tmp8 - (32*D0tsLL*Pi*tmp17*tmp2*tmp26*tmp28*(tmp350 + &
                          &tmp370/tmp17 + ss*tmp393 + tmp413/tmp17 + (-tmp172 + tmp370 + tmp43)/tmp177 + (t&
                          &mp172 + tmp43 + tmp436*(tmp1 + tmp44 + tmp45))/tmp395 + tmp59)*tmp8)/tmp177 + 8*&
                          &D0tsNN*Pi*tmp17*tmp21*tmp22*(tmp107 + tmp397/(tmp17*tmp177) + ((4*(1/tmp17 + tmp&
                          &203))/tmp177 + tmp365 + tmp43)/tmp395 + (-64*ss + tmp461)*tmp5 + 64*tmp58 + tmp5&
                          &9)*tmp8 - 4*D0tsLN*Pi*tmp169*tmp17*tmp2*tmp8*tmp84 - 4*D0tsNL*Pi*tmp169*tmp17*tm&
                          &p2*tmp8*tmp84 + D0IR6tsL*Pi*tmp17*tmp435*tmp53*((4*f1p*tmp2*(tmp350 + tmp42*(tmp&
                          &172 + tmp376 + tmp43) + (tmp351 + 8*tmp1*tmp42 + tmp43)/tmp395))/tmp177 + f2p*tm&
                          &p28*(tmp3*tmp380 + (tmp26 + tmp351 + tmp352 + tmp380/tmp177)/tmp395 + tmp42*(tmp&
                          &172 + tmp26 + 2*tmp43)) - tmp2*((tmp354 + (4*(-((-2 + tmp1)*tmp172) + tmp26*tmp2&
                          &78 + tmp355))/tmp177)*tmp42 + (tmp12*tmp3*tmp515)/tmp177 + (tmp354 + tmp12*tmp5*&
                          &tmp515 + (4*(tmp355 + ss*tmp12*tmp435 + tmp172*tmp507 + tmp84))/tmp177)/tmp395))&
                          & + C0tF2F2LFL*Pi*tmp2*tmp26*tmp28*tmp385*tmp8*(-2/tmp395 + tmp85) + 8*C0tF2F2NFL&
                          &*Pi*tmp17*tmp2*tmp8*tmp84*(((-4*tmp100)/tmp177 + tmp2*tmp324 + tmp86)/tmp17 + (2&
                          &*(kappa*tmp324 + tmp86 - (4*(kappa*tmp377 + tmp87))/tmp177))/tmp395) + DB0sFPlog&
                          &mut*Pi*tmp17*tmp198*tmp65*((tmp12*tmp256*tmp41)/tmp17 - 2*f2p*tmp15*(4*tmp3 + tm&
                          &p42/tmp17 + (4*tmp42)/tmp395) + tmp88) + (16*logbeL2*Pi*tmp1*tmp53*tmp80*((-4*f1&
                          &p*tmp2*tmp40)/tmp177 + f2p*tmp19*tmp28*tmp83 + tmp2*tmp40*((8*tmp15)/tmp177 + tm&
                          &p89)))/(tmp253*(1/tmp17 + tmp6)) + C0tF2F2NF0*Pi*tmp17*tmp2*tmp519*tmp53*((tmp29&
                          &9*tmp388)/tmp177 - 16/(tmp177*tmp395) + tmp313/tmp395 + (tmp1*tmp313)/tmp395 + (&
                          &tmp377*tmp388)/tmp395 + (kappa*tmp435)/(tmp177*tmp395) + f2p*tmp19*(tmp10 + tmp4&
                          &4) + tmp54 + tmp55 + tmp56 + tmp57 + f1p*(tmp43 + (2*(1/tmp17 + tmp9))/tmp395)) &
                          &- 4*logmuF2*Pi*tmp17*(f1p*tmp20*tmp256 + tmp2*tmp20*tmp289 + f2p*(tmp1*tmp41 + t&
                          &mp90)) + 16*logbeN2*Pi*tmp2*tmp26*tmp53*tmp80*(f1p*tmp40 + f2p*tmp19*tmp83 + tmp&
                          &40*(-2 + tmp91)) + 8*C0sF2P2FLP*Pi*tmp1*tmp17*tmp53*tmp65*((4*f1p*tmp2*(tmp181*(&
                          &1/tmp17 + tmp299) - tmp3*(tmp288/tmp177 + tmp296 + tmp300 + tmp407) + tmp42*(ss*&
                          &tmp293 - (2*tmp293)/tmp177 + tmp290*tmp5) + (ss*(tmp294 + tmp295 + tmp296) + (tm&
                          &p104 + tmp170 + tmp297)/tmp177 + tmp74/tmp17)/tmp395))/tmp177 - (4*tmp2*(-(tmp3*&
                          &(tmp297 + tmp298 + (-12*tmp1 + tmp305)/tmp177 + tmp306 - 20*tmp413)) + tmp181*(t&
                          &mp305 + tmp435) + tmp42*(ss*tmp304 - (2*tmp304)/tmp177 + (tmp299 + tmp305)*tmp5)&
                          & + ((-2*(tmp294 + tmp301 + tmp303 + tmp310))/tmp177 + ss*(tmp298 + tmp306 + tmp4&
                          &48 + tmp509) + tmp305*tmp74)/tmp395))/tmp177 - f2p*tmp28*(tmp3*(tmp300 + (tmp1 +&
                          & tmp394)/tmp177 + tmp406 + ss*tmp499) + ((tmp309 + tmp310 + tmp352)/tmp177 + ss*&
                          &(tmp367 + tmp371 + tmp375) + tmp1*tmp5)/tmp395 + tmp42*((tmp300 + tmp308 + tmp30&
                          &9)/tmp177 + ss*(tmp289/tmp17 + tmp301 + tmp415) + tmp307*tmp5) + tmp181*(tmp51 +&
                          & tmp92))) + 8*C0IR6sFP*Pi*tmp17*(tmp256*tmp305*tmp41 + tmp88 + f2p*(tmp42*(tmp89&
                          & + tmp90) + (tmp89 + tmp12*tmp92)/tmp395)) + 16*C0tP2P20P0*Pi*tmp24*(-(f1p*tmp25&
                          &6*tmp78) + tmp12*tmp256*tmp78 + f2p*tmp15*((tmp19 + tmp25)/tmp177 + (2*(tmp10 + &
                          &6/tmp177))/tmp395 - tmp43 + tmp94)) + logF2L2*Pi*tmp1*tmp17*tmp195*tmp395*tmp8*(&
                          &(-4*f1p*tmp2*(16*tmp178 - 2*tmp179*tmp386 + tmp181*(tmp202 + tmp324*(tmp330 + tm&
                          &p368) + tmp25*(tmp374 + tmp376 + tmp392 + tmp400) + (tmp317/tmp253 + tmp381 + tm&
                          &p418 + 64*tmp46)/tmp177) + tmp180*(tmp26 + tmp365 + tmp367 + tmp387 + (4*(tmp366&
                          & + tmp372 + tmp459))/tmp177 + tmp462 - 32*tmp49) + tmp364*tmp378*tmp51 + 4*tmp3*&
                          &((tmp13*(tmp300 + tmp352 + tmp373 + tmp374))/tmp177 + tmp401 + (tmp334 + tmp352 &
                          &+ tmp367 + tmp373)*tmp46 + (tmp367 + tmp375 + tmp399 - 7*tmp413)*tmp5 + (tmp174 &
                          &+ tmp198 + tmp372)*tmp58) + (2*(ss*(tmp295 + tmp320 + tmp370 + tmp371) + (tmp26 &
                          &+ tmp295 + tmp352 + 6*tmp413)/tmp177 + tmp369*tmp5)*tmp61)/tmp395))/(tmp177*tmp5&
                          &3) + (f2p*tmp28*(tmp179*(tmp372 + tmp377) + tmp1*tmp364*tmp378 - 2*tmp181*(tmp36&
                          &*(tmp381 + tmp428 + ss*tmp458) + (tmp379 + tmp416 + tmp465)/tmp177 + tmp442*tmp5&
                          &) + tmp180*(tmp295 + tmp302 + tmp379 + tmp422 + (4*(tmp380 + tmp51))/tmp177) + 4&
                          &*tmp3*((tmp13*(tmp1*tmp313 + tmp384))/tmp177 + (tmp295 + tmp384)*tmp46 + (tmp295&
                          & - tmp384)*tmp5 + tmp383*tmp58) - (2*(ss*(tmp295 + tmp370 + ss*tmp382) + (tmp26 &
                          &+ tmp295 + tmp382*tmp45)/tmp177 + tmp382*tmp5)*tmp61)/tmp395))/tmp53 + 8*tmp2*(t&
                          &mp181*(-256*tmp182 + 64*tmp109*(tmp289 + tmp330) + ss*tmp344*tmp362 + tmp214*(5*&
                          &tmp26 + tmp300 + tmp119*(tmp19 + tmp368) + tmp431) + (ss*(tmp374 + tmp1*(tmp339 &
                          &+ kappa*tmp344 + tmp377) + tmp392)*tmp436)/tmp177 + (-256*tmp186 + (48*tmp413)/t&
                          &mp17 + 64*(tmp299 + tmp37)*tmp46 + tmp376*(tmp1 + tmp391 + tmp499))*tmp5) + (tmp&
                          &396*tmp471*tmp51)/tmp177 - (2*tmp179*tmp386)/(tmp177*tmp53) + tmp180*(384*tmp109&
                          & + tmp429 + tmp105*(tmp172 + tmp393 + tmp405 + tmp410 + tmp449) + ((tmp387 + tmp&
                          &25*(tmp380 + tmp427) + tmp1*(tmp1 + tmp394 + tmp430))*tmp51)/tmp177 + 16*(tmp277&
                          & + tmp366 + tmp372)*tmp58) + tmp3*(64*tmp183 - 32*tmp182*(tmp119 + tmp369) - 3*t&
                          &mp362*tmp43*tmp46 + (tmp415*(7*tmp424 + tmp451 - 4*(-7*tmp1 + tmp380)*tmp46 + ss&
                          &*(-2 + tmp388)*tmp509))/tmp177 + tmp30*(32*tmp187 + tmp186*(88*tmp1 + tmp420) + &
                          &tmp362*tmp43 + tmp370*(2 + tmp388)*tmp49 + tmp46*(tmp390 + tmp511/tmp17)) - 4*((&
                          &tmp379*(6 + tmp388))/tmp17 + tmp433 + ss*(tmp389 + tmp1*tmp442) + (52*tmp1 - 32/&
                          &tmp17)*tmp46)*tmp58 + tmp401*(tmp387 + tmp389 + tmp444 + (tmp368 + tmp380)*tmp60&
                          &)) + (tmp42*(8*tmp182*tmp369 + tmp314*(tmp217*(tmp26 + tmp300) + tmp325*tmp369 +&
                          & tmp379*(tmp1 + tmp193 + tmp391)) + tmp109*(40*tmp172 + tmp390 + 72*tmp413 + tmp&
                          &414) + tmp429*tmp46 + (tmp413*(-7*tmp424 + ss*(tmp37 + tmp377 - kappa*tmp43)*tmp&
                          &436 + (tmp287 + tmp380)*tmp46))/tmp177 + tmp45*tmp5*(tmp334*tmp369 + ss*(tmp404 &
                          &+ tmp474) + tmp26*tmp501 + 3*tmp84)))/tmp395 + 16*tmp178*(tmp51/tmp177 + tmp94))&
                          &) + (logF2P2*Pi*tmp17*tmp195*tmp395*tmp8*((f2p*(ss*tmp26*tmp28*tmp364*tmp378 - 2&
                          &*tmp179*(tmp105*(tmp406 + tmp408) + (tmp101 + tmp19*(-12 + tmp256) + tmp407)*tmp&
                          &413 + (2*(-5*ss*tmp26 + tmp409 + tmp412 + 20*tmp1*tmp46))/tmp177) + 4*tmp3*tmp42&
                          &*(tmp401*(tmp371 + tmp408) + tmp1*tmp186*(tmp101 + (3 + tmp256)*tmp313 + 5*tmp41&
                          &3) + (tmp46*((1 - 12*kappa)*ss*tmp26 + tmp300 + tmp411 + tmp412 - 20*tmp1*tmp46)&
                          &)/tmp177 + ss*(28*tmp172 + (19 - 20*kappa)*ss*tmp26 - tmp362 + tmp410 + tmp411 +&
                          & 28*tmp1*tmp46)*tmp5 + (7*ss*(-16/tmp17 + tmp26) + tmp409 + tmp417)*tmp58) - 2*t&
                          &mp181*(16*tmp109*(tmp408 + tmp415) + tmp377*tmp46*((4 + tmp2*tmp277)*tmp301 + tm&
                          &p101*tmp419 - 10*tmp1*tmp46) - 4*(ss*tmp379 + tmp412 + tmp417 + tmp418)*tmp58 + &
                          &(ss*(80*tmp1*tmp186 + tmp421 + (12*(-5 + tmp162)*tmp26 + tmp420)*tmp46 + tmp60*(&
                          &(4 + tmp1*tmp110)*tmp172 - 4*tmp84)))/tmp177 + tmp25*tmp5*((9 - 10*kappa)*ss*tmp&
                          &26 + tmp1*tmp325 + ((-6 + tmp1)*tmp377)/tmp17 + tmp416 + tmp84)) + (2*tmp61*(8*t&
                          &mp109*(tmp313 + tmp413) + tmp1*tmp46*(ss*tmp191*(4 + tmp256) + tmp303 + tmp377*t&
                          &mp46 + tmp419*tmp498) + (-6*ss*tmp26 + tmp297 + tmp432 + 96*tmp49)*tmp58 + ss*tm&
                          &p5*((-13 + 20*kappa)*ss*tmp376 + (tmp273*tmp376)/tmp17 + tmp1*tmp399 + tmp414 + &
                          &tmp464 + tmp493*tmp84) + (ss*(tmp421 + tmp186*tmp435 + tmp319*((-9 + 4*kappa)*tm&
                          &p26 + tmp457) + tmp45*((tmp436*tmp478)/tmp17 + tmp526*tmp84)))/tmp177))/tmp395 +&
                          & (4*tmp178*(tmp413 + tmp92))/tmp53 + tmp180*(tmp105*(ss*tmp403 + tmp409 + tmp422&
                          & + tmp1*tmp438) + 32*(tmp408 + tmp413)*tmp58 + (tmp60*(40*tmp1*tmp46 + (tmp380 +&
                          & tmp26*(-7 + tmp423))*tmp60 + tmp2*tmp84 + (tmp377*(-8 + tmp91))/tmp17))/tmp177 &
                          &+ tmp413*(tmp101*tmp364 - 40*tmp1*tmp46 + tmp60*(tmp101 + (-2 + tmp256)*tmp92)))&
                          &))/tmp53 + ss*tmp2*tmp41*tmp435*(-((tmp180*(tmp324 + (2*(tmp119 + tmp368))/tmp17&
                          &7 + tmp397*tmp51))/tmp53) + (16*tmp183 - 16*tmp182*tmp402 + tmp109*(-28*tmp172 -&
                          & 9*tmp26 + tmp387 - 24*tmp413) + (56*tmp1*tmp186 + 16*tmp187 + tmp1*tmp325*tmp41&
                          &9 + tmp429 + ss*tmp26*(tmp339 - 8*kappa*tmp43 + tmp436))*tmp5 + (tmp415*(8*tmp18&
                          &6 + tmp319*(tmp19 + tmp363) + kappa*tmp43*tmp466 + tmp413*(tmp174 + tmp37 + tmp5&
                          &01)))/tmp177 + tmp26*tmp46*(tmp46 + tmp49 + tmp57) + (-64*tmp186 + ss*(-6*tmp26 &
                          &+ tmp418) + tmp26*(tmp194 + tmp277 + tmp430) + tmp435*tmp46)*tmp58)/tmp395 + (tm&
                          &p1*(ss*tmp26 + tmp19*tmp413 + tmp424 + tmp425 + tmp426)*tmp61)/tmp177 + tmp179/t&
                          &mp8 - tmp3*(-8*tmp109*(tmp119 + tmp174) + 64*tmp182 + ss*tmp26*(tmp296 + tmp334 &
                          &+ tmp1*tmp391) + (tmp432 + tmp433 + tmp26*(tmp204 + tmp277 + tmp434) + ss*tmp436&
                          &*tmp456)*tmp5 - 2*(18*tmp172 + tmp26 + 56*tmp413 + tmp431)*tmp58 + (tmp1*(-32*tm&
                          &p186 + tmp424 - 2*(tmp193 + tmp360)*tmp46 + tmp413*(tmp174 + tmp434 + tmp92)))/t&
                          &mp177) + tmp181*(96*tmp109 + tmp214*(tmp119 + tmp427) + (tmp1*(tmp26 + 26*tmp413&
                          & + tmp428 + kappa*tmp43*tmp436 - 48*tmp46 - 12*tmp49))/tmp177 + tmp26*(tmp296 + &
                          &6*tmp46 + tmp57) + (tmp376 + tmp399 + tmp400 - 30*tmp413)*tmp94)) + (f1p*ss*tmp2&
                          &*tmp41*tmp436*((tmp1*tmp364*tmp396)/tmp177 + tmp180*((6*tmp1 + tmp198)/tmp177 + &
                          &tmp1*tmp397 - 16*tmp5) + tmp3*(tmp311 + tmp314*(tmp119 + tmp363) + (ss*(tmp300 +&
                          & tmp404 + tmp405 - 16*tmp46))/tmp177 + tmp1*tmp330*tmp46 + tmp5*(3*tmp26 + tmp37&
                          &4 + tmp454 + ss*tmp515)) + tmp179/tmp53 + (tmp42*(tmp401 + (tmp13*(tmp291 + tmp3&
                          &34 + tmp404 + ss*tmp459))/tmp177 + tmp1*tmp318*tmp46 + (tmp1*tmp200 + tmp325 + t&
                          &mp403 - 13*tmp413)*tmp5 - 3*tmp402*tmp58))/tmp395 + tmp181*(tmp398 + (tmp379 + t&
                          &mp399 + tmp400 - 26*tmp413)/tmp177 - 3*tmp413*tmp71 + (tmp368 + tmp60)*tmp94)))/&
                          &tmp53))/tmp13 + logmuN2*Pi*tmp17*tmp2*tmp466*tmp8*((f2p*tmp7)/tmp17 + (f1p*tmp93&
                          &)/tmp53 + 2*(-4*tmp5 + (5*tmp7)/tmp395 + (tmp402 + (-4 + tmp91)*tmp92)/tmp177 + &
                          &tmp1*(tmp13 + tmp95/tmp17))) + 8*logmuL2*Pi*tmp1*tmp17*tmp8*((f2p*tmp28)/(tmp17*&
                          &tmp53) + (4*f1p*tmp2*tmp93)/(tmp177*tmp53) - (8*tmp2*(tmp1*(ss - tmp483/tmp17) +&
                          & 5/(tmp395*tmp53) + tmp94 - (tmp402 + tmp313*tmp95)/tmp177))/tmp177) + D0IR6tsN*&
                          &Pi*tmp17*tmp2*tmp519*tmp53*(tmp103 + tmp202 - (64*tmp3)/tmp177 - (32*tmp1*tmp3)/&
                          &tmp177 + (ss*tmp380)/tmp177 + (64*ss)/(tmp177*tmp395) + tmp211/(tmp17*tmp395) + &
                          &(tmp1*tmp313)/(tmp177*tmp395) + (tmp313*tmp413)/tmp177 + (8*tmp388*tmp413)/tmp17&
                          &7 + (tmp289*tmp43)/tmp177 + (tmp289*tmp43)/tmp395 + 2*tmp413*tmp43 + kappa*tmp41&
                          &5*tmp43 + (tmp388*tmp435)/(tmp177*tmp395) + 64*ss*tmp5 + tmp372*tmp5 - (64*tmp5)&
                          &/tmp395 - (48*tmp1*tmp5)/tmp395 + 48*tmp413*tmp5 + tmp388*tmp435*tmp5 + (tmp436*&
                          &tmp5)/tmp17 + kappa*tmp448*tmp5 + (ss*tmp515)/(tmp177*tmp395) + tmp55/tmp177 + t&
                          &mp55/tmp395 - 48*tmp1*tmp58 + f1p*(tmp107 + (tmp108 - (4*(tmp119 + tmp31))/tmp17&
                          &7 + tmp43)/tmp395 + tmp42*(tmp108 + tmp43 + tmp92/tmp177)) - 2*f2p*(tmp42*(tmp10&
                          &5 + tmp106 + tmp43) + tmp3*tmp92 + (2*(tmp104 + tmp175/tmp177 + tmp94))/tmp395) &
                          &+ (tmp324*tmp96)/tmp395 + (tmp43*tmp96)/tmp177 + (tmp43*tmp96)/tmp395 + 16*tmp58&
                          &*tmp96) + log4*Pi*tmp17*tmp177*tmp195*tmp2*tmp24*tmp393*tmp8*(-704*tmp109*tmp180&
                          & - 96*tmp1*tmp109*tmp180 + 128*ss*tmp109*tmp181 - 224*tmp1*tmp109*tmp181 + 16*tm&
                          &p101*tmp109*tmp181 - (288*tmp109*tmp181)/tmp17 + 112*tmp109*tmp172*tmp181 + 896*&
                          &tmp181*tmp182 + 64*tmp1*tmp181*tmp182 - 36*tmp172*tmp183 - 960*ss*tmp184 + (144*&
                          &tmp184)/tmp17 - 40*tmp172*tmp184 + 64*tmp185 + 256*tmp1*tmp109*tmp186 - 80*tmp10&
                          &1*tmp109*tmp186 + (192*tmp109*tmp186)/tmp17 + 208*tmp109*tmp172*tmp186 - 1024*tm&
                          &p182*tmp186 - 320*tmp1*tmp182*tmp186 + 192*tmp109*tmp187 + 160*tmp1*tmp109*tmp18&
                          &7 + tmp188*tmp202 - (48*ss*tmp179*tmp26)/tmp177 + 72*ss*tmp183*tmp26 - 16*tmp184&
                          &*tmp26 + 80*tmp109*tmp186*tmp26 - (160*tmp181*tmp186*tmp26)/tmp177 + ss*tmp181*t&
                          &mp202*tmp26 - 120*tmp109*tmp172*tmp3 + 1280*ss*tmp182*tmp3 + 96*tmp1*tmp182*tmp3&
                          & + 16*tmp101*tmp182*tmp3 + (480*tmp182*tmp3)/tmp17 - 208*tmp172*tmp182*tmp3 - 38&
                          &4*tmp183*tmp3 + 64*tmp1*tmp183*tmp3 - (72*tmp172*tmp186*tmp3)/tmp177 + (136*tmp1&
                          &09*tmp26*tmp3)/tmp17 - 56*tmp182*tmp26*tmp3 + (40*tmp186*tmp26*tmp3)/(tmp17*tmp1&
                          &77) + (120*tmp187*tmp26*tmp3)/tmp177 + tmp181*tmp26*tmp311 + ss*tmp26*tmp3*tmp31&
                          &1 + (tmp1*tmp188*tmp313)/tmp177 + tmp1*tmp188*tmp324 - 2*tmp178*tmp362 + 12*ss*t&
                          &mp179*tmp362 + (7*tmp179*tmp362)/tmp177 - (19*ss*tmp180*tmp362)/tmp177 - 3*ss*tm&
                          &p182*tmp362 + tmp183*tmp362 + 40*tmp181*tmp186*tmp362 - 2*tmp189*tmp362 + 8*tmp1&
                          &09*tmp3*tmp362 + (26*tmp186*tmp3*tmp362)/tmp177 - 30*tmp187*tmp3*tmp362 + tmp101&
                          &*tmp109*tmp3*tmp366 + (tmp188*tmp370)/(tmp17*tmp177) - 56*tmp1*tmp109*tmp181*tmp&
                          &388 - 104*tmp1*tmp109*tmp186*tmp388 + (15*ss*tmp180*tmp26*tmp388)/tmp177 + 47*ss&
                          &*tmp182*tmp26*tmp388 - 11*tmp183*tmp26*tmp388 + 104*tmp1*tmp182*tmp3*tmp388 - 54&
                          &*tmp109*tmp26*tmp3*tmp388 + (30*tmp186*tmp26*tmp3*tmp388)/tmp177 + (tmp179*tmp39&
                          &3)/(tmp17*tmp177) + (108*tmp172*tmp182)/tmp395 + (384*ss*tmp183)/tmp395 + (16*tm&
                          &p1*tmp183)/tmp395 - (24*tmp101*tmp183)/tmp395 - (432*tmp183)/(tmp17*tmp395) + (1&
                          &52*tmp172*tmp183)/tmp395 - (64*tmp184)/tmp395 - (96*tmp1*tmp184)/tmp395 - (1664*&
                          &tmp109*tmp186)/tmp395 - (384*tmp1*tmp109*tmp186)/tmp395 + (28*tmp172*tmp187)/(tm&
                          &p177*tmp395) - (80*ss*tmp182*tmp26)/tmp395 - (84*tmp182*tmp26)/(tmp17*tmp395) + &
                          &(56*tmp183*tmp26)/tmp395 - (20*tmp187*tmp26)/(tmp17*tmp177*tmp395) - (48*tmp188*&
                          &tmp26)/(tmp177*tmp395) + (tmp101*tmp182*tmp317)/tmp395 + (tmp109*tmp119*tmp362)/&
                          &tmp395 - (5*tmp182*tmp362)/tmp395 - (29*tmp187*tmp362)/(tmp177*tmp395) + (12*tmp&
                          &188*tmp362)/tmp395 - (76*tmp1*tmp183*tmp388)/tmp395 - (44*ss*tmp109*tmp26*tmp388&
                          &)/tmp395 + (41*tmp182*tmp26*tmp388)/tmp395 - (15*tmp187*tmp26*tmp388)/(tmp177*tm&
                          &p395) + (tmp179*tmp388*tmp403)/tmp177 + (ss*tmp180*tmp26*tmp408)/tmp177 + tmp183&
                          &*tmp26*tmp408 + (ss*tmp181*tmp362*tmp408)/tmp177 - (52*tmp180*tmp413)/(tmp17*tmp&
                          &177) + 128*tmp109*tmp181*tmp413 + (52*tmp182*tmp413)/tmp17 + 240*tmp183*tmp413 +&
                          & (168*tmp183*tmp413)/tmp17 - 160*tmp184*tmp413 + 80*kappa*tmp184*tmp413 - 320*tm&
                          &p109*tmp3*tmp413 - (144*tmp109*tmp3*tmp413)/tmp17 + 64*tmp182*tmp3*tmp413 - 32*k&
                          &appa*tmp182*tmp3*tmp413 - 84*tmp183*tmp388*tmp413 + 72*tmp109*tmp3*tmp388*tmp413&
                          & - (96*tmp182*tmp413)/tmp395 - (160*tmp182*tmp413)/(tmp17*tmp395) + (128*tmp183*&
                          &tmp413)/tmp395 + (80*tmp182*tmp388*tmp413)/tmp395 + kappa*tmp184*tmp418 - 12*ss*&
                          &tmp109*tmp424 - (12*ss*tmp181*tmp424)/tmp177 - (12*tmp109*tmp424)/tmp395 - (12*t&
                          &mp186*tmp424)/(tmp177*tmp395) + tmp105*tmp180*tmp43 + 112*ss*tmp182*tmp43 + 10*t&
                          &mp1*tmp182*tmp43 - 40*tmp183*tmp43 + 8*tmp1*tmp183*tmp43 + tmp105*tmp187*tmp43 +&
                          & tmp180*tmp266*tmp43 + tmp187*tmp266*tmp43 + (tmp180*tmp289*tmp43)/tmp177 + (tmp&
                          &187*tmp289*tmp43)/tmp177 - 96*tmp109*tmp3*tmp43 + 48*tmp1*tmp109*tmp3*tmp43 + (t&
                          &mp180*tmp393*tmp43)/tmp177 + tmp182*tmp393*tmp43 + (tmp187*tmp393*tmp43)/tmp177 &
                          &- (64*ss*tmp109*tmp43)/tmp395 - (28*tmp1*tmp109*tmp43)/tmp395 + (112*tmp182*tmp4&
                          &3)/tmp395 - (32*tmp1*tmp182*tmp43)/tmp395 + (8*tmp1*tmp186*tmp43)/(tmp177*tmp395&
                          &) - 28*tmp109*tmp413*tmp43 + (8*tmp181*tmp413*tmp43)/tmp177 - 32*tmp182*tmp413*t&
                          &mp43 + kappa*tmp109*tmp43*tmp432 + tmp101*tmp183*tmp437 + (tmp109*tmp172*tmp437)&
                          &/tmp395 + tmp184*tmp441 + kappa*tmp185*tmp441 + (kappa*tmp109*tmp43*tmp448)/tmp3&
                          &95 + (tmp101*tmp109*tmp449)/tmp395 + tmp1*tmp109*tmp43*tmp449 + (88*tmp172*tmp18&
                          &1*tmp46)/tmp177 - 448*tmp1*tmp182*tmp46 - 272*tmp172*tmp182*tmp46 + 1792*tmp183*&
                          &tmp46 + 320*tmp1*tmp183*tmp46 + (104*tmp109*tmp26*tmp46)/tmp17 + (120*tmp180*tmp&
                          &26*tmp46)/tmp177 - (40*tmp181*tmp26*tmp46)/(tmp17*tmp177) - 120*tmp182*tmp26*tmp&
                          &46 + 2048*tmp109*tmp3*tmp46 + 192*tmp1*tmp109*tmp3*tmp46 + tmp202*tmp26*tmp3*tmp&
                          &46 - 30*tmp180*tmp362*tmp46 + (6*tmp181*tmp362*tmp46)/tmp177 + 136*tmp1*tmp182*t&
                          &mp388*tmp46 - 78*tmp109*tmp26*tmp388*tmp46 - (30*tmp181*tmp26*tmp388*tmp46)/tmp1&
                          &77 + (tmp3*tmp362*tmp394*tmp46)/tmp177 - (224*tmp1*tmp109*tmp46)/tmp395 - (160*t&
                          &mp109*tmp46)/(tmp17*tmp395) - (176*tmp109*tmp172*tmp46)/tmp395 + (896*tmp182*tmp&
                          &46)/tmp395 + (192*tmp1*tmp182*tmp46)/tmp395 - (48*tmp109*tmp26*tmp46)/tmp395 + (&
                          &88*tmp1*tmp109*tmp388*tmp46)/tmp395 + tmp181*tmp362*tmp408*tmp46 + tmp182*tmp420&
                          &*tmp46 + (18*tmp3*tmp424*tmp46)/tmp177 - 96*tmp109*tmp43*tmp46 - (12*tmp1*tmp3*t&
                          &mp43*tmp46)/tmp177 + tmp211*tmp26*tmp3*tmp43*tmp46 + tmp1*tmp109*tmp457*tmp46 + &
                          &tmp101*tmp182*tmp463 + ss*tmp109*tmp26*tmp467 + (ss*tmp181*tmp26*tmp467)/tmp177 &
                          &+ (tmp109*tmp26*tmp467)/tmp395 + (tmp186*tmp26*tmp467)/(tmp177*tmp395) + kappa*t&
                          &mp182*tmp413*tmp467 - 208*tmp183*tmp49 - 76*tmp182*tmp26*tmp49 - 256*tmp109*tmp3&
                          &*tmp49 + (160*tmp182*tmp49)/tmp395 + (80*tmp109*tmp26*tmp49)/tmp395 + ss*tmp180*&
                          &tmp362*tmp499 - 48*tmp1*tmp179*tmp5 - (48*tmp179*tmp5)/tmp17 - 36*tmp172*tmp180*&
                          &tmp5 - 40*tmp179*tmp26*tmp5 + 136*ss*tmp180*tmp26*tmp5 + (36*tmp180*tmp26*tmp5)/&
                          &tmp17 - 24*tmp188*tmp26*tmp5 + 288*tmp1*tmp186*tmp3*tmp5 - 80*tmp101*tmp186*tmp3&
                          &*tmp5 + (288*tmp186*tmp3*tmp5)/tmp17 + 80*tmp172*tmp186*tmp3*tmp5 - 7*tmp180*tmp&
                          &362*tmp5 - 15*tmp187*tmp362*tmp5 + (tmp187*tmp370*tmp5)/tmp17 + tmp1*tmp188*tmp3&
                          &80*tmp5 + tmp180*tmp26*tmp388*tmp5 - 23*tmp187*tmp26*tmp388*tmp5 + tmp188*tmp299&
                          &*tmp388*tmp5 - 40*tmp1*tmp186*tmp3*tmp388*tmp5 - (136*tmp172*tmp186*tmp5)/tmp395&
                          & - (112*tmp1*tmp187*tmp5)/tmp395 + (40*tmp101*tmp187*tmp5)/tmp395 - (112*tmp187*&
                          &tmp5)/(tmp17*tmp395) - (40*tmp172*tmp187*tmp5)/tmp395 - (48*tmp186*tmp26*tmp5)/(&
                          &tmp17*tmp395) + (56*tmp187*tmp26*tmp5)/tmp395 + (20*tmp186*tmp362*tmp5)/tmp395 +&
                          & (68*tmp186*tmp26*tmp388*tmp5)/tmp395 + tmp181*tmp303*tmp408*tmp5 + 208*tmp180*t&
                          &mp413*tmp5 + (40*tmp180*tmp413*tmp5)/tmp17 - 20*tmp180*tmp388*tmp413*tmp5 + tmp1&
                          &87*tmp418*tmp5 + (kappa*tmp187*tmp418*tmp5)/tmp395 - 12*tmp181*tmp424*tmp5 - 12*&
                          &tmp186*tmp424*tmp5 - 32*ss*tmp181*tmp43*tmp5 + tmp181*tmp299*tmp43*tmp5 + tmp186&
                          &*tmp299*tmp43*tmp5 + tmp198*tmp26*tmp3*tmp43*tmp5 - (32*tmp186*tmp43*tmp5)/tmp39&
                          &5 - (32*tmp1*tmp186*tmp43*tmp5)/tmp395 - 32*tmp181*tmp413*tmp43*tmp5 + kappa*tmp&
                          &3*tmp43*tmp432*tmp5 + (tmp179*tmp435*tmp5)/tmp17 + tmp179*tmp388*tmp436*tmp5 + s&
                          &s*tmp3*tmp43*tmp436*tmp5 + tmp101*tmp180*tmp437*tmp5 + tmp3*tmp43*tmp449*tmp5 + &
                          &tmp1*tmp3*tmp43*tmp449*tmp5 + tmp3*tmp303*tmp450*tmp5 + tmp26*tmp3*tmp451*tmp5 +&
                          & tmp188*tmp457*tmp5 + tmp181*tmp362*tmp457*tmp5 - 352*tmp1*tmp181*tmp46*tmp5 - (&
                          &352*tmp181*tmp46*tmp5)/tmp17 - 80*tmp172*tmp181*tmp46*tmp5 - 144*tmp181*tmp26*tm&
                          &p46*tmp5 + 176*tmp172*tmp3*tmp46*tmp5 + (120*tmp26*tmp3*tmp46*tmp5)/tmp17 - 2*tm&
                          &p3*tmp362*tmp46*tmp5 + 40*tmp1*tmp181*tmp388*tmp46*tmp5 - 66*tmp26*tmp3*tmp388*t&
                          &mp46*tmp5 - (16*tmp26*tmp43*tmp46*tmp5)/tmp395 + (tmp43*tmp436*tmp46*tmp5)/tmp39&
                          &5 + (tmp101*tmp450*tmp46*tmp5)/tmp395 + tmp101*tmp181*tmp463*tmp5 + ss*tmp181*tm&
                          &p464*tmp5 + kappa*tmp188*tmp466*tmp5 + tmp181*tmp26*tmp467*tmp5 + tmp186*tmp26*t&
                          &mp467*tmp5 + kappa*tmp181*tmp413*tmp467*tmp5 + 208*tmp180*tmp49*tmp5 - 112*tmp18&
                          &1*tmp26*tmp49*tmp5 + tmp1*tmp183*tmp501 + tmp1*tmp180*tmp5*tmp501 + tmp1*tmp187*&
                          &tmp5*tmp501 + (tmp188*tmp506)/(tmp17*tmp177) + (tmp180*tmp43*tmp506)/tmp177 + tm&
                          &p182*tmp43*tmp506 + (tmp187*tmp43*tmp506)/tmp177 + (tmp179*tmp509)/tmp177 + kapp&
                          &a*ss*tmp109*tmp181*tmp511 + (kappa*ss*tmp183*tmp511)/tmp395 + tmp185*tmp515 + (s&
                          &s*tmp109*tmp43*tmp515)/tmp395 + (tmp178*tmp519)/tmp177 + kappa*tmp184*tmp519 + (&
                          &tmp189*tmp519)/tmp177 + kappa*tmp179*tmp5*tmp519 + 192*tmp179*tmp58 - 832*ss*tmp&
                          &180*tmp58 + 176*tmp1*tmp180*tmp58 - 24*tmp101*tmp180*tmp58 + (144*tmp180*tmp58)/&
                          &tmp17 + 72*tmp172*tmp181*tmp58 - 48*tmp172*tmp186*tmp58 - 48*tmp1*tmp187*tmp58 +&
                          & 40*tmp101*tmp187*tmp58 - (80*tmp187*tmp58)/tmp17 - 72*tmp172*tmp187*tmp58 - 32*&
                          &tmp1*tmp188*tmp58 + 64*tmp180*tmp26*tmp58 - (104*tmp181*tmp26*tmp58)/tmp17 - (56&
                          &*tmp186*tmp26*tmp58)/tmp17 - 1152*tmp186*tmp3*tmp58 - 320*tmp1*tmp186*tmp3*tmp58&
                          & + tmp101*tmp181*tmp317*tmp58 - 2*tmp181*tmp362*tmp58 + 10*tmp186*tmp362*tmp58 +&
                          & 26*tmp181*tmp26*tmp388*tmp58 + 62*tmp186*tmp26*tmp388*tmp58 - 38*ss*tmp26*tmp3*&
                          &tmp388*tmp58 + (416*tmp1*tmp186*tmp58)/tmp395 - (96*tmp101*tmp186*tmp58)/tmp395 &
                          &+ (544*tmp186*tmp58)/(tmp17*tmp395) + (224*tmp172*tmp186*tmp58)/tmp395 + (448*tm&
                          &p187*tmp58)/tmp395 + (160*tmp1*tmp187*tmp58)/tmp395 - (112*tmp1*tmp186*tmp388*tm&
                          &p58)/tmp395 - 160*tmp180*tmp413*tmp58 + 80*kappa*tmp180*tmp413*tmp58 - 32*tmp181&
                          &*tmp413*tmp58 + (96*tmp181*tmp413*tmp58)/tmp17 + (64*tmp3*tmp413*tmp58)/tmp17 - &
                          &48*tmp181*tmp388*tmp413*tmp58 + 18*tmp3*tmp424*tmp58 - 32*tmp1*tmp181*tmp43*tmp5&
                          &8 - 32*tmp1*tmp186*tmp43*tmp58 + tmp198*tmp3*tmp43*tmp58 - 24*tmp26*tmp3*tmp43*t&
                          &mp58 + (tmp198*tmp26*tmp43*tmp58)/tmp395 + (tmp26*tmp433*tmp58)/tmp395 + (tmp180&
                          &*tmp435*tmp58)/tmp17 + tmp180*tmp388*tmp436*tmp58 + tmp1*tmp3*tmp440*tmp58 + kap&
                          &pa*tmp179*tmp441*tmp58 + kappa*tmp3*tmp43*tmp448*tmp58 + tmp101*tmp3*tmp449*tmp5&
                          &8 + tmp3*tmp362*tmp45*tmp58 + (tmp303*tmp450*tmp58)/tmp395 + 1408*tmp181*tmp46*t&
                          &mp58 + 320*tmp1*tmp181*tmp46*tmp58 - 512*tmp1*tmp3*tmp46*tmp58 - (704*tmp3*tmp46&
                          &*tmp58)/tmp17 - 240*tmp172*tmp3*tmp46*tmp58 + 120*tmp1*tmp3*tmp388*tmp46*tmp58 +&
                          & (40*tmp172*tmp46*tmp58)/tmp395 + (72*tmp26*tmp46*tmp58)/(tmp17*tmp395) - (6*tmp&
                          &362*tmp46*tmp58)/tmp395 - (50*tmp26*tmp388*tmp46*tmp58)/tmp395 + 18*tmp424*tmp46&
                          &*tmp58 - 24*tmp26*tmp43*tmp46*tmp58 - (16*tmp43*tmp46*tmp58)/tmp395 + tmp1*tmp44&
                          &0*tmp46*tmp58 + (kappa*tmp43*tmp441*tmp46*tmp58)/tmp395 + tmp181*tmp467*tmp58 + &
                          &tmp186*tmp467*tmp58 + (tmp413*tmp467*tmp58)/tmp395 + kappa*tmp187*tmp474*tmp58 +&
                          & 96*tmp181*tmp49*tmp58 + 88*tmp26*tmp3*tmp49*tmp58 + tmp179*tmp515*tmp58 + ss*tm&
                          &p3*tmp43*tmp515*tmp58 + (tmp43*tmp46*tmp515*tmp58)/tmp395 + (f2p*(tmp180/tmp17 +&
                          & tmp29*((ss*(tmp175 + tmp246))/tmp177 + (tmp193 + tmp245)*tmp30 + tmp316) + (2*t&
                          &mp42*(tmp190 + (tmp197*tmp217)/tmp177 + (-11/tmp17 + tmp244)*tmp5))/tmp395 - (tm&
                          &p315 + (tmp192 + tmp200)*tmp5 + ((ss + tmp19)*tmp60)/tmp177)*tmp61 - 2*tmp181*(t&
                          &mp301 + (5*tmp71)/tmp177)))/(tmp177*tmp24*tmp53) - (f1p*(6*tmp179 - (26*ss + 1/t&
                          &mp17 + 22/tmp177)*tmp180 + tmp29*((ss*(1/tmp17 + tmp317))/tmp177 - 3*tmp46*tmp47&
                          &2 + (tmp192 + tmp193)*tmp5 - 6*tmp58) - (2*tmp42*((ss*(-19*ss + tmp37))/tmp177 +&
                          & (7*ss + tmp19)*tmp46 + (-5*ss + tmp194)*tmp5 + tmp58))/tmp395 + tmp181*(tmp196 &
                          &+ (11*ss + 1/tmp17)*tmp60 + (tmp191 + tmp60)/tmp177) + tmp61*(tmp314 + ((ss + tm&
                          &p191)*tmp36)/tmp177 + (-26*ss + tmp175)*tmp5 - tmp46*tmp71)))/(tmp177*tmp24*tmp5&
                          &3) + 2*tmp179*tmp388*tmp84 - (9*tmp180*tmp388*tmp84)/tmp177 - tmp182*tmp388*tmp8&
                          &4 + (7*tmp187*tmp388*tmp84)/tmp177 - 2*tmp188*tmp388*tmp84 - 20*tmp186*tmp3*tmp3&
                          &88*tmp84 + tmp109*tmp36*tmp388*tmp84 + (6*tmp109*tmp388*tmp84)/tmp395 - (12*tmp1&
                          &86*tmp388*tmp84)/(tmp177*tmp395) + (10*tmp187*tmp388*tmp84)/tmp395 - 8*tmp186*tm&
                          &p388*tmp5*tmp84 + tmp203*tmp3*tmp388*tmp5*tmp84 + (tmp188*tmp522*tmp84)/tmp177 -&
                          & 14*tmp3*tmp388*tmp58*tmp84 + tmp319*tmp388*tmp58*tmp84 + (tmp25*tmp388*tmp58*tm&
                          &p84)/tmp395 + ss*tmp181*tmp362*tmp94 + 48*tmp109*tmp180*tmp96 - 32*tmp181*tmp182&
                          &*tmp96 + 160*tmp182*tmp186*tmp96 - 80*tmp109*tmp187*tmp96 - 32*tmp183*tmp3*tmp96&
                          & + (48*tmp184*tmp96)/tmp395 + (192*tmp109*tmp186*tmp96)/tmp395 - 24*tmp109*tmp3*&
                          &tmp43*tmp96 - 160*tmp183*tmp46*tmp96 - 96*tmp109*tmp3*tmp46*tmp96 - (96*tmp182*t&
                          &mp46*tmp96)/tmp395 + (tmp182*tmp467*tmp96)/tmp395 + (tmp186*tmp467*tmp5*tmp96)/t&
                          &mp395 + 16*tmp188*tmp58*tmp96 + 160*tmp186*tmp3*tmp58*tmp96 - (80*tmp187*tmp58*t&
                          &mp96)/tmp395 - 160*tmp181*tmp46*tmp58*tmp96 + tmp181*tmp467*tmp58*tmp96 + tmp186&
                          &*tmp467*tmp58*tmp96) + 16*C0tP2P2NP0*Pi*tmp17*tmp2*tmp26*tmp53*(tmp104 + tmp106 &
                          &+ tmp203/tmp177 - 2*tmp3 + tmp289*tmp3 + tmp356 + (tmp368*tmp388)/tmp177 + (kapp&
                          &a*tmp299)/(tmp177*tmp395) - (4*tmp413)/tmp177 + tmp388*tmp413 + (tmp125*tmp413)/&
                          &tmp395 + (ss*tmp436)/tmp395 + tmp436/(tmp177*tmp395) - 2*tmp46 - (f2p*tmp47)/tmp&
                          &17 + 10*tmp5 + 6*tmp1*tmp5 + tmp50 + tmp54 + tmp55 + tmp56 + tmp57 + tmp60/tmp39&
                          &5 + f1p*(tmp3 + tmp332 + tmp48 + tmp49 - 5*tmp5 + (tmp10 + tmp60)/tmp177) + tmp3&
                          &*tmp96 + tmp46*tmp96 + tmp74*tmp96) + 8*C0tF2F2LF0*Pi*tmp1*tmp17*tmp53*(tmp2*((t&
                          &mp247 + (tmp15*tmp380 + tmp393)/tmp177)/tmp17 + (2*((8*(tmp172 + tmp173))/tmp177&
                          & + tmp247))/tmp395) - (4*f1p*tmp2*((2*tmp173)/tmp395 + tmp43))/tmp177 + (f2p*tmp&
                          &307*(tmp6 + tmp96))/tmp17) + logP2L2*Pi*tmp1*tmp17*tmp177*tmp195*tmp24*tmp395*tm&
                          &p8*(-((f2p*tmp28*(tmp455 + tmp179*(tmp1*(1/tmp17 + tmp245)*tmp380 + tmp105*tmp45&
                          &6 + (tmp191*(80*ss + tmp368 + tmp457))/tmp177) + (tmp178*tmp380)/tmp53 + 2*tmp18&
                          &1*((224*tmp109)/tmp17 + tmp172*(tmp245 + tmp37)*tmp438 + (ss*tmp191*(tmp376 + tm&
                          &p412 + tmp427/tmp17 + tmp462 + tmp463))/tmp177 + tmp5*(tmp198*(tmp351 + tmp379 +&
                          & 4*tmp43) + (288*tmp46)/tmp17 + tmp172*(34/tmp17 + tmp51)) + 4*(tmp26 - 24*tmp43&
                          & + tmp464 + 40*tmp49)*tmp58) + tmp180*(((tmp19 + tmp245)*tmp448)/tmp17 - 4*(-19*&
                          &tmp172 + tmp26 + 4*tmp384 - 32*tmp43)*tmp5 - 16*(tmp1 + tmp458)*tmp58 + (-38*tmp&
                          &172 + tmp26 + 320*tmp46 + (tmp459 + tmp460)*tmp60)/(tmp17*tmp177)) + (2*(tmp109*&
                          &(48/tmp17 + tmp299) + (tmp318*tmp436*tmp46)/tmp17 - ((tmp374 + tmp393 + 19*tmp41&
                          &3 + tmp416 + tmp465)*tmp49)/tmp177 + ((tmp1 + 10/tmp17)*tmp172 + (80/tmp17 + tmp&
                          &299)*tmp46 + tmp36*(tmp1*tmp238 + tmp466 + tmp467))*tmp5 - (35*tmp172 + tmp294 +&
                          & tmp370 + tmp467 + 112*tmp49)*tmp58)*tmp61)/tmp395 - 4*tmp3*tmp42*(tmp109*(88/tm&
                          &p17 + tmp299) + ((tmp291 + 29*tmp413 + tmp422 + 40*tmp46)*tmp49)/tmp177 - 2*(tmp&
                          &37*tmp413 - 7*tmp1*tmp43 + tmp46*(tmp289 + tmp461))*tmp5 - ((45*tmp1 + tmp119 + &
                          &tmp460)*tmp58)/tmp17 + (tmp356*(tmp245 + tmp92))/tmp17)))/tmp53) - (4*f1p*tmp2*(&
                          &tmp455 + 8*tmp178*(-6*tmp413 + (tmp37 + tmp435 + tmp443)/tmp177 + 20*tmp5) + 4*t&
                          &mp3*tmp42*(8*tmp182 + tmp109*(-66/tmp17 + tmp436 + tmp437) + tmp436*tmp46*(tmp30&
                          &1 + tmp43 + 3*tmp46) + tmp30*(36*tmp186 + tmp360*tmp43 + (45/tmp17 + tmp441)*tmp&
                          &46) + (tmp13*(tmp377*tmp43 + tmp453 + (46/tmp17 + tmp436)*tmp46 + (tmp174 + tmp4&
                          &42)*tmp49))/tmp177 + (31*tmp172 + 20*tmp413 + tmp438 + tmp439 + tmp440)*tmp58) -&
                          & 2*tmp181*(80*tmp182 + (tmp36*(120*tmp186 + (tmp1 + tmp204)*tmp320 + (21/tmp17 +&
                          & tmp299)*tmp334 + tmp172*(tmp339 + tmp377)))/tmp177 - 4*tmp109*(42/tmp17 + tmp44&
                          &3 + tmp452) - 12*tmp1*tmp46*(tmp301 + tmp43 + 5*tmp46) + (-160*tmp186 + tmp366*(&
                          &tmp344 + tmp379) + tmp447 - 8*(23/tmp17 + tmp427)*tmp46)*tmp5 - 4*(tmp26 + tmp37&
                          &4 - 18*tmp43 + tmp36*(tmp204 + tmp435) + tmp444)*tmp58) + (2*(tmp342*tmp377*tmp4&
                          &6 + (tmp13*(((tmp289 + tmp37)*tmp377)/tmp17 + tmp334*(tmp175 + tmp377) + tmp451 &
                          &+ (tmp452 + tmp458)*tmp49))/tmp177 + (tmp172*tmp383 + tmp453 + (76/tmp17 + tmp29&
                          &9)*tmp46 + tmp36*(tmp450 + tmp454 + tmp466))*tmp5 - (27*tmp172 + tmp370 + tmp448&
                          & + tmp449 + tmp450 + 92*tmp49)*tmp58 + tmp401*(tmp204 + tmp289 + tmp60))*tmp61)/&
                          &tmp395 + 8*tmp468*tmp7 - 2*tmp179*(tmp289*(tmp301 + tmp43 + 30*tmp46) + (-112*tm&
                          &p413 + tmp428 + tmp450 + 240*tmp46 + 68*tmp49)/tmp177 + 160*tmp58 + (60*ss - 22*&
                          &tmp1 + tmp37)*tmp94) + tmp180*(320*tmp109 + (640*tmp186 + tmp374*(19/tmp17 + tmp&
                          &441) + tmp320*(tmp1 + tmp442) + tmp447)/tmp177 + tmp448*(tmp301 + tmp43 + 10*tmp&
                          &46) + 16*(-13*tmp1 + tmp199 + tmp366)*tmp58 + (tmp1*tmp229 + tmp352 + tmp379 + t&
                          &mp440 + tmp463 + ss*tmp495)*tmp94)))/(tmp177*tmp53) - 4*tmp2*((tmp289*tmp396*tmp&
                          &471*tmp5)/tmp24 + 2*tmp468*((8*tmp473)/tmp177 - 32*tmp1*tmp12*tmp5 + tmp504 + 64&
                          &*tmp12*tmp58) + tmp179*(-128*(-20 + tmp1)*tmp182 - 10*kappa*ss*(1/tmp17 + tmp217&
                          &)*tmp22 + (tmp26*(-19*tmp303 - 9*tmp26*tmp388 + 240*tmp15*tmp46 + tmp43*tmp481 +&
                          & (16 + tmp482)*tmp49))/tmp177 + 16*tmp109*(-128*tmp1 + (15 + tmp1)*tmp366 + tmp3&
                          &81 + tmp458 + tmp474 + tmp506) + tmp1*(-((-20 + (72 + kappa)*tmp1)*tmp172) + 7*t&
                          &mp362 + 960*tmp12*tmp46 + tmp491 + (tmp377*(-56 + tmp1*(-19 + tmp423)) + (84 + t&
                          &mp497)/tmp17)*tmp60)*tmp74 + 4*tmp58*((8 + (-74 + kappa)*tmp1)*tmp172 + 88*tmp26&
                          & - 6*tmp362 + 480*tmp12*tmp46 + tmp487 + (-172*tmp1 + tmp26*(-18 + tmp423) + (68&
                          & + tmp485)/tmp17)*tmp60 + 14*tmp84)) - tmp178*(256*(5 + tmp1)*tmp109 + tmp125*tm&
                          &p22*tmp472 + tmp299*(-32*tmp1 + 96*ss*tmp12 + (-7 + kappa)*tmp376 + tmp458 + tmp&
                          &172*tmp476)*tmp5 + 16*(-52*tmp1 + 48*ss*tmp12 + tmp15*tmp458 + tmp26*tmp486)*tmp&
                          &58 + (-7*kappa*tmp22 + 96*ss*tmp473 + (tmp475*tmp84)/tmp17)/tmp177) + tmp321*(25&
                          &6*(-5 + tmp1)*tmp183 + 16*tmp182*(72*tmp1 + (-4 + tmp1)*tmp366 + tmp379*tmp477 -&
                          & (12*tmp478)/tmp17) + tmp1*tmp5*(640*tmp12*tmp186 + tmp36*(tmp362 + tmp172*(2 + &
                          &(-8 + kappa)*tmp363) + tmp491) + tmp334*(108/tmp17 + tmp376*tmp488 + tmp511 + tm&
                          &p516) + tmp172*(tmp1 + tmp191*(11 + tmp1*tmp423) + tmp430 + kappa*tmp519)) + 10*&
                          &kappa*tmp22*tmp46*tmp71 - tmp58*(1280*tmp12*tmp186 + 8*tmp1*tmp43*(-23 + tmp1*tm&
                          &p484) + tmp374*(-84*tmp1 + (76 + tmp485)/tmp17 + tmp26*tmp502) + tmp483*tmp84 + &
                          &(-56*tmp26 + (64 - 13*kappa)*tmp84)/tmp17 + tmp119*(22*tmp26 + tmp351*(2 + tmp1*&
                          &tmp486) + tmp487 - 2*tmp2*tmp84)) + tmp401*(-96*tmp172 - 48*tmp26 + ((38 - 7*kap&
                          &pa)*tmp26)/tmp17 + 64*(-5 + tmp277)*tmp46 - 32*tmp43*tmp479 + tmp496 + tmp203*(-&
                          &30*tmp1 + tmp517 + (-1 + tmp277)*tmp92)) + (tmp379*(160*tmp15*tmp186 + tmp46*(ka&
                          &ppa*tmp403 + (32 + tmp482)/tmp17) + tmp301*(-5*tmp101 + tmp494) - tmp357*tmp96))&
                          &/tmp177) - 2*tmp181*(64*(-10 + tmp1)*tmp184 + tmp186*tmp22*tmp423*tmp492 + 16*tm&
                          &p183*((7 + tmp1)*tmp458 + tmp198*tmp478 + tmp26*tmp493 + tmp515) - 4*tmp182*((21&
                          &2 + (-2 + 13*kappa)*tmp1)*tmp172 + (-4 + tmp363)*tmp431 + tmp376*(7 + tmp1*tmp49&
                          &3) + tmp503 + tmp25*(tmp26*tmp477 + tmp495 + tmp518)) + tmp413*tmp5*(480*tmp12*t&
                          &mp186 + tmp334*(tmp377*(-8 + tmp1*(13 + tmp423)) + (132 + tmp497)/tmp17) + (tmp3&
                          &77*(tmp388*tmp427 + tmp162*tmp43 + tmp436 + tmp498 + tmp499))/tmp17 + ss*(tmp362&
                          & + tmp450*tmp490 + tmp172*(16 + tmp277*tmp521))) - tmp58*(960*tmp12*tmp187 + tmp&
                          &451*(tmp26*(46 + tmp423) + tmp436 + (7 + tmp363)*tmp458) + (tmp26*(tmp289 + kapp&
                          &a*tmp389 + tmp501 + tmp508 + tmp172*tmp522))/tmp17 + tmp334*(tmp351*(36 + tmp1*t&
                          &mp502) + tmp503 + tmp376*(-18 + tmp1*tmp527)) + ss*(8*tmp1*(-22 + tmp1*(-6 + tmp&
                          &423))*tmp43 + tmp504 + 32*tmp84 + (32*tmp26 + (32 - 19*kappa)*tmp84)/tmp17)) + (&
                          &ss*tmp379*(120*tmp15*tmp186 + kappa*tmp357*tmp368 + tmp46*(13*tmp101 + (48 + tmp&
                          &482)/tmp17) + tmp296*(tmp494 + tmp89)))/tmp177 + tmp109*(256*(5 + tmp174)*tmp186&
                          & + ((120 + (-56 + 27*kappa)*tmp1)*tmp26)/tmp17 + (320*tmp1 + (-18 + kappa)*tmp39&
                          &3)*tmp43 + tmp25*(-64*tmp26 + tmp474 + tmp496 + tmp514 + (tmp26*tmp523)/tmp17) +&
                          & tmp374*(tmp495 + tmp525 + (23 + tmp459)*tmp92) - 4*tmp84*(2 + tmp96))) - tmp3*(&
                          &256*tmp15*tmp185 + tmp125*tmp187*tmp22*tmp505 + (tmp26*tmp46*(96*tmp15*tmp186 + &
                          &kappa*tmp287*tmp357 + tmp46*(29*tmp101 + (64 + tmp482)/tmp17) + tmp320*(tmp494 +&
                          & tmp506)))/tmp177 - 16*tmp184*(132/tmp17 + tmp299 + tmp376 + tmp474 + tmp506 + t&
                          &mp366*tmp507) + 4*tmp183*(380*tmp172 - 384*(-1 + tmp1)*tmp46 + tmp466 + 64*tmp43&
                          &*tmp479 + tmp366*(14*tmp1 + tmp101 + tmp508 + tmp509) + (tmp26*(34 + tmp528))/tm&
                          &p17 + 2*(-5 + tmp158)*tmp84) - 2*tmp413*tmp5*(192*tmp12*tmp187 + kappa*tmp357*tm&
                          &p404 + 6*(4 + tmp27 + 2*tmp388)*tmp413*tmp43 + 2*tmp186*(156/tmp17 + tmp376*(29 &
                          &+ tmp423) + tmp515 + tmp516) + tmp319*((30 + (24 + 17*kappa)*tmp1)*tmp172 + tmp4&
                          &91 + tmp423*tmp84)) + tmp182*(512*(4 + tmp174)*tmp186 + ((248 + (-48 + 41*kappa)&
                          &*tmp1)*tmp379)/tmp17 + tmp22*tmp423 + tmp43*tmp441*(32 + tmp277*tmp510) + tmp431&
                          &*(3*tmp26*tmp477 + tmp511 + tmp513) + tmp198*(106*tmp172 + tmp487 + tmp519 + tmp&
                          &520 + 2*tmp79*tmp84)) - 2*tmp109*(128*tmp187*(15 + tmp452) + 32*tmp186*((22 + tm&
                          &p158)*tmp26 + tmp287 + tmp513) + tmp334*(-52*tmp26 + tmp172*(96 + tmp1*(50 + tmp&
                          &500)) + tmp514 + 6*tmp268*tmp84) + tmp36*(tmp111*tmp22 + tmp43*tmp436*(-28 + tmp&
                          &277*tmp79) + (-62*tmp26 + (8 - 11*kappa)*tmp84)/tmp17) + (tmp26*(tmp501 + tmp506&
                          & + tmp19*(-20 + tmp96)))/tmp17) + tmp314*(384*tmp12*tmp188 + tmp358*tmp362 + 8*t&
                          &mp187*(92*tmp1 + tmp26*(78 + tmp423) + (92 + tmp485)/tmp17) + 8*tmp186*((98 + tm&
                          &p1*(58 + tmp151))*tmp172 + (-7 + tmp1*(5 + tmp158))*tmp376 + tmp487) + tmp376*tm&
                          &p49*(tmp101 + tmp430 + (-16 + tmp1*tmp500)/tmp17) + tmp46*(tmp158*tmp22 + 8*tmp1&
                          &*(24 + tmp1*(6 + tmp151))*tmp43 + (tmp26*(28 + 25*tmp96))/tmp17))) + (tmp42*(128&
                          &*tmp1*tmp185 + tmp111*tmp187*tmp22*tmp318 + (tmp379*tmp46*(kappa*tmp289*tmp357 +&
                          & tmp15*tmp451 + tmp46*(kappa*tmp404 + (16 + tmp1*tmp475)/tmp17) + tmp49*(tmp26*t&
                          &mp423 + tmp494)))/tmp177 + tmp413*tmp5*(64*tmp12*tmp187 + kappa*tmp357*tmp390 + &
                          &4*tmp186*((6 + (7 + kappa)*tmp1)*tmp377 + (36 + tmp1*tmp476)/tmp17) + (tmp413*(t&
                          &mp1*tmp388 + tmp434 + tmp441 + tmp460 + tmp506))/tmp17 + tmp334*(2*tmp362 + tmp4&
                          &3*tmp490 + tmp428*(3 + tmp1*tmp510))) - 16*tmp184*(tmp119*(2 + tmp363) + tmp435 &
                          &+ tmp517 + tmp518) - tmp182*(256*tmp186*(6 + tmp363) + tmp504 + (tmp26*(124 + tm&
                          &p1*tmp521))/tmp17 + tmp43*tmp436*(44 + tmp1*tmp523) + tmp431*(tmp289 + (7 + tmp2&
                          &77)*tmp458 + tmp525) + 16*tmp84 + tmp366*(77*tmp172 - 18*tmp26 + ((4 + kappa)*tm&
                          &p370)/tmp17 + tmp450*tmp479 + tmp112*tmp84)) + 4*tmp183*(144*tmp172 + 64*(4 + tm&
                          &p363)*tmp46 + tmp487 + tmp519 + tmp520 + 2*tmp2*tmp84 + tmp366*((3 + kappa)*tmp2&
                          &6 + tmp360 + (8 + tmp277)*tmp92)) + tmp109*(128*tmp187*(8 + tmp363) + tmp433*(tm&
                          &p174 + tmp12*tmp458 + tmp26*tmp526) + tmp438*(142*tmp172 - 42*tmp26 + tmp204*tmp&
                          &26*tmp477 + tmp440*tmp479 + 2*tmp527*tmp84) + (tmp26*(tmp434 + tmp436 + (32 + tm&
                          &p480)/tmp17 + tmp89))/tmp17 + tmp36*((tmp376*(33 + tmp1*(-2 + tmp522)))/tmp17 + &
                          &tmp377*tmp43*(92 + tmp1*(18 + tmp528)) - tmp84*(24 + tmp96))) + tmp13*tmp58*(128&
                          &*tmp12*tmp187 + tmp374*(tmp26 + tmp172*(39 + (10 + kappa)*tmp377) + 4*tmp43*tmp4&
                          &79 + 2*tmp510*tmp84) + tmp451*(28*tmp1 + (18 + kappa)*tmp26 + (5 + tmp277)*tmp92&
                          &) + tmp36*((tmp26*(26 + (4 + tmp151)*tmp277))/tmp17 + tmp377*tmp43*(68 + tmp1*(1&
                          &8 + tmp500)) + tmp84*(-32 + tmp96)) + (tmp26*(kappa*tmp467 + tmp51*(12 + tmp96) &
                          &+ (56 + 65*tmp96)/tmp17))/tmp17)))/tmp395)) + 16*DB0tLN*Pi*tmp17*tmp2*tmp24*tmp4&
                          &0*tmp8*tmp84*tmp98 + 4*logN2L2*Pi*tmp177*tmp2*tmp24*tmp8*tmp80*tmp84*(-2*(tmp105&
                          & + tmp172 - (2*tmp173)/tmp177)*tmp3 + tmp311 + tmp191*tmp318*tmp413 + (tmp173*tm&
                          &p334 + tmp176*tmp49 + tmp43*(tmp37 + tmp51))/tmp177 + 4*(tmp1 + tmp119 + tmp191)&
                          &*tmp58 + (tmp170 + tmp171 + tmp368/tmp17 + tmp374 + 28*tmp49)*tmp74 + (-((tmp119&
                          &*tmp173 + tmp176/tmp17)/tmp177) - 32*tmp58 + (tmp377*tmp71)/tmp17 + (tmp119 + tm&
                          &p194 + tmp377)*tmp94)/tmp395)*tmp98 - 16*C0tP2P2NPN*Pi*tmp17*tmp21*tmp22*tmp24*t&
                          &mp8*((1/tmp17 - 8/tmp177)*tmp3 + tmp398 - (tmp171 + tmp320 + tmp438)/tmp177 + (2&
                          &*(tmp105 + tmp119/tmp177 - tmp49))/tmp395 + (11/tmp17 + tmp198)*tmp5 + tmp99/tmp&
                          &17) + 16*C0tP2P2NPL*Pi*tmp17*tmp2*tmp24*tmp8*tmp84*(16*tmp109*tmp484 + 4*(tmp366&
                          & + tmp377*tmp488 + (-10 + tmp500)/tmp17)*tmp58 + tmp5*(tmp101 + (tmp102 + tmp1*t&
                          &mp112)*tmp198 - tmp172*tmp361 + (48 - 28*kappa)*tmp43 - 16*tmp46*tmp79) + (2*(32&
                          &*tmp2*tmp58 + (tmp102*tmp377 + 8*tmp2*tmp413 + tmp498 + ss*tmp313*tmp79)/tmp177 &
                          &- 8*tmp5*(tmp1 + tmp102 + tmp45*tmp79) + kappa*tmp413*tmp85))/tmp395 + (tmp100*t&
                          &mp320 + (tmp369*(tmp27 + tmp87))/tmp17 + tmp334*(tmp2*tmp289 + tmp87))/tmp177 - &
                          &tmp3*(tmp85 + tmp9)*tmp98 + (tmp96*tmp99)/tmp253) + C0tP2P2LP0*Pi*tmp17*tmp24*tm&
                          &p435*tmp53*((-4*f1p*tmp2*(tmp103 + tmp190 + tmp29/tmp253 + tmp356 + tmp358 - (2*&
                          &(tmp344 + tmp359))/tmp177 + (tmp291 + tmp308 + tmp359 + tmp372/tmp177)/tmp395 - &
                          &2*(1/tmp17 + tmp360)*tmp5 + tmp43*tmp51))/tmp177 + (f2p*tmp28*(tmp105 + tmp309 +&
                          & tmp351 + (24/tmp177 - 2*tmp364)/tmp395 + tmp55 - (2*(tmp191 + tmp363 + tmp60))/&
                          &tmp177))/tmp17 - 2*tmp2*(((tmp101 - (8*tmp15)/tmp177)*tmp3)/tmp253 + tmp214*(1/t&
                          &mp17 + tmp172 + tmp360 + tmp403) + (tmp362 + (tmp361*tmp379)/tmp17 + 16*tmp12*tm&
                          &p413 + tmp15*tmp440)*tmp5 + (2*(tmp105*(tmp15*tmp19 + tmp379) + (2*(-(tmp172*(2 &
                          &+ tmp269)) + tmp362 + (tmp15*tmp60)/tmp253))/tmp177 + tmp303*tmp85))/tmp395 + ((&
                          &tmp15*tmp191 + tmp26)*tmp320 - (8*tmp15*tmp46)/tmp253 + (tmp300 + 3*tmp362 - 8*t&
                          &mp15*tmp43 + tmp403*tmp87)/tmp17)/tmp177 + (tmp101*tmp99)/tmp253))

  END FUNCTION MP2MPL_MP_FIN_D11D21

  FUNCTION MP2MPL_MP_FIN_D12D21(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_FIN_d12d21
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206, tmp207, tmp208, tmp209, tmp210
  real tmp211, tmp212, tmp213, tmp214, tmp215
  real tmp216, tmp217, tmp218, tmp219, tmp220
  real tmp221, tmp222, tmp223, tmp224, tmp225
  real tmp226, tmp227, tmp228, tmp229, tmp230
  real tmp231, tmp232, tmp233, tmp234, tmp235
  real tmp236, tmp237, tmp238, tmp239, tmp240
  real tmp241, tmp242, tmp243, tmp244, tmp245
  real tmp246, tmp247, tmp248, tmp249, tmp250
  real tmp251, tmp252, tmp253, tmp254, tmp255
  real tmp256, tmp257, tmp258, tmp259, tmp260
  real tmp261, tmp262, tmp263, tmp264, tmp265
  real tmp266, tmp267, tmp268, tmp269, tmp270
  real tmp271, tmp272, tmp273, tmp274, tmp275
  real tmp276, tmp277, tmp278, tmp279, tmp280
  real tmp281, tmp282, tmp283, tmp284, tmp285
  real tmp286, tmp287, tmp288, tmp289, tmp290
  real tmp291, tmp292, tmp293, tmp294, tmp295
  real tmp296, tmp297, tmp298, tmp299, tmp300
  real tmp301, tmp302, tmp303, tmp304, tmp305
  real tmp306, tmp307, tmp308, tmp309, tmp310
  real tmp311, tmp312, tmp313, tmp314, tmp315
  real tmp316, tmp317, tmp318, tmp319, tmp320
  real tmp321, tmp322, tmp323, tmp324, tmp325
  real tmp326, tmp327, tmp328, tmp329, tmp330
  real tmp331, tmp332, tmp333, tmp334, tmp335
  real tmp336, tmp337, tmp338, tmp339, tmp340
  real tmp341, tmp342, tmp343, tmp344, tmp345
  real tmp346, tmp347, tmp348, tmp349, tmp350
  real tmp351, tmp352, tmp353, tmp354, tmp355
  real tmp356, tmp357, tmp358, tmp359, tmp360
  real tmp361, tmp362, tmp363, tmp364, tmp365
  real tmp366, tmp367, tmp368, tmp369, tmp370
  real tmp371, tmp372, tmp373, tmp374, tmp375
  real tmp376, tmp377, tmp378, tmp379, tmp380
  real tmp381, tmp382, tmp383, tmp384, tmp385
  real tmp386, tmp387, tmp388, tmp389, tmp390
  real tmp391, tmp392, tmp393, tmp394, tmp395
  real tmp396, tmp397, tmp398, tmp399, tmp400
  real tmp401, tmp402, tmp403, tmp404, tmp405
  real tmp406, tmp407, tmp408, tmp409, tmp410
  real tmp411, tmp412, tmp413, tmp414, tmp415
  real tmp416, tmp417, tmp418, tmp419, tmp420
  real tmp421, tmp422, tmp423, tmp424, tmp425
  real tmp426, tmp427, tmp428, tmp429, tmp430
  real tmp431, tmp432, tmp433, tmp434, tmp435
  real tmp436, tmp437, tmp438, tmp439, tmp440
  real tmp441, tmp442, tmp443, tmp444, tmp445
  real tmp446, tmp447, tmp448, tmp449, tmp450
  real tmp451, tmp452, tmp453, tmp454, tmp455
  real tmp456, tmp457, tmp458, tmp459, tmp460
  real tmp461, tmp462, tmp463, tmp464, tmp465
  real tmp466, tmp467, tmp468, tmp469, tmp470
  real tmp471, tmp472, tmp473, tmp474, tmp475
  real tmp476, tmp477, tmp478, tmp479, tmp480
  real tmp481, tmp482, tmp483, tmp484, tmp485
  real tmp486, tmp487, tmp488, tmp489, tmp490
  real tmp491, tmp492, tmp493, tmp494, tmp495
  real tmp496, tmp497, tmp498, tmp499, tmp500
  real tmp501, tmp502, tmp503, tmp504, tmp505
  real tmp506, tmp507, tmp508, tmp509, tmp510
  real tmp511, tmp512, tmp513, tmp514, tmp515
  real tmp516, tmp517, tmp518, tmp519, tmp520
  real tmp521, tmp522, tmp523, tmp524, tmp525
  real tmp526, tmp527, tmp528, tmp529, tmp530
  real tmp531, tmp532, tmp533, tmp534, tmp535
  real tmp536, tmp537, tmp538, tmp539, tmp540
  real tmp541, tmp542, tmp543, tmp544, tmp545
  real tmp546, tmp547, tmp548, tmp549, tmp550
  real tmp551, tmp552, tmp553, tmp554, tmp555
  real tmp556, tmp557, tmp558, tmp559, tmp560
  real tmp561, tmp562, tmp563, tmp564, tmp565
  real tmp566, tmp567, tmp568, tmp569, tmp570
  real tmp571, tmp572, tmp573, tmp574, tmp575
  real tmp576, tmp577, tmp578, tmp579, tmp580
  real tmp581, tmp582, tmp583, tmp584, tmp585
  real tmp586, tmp587, tmp588, tmp589, tmp590
  real tmp591, tmp592, tmp593, tmp594, tmp595
  real tmp596, tmp597, tmp598, tmp599, tmp600
  real tmp601, tmp602, tmp603, tmp604, tmp605
  real tmp606, tmp607, tmp608, tmp609, tmp610
  real tmp611, tmp612, tmp613, tmp614, tmp615
  real tmp616, tmp617, tmp618, tmp619, tmp620
  real tmp621, tmp622, tmp623, tmp624, tmp625
  real tmp626, tmp627, tmp628, tmp629, tmp630
  real tmp631, tmp632, tmp633, tmp634, tmp635
  real tmp636, tmp637, tmp638, tmp639, tmp640
  real tmp641, tmp642, tmp643, tmp644, tmp645
  real tmp646, tmp647, tmp648, tmp649, tmp650
  real tmp651, tmp652, tmp653, tmp654, tmp655
  real tmp656, tmp657, tmp658, tmp659, tmp660
  real tmp661, tmp662, tmp663, tmp664, tmp665
  real tmp666, tmp667, tmp668, tmp669, tmp670
  real tmp671, tmp672, tmp673, tmp674, tmp675
  real tmp676, tmp677, tmp678, tmp679, tmp680
  real tmp681, tmp682, tmp683, tmp684, tmp685
  real tmp686, tmp687, tmp688, tmp689, tmp690
  real tmp691, tmp692, tmp693, tmp694, tmp695
  real tmp696, tmp697, tmp698, tmp699, tmp700
  real tmp701, tmp702, tmp703, tmp704, tmp705
  real tmp706, tmp707, tmp708, tmp709, tmp710
  real tmp711, tmp712, tmp713, tmp714, tmp715
  real tmp716, tmp717, tmp718, tmp719, tmp720
  real tmp721, tmp722, tmp723, tmp724, tmp725
  real tmp726, tmp727, tmp728, tmp729, tmp730
  real tmp731, tmp732, tmp733, tmp734, tmp735
  real tmp736, tmp737, tmp738, tmp739, tmp740
  real tmp741, tmp742, tmp743, tmp744, tmp745
  real tmp746, tmp747, tmp748, tmp749, tmp750
  real tmp751, tmp752, tmp753, tmp754, tmp755
  real tmp756, tmp757, tmp758, tmp759, tmp760
  real tmp761, tmp762, tmp763, tmp764, tmp765
  real tmp766, tmp767, tmp768, tmp769, tmp770
  real tmp771, tmp772, tmp773, tmp774, tmp775
  real tmp776, tmp777, tmp778, tmp779, tmp780
  real tmp781, tmp782, tmp783, tmp784, tmp785
  real tmp786, tmp787, tmp788, tmp789, tmp790
  real tmp791, tmp792, tmp793, tmp794, tmp795
  real tmp796, tmp797, tmp798, tmp799, tmp800
  real tmp801, tmp802, tmp803, tmp804, tmp805
  real tmp806, tmp807, tmp808, tmp809, tmp810
  real tmp811, tmp812, tmp813, tmp814, tmp815
  real tmp816, tmp817, tmp818, tmp819, tmp820
  real tmp821, tmp822, tmp823, tmp824, tmp825
  real tmp826, tmp827, tmp828, tmp829, tmp830
  real tmp831, tmp832, tmp833, tmp834, tmp835
  real tmp836, tmp837, tmp838, tmp839, tmp840
  real tmp841, tmp842, tmp843, tmp844, tmp845
  real tmp846, tmp847, tmp848, tmp849, tmp850
  real tmp851, tmp852, tmp853, tmp854, tmp855
  real tmp856, tmp857, tmp858, tmp859, tmp860
  real tmp861, tmp862, tmp863, tmp864, tmp865
  real tmp866, tmp867, tmp868, tmp869, tmp870
  real tmp871, tmp872, tmp873, tmp874, tmp875
  real tmp876, tmp877, tmp878, tmp879, tmp880
  real tmp881, tmp882, tmp883, tmp884, tmp885
  real tmp886, tmp887, tmp888, tmp889, tmp890
  real tmp891, tmp892, tmp893, tmp894, tmp895
  real tmp896, tmp897, tmp898, tmp899, tmp900
  real tmp901, tmp902, tmp903, tmp904, tmp905
  real tmp906, tmp907, tmp908, tmp909, tmp910
  real tmp911, tmp912, tmp913, tmp914, tmp915
  real tmp916, tmp917, tmp918, tmp919, tmp920
  real tmp921, tmp922, tmp923, tmp924, tmp925
  real tmp926, tmp927, tmp928, tmp929, tmp930
  real tmp931, tmp932, tmp933, tmp934, tmp935
  real tmp936, tmp937, tmp938, tmp939, tmp940
  real tmp941, tmp942, tmp943, tmp944, tmp945
  real tmp946, tmp947, tmp948, tmp949, tmp950
  real tmp951, tmp952, tmp953, tmp954, tmp955
  real tmp956, tmp957, tmp958, tmp959, tmp960
  real tmp961, tmp962, tmp963, tmp964, tmp965
  real tmp966, tmp967, tmp968, tmp969, tmp970
  real tmp971, tmp972, tmp973, tmp974, tmp975
  real tmp976, tmp977, tmp978, tmp979, tmp980
  real tmp981, tmp982, tmp983, tmp984, tmp985
  real tmp986, tmp987, tmp988, tmp989, tmp990
  real tmp991, tmp992, tmp993, tmp994, tmp995
  real tmp996, tmp997, tmp998, tmp999, tmp1000
  real tmp1001, tmp1002, tmp1003, tmp1004, tmp1005
  real tmp1006, tmp1007, tmp1008, tmp1009, tmp1010
  real tmp1011, tmp1012, tmp1013, tmp1014, tmp1015
  real tmp1016, tmp1017, tmp1018, tmp1019, tmp1020
  real tmp1021, tmp1022, tmp1023, tmp1024, tmp1025
  real tmp1026, tmp1027, tmp1028, tmp1029, tmp1030
  real tmp1031, tmp1032, tmp1033, tmp1034, tmp1035
  real tmp1036, tmp1037, tmp1038, tmp1039, tmp1040
  real tmp1041, tmp1042, tmp1043, tmp1044, tmp1045
  real tmp1046, tmp1047, tmp1048, tmp1049, tmp1050
  real tmp1051, tmp1052, tmp1053, tmp1054, tmp1055
  real tmp1056, tmp1057, tmp1058, tmp1059, tmp1060
  real tmp1061, tmp1062, tmp1063, tmp1064, tmp1065
  real tmp1066, tmp1067, tmp1068, tmp1069, tmp1070
  real tmp1071, tmp1072, tmp1073, tmp1074, tmp1075
  real tmp1076, tmp1077, tmp1078, tmp1079, tmp1080
  real tmp1081, tmp1082, tmp1083, tmp1084, tmp1085
  real tmp1086, tmp1087, tmp1088, tmp1089, tmp1090
  real tmp1091, tmp1092, tmp1093, tmp1094, tmp1095
  real tmp1096, tmp1097, tmp1098, tmp1099, tmp1100
  real tmp1101, tmp1102, tmp1103, tmp1104, tmp1105
  real tmp1106, tmp1107, tmp1108, tmp1109, tmp1110
  real tmp1111, tmp1112, tmp1113, tmp1114, tmp1115
  real tmp1116, tmp1117, tmp1118, tmp1119, tmp1120
  real tmp1121, tmp1122, tmp1123, tmp1124, tmp1125
  real tmp1126, tmp1127, tmp1128, tmp1129, tmp1130
  real tmp1131, tmp1132, tmp1133, tmp1134, tmp1135
  real tmp1136, tmp1137, tmp1138, tmp1139, tmp1140
  real tmp1141, tmp1142, tmp1143, tmp1144, tmp1145
  real tmp1146, tmp1147, tmp1148, tmp1149, tmp1150
  real tmp1151, tmp1152, tmp1153, tmp1154, tmp1155
  real tmp1156, tmp1157, tmp1158, tmp1159, tmp1160
  real tmp1161, tmp1162, tmp1163, tmp1164, tmp1165
  real tmp1166, tmp1167, tmp1168, tmp1169, tmp1170
  real tmp1171, tmp1172, tmp1173, tmp1174, tmp1175
  real tmp1176, tmp1177, tmp1178, tmp1179, tmp1180
  real tmp1181, tmp1182, tmp1183, tmp1184, tmp1185
  real tmp1186, tmp1187, tmp1188, tmp1189, tmp1190
  real tmp1191, tmp1192, tmp1193, tmp1194, tmp1195
  real tmp1196, tmp1197, tmp1198, tmp1199, tmp1200
  real tmp1201, tmp1202, tmp1203, tmp1204, tmp1205
  real tmp1206, tmp1207, tmp1208, tmp1209, tmp1210
  real tmp1211, tmp1212, tmp1213, tmp1214, tmp1215
  real tmp1216, tmp1217, tmp1218, tmp1219, tmp1220
  real tmp1221, tmp1222, tmp1223, tmp1224, tmp1225
  real tmp1226, tmp1227, tmp1228, tmp1229, tmp1230
  real tmp1231, tmp1232, tmp1233, tmp1234, tmp1235
  real tmp1236, tmp1237, tmp1238, tmp1239, tmp1240
  real tmp1241, tmp1242, tmp1243, tmp1244, tmp1245
  real tmp1246, tmp1247, tmp1248, tmp1249, tmp1250
  real tmp1251, tmp1252, tmp1253, tmp1254, tmp1255
  real tmp1256, tmp1257, tmp1258, tmp1259, tmp1260
  real tmp1261, tmp1262, tmp1263, tmp1264, tmp1265
  real tmp1266, tmp1267, tmp1268, tmp1269, tmp1270
  real tmp1271, tmp1272, tmp1273, tmp1274, tmp1275
  real tmp1276, tmp1277, tmp1278, tmp1279, tmp1280
  real tmp1281, tmp1282, tmp1283, tmp1284, tmp1285
  real tmp1286, tmp1287, tmp1288, tmp1289, tmp1290
  real tmp1291, tmp1292, tmp1293, tmp1294, tmp1295
  real tmp1296, tmp1297, tmp1298, tmp1299, tmp1300
  real tmp1301, tmp1302, tmp1303, tmp1304, tmp1305
  real tmp1306, tmp1307, tmp1308, tmp1309, tmp1310
  real tmp1311, tmp1312, tmp1313, tmp1314, tmp1315
  real tmp1316, tmp1317, tmp1318, tmp1319, tmp1320
  real tmp1321, tmp1322, tmp1323, tmp1324, tmp1325
  real tmp1326, tmp1327, tmp1328, tmp1329, tmp1330
  real tmp1331, tmp1332, tmp1333, tmp1334, tmp1335
  real tmp1336, tmp1337, tmp1338, tmp1339, tmp1340
  real tmp1341, tmp1342, tmp1343, tmp1344, tmp1345
  real tmp1346, tmp1347, tmp1348, tmp1349, tmp1350
  real tmp1351, tmp1352, tmp1353, tmp1354, tmp1355
  real tmp1356, tmp1357, tmp1358, tmp1359, tmp1360
  real tmp1361, tmp1362, tmp1363, tmp1364, tmp1365
  real tmp1366, tmp1367, tmp1368, tmp1369, tmp1370
  real tmp1371, tmp1372, tmp1373, tmp1374, tmp1375
  real tmp1376, tmp1377, tmp1378, tmp1379, tmp1380
  real tmp1381, tmp1382, tmp1383, tmp1384, tmp1385
  real tmp1386, tmp1387, tmp1388, tmp1389, tmp1390
  real tmp1391, tmp1392, tmp1393, tmp1394, tmp1395
  real tmp1396, tmp1397, tmp1398, tmp1399, tmp1400
  real tmp1401, tmp1402, tmp1403, tmp1404, tmp1405
  real tmp1406, tmp1407, tmp1408, tmp1409, tmp1410
  real tmp1411, tmp1412, tmp1413, tmp1414, tmp1415
  real tmp1416, tmp1417, tmp1418, tmp1419, tmp1420
  real tmp1421, tmp1422, tmp1423, tmp1424, tmp1425
  real tmp1426, tmp1427, tmp1428, tmp1429, tmp1430
  real tmp1431, tmp1432, tmp1433, tmp1434, tmp1435
  real tmp1436, tmp1437, tmp1438, tmp1439, tmp1440
  real tmp1441, tmp1442, tmp1443, tmp1444, tmp1445
  real tmp1446, tmp1447, tmp1448, tmp1449, tmp1450
  real tmp1451, tmp1452, tmp1453, tmp1454, tmp1455
  real tmp1456, tmp1457, tmp1458, tmp1459, tmp1460
  real tmp1461, tmp1462, tmp1463, tmp1464, tmp1465
  real tmp1466, tmp1467, tmp1468, tmp1469, tmp1470
  real tmp1471, tmp1472, tmp1473, tmp1474, tmp1475
  real tmp1476, tmp1477, tmp1478, tmp1479, tmp1480
  real tmp1481, tmp1482, tmp1483, tmp1484, tmp1485
  real tmp1486, tmp1487, tmp1488, tmp1489, tmp1490
  real tmp1491, tmp1492, tmp1493, tmp1494, tmp1495
  real tmp1496, tmp1497, tmp1498, tmp1499, tmp1500
  real tmp1501, tmp1502, tmp1503, tmp1504, tmp1505
  real tmp1506, tmp1507, tmp1508, tmp1509, tmp1510
  real tmp1511, tmp1512, tmp1513, tmp1514, tmp1515

  tmp1 = sqrt(lambda)**2
  tmp2 = -1 + kappa
  tmp3 = 1 + tmp1
  tmp4 = me2**2
  tmp5 = -2*me2*mm2
  tmp6 = mm2**2
  tmp7 = -tt
  tmp8 = -ss
  tmp9 = me2 + mm2 + tmp7 + tmp8
  tmp10 = 2*me2
  tmp11 = tmp10 + tt
  tmp12 = -4*mm2
  tmp13 = 4*mm2
  tmp14 = -tmp1
  tmp15 = tmp11 + tmp14
  tmp16 = tmp1 + tmp12
  tmp17 = tmp16**(-2)
  tmp18 = tmp13 + tmp7
  tmp19 = 1/tt
  tmp20 = 5*me2
  tmp21 = 2/tmp19
  tmp22 = mm2 + tmp20 + tmp21 + tmp8
  tmp23 = me2 + mm2 + tmp8
  tmp24 = mm2 + tmp8
  tmp25 = sqrt(lambda)**4
  tmp26 = tmp24**2
  tmp27 = mm2 + ss
  tmp28 = -2*me2*tmp27
  tmp29 = tmp26 + tmp28 + tmp4
  tmp30 = 1/tmp29
  tmp31 = ss**2
  tmp32 = -2*ss
  tmp33 = tmp13 + tmp14
  tmp34 = 1/tmp33
  tmp35 = kappa*tmp14
  tmp36 = tmp13 + tmp35
  tmp37 = ss + tmp7
  tmp38 = ss*tmp37
  tmp39 = -2*mm2
  tmp40 = 1/tmp19 + tmp32 + tmp39
  tmp41 = me2*tmp40
  tmp42 = 2*ss
  tmp43 = 1/tmp19 + tmp42
  tmp44 = -(mm2*tmp43)
  tmp45 = tmp38 + tmp4 + tmp41 + tmp44 + tmp6
  tmp46 = kappa*tmp25
  tmp47 = 1/tmp19 + tmp46
  tmp48 = -4/tmp19
  tmp49 = 1/tmp18
  tmp50 = -tmp6
  tmp51 = 2*mm2
  tmp52 = ss/tmp19
  tmp53 = ss + tmp51
  tmp54 = -2*me2*tmp53
  tmp55 = (-3*mm2)/tmp19
  tmp56 = tmp19**(-2)
  tmp57 = tmp31 + tmp4 + tmp50 + tmp52 + tmp54 + tmp55 + tmp56
  tmp58 = tmp2**2
  tmp59 = sqrt(lambda)**8
  tmp60 = -4*ss
  tmp61 = 3*mm2
  tmp62 = me2 + tmp61 + tmp7 + tmp8
  tmp63 = 2*tmp4
  tmp64 = 2*tmp6
  tmp65 = -3/tmp19
  tmp66 = tmp13 + tmp60 + tmp65
  tmp67 = me2*tmp66
  tmp68 = 1/tmp19 + tmp60
  tmp69 = mm2*tmp68
  tmp70 = 3/tmp19
  tmp71 = tmp42 + tmp70
  tmp72 = ss*tmp71
  tmp73 = tmp63 + tmp64 + tmp67 + tmp69 + tmp72
  tmp74 = -2 + kappa
  tmp75 = tmp1*tmp74
  tmp76 = -1 + tmp75
  tmp77 = kappa*tmp1
  tmp78 = tmp12 + tmp77
  tmp79 = tmp13*tmp74
  tmp80 = tmp77 + tmp79
  tmp81 = 1/tmp56
  tmp82 = -tmp46
  tmp83 = 8*tmp1
  tmp84 = 4 + tmp83
  tmp85 = mm2*tmp84
  tmp86 = tmp82 + tmp85
  tmp87 = -(tmp75/tmp19)
  tmp88 = 4*tmp6
  tmp89 = 4*ss
  tmp90 = 2 + tmp1
  tmp91 = tmp20/tmp34
  tmp92 = (-8*tmp90)/tmp19
  tmp93 = tmp1 + tmp89 + tmp92
  tmp94 = -(mm2*tmp93)
  tmp95 = 2 + tmp77
  tmp96 = (-2*tmp95)/tmp19
  tmp97 = ss + tmp96
  tmp98 = tmp1*tmp97
  tmp99 = tmp88 + tmp91 + tmp94 + tmp98
  tmp100 = sqrt(lambda)**6
  tmp101 = -(mm2/tmp19)
  tmp102 = -(tmp80*tmp9)
  tmp103 = f1p*tmp80*tmp9
  tmp104 = tmp77/tmp19
  tmp105 = mm2*tmp48
  tmp106 = tmp104 + tmp105
  tmp107 = f2p*tmp106
  tmp108 = tmp102 + tmp103 + tmp107
  tmp109 = 4*tmp4
  tmp110 = tmp1 + 1/tmp19
  tmp111 = tmp1/tmp19
  tmp112 = tmp4*tmp83
  tmp113 = tmp25*tmp60
  tmp114 = (4*tmp25)/tmp19
  tmp115 = ss*tmp111
  tmp116 = tmp56*tmp8
  tmp117 = 4*tmp25
  tmp118 = -tmp111
  tmp119 = tmp117 + tmp118 + tmp56
  tmp120 = mm2*tmp119
  tmp121 = tmp1 + tmp32 + tmp51
  tmp122 = 4*tmp1*tmp121
  tmp123 = tmp111 + tmp122 + tmp56
  tmp124 = me2*tmp123
  tmp125 = tmp112 + tmp113 + tmp114 + tmp115 + tmp116 + tmp120 + tmp124
  tmp126 = me2*tmp32
  tmp127 = mm2*tmp89
  tmp128 = tmp111*tmp12
  tmp129 = 2*tmp1*tmp56
  tmp130 = -(tmp56*tmp77)
  tmp131 = tmp10 + tmp12 + 1/tmp19
  tmp132 = 8*tmp6
  tmp133 = -tmp52
  tmp134 = tmp31 + tmp52 + tmp56
  tmp135 = mm2**3
  tmp136 = 24*tmp135
  tmp137 = -8*mm2
  tmp138 = tmp137 + 1/tmp19
  tmp139 = tmp138*tmp4
  tmp140 = -16*ss
  tmp141 = 11/tmp19
  tmp142 = tmp140 + tmp141
  tmp143 = tmp142*tmp6
  tmp144 = 8*mm2*ss
  tmp145 = tmp132 + tmp133 + tmp144
  tmp146 = tmp10*tmp145
  tmp147 = tmp134/tmp19
  tmp148 = 8*tmp31
  tmp149 = tmp89/tmp19
  tmp150 = 9*tmp56
  tmp151 = tmp148 + tmp149 + tmp150
  tmp152 = -(mm2*tmp151)
  tmp153 = tmp136 + tmp139 + tmp143 + tmp146 + tmp147 + tmp152
  tmp154 = -4*tmp6
  tmp155 = tmp14 + 1/tmp19
  tmp156 = -4*tmp1*tmp31
  tmp157 = -2*tmp115
  tmp158 = tmp21*tmp31
  tmp159 = -3*tmp1*tmp56
  tmp160 = tmp42*tmp56
  tmp161 = tmp19**(-3)
  tmp162 = 2*tmp161
  tmp163 = -6*tmp1
  tmp164 = tmp163 + 1/tmp19
  tmp165 = -2*tmp164*tmp6
  tmp166 = -2*tmp1
  tmp167 = tmp166 + 1/tmp19
  tmp168 = tmp167*tmp63
  tmp169 = -8*ss*tmp1
  tmp170 = 6*tmp111
  tmp171 = -6*tmp56
  tmp172 = tmp169 + tmp170 + tmp171
  tmp173 = mm2*tmp172
  tmp174 = ss*tmp83
  tmp175 = 2*tmp111
  tmp176 = tmp60/tmp19
  tmp177 = tmp137*tmp155
  tmp178 = tmp174 + tmp175 + tmp176 + tmp177
  tmp179 = me2*tmp178
  tmp180 = tmp156 + tmp157 + tmp158 + tmp159 + tmp160 + tmp162 + tmp165 + tmp168 +&
                          & tmp173 + tmp179
  tmp181 = me2**3
  tmp182 = ss**3
  tmp183 = -8*ss
  tmp184 = 4*tmp135
  tmp185 = 16*ss
  tmp186 = mm2**4
  tmp187 = me2**4
  tmp188 = ss**4
  tmp189 = me2**5
  tmp190 = 8*tmp135
  tmp191 = -9/tmp19
  tmp192 = 14*ss
  tmp193 = -64*tmp135
  tmp194 = 9/tmp19
  tmp195 = 17/tmp19
  tmp196 = tmp187*tmp65
  tmp197 = tmp31*tmp65
  tmp198 = 3*ss
  tmp199 = -2/tmp19
  tmp200 = tmp1 + tmp7
  tmp201 = tmp200*tmp77
  tmp202 = tmp74/tmp19
  tmp203 = tmp200*tmp46
  tmp204 = tmp110 + tmp175
  tmp205 = tmp13*tmp204
  tmp206 = tmp203 + tmp205
  tmp207 = tmp10*tmp206
  tmp208 = -tmp25
  tmp209 = tmp175 + 1/tmp19 + tmp208
  tmp210 = tmp13*tmp209
  tmp211 = tmp203 + tmp210
  tmp212 = tmp211/tmp19
  tmp213 = tmp207 + tmp212
  tmp214 = tmp200*tmp56
  tmp215 = 4*me2*tmp1
  tmp216 = tmp111 + tmp215 + tmp56
  tmp217 = tmp13*tmp216
  tmp218 = tmp214 + tmp217
  tmp219 = -1 + tmp1
  tmp220 = 1/tmp155
  tmp221 = tmp14*tmp2*tmp218*tmp23
  tmp222 = f1p*tmp1*tmp2*tmp218*tmp23
  tmp223 = tmp1*tmp2
  tmp224 = tmp12 + 1/tmp19 + tmp223
  tmp225 = -4*tmp1*tmp224*tmp4
  tmp226 = -2*tmp46
  tmp227 = tmp118 + tmp155 + tmp226 + tmp25
  tmp228 = tmp227/tmp19
  tmp229 = tmp219/tmp19
  tmp230 = tmp1 + tmp229 + tmp25
  tmp231 = tmp13*tmp230
  tmp232 = tmp228 + tmp231
  tmp233 = tmp232*tmp24
  tmp234 = tmp1*tmp132
  tmp235 = tmp2*tmp25*tmp42
  tmp236 = 1 + tmp32 + tmp77
  tmp237 = tmp118*tmp236
  tmp238 = -3 + kappa
  tmp239 = tmp1*tmp238
  tmp240 = -2 + tmp239 + tmp89
  tmp241 = tmp1*tmp240
  tmp242 = tmp90/tmp19
  tmp243 = tmp241 + tmp242
  tmp244 = tmp243*tmp39
  tmp245 = tmp234 + tmp235 + tmp237 + tmp244 + tmp56
  tmp246 = tmp10*tmp245
  tmp247 = tmp225 + tmp233 + tmp246
  tmp248 = (f2p*tmp247)/tmp19
  tmp249 = tmp221 + tmp222 + tmp248
  tmp250 = 32*mm2*tmp4
  tmp251 = mm2*tmp176
  tmp252 = -2 + tmp239
  tmp253 = 2*tmp77
  tmp254 = -6 + kappa
  tmp255 = tmp13 + tmp200
  tmp256 = 8*mm2
  tmp257 = 8/tmp19
  tmp258 = tmp1 + tmp202
  tmp259 = -2*tmp223
  tmp260 = 2*kappa
  tmp261 = -1 + tmp260
  tmp262 = tmp2/tmp19
  tmp263 = 5*kappa
  tmp264 = -3 + tmp263
  tmp265 = 2*tmp1*tmp264
  tmp266 = 11*kappa
  tmp267 = -10 + tmp266
  tmp268 = tmp267/tmp19
  tmp269 = tmp185 + tmp265 + tmp268
  tmp270 = tmp149*tmp258
  tmp271 = tmp202 + tmp259
  tmp272 = 4*tmp271*tmp31
  tmp273 = -3*tmp1
  tmp274 = 4/tmp19
  tmp275 = tmp273 + tmp274
  tmp276 = tmp202 + tmp35
  tmp277 = (tmp275*tmp276)/tmp19
  tmp278 = tmp270 + tmp272 + tmp277
  tmp279 = 16*tmp31*tmp74
  tmp280 = 12 + kappa
  tmp281 = tmp111*tmp280
  tmp282 = 7*kappa
  tmp283 = -12 + tmp282
  tmp284 = 4*tmp283*tmp56
  tmp285 = tmp1*tmp261
  tmp286 = tmp262 + tmp285
  tmp287 = tmp185*tmp286
  tmp288 = tmp279 + tmp281 + tmp284 + tmp287 + tmp82
  tmp289 = 32*tmp135*tmp2
  tmp290 = ss*tmp201
  tmp291 = tmp32*tmp74
  tmp292 = tmp1 + tmp262 + tmp291
  tmp293 = -8*tmp292*tmp6
  tmp294 = tmp25*tmp260
  tmp295 = 8*ss*tmp223
  tmp296 = 2*tmp1*tmp262
  tmp297 = tmp202*tmp60
  tmp298 = tmp294 + tmp295 + tmp296 + tmp297
  tmp299 = mm2*tmp298
  tmp300 = tmp289 + tmp290 + tmp293 + tmp299
  tmp301 = -mm2
  tmp302 = 6*ss
  tmp303 = 20*ss
  tmp304 = 2*tmp52
  tmp305 = 2*tmp135
  tmp306 = 2*tmp31
  tmp307 = 2*tmp187
  tmp308 = tmp42 + tmp61
  tmp309 = -4*tmp181*tmp308
  tmp310 = 16*tmp6
  tmp311 = 12*tmp31
  tmp312 = tmp199 + tmp303
  tmp313 = mm2*tmp312
  tmp314 = tmp304 + tmp310 + tmp311 + tmp313 + tmp56
  tmp315 = tmp314*tmp4
  tmp316 = tmp70 + tmp89
  tmp317 = -2*tmp316*tmp6
  tmp318 = tmp31 + tmp56
  tmp319 = tmp318*tmp51
  tmp320 = 4*tmp31
  tmp321 = tmp304 + tmp320 + tmp56
  tmp322 = ss*tmp321
  tmp323 = tmp305 + tmp317 + tmp319 + tmp322
  tmp324 = -2*me2*tmp323
  tmp325 = -5/tmp19
  tmp326 = ss + tmp325
  tmp327 = -2*tmp326*tmp6
  tmp328 = tmp304 + tmp306 + tmp56
  tmp329 = ss*tmp328
  tmp330 = 3*tmp56
  tmp331 = tmp149 + tmp306 + tmp330
  tmp332 = tmp301*tmp331
  tmp333 = tmp305 + tmp327 + tmp329 + tmp332
  tmp334 = -(tmp24*tmp333)
  tmp335 = tmp307 + tmp309 + tmp315 + tmp324 + tmp334
  tmp336 = tmp1*tmp250
  tmp337 = -32*me2*mm2*ss*tmp1
  tmp338 = me2*mm2*tmp1*tmp274
  tmp339 = mm2*ss*tmp1*tmp274
  tmp340 = me2*tmp130
  tmp341 = tmp301*tmp56*tmp77
  tmp342 = ss*tmp56*tmp77
  tmp343 = 2*tmp56
  tmp344 = 3*tmp111
  tmp345 = -3*kappa
  tmp346 = 2 + tmp345
  tmp347 = tmp1*tmp346
  tmp348 = tmp261/tmp19
  tmp349 = 3*tmp2*tmp25
  tmp350 = tmp56*tmp74
  tmp351 = 8*ss
  tmp352 = 1 + tmp260
  tmp353 = sqrt(lambda)**3
  tmp354 = -(sqrt(lambda)/tmp19)
  tmp355 = tmp353 + tmp354
  tmp356 = tmp355**2
  tmp357 = -2*kappa
  tmp358 = 1 + tmp357
  tmp359 = -(kappa*me2*tmp100)
  tmp360 = kappa*tmp100*tmp301
  tmp361 = me2*mm2*tmp117
  tmp362 = -12*me2*mm2*tmp46
  tmp363 = -64*mm2*tmp4*tmp77
  tmp364 = 12*tmp25*tmp6
  tmp365 = -12*tmp46*tmp6
  tmp366 = 128*me2*tmp1*tmp6
  tmp367 = -112*me2*tmp6*tmp77
  tmp368 = 128*tmp4*tmp6
  tmp369 = 32*tmp1*tmp135
  tmp370 = -48*tmp135*tmp77
  tmp371 = 192*me2*tmp135
  tmp372 = kappa*me2*tmp193
  tmp373 = 192*tmp186
  tmp374 = -64*kappa*tmp186
  tmp375 = kappa*ss*tmp100
  tmp376 = -12*mm2*ss*tmp25
  tmp377 = 12*mm2*ss*tmp46
  tmp378 = 64*me2*mm2*ss*tmp77
  tmp379 = -32*ss*tmp1*tmp6
  tmp380 = 48*ss*tmp6*tmp77
  tmp381 = -128*me2*ss*tmp6
  tmp382 = -192*ss*tmp135
  tmp383 = 64*kappa*ss*tmp135
  tmp384 = (me2*tmp294)/tmp19
  tmp385 = (-14*mm2*tmp46)/tmp19
  tmp386 = -24*kappa*me2*mm2*tmp111
  tmp387 = 124*tmp111*tmp6
  tmp388 = -72*kappa*tmp111*tmp6
  tmp389 = (me2*tmp310)/tmp19
  tmp390 = (32*kappa*me2*tmp6)/tmp19
  tmp391 = (-16*tmp135)/tmp19
  tmp392 = (32*kappa*tmp135)/tmp19
  tmp393 = tmp25*tmp357*tmp52
  tmp394 = kappa*mm2*tmp111*tmp351
  tmp395 = tmp310*tmp52
  tmp396 = -32*kappa*tmp52*tmp6
  tmp397 = me2*tmp256*tmp56
  tmp398 = kappa*me2*tmp12*tmp56
  tmp399 = tmp132*tmp56
  tmp400 = kappa*tmp154*tmp56
  tmp401 = mm2*tmp183*tmp56
  tmp402 = kappa*tmp127*tmp56
  tmp403 = 16*tmp135
  tmp404 = 2*tmp1
  tmp405 = tmp404 + tmp7 + tmp89
  tmp406 = tmp154*tmp405
  tmp407 = tmp257*tmp4
  tmp408 = tmp111 + tmp25 + tmp343
  tmp409 = tmp408*tmp8
  tmp410 = tmp1*tmp195
  tmp411 = tmp174 + tmp176 + tmp25 + tmp343 + tmp410
  tmp412 = mm2*tmp411
  tmp413 = tmp183/tmp19
  tmp414 = 5/tmp19
  tmp415 = tmp166 + tmp414
  tmp416 = tmp13*tmp415
  tmp417 = tmp25 + tmp310 + tmp344 + tmp413 + tmp416
  tmp418 = me2*tmp417
  tmp419 = tmp403 + tmp406 + tmp407 + tmp409 + tmp412 + tmp418
  tmp420 = f2p*tmp36*tmp419
  tmp421 = 64*tmp186*tmp238
  tmp422 = mm2*tmp1*tmp358
  tmp423 = tmp422 + tmp88
  tmp424 = -32*tmp4*tmp423
  tmp425 = tmp238*tmp89
  tmp426 = tmp347 + tmp348 + tmp425
  tmp427 = -16*tmp135*tmp426
  tmp428 = kappa*tmp356*tmp8
  tmp429 = 18*kappa
  tmp430 = -31 + tmp429
  tmp431 = tmp111*tmp430
  tmp432 = tmp347 + tmp348
  tmp433 = tmp432*tmp89
  tmp434 = tmp349 + tmp350 + tmp431 + tmp433
  tmp435 = tmp434*tmp88
  tmp436 = 14*tmp111
  tmp437 = tmp25 + tmp436 + tmp56
  tmp438 = tmp437*tmp77
  tmp439 = tmp111*tmp352
  tmp440 = tmp349 + tmp350 + tmp439
  tmp441 = tmp440*tmp60
  tmp442 = tmp438 + tmp441
  tmp443 = mm2*tmp442
  tmp444 = 64*tmp135*tmp238
  tmp445 = -8 + tmp282
  tmp446 = tmp1*tmp445
  tmp447 = -(tmp352/tmp19)
  tmp448 = tmp351 + tmp446 + tmp447
  tmp449 = tmp310*tmp448
  tmp450 = kappa*tmp356
  tmp451 = 3*kappa
  tmp452 = -1 + tmp451
  tmp453 = tmp25*tmp452
  tmp454 = tmp1*tmp351*tmp358
  tmp455 = 6*kappa
  tmp456 = -1 + tmp455
  tmp457 = tmp111*tmp456
  tmp458 = tmp350 + tmp453 + tmp454 + tmp457
  tmp459 = tmp13*tmp458
  tmp460 = tmp444 + tmp449 + tmp450 + tmp459
  tmp461 = me2*tmp460
  tmp462 = tmp421 + tmp424 + tmp427 + tmp428 + tmp435 + tmp443 + tmp461
  tmp463 = f1p*tmp462
  tmp464 = tmp336 + tmp337 + tmp338 + tmp339 + tmp340 + tmp341 + tmp342 + tmp359 +&
                          & tmp360 + tmp361 + tmp362 + tmp363 + tmp364 + tmp365 + tmp366 + tmp367 + tmp368 &
                          &+ tmp369 + tmp370 + tmp371 + tmp372 + tmp373 + tmp374 + tmp375 + tmp376 + tmp377&
                          & + tmp378 + tmp379 + tmp380 + tmp381 + tmp382 + tmp383 + tmp384 + tmp385 + tmp38&
                          &6 + tmp387 + tmp388 + tmp389 + tmp390 + tmp391 + tmp392 + tmp393 + tmp394 + tmp3&
                          &95 + tmp396 + tmp397 + tmp398 + tmp399 + tmp400 + tmp401 + tmp402 + tmp420 + tmp&
                          &463
  tmp465 = 2*tmp25
  tmp466 = tmp220**(-2)
  tmp467 = kappa*tmp208*tmp466
  tmp468 = tmp1 + tmp25
  tmp469 = tmp3*tmp336
  tmp470 = 3*tmp100
  tmp471 = tmp208/tmp19
  tmp472 = tmp111 + tmp129 + tmp465 + tmp470 + tmp471 + tmp56
  tmp473 = tmp13*tmp472
  tmp474 = tmp467 + tmp473
  tmp475 = tmp24*tmp474
  tmp476 = 32*tmp468*tmp6
  tmp477 = tmp183*tmp468
  tmp478 = 3 + tmp1
  tmp479 = tmp111*tmp478
  tmp480 = 1 + tmp404
  tmp481 = tmp480*tmp56
  tmp482 = tmp100 + tmp477 + tmp479 + tmp481
  tmp483 = tmp13*tmp482
  tmp484 = tmp467 + tmp476 + tmp483
  tmp485 = me2*tmp484
  tmp486 = tmp469 + tmp475 + tmp485
  tmp487 = ss*tmp166
  tmp488 = 5*tmp1
  tmp489 = tmp273/tmp19
  tmp490 = -24*mm2
  tmp491 = -5*tmp1
  tmp492 = kappa*tmp100
  tmp493 = 1 + kappa
  tmp494 = tmp25*tmp493
  tmp495 = -6*tmp25
  tmp496 = tmp175 + 1/tmp19 + tmp491 + tmp495
  tmp497 = tmp185*tmp468
  tmp498 = tmp280*tmp471
  tmp499 = 12*tmp481
  tmp500 = tmp492 + tmp497 + tmp498 + tmp499
  tmp501 = ss*tmp203
  tmp502 = tmp132*tmp209
  tmp503 = tmp1 + tmp494
  tmp504 = -(tmp503/tmp19)
  tmp505 = (tmp42*tmp480)/tmp220
  tmp506 = tmp492 + tmp504 + tmp505
  tmp507 = tmp506*tmp51
  tmp508 = tmp501 + tmp502 + tmp507
  tmp509 = 1 + tmp77
  tmp510 = 3*tmp1
  tmp511 = tmp14*tmp509
  tmp512 = tmp1*tmp274
  tmp513 = tmp1 + tmp46
  tmp514 = ss*tmp25*tmp451
  tmp515 = tmp167 + tmp46
  tmp516 = ss*tmp515
  tmp517 = tmp175 + tmp516
  tmp518 = -4*tmp111
  tmp519 = -4*tmp1
  tmp520 = tmp47 + tmp519
  tmp521 = -8*tmp1
  tmp522 = tmp25*tmp451
  tmp523 = tmp181*tmp520
  tmp524 = -10*ss*tmp1
  tmp525 = tmp198/tmp19
  tmp526 = tmp164 + tmp46
  tmp527 = mm2*tmp526
  tmp528 = tmp512 + tmp514 + tmp524 + tmp525 + tmp527
  tmp529 = -(tmp4*tmp528)
  tmp530 = tmp515*tmp6
  tmp531 = tmp39*tmp517
  tmp532 = ss*tmp517
  tmp533 = tmp530 + tmp531 + tmp532
  tmp534 = tmp24*tmp533
  tmp535 = tmp47*tmp6
  tmp536 = ss*tmp520
  tmp537 = tmp518 + tmp536
  tmp538 = tmp51*tmp537
  tmp539 = tmp521 + tmp522 + tmp70
  tmp540 = ss*tmp539
  tmp541 = tmp170 + tmp540
  tmp542 = tmp541*tmp8
  tmp543 = tmp535 + tmp538 + tmp542
  tmp544 = -(me2*tmp543)
  tmp545 = tmp523 + tmp529 + tmp534 + tmp544
  tmp546 = tmp30**2
  tmp547 = tmp1*tmp301
  tmp548 = tmp1 + tmp21
  tmp549 = mm2**5
  tmp550 = -32/tmp19
  tmp551 = kappa/tmp19
  tmp552 = -7*tmp25
  tmp553 = 96*tmp31
  tmp554 = -6/tmp19
  tmp555 = 7*tmp25
  tmp556 = 6/tmp19
  tmp557 = kappa*tmp555*tmp56
  tmp558 = 10/tmp19
  tmp559 = me2**6
  tmp560 = tmp547 + tmp88
  tmp561 = 16*tmp559*tmp560
  tmp562 = 32*mm2
  tmp563 = 32*ss
  tmp564 = tmp48 + tmp488 + tmp562 + tmp563
  tmp565 = (tmp189*tmp39*tmp564)/tmp34
  tmp566 = tmp24**3
  tmp567 = tmp25*tmp357*tmp56
  tmp568 = tmp548*tmp88
  tmp569 = tmp1 + tmp89
  tmp570 = tmp301*tmp548*tmp569
  tmp571 = tmp175 + tmp25
  tmp572 = ss*tmp571
  tmp573 = tmp567 + tmp568 + tmp570 + tmp572
  tmp574 = tmp547*tmp566*tmp573
  tmp575 = mm2**6
  tmp576 = 32*tmp575
  tmp577 = tmp31*tmp330*tmp492
  tmp578 = tmp275 + tmp351
  tmp579 = -16*tmp549*tmp578
  tmp580 = -16*tmp111
  tmp581 = tmp257 + tmp273
  tmp582 = tmp581*tmp89
  tmp583 = tmp552 + tmp553 + tmp580 + tmp582
  tmp584 = 2*tmp186*tmp583
  tmp585 = -8*tmp182
  tmp586 = -7*tmp1
  tmp587 = tmp257 + tmp586
  tmp588 = tmp306*tmp587
  tmp589 = 1 + tmp551
  tmp590 = 12*tmp115*tmp589
  tmp591 = tmp557 + tmp585 + tmp588 + tmp590
  tmp592 = mm2*ss*tmp1*tmp591
  tmp593 = 16*tmp188
  tmp594 = 44*tmp1
  tmp595 = tmp550 + tmp594
  tmp596 = tmp182*tmp595
  tmp597 = -(tmp492*tmp56)
  tmp598 = -1 + tmp551
  tmp599 = tmp176*tmp25*tmp598
  tmp600 = tmp1*tmp550
  tmp601 = tmp555 + tmp600
  tmp602 = tmp31*tmp601
  tmp603 = tmp593 + tmp596 + tmp597 + tmp599 + tmp602
  tmp604 = tmp603*tmp64
  tmp605 = 64*tmp182
  tmp606 = 52*tmp1
  tmp607 = tmp550 + tmp606
  tmp608 = tmp31*tmp607
  tmp609 = -3 + tmp551
  tmp610 = (tmp465*tmp609)/tmp19
  tmp611 = 24*tmp111
  tmp612 = tmp552 + tmp611
  tmp613 = ss*tmp612
  tmp614 = tmp605 + tmp608 + tmp610 + tmp613
  tmp615 = -2*tmp135*tmp614
  tmp616 = tmp576 + tmp577 + tmp579 + tmp584 + tmp592 + tmp604 + tmp615
  tmp617 = tmp616*tmp63
  tmp618 = 384*tmp186
  tmp619 = -8/tmp19
  tmp620 = tmp185 + tmp510 + tmp619
  tmp621 = tmp403*tmp620
  tmp622 = tmp343*tmp492
  tmp623 = -4*tmp25
  tmp624 = 10*ss*tmp1
  tmp625 = 48*tmp31
  tmp626 = tmp140/tmp19
  tmp627 = tmp111 + tmp623 + tmp624 + tmp625 + tmp626
  tmp628 = tmp132*tmp627
  tmp629 = -9*tmp1
  tmp630 = tmp257 + tmp629
  tmp631 = tmp60*tmp630
  tmp632 = -8*kappa*tmp56
  tmp633 = tmp1 + tmp554 + tmp632
  tmp634 = tmp1*tmp633
  tmp635 = tmp553 + tmp631 + tmp634
  tmp636 = tmp547*tmp635
  tmp637 = tmp618 + tmp621 + tmp622 + tmp628 + tmp636
  tmp638 = tmp187*tmp637
  tmp639 = tmp31*tmp492*tmp56
  tmp640 = 4*tmp275*tmp549
  tmp641 = 36*ss*tmp1
  tmp642 = 20*tmp111
  tmp643 = -48*tmp52
  tmp644 = tmp555 + tmp641 + tmp642 + tmp643
  tmp645 = tmp186*tmp644
  tmp646 = 8*tmp46*tmp56
  tmp647 = tmp275*tmp320
  tmp648 = 36*tmp111
  tmp649 = tmp555 + tmp648
  tmp650 = ss*tmp649
  tmp651 = tmp470 + tmp646 + tmp647 + tmp650
  tmp652 = ss*tmp50*tmp651
  tmp653 = tmp275*tmp311
  tmp654 = tmp25 + tmp512
  tmp655 = tmp198*tmp654
  tmp656 = -4*kappa*tmp56
  tmp657 = tmp1 + tmp556 + tmp656
  tmp658 = tmp208*tmp657
  tmp659 = tmp653 + tmp655 + tmp658
  tmp660 = tmp135*tmp659
  tmp661 = tmp275*tmp31
  tmp662 = kappa*tmp343
  tmp663 = tmp404 + tmp662 + tmp70
  tmp664 = ss*tmp404*tmp663
  tmp665 = tmp557 + tmp661 + tmp664
  tmp666 = mm2*ss*tmp1*tmp665
  tmp667 = tmp639 + tmp640 + tmp645 + tmp652 + tmp660 + tmp666
  tmp668 = tmp10*tmp24*tmp667
  tmp669 = 128*tmp549
  tmp670 = ss*tmp330*tmp492
  tmp671 = tmp166 + tmp316
  tmp672 = -32*tmp186*tmp671
  tmp673 = 5*tmp25
  tmp674 = 32*tmp31
  tmp675 = tmp21 + tmp273
  tmp676 = tmp351*tmp675
  tmp677 = tmp512 + tmp673 + tmp674 + tmp676
  tmp678 = -4*tmp135*tmp677
  tmp679 = 16*tmp31
  tmp680 = (-12*ss)/tmp220
  tmp681 = kappa*tmp171
  tmp682 = tmp325 + tmp404 + tmp681
  tmp683 = tmp1*tmp682
  tmp684 = tmp679 + tmp680 + tmp683
  tmp685 = mm2*tmp487*tmp684
  tmp686 = 128*tmp182
  tmp687 = -24*tmp115
  tmp688 = tmp519 + tmp70
  tmp689 = -32*tmp31*tmp688
  tmp690 = 4*kappa*tmp56
  tmp691 = tmp14 + tmp558 + tmp690
  tmp692 = tmp25*tmp691
  tmp693 = tmp686 + tmp687 + tmp689 + tmp692
  tmp694 = tmp6*tmp693
  tmp695 = tmp669 + tmp670 + tmp672 + tmp678 + tmp685 + tmp694
  tmp696 = -2*tmp181*tmp695
  tmp697 = tmp561 + tmp565 + tmp574 + tmp617 + tmp638 + tmp668 + tmp696
  tmp698 = tmp1 + tmp257
  tmp699 = tmp1 + tmp274
  tmp700 = -10/tmp19
  tmp701 = 1/me2
  tmp702 = 1/mm2
  tmp703 = 1/tmp19 + tmp89
  tmp704 = tmp404*tmp52
  tmp705 = tmp1*tmp357*tmp56
  tmp706 = 7/tmp19
  tmp707 = 8*kappa*tmp56
  tmp708 = 7*tmp1
  tmp709 = ss*tmp65
  tmp710 = -64*tmp182
  tmp711 = 16*kappa*tmp56
  tmp712 = 1/tmp17
  tmp713 = tmp189*tmp712
  tmp714 = tmp273 + tmp351
  tmp715 = (2*tmp714)/tmp702
  tmp716 = tmp14*tmp703
  tmp717 = tmp310 + tmp715 + tmp716
  tmp718 = -((tmp187*tmp717)/tmp34)
  tmp719 = ss*tmp25
  tmp720 = tmp567 + tmp568 + tmp570 + tmp704 + tmp719
  tmp721 = (tmp1*tmp26*tmp720)/tmp702
  tmp722 = 96*tmp186
  tmp723 = tmp351 + tmp629
  tmp724 = tmp190*tmp723
  tmp725 = -30*ss*tmp1
  tmp726 = 24*tmp31
  tmp727 = tmp491/tmp19
  tmp728 = tmp465 + tmp725 + tmp726 + tmp727
  tmp729 = tmp728*tmp88
  tmp730 = 26*ss*tmp1
  tmp731 = -48*tmp31
  tmp732 = tmp488/tmp19
  tmp733 = -12*tmp52
  tmp734 = tmp1*tmp632
  tmp735 = tmp25 + tmp730 + tmp731 + tmp732 + tmp733 + tmp734
  tmp736 = (tmp1*tmp735)/tmp702
  tmp737 = 6*tmp31
  tmp738 = tmp525 + tmp705 + tmp737
  tmp739 = tmp25*tmp738
  tmp740 = tmp722 + tmp724 + tmp729 + tmp736 + tmp739
  tmp741 = tmp181*tmp740
  tmp742 = 16*tmp575
  tmp743 = -16*tmp549*tmp569
  tmp744 = -9*tmp25
  tmp745 = -24*ss*tmp1
  tmp746 = -28*tmp111
  tmp747 = tmp553 + tmp744 + tmp745 + tmp746
  tmp748 = tmp186*tmp747
  tmp749 = tmp31 + tmp52 + tmp705
  tmp750 = tmp25*tmp31*tmp749
  tmp751 = tmp31*tmp521
  tmp752 = tmp495 + tmp642
  tmp753 = ss*tmp752
  tmp754 = tmp510 + tmp632 + tmp706
  tmp755 = tmp25*tmp754
  tmp756 = tmp710 + tmp751 + tmp753 + tmp755
  tmp757 = tmp135*tmp756
  tmp758 = 8*tmp182
  tmp759 = tmp25*tmp711
  tmp760 = tmp21 + tmp488
  tmp761 = tmp306*tmp760
  tmp762 = tmp70 + tmp707 + tmp708
  tmp763 = ss*tmp1*tmp762
  tmp764 = tmp758 + tmp759 + tmp761 + tmp763
  tmp765 = (ss*tmp14*tmp764)/tmp702
  tmp766 = 56*tmp1*tmp182
  tmp767 = 1/tmp19 + tmp404
  tmp768 = tmp1*tmp311*tmp767
  tmp769 = 4*tmp1
  tmp770 = tmp325 + tmp711 + tmp769
  tmp771 = tmp719*tmp770
  tmp772 = tmp593 + tmp622 + tmp766 + tmp768 + tmp771
  tmp773 = tmp6*tmp772
  tmp774 = tmp742 + tmp743 + tmp748 + tmp750 + tmp757 + tmp765 + tmp773
  tmp775 = tmp774/tmp701
  tmp776 = -64*tmp549
  tmp777 = tmp351 + tmp708
  tmp778 = 8*tmp186*tmp777
  tmp779 = 56*ss*tmp1
  tmp780 = 18*tmp111
  tmp781 = tmp25 + tmp674 + tmp779 + tmp780
  tmp782 = tmp305*tmp781
  tmp783 = -4*tmp31
  tmp784 = kappa*tmp56*tmp769
  tmp785 = tmp709 + tmp783 + tmp784
  tmp786 = tmp719*tmp785
  tmp787 = tmp1*tmp726
  tmp788 = tmp208 + tmp512
  tmp789 = tmp788*tmp89
  tmp790 = tmp191 + tmp273 + tmp711
  tmp791 = tmp25*tmp790
  tmp792 = tmp710 + tmp787 + tmp789 + tmp791
  tmp793 = tmp6*tmp792
  tmp794 = 32*tmp182
  tmp795 = tmp25*tmp662
  tmp796 = tmp491 + tmp556
  tmp797 = tmp306*tmp796
  tmp798 = tmp48 + tmp586 + tmp711
  tmp799 = ss*tmp1*tmp798
  tmp800 = tmp794 + tmp795 + tmp797 + tmp799
  tmp801 = (tmp1*tmp800)/tmp702
  tmp802 = tmp776 + tmp778 + tmp782 + tmp786 + tmp793 + tmp801
  tmp803 = tmp4*tmp802
  tmp804 = tmp713 + tmp718 + tmp721 + tmp741 + tmp775 + tmp803
  tmp805 = ss*tmp1
  tmp806 = tmp31*tmp404
  tmp807 = kappa**2
  tmp808 = sqrt(lambda)**10
  tmp809 = -tmp805
  tmp810 = -tmp719
  tmp811 = 12*tmp52
  tmp812 = 80*tmp1*tmp182
  tmp813 = tmp2*tmp510
  tmp814 = tmp1*tmp700
  tmp815 = tmp1*tmp257
  tmp816 = tmp25*tmp558
  tmp817 = -5*kappa
  tmp818 = 4*kappa
  tmp819 = -(kappa*tmp59)
  tmp820 = tmp702**(-7)
  tmp821 = 96*tmp1*tmp182
  tmp822 = tmp199*tmp25
  tmp823 = 4 + tmp451
  tmp824 = 16/tmp19
  tmp825 = 7*tmp100*tmp56*tmp807
  tmp826 = -5 + kappa
  tmp827 = 4 + tmp223
  tmp828 = 7 + tmp260
  tmp829 = 5 + tmp451
  tmp830 = tmp1*tmp829
  tmp831 = tmp1*tmp826
  tmp832 = tmp14 + tmp199 + tmp690
  tmp833 = tmp702**(-8)
  tmp834 = ss**5
  tmp835 = ss**6
  tmp836 = tmp1*tmp202
  tmp837 = 10*ss
  tmp838 = 5 + tmp166
  tmp839 = tmp838/tmp19
  tmp840 = tmp1*tmp493*tmp837
  tmp841 = tmp166*tmp74
  tmp842 = tmp25*tmp345
  tmp843 = 20*tmp493*tmp805
  tmp844 = 1 + tmp75
  tmp845 = tmp404*tmp74
  tmp846 = 5 + tmp845
  tmp847 = tmp111*tmp846
  tmp848 = -4 + tmp1
  tmp849 = -2*tmp25
  tmp850 = -7 + tmp1
  tmp851 = 24/tmp19
  tmp852 = 4 + tmp345
  tmp853 = tmp111*tmp852
  tmp854 = tmp510*tmp74
  tmp855 = -2 + tmp854
  tmp856 = -40*ss
  tmp857 = tmp488*tmp74
  tmp858 = -6*kappa
  tmp859 = 3*tmp492
  tmp860 = -3*tmp492
  tmp861 = 80*tmp1*tmp31
  tmp862 = tmp25*tmp817
  tmp863 = tmp488*tmp823
  tmp864 = -20*tmp836
  tmp865 = -8 + tmp455
  tmp866 = 4*tmp56*tmp76
  tmp867 = -2 + tmp75
  tmp868 = ss + 1/tmp19
  tmp869 = 16*tmp25
  tmp870 = -8*tmp46
  tmp871 = -48*tmp75
  tmp872 = tmp1*tmp148
  tmp873 = tmp1*tmp823
  tmp874 = -4 + tmp451
  tmp875 = tmp1*tmp874
  tmp876 = -1 + tmp875
  tmp877 = -26*tmp1
  tmp878 = -5 + tmp451
  tmp879 = tmp708*tmp74
  tmp880 = tmp198 + tmp21
  tmp881 = -4*tmp492
  tmp882 = 2 + tmp75
  tmp883 = tmp111*tmp865
  tmp884 = kappa*tmp555
  tmp885 = 2 + tmp875
  tmp886 = 19*kappa
  tmp887 = 1 + tmp845
  tmp888 = -4 + tmp75
  tmp889 = -1 + tmp223
  tmp890 = 2 + kappa
  tmp891 = 4 + tmp1
  tmp892 = 8*tmp56*tmp891
  tmp893 = tmp1*tmp357
  tmp894 = -4 + kappa
  tmp895 = 6*tmp1
  tmp896 = 6*tmp2*tmp25
  tmp897 = -6 + tmp1
  tmp898 = ss*tmp824
  tmp899 = tmp56*tmp77
  tmp900 = -2 + tmp1
  tmp901 = tmp25*tmp865
  tmp902 = 3 + kappa
  tmp903 = 6 + tmp1
  tmp904 = 3*tmp25
  tmp905 = 4*tmp56*tmp891
  tmp906 = (tmp238*tmp404)/tmp19
  tmp907 = tmp2*tmp404
  tmp908 = 56*tmp1
  tmp909 = 12*tmp56*tmp891
  tmp910 = kappa*tmp708
  tmp911 = -3*tmp899
  tmp912 = -38*tmp1
  tmp913 = -16 + tmp239
  tmp914 = tmp455*tmp56
  tmp915 = -tmp100
  tmp916 = 9 + kappa
  tmp917 = 2 + tmp223
  tmp918 = 1/tmp19 + tmp302
  tmp919 = 2*tmp100
  tmp920 = tmp25 + tmp919
  tmp921 = 1/tmp19 + tmp198
  tmp922 = 16 + tmp451
  tmp923 = 7*tmp492
  tmp924 = 22 + kappa
  tmp925 = -5 + tmp404
  tmp926 = 18/tmp19
  tmp927 = 3 + tmp404
  tmp928 = kappa*tmp510
  tmp929 = 4 + tmp928
  tmp930 = tmp488*tmp922
  tmp931 = 60*tmp1
  tmp932 = 8*tmp56*tmp927
  tmp933 = 1280*tmp182*tmp3
  tmp934 = -32*tmp1
  tmp935 = 54/tmp19
  tmp936 = tmp732*tmp924
  tmp937 = 6 + kappa
  tmp938 = tmp1*tmp937
  tmp939 = 10 + tmp938
  tmp940 = 4*tmp56*tmp939
  tmp941 = tmp302 + tmp414
  tmp942 = 12*tmp111
  tmp943 = tmp929/tmp19
  tmp944 = 36*tmp1
  tmp945 = 14 + tmp263
  tmp946 = tmp1*tmp345
  tmp947 = 2*tmp551
  tmp948 = 17*kappa
  tmp949 = 22*tmp1
  tmp950 = 34 + tmp944
  tmp951 = tmp950/tmp19
  tmp952 = -3 + tmp404
  tmp953 = 13*kappa
  tmp954 = -22*tmp1
  tmp955 = tmp25*tmp953
  tmp956 = 14*tmp1
  tmp957 = 27*kappa
  tmp958 = tmp1*tmp493
  tmp959 = 16*tmp56*tmp927
  tmp960 = 66/tmp19
  tmp961 = -8 + tmp266
  tmp962 = 10*tmp1
  tmp963 = 22/tmp19
  tmp964 = 24*tmp56*tmp927
  tmp965 = tmp556*tmp927
  tmp966 = (tmp25*tmp945)/tmp19
  tmp967 = tmp1*tmp961
  tmp968 = -7*kappa
  tmp969 = 14 + tmp451
  tmp970 = tmp25*tmp969
  tmp971 = 4 + kappa
  tmp972 = -16 + tmp77
  tmp973 = 12*tmp56*tmp927
  tmp974 = -18*kappa
  tmp975 = tmp701**(-7)
  tmp976 = (4*tmp920)/tmp702
  tmp977 = -32*tmp468*tmp6
  tmp978 = 64*tmp135*tmp3
  tmp979 = tmp819 + tmp976 + tmp977 + tmp978
  tmp980 = 2*tmp975*tmp979
  tmp981 = (tmp1*tmp50*tmp566*tmp573)/tmp49
  tmp982 = 5 + tmp404
  tmp983 = 128*tmp186*tmp982
  tmp984 = tmp357*tmp59*tmp918
  tmp985 = -10*tmp25
  tmp986 = 48*ss*tmp3
  tmp987 = tmp46 + tmp556 + tmp877 + tmp942 + tmp985 + tmp986
  tmp988 = tmp403*tmp987
  tmp989 = tmp59*tmp968
  tmp990 = 48*ss*tmp920
  tmp991 = (tmp100*tmp922)/tmp19
  tmp992 = tmp989 + tmp990 + tmp991
  tmp993 = tmp992/tmp702
  tmp994 = -7 + kappa
  tmp995 = tmp1*tmp994
  tmp996 = -8 + tmp995
  tmp997 = tmp404*tmp996
  tmp998 = 96*ss*tmp3
  tmp999 = tmp1*tmp924
  tmp1000 = 6 + tmp999
  tmp1001 = tmp1000/tmp19
  tmp1002 = tmp1001 + tmp997 + tmp998
  tmp1003 = tmp1002*tmp519*tmp6
  tmp1004 = tmp1003 + tmp983 + tmp984 + tmp988 + tmp993
  tmp1005 = -(tmp1004*tmp559)
  tmp1006 = -10 + tmp1
  tmp1007 = -128*tmp1006*tmp549
  tmp1008 = -10*kappa*ss*tmp59*tmp921
  tmp1009 = -64*tmp1
  tmp1010 = 15 + tmp404
  tmp1011 = tmp1010*tmp351
  tmp1012 = tmp1009 + tmp1011 + tmp522 + tmp556 + tmp648 + tmp849
  tmp1013 = 16*tmp1012*tmp186
  tmp1014 = -19*kappa*tmp719
  tmp1015 = 120*tmp31*tmp480
  tmp1016 = tmp551*tmp744
  tmp1017 = 8 + tmp930
  tmp1018 = tmp1017*tmp52
  tmp1019 = tmp56*tmp929
  tmp1020 = tmp1014 + tmp1015 + tmp1016 + tmp1018 + tmp1019
  tmp1021 = (tmp1020*tmp25)/tmp702
  tmp1022 = 960*tmp3*tmp31
  tmp1023 = 72 + kappa
  tmp1024 = tmp1*tmp1023
  tmp1025 = -10 + tmp1024
  tmp1026 = tmp1025*tmp118
  tmp1027 = -56*tmp1
  tmp1028 = -19 + tmp263
  tmp1029 = tmp1028*tmp465
  tmp1030 = 42/tmp19
  tmp1031 = tmp1027 + tmp1029 + tmp1030 + tmp936
  tmp1032 = tmp1031*tmp89
  tmp1033 = tmp1022 + tmp1026 + tmp1032 + tmp923 + tmp940
  tmp1034 = tmp1*tmp1033*tmp50
  tmp1035 = 44*tmp25
  tmp1036 = 14*tmp100
  tmp1037 = tmp100*tmp858
  tmp1038 = 480*tmp3*tmp31
  tmp1039 = -74 + kappa
  tmp1040 = tmp1*tmp1039
  tmp1041 = 4 + tmp1040
  tmp1042 = tmp1041*tmp111
  tmp1043 = -86*tmp1
  tmp1044 = -18 + tmp263
  tmp1045 = tmp1044*tmp25
  tmp1046 = 34 + tmp931
  tmp1047 = tmp1046/tmp19
  tmp1048 = tmp1043 + tmp1045 + tmp1047
  tmp1049 = tmp1048*tmp89
  tmp1050 = tmp1035 + tmp1036 + tmp1037 + tmp1038 + tmp1042 + tmp1049 + tmp932
  tmp1051 = tmp1050*tmp184
  tmp1052 = tmp1007 + tmp1008 + tmp1013 + tmp1021 + tmp1034 + tmp1051
  tmp1053 = tmp1052*tmp189
  tmp1054 = 256*tmp575*tmp925
  tmp1055 = 20*kappa*tmp31*tmp43*tmp59
  tmp1056 = tmp185*tmp900
  tmp1057 = -12*tmp111
  tmp1058 = tmp1056 + tmp1057 + tmp495 + tmp82 + tmp926 + tmp944
  tmp1059 = 32*tmp1058*tmp549
  tmp1060 = -24*tmp25
  tmp1061 = tmp493*tmp919
  tmp1062 = -5 + tmp895
  tmp1063 = tmp1062*tmp674
  tmp1064 = -48*tmp111
  tmp1065 = 38 + tmp968
  tmp1066 = (tmp1065*tmp25)/tmp19
  tmp1067 = -16*tmp56*tmp927
  tmp1068 = -15*tmp1
  tmp1069 = tmp25*tmp890
  tmp1070 = -1 + tmp895
  tmp1071 = tmp1070*tmp21
  tmp1072 = tmp1068 + tmp1069 + tmp1071
  tmp1073 = tmp1072*tmp183
  tmp1074 = tmp1060 + tmp1061 + tmp1063 + tmp1064 + tmp1066 + tmp1067 + tmp1073
  tmp1075 = 8*tmp1074*tmp186
  tmp1076 = 80*tmp182*tmp480
  tmp1077 = tmp161*tmp77
  tmp1078 = tmp862 + tmp943
  tmp1079 = tmp1078*tmp304
  tmp1080 = 16 + tmp930
  tmp1081 = tmp1080/tmp19
  tmp1082 = tmp1081 + tmp842
  tmp1083 = tmp1082*tmp31
  tmp1084 = tmp1076 + tmp1077 + tmp1079 + tmp1083
  tmp1085 = (tmp1084*tmp849)/tmp702
  tmp1086 = tmp100*tmp95
  tmp1087 = -28*tmp25
  tmp1088 = -13*kappa
  tmp1089 = 64 + tmp1088
  tmp1090 = tmp100*tmp1089
  tmp1091 = tmp1087 + tmp1090
  tmp1092 = tmp1091/tmp19
  tmp1093 = tmp890*tmp895
  tmp1094 = 23 + tmp1093
  tmp1095 = tmp1094*tmp519*tmp56
  tmp1096 = -42*tmp1
  tmp1097 = tmp25*tmp945
  tmp1098 = 38 + tmp931
  tmp1099 = tmp1098/tmp19
  tmp1100 = tmp1096 + tmp1097 + tmp1099
  tmp1101 = tmp1100*tmp679
  tmp1102 = 11*tmp25
  tmp1103 = -2*tmp100*tmp2
  tmp1104 = -10 + kappa
  tmp1105 = tmp1104*tmp25
  tmp1106 = tmp1 + tmp1105
  tmp1107 = tmp1106*tmp70
  tmp1108 = tmp1102 + tmp1103 + tmp1107 + tmp932
  tmp1109 = tmp1108*tmp351
  tmp1110 = tmp1086 + tmp1092 + tmp1095 + tmp1101 + tmp1109 + tmp933
  tmp1111 = -2*tmp1110*tmp135
  tmp1112 = tmp264*tmp465
  tmp1113 = tmp1112 + tmp934 + tmp935 + tmp936
  tmp1114 = tmp1113*tmp148
  tmp1115 = kappa*tmp869
  tmp1116 = 11 + tmp253
  tmp1117 = tmp1116*tmp199
  tmp1118 = tmp1 + tmp1115 + tmp1117 + tmp632
  tmp1119 = tmp111*tmp1118
  tmp1120 = -8 + kappa
  tmp1121 = tmp1120*tmp673
  tmp1122 = tmp1 + tmp1121
  tmp1123 = tmp1122/tmp19
  tmp1124 = tmp1123 + tmp492 + tmp940
  tmp1125 = tmp1124*tmp89
  tmp1126 = tmp1114 + tmp1119 + tmp1125 + tmp933
  tmp1127 = tmp1*tmp1126*tmp6
  tmp1128 = tmp1054 + tmp1055 + tmp1059 + tmp1075 + tmp1085 + tmp1111 + tmp1127
  tmp1129 = tmp1128*tmp187
  tmp1130 = 128*tmp480*tmp833
  tmp1131 = tmp188*tmp357*tmp59*tmp941
  tmp1132 = tmp185*tmp478
  tmp1133 = tmp1132 + tmp166 + tmp465 + tmp522 + tmp648 + tmp960
  tmp1134 = -16*tmp1133*tmp820
  tmp1135 = tmp878*tmp919
  tmp1136 = -1 + tmp404
  tmp1137 = -192*tmp1136*tmp31
  tmp1138 = 190*tmp111
  tmp1139 = 34 + tmp886
  tmp1140 = (tmp1139*tmp25)/tmp19
  tmp1141 = 32*tmp56*tmp927
  tmp1142 = tmp46 + tmp708 + tmp942 + tmp963
  tmp1143 = tmp1142*tmp185
  tmp1144 = tmp1135 + tmp1137 + tmp1138 + tmp1140 + tmp1141 + tmp1143 + tmp623
  tmp1145 = 4*tmp1144*tmp575
  tmp1146 = 48*tmp182*tmp480
  tmp1147 = 6*tmp1077
  tmp1148 = tmp522 + tmp943
  tmp1149 = ss*tmp1148*tmp274
  tmp1150 = 29*tmp46
  tmp1151 = 32 + tmp930
  tmp1152 = tmp1151/tmp19
  tmp1153 = tmp1150 + tmp1152
  tmp1154 = tmp1153*tmp31
  tmp1155 = tmp1146 + tmp1147 + tmp1149 + tmp1154
  tmp1156 = (tmp1155*tmp25*tmp31)/tmp702
  tmp1157 = tmp263*tmp59
  tmp1158 = 2 + tmp708
  tmp1159 = 512*tmp1158*tmp182
  tmp1160 = -124*tmp25
  tmp1161 = -41*kappa
  tmp1162 = 48 + tmp1161
  tmp1163 = tmp100*tmp1162
  tmp1164 = tmp1160 + tmp1163
  tmp1165 = tmp1164/tmp19
  tmp1166 = tmp1*tmp254
  tmp1167 = -16 + tmp1166
  tmp1168 = 16*tmp1*tmp1167*tmp56
  tmp1169 = tmp904*tmp937
  tmp1170 = tmp1169 + tmp934 + tmp951
  tmp1171 = tmp1170*tmp674
  tmp1172 = tmp74*tmp919
  tmp1173 = 53*tmp111
  tmp1174 = tmp117 + tmp1172 + tmp1173 + tmp932 + tmp966
  tmp1175 = tmp1174*tmp140
  tmp1176 = tmp1157 + tmp1159 + tmp1165 + tmp1168 + tmp1171 + tmp1175
  tmp1177 = tmp1176*tmp549
  tmp1178 = 384*tmp3*tmp834
  tmp1179 = -2*tmp161*tmp492
  tmp1180 = 46*tmp1
  tmp1181 = 78 + tmp263
  tmp1182 = tmp1181*tmp25
  tmp1183 = 46 + tmp931
  tmp1184 = tmp1183/tmp19
  tmp1185 = tmp1180 + tmp1182 + tmp1184
  tmp1186 = 8*tmp1185*tmp188
  tmp1187 = tmp1*tmp948
  tmp1188 = 8 + tmp1187
  tmp1189 = tmp1188/tmp19
  tmp1190 = tmp1189 + tmp690 + tmp82
  tmp1191 = tmp1190*tmp52*tmp849
  tmp1192 = tmp829*tmp919
  tmp1193 = 49*tmp111
  tmp1194 = 58 + tmp282
  tmp1195 = (tmp1194*tmp25)/tmp19
  tmp1196 = tmp1192 + tmp1193 + tmp1195 + tmp552 + tmp932
  tmp1197 = tmp1196*tmp758
  tmp1198 = tmp451*tmp59
  tmp1199 = 25*tmp77
  tmp1200 = 14 + tmp1199
  tmp1201 = (tmp1200*tmp25)/tmp19
  tmp1202 = 6 + tmp817
  tmp1203 = tmp1*tmp1202
  tmp1204 = 12 + tmp1203
  tmp1205 = tmp1204*tmp56*tmp83
  tmp1206 = tmp1198 + tmp1201 + tmp1205
  tmp1207 = tmp1206*tmp31
  tmp1208 = tmp1178 + tmp1179 + tmp1186 + tmp1191 + tmp1197 + tmp1207
  tmp1209 = tmp1208*tmp305
  tmp1210 = 192*tmp188*tmp3
  tmp1211 = tmp161*tmp25*tmp968
  tmp1212 = -2 + tmp946 + tmp947
  tmp1213 = tmp1212*tmp171*tmp805
  tmp1214 = 29 + tmp263
  tmp1215 = tmp1*tmp1214
  tmp1216 = 8 + tmp1215
  tmp1217 = tmp1216*tmp404
  tmp1218 = 5*tmp999
  tmp1219 = 78 + tmp1218
  tmp1220 = tmp1219/tmp19
  tmp1221 = tmp1217 + tmp1220
  tmp1222 = 2*tmp1221*tmp182
  tmp1223 = 5*tmp492
  tmp1224 = 24 + tmp948
  tmp1225 = tmp1*tmp1224
  tmp1226 = 15 + tmp1225
  tmp1227 = tmp111*tmp1226
  tmp1228 = tmp1223 + tmp1227 + tmp940
  tmp1229 = tmp1228*tmp306
  tmp1230 = tmp1210 + tmp1211 + tmp1213 + tmp1222 + tmp1229
  tmp1231 = -2*tmp1230*tmp6*tmp805
  tmp1232 = 15 + tmp949
  tmp1233 = 64*tmp1232*tmp188
  tmp1234 = 22 + tmp451
  tmp1235 = tmp1234*tmp25
  tmp1236 = tmp1235 + tmp273 + tmp951
  tmp1237 = tmp1236*tmp794
  tmp1238 = 10 + tmp910
  tmp1239 = tmp1238*tmp199
  tmp1240 = tmp1239 + tmp522 + tmp690
  tmp1241 = (tmp1240*tmp25)/tmp19
  tmp1242 = -26*tmp25
  tmp1243 = 6*tmp100*tmp493
  tmp1244 = 50 + tmp266
  tmp1245 = tmp1*tmp1244
  tmp1246 = 48 + tmp1245
  tmp1247 = tmp111*tmp1246
  tmp1248 = tmp1242 + tmp1243 + tmp1247 + tmp959
  tmp1249 = tmp1248*tmp320
  tmp1250 = tmp357*tmp59
  tmp1251 = 31 + tmp967
  tmp1252 = (tmp1251*tmp25)/tmp19
  tmp1253 = 6 + tmp263
  tmp1254 = tmp1*tmp1253
  tmp1255 = 14 + tmp1254
  tmp1256 = tmp1255*tmp56*tmp769
  tmp1257 = tmp1250 + tmp1252 + tmp1256
  tmp1258 = tmp1257*tmp32
  tmp1259 = tmp1233 + tmp1237 + tmp1241 + tmp1249 + tmp1258
  tmp1260 = -2*tmp1259*tmp186
  tmp1261 = tmp1130 + tmp1131 + tmp1134 + tmp1145 + tmp1156 + tmp1177 + tmp1209 + &
                          &tmp1231 + tmp1260
  tmp1262 = -(tmp1261*tmp4)
  tmp1263 = -5 + tmp1
  tmp1264 = 64*tmp1263*tmp820
  tmp1265 = tmp1157*tmp182*tmp880
  tmp1266 = 16 + tmp519 + tmp77
  tmp1267 = tmp1*tmp1266
  tmp1268 = tmp183*tmp952
  tmp1269 = 7 + tmp404
  tmp1270 = tmp1269*tmp556
  tmp1271 = tmp1267 + tmp1268 + tmp1270
  tmp1272 = tmp1271*tmp742
  tmp1273 = tmp894*tmp919
  tmp1274 = -2 + tmp488
  tmp1275 = tmp1274*tmp674
  tmp1276 = 106*tmp111
  tmp1277 = -2 + tmp953
  tmp1278 = (tmp1277*tmp25)/tmp19
  tmp1279 = tmp25*tmp937
  tmp1280 = tmp1279 + tmp954 + tmp965
  tmp1281 = tmp1280*tmp60
  tmp1282 = tmp1273 + tmp1275 + tmp1276 + tmp1278 + tmp1281 + tmp555 + tmp964
  tmp1283 = -4*tmp1282*tmp549
  tmp1284 = 60*tmp182*tmp480
  tmp1285 = 3*tmp1077
  tmp1286 = tmp82 + tmp943
  tmp1287 = tmp1286*tmp525
  tmp1288 = 24 + tmp930
  tmp1289 = tmp1288/tmp19
  tmp1290 = tmp1289 + tmp955
  tmp1291 = tmp1290*tmp31
  tmp1292 = tmp1284 + tmp1285 + tmp1287 + tmp1291
  tmp1293 = (tmp1292*tmp810)/tmp702
  tmp1294 = kappa*tmp59
  tmp1295 = tmp100 + tmp1294
  tmp1296 = -4*tmp1295
  tmp1297 = 5 + tmp956
  tmp1298 = tmp1297*tmp686
  tmp1299 = -56 + tmp957
  tmp1300 = tmp1*tmp1299
  tmp1301 = 60 + tmp1300
  tmp1302 = (tmp1301*tmp25)/tmp19
  tmp1303 = 18 + tmp282
  tmp1304 = tmp1*tmp1303
  tmp1305 = 40 + tmp1304
  tmp1306 = tmp1305*tmp56*tmp769
  tmp1307 = 46 + tmp944
  tmp1308 = tmp1307/tmp19
  tmp1309 = tmp1308 + tmp954 + tmp970
  tmp1310 = tmp1309*tmp679
  tmp1311 = -16 + tmp958
  tmp1312 = tmp1311*tmp465
  tmp1313 = 2 + tmp451
  tmp1314 = tmp1*tmp1313
  tmp1315 = 6 + tmp1314
  tmp1316 = (tmp1315*tmp510)/tmp19
  tmp1317 = tmp1312 + tmp1316 + tmp959
  tmp1318 = tmp1317*tmp60
  tmp1319 = tmp1296 + tmp1298 + tmp1302 + tmp1306 + tmp1310 + tmp1318
  tmp1320 = tmp1319*tmp186
  tmp1321 = 480*tmp182*tmp3
  tmp1322 = 13 + tmp263
  tmp1323 = tmp1322*tmp465
  tmp1324 = tmp1323 + tmp521 + tmp936 + tmp960
  tmp1325 = tmp1324*tmp320
  tmp1326 = 2*tmp513
  tmp1327 = -5 + tmp928
  tmp1328 = tmp1327/tmp19
  tmp1329 = tmp1326 + tmp1328 + tmp681
  tmp1330 = (tmp1329*tmp404)/tmp19
  tmp1331 = 3*tmp967
  tmp1332 = 8 + tmp1331
  tmp1333 = tmp111*tmp1332
  tmp1334 = 12*tmp56*tmp939
  tmp1335 = tmp1333 + tmp1334 + tmp492
  tmp1336 = ss*tmp1335
  tmp1337 = tmp1321 + tmp1325 + tmp1330 + tmp1336
  tmp1338 = tmp1337*tmp6*tmp805
  tmp1339 = 960*tmp188*tmp3
  tmp1340 = 46 + tmp263
  tmp1341 = tmp1340*tmp25
  tmp1342 = 7 + tmp962
  tmp1343 = tmp1342*tmp556
  tmp1344 = tmp1341 + tmp1343 + tmp404
  tmp1345 = 16*tmp1344*tmp182
  tmp1346 = tmp25*tmp968
  tmp1347 = 9*tmp1*tmp551
  tmp1348 = tmp1346 + tmp1347 + tmp14 + tmp690 + tmp963
  tmp1349 = (tmp1348*tmp25)/tmp19
  tmp1350 = 1 + tmp451
  tmp1351 = tmp1*tmp1350
  tmp1352 = -9 + tmp1351
  tmp1353 = tmp1352*tmp465
  tmp1354 = tmp1*tmp945
  tmp1355 = 18 + tmp1354
  tmp1356 = (tmp1355*tmp510)/tmp19
  tmp1357 = tmp1353 + tmp1356 + tmp964
  tmp1358 = tmp1357*tmp320
  tmp1359 = tmp100*tmp972
  tmp1360 = -32 + tmp886
  tmp1361 = tmp1*tmp1360
  tmp1362 = -16 + tmp1361
  tmp1363 = (tmp1362*tmp25)/tmp19
  tmp1364 = 6 + tmp282
  tmp1365 = tmp1*tmp1364
  tmp1366 = 11 + tmp1365
  tmp1367 = tmp1366*tmp56*tmp83
  tmp1368 = tmp1359 + tmp1363 + tmp1367
  tmp1369 = tmp1368*tmp8
  tmp1370 = tmp1339 + tmp1345 + tmp1349 + tmp1358 + tmp1369
  tmp1371 = -(tmp135*tmp1370)
  tmp1372 = tmp1264 + tmp1265 + tmp1272 + tmp1283 + tmp1293 + tmp1320 + tmp1338 + &
                          &tmp1371
  tmp1373 = -2*tmp1372*tmp181
  tmp1374 = 128*tmp1*tmp833
  tmp1375 = 2*tmp1294*tmp188*tmp868
  tmp1376 = -4 + tmp404 + tmp77
  tmp1377 = tmp1*tmp1376
  tmp1378 = 1 + tmp488
  tmp1379 = tmp1378*tmp351
  tmp1380 = tmp1377 + tmp1379 + tmp965
  tmp1381 = -16*tmp1380*tmp820
  tmp1382 = tmp2*tmp919
  tmp1383 = 2 + tmp488
  tmp1384 = 64*tmp1383*tmp31
  tmp1385 = 72*tmp111
  tmp1386 = tmp465*tmp902
  tmp1387 = 4 + tmp510
  tmp1388 = tmp1387*tmp257
  tmp1389 = tmp1386 + tmp1388 + tmp491
  tmp1390 = tmp1389*tmp351
  tmp1391 = tmp117 + tmp1382 + tmp1384 + tmp1385 + tmp1390 + tmp932 + tmp966
  tmp1392 = 4*tmp1391*tmp575
  tmp1393 = tmp480*tmp758
  tmp1394 = 2*tmp1077
  tmp1395 = kappa*tmp673
  tmp1396 = tmp1395 + tmp943
  tmp1397 = tmp1396*tmp52
  tmp1398 = tmp1*tmp922
  tmp1399 = 8 + tmp1398
  tmp1400 = tmp1399/tmp19
  tmp1401 = tmp1400 + tmp884
  tmp1402 = tmp1401*tmp31
  tmp1403 = tmp1393 + tmp1394 + tmp1397 + tmp1402
  tmp1404 = (tmp1403*tmp208*tmp31)/tmp702
  tmp1405 = 8*tmp100
  tmp1406 = 3 + tmp488
  tmp1407 = 256*tmp1406*tmp182
  tmp1408 = 62 + tmp967
  tmp1409 = (tmp1408*tmp25)/tmp19
  tmp1410 = 6 + tmp968
  tmp1411 = tmp1*tmp1410
  tmp1412 = 22 + tmp1411
  tmp1413 = tmp1412*tmp56*tmp769
  tmp1414 = 7 + tmp895
  tmp1415 = tmp1414*tmp556
  tmp1416 = tmp14 + tmp1415 + tmp970
  tmp1417 = tmp1416*tmp674
  tmp1418 = -9 + tmp285
  tmp1419 = tmp1418*tmp465
  tmp1420 = tmp83*tmp971
  tmp1421 = 77 + tmp1420
  tmp1422 = tmp111*tmp1421
  tmp1423 = tmp1419 + tmp1422 + tmp973
  tmp1424 = tmp1423*tmp351
  tmp1425 = tmp1405 + tmp1407 + tmp1409 + tmp1413 + tmp1417 + tmp1424 + tmp819
  tmp1426 = -(tmp1425*tmp549)
  tmp1427 = 128*tmp188*tmp3
  tmp1428 = 18 + kappa
  tmp1429 = tmp1428*tmp25
  tmp1430 = 5 + tmp895
  tmp1431 = tmp1430*tmp21
  tmp1432 = tmp1429 + tmp1431 + tmp956
  tmp1433 = 16*tmp1432*tmp182
  tmp1434 = -28/tmp19
  tmp1435 = 47*tmp1*tmp551
  tmp1436 = tmp1434 + tmp1435 + tmp46 + tmp711 + tmp895
  tmp1437 = tmp1436*tmp471
  tmp1438 = 4*tmp100*tmp890
  tmp1439 = 10 + kappa
  tmp1440 = tmp1439*tmp769
  tmp1441 = 39 + tmp1440
  tmp1442 = tmp111*tmp1441
  tmp1443 = 4*tmp56*tmp927
  tmp1444 = tmp1438 + tmp1442 + tmp1443 + tmp25
  tmp1445 = tmp1444*tmp148
  tmp1446 = 2*tmp1359
  tmp1447 = 26*tmp25
  tmp1448 = 4 + tmp282
  tmp1449 = 6*tmp100*tmp1448
  tmp1450 = tmp1447 + tmp1449
  tmp1451 = tmp1450/tmp19
  tmp1452 = 18 + tmp817
  tmp1453 = tmp1*tmp1452
  tmp1454 = 34 + tmp1453
  tmp1455 = tmp1454*tmp56*tmp769
  tmp1456 = tmp1446 + tmp1451 + tmp1455
  tmp1457 = ss*tmp1456
  tmp1458 = tmp1427 + tmp1433 + tmp1437 + tmp1445 + tmp1457
  tmp1459 = tmp135*tmp1458*tmp8
  tmp1460 = 64*tmp188*tmp3
  tmp1461 = -14*tmp161*tmp46
  tmp1462 = 7 + kappa
  tmp1463 = tmp1462*tmp465
  tmp1464 = tmp999/tmp19
  tmp1465 = tmp1463 + tmp1464 + tmp895 + tmp926
  tmp1466 = 4*tmp1465*tmp182
  tmp1467 = tmp1187/tmp19
  tmp1468 = tmp1467 + tmp521 + tmp522 + tmp632 + tmp824
  tmp1469 = (tmp1468*tmp805)/tmp19
  tmp1470 = kappa*tmp1405
  tmp1471 = tmp404*tmp890
  tmp1472 = 3 + tmp1471
  tmp1473 = (tmp1472*tmp962)/tmp19
  tmp1474 = tmp1470 + tmp1473 + tmp940
  tmp1475 = tmp1474*tmp31
  tmp1476 = tmp1460 + tmp1461 + tmp1466 + tmp1469 + tmp1475
  tmp1477 = tmp1476*tmp6*tmp805
  tmp1478 = 4 + tmp488
  tmp1479 = 128*tmp1478*tmp188
  tmp1480 = 8 + kappa
  tmp1481 = tmp1480*tmp465
  tmp1482 = tmp3*tmp851
  tmp1483 = tmp1481 + tmp1482 + tmp708
  tmp1484 = tmp1483*tmp794
  tmp1485 = 16 + tmp928
  tmp1486 = tmp1485/tmp19
  tmp1487 = tmp1486 + tmp404 + tmp632 + tmp82
  tmp1488 = (tmp1487*tmp25)/tmp19
  tmp1489 = -21*tmp25
  tmp1490 = 2 + tmp455
  tmp1491 = tmp100*tmp1490
  tmp1492 = 71*tmp111
  tmp1493 = tmp1279*tmp194
  tmp1494 = tmp1489 + tmp1491 + tmp1492 + tmp1493 + tmp973
  tmp1495 = tmp148*tmp1494
  tmp1496 = 12 + tmp77
  tmp1497 = tmp100*tmp1496
  tmp1498 = -33*tmp25
  tmp1499 = 4 + tmp974
  tmp1500 = tmp100*tmp1499
  tmp1501 = tmp1498 + tmp1500
  tmp1502 = tmp1501/tmp19
  tmp1503 = -92*tmp1
  tmp1504 = 26*kappa
  tmp1505 = -36 + tmp1504
  tmp1506 = tmp1505*tmp25
  tmp1507 = tmp1503 + tmp1506
  tmp1508 = tmp1507*tmp56
  tmp1509 = tmp1497 + tmp1502 + tmp1508
  tmp1510 = tmp1509*tmp32
  tmp1511 = tmp1479 + tmp1484 + tmp1488 + tmp1495 + tmp1510
  tmp1512 = tmp1511*tmp186
  tmp1513 = tmp1374 + tmp1375 + tmp1381 + tmp1392 + tmp1404 + tmp1426 + tmp1459 + &
                          &tmp1477 + tmp1512
  tmp1514 = (tmp1513*tmp24)/tmp701
  tmp1515 = tmp1005 + tmp1053 + tmp1129 + tmp1262 + tmp1373 + tmp1514 + tmp980 + t&
                          &mp981
  MP2MPL_MP_FIN_d12d21 = -8*C0tF2F20F0*Pi*tmp3*(-(tmp11*tmp223) + f1p*tmp11*tmp223 + (f2p*tmp3)/tmp19) + &
                          &4*logmuF2*Pi*tmp19*(-(tmp22*tmp223) + f1p*tmp22*tmp223 + f2p*(tmp1*tmp23 + tmp21&
                          &*tmp3)) - 4*D0tsLN*Pi*tmp100*tmp17*tmp19*tmp2*tmp464 - 4*D0tsNL*Pi*tmp100*tmp17*&
                          &tmp19*tmp2*tmp464 + DB0sFP*logmuF2*Pi*tmp185*tmp19*tmp220*tmp249*tmp3*tmp30*tmp4&
                          &9 + DB0sFP*logF2P2*Pi*tmp19*tmp220*tmp249*tmp3*tmp30*tmp351*tmp49 + DB0sFPlogmut&
                          &*Pi*tmp185*tmp19*tmp3*tmp30*(tmp1*tmp23*tmp262 + f1p*tmp14*tmp23*tmp262 - f2p*tm&
                          &p3*(tmp109 + tmp24/tmp19 + (4*tmp24)/tmp701)) + 8*C0tF2F2NFN*Pi*tmp17*tmp58*tmp5&
                          &9*(f1p*tmp131 + tmp18 + f2p/tmp49 - 2/tmp701) + 8*C0tP2P2NPN*Pi*tmp17*tmp19*tmp4&
                          &9*tmp58*tmp59*(2*tmp153 - 2*f1p*tmp153 + (f2p*(tmp56 + 36*tmp6 + (tmp12 + tmp21)&
                          &/tmp701 - (2*(tmp42 + tmp706))/tmp702))/tmp19) + 8*C0tF2F2LFL*Pi*tmp17*tmp25*((f&
                          &2p*tmp36)/tmp220 - (4*tmp15*tmp2)/tmp702 + (4*f1p*tmp15*tmp2)/tmp702)*tmp78 + 8*&
                          &C0tP2P2LPL*Pi*tmp17*tmp19*tmp25*tmp49*(-((f2p*tmp36*(tmp154 + tmp487 + tmp489 + &
                          &tmp56 + (2*(tmp110 - 6/tmp702))/tmp701 + (2*(tmp42 + tmp488 + tmp7))/tmp702))/tm&
                          &p19) + (4*tmp180*tmp2)/tmp702 - (4*f1p*tmp180*tmp2)/tmp702)*tmp78 - 16*DB0tLL*Pi&
                          &*tmp17*tmp19*tmp25*tmp49*(-((f2p*tmp36*tmp62)/tmp19) - (4*tmp2*tmp73)/tmp702 + (&
                          &4*f1p*tmp2*tmp73)/tmp702)*tmp78 + D0IR6tsL*Pi*tmp19*tmp34*tmp521*(tmp2*tmp486 - &
                          &f1p*tmp2*tmp486 + f2p*tmp3*(tmp407 + tmp24*tmp408 + (tmp25 + tmp344 + tmp413 + t&
                          &mp257/tmp702)/tmp701)*tmp78) - (16*DB0sFP*f2p*Pi*(tmp4 + tmp5 + tmp6 + tmp8/tmp7&
                          &01 + tmp8/tmp702))/(tmp126 + tmp31 + tmp4 + tmp5 + tmp6 + tmp32/tmp702) + (16*DB&
                          &0F2FL*Pi*tmp1*tmp19*tmp30*tmp34*((4*tmp2*tmp45)/tmp702 - (4*f1p*tmp2*tmp45)/tmp7&
                          &02 - (f2p*tmp36*(tmp301 + 1/tmp701 + tmp8))/tmp19))/tmp701 + logmuP2*Pi*tmp19*tm&
                          &p769*(-(tmp2*tmp22) + f1p*tmp2*tmp22 - f2p*(tmp48 + 1/tmp701 + 1/tmp702 + tmp8))&
                          & + 16*DB0tLN*Pi*tmp100*tmp17*tmp19*tmp2*tmp49*(f2p*tmp199*tmp36*tmp62 - tmp73*tm&
                          &p80 + f1p*tmp73*tmp80) - 8*C0tP2P2NPL*Pi*tmp100*tmp17*tmp19*tmp2*tmp49*(32*tmp18&
                          &6*tmp254 + tmp190*tmp269 + (kappa*tmp134*tmp404)/tmp220 - 2*tmp288*tmp6 + (4*tmp&
                          &300)/tmp701 + (2*tmp278)/tmp702 - 2*tmp255*tmp4*tmp80 + 2*f1p*(-16*tmp186*tmp254&
                          & - 4*tmp135*tmp269 + tmp288*tmp6 - (2*tmp300)/tmp701 - tmp278/tmp702 - (tmp134*t&
                          &mp77)/tmp220 + tmp255*tmp4*tmp80) + (f2p*tmp36*(tmp344 - 2*tmp56 - 32*tmp6 + (2*&
                          &(tmp14 + tmp199 + tmp256))/tmp701 + (2*(tmp257 + tmp491))/tmp702 + 2*tmp805))/tm&
                          &p19) - 8*D0tsLL*Pi*tmp17*tmp19*tmp25*tmp78*((4*tmp125*tmp2)/tmp702 - (4*f1p*tmp1&
                          &25*tmp2)/tmp702 - (f2p*tmp36*(tmp109 + tmp133 + tmp465 + (tmp13 + tmp510 + tmp60&
                          &)/tmp701 + tmp110/tmp702 + tmp809))/tmp19) + 8*C0IR6sFP*Pi*tmp19*(-(tmp223*tmp23&
                          &*tmp47) + f1p*tmp223*tmp23*tmp47 + f2p*tmp3*((tmp46 + tmp48)/tmp701 - tmp24*(tmp&
                          &21 + tmp82))) + C0tF2F2LF0*Pi*tmp19*tmp34*(-(tmp2*tmp213) + f1p*tmp2*tmp213 + (f&
                          &2p*(tmp14 + tmp21)*tmp3*tmp36)/tmp19)*tmp83 + C0sF2P2FLP*Pi*tmp19*tmp30*tmp34*((&
                          &4*tmp2*tmp545)/tmp702 - (4*f1p*tmp2*tmp545)/tmp702 + f2p*tmp36*(tmp181*(tmp274 +&
                          & tmp511) + tmp4*(tmp512 + ss*(tmp510 + tmp522 + tmp700) + (tmp513 + tmp554)/tmp7&
                          &02) + (ss*(tmp351/tmp19 - 3*ss*tmp513 + tmp1*tmp554) + tmp513*tmp6 + (tmp42*(tmp&
                          &48 + tmp513) + tmp1*tmp619)/tmp702)/tmp701 + tmp24*((tmp21 + tmp511)*tmp6 + (tmp&
                          &512 + tmp42*(tmp199 + tmp513))/tmp702 + ss*(tmp1*tmp199 + tmp304 + tmp509*tmp809&
                          &))))*tmp83 + (16*logbeL2*Pi*tmp1*tmp200*tmp34*tmp81*(f2p*tmp21*tmp3*tmp36*tmp62 &
                          &+ tmp2*tmp73*tmp86 - f1p*tmp2*tmp73*tmp86))/(tmp12 + 1/tmp19) - 8*logmuN2*Pi*tmp&
                          &17*tmp19*tmp2*tmp25*(tmp518 + tmp560 + tmp491/tmp701 + (f2p*(tmp14 + tmp494 + (4&
                          & + tmp521)/tmp702))/tmp19 + tmp60/tmp702 + 20/(tmp701*tmp702) + (tmp619*tmp77)/t&
                          &mp702 + tmp805 + tmp822 + tmp824/tmp702 + (tmp1*tmp824)/tmp702 + f1p*(tmp154 + (&
                          &5*tmp16)/tmp701 + tmp1*(2*tmp242 + tmp8) + (tmp569 + tmp257*tmp867)/tmp702)) + (&
                          &DB0F2FN*Pi*tmp19*tmp2*tmp30*tmp34*(-tmp31 - tmp4 + f1p*tmp45 + tmp50 + tmp52 - 1&
                          &/(tmp19*tmp701) + tmp42/tmp701 + (f2p*(ss - 1/tmp701 + 1/tmp702))/tmp19 + 1/(tmp&
                          &19*tmp702) + tmp42/tmp702 + 2/(tmp701*tmp702))*tmp869)/tmp701 + logbeN2*Pi*tmp2*&
                          &tmp34*(f2p*tmp199*tmp3*tmp62 - tmp73*tmp76 + f1p*tmp73*tmp76)*tmp81*tmp869 + 4*l&
                          &ogN2L2*Pi*tmp100*tmp17*tmp2*tmp81*((tmp108*tmp255*tmp49)/tmp19 + (tmp13 + tmp155&
                          &)*tmp199*(tmp256 + tmp35 - (4*kappa)/tmp702 + f2p*tmp78 + f1p*tmp80) - (2*tmp108&
                          &*tmp23*(tmp111 + tmp132 - (2*tmp110)/tmp702))/(tmp101 + tmp88)) + 8*D0IR6tsN*Pi*&
                          &tmp19*tmp2*tmp25*tmp34*(tmp116 + 32*tmp135 + 48*tmp1*tmp135 + tmp250 + tmp251 + &
                          &tmp336 + tmp337 + tmp338 + tmp339 + tmp340 + tmp341 + tmp342 - 32*ss*tmp6 + tmp2&
                          &74*tmp6 + (tmp519*tmp6)/tmp19 + tmp56/tmp701 + (tmp404*tmp56)/tmp701 + (32*tmp6)&
                          &/tmp701 + (48*tmp1*tmp6)/tmp701 + tmp56/tmp702 + (tmp404*tmp56)/tmp702 - (32*ss)&
                          &/(tmp701*tmp702) + 12/(tmp19*tmp701*tmp702) - f1p*((32*tmp3*tmp4)/tmp702 + (-16*&
                          &tmp252*tmp6 - (4*(tmp3*tmp351 - (tmp253 + tmp478)/tmp19))/tmp702 + tmp56*(1 - tm&
                          &p75))/tmp701 - tmp24*(tmp252*tmp310 - (4*(1/tmp19 + tmp1*tmp348))/tmp702 + tmp56&
                          &*tmp76)) - 16*tmp135*tmp77 - (16*tmp6*tmp77)/tmp701 + kappa*tmp310*tmp805 - 2*tm&
                          &p56*tmp805 - 48*tmp6*tmp805 + (kappa*tmp619*tmp805)/tmp702 + kappa*tmp6*tmp815 +&
                          & (kappa*tmp815)/(tmp701*tmp702) + 2*f2p*tmp3*(tmp274*tmp4 + tmp24*(tmp132 + tmp5&
                          &6 + tmp21/tmp702) + (2*(ss*tmp199 + tmp414/tmp702 + tmp88))/tmp701)) - 16*DB0tNN&
                          &*Pi*tmp17*tmp19*tmp49*tmp58*tmp59*(tmp101 + tmp127 - 2*tmp31 - 2*tmp4 - 2*tmp6 +&
                          & (f2p*tmp62)/tmp19 + tmp70/tmp701 - 4/(tmp701*tmp702) + tmp709 + f1p*tmp73 + tmp&
                          &89/tmp701) + C0tP2P2LP0*Pi*tmp19*tmp34*tmp49*tmp83*(-2*tmp2*((tmp134*tmp46)/tmp2&
                          &20 + tmp184*tmp496 + tmp500*tmp6 + (2*tmp508)/tmp701 + (tmp4*(tmp46 - (4*tmp480)&
                          &/tmp702))/tmp220 + (tmp176*tmp209 + (tmp480*tmp783)/tmp220 + (tmp175 - 3*tmp202*&
                          &tmp25 - 4*tmp481 + tmp859)/tmp19)/tmp702) - 2*f1p*tmp2*((kappa*tmp134*tmp208)/tm&
                          &p220 - 4*tmp135*tmp496 + tmp50*tmp500 - (2*tmp508)/tmp701 + (tmp4*tmp86)/tmp220 &
                          &+ (ss*tmp209*tmp274 + (tmp320*tmp480)/tmp220 + (tmp56*tmp84 + tmp111*tmp855 + tm&
                          &p860)/tmp19)/tmp702) + (f2p*tmp3*tmp78*(tmp343 + tmp487 + tmp489 - 8*tmp6 + (tmp&
                          &274 + tmp404 + tmp490)/tmp701 + (2*(tmp199 + tmp488 + tmp89))/tmp702))/tmp19) + &
                          &8*D0tsNN*Pi*tmp17*tmp19*tmp58*tmp59*(tmp193 + tmp251 + ss*tmp56 + 64*ss*tmp6 - (&
                          &60*tmp6)/tmp19 - tmp56/tmp701 - (96*tmp6)/tmp701 - (32*tmp4)/tmp702 - tmp56/tmp7&
                          &02 + tmp48/(tmp701*tmp702) + tmp563/(tmp701*tmp702) + f1p*(tmp116 + 64*tmp135 + &
                          &tmp250 + (-64*ss + 60/tmp19)*tmp6 + (tmp56 + 96*tmp6 + (4*(tmp183 + 1/tmp19))/tm&
                          &p702)/tmp701 + tmp703/(tmp19*tmp702)) + (f2p*(-4*tmp4 + tmp52 - 36*tmp6 + (4*(ss&
                          & + tmp12))/tmp701 + (tmp7 + tmp89)/tmp702))/tmp19) + 8*C0tF2F2NF0*Pi*tmp19*tmp2*&
                          &tmp25*tmp34*(tmp128 + tmp129 + tmp130 + tmp56 + f2p*tmp199*tmp3*(tmp51 + tmp7) +&
                          & tmp21/tmp701 + 8/(tmp701*tmp702) + tmp769/(tmp19*tmp701) + (tmp111*tmp818)/tmp7&
                          &02 + (kappa*tmp83)/(tmp701*tmp702) - f1p*((2*(1/tmp19 + (4*tmp509)/tmp702 + tmp8&
                          &7))/tmp701 + (1/tmp19 + (tmp2*tmp769)/tmp702 + tmp87)/tmp19) + tmp893/(tmp19*tmp&
                          &701)) + 8*C0sF2P2FNP*Pi*tmp19*tmp2*tmp25*tmp30*tmp34*(-24*ss*tmp135 + 8*tmp186 +&
                          & (15*tmp135)/tmp19 - tmp181/tmp19 + tmp182/tmp19 + kappa*tmp135*tmp208 + kappa*t&
                          &mp181*tmp208 + tmp182*tmp46 + tmp4*tmp525 - 24*tmp4*tmp6 - 21*tmp52*tmp6 + tmp19&
                          &7/tmp701 - (32*ss*tmp6)/tmp701 - (31*tmp6)/(tmp19*tmp701) + (tmp46*tmp6)/tmp701 &
                          &+ (16*tmp181)/tmp702 + (tmp195*tmp4)/tmp702 + (tmp31*tmp414)/tmp702 + (tmp4*tmp4&
                          &6)/tmp702 + tmp585/tmp702 - (22*tmp52)/(tmp701*tmp702) + tmp674/(tmp701*tmp702) &
                          &+ (tmp260*tmp719)/(tmp701*tmp702) + tmp6*tmp726 + (tmp31*tmp842)/tmp701 + (tmp31&
                          &*tmp842)/tmp702 + (tmp4*tmp856)/tmp702 + f2p*(-(tmp181*(tmp13 + tmp46 + tmp48)) &
                          &- tmp24*(tmp184 + tmp31*(tmp199 + tmp46) + (tmp183 - 18/tmp19 + tmp46)*tmp6 + (t&
                          &mp42*(tmp42 + tmp556 + tmp82))/tmp702) + (tmp184 + (tmp351 + tmp46 + tmp550)*tmp&
                          &6 + (tmp32*(tmp302 + tmp82 + tmp824))/tmp702 + tmp31*(tmp257 + tmp842))/tmp701 +&
                          & tmp4*(tmp514 + ss*tmp700 + (12*ss + tmp46 + tmp558)/tmp702 + tmp88)) - f1p*((-3&
                          &*tmp31*tmp47 + (-32*ss - 31/tmp19 + tmp46)*tmp6 + (tmp42*(tmp185 - 11/tmp19 + tm&
                          &p46))/tmp702)/tmp701 + tmp181*(tmp7 + 16/tmp702 + tmp82) + tmp4*(tmp198*tmp47 - &
                          &24*tmp6 + (tmp195 + tmp46 + tmp856)/tmp702) + tmp24*(tmp190 - tmp31*tmp47 + (tmp&
                          &185 - 15/tmp19 + tmp46)*tmp50 + (tmp42*(tmp46 + tmp65 + tmp89))/tmp702)) + kappa&
                          &*ss*tmp4*tmp904 + kappa*ss*tmp6*tmp904) + 8*logmut*Pi*tmp3*(tmp259 - f2p*tmp3 + &
                          &f1p*tmp907) + 8*C0tP2P20P0*Pi*tmp3*tmp49*(tmp259*tmp57 - f2p*tmp3*(-tmp56 + (2*(&
                          &tmp7 + 6/tmp702))/tmp701 + (tmp21 + tmp60)/tmp702 + tmp88) + f1p*tmp57*tmp907) +&
                          & 8*logP2t*Pi*tmp19*tmp3*tmp49*(tmp10 + tmp32 + tmp51 + tmp7)*(-((f2p*tmp3)/tmp19&
                          &) + tmp259*tmp9 + f1p*tmp9*tmp907) + logF2L2*Pi*tmp1*tmp17*tmp19*tmp546*tmp701*t&
                          &mp702*((-4*tmp2*tmp697)/tmp702 + (4*f1p*tmp2*tmp697)/tmp702 + f2p*tmp36*(tmp574 &
                          &- (2*tmp189*(tmp1 + tmp48))/(tmp34*tmp702) - 2*tmp4*(8*tmp549*tmp698 + tmp135*(t&
                          &mp25*tmp274*tmp609 + tmp42*(tmp25 + tmp611) - 8*tmp31*tmp698) + tmp64*((32*tmp11&
                          &1 + tmp25)*tmp31 + tmp492*tmp56 + 4*tmp182*tmp698 + tmp274*tmp598*tmp719) + ((tm&
                          &p557 + tmp590 + tmp306*tmp698)*tmp809)/tmp702 + tmp31*tmp56*tmp860 - 2*tmp186*(t&
                          &mp25 + tmp580 + tmp698*tmp89)) + tmp187*(-16*tmp135*(tmp14 + tmp257) + tmp132*(t&
                          &mp111 + tmp42*(tmp1 + tmp619)) + tmp622 + (tmp25*(tmp14 + tmp556 + tmp707) + (tm&
                          &p208 + tmp815)*tmp89)/tmp702) + (2*tmp24*(tmp639 + 4*tmp549*tmp699 + tmp135*(tmp&
                          &658 + tmp198*(tmp512 + tmp673) + tmp311*tmp699) + ss*tmp50*(tmp470 + tmp646 + ss&
                          &*(19*tmp25 + tmp648) + tmp320*tmp699) + ((tmp557 + tmp664 + tmp31*tmp699)*tmp805&
                          &)/tmp702 + tmp186*(tmp642 - 12*ss*tmp699 + tmp904)))/tmp701 + 2*tmp181*(tmp722/t&
                          &mp19 + tmp6*(tmp553/tmp19 + tmp25*(tmp1 + tmp656 + tmp700) + tmp351*(tmp344 + tm&
                          &p849)) + ss*tmp56*tmp860 + tmp184*(tmp788 + tmp898) - (2*tmp805*(tmp811 + tmp1*(&
                          &tmp415 + tmp914)))/tmp702))) + 16*DB0P2LP*Pi*tmp1*tmp19*tmp30*tmp34*tmp49*((-2*t&
                          &mp2*tmp335)/tmp702 + (2*f1p*tmp2*tmp335)/tmp702 + (f2p*tmp36*(-tmp181 - tmp24*(7&
                          &*tmp6 - (2*tmp43)/tmp702 + ss*tmp868) + (9*tmp6 + (tmp302 + tmp65)/tmp702 + tmp8&
                          &*tmp880)/tmp701 + tmp4*(tmp301 + tmp921)))/tmp19) + logF2N2*Pi*tmp19*tmp2*tmp34*&
                          &tmp546*tmp623*(-12*ss*tmp186 + tmp185*tmp187 - 4*tmp189 + tmp186*tmp191 + (tmp13&
                          &5*tmp192)/tmp19 + tmp196 - 24*tmp181*tmp31 + tmp135*tmp311 + 64*tmp135*tmp4 + ss&
                          &*tmp154*tmp4 + 16*tmp182*tmp4 - (18*tmp31*tmp4)/tmp19 + 8*tmp549 - 56*tmp181*tmp&
                          &6 - 20*tmp182*tmp6 - (36*tmp4*tmp6)/tmp19 + tmp188*tmp65 + (28*ss*tmp135)/tmp701&
                          & - (36*tmp186)/tmp701 - (4*tmp188)/tmp701 + (30*tmp135)/(tmp19*tmp701) + (12*tmp&
                          &182)/(tmp19*tmp701) + (tmp351*tmp6)/(tmp19*tmp701) + (tmp6*tmp679)/tmp701 - (28*&
                          &ss*tmp181)/tmp702 + (24*tmp187)/tmp702 + (12*tmp188)/tmp702 + (tmp182*tmp21)/tmp&
                          &702 - (34*tmp4*tmp52)/tmp702 - (4*tmp182)/(tmp701*tmp702) + (14*tmp31)/(tmp19*tm&
                          &p701*tmp702) + (tmp6*tmp783)/tmp19 + (tmp4*tmp783)/tmp702 + tmp181*tmp811 + f2p*&
                          &(tmp196 - 2*tmp4*(tmp194*tmp31 + (5*ss + tmp194)*tmp64 + (ss*(18*ss + tmp195))/t&
                          &mp702) - (2*(tmp198 + 5/tmp702)*(tmp199*tmp31 + tmp6*(tmp42 + tmp65) + (ss*(1/tm&
                          &p19 + tmp32))/tmp702))/tmp701 + 2*tmp181*(ss*tmp556 + (tmp194 + tmp837)/tmp702) &
                          &+ tmp26*(tmp197 + (tmp191 + tmp303)*tmp6 + (tmp37*tmp89)/tmp702)) + f1p*(4*tmp18&
                          &9 + tmp187*(tmp140 + tmp490 + tmp70) + 2*tmp181*(28*tmp6 + tmp302*(tmp42 + tmp7)&
                          & + (tmp191 + tmp192)/tmp702) + (2*(18*tmp186 - tmp135*(15/tmp19 + tmp192) + ss*t&
                          &mp154*tmp43 + 2*tmp182*(ss + tmp65) + (tmp31*(-7/tmp19 + tmp42))/tmp702))/tmp701&
                          & + tmp4*(tmp193 + (tmp183 + tmp194)*tmp306 + (tmp42*(tmp195 + tmp42))/tmp702 + (&
                          &ss + tmp194)*tmp88) - tmp26*(tmp190 + tmp197 + ((tmp198 + tmp7)*tmp89)/tmp702 + &
                          &tmp6*(tmp191 + tmp89))) + (tmp181*tmp926)/tmp702) + C0tP2P2NP0*Pi*tmp19*tmp2*tmp&
                          &34*tmp869*(tmp101 + tmp126 + tmp127 + tmp128 + tmp129 + tmp130 + (f2p*tmp131*tmp&
                          &3)/tmp19 + tmp318 + tmp4 + tmp4*tmp404 + tmp52 - 5*tmp6 + tmp163*tmp6 + tmp519/(&
                          &tmp701*tmp702) + tmp704 - tmp31*tmp77 - tmp4*tmp77 + tmp6*tmp77 - (4*tmp805)/tmp&
                          &701 + (tmp260*tmp805)/tmp701 + (4*tmp805)/tmp702 + tmp806 + tmp551*tmp809 + (tmp&
                          &1*tmp818)/(tmp701*tmp702) + f1p*((5 - tmp1166)*tmp6 + tmp134*tmp76 + tmp4*tmp76 &
                          &+ ((tmp2*tmp519)/tmp702 + ss*(2 + tmp841))/tmp701 + (1/tmp19 + tmp3*tmp60 + tmp8&
                          &53)/tmp702) + tmp928/(tmp19*tmp702)) + C0tF2F2NFL*Pi*tmp1405*tmp17*tmp19*tmp2*((&
                          &f2p*(tmp1 + tmp13 + tmp199)*tmp36)/tmp19 + tmp208*tmp551 - 16*tmp551*tmp6 - (32*&
                          &kappa*tmp6)/tmp701 + f1p*((2*(tmp201 + kappa*tmp310 - (4*(tmp202 + tmp253))/tmp7&
                          &02))/tmp701 + (tmp201 + tmp2*tmp310 - (4*tmp258)/tmp702)/tmp19) - (8*tmp56)/tmp7&
                          &02 - 16/(tmp19*tmp701*tmp702) + (8*tmp551)/(tmp701*tmp702) + tmp769/(tmp19*tmp70&
                          &2) + (16*tmp77)/(tmp701*tmp702) + (tmp56*tmp818)/tmp702 + tmp6*tmp824 + (kappa*t&
                          &mp849)/tmp701 + tmp899 + (tmp1*tmp947)/tmp701) + log4*Pi*tmp117*tmp17*tmp19*tmp2&
                          &*tmp49*tmp546*tmp702*(kappa*ss*tmp1064*tmp135*tmp181 - 24*tmp111*tmp135*tmp182 +&
                          & 64*ss*tmp181*tmp186 - 112*tmp1*tmp181*tmp186 + kappa*ss*tmp1009*tmp181*tmp186 +&
                          & 112*tmp111*tmp181*tmp186 + tmp1115*tmp181*tmp186 + 208*tmp111*tmp182*tmp186 - 4&
                          &16*ss*tmp135*tmp187 + 88*tmp1*tmp135*tmp187 + kappa*tmp1060*tmp135*tmp187 - 352*&
                          &tmp186*tmp187 - 96*tmp1*tmp186*tmp187 - 24*tmp1*tmp135*tmp188 - 72*tmp111*tmp135&
                          &*tmp188 + 160*tmp1*tmp186*tmp188 + 96*tmp135*tmp189 - (144*tmp181*tmp186)/tmp19 &
                          &+ (72*tmp135*tmp187)/tmp19 - (40*tmp135*tmp188)/tmp19 - 16*tmp181*tmp186*tmp25 +&
                          & 80*tmp182*tmp186*tmp25 + 64*tmp135*tmp187*tmp25 - (104*tmp135*tmp181*tmp25)/tmp&
                          &19 + (tmp135*tmp1504*tmp181*tmp25)/tmp19 - (56*tmp135*tmp182*tmp25)/tmp19 + 704*&
                          &tmp135*tmp181*tmp31 + 320*tmp1*tmp135*tmp181*tmp31 + (104*tmp186*tmp25*tmp31)/tm&
                          &p19 + tmp189*tmp369 + tmp1*tmp187*tmp399 + tmp1*tmp188*tmp399 - 576*tmp135*tmp18&
                          &2*tmp4 - 320*tmp1*tmp135*tmp182*tmp4 - 60*tmp111*tmp186*tmp4 + ss*tmp1115*tmp186&
                          &*tmp4 + kappa*ss*tmp1385*tmp186*tmp4 + tmp1470*tmp186*tmp4 + (136*tmp186*tmp25*t&
                          &mp4)/tmp19 - 256*tmp1*tmp135*tmp31*tmp4 - 240*tmp111*tmp135*tmp31*tmp4 + 1024*tm&
                          &p186*tmp31*tmp4 - (352*tmp135*tmp31*tmp4)/tmp19 + tmp193*tmp25*tmp31*tmp4 + tmp1&
                          &*tmp31*tmp373*tmp4 - 80*tmp182*tmp186*tmp46 + 40*tmp135*tmp188*tmp46 + tmp132*tm&
                          &p189*tmp46 - 2*tmp135*tmp181*tmp492 + 10*tmp135*tmp182*tmp492 + 40*tmp181*tmp182&
                          &*tmp492 + 12*ss*tmp189*tmp492 - 30*tmp187*tmp31*tmp492 - 30*tmp188*tmp4*tmp492 +&
                          & tmp135*tmp32*tmp4*tmp492 + 48*tmp135*tmp181*tmp52 - 128*tmp186*tmp4*tmp52 + 448&
                          &*tmp181*tmp549 + 64*tmp1*tmp181*tmp549 - 512*tmp182*tmp549 - 320*tmp1*tmp182*tmp&
                          &549 - 224*tmp1*tmp31*tmp549 - 272*tmp111*tmp31*tmp549 - 120*tmp25*tmp31*tmp549 +&
                          & 640*ss*tmp4*tmp549 + 48*tmp1*tmp4*tmp549 - 208*tmp111*tmp4*tmp549 + tmp1115*tmp&
                          &4*tmp549 + (240*tmp4*tmp549)/tmp19 - 56*tmp25*tmp4*tmp549 + 80*tmp31*tmp46*tmp54&
                          &9 + tmp31*tmp549*tmp550 + tmp1027*tmp181*tmp186*tmp551 - 104*tmp1*tmp182*tmp186*&
                          &tmp551 + 62*tmp135*tmp182*tmp25*tmp551 + 20*tmp100*tmp181*tmp31*tmp551 - 78*tmp1&
                          &86*tmp25*tmp31*tmp551 - 14*tmp100*tmp135*tmp4*tmp551 - 20*tmp100*tmp182*tmp4*tmp&
                          &551 - 54*tmp186*tmp25*tmp4*tmp551 + 120*tmp1*tmp135*tmp31*tmp4*tmp551 + 136*tmp1&
                          &*tmp31*tmp549*tmp551 + 104*tmp1*tmp4*tmp549*tmp551 - 2*tmp492*tmp559 + tmp181*tm&
                          &p190*tmp56 + tmp1060*tmp135*tmp31*tmp56 + tmp1*tmp135*tmp311*tmp56 + tmp181*tmp3&
                          &65*tmp56 + tmp182*tmp365*tmp56 + 12*tmp1*tmp135*tmp4*tmp56 + tmp1060*tmp135*tmp4&
                          &*tmp56 + tmp135*tmp183*tmp4*tmp56 - 48*tmp186*tmp4*tmp56 + 48*tmp1*tmp186*tmp4*t&
                          &mp56 + tmp135*tmp25*tmp31*tmp429*tmp56 + tmp135*tmp25*tmp4*tmp429*tmp56 + 56*ss*&
                          &tmp549*tmp56 + tmp488*tmp549*tmp56 - 18*tmp111*tmp575 + (20*tmp25*tmp575)/tmp19 &
                          &+ 896*tmp31*tmp575 + 320*tmp1*tmp31*tmp575 - 192*tmp4*tmp575 + 64*tmp1*tmp4*tmp5&
                          &75 + tmp492*tmp575 - 104*tmp52*tmp575 - 11*tmp25*tmp551*tmp575 - 20*tmp56*tmp575&
                          & + ss*tmp1057*tmp181*tmp6 - 18*tmp111*tmp187*tmp6 - 24*tmp1*tmp189*tmp6 - (24*tm&
                          &p189*tmp6)/tmp19 - 40*tmp189*tmp25*tmp6 + (36*tmp187*tmp25*tmp6)/tmp19 + tmp188*&
                          &tmp25*tmp274*tmp6 - 176*tmp1*tmp181*tmp31*tmp6 - 80*tmp111*tmp181*tmp31*tmp6 - (&
                          &176*tmp181*tmp31*tmp6)/tmp19 - 144*tmp181*tmp25*tmp31*tmp6 + 144*tmp1*tmp182*tmp&
                          &4*tmp6 + (144*tmp182*tmp4*tmp6)/tmp19 + 88*tmp111*tmp31*tmp4*tmp6 + (120*tmp25*t&
                          &mp31*tmp4*tmp6)/tmp19 + 80*tmp181*tmp31*tmp46*tmp6 - 80*tmp182*tmp4*tmp46*tmp6 -&
                          & 15*tmp188*tmp492*tmp6 - 2*tmp31*tmp4*tmp492*tmp6 + 104*tmp187*tmp52*tmp6 + tmp1&
                          &87*tmp25*tmp551*tmp6 - 23*tmp188*tmp25*tmp551*tmp6 + 40*tmp1*tmp181*tmp31*tmp551&
                          &*tmp6 - 40*tmp1*tmp182*tmp4*tmp551*tmp6 - 66*tmp25*tmp31*tmp4*tmp551*tmp6 + tmp1&
                          &40*tmp181*tmp56*tmp6 + tmp166*tmp181*tmp56*tmp6 + tmp166*tmp182*tmp56*tmp6 + tmp&
                          &1*tmp135*tmp187*tmp619 + tmp1*tmp189*tmp6*tmp619 + tmp182*tmp492*tmp6*tmp619 + s&
                          &s*tmp4*tmp492*tmp6*tmp619 + tmp549*tmp56*tmp623 + tmp135*tmp4*tmp46*tmp625 + tmp&
                          &1*tmp186*tmp56*tmp625 + tmp1*tmp4*tmp56*tmp6*tmp625 + tmp1*tmp186*tmp686 + ss*tm&
                          &p187*tmp492*tmp700 + (208*tmp1*tmp135*tmp182)/tmp701 + (224*tmp111*tmp135*tmp182&
                          &)/tmp701 + (ss*tmp1470*tmp186)/tmp701 - (832*tmp182*tmp186)/tmp701 - (384*tmp1*t&
                          &mp182*tmp186)/tmp701 + (224*tmp135*tmp188)/tmp701 + (160*tmp1*tmp135*tmp188)/tmp&
                          &701 + (272*tmp135*tmp182)/(tmp19*tmp701) + (tmp1037*tmp135*tmp31)/tmp701 - (112*&
                          &tmp1*tmp186*tmp31)/tmp701 - (176*tmp111*tmp186*tmp31)/tmp701 - (80*tmp186*tmp31)&
                          &/(tmp19*tmp701) + (72*tmp135*tmp25*tmp31)/(tmp19*tmp701) - (96*tmp135*tmp182*tmp&
                          &46)/tmp701 - (84*tmp25*tmp549)/(tmp19*tmp701) + (448*tmp31*tmp549)/tmp701 + (192&
                          &*tmp1*tmp31*tmp549)/tmp701 + (80*tmp52*tmp549)/tmp701 - (112*tmp1*tmp135*tmp182*&
                          &tmp551)/tmp701 + (88*tmp1*tmp186*tmp31*tmp551)/tmp701 - (50*tmp135*tmp25*tmp31*t&
                          &mp551)/tmp701 + (41*tmp25*tmp549*tmp551)/tmp701 + (tmp186*tmp492*tmp556)/tmp701 &
                          &+ (tmp188*tmp492*tmp558)/tmp701 - (32*ss*tmp186*tmp56)/tmp701 - (14*tmp1*tmp186*&
                          &tmp56)/tmp701 - (8*tmp135*tmp31*tmp56)/tmp701 + (kappa*tmp31*tmp364*tmp56)/tmp70&
                          &1 - (12*tmp186*tmp46*tmp56)/tmp701 + (56*tmp549*tmp56)/tmp701 + (192*ss*tmp575)/&
                          &tmp701 + (kappa*ss*tmp1009*tmp575)/tmp701 + (kappa*tmp1060*tmp575)/tmp701 + (152&
                          &*tmp111*tmp575)/tmp701 - (216*tmp575)/(tmp19*tmp701) + (56*tmp25*tmp575)/tmp701 &
                          &- (76*tmp1*tmp551*tmp575)/tmp701 - (68*tmp111*tmp182*tmp6)/tmp701 + (tmp1027*tmp&
                          &188*tmp6)/tmp701 - (40*tmp111*tmp188*tmp6)/tmp701 - (56*tmp188*tmp6)/(tmp19*tmp7&
                          &01) + (56*tmp188*tmp25*tmp6)/tmp701 - (48*tmp182*tmp25*tmp6)/(tmp19*tmp701) + (4&
                          &0*tmp188*tmp46*tmp6)/tmp701 + (20*tmp182*tmp492*tmp6)/tmp701 + (68*tmp182*tmp25*&
                          &tmp551*tmp6)/tmp701 - (16*tmp182*tmp56*tmp6)/tmp701 - (16*tmp25*tmp31*tmp56*tmp6&
                          &)/tmp701 + (tmp135*tmp25*tmp605)/tmp701 + (tmp186*tmp46*tmp625)/tmp701 + (tmp135&
                          &*tmp31*tmp642)/tmp701 + (kappa*tmp188*tmp6*tmp642)/tmp701 + (tmp1*tmp135*tmp56*t&
                          &mp674)/tmp701 - (160*tmp181*tmp182*tmp25)/tmp702 + (120*tmp187*tmp25*tmp31)/tmp7&
                          &02 - (40*tmp181*tmp25*tmp31)/(tmp19*tmp702) - (36*tmp111*tmp182*tmp4)/tmp702 + (&
                          &tmp100*tmp1504*tmp182*tmp4)/tmp702 + (120*tmp188*tmp25*tmp4)/tmp702 + (40*tmp182&
                          &*tmp25*tmp4)/(tmp19*tmp702) + (tmp1*tmp171*tmp31*tmp4)/tmp702 + (tmp1037*tmp31*t&
                          &mp4)/(tmp19*tmp702) - (19*ss*tmp187*tmp492)/tmp702 - (9*tmp100*tmp187*tmp551)/tm&
                          &p702 + (tmp100*tmp181*tmp303*tmp551)/tmp702 - (30*tmp181*tmp25*tmp31*tmp551)/tmp&
                          &702 + (30*tmp182*tmp25*tmp4*tmp551)/tmp702 + (8*tmp25*tmp559)/tmp702 + (tmp14*tm&
                          &p187*tmp56)/tmp702 + (tmp14*tmp188*tmp56)/tmp702 + (tmp1060*tmp31*tmp4*tmp56)/tm&
                          &p702 + (tmp25*tmp31*tmp4*tmp429*tmp56)/tmp702 + (tmp181*tmp31*tmp594)/(tmp19*tmp&
                          &702) + (tmp189*tmp623)/(tmp19*tmp702) + (tmp187*tmp56*tmp623)/tmp702 + (tmp188*t&
                          &mp56*tmp623)/tmp702 - (20*tmp188*tmp25)/(tmp19*tmp701*tmp702) - (29*tmp188*tmp49&
                          &2)/(tmp701*tmp702) - (12*tmp100*tmp182*tmp551)/(tmp701*tmp702) - (15*tmp188*tmp2&
                          &5*tmp551)/(tmp701*tmp702) - (12*tmp182*tmp46*tmp56)/(tmp701*tmp702) + 32*kappa*t&
                          &mp135*tmp181*tmp719 + tmp181*tmp193*tmp719 - 16*tmp186*tmp4*tmp719 + (88*tmp135*&
                          &tmp4*tmp719)/tmp19 - (76*tmp549*tmp719)/tmp19 - 38*tmp135*tmp4*tmp551*tmp719 + 4&
                          &7*tmp549*tmp551*tmp719 - 12*kappa*tmp186*tmp56*tmp719 + 72*tmp575*tmp719 + 136*t&
                          &mp187*tmp6*tmp719 - (112*tmp181*tmp6*tmp719)/tmp19 + 20*tmp181*tmp551*tmp6*tmp71&
                          &9 - 16*tmp4*tmp56*tmp6*tmp719 + 12*kappa*tmp4*tmp56*tmp6*tmp719 + (80*tmp186*tmp&
                          &719)/(tmp19*tmp701) - (80*tmp549*tmp719)/tmp701 + (32*kappa*tmp549*tmp719)/tmp70&
                          &1 - (44*tmp186*tmp551*tmp719)/tmp701 - (16*tmp135*tmp56*tmp719)/tmp701 + (12*kap&
                          &pa*tmp135*tmp56*tmp719)/tmp701 - (48*tmp189*tmp719)/tmp702 + (20*tmp187*tmp719)/&
                          &(tmp19*tmp702) + (15*tmp187*tmp551*tmp719)/tmp702 - (12*kappa*tmp181*tmp56*tmp71&
                          &9)/tmp702 + tmp188*tmp722 + (tmp182*tmp722)/tmp19 + tmp4*tmp56*tmp6*tmp726 + tmp&
                          &186*tmp56*tmp731 + (tmp186*tmp25*tmp731)/tmp701 + (tmp181*tmp492*tmp737)/tmp702 &
                          &+ tmp135*tmp56*tmp758 + (tmp182*tmp56*tmp769)/(tmp701*tmp702) + 48*tmp186*tmp187&
                          &*tmp77 - 80*tmp186*tmp188*tmp77 - 16*tmp135*tmp189*tmp77 - 160*tmp135*tmp181*tmp&
                          &31*tmp77 + 160*tmp135*tmp182*tmp4*tmp77 - 96*tmp186*tmp31*tmp4*tmp77 + 160*tmp18&
                          &2*tmp549*tmp77 - 160*tmp31*tmp575*tmp77 - (80*tmp135*tmp188*tmp77)/tmp701 + (tmp&
                          &182*tmp373*tmp77)/tmp701 - (96*tmp31*tmp549*tmp77)/tmp701 - 16*tmp135*tmp181*tmp&
                          &805 + 128*tmp181*tmp186*tmp805 - 160*tmp135*tmp187*tmp805 + 80*kappa*tmp135*tmp1&
                          &87*tmp805 + (96*tmp135*tmp181*tmp805)/tmp19 - 160*tmp186*tmp4*tmp805 + (32*tmp13&
                          &5*tmp4*tmp805)/tmp19 - (144*tmp186*tmp4*tmp805)/tmp19 + (26*tmp549*tmp805)/tmp19&
                          & + 64*tmp4*tmp549*tmp805 - 14*tmp186*tmp56*tmp805 + 32*tmp135*tmp4*tmp56*tmp805 &
                          &+ 120*tmp575*tmp805 + (168*tmp575*tmp805)/tmp19 - 84*tmp551*tmp575*tmp805 + 104*&
                          &tmp187*tmp6*tmp805 + (40*tmp187*tmp6*tmp805)/tmp19 + tmp343*tmp4*tmp6*tmp805 - 2&
                          &0*tmp187*tmp551*tmp6*tmp805 - (20*tmp186*tmp805)/(tmp19*tmp701) - (48*tmp549*tmp&
                          &805)/tmp701 - (160*tmp549*tmp805)/(tmp19*tmp701) + (80*tmp549*tmp551*tmp805)/tmp&
                          &701 + (32*tmp186*tmp56*tmp805)/tmp701 + (tmp190*tmp56*tmp805)/tmp701 + (128*tmp5&
                          &75*tmp805)/tmp701 + (4*tmp181*tmp56*tmp805)/tmp702 + (tmp56*tmp6*tmp806)/tmp701 &
                          &+ (tmp4*tmp6*tmp812)/tmp19 + (tmp100*tmp549*tmp817)/tmp701 + tmp111*tmp135*tmp18&
                          &7*tmp818 + ss*tmp100*tmp181*tmp6*tmp818 + tmp111*tmp189*tmp6*tmp818 - 480*ss*tmp&
                          &820 - 40*tmp111*tmp820 + (72*tmp820)/tmp19 - 16*tmp25*tmp820 + 8*tmp46*tmp820 + &
                          &tmp521*tmp820 + kappa*tmp642*tmp820 - (32*tmp820)/tmp701 - (96*tmp1*tmp820)/tmp7&
                          &01 + (48*tmp77*tmp820)/tmp701 - 160*tmp805*tmp820 + 80*kappa*tmp805*tmp820 + tmp&
                          &181*tmp492*tmp6*tmp824 + tmp56*tmp575*tmp83 + (tmp575*tmp83)/tmp701 + 32*tmp833 &
                          &+ 32*tmp1*tmp833 - 16*tmp77*tmp833 - 32*tmp135*tmp834 + tmp234*tmp834 - 2*tmp100&
                          &*tmp551*tmp834 + tmp1060*tmp6*tmp834 + tmp257*tmp6*tmp834 + tmp519*tmp551*tmp6*t&
                          &mp834 + (12*tmp492*tmp834)/tmp701 + (tmp1*tmp199*tmp834)/tmp702 + (tmp25*tmp274*&
                          &tmp834)/tmp702 + (9*tmp492*tmp834)/tmp702 - (48*tmp25*tmp834)/(tmp701*tmp702) + &
                          &tmp403*tmp77*tmp834 + tmp6*tmp815*tmp834 - 2*tmp492*tmp835 + (8*tmp25*tmp835)/tm&
                          &p702 + (tmp189*tmp842)/(tmp19*tmp702) + tmp46*tmp575*tmp856 + tmp187*tmp46*tmp6*&
                          &tmp856 + ss*tmp549*tmp860 + ss*tmp186*tmp56*tmp869 + tmp182*tmp4*tmp6*tmp869 + t&
                          &mp181*tmp56*tmp6*tmp869 + tmp182*tmp56*tmp6*tmp869 + (tmp186*tmp56*tmp869)/tmp70&
                          &1 + (ss*tmp181*tmp56*tmp869)/tmp702 + (tmp182*tmp56*tmp869)/(tmp701*tmp702) + tm&
                          &p6*tmp834*tmp870 + (tmp186*tmp872)/tmp19 + (tmp187*tmp52*tmp877)/tmp702 + tmp187&
                          &*tmp56*tmp88 + tmp188*tmp56*tmp88 + (tmp135*tmp52*tmp881)/tmp701 + (tmp189*tmp89&
                          &5)/(tmp19*tmp702) + tmp154*tmp187*tmp899 + tmp154*tmp188*tmp899 - 24*tmp186*tmp3&
                          &1*tmp899 + ss*tmp181*tmp310*tmp899 + tmp135*tmp140*tmp4*tmp899 - 24*tmp186*tmp4*&
                          &tmp899 + tmp181*tmp403*tmp899 + tmp182*tmp403*tmp899 + tmp185*tmp549*tmp899 - 4*&
                          &tmp575*tmp899 - 24*tmp31*tmp4*tmp6*tmp899 + (tmp140*tmp186*tmp899)/tmp701 - (16*&
                          &tmp135*tmp31*tmp899)/tmp701 + (tmp182*tmp310*tmp899)/tmp701 + (16*tmp549*tmp899)&
                          &/tmp701 + kappa*tmp549*tmp56*tmp904 + (kappa*tmp187*tmp56*tmp904)/tmp702 + (kapp&
                          &a*tmp188*tmp56*tmp904)/tmp702 + (tmp551*tmp834*tmp904)/tmp702 + tmp549*tmp551*tm&
                          &p915 + (tmp189*tmp923)/tmp702 + (tmp188*tmp923)/(tmp19*tmp702) + kappa*tmp181*tm&
                          &p549*tmp934 + kappa*ss*tmp4*tmp549*tmp934 + tmp135*tmp181*tmp56*tmp934 + tmp135*&
                          &tmp182*tmp56*tmp934 + ss*tmp549*tmp56*tmp934 + kappa*tmp4*tmp575*tmp934 + ss*tmp&
                          &181*tmp56*tmp6*tmp934 + (tmp549*tmp56*tmp934)/tmp701 + (tmp182*tmp56*tmp6*tmp934&
                          &)/tmp701 + tmp135*tmp834*tmp934 + (tmp1*tmp549*tmp935)/tmp701 + (tmp135*tmp181*t&
                          &mp944)/tmp19 + tmp135*tmp188*tmp551*tmp944 + ss*tmp100*tmp186*tmp947 + tmp100*tm&
                          &p189*tmp947 + tmp100*tmp135*tmp31*tmp947 + (tmp188*tmp956)/(tmp19*tmp701*tmp702)&
                          & + (tmp188*tmp6*tmp962)/tmp19 + tmp100*tmp187*tmp6*tmp968 + f1p*(tmp559*((-8*tmp&
                          &25)/tmp702 + kappa*tmp919) - (tmp24*(tmp742*tmp855 + tmp186*((54 + (-84 + 41*kap&
                          &pa)*tmp1)*tmp111 + tmp100*tmp817 + 8*tmp56*(7 + tmp845) - 16*tmp31*(-38 + tmp879&
                          &) + ((-5 + tmp239)*tmp404 + (-34 + tmp75)/tmp19)*tmp89) + ss*tmp6*((tmp404*(tmp4&
                          &6 + tmp65))/tmp19 + ss*((54*tmp1 + (68 - 53*kappa)*tmp25)/tmp19 - 16*tmp56*tmp76&
                          & + tmp860) + tmp31*(56/tmp19 - 8*tmp25*(1 + tmp263) + tmp864 + tmp908)) + tmp135&
                          &*(16*tmp182*(-14 + tmp857) + (tmp404*(tmp522 + (-7 + tmp1*(8 + tmp858))/tmp19))/&
                          &tmp19 + ss*(34*tmp111 + 24*tmp56 + tmp471*tmp823 + tmp859) + tmp320*(-54/tmp19 +&
                          & (-9 + tmp282)*tmp465 + 23*tmp836 + tmp912)) - 4*tmp549*(tmp166 + (-7 + tmp451)*&
                          &tmp465 + 4*tmp74*tmp805 + 19*tmp836 + tmp856 + tmp935) - 2*tmp182*tmp492*tmp941 &
                          &+ (tmp1*tmp31*(tmp1*tmp625 + tmp21*(tmp46 + (-2 + tmp1*tmp865)/tmp19) + ss*((-14&
                          & + tmp863)/tmp19 + tmp25*tmp948)))/tmp702))/tmp701 + tmp187*(tmp186*(352 + tmp87&
                          &1) + (tmp1*(-120*tmp1*tmp31 + (1/tmp19 + 9*tmp46 + tmp853)/tmp19 + tmp52*(26 - 5&
                          &*tmp873) + tmp719*tmp886))/tmp702 + tmp492*tmp837*tmp921 + tmp6*((-18 + (36 + ka&
                          &ppa)*tmp1)*tmp118 + tmp866 + (-26/tmp19 + (-17 + tmp263)*tmp465 + tmp857/tmp19 +&
                          & tmp877)*tmp89 + tmp923) - 4*tmp135*((18 + tmp75)/tmp19 + tmp25*tmp858 + tmp869 &
                          &+ (-26 + tmp857)*tmp89 + tmp949)) + 2*tmp181*(-10*tmp31*tmp43*tmp492 + 16*tmp549&
                          &*(-14 + tmp75) + (tmp805*(tmp861 + ss*(tmp842 + (-22 + tmp863)/tmp19) + tmp21*(t&
                          &mp862 + tmp876/tmp19)))/tmp702 + tmp6*(tmp31*(88*tmp1 + 88/tmp19 + 8*tmp25*(9 + &
                          &tmp817) + tmp864) + tmp32*(tmp111*(-3 + tmp1*(-28 + tmp263)) + tmp492 + tmp866) &
                          &+ tmp111*(1/tmp19 + tmp870 + tmp883)) + tmp135*(tmp492 + tmp351*(tmp1 + tmp70*tm&
                          &p76 + tmp74*tmp849) + tmp679*(-22 + tmp857) - 4*tmp56*tmp887 + (-18*tmp1 - 13*tm&
                          &p25*tmp894)/tmp19) + 4*tmp186*(tmp351*tmp76 + tmp2*tmp849 + tmp879/tmp19 + tmp92&
                          &6 + tmp956)) + tmp63*(tmp742*(6 + tmp75) + tmp1223*tmp182*tmp880 + ss*tmp6*(ss*(&
                          &(-44 + (-60 + 33*kappa)*tmp1)*tmp111 + tmp492 + 12*tmp56*tmp76) + tmp320*((-9 + &
                          &tmp1*(-1 + tmp263))*tmp404 + (-18 + tmp857)/tmp19) + tmp118*(1/tmp19 + kappa*tmp&
                          &623 + tmp883)) + 4*tmp549*(tmp163 + tmp226 + tmp555 + (-30 - 13*tmp75)/tmp19 + (&
                          &-20 + tmp75)*tmp89) + tmp135*(tmp182*(288 - 80*tmp75) + tmp111*(tmp884 + tmp65*t&
                          &mp885) + ss*(tmp492 + tmp111*(-16 + tmp1*(-44 + tmp886)) + 4*tmp56*tmp887) + tmp&
                          &783*((-44 + 15*tmp75)/tmp19 + tmp901 + tmp934)) + (tmp1*tmp31*(-60*tmp1*tmp31 + &
                          &tmp70*(tmp47 + tmp853) + tmp8*((-18 + tmp863)/tmp19 + tmp955)))/tmp702 + tmp186*&
                          &(ss*(80*tmp1 - 8*tmp2*tmp25 + (64 - 36*tmp75)/tmp19) + tmp679*(-32 + tmp854) + t&
                          &mp881 + 12*tmp56*tmp882 + tmp111*(30 + tmp1*(-68 + tmp957)))) + tmp26*(tmp742*tm&
                          &p867 + tmp549*(tmp83 + tmp48*(18 + tmp857) + tmp869 + tmp870 + ss*(416 + tmp871)&
                          &) + (tmp14*tmp31*(ss*tmp1395 + tmp872 + tmp52*(-2 + tmp873) + tmp56*tmp876 + tmp&
                          &551*tmp904))/tmp702 + tmp186*(tmp111*(18 + tmp1*(-20 + tmp266)) + 4*tmp56*(5 + t&
                          &mp75) + tmp679*tmp855 + (tmp700 + 11*tmp836 + tmp877 + tmp465*tmp878)*tmp89 + ka&
                          &ppa*tmp915) + kappa*tmp182*tmp868*tmp919 + ss*tmp6*(tmp320*(tmp166 + tmp199 + 2*&
                          &tmp494 + tmp836) + tmp404*tmp56*tmp885 + ss*(tmp859 + tmp866 + tmp111*(-6 + tmp1&
                          &*(-12 + tmp948)))) + tmp135*(tmp111*(tmp46 + (-5 + tmp1*tmp852)/tmp19) - 16*tmp1&
                          &82*tmp867 + tmp783*(tmp166 + (-6 + tmp879)/tmp19 + tmp896) + ss*(tmp492 - 8*tmp5&
                          &6*tmp882 + ((36 - 25*kappa)*tmp25 + tmp962)/tmp19))) + tmp189*(tmp403*(-6 + tmp7&
                          &5) + tmp6*(24*tmp1 - 8*tmp25*tmp826 - 4*tmp836 + tmp851) - 2*tmp492*tmp918 + (48&
                          &*tmp719 + tmp111*(-6 + tmp873) + tmp100*tmp968)/tmp702)) + f2p*((tmp189*tmp465*(&
                          &(-2*tmp493)/tmp702 + tmp77))/tmp19 - 2*tmp181*(tmp31*tmp492*tmp700 + 16*tmp186*(&
                          &tmp837 + tmp839) - 4*tmp135*(tmp42*(1/tmp19 + tmp488 + tmp512) + (tmp208*(7 + tm&
                          &p451) + tmp488 + tmp839)/tmp19) + (tmp6*((tmp1 + 8*tmp1069 + tmp219*tmp274)*tmp4&
                          &2 - 8*tmp492 + tmp847))/tmp19 + (tmp21*tmp805*(1/tmp19 + tmp836 + tmp840 + tmp86&
                          &2))/tmp702) + (tmp187*(-16*tmp135*tmp219 - 10*ss*tmp492 + (tmp1*(1/tmp19 + kappa&
                          &*tmp744 + tmp836 + tmp843))/tmp702 + (tmp14 + tmp229 + tmp25*(6 + tmp818))*tmp88&
                          &))/tmp19 + (2*tmp24*((tmp100*tmp182*tmp817)/tmp19 + 16*tmp549*((-11 + tmp404)/tm&
                          &p19 + tmp837) + tmp135*(tmp304*(11*tmp1 - tmp494 + tmp556) - 8*tmp31*(tmp510 + (&
                          &-1 + tmp769)/tmp19) + tmp111*(tmp522 + (-11 + tmp841)/tmp19)) + (tmp111*tmp31*(t&
                          &mp46 + tmp840 + tmp21*tmp844))/tmp702 + tmp52*tmp6*(tmp489 + tmp492 + tmp32*(tmp&
                          &1 - 4*tmp229 + tmp25*tmp878)) + 2*tmp186*((tmp141 + tmp488)*tmp60 + tmp625 - ((-&
                          &22 + tmp769)/tmp19 + tmp25*tmp916 + tmp954)/tmp19)))/tmp701 - tmp26*(16*tmp549*(&
                          &tmp303 + tmp850/tmp19) + tmp304*tmp6*(tmp202*tmp25 + tmp229*tmp32 + tmp512 + tmp&
                          &513*tmp89) + tmp135*(tmp111*(tmp46 - (7 + tmp75)/tmp19) + ss*tmp274*(tmp14*(3 + &
                          &tmp239) + tmp21*tmp848) + tmp679*(tmp14 + tmp900/tmp19)) + tmp100*tmp182*tmp947 &
                          &+ 4*tmp186*(tmp679 + (tmp708 + tmp849 - tmp850/tmp19)/tmp19 + tmp60*(tmp488 + tm&
                          &p952/tmp19)) + (tmp118*tmp31*(1/tmp19 + tmp522 + tmp836 + tmp89*tmp958))/tmp702)&
                          & + tmp63*(tmp182*tmp492*tmp700 + (tmp111*tmp31*(tmp842 + tmp843 + tmp70*tmp844))&
                          &/tmp702 + 16*tmp549*(tmp837 + tmp65*tmp848) + tmp52*tmp6*((tmp1 + tmp117 + 2*tmp&
                          &229)*tmp302 + tmp847 + tmp881) - tmp135*((tmp175 + 1/tmp19 + tmp510)*tmp726 + tm&
                          &p111*(tmp65*(4 + tmp75) + tmp884) + ss*tmp199*((5 + kappa)*tmp465 + tmp491 + tmp&
                          &21*tmp925)) + 4*tmp186*(72*tmp31 + tmp32*(tmp488 + (-5 + tmp769)/tmp19) + (tmp70&
                          &*tmp848 + tmp404*(-6 + tmp1*tmp971))/tmp19)))) + logP2L2*Pi*tmp1*tmp17*tmp19*tmp&
                          &49*tmp546*tmp701*tmp702*(4*tmp1515*tmp2 - 4*f1p*tmp1515*tmp2 + f2p*tmp36*((tmp1*&
                          &tmp566*tmp573)/(tmp49*tmp702) - 2*tmp189*(tmp25*tmp274*(1/tmp19 + ss*(5 + tmp2*t&
                          &mp491)) + tmp403*(tmp14 + (4 + tmp895)/tmp19) + tmp88*(40*ss*tmp242 + tmp25 + tm&
                          &p111*(-7 + tmp404*(-7 + tmp818)) + tmp905) + (tmp111*(-18*tmp2*tmp25 + tmp510 + &
                          &tmp550 + 40*ss*tmp888 + tmp906))/tmp702) + tmp187*(64*tmp186*(tmp14 + tmp21*tmp8&
                          &97) + (tmp465*(-40*tmp31*tmp889 + tmp898 + tmp899))/tmp19 + tmp403*(tmp89*(tmp14&
                          & + tmp274*tmp900) + (31*tmp1 + tmp257*tmp891 + tmp901)/tmp19) + tmp88*(tmp100 + &
                          &160*tmp242*tmp31 + ((9 + 8*tmp223)*tmp849)/tmp19 + tmp89*(tmp25 + tmp111*(17 + (&
                          &-3 + tmp260)*tmp769) + tmp892) + tmp166*tmp56*(35 + tmp404*tmp902)) + (tmp111*(t&
                          &mp14*(tmp1 + tmp632 + (-38 + kappa*tmp83)/tmp19) + 160*tmp31*tmp888 + tmp89*(-20&
                          &*tmp2*tmp25 + tmp629 + tmp274*tmp913)))/tmp702) + tmp257*tmp559*(tmp25 + (tmp404&
                          &*tmp888)/tmp702 + tmp132*tmp90 + tmp2*tmp915) + 2*tmp181*((64*(14 + tmp1)*tmp549&
                          &)/tmp19 + (tmp719*(-24*tmp52 + 40*tmp31*tmp889 + tmp911))/tmp19 + 16*tmp186*(tmp&
                          &25 + ss*(10 + tmp1)*tmp274 + tmp171*tmp891 + (tmp226 + tmp25 + tmp912)/tmp19) + &
                          &(tmp199*tmp805*(40*tmp31*tmp888 + tmp302*(tmp163 + tmp25 + tmp82 + tmp913/tmp19)&
                          & + tmp1*(tmp166 + (9 + tmp1*tmp858)/tmp19 + tmp914)))/tmp702 + tmp184*((tmp25*(2&
                          &3 + tmp2*tmp708))/tmp19 + (tmp625*tmp903)/tmp19 + tmp60*(tmp623 + tmp111*(22 + t&
                          &mp77) + tmp905) + tmp915 + tmp1*tmp56*(58 + tmp1*tmp916)) + tmp50*(320*tmp182*tm&
                          &p242 + (tmp25*(tmp14 + 34/tmp19 + tmp690))/tmp19 + (tmp625*(tmp1*(12 + tmp1) + t&
                          &mp21*tmp891))/tmp19 + tmp183*(-2*tmp100 + tmp1*(17 + tmp1*(3 + tmp263))*tmp56 + &
                          &(tmp465*tmp917)/tmp19))) - 2*tmp4*((tmp14 + (22 + tmp510)/tmp19)*tmp576 + 2*tmp1&
                          &86*(tmp89*(tmp208 + tmp111*(63 + tmp769) + tmp892) - 16*tmp31*(tmp14 + (28 + tmp&
                          &895)/tmp19) + tmp111*(45*tmp1 + (88 - 12*tmp223)/tmp19 + tmp896)) - 8*tmp549*(67&
                          &*tmp111 + tmp1069/tmp19 + tmp208 - 4*tmp805 + tmp892 + tmp898*tmp903) + (tmp25*t&
                          &mp31*(tmp626 + 20*tmp31*tmp889 + tmp911))/tmp19 + (tmp805*(tmp1346*tmp56 + tmp31&
                          &*(78*tmp1 + 128/tmp19 + tmp1060*tmp2 + tmp239*tmp619) - 40*tmp182*tmp888 + tmp48&
                          &*tmp805*(-5 + 3*tmp551 + tmp946)))/(tmp19*tmp702) - 2*tmp6*(80*tmp188*tmp242 + t&
                          &mp111*tmp31*(35*tmp1 + (72 + (-3 + tmp282)*tmp519)/tmp19) + 4*tmp182*(79*tmp111 &
                          &+ tmp208 + ((20 - 8*kappa)*tmp25)/tmp19 + tmp892) + kappa*tmp161*tmp915 + ss*tmp&
                          &56*tmp849*(6 + tmp910 + tmp947)) + tmp305*(16*tmp182*(tmp14 + tmp274*(10 + tmp51&
                          &0)) + tmp465*tmp56*(-7 + tmp551 + tmp893) + tmp320*(63*tmp111 + tmp208 + tmp892 &
                          &+ (tmp849*tmp894)/tmp19) + (tmp809*(39*tmp1 + 80/tmp19 + tmp117*tmp2 + (12*tmp95&
                          &8)/tmp19))/tmp19)) + (2*tmp24*((tmp25*tmp31*(tmp149 + tmp31*(4 + tmp2*tmp519) + &
                          &tmp899))/tmp19 + tmp742*(tmp14 + tmp21*tmp903) - 4*tmp549*(tmp111*(47 + tmp404) &
                          &+ ((5 + tmp1)*tmp257 + tmp273)*tmp89 + tmp904 + tmp905) + (tmp805*(tmp557 + 2*(-&
                          &((7 + tmp253)/tmp19) + tmp404 + tmp662)*tmp805 + tmp758*tmp888 + tmp31*(-23*tmp1&
                          & + 10*tmp2*tmp25 + tmp550 + tmp906)))/(tmp19*tmp702) - tmp135*(16*tmp182*(tmp14 &
                          &+ tmp257*tmp478) + (tmp25*(tmp1 + tmp558 + tmp656))/tmp19 + ss*(-12*tmp100 + (64&
                          & + (9 - 19*kappa)*tmp1)*tmp404*tmp56 + (tmp25*(57 + tmp907))/tmp19) + tmp320*(10&
                          &1*tmp111 - 19*tmp25 + tmp238*tmp25*tmp554 + tmp909)) + ss*tmp6*(ss*(-16*tmp100 +&
                          & (-6*tmp100*tmp2 + 37*tmp25)/tmp19 + (52 + (9 - 11*kappa)*tmp1)*tmp404*tmp56) + &
                          &tmp242*tmp794 + tmp320*(47*tmp111 + tmp208 + (tmp25*(14 + tmp858))/tmp19 + tmp90&
                          &5) + tmp471*(tmp510 + tmp707 + tmp274*(-5 + tmp910))) + tmp186*(4*tmp100 + tmp62&
                          &5*(tmp14 + tmp274*tmp891) + (tmp25*(39 + tmp907))/tmp19 + tmp89*(109*tmp111 - 15&
                          &*tmp25 + (tmp826*tmp849)/tmp19 + tmp909) + tmp56*(tmp908 + tmp25*(6 + tmp974))))&
                          &)/tmp701)) + (logF2P2*Pi*tmp17*tmp19*tmp546*tmp701*tmp702*((-4*tmp2*tmp23*tmp804&
                          &*tmp805)/tmp702 + (4*f1p*tmp2*tmp23*tmp804*tmp805)/tmp702 + f2p*((tmp36*tmp566*t&
                          &mp573*tmp719)/tmp702 + (4*tmp559*tmp712*(tmp21 + tmp805))/tmp702 - (2*tmp189*((t&
                          &mp21*(-6 + tmp223) + tmp46 + tmp524)*tmp805 + tmp132*(tmp558 + 3*tmp805) + (2*(2&
                          &0*tmp1*tmp31 - 5*tmp719 + tmp814 + ss*tmp851))/tmp702))/(tmp34*tmp702) + tmp187*&
                          &(tmp669*(tmp558 + tmp805) + ss*tmp343*tmp807*tmp808 + 128*tmp186*(tmp149 + tmp72&
                          &7 + tmp806 + tmp810) + tmp190*(tmp148*(tmp25*(-8 + tmp263) + tmp274) + ss*(tmp10&
                          &0*tmp352 + ((-8 + tmp75)*tmp769)/tmp19) + tmp812 + tmp816) + (tmp719*(40*tmp1*tm&
                          &p31 + tmp46*(tmp14 + tmp199 + 8*tmp2*tmp56) + tmp60*(tmp46 + tmp274*tmp889)))/tm&
                          &p702 + tmp154*tmp805*(tmp242*tmp519 + ((7 - 4*kappa)*tmp25 + (-2 + tmp223)*tmp27&
                          &4)*tmp60 + tmp646 + tmp861 + tmp915)) + (2*tmp24*(-32*tmp575*(tmp118 + ss*(tmp25&
                          & + tmp619) + tmp31*tmp769) + tmp182*tmp56*tmp807*tmp808 + 32*(tmp199 + tmp805)*t&
                          &mp820 + ss*tmp135*(32*tmp1*tmp188 - 16*tmp182*(tmp25*(-7 + tmp260) + tmp274) + s&
                          &s*(((-12 + tmp223)*tmp465)/tmp19 + 32*tmp492*tmp56 + 3*(4 + tmp263)*tmp59) + tmp&
                          &31*(4*tmp100*(14 + tmp266) + tmp1*tmp619*(16 + tmp831)) + tmp1294*tmp832) + 2*tm&
                          &p549*(tmp821 + tmp822 + ss*(tmp100*(-5 + tmp260) + (tmp769*(-16 + tmp831))/tmp19&
                          &) - 8*tmp31*((3 - 10*kappa)*tmp25 + tmp851)) + tmp1*tmp31*tmp50*(16*tmp1*tmp182 &
                          &+ tmp783*(tmp25*tmp826 + tmp21*tmp827) + tmp492*(tmp510 + 4*tmp56*tmp828) + ss*(&
                          &tmp759 + (tmp404*(-8 + tmp830))/tmp19 + tmp100*(16 + tmp886))) + (tmp25*tmp31*(t&
                          &mp182*tmp404 + tmp260*tmp719*(tmp2*tmp343 + tmp767) + tmp825 + tmp31*(tmp46 + tm&
                          &p199*tmp917)))/tmp702 + ss*tmp186*(-128*tmp1*tmp182 - 16*tmp492*tmp56 + tmp59*tm&
                          &p823 + tmp679*((1 + 8*kappa)*tmp208 + tmp824) + (tmp465*(8 + tmp830))/tmp19 + tm&
                          &p60*(((-24 + tmp223)*tmp404)/tmp19 + tmp100*(7 + tmp953)))))/tmp701 + tmp63*(tmp&
                          &182*tmp330*tmp807*tmp808 + tmp186*tmp32*(192*tmp1*tmp182 - 32*(tmp238*tmp25 + tm&
                          &p274)*tmp31 + ((12 + tmp1 + tmp35)*tmp465)/tmp19 + tmp100*tmp632 + 12*ss*(tmp100&
                          &*tmp358 + tmp815) + tmp819) + 32*(tmp558 - 3*tmp805)*tmp820 + ss*tmp305*(80*tmp1&
                          &*tmp188 - 2*tmp1294*tmp350 - 2*tmp31*(tmp100*(-9 + tmp260) + ((8 + tmp223)*tmp76&
                          &9)/tmp19) + ss*((12*tmp25)/tmp19 + tmp100*tmp711 + tmp819) + tmp182*(-96/tmp19 +&
                          & tmp823*tmp869)) + tmp742*(tmp643 + 5*tmp719 + tmp814 + tmp872) + 2*tmp549*(tmp8&
                          &16 + tmp679*(12/tmp19 + tmp25*(2 + tmp817)) + tmp821 + ss*((24 + tmp1 + tmp35)*t&
                          &mp815 + (7 + tmp818)*tmp915)) - 2*tmp6*tmp805*(40*tmp1*tmp188 + tmp56*tmp59*tmp8&
                          &07 - 4*tmp182*((3 + tmp223)*tmp274 + tmp208*(1 + tmp818)) + tmp31*(tmp492 + 24*t&
                          &mp46*tmp56 + tmp1*tmp199*tmp827) + kappa*ss*tmp56*tmp828*tmp919) + (tmp25*tmp31*&
                          &(12*kappa*tmp2*tmp56*tmp719 + tmp825 + tmp31*(tmp294 + tmp48*(3 + tmp907)) + tmp&
                          &182*tmp962))/tmp702) - 2*tmp181*(tmp31*tmp330*tmp807*tmp808 + 64*tmp575*(tmp558 &
                          &+ tmp809) + tmp6*tmp805*(-160*tmp1*tmp182 + tmp492*tmp832 + tmp31*((4 + tmp813)*&
                          &tmp824 + 12*tmp25*(5 + tmp858)) + 4*tmp805*(4*tmp494 - 12*tmp899 + (-2 + tmp1*tm&
                          &p902)/tmp19)) + ss*tmp184*(tmp493*tmp59 + tmp812 + tmp56*tmp881 - 8*tmp31*(tmp27&
                          &4 + tmp346*tmp904) + ss*(((4 + tmp239)*tmp769)/tmp19 + (7 + 10*kappa)*tmp915) + &
                          &(tmp465*(-3 + tmp958))/tmp19) - 32*tmp549*(tmp806 + tmp810 + tmp811 + tmp962/tmp&
                          &19) + (tmp31*tmp465*(tmp46*(tmp166 + 6*tmp2*tmp56 + tmp7) + ss*tmp199*(2 + tmp81&
                          &3) + tmp31*tmp962))/tmp702 - 4*tmp186*(48*tmp1*tmp182 + tmp148*(tmp274 + tmp25*(&
                          &4 + tmp817)) + ss*(5*tmp100 + tmp815*tmp897) + tmp985/tmp19)))))/ss + logmuL2*Pi&
                          &*tmp17*tmp19*tmp83*((f2p*(tmp219*tmp310 + tmp46*(-1 + tmp77) + (tmp769*(tmp493 +&
                          & tmp893))/tmp702))/tmp19 + (4*tmp2*tmp99)/tmp702 - (4*f1p*tmp2*tmp99)/tmp702)

  END FUNCTION MP2MPL_MP_FIN_D12D21

  FUNCTION MP2MPL_MP_FIN_D10D22(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_FIN_d10d22
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206, tmp207, tmp208, tmp209, tmp210
  real tmp211, tmp212, tmp213, tmp214, tmp215
  real tmp216, tmp217, tmp218, tmp219, tmp220
  real tmp221, tmp222, tmp223, tmp224, tmp225
  real tmp226, tmp227, tmp228, tmp229, tmp230
  real tmp231, tmp232, tmp233, tmp234, tmp235
  real tmp236, tmp237, tmp238, tmp239, tmp240
  real tmp241, tmp242, tmp243, tmp244, tmp245
  real tmp246, tmp247, tmp248, tmp249, tmp250
  real tmp251, tmp252, tmp253, tmp254, tmp255
  real tmp256, tmp257, tmp258, tmp259, tmp260
  real tmp261, tmp262, tmp263, tmp264, tmp265
  real tmp266, tmp267, tmp268, tmp269, tmp270
  real tmp271, tmp272, tmp273, tmp274, tmp275
  real tmp276, tmp277, tmp278, tmp279, tmp280
  real tmp281, tmp282, tmp283, tmp284, tmp285
  real tmp286, tmp287, tmp288, tmp289, tmp290
  real tmp291, tmp292, tmp293, tmp294, tmp295
  real tmp296, tmp297, tmp298, tmp299, tmp300
  real tmp301, tmp302, tmp303, tmp304, tmp305
  real tmp306, tmp307, tmp308, tmp309, tmp310
  real tmp311, tmp312, tmp313, tmp314, tmp315
  real tmp316, tmp317, tmp318, tmp319, tmp320
  real tmp321, tmp322, tmp323, tmp324, tmp325
  real tmp326, tmp327, tmp328, tmp329, tmp330
  real tmp331, tmp332, tmp333, tmp334, tmp335
  real tmp336, tmp337, tmp338, tmp339, tmp340
  real tmp341, tmp342, tmp343, tmp344, tmp345
  real tmp346, tmp347, tmp348, tmp349, tmp350
  real tmp351, tmp352, tmp353, tmp354, tmp355
  real tmp356, tmp357, tmp358, tmp359, tmp360
  real tmp361, tmp362, tmp363, tmp364, tmp365
  real tmp366, tmp367, tmp368, tmp369, tmp370
  real tmp371, tmp372, tmp373, tmp374, tmp375
  real tmp376, tmp377, tmp378, tmp379, tmp380
  real tmp381, tmp382, tmp383, tmp384, tmp385
  real tmp386, tmp387, tmp388, tmp389, tmp390
  real tmp391, tmp392, tmp393, tmp394, tmp395
  real tmp396, tmp397, tmp398, tmp399, tmp400
  real tmp401, tmp402, tmp403, tmp404, tmp405
  real tmp406, tmp407, tmp408, tmp409, tmp410
  real tmp411, tmp412, tmp413, tmp414, tmp415
  real tmp416, tmp417, tmp418, tmp419, tmp420
  real tmp421, tmp422, tmp423, tmp424, tmp425
  real tmp426, tmp427, tmp428, tmp429, tmp430
  real tmp431, tmp432, tmp433, tmp434, tmp435
  real tmp436, tmp437, tmp438, tmp439, tmp440
  real tmp441, tmp442, tmp443, tmp444, tmp445
  real tmp446, tmp447, tmp448, tmp449, tmp450
  real tmp451, tmp452, tmp453, tmp454, tmp455
  real tmp456, tmp457, tmp458, tmp459, tmp460
  real tmp461, tmp462, tmp463, tmp464, tmp465
  real tmp466, tmp467, tmp468, tmp469, tmp470
  real tmp471, tmp472, tmp473, tmp474, tmp475
  real tmp476, tmp477, tmp478, tmp479, tmp480
  real tmp481, tmp482, tmp483, tmp484, tmp485
  real tmp486, tmp487, tmp488, tmp489, tmp490
  real tmp491, tmp492, tmp493, tmp494, tmp495
  real tmp496, tmp497, tmp498, tmp499, tmp500
  real tmp501, tmp502, tmp503, tmp504, tmp505
  real tmp506, tmp507, tmp508, tmp509, tmp510
  real tmp511, tmp512, tmp513, tmp514, tmp515
  real tmp516, tmp517, tmp518, tmp519, tmp520
  real tmp521, tmp522, tmp523, tmp524, tmp525
  real tmp526, tmp527, tmp528, tmp529, tmp530
  real tmp531, tmp532, tmp533, tmp534, tmp535
  real tmp536, tmp537, tmp538, tmp539, tmp540
  real tmp541, tmp542, tmp543, tmp544, tmp545
  real tmp546, tmp547, tmp548, tmp549, tmp550
  real tmp551, tmp552, tmp553, tmp554, tmp555
  real tmp556, tmp557, tmp558, tmp559, tmp560
  real tmp561, tmp562, tmp563, tmp564, tmp565
  real tmp566, tmp567, tmp568, tmp569, tmp570
  real tmp571, tmp572, tmp573, tmp574, tmp575
  real tmp576, tmp577, tmp578, tmp579, tmp580
  real tmp581, tmp582, tmp583, tmp584, tmp585
  real tmp586, tmp587, tmp588, tmp589, tmp590
  real tmp591, tmp592, tmp593, tmp594, tmp595
  real tmp596, tmp597, tmp598, tmp599, tmp600
  real tmp601, tmp602, tmp603, tmp604, tmp605
  real tmp606, tmp607, tmp608, tmp609, tmp610
  real tmp611, tmp612, tmp613, tmp614, tmp615
  real tmp616, tmp617, tmp618, tmp619, tmp620
  real tmp621, tmp622, tmp623, tmp624, tmp625
  real tmp626, tmp627, tmp628, tmp629, tmp630
  real tmp631, tmp632, tmp633, tmp634, tmp635
  real tmp636, tmp637, tmp638, tmp639, tmp640
  real tmp641, tmp642, tmp643, tmp644, tmp645
  real tmp646, tmp647, tmp648, tmp649, tmp650
  real tmp651, tmp652, tmp653, tmp654, tmp655
  real tmp656, tmp657, tmp658, tmp659, tmp660
  real tmp661, tmp662, tmp663, tmp664, tmp665
  real tmp666, tmp667, tmp668, tmp669, tmp670
  real tmp671, tmp672, tmp673, tmp674, tmp675
  real tmp676, tmp677, tmp678, tmp679, tmp680
  real tmp681, tmp682, tmp683, tmp684, tmp685
  real tmp686, tmp687, tmp688, tmp689, tmp690
  real tmp691, tmp692, tmp693, tmp694, tmp695
  real tmp696, tmp697, tmp698, tmp699, tmp700
  real tmp701, tmp702, tmp703, tmp704, tmp705
  real tmp706, tmp707, tmp708, tmp709, tmp710
  real tmp711, tmp712, tmp713, tmp714, tmp715
  real tmp716, tmp717, tmp718, tmp719, tmp720
  real tmp721, tmp722, tmp723, tmp724, tmp725
  real tmp726, tmp727, tmp728, tmp729, tmp730
  real tmp731, tmp732, tmp733, tmp734, tmp735
  real tmp736, tmp737, tmp738, tmp739, tmp740
  real tmp741

  tmp1 = 2*ss
  tmp2 = tmp1 + tt
  tmp3 = mm2**2
  tmp4 = -1 + kappa
  tmp5 = 1/mm2
  tmp6 = 1/tt
  tmp7 = sqrt(lambda)**2
  tmp8 = 2/tmp5
  tmp9 = me2**2
  tmp10 = -4*ss
  tmp11 = sqrt(lambda)**4
  tmp12 = 16*tmp3
  tmp13 = -4/tmp5
  tmp14 = tmp13 + tmp7
  tmp15 = ss*tmp2
  tmp16 = 4*ss
  tmp17 = tmp14**(-2)
  tmp18 = tmp6**(-2)
  tmp19 = ss**2
  tmp20 = tmp5**(-4)
  tmp21 = tmp6**(-3)
  tmp22 = tmp11 + tmp12
  tmp23 = 1/tmp17
  tmp24 = 4/tmp5
  tmp25 = -ss
  tmp26 = tmp25 + 1/tmp5
  tmp27 = me2**3
  tmp28 = 5*ss
  tmp29 = tmp5**(-3)
  tmp30 = 10*ss
  tmp31 = 3/tmp6
  tmp32 = -(1/tmp6)
  tmp33 = -17/tmp6
  tmp34 = ss**3
  tmp35 = 2/tmp6
  tmp36 = 8*ss
  tmp37 = tmp4**2
  tmp38 = 6*ss
  tmp39 = tmp24 + tmp32 + tmp7
  tmp40 = -tmp7
  tmp41 = 12*ss
  tmp42 = 5/tmp6
  tmp43 = -tmp18
  tmp44 = 3*ss
  tmp45 = tmp40 + 1/tmp6
  tmp46 = sqrt(lambda)**6
  tmp47 = 24*tmp20
  tmp48 = -2*tmp27*tmp39
  tmp49 = 6/tmp5
  tmp50 = tmp38 + tmp49 + tmp7
  tmp51 = tmp39*tmp50*tmp9
  tmp52 = tmp1 + tmp7
  tmp53 = -(tmp19*tmp45*tmp52)
  tmp54 = -7*tmp7
  tmp55 = tmp41 + tmp42 + tmp54
  tmp56 = -2*tmp29*tmp55
  tmp57 = -3*tmp11
  tmp58 = 8*tmp19
  tmp59 = (-4*tmp7)/tmp6
  tmp60 = 1/tmp6 + tmp7
  tmp61 = tmp1*tmp60
  tmp62 = tmp43 + tmp57 + tmp58 + tmp59 + tmp61
  tmp63 = (ss*tmp62)/tmp5
  tmp64 = 2*tmp11
  tmp65 = -8*tmp19
  tmp66 = (29*tmp7)/tmp6
  tmp67 = -9*tmp7
  tmp68 = tmp42 + tmp67
  tmp69 = tmp1*tmp68
  tmp70 = tmp18 + tmp64 + tmp65 + tmp66 + tmp69
  tmp71 = tmp3*tmp70
  tmp72 = 8*tmp29
  tmp73 = tmp44 + tmp7
  tmp74 = -2*ss*tmp45*tmp73
  tmp75 = -29*tmp7
  tmp76 = tmp36 + 1/tmp6 + tmp75
  tmp77 = 2*tmp3*tmp76
  tmp78 = tmp41*tmp7
  tmp79 = 24*tmp19
  tmp80 = (-8*tmp7)/tmp6
  tmp81 = tmp10/tmp6
  tmp82 = tmp11 + tmp43 + tmp78 + tmp79 + tmp80 + tmp81
  tmp83 = tmp82/tmp5
  tmp84 = tmp72 + tmp74 + tmp77 + tmp83
  tmp85 = -(me2*tmp84)
  tmp86 = tmp47 + tmp48 + tmp51 + tmp53 + tmp56 + tmp63 + tmp71 + tmp85
  tmp87 = -2*tmp7
  tmp88 = 1/tmp6 + tmp87
  tmp89 = 6*tmp19*tmp88
  tmp90 = 12*tmp11
  tmp91 = tmp24 + tmp40
  tmp92 = 1/tmp91
  tmp93 = tmp26**2
  tmp94 = ss + 1/tmp5
  tmp95 = -2*me2*tmp94
  tmp96 = tmp9 + tmp93 + tmp95
  tmp97 = 1/tmp96
  tmp98 = me2**4
  tmp99 = tmp16 + 1/tmp6
  tmp100 = 4*me2
  tmp101 = tmp100 + tmp32
  tmp102 = 1/tmp101
  tmp103 = 2*tmp98
  tmp104 = 2*tmp20
  tmp105 = -8*ss
  tmp106 = tmp105 + 1/tmp6
  tmp107 = tmp106*tmp29
  tmp108 = tmp2*tmp34
  tmp109 = 8/tmp5
  tmp110 = tmp109 + tmp36 + 1/tmp6
  tmp111 = -(tmp110*tmp27)
  tmp112 = 12*tmp19
  tmp113 = ss*tmp32
  tmp114 = tmp112 + tmp113 + tmp43
  tmp115 = tmp114*tmp3
  tmp116 = ss/tmp6
  tmp117 = tmp116 + tmp18 + tmp58
  tmp118 = (tmp117*tmp25)/tmp5
  tmp119 = 12*tmp3
  tmp120 = tmp32 + tmp36
  tmp121 = tmp120/tmp5
  tmp122 = tmp44*tmp99
  tmp123 = tmp119 + tmp121 + tmp122
  tmp124 = tmp123*tmp9
  tmp125 = -8*tmp29
  tmp126 = tmp36 + 1/tmp6
  tmp127 = tmp126*tmp3
  tmp128 = tmp31 + tmp36
  tmp129 = -(tmp128*tmp19)
  tmp130 = 2*tmp116
  tmp131 = tmp130 + tmp18 + tmp58
  tmp132 = tmp131/tmp5
  tmp133 = tmp125 + tmp127 + tmp129 + tmp132
  tmp134 = me2*tmp133
  tmp135 = tmp103 + tmp104 + tmp107 + tmp108 + tmp111 + tmp115 + tmp118 + tmp124 +&
                          & tmp134
  tmp136 = sqrt(lambda)**8
  tmp137 = 5/tmp5
  tmp138 = 16*tmp19
  tmp139 = 2*tmp18
  tmp140 = 11/tmp6
  tmp141 = 10*tmp116
  tmp142 = 128*tmp20
  tmp143 = 7*tmp18
  tmp144 = -4*me2
  tmp145 = tmp144 + 1/tmp6
  tmp146 = tmp145**(-2)
  tmp147 = tmp13 + 1/tmp6
  tmp148 = tmp147**(-2)
  tmp149 = 3*tmp18
  tmp150 = 7/tmp6
  tmp151 = 16*ss
  tmp152 = tmp5**(-5)
  tmp153 = 32*tmp19
  tmp154 = 256*tmp19
  tmp155 = 360*tmp116
  tmp156 = tmp41 + tmp42
  tmp157 = 17/tmp6
  tmp158 = tmp157 + tmp36
  tmp159 = 14*tmp116
  tmp160 = 39*tmp18
  tmp161 = 8*tmp11
  tmp162 = tmp11 + tmp139
  tmp163 = tmp31 + tmp87
  tmp164 = -7*tmp18
  tmp165 = -8*tmp11
  tmp166 = (12*tmp7)/tmp6
  tmp167 = me2**5
  tmp168 = (4*tmp7)/tmp6
  tmp169 = 10*tmp19
  tmp170 = 2*tmp19
  tmp171 = tmp35*tmp7
  tmp172 = ss*tmp60
  tmp173 = tmp170 + tmp171 + tmp172
  tmp174 = 6*tmp19
  tmp175 = -tmp172
  tmp176 = tmp31*tmp7
  tmp177 = 2*tmp9
  tmp178 = 2*tmp3
  tmp179 = tmp10 + 1/tmp6
  tmp180 = tmp179/tmp5
  tmp181 = 2*tmp27
  tmp182 = tmp38 + 1/tmp6 + tmp8
  tmp183 = -(tmp182*tmp9)
  tmp184 = (-2*tmp2)/tmp5
  tmp185 = tmp15 + tmp178 + tmp184
  tmp186 = tmp185*tmp26
  tmp187 = -2*tmp3
  tmp188 = tmp44 + 1/tmp6
  tmp189 = tmp1*tmp188
  tmp190 = tmp180 + tmp187 + tmp189
  tmp191 = me2*tmp190
  tmp192 = tmp181 + tmp183 + tmp186 + tmp191
  tmp193 = -6*tmp116
  tmp194 = 4*tmp29
  tmp195 = -2*tmp116
  tmp196 = 2*me2
  tmp197 = me2 + tmp26
  tmp198 = 1/tmp9
  tmp199 = tmp196 + tmp40
  tmp200 = tmp197**2
  tmp201 = 1/me2
  tmp202 = ss*tmp11
  tmp203 = 1/ss
  tmp204 = -3/(tmp203*tmp5)
  tmp205 = tmp1 + 1/tmp5
  tmp206 = -(tmp205/tmp201)
  tmp207 = tmp178 + tmp19 + tmp204 + tmp206 + tmp9
  tmp208 = -(1/tmp5)
  tmp209 = 1/tmp203 + tmp208
  tmp210 = (tmp11*tmp209)/tmp203
  tmp211 = -3*tmp27*tmp94
  tmp212 = 3*tmp3
  tmp213 = tmp8/tmp203
  tmp214 = 3*tmp19
  tmp215 = tmp212 + tmp213 + tmp214
  tmp216 = tmp215*tmp9
  tmp217 = -tmp29
  tmp218 = tmp3/tmp203
  tmp219 = tmp19/tmp5
  tmp220 = -tmp34
  tmp221 = tmp202 + tmp217 + tmp218 + tmp219 + tmp220
  tmp222 = tmp221/tmp201
  tmp223 = tmp210 + tmp211 + tmp216 + tmp222 + tmp98
  tmp224 = -6/tmp203
  tmp225 = tmp97**2
  tmp226 = tmp25 + tmp8
  tmp227 = -(tmp226*tmp93)
  tmp228 = 1/tmp203 + tmp8
  tmp229 = -(tmp228*tmp9)
  tmp230 = -2/(tmp203*tmp5)
  tmp231 = -tmp19
  tmp232 = tmp212 + tmp230 + tmp231
  tmp233 = tmp232/tmp201
  tmp234 = tmp227 + tmp229 + tmp233 + tmp27
  tmp235 = 6*tmp27
  tmp236 = tmp40*tmp93
  tmp237 = 12/tmp5
  tmp238 = tmp237 + tmp41 + tmp7
  tmp239 = -(tmp238*tmp9)
  tmp240 = tmp224 + tmp7
  tmp241 = tmp240/tmp5
  tmp242 = 4*tmp7
  tmp243 = tmp242 + tmp44
  tmp244 = tmp243/tmp203
  tmp245 = tmp212 + tmp241 + tmp244
  tmp246 = (2*tmp245)/tmp201
  tmp247 = tmp235 + tmp236 + tmp239 + tmp246
  tmp248 = tmp196 + tmp32
  tmp249 = 1/tmp201 + tmp209
  tmp250 = f2p*tmp197*tmp208
  tmp251 = tmp207*tmp4*tmp7
  tmp252 = tmp250 + tmp251
  tmp253 = tmp100 + tmp40
  tmp254 = 1/tmp201 + tmp26 + tmp32
  tmp255 = tmp254**2
  tmp256 = -2/tmp6
  tmp257 = -2/tmp203
  tmp258 = 4*tmp9
  tmp259 = 4*tmp3
  tmp260 = tmp31/tmp203
  tmp261 = tmp11/tmp6
  tmp262 = -4*tmp7
  tmp263 = tmp40/tmp203
  tmp264 = -7/tmp6
  tmp265 = tmp10 + tmp264 + tmp7
  tmp266 = tmp265/tmp5
  tmp267 = tmp10 + tmp109 + tmp32 + tmp7
  tmp268 = tmp267/tmp201
  tmp269 = tmp7/tmp6
  tmp270 = tmp258 + tmp259 + tmp260 + tmp263 + tmp266 + tmp268 + tmp269
  tmp271 = -tmp269
  tmp272 = tmp13 + tmp253
  tmp273 = tmp179 + tmp7
  tmp274 = tmp273/tmp5
  tmp275 = tmp258 + tmp259 + tmp260 + tmp263 + tmp268 + tmp271 + tmp274
  tmp276 = -2/tmp201
  tmp277 = tmp276 + tmp7
  tmp278 = 3*tmp9
  tmp279 = tmp10 + tmp24 + tmp7
  tmp280 = tmp279/tmp201
  tmp281 = tmp257 + tmp60
  tmp282 = tmp281/tmp5
  tmp283 = tmp19 + tmp263 + tmp271 + tmp278 + tmp280 + tmp282 + tmp3
  tmp284 = 2*tmp7
  tmp285 = tmp10 + tmp24 + tmp284 + tmp32
  tmp286 = tmp285/tmp201
  tmp287 = -(tmp163*tmp26)
  tmp288 = tmp258 + tmp286 + tmp287
  tmp289 = 3*tmp7
  tmp290 = -3/tmp6
  tmp291 = tmp290 + tmp7
  tmp292 = tmp291/tmp5
  tmp293 = tmp279 + tmp32
  tmp294 = tmp293/tmp201
  tmp295 = tmp258 + tmp260 + tmp263 + tmp271 + tmp292 + tmp294
  tmp296 = f2p*tmp197*tmp32
  tmp297 = tmp258 + tmp260 + tmp263 + tmp269 + tmp292 + tmp294
  tmp298 = tmp297*tmp4*tmp7
  tmp299 = tmp296 + tmp298
  tmp300 = tmp11*tmp18
  tmp301 = -3*tmp18
  tmp302 = -tmp261
  tmp303 = (16*tmp27)/tmp6
  tmp304 = -tmp300
  tmp305 = -10*tmp269
  tmp306 = 48*tmp3
  tmp307 = 8*tmp292
  tmp308 = -6*tmp269
  tmp309 = 14*tmp269
  tmp310 = 6*tmp269
  tmp311 = tmp11*tmp21
  tmp312 = -5*tmp18
  tmp313 = 1/tmp203 + 1/tmp6
  tmp314 = tmp1 + 1/tmp6 + tmp8
  tmp315 = tmp284 + 1/tmp6
  tmp316 = -4/tmp6
  tmp317 = -16/tmp203
  tmp318 = 7*tmp3
  tmp319 = 8*tmp3
  tmp320 = (8*tmp7)/tmp203
  tmp321 = 8*tmp9
  tmp322 = tmp109 + tmp36 + tmp42
  tmp323 = tmp276*tmp322
  tmp324 = 13/tmp6
  tmp325 = tmp324 + tmp36
  tmp326 = (-2*tmp325)/tmp5
  tmp327 = tmp141 + tmp319 + tmp321 + tmp323 + tmp326 + tmp43 + tmp58
  tmp328 = 16*tmp29
  tmp329 = tmp289 + 1/tmp6
  tmp330 = tmp151 + tmp42
  tmp331 = tmp24 + tmp32
  tmp332 = tmp2/(tmp203*tmp6)
  tmp333 = tmp42/tmp203
  tmp334 = tmp49*tmp99
  tmp335 = 32*tmp20
  tmp336 = 4*tmp18
  tmp337 = -34*tmp19
  tmp338 = -2*tmp29
  tmp339 = 2*tmp219
  tmp340 = tmp44 + tmp8
  tmp341 = -(tmp340*tmp9)
  tmp342 = tmp19 + tmp3
  tmp343 = (3*tmp342)/tmp201
  tmp344 = tmp218 + tmp220 + tmp27 + tmp338 + tmp339 + tmp341 + tmp343
  tmp345 = 1/tmp331
  tmp346 = tmp5**2
  tmp347 = tmp40 + tmp8
  tmp348 = Sqrt(1/tmp5)
  tmp349 = tmp348*tmp7
  tmp350 = (1/tmp5)**1.5
  tmp351 = -4*tmp350
  tmp352 = tmp349 + tmp351
  tmp353 = tmp352**(-2)
  tmp354 = tmp9/tmp5
  tmp355 = tmp257*tmp3
  tmp356 = (tmp276*tmp94)/tmp5
  tmp357 = tmp202 + tmp219 + tmp29 + tmp354 + tmp355 + tmp356
  tmp358 = tmp40 + tmp49
  tmp359 = 6*tmp29
  tmp360 = tmp358*tmp9
  tmp361 = tmp231*tmp7
  tmp362 = tmp276*tmp358*tmp94
  tmp363 = tmp244*tmp8
  tmp364 = tmp41 + tmp7
  tmp365 = -(tmp3*tmp364)
  tmp366 = tmp359 + tmp360 + tmp361 + tmp362 + tmp363 + tmp365
  tmp367 = 3/tmp5
  tmp368 = tmp137 + 1/tmp203
  tmp369 = tmp354*tmp368
  tmp370 = tmp1 + tmp367
  tmp371 = -(tmp27*tmp370)
  tmp372 = 1/(tmp203*tmp5)
  tmp373 = tmp178 + tmp231 + tmp372
  tmp374 = tmp373*tmp93
  tmp375 = -5*tmp29
  tmp376 = tmp259/tmp203
  tmp377 = tmp231/tmp5
  tmp378 = 2*tmp34
  tmp379 = tmp375 + tmp376 + tmp377 + tmp378
  tmp380 = tmp379/tmp201
  tmp381 = tmp369 + tmp371 + tmp374 + tmp380 + tmp98
  tmp382 = 5*tmp7
  tmp383 = 2*tmp29
  tmp384 = 7*tmp7
  tmp385 = tmp27*tmp347
  tmp386 = tmp224 + tmp382
  tmp387 = tmp386/tmp5
  tmp388 = tmp289/tmp203
  tmp389 = tmp187 + tmp387 + tmp388
  tmp390 = tmp389*tmp9
  tmp391 = tmp10 + tmp289
  tmp392 = tmp3*tmp391
  tmp393 = 1/tmp203 + tmp284
  tmp394 = 2*tmp372*tmp393
  tmp395 = tmp361 + tmp383 + tmp392 + tmp394
  tmp396 = tmp26*tmp395
  tmp397 = tmp19*tmp289
  tmp398 = -6*tmp219
  tmp399 = tmp16 + tmp384
  tmp400 = tmp3*tmp399
  tmp401 = tmp383 + tmp397 + tmp398 + tmp400
  tmp402 = -(tmp401/tmp201)
  tmp403 = tmp385 + tmp390 + tmp396 + tmp402
  tmp404 = -2/tmp5
  tmp405 = tmp404 + tmp7
  tmp406 = -12*tmp3
  tmp407 = -3/tmp203
  tmp408 = -6*tmp7
  tmp409 = tmp98/tmp92
  tmp410 = tmp236*tmp3
  tmp411 = tmp407 + tmp7
  tmp412 = tmp24*tmp411
  tmp413 = tmp388 + tmp406 + tmp412
  tmp414 = tmp27*tmp413
  tmp415 = -4*tmp20
  tmp416 = tmp34*tmp7
  tmp417 = 1/tmp203 + tmp7
  tmp418 = tmp194*tmp417
  tmp419 = tmp1 + tmp289
  tmp420 = tmp19*tmp404*tmp419
  tmp421 = tmp16 + tmp54
  tmp422 = tmp218*tmp421
  tmp423 = tmp415 + tmp416 + tmp418 + tmp420 + tmp422
  tmp424 = tmp423/tmp201
  tmp425 = 12*tmp29
  tmp426 = -3*tmp19*tmp7
  tmp427 = tmp38 + tmp7
  tmp428 = 2*tmp372*tmp427
  tmp429 = tmp36 + tmp408
  tmp430 = tmp3*tmp429
  tmp431 = tmp425 + tmp426 + tmp428 + tmp430
  tmp432 = tmp431*tmp9
  tmp433 = tmp409 + tmp410 + tmp414 + tmp424 + tmp432
  tmp434 = -(1/tmp201)
  tmp435 = 1/tmp203 + tmp434 + 1/tmp5
  tmp436 = -3/tmp5
  tmp437 = tmp436 + tmp7
  tmp438 = tmp437**2
  tmp439 = tmp313 + tmp434
  tmp440 = -5/tmp6
  tmp441 = -8*tmp9
  tmp442 = -9/tmp6
  tmp443 = tmp262 + 1/tmp6
  tmp444 = tmp257*tmp7
  tmp445 = -4*tmp3
  tmp446 = -16*tmp7
  tmp447 = tmp446 + 1/tmp6
  tmp448 = tmp382 + 1/tmp6
  tmp449 = -2*tmp269
  tmp450 = tmp32 + tmp7
  tmp451 = tmp329/tmp6
  tmp452 = tmp289 + tmp42
  tmp453 = tmp262/tmp203
  tmp454 = tmp109 + tmp284 + tmp440
  tmp455 = (2*tmp454)/tmp201
  tmp456 = tmp36 + tmp42 + tmp87
  tmp457 = tmp404*tmp456
  tmp458 = tmp12 + tmp141 + tmp43 + tmp449 + tmp453 + tmp455 + tmp457
  tmp459 = tmp11 + tmp301 + tmp310
  tmp460 = -2*tmp18
  tmp461 = tmp168 + tmp460 + tmp64
  tmp462 = tmp35 + tmp384
  tmp463 = tmp18 + tmp305 + tmp57
  tmp464 = tmp11 + tmp18 + tmp308
  tmp465 = tmp242 + tmp42
  tmp466 = 16*tmp269
  tmp467 = tmp18 + tmp466 + tmp64
  tmp468 = tmp289 + tmp35
  tmp469 = 7*tmp11
  tmp470 = -5*tmp11
  tmp471 = (tmp460*tmp468*tmp7)/tmp5
  tmp472 = tmp11 + tmp18 + tmp309
  tmp473 = 5*tmp18
  tmp474 = tmp310 + tmp473 + tmp57
  tmp475 = tmp276 + tmp314
  tmp476 = f2p*tmp35*tmp475
  tmp477 = tmp109 + tmp440 + tmp7
  tmp478 = (2*tmp477)/tmp201
  tmp479 = tmp290*tmp7
  tmp480 = tmp36 + tmp40 + tmp42
  tmp481 = tmp404*tmp480
  tmp482 = tmp12 + tmp141 + tmp43 + tmp444 + tmp478 + tmp479 + tmp481
  tmp483 = tmp4*tmp482*tmp7
  tmp484 = tmp476 + tmp483
  tmp485 = tmp12 + tmp141 + tmp269 + tmp43 + tmp444 + tmp478 + tmp481
  tmp486 = tmp4*tmp485*tmp7
  tmp487 = tmp476 + tmp486
  tmp488 = 6*tmp9
  tmp489 = 23*tmp3
  tmp490 = 29/tmp5
  tmp491 = -12/tmp203
  tmp492 = -29/tmp203
  tmp493 = tmp150 + tmp16
  tmp494 = tmp7/tmp203
  tmp495 = 16/tmp5
  tmp496 = 9/tmp6
  tmp497 = tmp257 + tmp32 + tmp8
  tmp498 = 32*tmp3
  tmp499 = 64/(tmp203*tmp6)
  tmp500 = 15*tmp18
  tmp501 = 6*tmp3
  tmp502 = tmp2**2
  tmp503 = 28*tmp19
  tmp504 = 64*tmp3
  tmp505 = 64*tmp19
  tmp506 = 7/tmp203
  tmp507 = 25/tmp6
  tmp508 = 12/(tmp203*tmp6)
  tmp509 = tmp284 + tmp31
  tmp510 = tmp138*tmp163
  tmp511 = 6*tmp7
  tmp512 = 10*tmp7
  tmp513 = 37/tmp6
  tmp514 = 77/tmp6
  tmp515 = 29/tmp6
  tmp516 = 20*tmp18
  tmp517 = tmp31 + tmp7
  tmp518 = 4/tmp6
  tmp519 = tmp150 + tmp512
  tmp520 = 512*tmp152
  tmp521 = 8/tmp6
  tmp522 = tmp42 + tmp87
  tmp523 = -10*tmp7
  tmp524 = 48/tmp203
  tmp525 = 27/tmp6
  tmp526 = 47/tmp6
  tmp527 = tmp35 + tmp7
  tmp528 = -3*tmp7
  tmp529 = -7*tmp269
  tmp530 = 4*tmp19
  tmp531 = -16*tmp19
  tmp532 = 3*tmp11
  tmp533 = 32*tmp494
  tmp534 = tmp26**3
  tmp535 = -5*tmp7
  tmp536 = -6*tmp11
  tmp537 = -20*tmp269
  tmp538 = 17*tmp18
  tmp539 = tmp440 + tmp7
  tmp540 = tmp16 + tmp7
  tmp541 = -tmp11
  tmp542 = -8*tmp494
  tmp543 = -17*tmp18
  tmp544 = -2*tmp19
  tmp545 = tmp24 + 1/tmp6
  tmp546 = tmp521*tmp7
  tmp547 = tmp11 + tmp18
  tmp548 = tmp143 + tmp165 + tmp546
  tmp549 = 64*tmp29
  tmp550 = 8*tmp7
  tmp551 = -96/tmp203
  tmp552 = tmp470/tmp6
  tmp553 = tmp31 + tmp40
  tmp554 = 6*tmp11
  tmp555 = -32*tmp19
  tmp556 = tmp317*tmp88
  tmp557 = 96*tmp20
  tmp558 = 48*tmp19
  tmp559 = 96/(tmp203*tmp6)
  tmp560 = 9*tmp18
  tmp561 = tmp1 + tmp31
  tmp562 = tmp150 + tmp41
  tmp563 = 160/(tmp203*tmp6)
  tmp564 = 37*tmp18
  tmp565 = tmp558 + tmp563 + tmp564
  tmp566 = 64*tmp20
  tmp567 = tmp149 + tmp159 + tmp169
  tmp568 = tmp150 + tmp36
  tmp569 = tmp513/tmp203
  tmp570 = tmp143 + tmp503 + tmp569
  tmp571 = 72/(tmp203*tmp6)
  tmp572 = 13*tmp18
  tmp573 = 128*tmp19
  tmp574 = -12*tmp19
  tmp575 = tmp317 + tmp42
  tmp576 = tmp575/tmp6
  tmp577 = 32*tmp29
  tmp578 = tmp156/(tmp203*tmp6)
  tmp579 = tmp521/tmp203
  tmp580 = tmp164*tmp19
  tmp581 = 49*tmp18
  tmp582 = 1/tmp18
  tmp583 = 30*tmp269
  tmp584 = 18*tmp269
  tmp585 = 10*tmp18
  tmp586 = tmp512/tmp6
  tmp587 = tmp143 + tmp57 + tmp584
  tmp588 = tmp500 + tmp541 + tmp546
  tmp589 = 31*tmp18
  tmp590 = 38*tmp18
  tmp591 = tmp5**(-6)
  tmp592 = tmp535/tmp6
  tmp593 = 20*tmp269
  tmp594 = tmp496*tmp7
  tmp595 = -2*tmp11
  tmp596 = 19*tmp18
  tmp597 = 24*tmp269
  tmp598 = -4*tmp18
  tmp599 = -4*tmp11
  tmp600 = tmp1 + tmp60
  tmp601 = (-4*tmp94)/tmp201
  tmp602 = tmp170 + tmp177 + tmp178 + tmp180 + tmp601
  tmp603 = tmp518/tmp203
  tmp604 = 1/tmp45
  tmp605 = (tmp197*tmp4*tmp602*tmp7)/(tmp345*tmp604)
  tmp606 = tmp181*tmp39
  tmp607 = tmp38 + 1/tmp6
  tmp608 = -(tmp607/tmp604)
  tmp609 = tmp329 + tmp41
  tmp610 = tmp609*tmp8
  tmp611 = tmp319 + tmp608 + tmp610
  tmp612 = -(tmp611*tmp9)
  tmp613 = -(tmp2/(tmp203*tmp604))
  tmp614 = tmp36 + tmp452
  tmp615 = tmp187*tmp614
  tmp616 = tmp18 + tmp271 + tmp530 + tmp603
  tmp617 = tmp616*tmp8
  tmp618 = tmp613 + tmp615 + tmp617 + tmp72
  tmp619 = tmp26*tmp618
  tmp620 = 15*tmp7
  tmp621 = tmp290 + tmp36 + tmp620
  tmp622 = tmp187*tmp621
  tmp623 = (tmp188*tmp257)/tmp604
  tmp624 = tmp450/tmp6
  tmp625 = (4*tmp517)/tmp203
  tmp626 = tmp624 + tmp625 + tmp79
  tmp627 = tmp626/tmp5
  tmp628 = tmp125 + tmp622 + tmp623 + tmp627
  tmp629 = tmp628/tmp201
  tmp630 = tmp606 + tmp612 + tmp619 + tmp629
  tmp631 = f2p*tmp35*tmp630
  tmp632 = tmp605 + tmp631
  tmp633 = (tmp313*tmp32)/tmp203
  tmp634 = tmp187*tmp2
  tmp635 = tmp372*tmp561
  tmp636 = -4*tmp313
  tmp637 = tmp49 + tmp636
  tmp638 = tmp637*tmp9
  tmp639 = tmp404*tmp99
  tmp640 = tmp170 + tmp18 + tmp333 + tmp501 + tmp639
  tmp641 = tmp640/tmp201
  tmp642 = tmp181 + tmp383 + tmp633 + tmp634 + tmp635 + tmp638 + tmp641
  tmp643 = -76*tmp18
  tmp644 = tmp150 + tmp40
  tmp645 = 19*tmp7
  tmp646 = tmp513 + tmp645
  tmp647 = -15*tmp7
  tmp648 = tmp526 + tmp647
  tmp649 = 59/tmp6
  tmp650 = tmp31 + tmp382
  tmp651 = tmp18*tmp650
  tmp652 = tmp170*tmp452
  tmp653 = tmp150 + tmp382
  tmp654 = (tmp35*tmp653)/tmp203
  tmp655 = tmp651 + tmp652 + tmp654
  tmp656 = tmp18*tmp655
  tmp657 = tmp42 + tmp7
  tmp658 = tmp35 + tmp40
  tmp659 = tmp143*tmp60
  tmp660 = tmp530*tmp644
  tmp661 = tmp289 + tmp513
  tmp662 = tmp661/(tmp203*tmp6)
  tmp663 = tmp659 + tmp660 + tmp662
  tmp664 = 89/tmp6
  tmp665 = 6/tmp6
  tmp666 = 37*tmp7
  tmp667 = 95/tmp6
  tmp668 = tmp60*tmp8
  tmp669 = tmp317/tmp604
  tmp670 = tmp52/tmp203
  tmp671 = tmp179 + tmp87
  tmp672 = tmp671/tmp5
  tmp673 = tmp178 + tmp670 + tmp672
  tmp674 = tmp553*tmp58
  tmp675 = tmp168 + tmp470 + tmp473
  tmp676 = (2*tmp675)/tmp203
  tmp677 = tmp143 + tmp168 + tmp57
  tmp678 = 24/(tmp203*tmp6)
  tmp679 = -34*tmp18
  tmp680 = tmp554 + tmp59 + tmp679
  tmp681 = -15*tmp18
  tmp682 = 1/tmp148
  tmp683 = tmp49 + tmp607
  tmp684 = 20/(tmp203*tmp6)
  tmp685 = 6*tmp20
  tmp686 = 20*tmp19
  tmp687 = 4*tmp20
  tmp688 = 24/tmp203
  tmp689 = -22/tmp6
  tmp690 = 36/(tmp203*tmp6)
  tmp691 = 6*tmp18
  tmp692 = -7*tmp11
  tmp693 = -16*tmp494
  tmp694 = 52/(tmp203*tmp6)
  tmp695 = tmp324*tmp7
  tmp696 = tmp149 + tmp593 + tmp692
  tmp697 = 256*tmp152
  tmp698 = 32*tmp269
  tmp699 = 11*tmp18
  tmp700 = 13*tmp11
  tmp701 = -11*tmp11
  tmp702 = tmp324/tmp203
  tmp703 = 112*tmp19
  tmp704 = -4*tmp19*tmp696
  tmp705 = 10*tmp11
  tmp706 = tmp150 + tmp284
  tmp707 = 55*tmp18
  tmp708 = tmp201**(-6)
  tmp709 = 20/tmp203
  tmp710 = 1/tmp203 + tmp32
  tmp711 = tmp506/tmp6
  tmp712 = -64/tmp203
  tmp713 = tmp203**(-4)
  tmp714 = 56*tmp19
  tmp715 = tmp665/tmp203
  tmp716 = tmp496/tmp203
  tmp717 = -24*tmp19
  tmp718 = tmp201**(-7)
  tmp719 = tmp109 + tmp40
  tmp720 = tmp40 + tmp99
  tmp721 = -8*tmp3*tmp720
  tmp722 = 8*tmp372*tmp553
  tmp723 = tmp302 + tmp577 + tmp721 + tmp722
  tmp724 = 24*tmp3
  tmp725 = 32/tmp203
  tmp726 = 18*tmp18
  tmp727 = -6*tmp18
  tmp728 = 31/tmp6
  tmp729 = -11*tmp269
  tmp730 = tmp6**(-4)
  tmp731 = tmp384/tmp6
  tmp732 = 5*tmp11
  tmp733 = 34*tmp18
  tmp734 = 54*tmp18
  tmp735 = tmp284 + tmp42
  tmp736 = -27*tmp18
  tmp737 = 704*tmp34
  tmp738 = tmp324 + tmp382
  tmp739 = 9*tmp11
  tmp740 = -9*tmp11
  tmp741 = 35*tmp18
  MP2MPL_MP_FIN_d10d22 = C0IR6sFP*f2p*Pi*tmp192*tmp242*tmp4*tmp5*tmp6 - 2*D0tsNN*Pi*tmp136*tmp17*tmp37*tm&
                          &p5*tmp6*(112*tmp20 + (tmp138 + tmp18 + tmp193)*tmp3 + tmp256*tmp34 + tmp372*(tmp&
                          &138 + tmp195 + tmp43) + tmp177*(tmp109 + tmp32)*(tmp137 + tmp44) + tmp338*(72/tm&
                          &p203 - 53/tmp6) + tmp181*(-8/tmp5 + 1/tmp6) + (208*tmp29 + (tmp18 - 48*tmp19 + t&
                          &mp508)/tmp5 + tmp3*(tmp551 + 30/tmp6) + tmp19*tmp665)/tmp201) + logmuP2*Pi*tmp17&
                          &*tmp4*tmp5*((tmp22*tmp269*tmp4)/tmp201 + f2p*tmp23*(tmp258 + tmp276*(tmp16 + tmp&
                          &35 + tmp367) + tmp501 + tmp502 + tmp404*(tmp28 + tmp521)))*tmp6*tmp7 - C0tP2P2NP&
                          &N*Pi*tmp136*tmp148*tmp17*tmp37*tmp5*tmp6*(-512*tmp152 + 270*tmp18*tmp29 + (tmp15&
                          &1 + tmp290)*tmp335 + (tmp18*(tmp143 + tmp159 + tmp337))/tmp5 + tmp21*tmp530 - (4&
                          &*(tmp142 + (2*tmp21)/tmp203 + (tmp18*(-17/tmp203 + tmp31))/tmp5 + (tmp3*(tmp33 +&
                          & tmp524))/tmp6))/tmp201 + (tmp177*(tmp139 + tmp306 + tmp33/tmp5))/tmp6 + (tmp445&
                          &*(tmp516 + 19/(tmp203*tmp6) + tmp717))/tmp6) - 8*C0tP2P2NPL*Pi*tmp148*tmp17*tmp3&
                          &7*tmp46*tmp6*(288*tmp152 + tmp177*(-2*tmp21 + tmp328 + tmp319*tmp539 + (tmp449 +&
                          & tmp538 + tmp57)/tmp5) - 2*tmp21*tmp670 + tmp29*(tmp153 - 222*tmp18 + tmp499 + t&
                          &mp536 + tmp537 + tmp551*tmp7) - 16*tmp20*(tmp264 + tmp535 + tmp709) + tmp259*((t&
                          &mp143 + tmp171 + tmp532)/tmp203 + tmp530*tmp539 + (tmp11 + tmp449 + tmp726)/tmp6&
                          &) + (tmp43*(tmp11 + tmp143 + tmp449) + tmp224/(tmp6*tmp604**2) + tmp19*(tmp536 +&
                          & tmp59 + tmp733))/tmp5 + (2*(224*tmp20 - 16*tmp29*(tmp1 + tmp40 + tmp42) + tmp21&
                          &*tmp540 + tmp178*(tmp310 + tmp43 + tmp541 + tmp542 + 40/(tmp203*tmp6)) + ((tmp17&
                          &1 + tmp532 + tmp543)/tmp203 + (tmp547 + tmp59)/tmp6)*tmp8))/tmp201) + 8*D0tsLN*P&
                          &i*tmp17*tmp37*tmp46*tmp6*tmp86 + 8*D0tsNL*Pi*tmp17*tmp37*tmp46*tmp6*tmp86 - (16*&
                          &C0tP2P2LPL*Pi*tmp11*tmp148*tmp17*tmp37*tmp6*(tmp208*((4*tmp11 + tmp164 + tmp166)&
                          &*tmp18 + tmp170*(tmp165 + tmp168 + tmp538) + (tmp35*(tmp18 + tmp536 + tmp593))/t&
                          &mp203) + tmp335*(tmp408 + 1/tmp6) + tmp177*(tmp12*tmp163 + (tmp161 + tmp543 + tm&
                          &p59)/tmp5 + tmp162/tmp6) + (tmp162*tmp170 + tmp300 + tmp35*tmp494*tmp527)/tmp6 +&
                          & tmp178*(tmp510 + (2*(tmp165 + tmp572 + tmp59))/tmp203 + (tmp11 - 24*tmp18 + tmp&
                          &698)/tmp6) + (4*(tmp3*(tmp161 + tmp166 + tmp533 - 48/(tmp203*tmp6) + tmp681) + t&
                          &mp32*(tmp202 + tmp139/tmp203 + tmp18*tmp7) + (tmp165/tmp203 + tmp21 + tmp538/tmp&
                          &203 + tmp18*tmp550 + tmp552 + tmp603*tmp7)/tmp5 + tmp577*tmp88))/tmp201 + tmp383&
                          &*(tmp160 + tmp161 + tmp537 + tmp712*tmp88)))/tmp5 + logP2t*Pi*tmp148*tmp4*tmp5*t&
                          &mp6*tmp7*((f2p*tmp256*((-4*tmp2)/tmp201 + tmp258 + tmp445 + tmp502))/tmp345 + tm&
                          &p4*tmp7*(tmp125*(tmp140 + tmp151) + (tmp18*(tmp31 + tmp506))/tmp203 + tmp566 + t&
                          &mp3*(tmp160 + tmp505 + 144/(tmp203*tmp6)) + (2*(-8*tmp3*(tmp36 + tmp496) + tmp43&
                          &*(tmp35 + tmp506) + tmp549 + (56/tmp203 + tmp507)/(tmp5*tmp6)))/tmp201 + (tmp404&
                          &*(tmp149 + tmp503 + 23/(tmp203*tmp6)))/tmp6 + (tmp143 + tmp504 - 56/(tmp5*tmp6))&
                          &*tmp9)) - (32*D0tsLL*Pi*tmp11*tmp17*tmp37*tmp6*(tmp181*tmp88 + tmp383*tmp88 + tm&
                          &p3*(tmp224 + tmp443)*tmp88 + tmp257*(tmp261 + tmp19*tmp88 + tmp494*tmp88) - 2*(1&
                          &/tmp5 + tmp73)*tmp88*tmp9 + (tmp11*tmp521 + tmp89 - (tmp18 + tmp80 + tmp90)/tmp2&
                          &03)/tmp5 + (2*tmp261 + tmp187*tmp88 + 4*tmp494*tmp88 + tmp89 + (tmp18 + tmp320 +&
                          & tmp81 + tmp90)/tmp5)/tmp201))/tmp5 - (logmuN2*Pi*tmp11*tmp17*tmp4*tmp5*tmp6*(tm&
                          &p4*tmp7*(128*tmp29 + tmp24*(tmp139 + tmp153 + tmp176 + tmp320 + tmp491/tmp6) + (&
                          &tmp18 + tmp531)*tmp7 - 16*tmp3*(tmp150 + tmp151 + tmp7) + (2*(128*tmp3 - (8*(tmp&
                          &151 + tmp315))/tmp5 + tmp330*tmp7))/tmp201 + 16*tmp719*tmp9) - (2*f2p*tmp327)/tm&
                          &p92))/2. + logmuL2*Pi*tmp17*tmp242*tmp4*tmp6*(tmp284*tmp4*(tmp328 + tmp128*tmp44&
                          &5 + (tmp176 + tmp330*tmp404 + tmp498)/tmp201 + (tmp138 - tmp451)/tmp5 + tmp269*(&
                          &tmp407 + 1/tmp6) + tmp495*tmp9) - (f2p*tmp327)/tmp92) + 16*D0IR6tsL*Pi*tmp4*tmp6&
                          &*tmp7*((tmp4*tmp7*(tmp181 + tmp26*tmp673 + (tmp187 + tmp274 + (2*tmp73)/tmp203)/&
                          &tmp201 - (tmp427 + tmp8)*tmp9))/tmp604 + f2p*(tmp181*tmp60 + tmp26*tmp60*(tmp178&
                          & + tmp600/tmp203 + tmp404*tmp600) + (tmp187*tmp60 + (-14*tmp269 + tmp547 - (4*tm&
                          &p60)/tmp203)/tmp5 + (2*tmp60*(tmp188 + tmp7))/tmp203)/tmp201 - tmp60*(tmp607 + t&
                          &mp7 + tmp8)*tmp9))*tmp92 + 4*DB0sFPlogmut*Pi*tmp4*tmp494*tmp5*tmp6*(-2*f2p*tmp19&
                          &2 + tmp197*tmp4*tmp40*tmp602)*tmp97 + 2*DB0sFP*logF2P2*Pi*tmp345*tmp4*tmp494*tmp&
                          &5*tmp6*tmp604*tmp632*tmp97 + 4*DB0sFP*logmuF2*Pi*tmp345*tmp4*tmp494*tmp5*tmp6*tm&
                          &p604*tmp632*tmp97 + (DB0F2FN*f2p*Pi*tmp102*tmp135*tmp161*tmp4*tmp5*tmp6*tmp92*tm&
                          &p97)/tmp201 - (32*DB0F2FL*f2p*Pi*tmp102*tmp135*tmp4*tmp6*tmp7*tmp92*tmp97)/tmp20&
                          &1 - (32*DB0P2LP*f2p*Pi*tmp345*tmp4*tmp6*tmp7*(tmp103 + tmp111 + tmp26*(-((tmp139&
                          & + tmp170 + tmp260)/tmp203) - tmp3*(tmp140 + tmp38) + tmp383 + (tmp141 + tmp149 &
                          &+ tmp174)/tmp5) + (tmp125 + tmp158*tmp3 + (tmp141 + tmp312 + tmp58)/tmp5 - (tmp3&
                          &36 + tmp58 + tmp711)/tmp203)/tmp201 + (tmp112 + tmp119 + tmp139 + tmp333 + (tmp3&
                          &6 + tmp440)/tmp5)*tmp9)*tmp92*tmp97)/tmp5 + (logP2L2*Pi*tmp4*tmp7*(tmp148*tmp17*&
                          &tmp197*tmp4*(tmp302 + tmp328 + tmp178*tmp447 + (tmp284*tmp448)/tmp5)*tmp5*tmp597&
                          & + tmp148*tmp17*tmp346*tmp4*tmp57*(96*tmp152 + tmp311 + tmp29*(22*tmp11 + tmp168&
                          & + tmp460) - 16*tmp20*tmp462 + tmp471 + (tmp3*(22*tmp269 + tmp547))/tmp6) - tmp1&
                          &36*tmp17*tmp254*tmp346*tmp358*tmp4*tmp6 + 4*tmp11*tmp254*tmp353*tmp4*tmp438*tmp6&
                          & - 3*tmp17*tmp200*tmp346*tmp347*tmp4*tmp46*tmp6 + 4*tmp254*tmp353*tmp358*tmp4*tm&
                          &p46*tmp6 + (6*tmp17*tmp225*tmp344*tmp346*tmp366*tmp4*tmp46*tmp6)/tmp201 + (6*tmp&
                          &17*tmp207*tmp225*tmp346*tmp4*tmp403*tmp46*tmp6)/tmp203 - 6*tmp17*tmp201*tmp225*t&
                          &mp234*tmp346*tmp4*tmp433*tmp46*tmp6 - tmp17*tmp254*tmp346*tmp4*tmp438*tmp46*tmp6&
                          & - (24*tmp11*tmp17*tmp225*tmp344*tmp366*tmp4*tmp5*tmp6)/tmp201 + 24*tmp11*tmp17*&
                          &tmp201*tmp225*tmp234*tmp4*tmp433*tmp5*tmp6 - 3*tmp197*tmp345*tmp484*tmp5*tmp6 - &
                          &3*tmp197*tmp345*tmp487*tmp5*tmp6 + tmp254*tmp346*tmp4*tmp439*tmp532*tmp6 + tmp14&
                          &8*tmp17*tmp200*tmp346*tmp4*tmp554*(tmp304 + tmp291*tmp328 + tmp178*(tmp149 + tmp&
                          &449 + tmp470) + (tmp11*tmp521)/tmp5 + tmp557)*tmp6 - (12*tmp11*tmp345*tmp353*tmp&
                          &4*tmp6*(tmp261 + tmp328 + (tmp18 + tmp449 + tmp57)/tmp5 - (8*tmp3)/tmp604))/tmp2&
                          &01 + tmp353*tmp4*tmp405*tmp532*tmp6*(tmp263 + tmp269 + tmp333 + tmp441 + tmp445 &
                          &+ (tmp364 + tmp442)/tmp5 + (tmp151 - 12/tmp5 + tmp539)/tmp201 + tmp65) + (48*tmp&
                          &17*tmp345*tmp4*tmp405**2*tmp7)/tmp201 + 12*tmp17*tmp345*tmp4*tmp405*tmp458*tmp7 &
                          &+ 24*tmp148*tmp17*tmp200*tmp4*(tmp261 - 6*tmp3*tmp443 - (10*tmp11)/tmp5)*tmp5*tm&
                          &p7 + 24*tmp17*tmp197*tmp345*tmp347*tmp4*tmp458*tmp6*tmp7 + 12*tmp17*tmp4*tmp405*&
                          &(tmp116 + tmp174 + tmp263 + tmp43 + tmp449 + tmp488 + tmp489 + (tmp450 + tmp490 &
                          &+ tmp491)/tmp201 + (tmp492 + tmp527)/tmp5)*tmp6*tmp7 - 12*tmp17*tmp4*tmp405*tmp6&
                          &*(tmp269 - 8*tmp3 + tmp333 + tmp441 + tmp444 + (tmp151 + tmp284 + tmp440 - 16/tm&
                          &p5)/tmp201 + (tmp151 + tmp284 + tmp442)/tmp5 + tmp65)*tmp7 + 12*tmp148*tmp17*tmp&
                          &269*tmp4*tmp5*(tmp300 - 4*tmp29*tmp465 + tmp178*tmp467 + (tmp315*tmp316*tmp7)/tm&
                          &p5) + tmp148*tmp17*tmp197*tmp346*tmp4*tmp554*tmp6*(128*tmp152 + tmp311 - 16*tmp2&
                          &0*tmp465 + (tmp3*(tmp43 + tmp469 + tmp586))/tmp6 + tmp18*tmp208*tmp462*tmp7 + (t&
                          &mp162 + tmp269)*tmp72) + tmp345*tmp353*tmp4*(tmp271 + tmp445 + tmp329/tmp5)*tmp5&
                          &32*tmp6*(tmp141 + tmp269 + tmp43 + tmp444 + (2*(tmp237 + tmp539))/tmp201 + (-24/&
                          &tmp203 + tmp284 + tmp689)/tmp5 + tmp724) + tmp353*tmp4*tmp405*tmp532*tmp6*(tmp11&
                          &3 + tmp18 - 6*tmp19 + tmp269 - 27*tmp3 + (33/tmp203 + tmp35)/tmp5 + (tmp41 - 33/&
                          &tmp5 + 1/tmp6)/tmp201 - 6*tmp9) + tmp11*tmp17*tmp207*tmp225*tmp4*tmp5*tmp6*tmp68&
                          &8*(-tmp396 + tmp401/tmp201 + tmp27*tmp405 + (tmp178 + tmp528/tmp203 + (tmp38 + t&
                          &mp535)/tmp5)*tmp9) + tmp200*tmp347*tmp353*tmp4*tmp6*tmp90 + tmp148*tmp4*tmp450*(&
                          &tmp451 + tmp404*tmp452)*tmp511*tmp92 + tmp148*tmp197*tmp346*tmp4*tmp408*(tmp304 &
                          &+ tmp178*tmp463 + (tmp269*tmp462)/tmp5 + tmp328/tmp6)*tmp92 + tmp148*tmp346*tmp4&
                          &*tmp528*(tmp311 + tmp471 - 4*tmp29*tmp474 + (tmp178*tmp472)/tmp6)*tmp92 + tmp201&
                          &*tmp225*tmp249*tmp252*tmp346*tmp408*tmp433*tmp6*tmp92 - tmp254*tmp346*tmp358*tmp&
                          &4*tmp46*tmp6*tmp92 + 3*tmp254*tmp346*tmp4*tmp439*tmp46*tmp6*tmp92 + tmp148*tmp20&
                          &0*tmp346*tmp4*tmp408*(tmp300 + tmp259*tmp459 - (8*tmp261)/tmp5)*tmp6*tmp92 - 12*&
                          &tmp11*tmp254*tmp4*tmp439*tmp5*tmp6*tmp92 + (tmp225*(1/tmp201 + tmp208 + tmp25)*t&
                          &mp252*tmp346*tmp366*tmp511*tmp6*tmp92)/tmp201 + (tmp225*tmp252*tmp346*tmp403*tmp&
                          &511*tmp6*tmp92)/tmp203 + tmp254*tmp346*tmp4*tmp438*tmp541*tmp6*tmp92 + tmp200*tm&
                          &p346*tmp347*tmp4*tmp57*tmp6*tmp92 + (6*tmp345*tmp484*tmp6*tmp92)/tmp604 + 3*tmp3&
                          &45*tmp487*tmp5*tmp6*(tmp271 + tmp668)*tmp92 + (12*tmp345*tmp4*(tmp302 + tmp461/t&
                          &mp5)*tmp5*tmp6*tmp7*tmp92)/tmp201 + 3*tmp347*tmp5*tmp6*(2*f2p*(tmp170 + tmp177 +&
                          & tmp178 + tmp18 + tmp260 + (tmp16 + tmp24 + tmp31)*tmp434 + tmp208*tmp493) + tmp&
                          &4*(tmp116 + tmp174 + tmp271 + tmp43 + tmp488 + tmp489 + (tmp32 + tmp490 + tmp491&
                          &)/tmp201 + (tmp35 + tmp492)/tmp5)*tmp7)*tmp92 + 3*tmp347*tmp5*tmp6*(2*f2p*(tmp15&
                          & + tmp177 + tmp178 + tmp208*(tmp16 + tmp496) + tmp434*(tmp16 + tmp545)) + tmp4*t&
                          &mp7*(tmp271 + tmp319 + tmp321 + tmp440/tmp203 + tmp494 + (tmp40 + tmp495 + tmp57&
                          &5)/tmp201 + tmp58 + tmp208*(tmp151 + tmp442 + tmp7)))*tmp92 + (tmp197*tmp4*tmp40&
                          &8*(tmp261 + tmp12/tmp6 + tmp464*tmp8)*tmp92)/(tmp351 + tmp348/tmp6)**2 - 48*tmp1&
                          &1*tmp17*tmp344*tmp4*tmp97 - 12*tmp17*tmp201*tmp234*tmp4*tmp46*tmp97 + 12*tmp344*&
                          &tmp353*tmp4*tmp46*tmp97 + (48*tmp11*tmp17*tmp201*tmp234*tmp4*tmp97)/tmp5 + 48*tm&
                          &p11*tmp17*tmp248*tmp344*tmp4*tmp6*tmp97 - 48*tmp11*tmp17*tmp381*tmp4*tmp6*tmp97 &
                          &- 12*tmp248*tmp344*tmp353*tmp4*tmp46*tmp6*tmp97 + 12*tmp353*tmp381*tmp4*tmp46*tm&
                          &p6*tmp97 - 48*tmp17*tmp203*tmp344*tmp357*tmp4*tmp6*tmp7*tmp97 + tmp203*tmp344*tm&
                          &p353*tmp357*tmp4*tmp6*tmp90*tmp97 - 12*tmp201*tmp249*tmp252*tmp7*tmp92*tmp97 - 1&
                          &2*tmp252*tmp435*tmp5*tmp7*tmp92*tmp97 + 12*tmp248*tmp252*tmp435*tmp5*tmp6*tmp7*t&
                          &mp92*tmp97 + 12*tmp252*tmp5*tmp6*tmp7*(tmp231 + tmp3 + tmp404/tmp201 + tmp9)*tmp&
                          &92*tmp97 + tmp201*tmp252*tmp5*tmp6*(tmp3 - tmp372 + tmp228*tmp434 + tmp9)*tmp90*&
                          &tmp92*tmp97))/3. + logF2L2*Pi*tmp284*tmp4*(tmp102*tmp12*tmp17*tmp201*(sqrt(lambda)**3 &
                          &+ sqrt(lambda)*tmp276)**2*tmp4 + (tmp102*tmp17*tmp201*tmp277*tmp288*tmp4*tmp550)/tmp5 &
                          &+ tmp146*tmp17*tmp197*tmp198*tmp4*tmp595*(tmp303 + tmp304 + tmp177*(tmp12 + tmp3&
                          &07 + tmp463) + (tmp12 + tmp171 + tmp469 + tmp550/tmp5)/(tmp201*tmp6)) + (tmp146*&
                          &tmp17*tmp197*tmp198*tmp4*(16*tmp27 + tmp302 + tmp177*tmp447 + (tmp284*tmp448)/tm&
                          &p201)*tmp550)/(tmp5*tmp6) + tmp11*tmp17*tmp198*tmp199*tmp200*tmp24*tmp4*tmp6 + t&
                          &mp11*tmp17*tmp201*tmp225*tmp234*tmp24*tmp247*tmp4*tmp6 + tmp11*tmp17*tmp198*tmp2&
                          &4*tmp253*tmp255*tmp4*tmp6 - tmp17*tmp198*tmp199*tmp200*tmp4*tmp46*tmp6 - tmp17*t&
                          &mp201*tmp225*tmp234*tmp247*tmp4*tmp46*tmp6 - tmp17*tmp198*tmp253*tmp255*tmp4*tmp&
                          &46*tmp6 + (tmp17*tmp201*tmp277*tmp283*tmp4*tmp550*tmp6)/tmp5 + (tmp17*tmp201*tmp&
                          &277*tmp4*(tmp113 + tmp170 + tmp178 + tmp271 + tmp494 + (tmp40 + tmp497)/tmp201 +&
                          & tmp208*(tmp256 + tmp540))*tmp550*tmp6)/tmp5 + tmp102*tmp17*tmp197*tmp201*tmp270&
                          &*tmp272*tmp4*tmp595*tmp6 + tmp102*tmp17*tmp197*tmp201*tmp272*tmp275*tmp4*tmp595*&
                          &tmp6 + tmp17*tmp201*tmp277*tmp283*tmp4*tmp595*tmp6 + tmp102*tmp17*tmp275*tmp39*t&
                          &mp4*tmp595*tmp6 + (tmp102*tmp17*tmp201*tmp4*tmp599*tmp6*(tmp261 + (tmp12 + tmp18&
                          & + tmp449 + tmp57 - 8/(tmp5*tmp604))/tmp201))/tmp5 + tmp17*tmp201*tmp277*tmp4*(t&
                          &mp116 + tmp178 + tmp269 + tmp314/tmp201 + tmp544 + tmp404/tmp6)*tmp6*tmp64 + tmp&
                          &146*tmp17*tmp4*tmp541*((tmp12 + tmp171 + tmp18 - (8*tmp315)/tmp5 + tmp57)/tmp6 +&
                          & (2*(tmp171 - 80*tmp3 + tmp312 + tmp532 + tmp109*tmp657))/tmp201) + tmp102*tmp17&
                          &*tmp197*tmp199*tmp201*tmp288*tmp4*tmp495*tmp6*tmp7 + (tmp146*tmp17*tmp198*tmp4*t&
                          &mp518*tmp7*(tmp300 - 4*tmp27*tmp465 + tmp177*tmp467 + (tmp315*tmp316*tmp7)/tmp20&
                          &1))/tmp5 + tmp102*tmp17*tmp201*tmp270*tmp4*tmp6*tmp64*(tmp271 + (2*(tmp147 + tmp&
                          &7))/tmp201) + tmp146*tmp17*tmp198*tmp200*tmp4*tmp6*tmp64*(tmp304 + tmp177*(tmp14&
                          &9 + tmp306 + tmp307 + tmp308 + tmp470) + (tmp550*(1/tmp5 + tmp7))/(tmp201*tmp6))&
                          & + (tmp146*tmp17*tmp198*tmp200*tmp4*tmp550*((-10*tmp11)/tmp201 + tmp261 - 6*tmp4&
                          &43*tmp9))/tmp5 + tmp146*tmp17*tmp198*tmp4*tmp541*(tmp311 + (tmp460*(tmp24 + tmp4&
                          &68)*tmp7)/tmp201 + tmp181*(11*tmp11 + tmp306 + tmp312 + tmp109*(tmp54 + 1/tmp6) &
                          &+ tmp689*tmp7) + ((-48*tmp3 + tmp547 + tmp583 + tmp109*(tmp511 + 1/tmp6))*tmp9)/&
                          &tmp6) + tmp146*tmp284*tmp4*tmp450*(tmp451 + tmp276*tmp452)*tmp92 - 2*tmp102*tmp1&
                          &97*tmp201*tmp253*tmp299*tmp6*tmp92 + tmp201*tmp225*tmp247*tmp249*tmp252*tmp40*tm&
                          &p6*tmp92 + (tmp102*tmp201*tmp242*tmp4*(tmp302 + tmp461/tmp201)*tmp6*tmp92)/tmp5 &
                          &+ tmp198*tmp199*tmp200*tmp4*tmp541*tmp6*tmp92 + tmp198*tmp253*tmp255*tmp4*tmp541&
                          &*tmp6*tmp92 + 2*tmp102*tmp201*tmp299*tmp6*(tmp271 + (2*tmp60)/tmp201)*tmp92 - (4&
                          &*tmp102*(tmp295*tmp4*tmp40 + (f2p*tmp197)/tmp6)*tmp6*tmp92)/tmp604 - 2*tmp102*tm&
                          &p197*tmp201*tmp253*tmp6*(tmp296 + tmp295*tmp4*tmp7)*tmp92 + tmp146*tmp198*tmp4*t&
                          &mp40*(tmp311 - 4*tmp27*tmp474 + (tmp177*tmp472)/tmp6 + (tmp460*tmp468*tmp7)/tmp2&
                          &01)*tmp92 + tmp146*tmp197*tmp198*tmp4*(tmp303 + tmp304 + (tmp269*tmp462)/tmp201 &
                          &+ tmp177*tmp463)*tmp87*tmp92 + tmp146*tmp198*tmp200*tmp4*((-8*tmp261)/tmp201 + t&
                          &mp300 + tmp258*tmp459)*tmp6*tmp87*tmp92 + (tmp197*tmp4*tmp87*(tmp261 + (2*tmp464&
                          &)/tmp201 + (16*tmp9)/tmp6)*tmp92)/(-4*(1/tmp201)**1.5 + Sqrt(1/tmp201)/tmp6)**2 &
                          &+ 2*tmp199*tmp201*tmp6*(tmp283*tmp4*tmp7 + f2p*(tmp3 + tmp313/tmp203 + tmp313*tm&
                          &p404 + tmp314*tmp434 + tmp9))*tmp92 + 2*tmp199*tmp201*tmp6*tmp92*(tmp4*(tmp113 +&
                          & tmp170 + tmp178 + tmp271 + tmp497/tmp201 + (tmp10 + tmp35)/tmp5)*tmp7 + f2p*(tm&
                          &p2*tmp208 + tmp342 + tmp9 + tmp95)) + 2*tmp17*tmp201*tmp234*tmp248*tmp4*tmp46*tm&
                          &p6*tmp97 + (tmp17*tmp201*tmp203*tmp207*tmp223*tmp4*tmp550*tmp6*tmp97)/tmp5 + tmp&
                          &17*tmp201*tmp203*tmp207*tmp223*tmp4*tmp595*tmp6*tmp97 + tmp201*tmp248*tmp249*tmp&
                          &252*tmp284*tmp6*tmp92*tmp97 + (tmp161*tmp17*tmp201*tmp248*tmp4*tmp6*(-tmp27 + (t&
                          &mp19 + tmp213 - 3*tmp3)/tmp201 + tmp228*tmp9 + tmp226*tmp93)*tmp97)/tmp5) - (32*&
                          &C0tF2F2LFL*Pi*tmp11*tmp146*tmp17*tmp37*tmp6*((tmp178*tmp547 + 2*(tmp300 + tmp19*&
                          &tmp547 + (tmp315*tmp494)/tmp6) + ((-4*tmp547)/tmp203 + (tmp18 + tmp59 + tmp595)/&
                          &tmp6)/tmp5)/tmp6 + tmp276*(tmp35*tmp494*(tmp40 + tmp521) + tmp19*tmp548 + tmp3*t&
                          &mp548 + (tmp257*tmp548 + (tmp149 + tmp537 + tmp554)/tmp6)/tmp5 + tmp18*tmp448*tm&
                          &p7) + tmp177*(tmp163*tmp319 + tmp404*(tmp149 + tmp165 + (8*tmp163)/tmp203 + tmp5&
                          &46) + (2*(tmp165 + tmp473 + tmp546))/tmp203 + tmp163*tmp58 + tmp732/tmp6) + tmp1&
                          &81*(tmp161 + tmp473 + tmp546 + tmp556 + tmp495*tmp88) - 16*tmp315*tmp98))/tmp5 +&
                          & C0sF2P2FNP*f2p*Pi*tmp4*tmp5*tmp599*tmp6*tmp92*tmp97*(-2*tmp167 + (-14*tmp20 - 2&
                          &*tmp34*(tmp28 + tmp35) + tmp219*(tmp33 + tmp36) + 2*tmp218*(tmp36 + tmp515) + (5&
                          &1*tmp29)/tmp6)/tmp201 - tmp27*(tmp12 + (4*(tmp28 + 1/tmp6))/tmp203 + (tmp140 + t&
                          &mp688)/tmp5) + tmp26*(tmp2*tmp220 - 6*tmp29*(tmp1 + tmp42) + tmp218*(tmp16 + tmp&
                          &525) + tmp685 + (tmp530*tmp710)/tmp5) + (tmp170*(tmp30 + tmp31) + tmp328 + tmp37&
                          &2*(tmp41 + tmp507) - (11*tmp3)/tmp6)*tmp9 + (tmp30 + 10/tmp5 + 1/tmp6)*tmp98) + &
                          &(DB0tNN*Pi*tmp136*tmp146*tmp148*tmp17*tmp37*tmp5*tmp6*(16*tmp27*(512*tmp29 - 64*&
                          &tmp3*tmp493 + tmp43*tmp562 + (tmp518*(tmp151 + 21/tmp6))/tmp5) + tmp177*(5120*tm&
                          &p20 - 512*tmp156*tmp29 + tmp18*tmp565 + tmp12*(81*tmp18 + tmp505 + 384/(tmp203*t&
                          &mp6)) - (32*(tmp526/tmp203 + tmp58 + tmp585))/(tmp5*tmp6)) + (-64*tmp20*(tmp151 &
                          &+ tmp496) + tmp256*tmp3*(tmp153 + 83*tmp18 + tmp499) + tmp520 + tmp21*tmp567 + t&
                          &mp158*tmp372*tmp598 + tmp577*(tmp138 + tmp596 + tmp690))/tmp6 + (4*(1024*tmp152 &
                          &+ ((tmp154 + tmp155 + tmp160)*tmp18)/tmp5 - 256*tmp20*tmp568 - tmp21*tmp570 + (t&
                          &mp445*(tmp154 + tmp155 + tmp500))/tmp6 + tmp577*(tmp153 + tmp571 + tmp699)))/tmp&
                          &201 + 32*(tmp149 + tmp504 - 16/(tmp5*tmp6))*tmp98))/2. + (DB0tLL*Pi*tmp146*tmp14&
                          &8*tmp161*tmp17*tmp37*tmp6*(-16*tmp27*((tmp262 + tmp31)*tmp498 + (tmp143 + tmp320&
                          & + tmp449 + tmp508)/tmp6 + (88*tmp269 + 64*tmp494 + tmp551/tmp6 + tmp643)/tmp5) &
                          &+ (tmp18*(tmp170*(tmp42 + tmp511) + tmp18*(tmp31 + tmp512) + (tmp35*tmp519)/tmp2&
                          &03) + tmp315*tmp566 - 16*tmp29*((8*tmp315)/tmp203 + (tmp324 + tmp512)/tmp6) + tm&
                          &p178*(tmp153*tmp315 + (16*(tmp140 + tmp512))/(tmp203*tmp6) + tmp18*(tmp514 + 102&
                          &*tmp7)) + (tmp316*(tmp315*tmp585 + (tmp513 + 30*tmp7)/(tmp203*tmp6) + tmp530*tmp&
                          &735))/tmp5)/tmp6 - (4*(-128*tmp29*((tmp518 + tmp528)/tmp6 + 4/(tmp203*tmp604)) +&
                          & (256*tmp20)/tmp604 + tmp259*(tmp505/tmp604 + tmp18*(tmp512 + tmp664) + tmp579*(&
                          &tmp515 - 14*tmp7)) + (tmp208*(tmp153*(tmp150 + tmp408) + (128*tmp553)/(tmp203*tm&
                          &p6) + tmp18*(tmp667 + 74*tmp7)))/tmp6 + tmp18*(tmp143*tmp315 + (tmp511 + tmp513)&
                          &/(tmp203*tmp6) + tmp530*(tmp150 + tmp87))))/tmp201 + ((320*tmp21)/tmp203 - 256*t&
                          &mp29*(tmp150 + tmp523) + (tmp153*tmp509)/tmp6 + tmp498*(tmp163*tmp524 + (tmp514 &
                          &- 26*tmp7)/tmp6) + 2*tmp21*(tmp513 + 38*tmp7) - (16*(tmp510 + tmp603*(tmp526 - 3&
                          &0*tmp7) + tmp18*(tmp649 + tmp87)))/tmp5)*tmp9 + 32*((-8*tmp163)/tmp5 + tmp509/tm&
                          &p6)*tmp98))/tmp5 - 4*DB0tLN*Pi*tmp146*tmp148*tmp17*tmp37*tmp46*tmp6*((-64*tmp20*&
                          &(tmp36 + tmp40 + tmp518) + tmp656 + tmp697 + tmp178*(tmp519*tmp579 + tmp301*(1/t&
                          &mp6 - 17*tmp7) + tmp153*tmp7) + (tmp316*(tmp315*tmp473 + tmp260*(tmp382 + tmp496&
                          &) + tmp170*tmp706))/tmp5 + (tmp153 + (16*(tmp40 + tmp518))/tmp203 + tmp42*tmp522&
                          &)*tmp72)/tmp6 + 16*tmp27*(256*tmp29 - 16*(tmp158 + tmp262)*tmp3 + ((-4*tmp517)/t&
                          &mp203 + (tmp264 + tmp7)/tmp6)/tmp6 + tmp24*(tmp516 + tmp542 + tmp684 + tmp729)) &
                          &+ (4*(tmp520 - 128*tmp20*(tmp36 + tmp40 + tmp521) + tmp445*(tmp153*(tmp40 + tmp4&
                          &2) + (tmp513 + tmp54)*tmp579 + tmp18*(tmp382 + 52/tmp6)) + tmp43*tmp663 + (tmp52&
                          &2*tmp558 + (tmp446 + 93/tmp6)*tmp603 + tmp18*(67/tmp6 + tmp666))/(tmp5*tmp6) + t&
                          &mp328*(tmp153 + tmp31*(tmp262 + tmp496) + (8*(tmp140 + tmp87))/tmp203)))/tmp201 &
                          &+ tmp177*(2560*tmp20 + (160*tmp21)/tmp203 - 64*tmp29*(tmp523 + tmp524 + tmp525) &
                          &+ tmp12*(tmp153 + 79*tmp18 - 13*tmp269 - 48*tmp494 + 264/(tmp203*tmp6)) + (tmp13&
                          &8*tmp517)/tmp6 + tmp21*tmp646 + tmp13*(tmp138*tmp522 + tmp579*tmp648 + tmp18*(99&
                          &/tmp6 + tmp87))) + 32*(tmp498 + (tmp550 - 20/tmp6)/tmp5 + tmp517/tmp6)*tmp98) + &
                          &C0sF2P2FLP*f2p*Pi*tmp4*tmp446*tmp6*tmp92*tmp97*(2*tmp167 + tmp26*(tmp104 + tmp17&
                          &3*tmp19 - 4*tmp173*tmp372 + tmp338*(tmp540 + 1/tmp6) + tmp3*(tmp112 + tmp546 + (&
                          &5*tmp60)/tmp203)) + tmp27*(tmp171 + tmp259 + (4*tmp60)/tmp203 + (tmp36 + 3*tmp60&
                          &)/tmp5 + tmp686) + (-6*tmp20 + tmp178*(tmp174 + tmp175 + tmp529) + tmp29*(tmp36 &
                          &+ 5*tmp60) + tmp170*(tmp176 + 5*tmp19 + tmp61) - tmp372*(tmp309 + tmp506*tmp60 +&
                          & tmp79))/tmp201 + (tmp194 + (tmp112 + tmp168 + tmp175)/tmp5 + tmp3*(tmp16 - 5*tm&
                          &p60) + tmp257*(tmp169 + tmp176 + (3*tmp60)/tmp203))*tmp9 - (tmp30 + tmp49 + tmp6&
                          &0)*tmp98) + 16*C0tF2F2NFL*Pi*tmp146*tmp17*tmp37*tmp46*tmp6*(tmp181*(tmp168 + tmp&
                          &306 + tmp469 + tmp473 + tmp109*(tmp36 + tmp518 + tmp535) + tmp669) + (tmp187*(tm&
                          &p149 + tmp168 + tmp320) + tmp29*tmp550 + tmp18*tmp670 + (tmp21 + tmp19*tmp550 + &
                          &(16*tmp494)/tmp6 + tmp691*tmp7)/tmp5)/tmp6 + tmp177*(tmp549 + tmp552 + tmp3*(tmp&
                          &550 + tmp551 - 88/tmp6) + tmp674 + tmp676 + (52*tmp269 + tmp554 + tmp555 + tmp55&
                          &6 + tmp727)/tmp5) + (-16*tmp29*(tmp40 + tmp41 + tmp521) + tmp557 + tmp404*((2*(t&
                          &mp532 + tmp546 + tmp560))/tmp203 + (tmp473 + tmp532 + tmp584)/tmp6 + tmp58*tmp65&
                          &8) + tmp544*tmp677 + tmp178*(tmp532 + tmp558 + tmp559 + tmp581 + tmp59 + tmp693)&
                          & + tmp579*tmp7*(tmp256 + tmp7) + tmp43*tmp7*tmp88)/tmp201 - 16*(tmp545 + tmp7)*t&
                          &mp98) + 2*logN2L2*Pi*tmp102*tmp148*tmp17*tmp201*tmp37*tmp46*tmp5*tmp582*((tmp26*&
                          &tmp404*tmp682*tmp723)/tmp6 + (2048/tmp5**7 + tmp21*tmp494*(tmp18 + (2*tmp539)/tm&
                          &p203) - 256*(tmp151 + tmp157)*tmp591 + 128*tmp152*(tmp138 + tmp269 + tmp541 + tm&
                          &p560 + 56/(tmp203*tmp6)) + (tmp18*(tmp18*(tmp149 + tmp469 + tmp592) + tmp530*(tm&
                          &p149 + tmp470 + tmp597) - (8*(tmp162 + tmp59))/(tmp203*tmp6)))/tmp5 - 16*tmp20*(&
                          &(4*(73*tmp18 + tmp546 + tmp599))/tmp203 + (176*tmp19)/tmp6 + (tmp593 + tmp598 + &
                          &tmp692)/tmp6) + (tmp58*(tmp310 + tmp595 + tmp596) + (tmp536 + tmp589 + tmp597)*t&
                          &mp603 + tmp18*(tmp336 + tmp594 + tmp705))*tmp72 + (tmp3*(tmp531*(tmp470 + tmp572&
                          & + tmp593) + tmp603*(tmp149 - 76*tmp269 + tmp700) + tmp18*(-53*tmp11 + tmp593 + &
                          &tmp736)))/tmp6)/tmp201 + tmp181*(2048*tmp152 - 256*(tmp140 + tmp16)*tmp20 - 16*t&
                          &mp3*((4*tmp588)/tmp203 + (tmp470 + tmp584 + tmp585)/tmp6) + tmp18*((8*tmp539)/tm&
                          &p203 + tmp291/tmp6)*tmp7 + (tmp35*((8*tmp587)/tmp203 + (tmp149 + tmp583 + tmp701&
                          &)/tmp6))/tmp5 + tmp577*(tmp166 + tmp571 + tmp599 + tmp741)) + (-512*tmp152*tmp56&
                          &2 + 5120*tmp591 + tmp566*(tmp138 + 45*tmp18 + tmp470 + tmp586 + 164/(tmp203*tmp6&
                          &)) + (tmp316*((tmp162 + tmp171)*tmp18 + tmp530*tmp587 + (tmp257*(tmp161 + tmp18 &
                          &- 31*tmp269))/tmp6))/tmp5 - 32*tmp29*((72*tmp19)/tmp6 + (tmp466 + tmp541 + tmp59&
                          &0)/tmp6 + (4*(tmp57 + tmp589 + tmp695))/tmp203) + tmp43*tmp7*(tmp21 + tmp539*tmp&
                          &58 + tmp603*(tmp316 + tmp7)) + tmp319*(((-15*tmp11 + 25*tmp18 + 72*tmp269)*tmp35&
                          &)/tmp203 + tmp58*tmp588 + tmp18*(24*tmp18 + 17*tmp269 + tmp732)))*tmp9 + (8*(tmp&
                          &269*tmp539 + tmp577 - (64*tmp3)/tmp6 + (tmp143 + tmp541 + tmp546)*tmp8)*tmp98)/t&
                          &mp345) + C0tF2F20F0*Pi*tmp146*tmp284*tmp4*tmp5*((2*f2p*(4*tmp27 + (tmp18 + tmp19&
                          &3 + tmp334 + tmp406 + tmp574)/tmp201 + tmp185/tmp6 + (tmp109 + tmp316 + tmp36)*t&
                          &mp9))/tmp102 + tmp4*tmp7*((tmp170 + tmp178 + tmp180)*tmp43 + (tmp35*(7*tmp19 + t&
                          &mp318 + (-14/tmp203 + tmp31)/tmp5))/tmp201 - 2*tmp27*(tmp495 + tmp575) - 4*(tmp1&
                          &19 + tmp156/tmp203 + tmp126*tmp436)*tmp9 + 16*tmp98)) + logF2t*Pi*tmp146*tmp4*tm&
                          &p5*tmp6*tmp7*((f2p*tmp197*tmp316*(tmp196 + tmp497))/tmp102 + tmp4*tmp7*(64*tmp27&
                          &*tmp497 + tmp177*(tmp153 + tmp498 + tmp499 - (64*tmp313)/tmp5 + tmp500) + tmp18*&
                          &(tmp174 + tmp18 - (6*tmp2)/tmp5 + tmp501 + tmp603) + (tmp276*(tmp153 + tmp473 + &
                          &tmp498 + 26/(tmp203*tmp6) + tmp404*(15/tmp6 + tmp725)))/tmp6 + 64*tmp98)) + logb&
                          &eN2*Pi*tmp146*tmp4*tmp5*tmp541*tmp582*tmp92*((-16*f2p*tmp642)/(tmp102*tmp6) + tm&
                          &p4*tmp7*(tmp177*(640*tmp29 - 16*tmp3*(tmp157 + tmp524) + tmp32*tmp565 + tmp109*(&
                          &tmp138 + tmp589 + 108/(tmp203*tmp6))) + 16*tmp27*(tmp13*tmp325 + tmp504 + tmp562&
                          &/tmp6) + (4*(tmp142 - 32*tmp29*tmp568 + tmp18*tmp570 + tmp259*(tmp153 + tmp571 +&
                          & tmp572) + (tmp208*(tmp160 + tmp573 + 224/(tmp203*tmp6)))/tmp6))/tmp201 + (-16*t&
                          &mp128*tmp29 + tmp566 + tmp43*tmp567 + tmp3*(tmp505 + tmp559 + tmp590) + (tmp518*&
                          &(tmp139 + tmp530 + tmp702))/tmp5)/tmp6 + 32*(tmp109 + tmp290)*tmp98)) + 4*C0tF2F&
                          &2NF0*Pi*tmp11*tmp146*tmp4*tmp5*tmp6*tmp92*((f2p*(4*tmp147*tmp27 + (tmp185*tmp545&
                          &)/tmp6 + tmp258*(tmp319 + (tmp1 + tmp32)/tmp6 + (tmp36 + tmp665)/tmp5) + (tmp128&
                          &*tmp259 - 16*tmp29 + (tmp18 + tmp193 + tmp574)/tmp6 + (tmp18 + tmp65)*tmp8)/tmp2&
                          &01))/tmp102 + tmp4*tmp7*(-2*tmp27*(tmp306 + (32*tmp2)/tmp5 + tmp576) - tmp21*(tm&
                          &p170 - 6*tmp3 + 1/(tmp5*tmp6)) + tmp276*(48*tmp20 - 32*tmp29*(tmp35 + tmp44) + t&
                          &mp580 + tmp3*(tmp558 + tmp559 + tmp581) + (tmp208*(tmp138 + tmp473 + 18/(tmp203*&
                          &tmp6)))/tmp6) - 4*((tmp140 + tmp41)*tmp445 + tmp577 + tmp578 + tmp208*(tmp138 + &
                          &tmp149 + tmp579))*tmp9 + 16*tmp545*tmp98)) + (logbeL2*Pi*tmp146*tmp148*tmp242*tm&
                          &p4*tmp582*tmp92*((-16*f2p*tmp147*tmp642)/(tmp102*tmp6) + tmp4*tmp7*((4*(-64*tmp2&
                          &9*(8*tmp18 + tmp453 + tmp479 + tmp579) + tmp142*tmp658 + tmp18*tmp663 + tmp259*(&
                          &(tmp515 + tmp54)*tmp579 + tmp153*tmp658 + tmp18*(tmp382 + tmp664)) + (tmp208*(tm&
                          &p153*(tmp150 + tmp528) + tmp499*(tmp40 + tmp665) + tmp18*(tmp666 + tmp667)))/tmp&
                          &6))/tmp201 + tmp177*(128*tmp29*(tmp150 + tmp535) + tmp32*((160*tmp18)/tmp203 + t&
                          &mp138*tmp517 + tmp18*tmp646) + tmp109*(tmp138*tmp553 + tmp603*tmp648 + tmp18*(tm&
                          &p40 + tmp649)) - 16*tmp3*(tmp524*tmp553 + (tmp514 - 13*tmp7)/tmp6)) + 16*tmp27*(&
                          &tmp163*tmp498 + (tmp625 + tmp644/tmp6)/tmp6 + (44*tmp269 + tmp643 + tmp291*tmp72&
                          &5)/tmp5) + tmp32*(tmp566*tmp60 + tmp656 + (tmp316*(tmp585*tmp60 + (tmp513 + tmp6&
                          &20)/(tmp203*tmp6) + tmp530*tmp657))/tmp5 + tmp178*((16*(tmp140 + tmp382))/(tmp20&
                          &3*tmp6) + tmp153*tmp60 + tmp18*(tmp514 + 51*tmp7)) - 16*tmp29*((8*tmp60)/tmp203 &
                          &+ tmp738/tmp6)) + 32*(tmp32*tmp517 + tmp109*tmp553)*tmp98)))/tmp604 + C0tF2F2LF0&
                          &*Pi*tmp146*tmp4*tmp446*tmp6*tmp92*((f2p*((tmp185*tmp60)/tmp6 + (4*tmp27)/tmp604 &
                          &+ (-4*tmp19*tmp517 + tmp445*tmp517 + (tmp224*tmp60)/tmp6 + tmp18/tmp604 + ((8*tm&
                          &p517)/tmp203 + tmp60*tmp665)/tmp5)/tmp201 + tmp258*(tmp61 + tmp624 + tmp668)))/t&
                          &mp102 + tmp4*tmp7*(-2*tmp27*(tmp168 + tmp469 + tmp473 + tmp495/tmp604 + tmp669) &
                          &- tmp21*tmp673 + (tmp170*tmp677 + tmp178*tmp677 + tmp404*((tmp301 + tmp57 + tmp5&
                          &86)/tmp6 + (2*tmp677)/tmp203) + tmp579*tmp658*tmp7 + tmp18*tmp7*tmp88)/tmp201 - &
                          &2*(tmp552 + tmp319*tmp553 + tmp674 + tmp676 + tmp404*(tmp149 + tmp168 + tmp542 +&
                          & tmp57 + tmp678))*tmp9 + 16*tmp60*tmp98)) + log4*Pi*tmp11*tmp148*tmp17*tmp225*tm&
                          &p4*tmp5*tmp6*(-((f2p*tmp682*(-4*tmp167*tmp683 - (4*tmp26*((tmp174 + tmp18 + tmp1&
                          &95)*tmp3 + tmp231*(tmp174 + tmp18 + tmp333) + tmp219*(tmp324 + tmp41) + tmp338*(&
                          &1/tmp203 + tmp496) + tmp685))/tmp201 - 4*tmp27*(20*tmp29 + (tmp1 + tmp264)*tmp3 &
                          &+ (tmp112 + tmp18 + tmp195)/tmp5 + (tmp141 + tmp18 + tmp686)/tmp203) + 4*tmp708 &
                          &+ (60*tmp20 + tmp125*(11/tmp203 + 10/tmp6) + 4*tmp372*(tmp18 + tmp574 - 15/(tmp2&
                          &03*tmp6)) + tmp170*(tmp149 + 30*tmp19 + tmp684) + tmp3*(-96*tmp19 - 44/(tmp203*t&
                          &mp6) + tmp691))*tmp9 + (tmp19*tmp502 + tmp687 + tmp29*(tmp688 + tmp689) + tmp3*(&
                          &tmp153 + tmp18 + tmp690) + (tmp404*(tmp18 + tmp58 + tmp716))/tmp203)*tmp93 + (tm&
                          &p18 + 60*tmp19 + 60*tmp3 + tmp49*(tmp41 + 1/tmp6) + tmp684)*tmp98))/tmp92) + (tm&
                          &p4*tmp7*(tmp27*(-384*tmp20 - 128*tmp29*(tmp256 + tmp38 + tmp40) + tmp319*(tmp164&
                          & - 26*tmp269 + tmp469 + tmp533 + tmp524/tmp6) + tmp269*(tmp312 + tmp693 + tmp694&
                          &) + tmp24*((tmp547 + tmp695)/tmp6 - (4*tmp696)/tmp203)) + (-64*tmp152*(tmp284 + &
                          &tmp324) - 128*tmp591 - 4*tmp29*(192*tmp34 + tmp153*tmp517 - (8*(tmp157 + tmp512)&
                          &)/(tmp203*tmp6) + (14*tmp18 + 41*tmp269 + tmp536)/tmp6) + tmp19*tmp269*(-11*tmp1&
                          &8 + tmp693 + tmp694) + tmp3*(128*tmp34*tmp509 + (tmp316*(51*tmp269 + tmp538 + tm&
                          &p57))/tmp203 + tmp21*tmp645 + tmp58*(tmp149 + tmp309 + tmp701)) + 4*tmp372*(tmp2&
                          &1*tmp384 + (tmp176 + tmp18 + tmp700)/(tmp203*tmp6) + tmp704) + 8*tmp20*(42*tmp26&
                          &9 + tmp57 - 136/(tmp203*tmp6) + tmp703 + tmp707))/tmp201 + tmp26*(64*tmp591 - 64&
                          &*tmp152*(tmp28 + tmp40 - 15/tmp6) + tmp19*tmp269*(tmp301 + tmp453 + tmp702) + tm&
                          &p687*(-123*tmp18 - 80*tmp269 + tmp532 + tmp317*tmp657 + tmp703) + tmp372*(tmp21*&
                          &tmp620 + (tmp494*(tmp264 + 20*tmp7))/tmp6 + tmp704) + tmp3*(32*tmp34*tmp509 - 20&
                          &*tmp21*tmp7 + tmp530*(tmp143 + tmp593 + tmp701) + (tmp32*(30*tmp18 + 101*tmp269 &
                          &+ tmp705))/tmp203) + tmp29*(-192*tmp34 + (62*tmp18 + 159*tmp269 + tmp536)/tmp6 +&
                          & tmp555*tmp706 + (4*(tmp11 + 48*tmp269 + tmp707))/tmp203)) + (tmp335*tmp493 + tm&
                          &p328*(-10*tmp18 + 72*tmp19 + tmp271 - (4*tmp509)/tmp203 + tmp541) + tmp697 + tmp&
                          &3*(-192*tmp19*tmp509 + (8*(tmp470 + tmp560 + tmp698))/tmp203 + tmp35*(-13*tmp11 &
                          &+ tmp620/tmp6 + tmp699)) + (tmp494*(tmp572 - 78/(tmp203*tmp6) + tmp688*tmp7))/tm&
                          &p6 + (tmp21*tmp40 + (tmp257*(tmp139 + tmp161 + 21*tmp269))/tmp6 + tmp112*tmp696)&
                          &*tmp8)*tmp9 + (192*tmp29 - 32*tmp3*tmp509 + tmp269*(tmp242 - 13/tmp6) + tmp24*tm&
                          &p696)*tmp98))/tmp97) + logF2N2*Pi*tmp11*tmp146*tmp17*tmp201*tmp225*tmp4*tmp5*tmp&
                          &6*((f2p*tmp276*(-2*(32/tmp5 + 1/tmp6 + tmp688)*tmp708 + tmp167*(120*tmp19 + 216*&
                          &tmp3 + tmp43 + tmp579 + tmp237*(1/tmp6 + tmp709)) + (104*tmp591 + tmp19*tmp445*(&
                          &38*tmp19 + tmp473 + 21/(tmp203*tmp6)) + (tmp194*(tmp195 + tmp301 + tmp686))/tmp2&
                          &03 + 4*tmp152*(1/tmp6 + tmp712) + tmp237*(tmp16 + tmp42)*tmp713 + (tmp312 + tmp5&
                          &8 - 8/(tmp203*tmp6))*tmp713 + 3*tmp20*(-9*tmp18 + tmp508 + tmp714))/tmp201 + tmp&
                          &177*(-144*tmp152 + tmp20*(128/tmp203 + tmp496) + tmp29*(tmp138 + tmp159 + tmp538&
                          &) + tmp398*(tmp43 + tmp58 + tmp715) + tmp376*(18*tmp19 + tmp336 + tmp716) + tmp3&
                          &4*(tmp333 + tmp473 + tmp717)) + 8*tmp718 + tmp181*(220*tmp20 + tmp19*tmp312 + tm&
                          &p256*tmp3*(tmp42 + tmp506) + 4*tmp372*(tmp460 + tmp503 + tmp711) + 60*tmp713 + t&
                          &mp710*tmp72) - (16*tmp152 + tmp313*tmp34*tmp495 - tmp218*(tmp143 + tmp505 + 16/(&
                          &tmp203*tmp6)) + (tmp2*tmp220)/tmp6 + tmp20*(tmp665 + tmp712) + tmp194*(tmp333 + &
                          &tmp460 + tmp79))*tmp93 - (400*tmp29 + tmp178*(152/tmp203 + tmp42) + (5*(tmp130 +&
                          & tmp153 + tmp43))/tmp203 + tmp49*(tmp43 + tmp714 + tmp715))*tmp98))/(tmp102*tmp9&
                          &2) + (tmp4*tmp7*(-2*tmp167*(960*tmp29 + tmp498*(68/tmp203 - 3*tmp60) + tmp13*(tm&
                          &p11 + 29*tmp18 + 384*tmp19 - 12*tmp269 + (56*tmp657)/tmp203) + (23*tmp18 + 192*t&
                          &mp19 + 176/(tmp203*tmp6))*tmp7) + 64*tmp718*tmp719 + (tmp518*tmp534*tmp723)/tmp5&
                          & + 32*tmp708*(tmp128*tmp7 + tmp724 + tmp404*(tmp496 + tmp7 + tmp725)) + tmp177*(&
                          &640*tmp591 + 64*tmp152*(tmp52 - 18/tmp6) + (tmp494*((12*tmp18)/tmp203 + tmp21 + &
                          &32*tmp34 + (40*tmp19)/tmp6))/tmp6 - 8*tmp20*(272*tmp19 + tmp336 + tmp532 - (4*(t&
                          &mp284 + 71/tmp6))/tmp203 + tmp729) + tmp208*((tmp170*(38*tmp269 + tmp57 + tmp589&
                          &))/tmp6 + (256*tmp713)/tmp6 + tmp40*tmp730 + (tmp139*(tmp139 + tmp57 + tmp731))/&
                          &tmp203 + 8*tmp34*(tmp594 + tmp732 + tmp733)) + tmp178*(tmp18*(tmp11 + tmp271 + t&
                          &mp460) + (tmp35*(tmp572 + tmp593 + tmp692))/tmp203 + tmp530*(60*tmp18 + tmp469 +&
                          & tmp507*tmp7) + 48*tmp34*tmp735) + tmp383*((tmp157 + tmp382)*tmp555 + (tmp308 + &
                          &tmp473 + tmp532)/tmp6 - (4*(tmp541 + tmp7*tmp728 + tmp734))/tmp203 + tmp737)) + &
                          &tmp27*(1280*tmp152 - 832*tmp20*(tmp16 + tmp290) + tmp40*(188*tmp18*tmp19 + (30*t&
                          &mp21)/tmp203 + (288*tmp34)/tmp6 + 64*tmp713 + tmp730) + tmp328*(144*tmp19 - 17*t&
                          &mp269 + 44*tmp494 + 80/(tmp203*tmp6) + tmp64 + tmp736) + tmp445*(tmp138*(58/tmp6&
                          & + tmp620) + tmp737 + tmp579*tmp738 + (-25*tmp269 + tmp543 + tmp739)/tmp6) + tmp&
                          &24*(tmp18*(tmp18 + tmp541 + tmp59) + 16*tmp34*(tmp382 + 33/tmp6) + tmp530*(69*tm&
                          &p18 + tmp554 + tmp66) + 128*tmp713 + (tmp584 + tmp740 + tmp741)/(tmp203*tmp6))) &
                          &+ tmp26*tmp434*(-128*tmp152*(21/tmp203 + tmp553) + 896*tmp591 + tmp43*tmp494*(tm&
                          &p174 + tmp18 + tmp715) + 8*tmp20*(336*tmp19 + tmp543 + tmp57 + tmp586 + (8*(tmp4&
                          &08 + tmp728))/tmp203) + (24*tmp34*(tmp11 + tmp149 + tmp449) + (tmp170*(tmp529 + &
                          &tmp64 + tmp726))/tmp6 + tmp7*tmp730 + (tmp139*(tmp139 + tmp536 + tmp731))/tmp203&
                          &)/tmp5 + tmp338*(448*tmp34 + tmp505*(tmp157 + tmp528) + (tmp460 + tmp382/tmp6 + &
                          &tmp64)/tmp6 - (4*(27*tmp18 + tmp305 + tmp739))/tmp203) + tmp178*(tmp460*tmp547 +&
                          & (tmp149*(tmp382 - 12/tmp6))/tmp203 + tmp530*(tmp310 + tmp572 + tmp740) + 32*tmp&
                          &34*(tmp496 + tmp87))) - 4*(256*tmp20 + (36/tmp203 + tmp291)*tmp328 + tmp40*((40*&
                          &tmp18)/tmp203 + 3*tmp21 + 64*tmp34 + (120*tmp19)/tmp6) + tmp445*(400*tmp19 + tmp&
                          &269 + tmp541 + tmp727 + (4*(tmp550 + tmp728))/tmp203) + (512*tmp34 + (tmp308 + t&
                          &mp470 + tmp572)/tmp6 + tmp138*(51/tmp6 + 11*tmp7) + (4*(11*tmp269 + tmp532 + tmp&
                          &734))/tmp203)/tmp5)*tmp98))/tmp97) - (logmut*Pi*tmp4*tmp5*tmp6*(f2p*tmp518*tmp7*&
                          &(tmp196 + tmp257 + 1/tmp6 + tmp8) + tmp11*tmp4*(tmp12 + tmp138 + (8*(tmp10 + tmp&
                          &331))/tmp201 + tmp43 + 16*tmp9 - (8*tmp99)/tmp5)))/2. - 2*C0tF2F2NFN*Pi*tmp136*t&
                          &mp146*tmp17*tmp37*tmp5*tmp6*(tmp181*(tmp128*tmp495 + tmp576) + (2*(tmp142 - 128*&
                          &tmp2*tmp29 + tmp3*(tmp164 + tmp559 + tmp573) + tmp580 + (tmp208*(tmp143 + tmp153&
                          & + 50/(tmp203*tmp6)))/tmp6))/tmp201 + tmp258*(96*tmp29 + tmp578 + (tmp301 + tmp5&
                          &55 + tmp579)/tmp5 + tmp3*(20/tmp6 + tmp712)) - 16*(tmp109 + 1/tmp6)*tmp98 + (tmp&
                          &170*tmp18 - 32*tmp2*tmp29 + tmp335 + tmp178*(tmp138 + tmp560 + tmp725/tmp6) + (t&
                          &mp18*tmp99)/tmp5)/tmp6) + logmuF2*Pi*tmp17*tmp284*tmp4*tmp5*tmp6*((tmp22*tmp269*&
                          &tmp4)/tmp5 + f2p*tmp23*(tmp15 + tmp177 + tmp3 + tmp313*tmp436 + tmp434*(tmp137 +&
                          & tmp99))) + C0tP2P20P0*Pi*tmp148*tmp4*tmp40*tmp5*(tmp4*tmp7*(tmp335 + tmp18*tmp5&
                          &30 + tmp29*(-128/tmp203 + 78/tmp6) + (4*(-3*tmp3*tmp330 + tmp460/tmp203 + tmp577&
                          & + (17/tmp203 + 1/tmp6)/(tmp5*tmp6)))/tmp201 + (tmp143 + tmp195 + tmp337)/(tmp5*&
                          &tmp6) + tmp3*(-48*tmp18 + 96*tmp19 + tmp694) + (96*tmp3 + tmp336 - 34/(tmp5*tmp6&
                          &))*tmp9) - (4*f2p*(tmp194 + tmp332 + (tmp139 + tmp174 + tmp333)*tmp404 + tmp259*&
                          &tmp561 + tmp177*(-6/tmp5 + 1/tmp6) + (tmp319 + tmp334 + tmp32*tmp99)/tmp201))/tm&
                          &p345) + 4*D0IR6tsN*Pi*tmp11*tmp4*tmp5*tmp6*tmp92*(f2p*(-2*tmp27*tmp545 + tmp26*(&
                          &-(tmp2/tmp203) + tmp35/tmp5 + tmp501)*tmp545 + tmp434*((tmp188*tmp35)/tmp203 + t&
                          &mp178*(tmp36 - 29/tmp6) + tmp72 + (tmp18 + tmp508 + tmp79)/tmp5) + tmp545*tmp683&
                          &*tmp9) + (tmp4*tmp7*(tmp181 - tmp26*(tmp501 + tmp544 + tmp208/tmp6) - 6*tmp9*tmp&
                          &94 + (tmp174 + tmp178 + tmp99/tmp5)/tmp201))/tmp345) + C0tP2P2LP0*Pi*tmp148*tmp4&
                          &*tmp550*tmp6*tmp92*(tmp4*tmp7*(tmp335*(tmp528 + 1/tmp6) + 2*tmp21*tmp670 + (tmp1&
                          &8*(tmp11 + tmp143 + tmp308) + (tmp257*(tmp18 + tmp57 + tmp586))/tmp6 + tmp19*tmp&
                          &680)/tmp5 + tmp383*(tmp160 + tmp305 + tmp532 + tmp712/tmp604) + (2*(-(tmp21*tmp5&
                          &40) + tmp549/tmp604 + tmp178*(tmp11 + (16*tmp291)/tmp203 + tmp310 + tmp681) + ((&
                          &tmp171 + tmp538 + tmp57)/tmp203 + (tmp168 + tmp18 + tmp541)/tmp6)*tmp8))/tmp201 &
                          &+ tmp259*((tmp449 + tmp57 + tmp572)/tmp203 + tmp674 + tmp32*(tmp11 + 12*tmp18 + &
                          &tmp80)) + (4*tmp21 + tmp498*tmp553 + tmp680/tmp5)*tmp9) - (2*f2p*(tmp332*tmp60 +&
                          & tmp194/tmp604 + tmp259*(tmp31/tmp604 + tmp61) + tmp404*(tmp170*tmp517 + tmp139/&
                          &tmp604 + tmp657/(tmp203*tmp6)) + (tmp13*tmp517 + tmp35*tmp60)*tmp9 + (tmp319*tmp&
                          &60 + (tmp320 + tmp449 + tmp678 + tmp691)/tmp5 + tmp32*tmp60*tmp99)/tmp201))/tmp3&
                          &45) + logF2P2*Pi*tmp201*tmp203*tmp225*tmp4*tmp5*tmp6*tmp87*tmp92*(tmp4*tmp7*(tmp&
                          &103/tmp5 + tmp209*tmp372*tmp64 + (tmp178*tmp19 - 2*tmp20 + tmp19*tmp269 + tmp383&
                          &/tmp203 + (-2*tmp34 + tmp31*tmp494 + tmp64/tmp203)/tmp5)/tmp201 + (6*tmp219 + tm&
                          &p359 + tmp376 + tmp32*tmp494)*tmp9 - (6*tmp27*tmp94)/tmp5)*(-(tmp27*(tmp16 + tmp&
                          &367)) + (2*tmp218 + 7*tmp219 - 4*tmp34 + tmp375)/tmp201 + tmp226*tmp534 + (tmp17&
                          &4 + 5*tmp3 + tmp372)*tmp9 + tmp98) + f2p*tmp197*tmp372*((tmp527*tmp534*tmp7)/tmp&
                          &5 + (tmp26*(-(tmp372*(tmp171 + tmp469 + 9*tmp494 + tmp530)) - 3*tmp29*tmp540 + t&
                          &mp3*(tmp112 - 13*tmp494 + tmp529 + tmp57) + tmp687 + tmp19*tmp313*tmp7))/tmp201 &
                          &+ tmp27*(24*tmp29 + (tmp2*tmp528)/tmp203 + tmp259*(tmp16 + tmp528) + (-26*tmp494&
                          & + tmp541 + tmp592 + tmp79)/tmp5) + (-16*tmp20 + (tmp36 + tmp382)*tmp383 + tmp37&
                          &2*(tmp168 + tmp469 + tmp512/tmp203 + tmp531) + tmp3*(tmp138 + tmp532 + tmp533 + &
                          &tmp594) + tmp19*(tmp16 + tmp31)*tmp7)*tmp9 + tmp167/tmp92 + tmp98*(-16*tmp3 + (t&
                          &mp317 + tmp511)/tmp5 + tmp7*tmp99))) + C0tP2P2NP0*Pi*tmp4*tmp5*tmp6*tmp64*tmp92*&
                          &(tmp4*(18*tmp29 + tmp19*tmp316 + (tmp28 + tmp316)*tmp445 + tmp177*(tmp256 + 1/tm&
                          &p5) + (tmp164 + tmp170 + tmp193)/tmp5 + (4*(tmp130 + tmp318 + (tmp25 + 1/tmp6)/t&
                          &mp5))/tmp201)*tmp7 - 2*f2p*(tmp194 + tmp332 + (tmp195 + tmp530 + tmp598)/tmp5 + &
                          &tmp3*(tmp105 + 12/tmp6) + tmp177*(1/tmp6 + tmp8) + tmp434*(tmp319 + tmp99/tmp6 +&
                          & tmp8*tmp99)))

  END FUNCTION MP2MPL_MP_FIN_D10D22

  FUNCTION MP2MPL_MP_FIN_D11D22(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_FIN_d11d22
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206, tmp207, tmp208, tmp209, tmp210
  real tmp211, tmp212, tmp213, tmp214, tmp215
  real tmp216, tmp217, tmp218, tmp219, tmp220
  real tmp221, tmp222, tmp223, tmp224, tmp225
  real tmp226, tmp227, tmp228, tmp229, tmp230
  real tmp231, tmp232, tmp233, tmp234, tmp235
  real tmp236, tmp237, tmp238, tmp239, tmp240
  real tmp241, tmp242, tmp243, tmp244, tmp245
  real tmp246, tmp247, tmp248, tmp249, tmp250
  real tmp251, tmp252, tmp253, tmp254, tmp255
  real tmp256, tmp257, tmp258, tmp259, tmp260
  real tmp261, tmp262, tmp263, tmp264, tmp265
  real tmp266, tmp267, tmp268, tmp269, tmp270
  real tmp271, tmp272, tmp273, tmp274, tmp275
  real tmp276, tmp277, tmp278, tmp279, tmp280
  real tmp281, tmp282, tmp283, tmp284, tmp285
  real tmp286, tmp287, tmp288, tmp289, tmp290
  real tmp291, tmp292, tmp293, tmp294, tmp295
  real tmp296, tmp297, tmp298, tmp299, tmp300
  real tmp301, tmp302, tmp303, tmp304, tmp305
  real tmp306, tmp307, tmp308, tmp309, tmp310
  real tmp311, tmp312, tmp313, tmp314, tmp315
  real tmp316, tmp317, tmp318, tmp319, tmp320
  real tmp321, tmp322, tmp323, tmp324, tmp325
  real tmp326, tmp327, tmp328, tmp329, tmp330
  real tmp331, tmp332, tmp333, tmp334, tmp335
  real tmp336, tmp337, tmp338, tmp339, tmp340
  real tmp341, tmp342, tmp343, tmp344, tmp345
  real tmp346, tmp347, tmp348, tmp349, tmp350
  real tmp351, tmp352, tmp353, tmp354, tmp355
  real tmp356, tmp357, tmp358, tmp359, tmp360
  real tmp361, tmp362, tmp363, tmp364, tmp365
  real tmp366, tmp367, tmp368, tmp369, tmp370
  real tmp371, tmp372, tmp373, tmp374, tmp375
  real tmp376, tmp377, tmp378, tmp379, tmp380
  real tmp381, tmp382, tmp383, tmp384, tmp385
  real tmp386, tmp387, tmp388, tmp389, tmp390
  real tmp391, tmp392, tmp393, tmp394, tmp395
  real tmp396, tmp397, tmp398, tmp399, tmp400
  real tmp401, tmp402, tmp403, tmp404, tmp405
  real tmp406, tmp407, tmp408, tmp409, tmp410
  real tmp411, tmp412, tmp413, tmp414, tmp415
  real tmp416, tmp417, tmp418, tmp419, tmp420
  real tmp421, tmp422, tmp423, tmp424, tmp425
  real tmp426, tmp427, tmp428, tmp429, tmp430
  real tmp431, tmp432, tmp433, tmp434, tmp435
  real tmp436, tmp437, tmp438, tmp439, tmp440
  real tmp441, tmp442, tmp443, tmp444, tmp445
  real tmp446, tmp447, tmp448, tmp449, tmp450
  real tmp451, tmp452, tmp453, tmp454, tmp455
  real tmp456, tmp457, tmp458, tmp459, tmp460
  real tmp461, tmp462, tmp463, tmp464, tmp465
  real tmp466, tmp467, tmp468, tmp469, tmp470
  real tmp471, tmp472, tmp473, tmp474, tmp475
  real tmp476, tmp477, tmp478, tmp479, tmp480
  real tmp481, tmp482, tmp483, tmp484, tmp485
  real tmp486, tmp487, tmp488, tmp489, tmp490
  real tmp491, tmp492, tmp493, tmp494, tmp495
  real tmp496, tmp497, tmp498, tmp499, tmp500
  real tmp501, tmp502, tmp503, tmp504, tmp505
  real tmp506, tmp507, tmp508, tmp509, tmp510
  real tmp511, tmp512, tmp513, tmp514, tmp515
  real tmp516, tmp517, tmp518, tmp519, tmp520
  real tmp521, tmp522, tmp523, tmp524, tmp525
  real tmp526, tmp527, tmp528, tmp529, tmp530
  real tmp531, tmp532, tmp533, tmp534, tmp535
  real tmp536, tmp537, tmp538, tmp539, tmp540
  real tmp541, tmp542, tmp543, tmp544, tmp545
  real tmp546, tmp547, tmp548, tmp549, tmp550
  real tmp551, tmp552, tmp553, tmp554, tmp555
  real tmp556, tmp557, tmp558, tmp559, tmp560
  real tmp561, tmp562, tmp563, tmp564, tmp565
  real tmp566, tmp567, tmp568, tmp569, tmp570
  real tmp571, tmp572, tmp573, tmp574, tmp575
  real tmp576, tmp577, tmp578, tmp579, tmp580
  real tmp581, tmp582, tmp583, tmp584, tmp585
  real tmp586, tmp587, tmp588, tmp589, tmp590
  real tmp591, tmp592, tmp593, tmp594, tmp595
  real tmp596, tmp597, tmp598, tmp599, tmp600
  real tmp601, tmp602, tmp603, tmp604, tmp605
  real tmp606, tmp607, tmp608, tmp609, tmp610
  real tmp611, tmp612, tmp613, tmp614, tmp615
  real tmp616, tmp617, tmp618, tmp619, tmp620
  real tmp621, tmp622, tmp623, tmp624, tmp625
  real tmp626, tmp627, tmp628, tmp629, tmp630
  real tmp631, tmp632, tmp633, tmp634, tmp635
  real tmp636, tmp637, tmp638, tmp639, tmp640
  real tmp641, tmp642, tmp643, tmp644, tmp645
  real tmp646, tmp647, tmp648, tmp649, tmp650
  real tmp651, tmp652, tmp653, tmp654, tmp655
  real tmp656, tmp657, tmp658, tmp659, tmp660
  real tmp661, tmp662, tmp663, tmp664, tmp665
  real tmp666, tmp667, tmp668, tmp669, tmp670
  real tmp671, tmp672, tmp673, tmp674, tmp675
  real tmp676, tmp677, tmp678, tmp679, tmp680
  real tmp681, tmp682, tmp683, tmp684, tmp685
  real tmp686, tmp687, tmp688, tmp689, tmp690
  real tmp691, tmp692, tmp693, tmp694, tmp695
  real tmp696, tmp697, tmp698, tmp699, tmp700
  real tmp701, tmp702, tmp703, tmp704, tmp705
  real tmp706, tmp707, tmp708, tmp709, tmp710
  real tmp711, tmp712, tmp713, tmp714, tmp715
  real tmp716, tmp717, tmp718, tmp719, tmp720
  real tmp721, tmp722, tmp723, tmp724, tmp725
  real tmp726, tmp727, tmp728, tmp729, tmp730
  real tmp731, tmp732, tmp733, tmp734, tmp735
  real tmp736, tmp737, tmp738, tmp739, tmp740
  real tmp741, tmp742, tmp743, tmp744, tmp745
  real tmp746, tmp747, tmp748, tmp749, tmp750
  real tmp751, tmp752, tmp753, tmp754, tmp755
  real tmp756, tmp757, tmp758, tmp759, tmp760
  real tmp761, tmp762, tmp763, tmp764, tmp765
  real tmp766, tmp767, tmp768, tmp769, tmp770
  real tmp771, tmp772, tmp773, tmp774, tmp775
  real tmp776, tmp777, tmp778, tmp779, tmp780
  real tmp781, tmp782, tmp783, tmp784, tmp785
  real tmp786, tmp787, tmp788, tmp789, tmp790
  real tmp791, tmp792, tmp793, tmp794, tmp795
  real tmp796, tmp797, tmp798, tmp799, tmp800
  real tmp801, tmp802, tmp803, tmp804, tmp805
  real tmp806, tmp807, tmp808, tmp809, tmp810
  real tmp811, tmp812, tmp813, tmp814, tmp815
  real tmp816, tmp817, tmp818, tmp819, tmp820
  real tmp821, tmp822, tmp823, tmp824, tmp825
  real tmp826, tmp827, tmp828, tmp829, tmp830
  real tmp831, tmp832, tmp833, tmp834, tmp835
  real tmp836, tmp837, tmp838, tmp839, tmp840
  real tmp841, tmp842, tmp843, tmp844, tmp845
  real tmp846, tmp847, tmp848, tmp849, tmp850
  real tmp851, tmp852, tmp853, tmp854, tmp855
  real tmp856, tmp857, tmp858, tmp859, tmp860
  real tmp861, tmp862, tmp863, tmp864, tmp865
  real tmp866, tmp867, tmp868, tmp869, tmp870
  real tmp871, tmp872, tmp873, tmp874, tmp875
  real tmp876, tmp877, tmp878, tmp879, tmp880
  real tmp881, tmp882, tmp883, tmp884, tmp885
  real tmp886, tmp887, tmp888, tmp889, tmp890
  real tmp891, tmp892, tmp893, tmp894, tmp895
  real tmp896, tmp897, tmp898, tmp899, tmp900
  real tmp901, tmp902, tmp903, tmp904, tmp905
  real tmp906, tmp907, tmp908, tmp909, tmp910
  real tmp911, tmp912, tmp913, tmp914, tmp915
  real tmp916, tmp917, tmp918, tmp919, tmp920
  real tmp921, tmp922, tmp923, tmp924, tmp925
  real tmp926, tmp927, tmp928, tmp929, tmp930
  real tmp931, tmp932, tmp933, tmp934, tmp935
  real tmp936, tmp937, tmp938, tmp939, tmp940
  real tmp941, tmp942, tmp943, tmp944, tmp945
  real tmp946, tmp947, tmp948, tmp949, tmp950
  real tmp951, tmp952, tmp953, tmp954, tmp955
  real tmp956, tmp957, tmp958, tmp959, tmp960
  real tmp961, tmp962, tmp963, tmp964, tmp965
  real tmp966, tmp967, tmp968, tmp969, tmp970
  real tmp971, tmp972, tmp973, tmp974, tmp975
  real tmp976, tmp977, tmp978, tmp979, tmp980
  real tmp981, tmp982, tmp983, tmp984, tmp985
  real tmp986, tmp987, tmp988, tmp989, tmp990
  real tmp991, tmp992, tmp993, tmp994, tmp995
  real tmp996, tmp997, tmp998, tmp999, tmp1000
  real tmp1001, tmp1002, tmp1003, tmp1004, tmp1005
  real tmp1006, tmp1007, tmp1008, tmp1009, tmp1010
  real tmp1011, tmp1012, tmp1013, tmp1014, tmp1015
  real tmp1016, tmp1017, tmp1018, tmp1019, tmp1020
  real tmp1021, tmp1022, tmp1023, tmp1024, tmp1025
  real tmp1026, tmp1027, tmp1028, tmp1029, tmp1030
  real tmp1031, tmp1032, tmp1033, tmp1034, tmp1035
  real tmp1036, tmp1037, tmp1038, tmp1039, tmp1040
  real tmp1041, tmp1042, tmp1043, tmp1044, tmp1045
  real tmp1046, tmp1047, tmp1048, tmp1049, tmp1050
  real tmp1051, tmp1052, tmp1053, tmp1054, tmp1055
  real tmp1056, tmp1057, tmp1058, tmp1059, tmp1060
  real tmp1061, tmp1062, tmp1063, tmp1064, tmp1065
  real tmp1066, tmp1067, tmp1068, tmp1069, tmp1070
  real tmp1071, tmp1072, tmp1073, tmp1074, tmp1075
  real tmp1076, tmp1077, tmp1078, tmp1079, tmp1080
  real tmp1081, tmp1082, tmp1083, tmp1084, tmp1085
  real tmp1086, tmp1087, tmp1088, tmp1089, tmp1090
  real tmp1091, tmp1092, tmp1093, tmp1094, tmp1095
  real tmp1096, tmp1097, tmp1098, tmp1099, tmp1100
  real tmp1101, tmp1102, tmp1103, tmp1104, tmp1105
  real tmp1106, tmp1107, tmp1108, tmp1109, tmp1110
  real tmp1111, tmp1112, tmp1113, tmp1114, tmp1115
  real tmp1116, tmp1117, tmp1118, tmp1119, tmp1120
  real tmp1121, tmp1122, tmp1123, tmp1124, tmp1125
  real tmp1126, tmp1127, tmp1128, tmp1129, tmp1130
  real tmp1131, tmp1132, tmp1133, tmp1134, tmp1135
  real tmp1136, tmp1137, tmp1138, tmp1139, tmp1140
  real tmp1141, tmp1142, tmp1143, tmp1144, tmp1145
  real tmp1146, tmp1147, tmp1148, tmp1149, tmp1150
  real tmp1151, tmp1152, tmp1153, tmp1154, tmp1155
  real tmp1156, tmp1157, tmp1158, tmp1159, tmp1160
  real tmp1161, tmp1162, tmp1163, tmp1164, tmp1165
  real tmp1166, tmp1167, tmp1168, tmp1169, tmp1170
  real tmp1171, tmp1172, tmp1173, tmp1174, tmp1175
  real tmp1176, tmp1177, tmp1178, tmp1179, tmp1180
  real tmp1181, tmp1182, tmp1183, tmp1184, tmp1185
  real tmp1186, tmp1187, tmp1188, tmp1189, tmp1190
  real tmp1191, tmp1192, tmp1193, tmp1194, tmp1195
  real tmp1196, tmp1197, tmp1198, tmp1199, tmp1200
  real tmp1201, tmp1202, tmp1203, tmp1204, tmp1205
  real tmp1206, tmp1207, tmp1208, tmp1209, tmp1210
  real tmp1211, tmp1212, tmp1213, tmp1214, tmp1215
  real tmp1216, tmp1217, tmp1218, tmp1219, tmp1220
  real tmp1221, tmp1222, tmp1223, tmp1224, tmp1225
  real tmp1226, tmp1227, tmp1228, tmp1229, tmp1230
  real tmp1231, tmp1232, tmp1233, tmp1234, tmp1235
  real tmp1236, tmp1237, tmp1238, tmp1239, tmp1240
  real tmp1241, tmp1242, tmp1243, tmp1244, tmp1245
  real tmp1246, tmp1247, tmp1248, tmp1249, tmp1250
  real tmp1251, tmp1252, tmp1253, tmp1254, tmp1255
  real tmp1256, tmp1257, tmp1258, tmp1259, tmp1260
  real tmp1261, tmp1262, tmp1263, tmp1264, tmp1265
  real tmp1266, tmp1267, tmp1268, tmp1269, tmp1270
  real tmp1271, tmp1272, tmp1273, tmp1274, tmp1275
  real tmp1276, tmp1277, tmp1278, tmp1279, tmp1280
  real tmp1281, tmp1282, tmp1283, tmp1284, tmp1285
  real tmp1286, tmp1287, tmp1288, tmp1289, tmp1290
  real tmp1291, tmp1292, tmp1293, tmp1294, tmp1295
  real tmp1296, tmp1297, tmp1298, tmp1299, tmp1300
  real tmp1301, tmp1302, tmp1303, tmp1304, tmp1305
  real tmp1306, tmp1307, tmp1308, tmp1309, tmp1310
  real tmp1311, tmp1312, tmp1313, tmp1314, tmp1315
  real tmp1316, tmp1317, tmp1318, tmp1319, tmp1320
  real tmp1321, tmp1322, tmp1323, tmp1324, tmp1325
  real tmp1326, tmp1327, tmp1328, tmp1329, tmp1330
  real tmp1331, tmp1332, tmp1333, tmp1334, tmp1335
  real tmp1336, tmp1337, tmp1338, tmp1339, tmp1340
  real tmp1341, tmp1342, tmp1343, tmp1344, tmp1345
  real tmp1346, tmp1347, tmp1348, tmp1349, tmp1350
  real tmp1351, tmp1352, tmp1353, tmp1354, tmp1355
  real tmp1356, tmp1357, tmp1358, tmp1359, tmp1360
  real tmp1361, tmp1362, tmp1363, tmp1364, tmp1365
  real tmp1366, tmp1367, tmp1368, tmp1369, tmp1370
  real tmp1371, tmp1372, tmp1373, tmp1374, tmp1375
  real tmp1376, tmp1377, tmp1378, tmp1379, tmp1380
  real tmp1381, tmp1382, tmp1383, tmp1384, tmp1385
  real tmp1386, tmp1387, tmp1388, tmp1389, tmp1390
  real tmp1391, tmp1392, tmp1393, tmp1394, tmp1395
  real tmp1396, tmp1397, tmp1398, tmp1399, tmp1400
  real tmp1401, tmp1402, tmp1403, tmp1404, tmp1405
  real tmp1406, tmp1407, tmp1408, tmp1409, tmp1410
  real tmp1411, tmp1412, tmp1413, tmp1414, tmp1415
  real tmp1416, tmp1417, tmp1418, tmp1419, tmp1420
  real tmp1421, tmp1422, tmp1423, tmp1424, tmp1425
  real tmp1426, tmp1427, tmp1428, tmp1429, tmp1430
  real tmp1431, tmp1432, tmp1433, tmp1434, tmp1435
  real tmp1436, tmp1437, tmp1438, tmp1439, tmp1440
  real tmp1441, tmp1442, tmp1443, tmp1444, tmp1445
  real tmp1446, tmp1447, tmp1448, tmp1449, tmp1450
  real tmp1451, tmp1452, tmp1453, tmp1454, tmp1455
  real tmp1456, tmp1457, tmp1458, tmp1459, tmp1460
  real tmp1461, tmp1462, tmp1463, tmp1464, tmp1465
  real tmp1466, tmp1467, tmp1468, tmp1469, tmp1470
  real tmp1471, tmp1472, tmp1473, tmp1474, tmp1475
  real tmp1476, tmp1477, tmp1478, tmp1479, tmp1480
  real tmp1481, tmp1482, tmp1483, tmp1484, tmp1485
  real tmp1486, tmp1487, tmp1488, tmp1489, tmp1490
  real tmp1491, tmp1492, tmp1493, tmp1494, tmp1495
  real tmp1496, tmp1497, tmp1498, tmp1499, tmp1500
  real tmp1501, tmp1502, tmp1503, tmp1504, tmp1505
  real tmp1506, tmp1507, tmp1508, tmp1509, tmp1510
  real tmp1511, tmp1512, tmp1513, tmp1514, tmp1515
  real tmp1516, tmp1517, tmp1518, tmp1519, tmp1520
  real tmp1521, tmp1522, tmp1523, tmp1524, tmp1525
  real tmp1526, tmp1527, tmp1528, tmp1529, tmp1530
  real tmp1531, tmp1532, tmp1533, tmp1534, tmp1535
  real tmp1536, tmp1537, tmp1538, tmp1539, tmp1540
  real tmp1541, tmp1542, tmp1543, tmp1544, tmp1545
  real tmp1546, tmp1547, tmp1548, tmp1549, tmp1550
  real tmp1551, tmp1552, tmp1553, tmp1554, tmp1555
  real tmp1556, tmp1557, tmp1558, tmp1559, tmp1560
  real tmp1561, tmp1562, tmp1563, tmp1564, tmp1565
  real tmp1566, tmp1567, tmp1568, tmp1569, tmp1570
  real tmp1571, tmp1572, tmp1573, tmp1574, tmp1575
  real tmp1576, tmp1577, tmp1578, tmp1579, tmp1580
  real tmp1581, tmp1582, tmp1583, tmp1584, tmp1585
  real tmp1586, tmp1587, tmp1588, tmp1589, tmp1590
  real tmp1591, tmp1592, tmp1593, tmp1594, tmp1595
  real tmp1596, tmp1597, tmp1598, tmp1599, tmp1600
  real tmp1601, tmp1602, tmp1603, tmp1604, tmp1605
  real tmp1606, tmp1607, tmp1608, tmp1609, tmp1610
  real tmp1611, tmp1612, tmp1613, tmp1614, tmp1615
  real tmp1616, tmp1617, tmp1618, tmp1619, tmp1620
  real tmp1621, tmp1622, tmp1623, tmp1624, tmp1625
  real tmp1626, tmp1627, tmp1628, tmp1629, tmp1630
  real tmp1631, tmp1632, tmp1633, tmp1634, tmp1635
  real tmp1636, tmp1637, tmp1638, tmp1639, tmp1640
  real tmp1641, tmp1642, tmp1643, tmp1644, tmp1645
  real tmp1646, tmp1647, tmp1648, tmp1649, tmp1650
  real tmp1651, tmp1652, tmp1653, tmp1654, tmp1655
  real tmp1656, tmp1657, tmp1658, tmp1659, tmp1660
  real tmp1661, tmp1662, tmp1663, tmp1664, tmp1665
  real tmp1666, tmp1667, tmp1668, tmp1669, tmp1670
  real tmp1671, tmp1672, tmp1673, tmp1674, tmp1675
  real tmp1676, tmp1677, tmp1678, tmp1679, tmp1680
  real tmp1681, tmp1682, tmp1683, tmp1684, tmp1685
  real tmp1686, tmp1687, tmp1688, tmp1689, tmp1690
  real tmp1691, tmp1692, tmp1693, tmp1694, tmp1695
  real tmp1696, tmp1697, tmp1698, tmp1699, tmp1700
  real tmp1701, tmp1702, tmp1703, tmp1704, tmp1705
  real tmp1706, tmp1707, tmp1708, tmp1709, tmp1710
  real tmp1711, tmp1712, tmp1713, tmp1714, tmp1715
  real tmp1716, tmp1717, tmp1718, tmp1719, tmp1720
  real tmp1721, tmp1722, tmp1723, tmp1724, tmp1725
  real tmp1726, tmp1727, tmp1728, tmp1729, tmp1730
  real tmp1731, tmp1732, tmp1733, tmp1734, tmp1735
  real tmp1736, tmp1737, tmp1738, tmp1739, tmp1740
  real tmp1741, tmp1742, tmp1743, tmp1744, tmp1745
  real tmp1746, tmp1747, tmp1748, tmp1749, tmp1750
  real tmp1751, tmp1752, tmp1753, tmp1754, tmp1755
  real tmp1756, tmp1757, tmp1758, tmp1759, tmp1760
  real tmp1761, tmp1762, tmp1763, tmp1764, tmp1765
  real tmp1766, tmp1767, tmp1768, tmp1769, tmp1770
  real tmp1771, tmp1772, tmp1773, tmp1774, tmp1775
  real tmp1776, tmp1777, tmp1778, tmp1779, tmp1780
  real tmp1781, tmp1782, tmp1783, tmp1784, tmp1785
  real tmp1786, tmp1787, tmp1788, tmp1789, tmp1790
  real tmp1791, tmp1792, tmp1793, tmp1794, tmp1795
  real tmp1796, tmp1797, tmp1798, tmp1799, tmp1800
  real tmp1801, tmp1802, tmp1803, tmp1804, tmp1805
  real tmp1806, tmp1807, tmp1808, tmp1809, tmp1810
  real tmp1811, tmp1812, tmp1813, tmp1814, tmp1815
  real tmp1816, tmp1817, tmp1818, tmp1819, tmp1820
  real tmp1821, tmp1822, tmp1823, tmp1824, tmp1825
  real tmp1826, tmp1827, tmp1828, tmp1829, tmp1830
  real tmp1831, tmp1832, tmp1833, tmp1834, tmp1835
  real tmp1836, tmp1837, tmp1838, tmp1839, tmp1840
  real tmp1841, tmp1842, tmp1843, tmp1844, tmp1845
  real tmp1846, tmp1847, tmp1848, tmp1849, tmp1850
  real tmp1851, tmp1852, tmp1853, tmp1854, tmp1855
  real tmp1856, tmp1857, tmp1858, tmp1859, tmp1860
  real tmp1861, tmp1862, tmp1863, tmp1864, tmp1865
  real tmp1866, tmp1867, tmp1868, tmp1869, tmp1870
  real tmp1871, tmp1872, tmp1873, tmp1874, tmp1875
  real tmp1876, tmp1877, tmp1878, tmp1879, tmp1880
  real tmp1881, tmp1882, tmp1883, tmp1884, tmp1885
  real tmp1886, tmp1887, tmp1888, tmp1889, tmp1890
  real tmp1891, tmp1892, tmp1893

  tmp1 = sqrt(lambda)**2
  tmp2 = -1 + kappa
  tmp3 = sqrt(lambda)**4
  tmp4 = me2**2
  tmp5 = 16*tmp4
  tmp6 = mm2**2
  tmp7 = 16*tmp6
  tmp8 = ss**2
  tmp9 = 16*tmp8
  tmp10 = 4*mm2
  tmp11 = -4*ss
  tmp12 = -tt
  tmp13 = tmp10 + tmp11 + tmp12
  tmp14 = 8*me2*tmp13
  tmp15 = tt**2
  tmp16 = -tmp15
  tmp17 = 4*ss
  tmp18 = tmp17 + tt
  tmp19 = -8*mm2*tmp18
  tmp20 = tmp14 + tmp16 + tmp19 + tmp5 + tmp7 + tmp9
  tmp21 = 1/mm2
  tmp22 = 1/tt
  tmp23 = tmp3 + tmp7
  tmp24 = -4/tmp21
  tmp25 = tmp1 + tmp24
  tmp26 = 2/tmp21
  tmp27 = 2*tmp6
  tmp28 = 2*ss
  tmp29 = 1/tmp22 + tmp28
  tmp30 = ss*tmp29
  tmp31 = kappa*tmp3
  tmp32 = tmp25**(-2)
  tmp33 = 1/tmp32
  tmp34 = 2/tmp22
  tmp35 = -ss
  tmp36 = 1/tmp21 + tmp35
  tmp37 = me2**3
  tmp38 = 5*ss
  tmp39 = 10*ss
  tmp40 = 11/tmp22
  tmp41 = tmp21**(-3)
  tmp42 = tmp21**(-4)
  tmp43 = ss**3
  tmp44 = -3*tmp31
  tmp45 = -tmp31
  tmp46 = -tmp1
  tmp47 = tmp10 + tmp46
  tmp48 = 1/tmp47
  tmp49 = tmp36**2
  tmp50 = ss + 1/tmp21
  tmp51 = -2*me2*tmp50
  tmp52 = tmp4 + tmp49 + tmp51
  tmp53 = 1/tmp52
  tmp54 = me2**4
  tmp55 = -8*ss
  tmp56 = 8*ss
  tmp57 = 3/tmp22
  tmp58 = 8*tmp8
  tmp59 = 4*me2
  tmp60 = tmp12 + tmp59
  tmp61 = 1/tmp60
  tmp62 = 2*tmp54
  tmp63 = 2*tmp42
  tmp64 = 1/tmp22 + tmp55
  tmp65 = tmp41*tmp64
  tmp66 = tmp29*tmp43
  tmp67 = 8/tmp21
  tmp68 = 1/tmp22 + tmp56 + tmp67
  tmp69 = -(tmp37*tmp68)
  tmp70 = 12*tmp8
  tmp71 = tmp35/tmp22
  tmp72 = tmp16 + tmp70 + tmp71
  tmp73 = tmp6*tmp72
  tmp74 = ss/tmp22
  tmp75 = tmp15 + tmp58 + tmp74
  tmp76 = (tmp35*tmp75)/tmp21
  tmp77 = 12*tmp6
  tmp78 = tmp12 + tmp56
  tmp79 = tmp78/tmp21
  tmp80 = 3*ss*tmp18
  tmp81 = tmp77 + tmp79 + tmp80
  tmp82 = tmp4*tmp81
  tmp83 = -8*tmp41
  tmp84 = 1/tmp22 + tmp56
  tmp85 = tmp6*tmp84
  tmp86 = tmp56 + tmp57
  tmp87 = -(tmp8*tmp86)
  tmp88 = 2*tmp74
  tmp89 = tmp15 + tmp58 + tmp88
  tmp90 = tmp89/tmp21
  tmp91 = tmp83 + tmp85 + tmp87 + tmp90
  tmp92 = me2*tmp91
  tmp93 = tmp62 + tmp63 + tmp65 + tmp66 + tmp69 + tmp73 + tmp76 + tmp82 + tmp92
  tmp94 = 6*ss
  tmp95 = 2*tmp15
  tmp96 = 17/tmp22
  tmp97 = 10*tmp74
  tmp98 = 2*tmp4
  tmp99 = 2*tmp8
  tmp100 = me2 + tmp36
  tmp101 = -4*me2*tmp50
  tmp102 = tmp11 + 1/tmp22
  tmp103 = tmp102/tmp21
  tmp104 = tmp101 + tmp103 + tmp27 + tmp98 + tmp99
  tmp105 = 1 + tmp1
  tmp106 = 1/tmp22 + tmp26 + tmp94
  tmp107 = 3*ss
  tmp108 = tmp107 + 1/tmp22
  tmp109 = -4/tmp22
  tmp110 = 18*tmp41
  tmp111 = tmp109 + tmp38
  tmp112 = -4*tmp111*tmp6
  tmp113 = -2/tmp22
  tmp114 = tmp113 + 1/tmp21
  tmp115 = tmp114*tmp98
  tmp116 = tmp109*tmp8
  tmp117 = -6*tmp74
  tmp118 = -7*tmp15
  tmp119 = tmp117 + tmp118 + tmp99
  tmp120 = tmp119/tmp21
  tmp121 = 7*tmp6
  tmp122 = 1/tmp22 + tmp35
  tmp123 = tmp122/tmp21
  tmp124 = tmp121 + tmp123 + tmp88
  tmp125 = tmp124*tmp59
  tmp126 = tmp110 + tmp112 + tmp115 + tmp116 + tmp120 + tmp125
  tmp127 = kappa*tmp1
  tmp128 = 1 + tmp127
  tmp129 = -2 + kappa
  tmp130 = tmp1*tmp129
  tmp131 = -1 + tmp130
  tmp132 = tmp22**(-3)
  tmp133 = tmp10 + tmp12
  tmp134 = 4*tmp15
  tmp135 = tmp21**(-5)
  tmp136 = (-2*tmp29)/tmp21
  tmp137 = tmp136 + tmp27 + tmp30
  tmp138 = 12*ss
  tmp139 = 5/tmp22
  tmp140 = 16*tmp54
  tmp141 = 16/tmp21
  tmp142 = -16*ss
  tmp143 = tmp139 + tmp141 + tmp142
  tmp144 = -2*tmp143*tmp37
  tmp145 = tmp103 + tmp27 + tmp99
  tmp146 = tmp145*tmp16
  tmp147 = 7*tmp8
  tmp148 = -14*ss
  tmp149 = tmp148 + tmp57
  tmp150 = tmp149/tmp21
  tmp151 = tmp121 + tmp147 + tmp150
  tmp152 = me2*tmp151*tmp34
  tmp153 = (-3*tmp84)/tmp21
  tmp154 = tmp138 + tmp139
  tmp155 = ss*tmp154
  tmp156 = tmp153 + tmp155 + tmp77
  tmp157 = -4*tmp156*tmp4
  tmp158 = tmp140 + tmp144 + tmp146 + tmp152 + tmp157
  tmp159 = 1/tmp22 + tmp24
  tmp160 = tmp159**(-2)
  tmp161 = 6*tmp8
  tmp162 = ss*tmp139
  tmp163 = 8*tmp6
  tmp164 = (6*tmp18)/tmp21
  tmp165 = 32*tmp42
  tmp166 = tmp134*tmp8
  tmp167 = -128*ss
  tmp168 = 78/tmp22
  tmp169 = tmp167 + tmp168
  tmp170 = tmp169*tmp41
  tmp171 = 96*tmp8
  tmp172 = 52*tmp74
  tmp173 = -48*tmp15
  tmp174 = tmp171 + tmp172 + tmp173
  tmp175 = tmp174*tmp6
  tmp176 = 96*tmp6
  tmp177 = -34/(tmp21*tmp22)
  tmp178 = tmp134 + tmp176 + tmp177
  tmp179 = tmp178*tmp4
  tmp180 = -34*tmp8
  tmp181 = ss*tmp113
  tmp182 = 7*tmp15
  tmp183 = tmp180 + tmp181 + tmp182
  tmp184 = tmp183/(tmp21*tmp22)
  tmp185 = 32*tmp41
  tmp186 = -2*ss*tmp15
  tmp187 = 17*ss
  tmp188 = tmp187 + 1/tmp22
  tmp189 = tmp188/(tmp21*tmp22)
  tmp190 = 16*ss
  tmp191 = tmp139 + tmp190
  tmp192 = -3*tmp191*tmp6
  tmp193 = tmp185 + tmp186 + tmp189 + tmp192
  tmp194 = tmp193*tmp59
  tmp195 = tmp165 + tmp166 + tmp170 + tmp175 + tmp179 + tmp184 + tmp194
  tmp196 = me2**5
  tmp197 = -2*tmp196
  tmp198 = tmp1 + 1/tmp22
  tmp199 = -12*tmp8
  tmp200 = tmp1*tmp34
  tmp201 = (8*tmp1)/tmp22
  tmp202 = 5*tmp1
  tmp203 = 6*tmp42
  tmp204 = tmp1*tmp57
  tmp205 = -4*me2
  tmp206 = tmp205 + 1/tmp22
  tmp207 = tmp206**(-2)
  tmp208 = 2*me2
  tmp209 = -2*ss
  tmp210 = ss + 1/tmp22
  tmp211 = 6*tmp6
  tmp212 = 32*tmp6
  tmp213 = 32*tmp8
  tmp214 = 64*tmp54
  tmp215 = tmp12 + tmp209 + tmp26
  tmp216 = 64*tmp215*tmp37
  tmp217 = 64*tmp74
  tmp218 = 15*tmp15
  tmp219 = (-64*tmp210)/tmp21
  tmp220 = tmp212 + tmp213 + tmp217 + tmp218 + tmp219
  tmp221 = tmp220*tmp98
  tmp222 = 4*tmp74
  tmp223 = (-6*tmp29)/tmp21
  tmp224 = tmp15 + tmp161 + tmp211 + tmp222 + tmp223
  tmp225 = tmp15*tmp224
  tmp226 = 26*tmp74
  tmp227 = 5*tmp15
  tmp228 = 32*ss
  tmp229 = 15/tmp22
  tmp230 = tmp228 + tmp229
  tmp231 = (-2*tmp230)/tmp21
  tmp232 = tmp212 + tmp213 + tmp226 + tmp227 + tmp231
  tmp233 = me2*tmp113*tmp232
  tmp234 = tmp214 + tmp216 + tmp221 + tmp225 + tmp233
  tmp235 = 4*tmp4
  tmp236 = tmp29**2
  tmp237 = 3*tmp15
  tmp238 = 7*ss
  tmp239 = 25/tmp22
  tmp240 = 64*tmp42
  tmp241 = tmp238 + tmp57
  tmp242 = ss*tmp15*tmp241
  tmp243 = tmp190 + tmp40
  tmp244 = tmp243*tmp83
  tmp245 = 28*tmp8
  tmp246 = 23*tmp74
  tmp247 = tmp237 + tmp245 + tmp246
  tmp248 = (tmp113*tmp247)/tmp21
  tmp249 = 64*tmp6
  tmp250 = -56/(tmp21*tmp22)
  tmp251 = tmp182 + tmp249 + tmp250
  tmp252 = tmp251*tmp4
  tmp253 = 64*tmp8
  tmp254 = 144*tmp74
  tmp255 = 39*tmp15
  tmp256 = tmp253 + tmp254 + tmp255
  tmp257 = tmp256*tmp6
  tmp258 = 64*tmp41
  tmp259 = tmp238 + tmp34
  tmp260 = tmp16*tmp259
  tmp261 = 9/tmp22
  tmp262 = tmp261 + tmp56
  tmp263 = -8*tmp262*tmp6
  tmp264 = 56*ss
  tmp265 = tmp239 + tmp264
  tmp266 = tmp265/(tmp21*tmp22)
  tmp267 = tmp258 + tmp260 + tmp263 + tmp266
  tmp268 = tmp208*tmp267
  tmp269 = tmp240 + tmp242 + tmp244 + tmp248 + tmp252 + tmp257 + tmp268
  tmp270 = 7/tmp22
  tmp271 = 2*tmp1
  tmp272 = tmp46 + tmp67
  tmp273 = tmp1 + tmp190 + tmp270
  tmp274 = tmp1*tmp56
  tmp275 = -12*tmp74
  tmp276 = tmp204 + tmp213 + tmp274 + tmp275 + tmp95
  tmp277 = 128*tmp6
  tmp278 = tmp190 + 1/tmp22 + tmp271
  tmp279 = (-8*tmp278)/tmp21
  tmp280 = tmp1*tmp191
  tmp281 = tmp277 + tmp279 + tmp280
  tmp282 = 5 + tmp271
  tmp283 = -2*tmp130
  tmp284 = 16*tmp41
  tmp285 = 3*tmp1
  tmp286 = tmp141*tmp4
  tmp287 = -3*ss
  tmp288 = 1/tmp22 + tmp287
  tmp289 = (tmp1*tmp288)/tmp22
  tmp290 = -4*tmp6*tmp86
  tmp291 = 1/tmp22 + tmp285
  tmp292 = -(tmp291/tmp22)
  tmp293 = tmp292 + tmp9
  tmp294 = tmp293/tmp21
  tmp295 = (-2*tmp191)/tmp21
  tmp296 = tmp204 + tmp212 + tmp295
  tmp297 = me2*tmp296
  tmp298 = tmp284 + tmp286 + tmp289 + tmp290 + tmp294 + tmp297
  tmp299 = (8*tmp4)/tmp48
  tmp300 = 13 + tmp271
  tmp301 = -8*tmp8
  tmp302 = kappa*tmp271
  tmp303 = 2*tmp31
  tmp304 = tmp1*tmp109
  tmp305 = -2*tmp1
  tmp306 = 5 + tmp302
  tmp307 = -2*tmp37
  tmp308 = tmp107/tmp22
  tmp309 = ss*tmp198
  tmp310 = 1/tmp22 + tmp305
  tmp311 = tmp161*tmp310
  tmp312 = 12*tmp3
  tmp313 = 2*tmp310*tmp37
  tmp314 = 2*tmp310*tmp41
  tmp315 = tmp1 + tmp107 + 1/tmp21
  tmp316 = -2*tmp310*tmp315*tmp4
  tmp317 = -4*tmp1
  tmp318 = -6*ss
  tmp319 = 1/tmp22 + tmp317 + tmp318
  tmp320 = tmp310*tmp319*tmp6
  tmp321 = tmp3/tmp22
  tmp322 = ss*tmp1*tmp310
  tmp323 = tmp310*tmp8
  tmp324 = tmp321 + tmp322 + tmp323
  tmp325 = tmp209*tmp324
  tmp326 = 8*tmp321
  tmp327 = (-8*tmp1)/tmp22
  tmp328 = tmp15 + tmp312 + tmp327
  tmp329 = tmp328*tmp35
  tmp330 = tmp311 + tmp326 + tmp329
  tmp331 = tmp330/tmp21
  tmp332 = 2*tmp321
  tmp333 = -2*tmp310*tmp6
  tmp334 = 4*tmp322
  tmp335 = ss*tmp109
  tmp336 = tmp15 + tmp274 + tmp312 + tmp335
  tmp337 = tmp336/tmp21
  tmp338 = tmp311 + tmp332 + tmp333 + tmp334 + tmp337
  tmp339 = me2*tmp338
  tmp340 = tmp313 + tmp314 + tmp316 + tmp320 + tmp325 + tmp331 + tmp339
  tmp341 = tmp2**2
  tmp342 = sqrt(lambda)**8
  tmp343 = 6/tmp21
  tmp344 = tmp28 + tmp57
  tmp345 = tmp108*tmp209
  tmp346 = 5/tmp21
  tmp347 = tmp12 + tmp67
  tmp348 = -16*tmp8
  tmp349 = tmp22**(-4)
  tmp350 = 4*tmp37
  tmp351 = tmp29*tmp74
  tmp352 = 4*tmp6
  tmp353 = 2*tmp37
  tmp354 = 2*tmp41
  tmp355 = (-2*tmp18)/tmp21
  tmp356 = 48*ss
  tmp357 = 4*tmp8
  tmp358 = 10*tmp8
  tmp359 = 128*tmp42
  tmp360 = -3/tmp22
  tmp361 = tmp360 + tmp67
  tmp362 = 32*tmp361*tmp54
  tmp363 = tmp138 + tmp270
  tmp364 = tmp363/tmp22
  tmp365 = 13/tmp22
  tmp366 = tmp365 + tmp56
  tmp367 = tmp24*tmp366
  tmp368 = tmp249 + tmp364 + tmp367
  tmp369 = 16*tmp368*tmp37
  tmp370 = 640*tmp41
  tmp371 = tmp356 + tmp96
  tmp372 = -16*tmp371*tmp6
  tmp373 = 108*tmp74
  tmp374 = 31*tmp15
  tmp375 = tmp373 + tmp374 + tmp9
  tmp376 = tmp375*tmp67
  tmp377 = 48*tmp8
  tmp378 = 160*tmp74
  tmp379 = 37*tmp15
  tmp380 = tmp377 + tmp378 + tmp379
  tmp381 = -(tmp380/tmp22)
  tmp382 = tmp370 + tmp372 + tmp376 + tmp381
  tmp383 = tmp382*tmp98
  tmp384 = -16*tmp41*tmp86
  tmp385 = ss*tmp365
  tmp386 = tmp357 + tmp385 + tmp95
  tmp387 = (4*tmp386)/(tmp21*tmp22)
  tmp388 = 14*tmp74
  tmp389 = tmp237 + tmp358 + tmp388
  tmp390 = tmp16*tmp389
  tmp391 = 96*tmp74
  tmp392 = 38*tmp15
  tmp393 = tmp253 + tmp391 + tmp392
  tmp394 = tmp393*tmp6
  tmp395 = tmp240 + tmp384 + tmp387 + tmp390 + tmp394
  tmp396 = tmp395/tmp22
  tmp397 = tmp270 + tmp56
  tmp398 = -32*tmp397*tmp41
  tmp399 = 37*tmp74
  tmp400 = tmp182 + tmp245 + tmp399
  tmp401 = tmp15*tmp400
  tmp402 = 72*tmp74
  tmp403 = 13*tmp15
  tmp404 = tmp213 + tmp402 + tmp403
  tmp405 = tmp352*tmp404
  tmp406 = 128*tmp8
  tmp407 = 224*tmp74
  tmp408 = tmp255 + tmp406 + tmp407
  tmp409 = -(tmp408/(tmp21*tmp22))
  tmp410 = tmp359 + tmp398 + tmp401 + tmp405 + tmp409
  tmp411 = tmp410*tmp59
  tmp412 = tmp362 + tmp369 + tmp383 + tmp396 + tmp411
  tmp413 = tmp22**(-5)
  tmp414 = -(tmp210*tmp74)
  tmp415 = -2*tmp29*tmp6
  tmp416 = (ss*tmp344)/tmp21
  tmp417 = -4*tmp210
  tmp418 = tmp343 + tmp417
  tmp419 = tmp4*tmp418
  tmp420 = tmp15 + tmp162 + tmp211 + tmp355 + tmp99
  tmp421 = me2*tmp420
  tmp422 = tmp353 + tmp354 + tmp414 + tmp415 + tmp416 + tmp419 + tmp421
  tmp423 = 256*tmp8
  tmp424 = 360*tmp74
  tmp425 = 512*tmp135
  tmp426 = tmp56 + tmp96
  tmp427 = tmp1 + tmp34
  tmp428 = 2*tmp3
  tmp429 = 24*tmp8
  tmp430 = -48*tmp8
  tmp431 = tmp1 + tmp17
  tmp432 = tmp15*tmp302
  tmp433 = 4*tmp1
  tmp434 = -5/tmp22
  tmp435 = 7*tmp1
  tmp436 = kappa*tmp15*tmp428
  tmp437 = 4/tmp22
  tmp438 = 16*kappa*tmp15
  tmp439 = -tmp127
  tmp440 = tmp10 + tmp439
  tmp441 = 4*tmp41
  tmp442 = -6/tmp21
  tmp443 = 6/tmp22
  tmp444 = 24*ss
  tmp445 = 8*tmp3
  tmp446 = tmp3 + tmp95
  tmp447 = tmp305 + tmp57
  tmp448 = -8*tmp3
  tmp449 = -6*tmp3
  tmp450 = (20*tmp1)/tmp22
  tmp451 = (12*tmp1)/tmp22
  tmp452 = ss*tmp3
  tmp453 = -6*tmp1
  tmp454 = 1/tmp22 + tmp453
  tmp455 = tmp165*tmp454
  tmp456 = (-20*tmp1)/tmp22
  tmp457 = -64*ss*tmp310
  tmp458 = tmp255 + tmp445 + tmp456 + tmp457
  tmp459 = tmp354*tmp458
  tmp460 = tmp15*tmp3
  tmp461 = tmp271*tmp427*tmp74
  tmp462 = tmp446*tmp99
  tmp463 = tmp460 + tmp461 + tmp462
  tmp464 = tmp463/tmp22
  tmp465 = tmp447*tmp7
  tmp466 = -17*tmp15
  tmp467 = tmp304 + tmp445 + tmp466
  tmp468 = tmp467/tmp21
  tmp469 = tmp446/tmp22
  tmp470 = tmp465 + tmp468 + tmp469
  tmp471 = tmp470*tmp98
  tmp472 = tmp447*tmp9
  tmp473 = (32*tmp1)/tmp22
  tmp474 = -24*tmp15
  tmp475 = tmp3 + tmp473 + tmp474
  tmp476 = tmp475/tmp22
  tmp477 = tmp304 + tmp403 + tmp448
  tmp478 = tmp28*tmp477
  tmp479 = tmp472 + tmp476 + tmp478
  tmp480 = tmp27*tmp479
  tmp481 = 4*tmp3
  tmp482 = tmp118 + tmp451 + tmp481
  tmp483 = tmp15*tmp482
  tmp484 = tmp15 + tmp449 + tmp450
  tmp485 = tmp484*tmp88
  tmp486 = tmp1*tmp437
  tmp487 = 17*tmp15
  tmp488 = tmp448 + tmp486 + tmp487
  tmp489 = tmp488*tmp99
  tmp490 = tmp483 + tmp485 + tmp489
  tmp491 = -(tmp490/tmp21)
  tmp492 = tmp185*tmp310
  tmp493 = tmp1*tmp228
  tmp494 = -48*tmp74
  tmp495 = -15*tmp15
  tmp496 = tmp445 + tmp451 + tmp493 + tmp494 + tmp495
  tmp497 = tmp496*tmp6
  tmp498 = tmp1*tmp15
  tmp499 = ss*tmp95
  tmp500 = tmp452 + tmp498 + tmp499
  tmp501 = -(tmp500/tmp22)
  tmp502 = -8*tmp452
  tmp503 = tmp3*tmp434
  tmp504 = ss*tmp486
  tmp505 = 8*tmp498
  tmp506 = ss*tmp487
  tmp507 = tmp132 + tmp502 + tmp503 + tmp504 + tmp505 + tmp506
  tmp508 = tmp507/tmp21
  tmp509 = tmp492 + tmp497 + tmp501 + tmp508
  tmp510 = tmp509*tmp59
  tmp511 = tmp455 + tmp459 + tmp464 + tmp471 + tmp480 + tmp491 + tmp510
  tmp512 = sqrt(lambda)**6
  tmp513 = tmp22**2
  tmp514 = 3/tmp21
  tmp515 = -1 + f1p
  tmp516 = 3*me2
  tmp517 = tmp288 + tmp514 + tmp516
  tmp518 = ss*tmp46
  tmp519 = -7/tmp22
  tmp520 = tmp1 + tmp11 + tmp519
  tmp521 = tmp520/tmp21
  tmp522 = tmp1 + tmp11 + tmp347
  tmp523 = me2*tmp522
  tmp524 = tmp1/tmp22
  tmp525 = tmp235 + tmp308 + tmp352 + tmp518 + tmp521 + tmp523 + tmp524
  tmp526 = 1/me2
  tmp527 = (4*tmp129)/tmp21
  tmp528 = tmp127 + tmp527
  tmp529 = -((f2p*tmp100*tmp528)/tmp22)
  tmp530 = tmp2*tmp24*tmp525
  tmp531 = (4*f1p*tmp2*tmp525)/tmp21
  tmp532 = tmp529 + tmp530 + tmp531
  tmp533 = 1/tmp133
  tmp534 = tmp1 + tmp133
  tmp535 = 24*tmp6
  tmp536 = ss*tmp305
  tmp537 = -24*ss
  tmp538 = -6/tmp22
  tmp539 = tmp271 + tmp537 + tmp538
  tmp540 = tmp539/tmp21
  tmp541 = 12/tmp21
  tmp542 = tmp1 + tmp434 + tmp541
  tmp543 = (2*tmp542)/tmp526
  tmp544 = -3*tmp524
  tmp545 = tmp16 + tmp535 + tmp536 + tmp540 + tmp543 + tmp544 + tmp97
  tmp546 = -2/tmp21
  tmp547 = tmp12 + tmp208 + tmp209 + tmp546
  tmp548 = -((f2p*tmp528*tmp547)/tmp22)
  tmp549 = tmp2*tmp545*tmp546
  tmp550 = f1p*tmp2*tmp26*tmp545
  tmp551 = tmp548 + tmp549 + tmp550
  tmp552 = tmp21**2
  tmp553 = tmp198*tmp546
  tmp554 = tmp163 + tmp524 + tmp553
  tmp555 = -22/tmp22
  tmp556 = tmp271 + tmp537 + tmp555
  tmp557 = tmp556/tmp21
  tmp558 = tmp16 + tmp524 + tmp535 + tmp536 + tmp543 + tmp557 + tmp97
  tmp559 = 5*tmp524
  tmp560 = -3*tmp3
  tmp561 = -tmp3
  tmp562 = tmp1*tmp538
  tmp563 = -5*tmp1
  tmp564 = (-8*tmp198)/tmp21
  tmp565 = tmp15 + tmp23 + tmp564
  tmp566 = tmp546/tmp22
  tmp567 = 1/tmp22 + tmp47
  tmp568 = tmp567/tmp526
  tmp569 = tmp566 + tmp568
  tmp570 = -tmp524
  tmp571 = tmp11 + tmp198
  tmp572 = tmp571/tmp21
  tmp573 = tmp235 + tmp308 + tmp352 + tmp518 + tmp523 + tmp570 + tmp572
  tmp574 = (1/tmp526)**1.5
  tmp575 = -4*tmp574
  tmp576 = Sqrt(1/tmp526)
  tmp577 = tmp576/tmp22
  tmp578 = tmp575 + tmp577
  tmp579 = tmp578**(-2)
  tmp580 = 1/tmp22 + tmp46
  tmp581 = tmp580**2
  tmp582 = tmp208 + tmp209 + 1/tmp22 + tmp26
  tmp583 = 6*tmp1
  tmp584 = ss*tmp443
  tmp585 = ss*tmp583
  tmp586 = tmp563/tmp22
  tmp587 = 40*tmp6
  tmp588 = 28*ss
  tmp589 = 3*tmp198
  tmp590 = tmp588 + tmp589
  tmp591 = tmp546*tmp590
  tmp592 = 56/tmp21
  tmp593 = tmp190 + tmp291
  tmp594 = -2*tmp593
  tmp595 = tmp592 + tmp594
  tmp596 = tmp595/tmp526
  tmp597 = tmp117 + tmp15 + tmp5 + tmp585 + tmp586 + tmp587 + tmp591 + tmp596 + tm&
                          &p9
  tmp598 = 8*tmp41
  tmp599 = 1/tmp580
  tmp600 = (tmp100*tmp104*tmp2*tmp46)/(tmp533*tmp599)
  tmp601 = (f1p*tmp1*tmp100*tmp104*tmp2)/(tmp533*tmp599)
  tmp602 = tmp353*tmp534
  tmp603 = 1/tmp22 + tmp94
  tmp604 = -(tmp603/tmp599)
  tmp605 = tmp138 + tmp291
  tmp606 = tmp26*tmp605
  tmp607 = tmp163 + tmp604 + tmp606
  tmp608 = -(tmp4*tmp607)
  tmp609 = (tmp29*tmp35)/tmp599
  tmp610 = tmp139 + tmp285 + tmp56
  tmp611 = -2*tmp6*tmp610
  tmp612 = tmp15 + tmp222 + tmp357 + tmp570
  tmp613 = tmp26*tmp612
  tmp614 = tmp598 + tmp609 + tmp611 + tmp613
  tmp615 = tmp36*tmp614
  tmp616 = 15*tmp1
  tmp617 = tmp360 + tmp56 + tmp616
  tmp618 = -2*tmp6*tmp617
  tmp619 = tmp345/tmp599
  tmp620 = tmp1 + tmp12
  tmp621 = tmp620/tmp22
  tmp622 = tmp1 + tmp57
  tmp623 = tmp17*tmp622
  tmp624 = tmp429 + tmp621 + tmp623
  tmp625 = tmp624/tmp21
  tmp626 = tmp618 + tmp619 + tmp625 + tmp83
  tmp627 = tmp626/tmp526
  tmp628 = tmp602 + tmp608 + tmp615 + tmp627
  tmp629 = f2p*tmp105*tmp113*tmp628
  tmp630 = tmp600 + tmp601 + tmp629
  tmp631 = tmp46 + tmp57
  tmp632 = -15*tmp1
  tmp633 = 37/tmp22
  tmp634 = 77/tmp22
  tmp635 = 8*tmp74
  tmp636 = tmp270 + tmp46
  tmp637 = tmp34 + tmp46
  tmp638 = 29/tmp22
  tmp639 = -3*tmp1
  tmp640 = tmp631*tmp67
  tmp641 = -(tmp622/tmp22)
  tmp642 = tmp640 + tmp641
  tmp643 = tmp212*tmp447
  tmp644 = tmp1 + tmp360
  tmp645 = tmp228*tmp644
  tmp646 = 44*tmp524
  tmp647 = -76*tmp15
  tmp648 = tmp645 + tmp646 + tmp647
  tmp649 = tmp648/tmp21
  tmp650 = tmp636/tmp22
  tmp651 = tmp623 + tmp650
  tmp652 = tmp651/tmp22
  tmp653 = tmp643 + tmp649 + tmp652
  tmp654 = tmp270 + tmp563
  tmp655 = 19*tmp1
  tmp656 = tmp633 + tmp655
  tmp657 = tmp631*tmp9
  tmp658 = 47/tmp22
  tmp659 = tmp632 + tmp658
  tmp660 = ss*tmp437*tmp659
  tmp661 = 59/tmp22
  tmp662 = tmp46 + tmp661
  tmp663 = tmp15*tmp662
  tmp664 = tmp657 + tmp660 + tmp663
  tmp665 = tmp356*tmp631
  tmp666 = -13*tmp1
  tmp667 = tmp634 + tmp666
  tmp668 = tmp667/tmp22
  tmp669 = tmp665 + tmp668
  tmp670 = tmp198*tmp240
  tmp671 = tmp202 + tmp57
  tmp672 = tmp15*tmp671
  tmp673 = tmp139 + tmp285
  tmp674 = tmp673*tmp99
  tmp675 = tmp202 + tmp270
  tmp676 = tmp675*tmp88
  tmp677 = tmp672 + tmp674 + tmp676
  tmp678 = tmp15*tmp677
  tmp679 = 8*tmp309
  tmp680 = tmp202 + tmp365
  tmp681 = tmp680/tmp22
  tmp682 = tmp679 + tmp681
  tmp683 = -16*tmp41*tmp682
  tmp684 = 10*tmp15*tmp198
  tmp685 = tmp1 + tmp139
  tmp686 = tmp357*tmp685
  tmp687 = tmp616 + tmp633
  tmp688 = tmp687*tmp74
  tmp689 = tmp684 + tmp686 + tmp688
  tmp690 = (tmp109*tmp689)/tmp21
  tmp691 = tmp198*tmp213
  tmp692 = tmp202 + tmp40
  tmp693 = (tmp190*tmp692)/tmp22
  tmp694 = 51*tmp1
  tmp695 = tmp634 + tmp694
  tmp696 = tmp15*tmp695
  tmp697 = tmp691 + tmp693 + tmp696
  tmp698 = tmp27*tmp697
  tmp699 = tmp670 + tmp678 + tmp683 + tmp690 + tmp698
  tmp700 = tmp359*tmp637
  tmp701 = ss*tmp317
  tmp702 = 8*tmp15
  tmp703 = tmp544 + tmp635 + tmp701 + tmp702
  tmp704 = -64*tmp41*tmp703
  tmp705 = tmp182*tmp198
  tmp706 = tmp357*tmp636
  tmp707 = tmp285 + tmp633
  tmp708 = tmp707*tmp74
  tmp709 = tmp705 + tmp706 + tmp708
  tmp710 = tmp15*tmp709
  tmp711 = tmp213*tmp637
  tmp712 = -7*tmp1
  tmp713 = tmp638 + tmp712
  tmp714 = tmp635*tmp713
  tmp715 = 89/tmp22
  tmp716 = tmp202 + tmp715
  tmp717 = tmp15*tmp716
  tmp718 = tmp711 + tmp714 + tmp717
  tmp719 = tmp352*tmp718
  tmp720 = tmp443 + tmp46
  tmp721 = tmp217*tmp720
  tmp722 = tmp270 + tmp639
  tmp723 = tmp213*tmp722
  tmp724 = 37*tmp1
  tmp725 = 95/tmp22
  tmp726 = tmp724 + tmp725
  tmp727 = tmp15*tmp726
  tmp728 = tmp721 + tmp723 + tmp727
  tmp729 = -(tmp728/(tmp21*tmp22))
  tmp730 = tmp700 + tmp704 + tmp710 + tmp719 + tmp729
  tmp731 = -96*tmp74
  tmp732 = tmp138/tmp22
  tmp733 = 160*ss*tmp15
  tmp734 = tmp271 + tmp57
  tmp735 = 1/tmp22 + tmp271
  tmp736 = 10*tmp1
  tmp737 = tmp447*tmp67
  tmp738 = -(tmp734/tmp22)
  tmp739 = tmp737 + tmp738
  tmp740 = 32*tmp54*tmp739
  tmp741 = tmp317 + tmp57
  tmp742 = tmp212*tmp741
  tmp743 = 64*ss*tmp1
  tmp744 = 88*tmp524
  tmp745 = tmp647 + tmp731 + tmp743 + tmp744
  tmp746 = tmp745/tmp21
  tmp747 = -2*tmp524
  tmp748 = tmp182 + tmp274 + tmp732 + tmp747
  tmp749 = tmp748/tmp22
  tmp750 = tmp742 + tmp746 + tmp749
  tmp751 = 16*tmp37*tmp750
  tmp752 = -10*tmp1
  tmp753 = tmp270 + tmp752
  tmp754 = 128*tmp41*tmp753
  tmp755 = tmp734*tmp9
  tmp756 = 38*tmp1
  tmp757 = tmp633 + tmp756
  tmp758 = tmp15*tmp757
  tmp759 = tmp733 + tmp755 + tmp758
  tmp760 = -(tmp759/tmp22)
  tmp761 = -30*tmp1
  tmp762 = tmp658 + tmp761
  tmp763 = ss*tmp437*tmp762
  tmp764 = tmp305 + tmp661
  tmp765 = tmp15*tmp764
  tmp766 = tmp472 + tmp763 + tmp765
  tmp767 = tmp67*tmp766
  tmp768 = tmp356*tmp447
  tmp769 = -26*tmp1
  tmp770 = tmp634 + tmp769
  tmp771 = tmp770/tmp22
  tmp772 = tmp768 + tmp771
  tmp773 = -16*tmp6*tmp772
  tmp774 = tmp754 + tmp760 + tmp767 + tmp773
  tmp775 = tmp774*tmp98
  tmp776 = tmp240*tmp735
  tmp777 = tmp57 + tmp736
  tmp778 = tmp15*tmp777
  tmp779 = tmp139 + tmp583
  tmp780 = tmp779*tmp99
  tmp781 = tmp270 + tmp736
  tmp782 = tmp781*tmp88
  tmp783 = tmp778 + tmp780 + tmp782
  tmp784 = tmp15*tmp783
  tmp785 = tmp56*tmp735
  tmp786 = tmp365 + tmp736
  tmp787 = tmp786/tmp22
  tmp788 = tmp785 + tmp787
  tmp789 = -16*tmp41*tmp788
  tmp790 = 10*tmp15*tmp735
  tmp791 = tmp139 + tmp271
  tmp792 = tmp357*tmp791
  tmp793 = 30*tmp1
  tmp794 = tmp633 + tmp793
  tmp795 = tmp74*tmp794
  tmp796 = tmp790 + tmp792 + tmp795
  tmp797 = (tmp109*tmp796)/tmp21
  tmp798 = tmp213*tmp735
  tmp799 = tmp40 + tmp736
  tmp800 = (tmp190*tmp799)/tmp22
  tmp801 = 102*tmp1
  tmp802 = tmp634 + tmp801
  tmp803 = tmp15*tmp802
  tmp804 = tmp798 + tmp800 + tmp803
  tmp805 = tmp27*tmp804
  tmp806 = tmp776 + tmp784 + tmp789 + tmp797 + tmp805
  tmp807 = -(tmp806/tmp22)
  tmp808 = (256*tmp42)/tmp599
  tmp809 = tmp17/tmp599
  tmp810 = tmp437 + tmp639
  tmp811 = tmp810/tmp22
  tmp812 = tmp809 + tmp811
  tmp813 = -128*tmp41*tmp812
  tmp814 = tmp182*tmp735
  tmp815 = tmp270 + tmp305
  tmp816 = tmp357*tmp815
  tmp817 = tmp583 + tmp633
  tmp818 = tmp74*tmp817
  tmp819 = tmp814 + tmp816 + tmp818
  tmp820 = tmp15*tmp819
  tmp821 = tmp253/tmp599
  tmp822 = -14*tmp1
  tmp823 = tmp638 + tmp822
  tmp824 = tmp635*tmp823
  tmp825 = tmp715 + tmp736
  tmp826 = tmp15*tmp825
  tmp827 = tmp821 + tmp824 + tmp826
  tmp828 = tmp352*tmp827
  tmp829 = 128*tmp631*tmp74
  tmp830 = tmp270 + tmp453
  tmp831 = tmp213*tmp830
  tmp832 = 74*tmp1
  tmp833 = tmp725 + tmp832
  tmp834 = tmp15*tmp833
  tmp835 = tmp829 + tmp831 + tmp834
  tmp836 = -(tmp835/(tmp21*tmp22))
  tmp837 = tmp808 + tmp813 + tmp820 + tmp828 + tmp836
  tmp838 = (4*tmp837)/tmp526
  tmp839 = tmp740 + tmp751 + tmp775 + tmp807 + tmp838
  tmp840 = 8*tmp1
  tmp841 = 8/tmp22
  tmp842 = tmp139 + tmp305
  tmp843 = 160*ss*tmp132
  tmp844 = (tmp622*tmp9)/tmp22
  tmp845 = 27/tmp22
  tmp846 = tmp132*tmp656
  tmp847 = -20/tmp22
  tmp848 = tmp840 + tmp847
  tmp849 = tmp848/tmp21
  tmp850 = tmp622/tmp22
  tmp851 = tmp212 + tmp849 + tmp850
  tmp852 = 32*tmp54*tmp851
  tmp853 = 256*tmp41
  tmp854 = tmp317 + tmp426
  tmp855 = -16*tmp6*tmp854
  tmp856 = tmp1*tmp55
  tmp857 = -11*tmp524
  tmp858 = 20*tmp74
  tmp859 = 20*tmp15
  tmp860 = tmp856 + tmp857 + tmp858 + tmp859
  tmp861 = (4*tmp860)/tmp21
  tmp862 = tmp1 + tmp519
  tmp863 = tmp862/tmp22
  tmp864 = tmp11*tmp622
  tmp865 = tmp863 + tmp864
  tmp866 = tmp865/tmp22
  tmp867 = tmp853 + tmp855 + tmp861 + tmp866
  tmp868 = 16*tmp37*tmp867
  tmp869 = 256*tmp135
  tmp870 = tmp437 + tmp46 + tmp56
  tmp871 = -64*tmp42*tmp870
  tmp872 = tmp437 + tmp46
  tmp873 = tmp190*tmp872
  tmp874 = tmp139*tmp842
  tmp875 = tmp213 + tmp873 + tmp874
  tmp876 = tmp598*tmp875
  tmp877 = tmp1*tmp213
  tmp878 = -17*tmp1
  tmp879 = 1/tmp22 + tmp878
  tmp880 = -3*tmp15*tmp879
  tmp881 = ss*tmp781*tmp841
  tmp882 = tmp877 + tmp880 + tmp881
  tmp883 = tmp27*tmp882
  tmp884 = tmp227*tmp735
  tmp885 = tmp270 + tmp271
  tmp886 = tmp885*tmp99
  tmp887 = tmp202 + tmp261
  tmp888 = tmp308*tmp887
  tmp889 = tmp884 + tmp886 + tmp888
  tmp890 = (tmp109*tmp889)/tmp21
  tmp891 = tmp678 + tmp869 + tmp871 + tmp876 + tmp883 + tmp890
  tmp892 = tmp891/tmp22
  tmp893 = tmp46 + tmp56 + tmp841
  tmp894 = -128*tmp42*tmp893
  tmp895 = tmp261 + tmp317
  tmp896 = tmp57*tmp895
  tmp897 = tmp305 + tmp40
  tmp898 = tmp56*tmp897
  tmp899 = tmp213 + tmp896 + tmp898
  tmp900 = tmp284*tmp899
  tmp901 = -tmp710
  tmp902 = tmp139 + tmp46
  tmp903 = tmp213*tmp902
  tmp904 = tmp633 + tmp712
  tmp905 = ss*tmp841*tmp904
  tmp906 = 52/tmp22
  tmp907 = tmp202 + tmp906
  tmp908 = tmp15*tmp907
  tmp909 = tmp903 + tmp905 + tmp908
  tmp910 = -4*tmp6*tmp909
  tmp911 = tmp377*tmp842
  tmp912 = 67/tmp22
  tmp913 = tmp724 + tmp912
  tmp914 = tmp15*tmp913
  tmp915 = -16*tmp1
  tmp916 = 93/tmp22
  tmp917 = tmp915 + tmp916
  tmp918 = ss*tmp437*tmp917
  tmp919 = tmp911 + tmp914 + tmp918
  tmp920 = tmp919/(tmp21*tmp22)
  tmp921 = tmp425 + tmp894 + tmp900 + tmp901 + tmp910 + tmp920
  tmp922 = (4*tmp921)/tmp526
  tmp923 = 2560*tmp42
  tmp924 = tmp356 + tmp752 + tmp845
  tmp925 = -64*tmp41*tmp924
  tmp926 = -48*ss*tmp1
  tmp927 = tmp666/tmp22
  tmp928 = 264*tmp74
  tmp929 = 79*tmp15
  tmp930 = tmp213 + tmp926 + tmp927 + tmp928 + tmp929
  tmp931 = tmp7*tmp930
  tmp932 = tmp842*tmp9
  tmp933 = ss*tmp659*tmp841
  tmp934 = 99/tmp22
  tmp935 = tmp305 + tmp934
  tmp936 = tmp15*tmp935
  tmp937 = tmp932 + tmp933 + tmp936
  tmp938 = tmp24*tmp937
  tmp939 = tmp843 + tmp844 + tmp846 + tmp923 + tmp925 + tmp931 + tmp938
  tmp940 = tmp939*tmp98
  tmp941 = tmp852 + tmp868 + tmp892 + tmp922 + tmp940
  tmp942 = 48*tmp6
  tmp943 = tmp139 + tmp142
  tmp944 = tmp943/tmp22
  tmp945 = tmp155/tmp22
  tmp946 = tmp10 + 1/tmp22
  tmp947 = tmp140*tmp946
  tmp948 = -6*tmp6
  tmp949 = 1/(tmp21*tmp22)
  tmp950 = tmp948 + tmp949 + tmp99
  tmp951 = -(tmp132*tmp950)
  tmp952 = (32*tmp29)/tmp21
  tmp953 = tmp942 + tmp944 + tmp952
  tmp954 = tmp307*tmp953
  tmp955 = tmp138 + tmp40
  tmp956 = -4*tmp6*tmp955
  tmp957 = tmp237 + tmp635 + tmp9
  tmp958 = -(tmp957/tmp21)
  tmp959 = tmp185 + tmp945 + tmp956 + tmp958
  tmp960 = -4*tmp4*tmp959
  tmp961 = 48*tmp42
  tmp962 = tmp118*tmp8
  tmp963 = tmp107 + tmp34
  tmp964 = -32*tmp41*tmp963
  tmp965 = 18*tmp74
  tmp966 = tmp227 + tmp9 + tmp965
  tmp967 = -(tmp949*tmp966)
  tmp968 = 49*tmp15
  tmp969 = tmp377 + tmp391 + tmp968
  tmp970 = tmp6*tmp969
  tmp971 = tmp961 + tmp962 + tmp964 + tmp967 + tmp970
  tmp972 = (-2*tmp971)/tmp526
  tmp973 = tmp947 + tmp951 + tmp954 + tmp960 + tmp972
  tmp974 = tmp128*tmp163
  tmp975 = tmp128*tmp56
  tmp976 = 3*kappa
  tmp977 = -4 + tmp976
  tmp978 = tmp1*tmp977
  tmp979 = -1 + tmp978
  tmp980 = tmp131/tmp22
  tmp981 = -6*tmp4*tmp50
  tmp982 = -2*tmp8
  tmp983 = -tmp949
  tmp984 = tmp211 + tmp982 + tmp983
  tmp985 = -(tmp36*tmp984)
  tmp986 = tmp18/tmp21
  tmp987 = tmp161 + tmp27 + tmp986
  tmp988 = tmp987/tmp526
  tmp989 = tmp353 + tmp981 + tmp985 + tmp988
  tmp990 = (4*tmp128)/tmp21
  tmp991 = tmp129*tmp570
  tmp992 = tmp29*tmp35*tmp980
  tmp993 = tmp128*tmp357
  tmp994 = kappa*tmp524
  tmp995 = 1/tmp22 + tmp994
  tmp996 = tmp129*tmp524
  tmp997 = tmp129*tmp95
  tmp998 = tmp31 + tmp996 + tmp997
  tmp999 = tmp3*tmp976
  tmp1000 = 2 + kappa
  tmp1001 = tmp977/tmp22
  tmp1002 = tmp1001 + tmp302
  tmp1003 = 5*kappa
  tmp1004 = tmp1 + tmp434
  tmp1005 = 3*tmp3
  tmp1006 = 288*tmp135
  tmp1007 = 20*ss
  tmp1008 = tmp1007 + tmp519 + tmp563
  tmp1009 = -16*tmp1008*tmp42
  tmp1010 = tmp1 + tmp28
  tmp1011 = tmp1010*tmp132*tmp209
  tmp1012 = -96*ss*tmp1
  tmp1013 = -222*tmp15
  tmp1014 = tmp1012 + tmp1013 + tmp213 + tmp217 + tmp449 + tmp456
  tmp1015 = tmp1014*tmp41
  tmp1016 = tmp1004*tmp163
  tmp1017 = -2*tmp132
  tmp1018 = tmp487 + tmp560 + tmp747
  tmp1019 = tmp1018/tmp21
  tmp1020 = tmp1016 + tmp1017 + tmp1019 + tmp284
  tmp1021 = tmp1020*tmp98
  tmp1022 = tmp1004*tmp357
  tmp1023 = tmp1005 + tmp182 + tmp200
  tmp1024 = ss*tmp1023
  tmp1025 = 18*tmp15
  tmp1026 = tmp1025 + tmp3 + tmp747
  tmp1027 = tmp1026/tmp22
  tmp1028 = tmp1022 + tmp1024 + tmp1027
  tmp1029 = tmp1028*tmp352
  tmp1030 = ss*tmp538*tmp581
  tmp1031 = tmp182 + tmp3 + tmp747
  tmp1032 = tmp1031*tmp16
  tmp1033 = 34*tmp15
  tmp1034 = tmp1033 + tmp304 + tmp449
  tmp1035 = tmp1034*tmp8
  tmp1036 = tmp1030 + tmp1032 + tmp1035
  tmp1037 = tmp1036/tmp21
  tmp1038 = 224*tmp42
  tmp1039 = tmp132*tmp431
  tmp1040 = tmp28 + tmp902
  tmp1041 = -16*tmp1040*tmp41
  tmp1042 = tmp583/tmp22
  tmp1043 = 40*tmp74
  tmp1044 = tmp1042 + tmp1043 + tmp16 + tmp561 + tmp856
  tmp1045 = tmp1044*tmp27
  tmp1046 = tmp1005 + tmp200 + tmp466
  tmp1047 = ss*tmp1046
  tmp1048 = tmp15 + tmp3 + tmp304
  tmp1049 = tmp1048/tmp22
  tmp1050 = tmp1047 + tmp1049
  tmp1051 = tmp1050*tmp26
  tmp1052 = tmp1038 + tmp1039 + tmp1041 + tmp1045 + tmp1051
  tmp1053 = (2*tmp1052)/tmp526
  tmp1054 = tmp1006 + tmp1009 + tmp1011 + tmp1015 + tmp1021 + tmp1029 + tmp1037 + &
                          &tmp1053
  tmp1055 = tmp53**2
  tmp1056 = tmp21**(-6)
  tmp1057 = 4 + tmp840
  tmp1058 = -4 + kappa
  tmp1059 = -8*tmp1
  tmp1060 = -9*tmp1
  tmp1061 = tmp1058*tmp46
  tmp1062 = 2 + tmp1061
  tmp1063 = tmp1062/tmp22
  tmp1064 = tmp129*tmp285
  tmp1065 = tmp1*tmp1058
  tmp1066 = 4 + kappa
  tmp1067 = -2 + tmp1064
  tmp1068 = tmp129*tmp271
  tmp1069 = tmp1063 + tmp31
  tmp1070 = tmp1069/tmp22
  tmp1071 = tmp129*tmp583
  tmp1072 = -2*tmp1066*tmp3
  tmp1073 = 20/tmp22
  tmp1074 = tmp129*tmp433
  tmp1075 = 4*kappa
  tmp1076 = tmp1071/tmp22
  tmp1077 = tmp1067*tmp139
  tmp1078 = -12*tmp1
  tmp1079 = -3 + kappa
  tmp1080 = tmp1064/tmp22
  tmp1081 = 13*tmp1
  tmp1082 = 128*ss
  tmp1083 = 2*kappa
  tmp1084 = ss**4
  tmp1085 = 5*tmp1070
  tmp1086 = tmp1058/tmp22
  tmp1087 = tmp1086 + tmp439
  tmp1088 = (tmp1005*tmp1087)/tmp22
  tmp1089 = tmp1060*tmp129
  tmp1090 = -7 + tmp1071
  tmp1091 = 192*tmp43
  tmp1092 = -3*tmp1065
  tmp1093 = 21/tmp22
  tmp1094 = 1/tmp1055
  tmp1095 = 64*tmp196*tmp272
  tmp1096 = tmp561/tmp22
  tmp1097 = tmp17 + tmp580
  tmp1098 = -8*tmp1097*tmp6
  tmp1099 = ss*tmp640
  tmp1100 = tmp1096 + tmp1098 + tmp1099 + tmp185
  tmp1101 = 4*tmp1100*tmp36*tmp949
  tmp1102 = 28*tmp6
  tmp1103 = tmp1*tmp29
  tmp1104 = tmp190 + tmp270 + tmp285
  tmp1105 = -(tmp1104/tmp21)
  tmp1106 = tmp1102 + tmp1103 + tmp1105
  tmp1107 = tmp1106*tmp214
  tmp1108 = 256*tmp42
  tmp1109 = -9/tmp22
  tmp1110 = tmp1109 + tmp46 + tmp56
  tmp1111 = -64*tmp1110*tmp41
  tmp1112 = tmp213 + tmp226 + tmp227
  tmp1113 = tmp1112*tmp570
  tmp1114 = 7*tmp524
  tmp1115 = 68*tmp74
  tmp1116 = tmp1114 + tmp1115 + tmp274 + tmp374
  tmp1117 = -8*tmp1116*tmp6
  tmp1118 = 8*tmp452
  tmp1119 = tmp524*tmp588
  tmp1120 = tmp406/tmp22
  tmp1121 = 25*tmp498
  tmp1122 = 84*ss*tmp15
  tmp1123 = 11*tmp132
  tmp1124 = tmp1118 + tmp1119 + tmp1120 + tmp1121 + tmp1122 + tmp1123 + tmp503
  tmp1125 = tmp1124*tmp26
  tmp1126 = tmp1108 + tmp1111 + tmp1113 + tmp1117 + tmp1125
  tmp1127 = -2*tmp1126*tmp4
  tmp1128 = 576*tmp41
  tmp1129 = 19/tmp22
  tmp1130 = tmp1129 + tmp271 + tmp588
  tmp1131 = -32*tmp1130*tmp6
  tmp1132 = tmp213 + tmp217 + tmp218
  tmp1133 = tmp1132*tmp46
  tmp1134 = 21*tmp15
  tmp1135 = tmp444*tmp685
  tmp1136 = tmp1134 + tmp1135 + tmp253 + tmp3 + tmp450
  tmp1137 = (4*tmp1136)/tmp21
  tmp1138 = tmp1128 + tmp1131 + tmp1133 + tmp1137
  tmp1139 = tmp1138*tmp353
  tmp1140 = -896*tmp135
  tmp1141 = 14*ss
  tmp1142 = tmp1141 + tmp902
  tmp1143 = tmp1142*tmp359
  tmp1144 = tmp15 + tmp161 + tmp222
  tmp1145 = -(tmp1144*tmp498)
  tmp1146 = 112*tmp8
  tmp1147 = -9*tmp15
  tmp1148 = tmp1093 + tmp317
  tmp1149 = tmp1148*tmp56
  tmp1150 = tmp1146 + tmp1147 + tmp1149 + tmp200 + tmp560
  tmp1151 = tmp1150*tmp83
  tmp1152 = tmp261 + tmp305
  tmp1153 = tmp1152*tmp213
  tmp1154 = -26*tmp15
  tmp1155 = tmp1154 + tmp428 + tmp559
  tmp1156 = tmp1155/tmp22
  tmp1157 = tmp1005 + tmp15 + tmp304
  tmp1158 = tmp1157*tmp55
  tmp1159 = tmp1153 + tmp1156 + tmp1158
  tmp1160 = tmp1159*tmp27
  tmp1161 = tmp15 + tmp204 + tmp561
  tmp1162 = tmp1161*tmp15
  tmp1163 = tmp237 + tmp3 + tmp747
  tmp1164 = tmp1163*tmp161
  tmp1165 = tmp182 + tmp3 + tmp586
  tmp1166 = tmp1165*tmp74
  tmp1167 = tmp1162 + tmp1164 + tmp1166
  tmp1168 = (4*tmp1167)/tmp21
  tmp1169 = tmp1140 + tmp1143 + tmp1145 + tmp1151 + tmp1160 + tmp1168
  tmp1170 = tmp1169/tmp526
  tmp1171 = tmp1095 + tmp1101 + tmp1107 + tmp1127 + tmp1139 + tmp1170
  tmp1172 = tmp29*tmp309
  tmp1173 = 4*tmp309
  tmp1174 = -12*tmp6
  tmp1175 = tmp15 + tmp3
  tmp1176 = -2*tmp3
  tmp1177 = tmp182 + tmp201 + tmp448
  tmp1178 = tmp141*tmp310
  tmp1179 = tmp142*tmp310
  tmp1180 = tmp1178 + tmp1179 + tmp201 + tmp227 + tmp445
  tmp1181 = 5*tmp321
  tmp1182 = tmp163*tmp447
  tmp1183 = tmp447*tmp58
  tmp1184 = tmp201 + tmp227 + tmp448
  tmp1185 = tmp1184*tmp28
  tmp1186 = tmp447*tmp56
  tmp1187 = tmp1186 + tmp201 + tmp237 + tmp448
  tmp1188 = tmp1187*tmp546
  tmp1189 = tmp1181 + tmp1182 + tmp1183 + tmp1185 + tmp1188
  tmp1190 = tmp1175*tmp27
  tmp1191 = ss*tmp524*tmp735
  tmp1192 = tmp1175*tmp8
  tmp1193 = tmp1191 + tmp1192 + tmp460
  tmp1194 = 2*tmp1193
  tmp1195 = tmp11*tmp1175
  tmp1196 = tmp1176 + tmp15 + tmp304
  tmp1197 = tmp1196/tmp22
  tmp1198 = tmp1195 + tmp1197
  tmp1199 = tmp1198/tmp21
  tmp1200 = tmp1190 + tmp1194 + tmp1199
  tmp1201 = tmp202 + 1/tmp22
  tmp1202 = tmp1201*tmp498
  tmp1203 = tmp46 + tmp841
  tmp1204 = tmp1203*tmp28*tmp524
  tmp1205 = tmp1177*tmp6
  tmp1206 = tmp1177*tmp8
  tmp1207 = 6*tmp3
  tmp1208 = tmp1207 + tmp237 + tmp456
  tmp1209 = tmp1208/tmp22
  tmp1210 = tmp1177*tmp209
  tmp1211 = tmp1209 + tmp1210
  tmp1212 = tmp1211/tmp21
  tmp1213 = tmp1202 + tmp1204 + tmp1205 + tmp1206 + tmp1212
  tmp1214 = 1 + tmp271
  tmp1215 = -(kappa*tmp512)
  tmp1216 = 3 + tmp583
  tmp1217 = tmp1216/tmp22
  tmp1218 = tmp1 + tmp1217
  tmp1219 = 2 + tmp976
  tmp1220 = tmp1*tmp1219
  tmp1221 = 2 + tmp1220
  tmp1222 = tmp1221*tmp524
  tmp1223 = 2 + tmp433
  tmp1224 = tmp1223*tmp15
  tmp1225 = tmp1215 + tmp1222 + tmp1224
  tmp1226 = tmp198 + tmp200
  tmp1227 = -34*tmp15
  tmp1228 = tmp1207 + tmp1227 + tmp304
  tmp1229 = tmp1010*tmp132*tmp28
  tmp1230 = 1/tmp22 + tmp639
  tmp1231 = tmp1230*tmp165
  tmp1232 = tmp752/tmp22
  tmp1233 = (-64*ss)/tmp599
  tmp1234 = tmp1005 + tmp1232 + tmp1233 + tmp255
  tmp1235 = tmp1234*tmp354
  tmp1236 = 4*tmp132
  tmp1237 = tmp212*tmp631
  tmp1238 = tmp1228/tmp21
  tmp1239 = tmp1236 + tmp1237 + tmp1238
  tmp1240 = tmp1239*tmp4
  tmp1241 = tmp1228*tmp8
  tmp1242 = tmp736/tmp22
  tmp1243 = tmp1242 + tmp15 + tmp560
  tmp1244 = (tmp1243*tmp209)/tmp22
  tmp1245 = tmp182 + tmp3 + tmp562
  tmp1246 = tmp1245*tmp15
  tmp1247 = tmp1241 + tmp1244 + tmp1246
  tmp1248 = tmp1247/tmp21
  tmp1249 = tmp58*tmp631
  tmp1250 = 12*tmp15
  tmp1251 = tmp1250 + tmp3 + tmp327
  tmp1252 = -(tmp1251/tmp22)
  tmp1253 = tmp403 + tmp560 + tmp747
  tmp1254 = ss*tmp1253
  tmp1255 = tmp1249 + tmp1252 + tmp1254
  tmp1256 = tmp1255*tmp352
  tmp1257 = -tmp1039
  tmp1258 = tmp258/tmp599
  tmp1259 = tmp190*tmp644
  tmp1260 = tmp1042 + tmp1259 + tmp3 + tmp495
  tmp1261 = tmp1260*tmp27
  tmp1262 = tmp15 + tmp486 + tmp561
  tmp1263 = tmp1262/tmp22
  tmp1264 = tmp200 + tmp487 + tmp560
  tmp1265 = ss*tmp1264
  tmp1266 = tmp1263 + tmp1265
  tmp1267 = tmp1266*tmp26
  tmp1268 = tmp1257 + tmp1258 + tmp1261 + tmp1267
  tmp1269 = (2*tmp1268)/tmp526
  tmp1270 = tmp1229 + tmp1231 + tmp1235 + tmp1240 + tmp1248 + tmp1256 + tmp1269
  tmp1271 = 2*tmp309
  tmp1272 = tmp1 + tmp343 + tmp94
  tmp1273 = tmp154 + tmp712
  tmp1274 = -10*tmp74
  tmp1275 = -2*tmp309
  tmp1276 = tmp1 + tmp107
  tmp1277 = (tmp1276*tmp209)/tmp599
  tmp1278 = -29*tmp1
  tmp1279 = tmp1278 + tmp84
  tmp1280 = tmp1279*tmp27
  tmp1281 = tmp1*tmp138
  tmp1282 = tmp1281 + tmp16 + tmp3 + tmp327 + tmp335 + tmp429
  tmp1283 = tmp1282/tmp21
  tmp1284 = tmp1277 + tmp1280 + tmp1283 + tmp598
  tmp1285 = -8 + tmp1003
  tmp1286 = tmp129*tmp840
  tmp1287 = tmp129/tmp22
  tmp1288 = tmp1287 + tmp302
  tmp1289 = -2 + tmp976
  tmp1290 = tmp1*tmp638
  tmp1291 = -4*tmp129*tmp15
  tmp1292 = -30 + kappa
  tmp1293 = tmp129*tmp3
  tmp1294 = 7*kappa
  tmp1295 = 24*tmp42
  tmp1296 = tmp307*tmp534
  tmp1297 = tmp1272*tmp4*tmp534
  tmp1298 = -((tmp1010*tmp8)/tmp599)
  tmp1299 = -2*tmp1273*tmp41
  tmp1300 = tmp1271 + tmp16 + tmp304 + tmp560 + tmp58
  tmp1301 = (ss*tmp1300)/tmp21
  tmp1302 = tmp1060 + tmp139
  tmp1303 = tmp1302*tmp28
  tmp1304 = tmp1290 + tmp1303 + tmp15 + tmp301 + tmp428
  tmp1305 = tmp1304*tmp6
  tmp1306 = -(tmp1284/tmp526)
  tmp1307 = tmp1295 + tmp1296 + tmp1297 + tmp1298 + tmp1299 + tmp1301 + tmp1305 + &
                          &tmp1306
  tmp1308 = tmp1307*tmp2*tmp24
  tmp1309 = -24*tmp42
  tmp1310 = -tmp1297
  tmp1311 = (tmp1010*tmp8)/tmp599
  tmp1312 = tmp1273*tmp354
  tmp1313 = 18*ss*tmp1
  tmp1314 = tmp1278/tmp22
  tmp1315 = tmp1176 + tmp1274 + tmp1313 + tmp1314 + tmp16 + tmp58
  tmp1316 = tmp1315*tmp6
  tmp1317 = tmp1005 + tmp1275 + tmp15 + tmp301 + tmp486
  tmp1318 = (ss*tmp1317)/tmp21
  tmp1319 = tmp1284/tmp526
  tmp1320 = tmp1309 + tmp1310 + tmp1311 + tmp1312 + tmp1316 + tmp1318 + tmp1319 + &
                          &tmp602
  tmp1321 = f1p*tmp1320*tmp2*tmp24
  tmp1322 = -96*tmp129*tmp135
  tmp1323 = -((kappa*tmp1103*tmp8)/tmp599)
  tmp1324 = tmp1079*tmp433
  tmp1325 = tmp1285*tmp17
  tmp1326 = tmp1294/tmp22
  tmp1327 = tmp1324 + tmp1325 + tmp1326
  tmp1328 = 8*tmp1327*tmp42
  tmp1329 = kappa*tmp7
  tmp1330 = tmp127*tmp620
  tmp1331 = tmp1288*tmp24
  tmp1332 = tmp1329 + tmp1330 + tmp1331
  tmp1333 = tmp1332*tmp307
  tmp1334 = 6 + kappa
  tmp1335 = tmp1334*tmp561
  tmp1336 = tmp1289*tmp9
  tmp1337 = 79*kappa
  tmp1338 = -130 + tmp1337
  tmp1339 = tmp1338*tmp524
  tmp1340 = tmp129*tmp134
  tmp1341 = 11*kappa
  tmp1342 = -2 + tmp1341
  tmp1343 = tmp1342/tmp22
  tmp1344 = tmp1286 + tmp1343
  tmp1345 = tmp1344*tmp17
  tmp1346 = tmp1335 + tmp1336 + tmp1339 + tmp1340 + tmp1345
  tmp1347 = -2*tmp1346*tmp41
  tmp1348 = tmp1288*tmp301
  tmp1349 = tmp237 + tmp3 + tmp486
  tmp1350 = tmp127*tmp1349
  tmp1351 = -3*kappa
  tmp1352 = 2 + tmp1351
  tmp1353 = tmp1352*tmp428
  tmp1354 = -5*kappa
  tmp1355 = 2 + tmp1354
  tmp1356 = 2*tmp1355*tmp524
  tmp1357 = tmp1291 + tmp1353 + tmp1356
  tmp1358 = ss*tmp1357
  tmp1359 = tmp1348 + tmp1350 + tmp1358
  tmp1360 = (ss*tmp1359)/tmp21
  tmp1361 = 32*kappa*tmp43
  tmp1362 = tmp1289*tmp271
  tmp1363 = -4 + tmp1003
  tmp1364 = tmp1363/tmp22
  tmp1365 = tmp1362 + tmp1364
  tmp1366 = tmp1365*tmp58
  tmp1367 = tmp1290 + tmp446
  tmp1368 = tmp1367*tmp439
  tmp1369 = -8 + kappa
  tmp1370 = tmp1369*tmp3
  tmp1371 = 37*kappa
  tmp1372 = -36 + tmp1371
  tmp1373 = tmp1372*tmp524
  tmp1374 = 6*tmp129*tmp15
  tmp1375 = tmp1370 + tmp1373 + tmp1374
  tmp1376 = tmp1375*tmp28
  tmp1377 = tmp1361 + tmp1366 + tmp1368 + tmp1376
  tmp1378 = tmp1377*tmp6
  tmp1379 = tmp1000*tmp185
  tmp1380 = tmp127*tmp604
  tmp1381 = tmp1000*tmp305
  tmp1382 = kappa*tmp138
  tmp1383 = tmp1066/tmp22
  tmp1384 = tmp1381 + tmp1382 + tmp1383
  tmp1385 = tmp1384*tmp163
  tmp1386 = tmp1000*tmp428
  tmp1387 = -9*kappa
  tmp1388 = 2 + tmp1387
  tmp1389 = 2*tmp1388*tmp524
  tmp1390 = tmp1288*tmp537
  tmp1391 = tmp1291 + tmp1386 + tmp1389 + tmp1390
  tmp1392 = tmp1391/tmp21
  tmp1393 = tmp1379 + tmp1380 + tmp1385 + tmp1392
  tmp1394 = tmp1393*tmp4
  tmp1395 = kappa*tmp165
  tmp1396 = tmp127*tmp619
  tmp1397 = tmp129*tmp55
  tmp1398 = tmp1292/tmp22
  tmp1399 = tmp1397 + tmp1398
  tmp1400 = tmp1399*tmp598
  tmp1401 = tmp130*tmp190
  tmp1402 = kappa*tmp377
  tmp1403 = 73*kappa
  tmp1404 = -60 + tmp1403
  tmp1405 = tmp1404*tmp524
  tmp1406 = (kappa*tmp444)/tmp22
  tmp1407 = -2*tmp129*tmp15
  tmp1408 = tmp1401 + tmp1402 + tmp1405 + tmp1406 + tmp1407 + tmp44
  tmp1409 = tmp1408*tmp27
  tmp1410 = -24*tmp1288*tmp8
  tmp1411 = tmp16 + tmp201 + tmp3
  tmp1412 = tmp127*tmp1411
  tmp1413 = -2 + tmp1294
  tmp1414 = tmp1413*tmp524
  tmp1415 = tmp1293 + tmp1414 + tmp997
  tmp1416 = tmp11*tmp1415
  tmp1417 = tmp1410 + tmp1412 + tmp1416
  tmp1418 = tmp1417/tmp21
  tmp1419 = tmp1395 + tmp1396 + tmp1400 + tmp1409 + tmp1418
  tmp1420 = -(tmp1419/tmp526)
  tmp1421 = tmp1322 + tmp1323 + tmp1328 + tmp1333 + tmp1347 + tmp1360 + tmp1378 + &
                          &tmp1394 + tmp1420
  tmp1422 = f2p*tmp1421
  tmp1423 = tmp1308 + tmp1321 + tmp1422
  tmp1424 = tmp190*tmp524
  tmp1425 = -96*ss
  tmp1426 = -32*tmp8
  tmp1427 = 96*tmp42
  tmp1428 = 18*tmp524
  tmp1429 = 9*tmp15
  tmp1430 = tmp1 + tmp946
  tmp1431 = 7*tmp3
  tmp1432 = tmp142/tmp599
  tmp1433 = tmp437 + tmp56 + tmp563
  tmp1434 = tmp1433*tmp67
  tmp1435 = tmp1431 + tmp1432 + tmp1434 + tmp227 + tmp486 + tmp942
  tmp1436 = tmp41*tmp840
  tmp1437 = ss*tmp1010*tmp15
  tmp1438 = tmp237 + tmp274 + tmp486
  tmp1439 = -2*tmp1438*tmp6
  tmp1440 = tmp8*tmp840
  tmp1441 = tmp15*tmp583
  tmp1442 = tmp132 + tmp1424 + tmp1440 + tmp1441
  tmp1443 = tmp1442/tmp21
  tmp1444 = tmp1436 + tmp1437 + tmp1439 + tmp1443
  tmp1445 = -88/tmp22
  tmp1446 = tmp1425 + tmp1445 + tmp840
  tmp1447 = tmp1446*tmp6
  tmp1448 = -5*tmp3
  tmp1449 = tmp1448 + tmp227 + tmp486
  tmp1450 = tmp1449*tmp28
  tmp1451 = tmp1*tmp906
  tmp1452 = -6*tmp15
  tmp1453 = tmp1179 + tmp1207 + tmp1426 + tmp1451 + tmp1452
  tmp1454 = tmp1453/tmp21
  tmp1455 = tmp1249 + tmp1447 + tmp1450 + tmp1454 + tmp258 + tmp503
  tmp1456 = tmp1203 + tmp138
  tmp1457 = tmp182 + tmp486 + tmp560
  tmp1458 = ss*tmp915
  tmp1459 = tmp1005 + tmp1458 + tmp304 + tmp969
  tmp1460 = tmp58*tmp637
  tmp1461 = tmp1005 + tmp1428 + tmp227
  tmp1462 = tmp1461/tmp22
  tmp1463 = tmp1005 + tmp1429 + tmp201
  tmp1464 = tmp1463*tmp28
  tmp1465 = tmp1460 + tmp1462 + tmp1464
  tmp1466 = kappa*tmp305
  tmp1467 = tmp427/tmp22
  tmp1468 = -1 + tmp1083
  tmp1469 = tmp1*tmp1468
  tmp1470 = tmp1 + tmp113
  tmp1471 = tmp1083*tmp431
  tmp1472 = -2 + tmp1003
  tmp1473 = kappa*tmp9
  tmp1474 = 6*kappa
  tmp1475 = tmp2*tmp34
  tmp1476 = tmp127 + tmp1475
  tmp1477 = tmp1476*tmp190
  tmp1478 = -2*tmp6
  tmp1479 = tmp1 + tmp26 + tmp94
  tmp1480 = -(tmp1479*tmp4)
  tmp1481 = ss*tmp1010
  tmp1482 = tmp11 + tmp310
  tmp1483 = tmp1482/tmp21
  tmp1484 = tmp1481 + tmp1483 + tmp27
  tmp1485 = tmp1484*tmp36
  tmp1486 = tmp1276*tmp28
  tmp1487 = tmp1478 + tmp1486 + tmp572
  tmp1488 = tmp1487/tmp526
  tmp1489 = tmp1480 + tmp1485 + tmp1488 + tmp353
  tmp1490 = tmp1*tmp1000
  tmp1491 = tmp1226*tmp56
  tmp1492 = tmp1214*tmp15
  tmp1493 = tmp1226*tmp598
  tmp1494 = tmp310*tmp498
  tmp1495 = ss*tmp1*tmp637*tmp841
  tmp1496 = tmp1457*tmp99
  tmp1497 = -3*tmp15
  tmp1498 = tmp141/tmp599
  tmp1499 = tmp1431 + tmp1432 + tmp1498 + tmp227 + tmp486
  tmp1500 = tmp163*tmp631
  tmp1501 = tmp444/tmp22
  tmp1502 = tmp1501 + tmp237 + tmp486 + tmp560 + tmp856
  tmp1503 = tmp1502*tmp546
  tmp1504 = tmp1249 + tmp1450 + tmp1500 + tmp1503 + tmp503
  tmp1505 = tmp1457*tmp27
  tmp1506 = tmp1242 + tmp1497 + tmp560
  tmp1507 = tmp1506/tmp22
  tmp1508 = tmp1457*tmp28
  tmp1509 = tmp1507 + tmp1508
  tmp1510 = tmp1509*tmp546
  tmp1511 = tmp1494 + tmp1495 + tmp1496 + tmp1505 + tmp1510
  tmp1512 = tmp200 + tmp580
  tmp1513 = tmp11/tmp599
  tmp1514 = tmp1467 + tmp1513
  tmp1515 = kappa*tmp512
  tmp1516 = -2*tmp1492
  tmp1517 = tmp1226 + tmp3
  tmp1518 = 1 + kappa
  tmp1519 = tmp1000*tmp3
  tmp1520 = tmp357*tmp644
  tmp1521 = tmp644*tmp88
  tmp1522 = tmp132 + tmp1520 + tmp1521
  tmp1523 = tmp1218*tmp58
  tmp1524 = tmp1216*tmp15
  tmp1525 = 64*tmp135
  tmp1526 = ss*tmp433
  tmp1527 = -7*tmp3
  tmp1528 = tmp1527 + tmp237 + tmp450
  tmp1529 = tmp12 + tmp28
  tmp1530 = tmp17 + tmp360 + tmp46
  tmp1531 = -64*tmp1530*tmp42
  tmp1532 = -32*tmp524
  tmp1533 = -27*tmp15
  tmp1534 = tmp1005 + tmp1532 + tmp1533 + tmp377
  tmp1535 = tmp1534*tmp441
  tmp1536 = -13*tmp74
  tmp1537 = tmp1526 + tmp1536 + tmp227
  tmp1538 = ss*tmp1537*tmp524
  tmp1539 = 192*tmp41
  tmp1540 = -13/tmp22
  tmp1541 = tmp1540 + tmp433
  tmp1542 = tmp1541*tmp524
  tmp1543 = -32*tmp6*tmp734
  tmp1544 = (4*tmp1528)/tmp21
  tmp1545 = tmp1539 + tmp1542 + tmp1543 + tmp1544
  tmp1546 = tmp1545*tmp4
  tmp1547 = tmp132*tmp317
  tmp1548 = tmp1528*tmp99
  tmp1549 = 11*tmp524
  tmp1550 = tmp134 + tmp1549 + tmp445
  tmp1551 = -(tmp1550*tmp74)
  tmp1552 = tmp1547 + tmp1548 + tmp1551
  tmp1553 = tmp1552*tmp26
  tmp1554 = tmp1426*tmp734
  tmp1555 = tmp200 + tmp237 + tmp3
  tmp1556 = tmp1555*tmp190
  tmp1557 = 63*tmp524
  tmp1558 = 14*tmp15
  tmp1559 = tmp1557 + tmp1558 + tmp449
  tmp1560 = tmp1559/tmp22
  tmp1561 = tmp1554 + tmp1556 + tmp1560
  tmp1562 = tmp1561*tmp6
  tmp1563 = -192*tmp1529*tmp41
  tmp1564 = tmp118 + tmp226 + tmp856
  tmp1565 = tmp1564*tmp524
  tmp1566 = tmp1452 + tmp274 + tmp586 + tmp732
  tmp1567 = tmp1566*tmp7
  tmp1568 = tmp1114 + tmp428 + tmp95
  tmp1569 = tmp1568*tmp443
  tmp1570 = tmp1528*tmp55
  tmp1571 = tmp1569 + tmp1570
  tmp1572 = tmp1571/tmp21
  tmp1573 = tmp1563 + tmp1565 + tmp1567 + tmp1572
  tmp1574 = tmp1573/tmp526
  tmp1575 = tmp1525 + tmp1531 + tmp1535 + tmp1538 + tmp1546 + tmp1553 + tmp1562 + &
                          &tmp1574
  tmp1576 = tmp526**(-6)
  tmp1577 = 2 + tmp130
  tmp1578 = -tmp130
  tmp1579 = 72*ss
  tmp1580 = -1 + tmp271
  tmp1581 = 240*tmp8
  tmp1582 = tmp21**(-7)
  tmp1583 = 4 + tmp1351
  tmp1584 = tmp1*tmp1583
  tmp1585 = 2 + tmp1584
  tmp1586 = tmp1079*tmp428
  tmp1587 = tmp1585/tmp22
  tmp1588 = -8 + tmp130
  tmp1589 = tmp1588/tmp22
  tmp1590 = 4 + tmp1578
  tmp1591 = tmp1590/tmp22
  tmp1592 = 4 + tmp283
  tmp1593 = tmp15*tmp1592
  tmp1594 = tmp1176*tmp2
  tmp1595 = -5 + tmp1083
  tmp1596 = tmp1587 + tmp31
  tmp1597 = tmp1*tmp976
  tmp1598 = -4 + tmp1597
  tmp1599 = 384*tmp43
  tmp1600 = 6 + tmp130
  tmp1601 = -2 + tmp130
  tmp1602 = 26*tmp1
  tmp1603 = -10*kappa
  tmp1604 = -4 + tmp130
  tmp1605 = tmp1604*tmp434
  tmp1606 = kappa*tmp1176
  tmp1607 = tmp1452*tmp1601
  tmp1608 = kappa*tmp1431
  tmp1609 = -8 + tmp976
  tmp1610 = tmp1*tmp1609
  tmp1611 = tmp129*tmp563
  tmp1612 = tmp15*tmp1585
  tmp1613 = 22/tmp22
  tmp1614 = -2 + tmp978
  tmp1615 = tmp1614/tmp22
  tmp1616 = 640*tmp43
  tmp1617 = tmp129*tmp202
  tmp1618 = tmp134*tmp1601
  tmp1619 = -8 + tmp1474
  tmp1620 = tmp1287*tmp639
  tmp1621 = 68/tmp22
  tmp1622 = tmp1*tmp1619
  tmp1623 = -4 + tmp1622
  tmp1624 = tmp526**(-8)
  tmp1625 = tmp526**(-7)
  tmp1626 = tmp1083*tmp524
  tmp1627 = tmp2*tmp428
  tmp1628 = 192*tmp8
  tmp1629 = tmp1580/tmp22
  tmp1630 = 12*tmp1
  tmp1631 = -19*tmp1
  tmp1632 = kappa*tmp702
  tmp1633 = -48*tmp1
  tmp1634 = -13*tmp3
  tmp1635 = tmp1*tmp1472
  tmp1636 = tmp282/tmp22
  tmp1637 = -7*kappa
  tmp1638 = -1280*tmp43
  tmp1639 = -5 + kappa
  tmp1640 = 10*tmp1492
  tmp1641 = tmp1220/tmp22
  tmp1642 = tmp2*tmp840
  tmp1643 = 3 + tmp271
  tmp1644 = 5*tmp1492
  tmp1645 = 9*kappa
  tmp1646 = 16*tmp1084
  tmp1647 = -2 + tmp1075
  tmp1648 = tmp1647*tmp3
  tmp1649 = 1280*tmp43
  tmp1650 = -1 + tmp285
  tmp1651 = -6 + tmp1294
  tmp1652 = tmp1*tmp1651
  tmp1653 = 2 + tmp1341
  tmp1654 = -1 + tmp583
  tmp1655 = -2 + tmp1645
  tmp1656 = tmp1*tmp1655
  tmp1657 = 13*kappa
  tmp1658 = ss**5
  tmp1659 = 11*tmp1
  tmp1660 = 1 + tmp583
  tmp1661 = -4*tmp3
  tmp1662 = tmp200 + tmp310
  tmp1663 = kappa*tmp132*tmp561
  tmp1664 = -22*tmp3
  tmp1665 = 6 + tmp1294
  tmp1666 = -16*kappa*tmp15
  tmp1667 = -6*tmp512
  tmp1668 = -12/tmp22
  tmp1669 = -2*tmp512
  tmp1670 = 128*tmp41
  tmp1671 = tmp1*tmp537
  tmp1672 = 19*tmp15
  tmp1673 = 5*tmp3
  tmp1674 = 10*tmp15
  tmp1675 = tmp1*tmp264
  tmp1676 = -3*tmp512
  tmp1677 = tmp481/tmp22
  tmp1678 = (256*tmp196)/tmp21
  tmp1679 = tmp1100*tmp36*tmp524
  tmp1680 = tmp1/tmp599
  tmp1681 = tmp56 + tmp810
  tmp1682 = tmp1681*tmp24
  tmp1683 = tmp1680 + tmp1682 + tmp212
  tmp1684 = tmp140*tmp1683
  tmp1685 = tmp1073 + tmp190 + tmp563
  tmp1686 = -16*tmp1685*tmp6
  tmp1687 = -8*tmp74
  tmp1688 = tmp1147 + tmp1687 + tmp274 + tmp3
  tmp1689 = tmp1*tmp1688
  tmp1690 = tmp1132 + tmp1176 + tmp1671 + tmp562
  tmp1691 = (4*tmp1690)/tmp21
  tmp1692 = tmp1670 + tmp1686 + tmp1689 + tmp1691
  tmp1693 = tmp1692*tmp353
  tmp1694 = -128*tmp41*tmp427
  tmp1695 = tmp190*tmp427
  tmp1696 = tmp1176 + tmp1232 + tmp1672 + tmp1695
  tmp1697 = tmp1696*tmp7
  tmp1698 = tmp1176 + tmp227 + tmp524
  tmp1699 = tmp1698*tmp17
  tmp1700 = tmp1429 + tmp1673 + tmp562
  tmp1701 = tmp1700/tmp22
  tmp1702 = tmp1699 + tmp1701
  tmp1703 = tmp1*tmp1702
  tmp1704 = tmp253/tmp22
  tmp1705 = tmp1674 + tmp560 + tmp570
  tmp1706 = tmp1705/tmp22
  tmp1707 = 52*tmp15
  tmp1708 = tmp1707 + tmp304 + tmp448
  tmp1709 = ss*tmp1708
  tmp1710 = tmp1704 + tmp1706 + tmp1709
  tmp1711 = tmp1710*tmp24
  tmp1712 = tmp1694 + tmp1697 + tmp1703 + tmp1711
  tmp1713 = tmp1712*tmp4
  tmp1714 = -224*tmp1*tmp42
  tmp1715 = tmp1661 + tmp1675 + tmp237 + tmp450
  tmp1716 = tmp1715*tmp598
  tmp1717 = tmp161*tmp581
  tmp1718 = tmp1175 + tmp570
  tmp1719 = tmp16*tmp1718
  tmp1720 = tmp15 + tmp201 + tmp561
  tmp1721 = -(tmp1720*tmp74)
  tmp1722 = tmp1717 + tmp1719 + tmp1721
  tmp1723 = tmp1*tmp1722
  tmp1724 = tmp1*tmp1146
  tmp1725 = tmp1*tmp1497
  tmp1726 = 16*tmp132
  tmp1727 = tmp1*tmp1093
  tmp1728 = tmp1661 + tmp1727 + tmp237
  tmp1729 = tmp1728*tmp56
  tmp1730 = tmp1676 + tmp1724 + tmp1725 + tmp1726 + tmp1729 + tmp332
  tmp1731 = tmp1478*tmp1730
  tmp1732 = tmp1428 + tmp1661 + tmp237
  tmp1733 = tmp1732*tmp58
  tmp1734 = -tmp498
  tmp1735 = tmp1236 + tmp1677 + tmp1734 + tmp512
  tmp1736 = tmp1735/tmp22
  tmp1737 = 5*tmp498
  tmp1738 = tmp1236 + tmp1676 + tmp1677 + tmp1737
  tmp1739 = tmp17*tmp1738
  tmp1740 = tmp1733 + tmp1736 + tmp1739
  tmp1741 = tmp1740/tmp21
  tmp1742 = tmp1714 + tmp1716 + tmp1723 + tmp1731 + tmp1741
  tmp1743 = tmp1742/tmp526
  tmp1744 = tmp1678 + tmp1679 + tmp1684 + tmp1693 + tmp1713 + tmp1743
  tmp1745 = 32*tmp1
  tmp1746 = 14/tmp22
  tmp1747 = tmp1659 + tmp1746
  tmp1748 = 34*tmp524
  tmp1749 = tmp1748 + tmp182 + tmp445
  tmp1750 = 1024*tmp135
  tmp1751 = tmp1527 + tmp1748 + tmp487
  tmp1752 = 512*tmp1056
  tmp1753 = 9*tmp1
  tmp1754 = tmp1613 + tmp1753 + tmp228
  tmp1755 = -32*tmp135*tmp1754
  tmp1756 = tmp16 + tmp536 + tmp97
  tmp1757 = (tmp1756*tmp452)/tmp22
  tmp1758 = tmp190*tmp887
  tmp1759 = 39/tmp22
  tmp1760 = tmp1745 + tmp1759
  tmp1761 = tmp1760/tmp22
  tmp1762 = tmp1758 + tmp1761 + tmp253
  tmp1763 = 8*tmp1762*tmp42
  tmp1764 = tmp1*tmp132
  tmp1765 = tmp1176 + tmp204 + tmp95
  tmp1766 = tmp1765*tmp335
  tmp1767 = tmp1751*tmp982
  tmp1768 = tmp1764 + tmp1766 + tmp1767
  tmp1769 = (tmp1*tmp1768)/tmp21
  tmp1770 = 3*tmp512
  tmp1771 = -10*tmp321
  tmp1772 = tmp15*tmp694
  tmp1773 = 24*tmp132
  tmp1774 = tmp1747*tmp9
  tmp1775 = 36*tmp524
  tmp1776 = 23*tmp15
  tmp1777 = tmp1775 + tmp1776 + tmp481
  tmp1778 = tmp1777*tmp56
  tmp1779 = tmp1770 + tmp1771 + tmp1772 + tmp1773 + tmp1774 + tmp1778
  tmp1780 = -2*tmp1779*tmp41
  tmp1781 = tmp321*tmp902
  tmp1782 = -16*tmp1747*tmp41
  tmp1783 = tmp1749*tmp352
  tmp1784 = 7*tmp512
  tmp1785 = -34*tmp321
  tmp1786 = tmp15*tmp878
  tmp1787 = tmp1784 + tmp1785 + tmp1786
  tmp1788 = tmp1787/tmp21
  tmp1789 = tmp1108 + tmp1781 + tmp1782 + tmp1783 + tmp1788
  tmp1790 = tmp1789*tmp98
  tmp1791 = tmp1749*tmp58
  tmp1792 = tmp1*tmp1668
  tmp1793 = tmp1005 + tmp1792 + tmp487
  tmp1794 = tmp1793*tmp524
  tmp1795 = -tmp512
  tmp1796 = tmp1431/tmp22
  tmp1797 = tmp1*tmp859
  tmp1798 = 3*tmp132
  tmp1799 = tmp1795 + tmp1796 + tmp1797 + tmp1798
  tmp1800 = tmp1799*tmp56
  tmp1801 = tmp1791 + tmp1794 + tmp1800
  tmp1802 = tmp1801*tmp6
  tmp1803 = tmp17 + tmp791
  tmp1804 = -256*tmp1803*tmp42
  tmp1805 = tmp1004*tmp17
  tmp1806 = tmp15 + tmp1805
  tmp1807 = tmp1806*tmp321
  tmp1808 = 44*ss*tmp1
  tmp1809 = tmp793/tmp22
  tmp1810 = tmp264/tmp22
  tmp1811 = 29*tmp15
  tmp1812 = tmp1808 + tmp1809 + tmp1810 + tmp1811 + tmp481
  tmp1813 = tmp1812*tmp284
  tmp1814 = tmp1*tmp1129
  tmp1815 = tmp1673 + tmp1814 + tmp227
  tmp1816 = tmp1815/tmp22
  tmp1817 = tmp1749*tmp28
  tmp1818 = tmp1816 + tmp1817
  tmp1819 = -8*tmp1818*tmp6
  tmp1820 = tmp227 + tmp486 + tmp560
  tmp1821 = tmp1820/tmp22
  tmp1822 = tmp1751*tmp28
  tmp1823 = tmp1821 + tmp1822
  tmp1824 = (tmp1823*tmp271)/tmp21
  tmp1825 = tmp1750 + tmp1804 + tmp1807 + tmp1813 + tmp1819 + tmp1824
  tmp1826 = tmp1825/tmp526
  tmp1827 = tmp1752 + tmp1755 + tmp1757 + tmp1763 + tmp1769 + tmp1780 + tmp1790 + &
                          &tmp1802 + tmp1826
  tmp1828 = tmp36**3
  tmp1829 = tmp352*tmp427
  tmp1830 = -((tmp427*tmp431)/tmp21)
  tmp1831 = tmp138 + 1/tmp22
  tmp1832 = tmp1057*tmp15
  tmp1833 = tmp1230 + tmp200
  tmp1834 = tmp310 + tmp486
  tmp1835 = -30/tmp22
  tmp1836 = 8 + tmp976
  tmp1837 = tmp563 + tmp57
  tmp1838 = 30 + kappa
  tmp1839 = tmp1*tmp1838
  tmp1840 = 8*tmp1492
  tmp1841 = 15*tmp1515
  tmp1842 = 14 + kappa
  tmp1843 = tmp1*tmp1334
  tmp1844 = 20*tmp1492
  tmp1845 = tmp1*tmp1842
  tmp1846 = 4 + tmp1845
  tmp1847 = tmp139*tmp1846
  tmp1848 = 2 + tmp1843
  tmp1849 = tmp1848*tmp95
  tmp1850 = tmp21**(-8)
  tmp1851 = 36*ss
  tmp1852 = tmp162*tmp1836
  tmp1853 = 768*tmp43
  tmp1854 = 30/tmp22
  tmp1855 = tmp1*tmp1066
  tmp1856 = 29*kappa
  tmp1857 = 6 + tmp1003
  tmp1858 = -27*tmp1
  tmp1859 = 2*tmp1490
  tmp1860 = 13*tmp3
  tmp1861 = -9 + tmp271
  tmp1862 = kappa*tmp1784
  tmp1863 = -6 + kappa
  tmp1864 = -17/tmp22
  tmp1865 = kappa*tmp1770
  tmp1866 = 18 + tmp130
  tmp1867 = -7 + tmp1003
  tmp1868 = tmp1*tmp1867
  tmp1869 = -52 + tmp130
  tmp1870 = 12*tmp1492
  tmp1871 = -12*tmp3
  tmp1872 = tmp1*tmp1857
  tmp1873 = tmp1*tmp1665
  tmp1874 = 1920*tmp1084
  tmp1875 = 8*tmp512
  tmp1876 = tmp1*tmp1079
  tmp1877 = 25*kappa
  tmp1878 = 8*tmp1214*tmp132
  tmp1879 = tmp21**(-9)
  tmp1880 = 160*tmp8
  tmp1881 = 21 + tmp271
  tmp1882 = 15*kappa
  tmp1883 = 3 + kappa
  tmp1884 = 4*tmp2*tmp512
  tmp1885 = -10*tmp3
  tmp1886 = 10 + kappa
  tmp1887 = -4 + tmp127
  tmp1888 = -18 + tmp1003
  tmp1889 = tmp1*tmp1888
  tmp1890 = tmp1058*tmp561
  tmp1891 = tmp1*tmp1075
  tmp1892 = tmp1*tmp1413
  tmp1893 = 12*tmp1214*tmp132
  MP2MPL_MP_FIN_d11d22 = 2*D0tsLN*Pi*tmp1423*tmp2*tmp21*tmp22*tmp32*tmp512 + 2*D0tsNL*Pi*tmp1423*tmp2*tmp&
                          &21*tmp22*tmp32*tmp512 + C0IR6sFP*f2p*Pi*tmp2*tmp21*tmp22*tmp433*(tmp307 - (tmp27&
                          & + tmp30 - (tmp17 + tmp31 + tmp34)/tmp21)*tmp36 + tmp106*tmp4 + (tmp27 + (tmp12 &
                          &+ tmp17 + tmp31)/tmp21 + tmp345)/tmp526) - (logmut*Pi*tmp2*tmp21*tmp22*(f1p*tmp2&
                          &*tmp20*tmp3 + tmp2*tmp20*tmp561 - 4*f2p*tmp105*tmp524*tmp582))/2. + logP2t*Pi*tm&
                          &p1*tmp160*tmp2*tmp21*tmp22*(f1p*tmp1*tmp2*tmp269 + tmp2*tmp269*tmp46 + (f2p*tmp1&
                          &05*tmp34*(tmp235 + tmp236 - (4*tmp29)/tmp526 - 4*tmp6))/tmp533) + logF2t*Pi*tmp1&
                          &*tmp2*tmp207*tmp21*tmp22*(f1p*tmp1*tmp2*tmp234 + tmp2*tmp234*tmp46 + (f2p*tmp100&
                          &*tmp105*(tmp208 + tmp215)*tmp437)/tmp61) + D0IR6tsL*Pi*tmp2*tmp21*tmp22*tmp433*t&
                          &mp48*((4*f1p*tmp1489*tmp1680*tmp2)/tmp21 + (tmp1489*tmp2*tmp433*tmp620)/tmp21 + &
                          &f2p*((tmp1493 + tmp27*((-28 + kappa)*tmp1096 + tmp1491 + tmp1516 + (-2 + tmp127)&
                          &*tmp3 + 28*tmp524) - ((tmp16 + tmp3)*tmp31 + tmp1226*tmp429 + tmp17*(tmp1224 + t&
                          &mp129*tmp1795 + tmp428 + (4 + tmp1490)*tmp524))/tmp21 + (tmp108*tmp1083*tmp452)/&
                          &tmp599)/tmp526 + tmp4*(tmp1226*tmp163 + tmp26*(tmp1224 + tmp1226*tmp138 - tmp129&
                          &3/tmp22 + tmp428 + tmp486 + tmp1000*tmp512) + tmp31*tmp604) - tmp36*(tmp1493 + t&
                          &mp1478*(tmp1215 + tmp1491 + tmp1832 + tmp481 + 6*tmp512 + (8 + tmp1490)*tmp524) &
                          &+ (tmp17*(tmp1215 + tmp1492 + tmp3 + tmp512 + (2 + tmp1*tmp1518)*tmp524) + tmp31&
                          &*(tmp446 + tmp544) + tmp1226*tmp58)/tmp21 + tmp31*tmp609) + tmp307*((4*tmp1226)/&
                          &tmp21 + tmp31*tmp620))) + DB0sFP*logmuF2*Pi*tmp1526*tmp2*tmp21*tmp22*tmp53*tmp53&
                          &3*tmp599*tmp630 + DB0sFP*logF2P2*Pi*ss*tmp2*tmp21*tmp22*tmp271*tmp53*tmp533*tmp5&
                          &99*tmp630 + C0tF2F20F0*Pi*tmp2*tmp207*tmp21*tmp271*(f1p*tmp1*tmp158*tmp2 + tmp15&
                          &8*tmp2*tmp46 - (2*f2p*tmp105*(tmp137/tmp22 + tmp350 + (tmp117 + tmp1174 + tmp15 &
                          &+ tmp164 + tmp199)/tmp526 + tmp4*(tmp109 + tmp56 + tmp67)))/tmp61) + DB0sFPlogmu&
                          &t*Pi*tmp2*tmp21*tmp22*(f1p*tmp1*tmp100*tmp104*tmp2 + tmp100*tmp104*tmp2*tmp46 - &
                          &2*f2p*tmp105*(tmp353 + tmp137*tmp36 - tmp106*tmp4 + (tmp103 + tmp1478 + tmp108*t&
                          &mp28)/tmp526))*tmp53*tmp701 + DB0tLL*Pi*tmp160*tmp2*tmp207*tmp22*tmp32*tmp445*((&
                          &-8*f2p*tmp159*tmp422*tmp440)/(tmp22*tmp61) + (tmp2*tmp839)/tmp21 - (f1p*tmp2*tmp&
                          &839)/tmp21) + logmuP2*Pi*tmp1*tmp2*tmp21*tmp22*tmp32*((f1p*tmp2*tmp23*tmp524)/tm&
                          &p526 + (tmp2*tmp23*tmp570)/tmp526 - f2p*tmp33*(tmp211 + tmp235 + tmp236 - (2*(tm&
                          &p17 + tmp34 + tmp514))/tmp526 + tmp546*(tmp38 + tmp841))) + logmuL2*Pi*tmp2*tmp2&
                          &2*tmp32*tmp433*(f1p*tmp2*tmp271*tmp298 + tmp2*tmp298*tmp305 + f2p*(tmp185 + tmp2&
                          &99 + tmp1*(tmp301 + tmp15*(1 + tmp302) + (tmp209*tmp306)/tmp22) - 8*(tmp1 + tmp3&
                          &00/tmp22 + tmp56)*tmp6 - (2*(tmp212 + tmp46*(tmp306/tmp22 + tmp56) + (4*(tmp200 &
                          &+ tmp56 + tmp842))/tmp21))/tmp526 + tmp26*(tmp1636*tmp17 + tmp274 + (tmp1081 + t&
                          &mp113 + tmp303 + tmp304)/tmp22 + tmp9))) - (logmuN2*Pi*tmp2*tmp21*tmp22*tmp3*tmp&
                          &32*(f1p*tmp1*tmp2*(tmp1670 + (4*tmp276)/tmp21 + tmp1*(tmp15 + tmp348) + tmp272*t&
                          &mp5 + (2*tmp281)/tmp526 - 16*tmp273*tmp6) + tmp1*tmp2*(tmp24*tmp276 - 16*tmp272*&
                          &tmp4 - 128*tmp41 - (2*tmp281)/tmp526 + tmp273*tmp7 + tmp1*(tmp16 + tmp9)) + 2*f2&
                          &p*(tmp185 + tmp299 + tmp1*(tmp1492 + tmp1636*tmp209 + tmp301) - (2*(tmp212 + tmp&
                          &46*(tmp1636 + tmp56) + (4*((5 + tmp283)/tmp22 + tmp305 + tmp56))/tmp21))/tmp526 &
                          &- 8*(tmp1 + (13 + tmp283)/tmp22 + tmp56)*tmp6 + tmp26*((-2 + tmp1074)*tmp15 + tm&
                          &p274 + tmp300*tmp524 + (20 + tmp1059*tmp129)*tmp74 + tmp9))))/2. + (DB0F2FL*f2p*&
                          &Pi*tmp1745*tmp2*tmp22*tmp48*tmp53*tmp61*tmp93)/tmp526 + (DB0F2FN*f2p*Pi*tmp2*tmp&
                          &21*tmp22*tmp448*tmp48*tmp53*tmp61*tmp93)/tmp526 - 4*DB0tLN*Pi*tmp160*tmp2*tmp207&
                          &*tmp21*tmp22*tmp32*tmp512*((f2p*tmp159*tmp422*tmp437*tmp528)/tmp61 - (tmp2*tmp94&
                          &1)/tmp21 + (f1p*tmp2*tmp941)/tmp21) + (logN2L2*Pi*tmp2*tmp32*tmp512*(-(tmp21*tmp&
                          &22*tmp533*tmp534*tmp551) + tmp100*tmp513*tmp533*tmp551*tmp552*tmp554 + tmp100*tm&
                          &p513*tmp533*tmp552*tmp554*(tmp548 + f1p*tmp2*tmp26*tmp558 + tmp2*tmp546*tmp558) &
                          &- 4*tmp2*tmp513*tmp515*tmp517*tmp565 - tmp21*tmp513*tmp567*((f2p*tmp528*tmp582)/&
                          &tmp22 + f1p*tmp2*tmp26*tmp597 + tmp2*tmp546*tmp597) + 2*tmp160*tmp2*tmp515*(160*&
                          &tmp41 - (tmp15 + tmp200 + tmp560)/tmp22 + tmp343*(tmp200 + tmp237 + tmp561) - 16&
                          &*(tmp1 + tmp443)*tmp6) + 2*tmp21*tmp22*tmp532*tmp534*tmp61 + 4*tmp100*tmp21*tmp5&
                          &13*tmp526*tmp532*tmp569*tmp61 + 4*tmp100*tmp21*tmp513*tmp526*tmp569*(tmp529 + (4&
                          &*f1p*tmp2*tmp573)/tmp21 + tmp2*tmp24*tmp573)*tmp61 - 8*tmp100*tmp2*tmp22*tmp515*&
                          &tmp579*(tmp460 + 8*tmp4*tmp565 + (tmp15 + tmp560 + tmp562 - (8*(1/tmp22 + tmp563&
                          &))/tmp21 + tmp7)/(tmp22*tmp526)) + 8*tmp100*tmp2*tmp22*tmp515*tmp579*(((-8*tmp18&
                          &37)/tmp21 + tmp581 - 48*tmp6)/(tmp22*tmp526) + tmp15*tmp7 + 8*tmp4*(tmp1059/tmp2&
                          &1 + tmp581 + tmp7)) - 4*tmp2*tmp513*tmp515*tmp517*(tmp15 + tmp200 + tmp561 - 16*&
                          &tmp6 + tmp840/tmp21) + tmp21*tmp513*tmp567*(-((f2p*tmp528*tmp582)/tmp22) + tmp2*&
                          &tmp546*(tmp16 + tmp163 + tmp348 - 16*tmp4 + tmp524 + (-8/tmp21 + tmp228 + tmp34 &
                          &+ tmp453)/tmp526 + (tmp1613 + tmp453 + tmp56)/tmp21 + tmp584 + tmp585) + f1p*tmp&
                          &2*tmp546*(tmp117 + tmp15 + ss*tmp453 + tmp5 + tmp570 + (tmp55 + tmp555 + tmp583)&
                          &/tmp21 - 8*tmp6 + (-32*ss + tmp113 + tmp583 + tmp67)/tmp526 + tmp9)) - (4*tmp100&
                          &*tmp2*tmp22*tmp515*(tmp359 + tmp460 + tmp163*(tmp3 + tmp559) - 16*tmp41*(tmp433 &
                          &+ tmp57) + (tmp15 + tmp560 + tmp562)*tmp949))/(-4*(1/tmp21)**1.5 + Sqrt(1/tmp21)&
                          &/tmp22)**2))/2. + C0tP2P2NPN*Pi*tmp160*tmp21*tmp22*tmp32*tmp341*tmp342*(-512*tmp&
                          &135 + (tmp1141*tmp132)/tmp21 + (7*tmp349)/tmp21 + tmp1236*tmp4 + (tmp1227*tmp4)/&
                          &tmp21 + (tmp176*tmp4)/tmp22 + 270*tmp15*tmp41 + 512*ss*tmp42 - (96*tmp42)/tmp22 &
                          &- (12*tmp132)/(tmp21*tmp526) + (68*ss*tmp15)/(tmp21*tmp526) - (512*tmp42)/tmp526&
                          & + (tmp132*tmp55)/tmp526 - 80*tmp132*tmp6 + (68*tmp15*tmp6)/tmp526 + (f2p*tmp109&
                          &*(-tmp351 + (tmp10 + tmp113)*tmp4 + 116*tmp41 + (tmp163 + tmp18/tmp22 + tmp355)/&
                          &tmp526 + (tmp134 + tmp357 + tmp584)/tmp21 - 24*(ss + tmp34)*tmp6))/tmp533 + ss*t&
                          &mp6*tmp647 - (192*tmp6*tmp74)/tmp526 + tmp1236*tmp8 + (tmp1227*tmp8)/tmp21 + (tm&
                          &p176*tmp8)/tmp22 + f1p*(-270*tmp15*tmp41 + (-512*ss + 96/tmp22)*tmp42 + tmp425 +&
                          & (4*(tmp132*tmp28 + tmp359 + (tmp15*(-17*ss + tmp57))/tmp21 + ((tmp1864 + tmp356&
                          &)*tmp6)/tmp22))/tmp526 - 4*tmp132*tmp8 + (tmp15*(tmp118 + tmp148/tmp22 + 34*tmp8&
                          &))/tmp21 + tmp6*(80*tmp132 + 76*ss*tmp15 - (96*tmp8)/tmp22) + tmp113*tmp4*(tmp18&
                          &64/tmp21 + tmp942 + tmp95))) - 16*C0tF2F2LFL*Pi*tmp2*tmp207*tmp22*tmp3*tmp32*(f1&
                          &p*tmp2*tmp546*(-(tmp1200/tmp22) + tmp1180*tmp307 - 2*tmp1189*tmp4 + (2*tmp1213)/&
                          &tmp526 + tmp140*tmp735) + tmp2*tmp546*(tmp1200/tmp22 + tmp1180*tmp353 - (2*tmp12&
                          &13)/tmp526 - 16*tmp54*tmp735 + tmp1189*tmp98) + (f2p*tmp440*(tmp1172 - (tmp1173 &
                          &+ tmp1467)/tmp21 + tmp198*tmp27 + tmp350 + (tmp117 + tmp1174 + tmp15 + tmp199 + &
                          &(tmp433 + tmp443 + tmp444)/tmp21 + tmp570 + tmp701)/tmp526 + (tmp10 + tmp1470 + &
                          &tmp17)*tmp98))/(tmp22*tmp61)) - 16*C0tP2P2LPL*Pi*tmp160*tmp2*tmp22*tmp3*tmp32*(-&
                          &((tmp2*tmp511)/tmp21) + (f1p*tmp2*tmp511)/tmp21 + (f2p*tmp440*(tmp1172 + (tmp127&
                          &4 - 4*tmp15 + tmp1526 + tmp199 + tmp1753/tmp22)/tmp21 + tmp441 + (tmp163 - tmp18&
                          &*tmp198 + (tmp317 + tmp443 + tmp444)/tmp21)/tmp526 + tmp27*(tmp17 + tmp443 + tmp&
                          &632) + (tmp198 + tmp442)*tmp98))/(tmp22*tmp533)) + C0tP2P20P0*Pi*tmp160*tmp2*tmp&
                          &21*tmp46*(f1p*tmp1*tmp195*tmp2 + tmp195*tmp2*tmp46 + (4*f2p*tmp105*(tmp351 + tmp&
                          &344*tmp352 + tmp441 + (tmp163 + tmp164 - tmp18/tmp22)/tmp526 + tmp546*(tmp161 + &
                          &tmp162 + tmp95) + (1/tmp22 + tmp442)*tmp98))/tmp533) + C0tF2F2LF0*Pi*tmp2*tmp207&
                          &*tmp21*tmp22*tmp317*tmp48*((f1p*tmp2*tmp433*(-(tmp132*tmp1484) + tmp140*tmp198 +&
                          & tmp1499*tmp307 - 2*tmp1504*tmp4 + tmp1511/tmp526))/tmp21 + (tmp2*tmp433*(tmp132&
                          &*tmp1484 + tmp1499*tmp353 - tmp1511/tmp526 - 16*tmp198*tmp54 + tmp1504*tmp98))/t&
                          &mp21 - (f2p*(tmp350*((4*tmp1512)/tmp21 + kappa*tmp198*tmp561) + (kappa*tmp1096*t&
                          &mp30 + tmp1478*((tmp1519 + tmp1057/tmp22 + tmp433)/tmp22 + tmp1517*tmp56) + tmp1&
                          &517*tmp598 + (ss*(tmp1226 + tmp1518*tmp3)*tmp437 + tmp1517*tmp58 + kappa*tmp321*&
                          &tmp637)/tmp21)/tmp22 - (tmp1218*tmp284 + tmp1522*tmp31 + tmp26*(tmp1523 + (tmp12&
                          &15 + tmp1222 + tmp1516)/tmp22 + tmp17*(tmp1215 + tmp1524 + (3 + tmp1220)*tmp524)&
                          &) - 4*tmp6*(tmp1215 + 6*tmp1492 + tmp1218*tmp56 + tmp524*(6 + tmp1*(4 + tmp976))&
                          &))/tmp526 + (tmp1514*tmp31 + (4*(tmp1515 + tmp1516 + tmp1226*tmp17 + (tmp271 + t&
                          &mp3 + tmp45)/tmp22))/tmp21 + tmp1226*tmp7)*tmp98))/tmp61) + 4*logbeL2*Pi*tmp160*&
                          &tmp1680*tmp2*tmp207*tmp21*tmp48*tmp513*((f2p*tmp159*tmp422*tmp437*(tmp1057/tmp21&
                          & + tmp45))/tmp61 + (tmp1*tmp2*(-32*tmp54*tmp642 - 16*tmp37*tmp653 + tmp699/tmp22&
                          & - (4*tmp730)/tmp526 + (-128*tmp41*tmp654 - (8*tmp664)/tmp21 + tmp669*tmp7 + tmp&
                          &843 + tmp844 + tmp846)*tmp98))/tmp21 + (f1p*tmp1*tmp2*(32*tmp54*tmp642 + 16*tmp3&
                          &7*tmp653 - tmp699/tmp22 + (4*tmp730)/tmp526 + (tmp1670*tmp654 - 16*tmp6*tmp669 +&
                          & tmp664*tmp67 - (tmp15*tmp656 + tmp733 + tmp622*tmp9)/tmp22)*tmp98))/tmp21) + (D&
                          &B0tNN*Pi*tmp160*tmp207*tmp21*tmp22*tmp32*tmp341*tmp342*((tmp132*tmp213)/tmp21 - &
                          &3/tmp22**6 - (512*tmp135)/tmp22 + (68*ss*tmp349)/tmp21 + 112*tmp132*tmp37 + 192*&
                          &ss*tmp15*tmp37 - (1344*tmp15*tmp37)/tmp21 - 320*ss*tmp132*tmp4 + (640*tmp132*tmp&
                          &4)/tmp21 + (3008*ss*tmp15*tmp4)/tmp21 - 74*tmp349*tmp4 - 608*tmp132*tmp41 - 1152&
                          &*ss*tmp15*tmp41 - 8192*tmp37*tmp41 + 12288*ss*tmp4*tmp41 + (5120*tmp4*tmp41)/tmp&
                          &22 + tmp148*tmp413 + 576*tmp15*tmp42 - 10240*tmp4*tmp42 + (tmp1146*tmp132)/tmp52&
                          &6 - (4096*tmp135)/tmp526 - (1440*ss*tmp132)/(tmp21*tmp526) + (148*ss*tmp349)/tmp&
                          &526 - (156*tmp349)/(tmp21*tmp526) - (1408*tmp15*tmp41)/tmp526 + (28*tmp413)/tmp5&
                          &26 + (8192*ss*tmp42)/tmp526 + (7168*tmp42)/(tmp22*tmp526) - 96*tmp15*tmp54 + tmp&
                          &1082*tmp132*tmp6 + tmp15*tmp253*tmp6 + 166*tmp349*tmp6 + 4096*ss*tmp37*tmp6 + (7&
                          &168*tmp37*tmp6)/tmp22 - 2592*tmp15*tmp4*tmp6 + (240*tmp132*tmp6)/tmp526 + (5760*&
                          &ss*tmp15*tmp6)/tmp526 - 2048*tmp54*tmp6 + (32*f2p*tmp159*tmp422)/(tmp22*tmp61) +&
                          & 1024*tmp42*tmp74 - (9216*tmp41*tmp74)/tmp526 - 12288*tmp4*tmp6*tmp74 - 10*tmp34&
                          &9*tmp8 - 96*tmp15*tmp4*tmp8 - (512*tmp41*tmp8)/tmp22 - (1024*tmp15*tmp8)/(tmp21*&
                          &tmp526) - (4096*tmp41*tmp8)/tmp526 - 2048*tmp4*tmp6*tmp8 + (4096*tmp6*tmp8)/(tmp&
                          &22*tmp526) - 1024*ss*tmp37*tmp949 + 512*tmp54*tmp949 + 512*tmp4*tmp8*tmp949 + f1&
                          &p*((4*(tmp1750 - tmp132*tmp400 + tmp185*(11*tmp15 + tmp213 + tmp402) - 256*tmp39&
                          &7*tmp42 + (tmp15*(tmp255 + tmp423 + tmp424))/tmp21 + tmp109*(tmp218 + tmp423 + t&
                          &mp424)*tmp6))/tmp526 + ((tmp1478*(83*tmp15 + tmp213 + tmp217))/tmp22 + tmp132*tm&
                          &p389 - 64*(tmp190 + tmp261)*tmp42 + tmp425 + ss*tmp15*tmp24*tmp426 + tmp185*(tmp&
                          &1672 + tmp1851/tmp22 + tmp9))/tmp22 + 32*tmp54*(tmp237 + tmp249 - 16*tmp949) + 1&
                          &6*tmp37*(tmp16*tmp363 + 512*tmp41 - 64*(tmp17 + tmp270)*tmp6 + 4*(tmp1093 + tmp1&
                          &90)*tmp949) + (tmp15*tmp380 - 512*tmp154*tmp41 + 5120*tmp42 + tmp7*(81*tmp15 + t&
                          &mp253 + 384*tmp74) - 32*(tmp1674 + tmp58 + ss*tmp658)*tmp949)*tmp98)))/2. + logb&
                          &eN2*Pi*tmp2*tmp207*tmp21*tmp48*tmp513*tmp561*(f1p*tmp1*tmp2*tmp412 + tmp2*tmp412&
                          &*tmp46 - (16*f2p*tmp422*tmp980)/tmp61) + logmuF2*Pi*tmp2*tmp21*tmp22*tmp271*tmp3&
                          &2*(f1p*tmp1*tmp2*tmp23*tmp949 - f2p*tmp33*((-3*tmp210)/tmp21 + tmp30 - (tmp17 + &
                          &1/tmp22 + tmp346)/tmp526 + tmp6 + tmp98) + tmp1*tmp2*tmp23*tmp983) + 2*D0tsNN*Pi&
                          &*tmp21*tmp22*tmp32*tmp341*tmp342*((tmp15*tmp35)/tmp21 + tmp353/tmp22 - (16*tmp37&
                          &)/tmp21 + (tmp356*tmp4)/tmp21 - 144*ss*tmp41 + (106*tmp41)/tmp22 + 112*tmp42 + t&
                          &mp113*tmp43 + tmp141*tmp43 + tmp15/(tmp21*tmp526) + (208*tmp41)/tmp526 + tmp430/&
                          &(tmp21*tmp526) + ss*tmp4*tmp538 + tmp15*tmp6 + 80*tmp4*tmp6 + (tmp1425*tmp6)/tmp&
                          &526 + (tmp1854*tmp6)/tmp526 + (tmp443*tmp8)/tmp526 + tmp6*tmp9 + f1p*(tmp347*tmp&
                          &353 + (tmp1579 - 53/tmp22)*tmp354 - 2*(tmp107 + tmp346)*tmp347*tmp4 - 112*tmp42 &
                          &+ tmp34*tmp43 - (208*tmp41 + (tmp1425 + tmp1854)*tmp6 + (tmp15 + tmp430 + tmp732&
                          &)/tmp21 + tmp443*tmp8)/tmp526 + (ss*(tmp15 + tmp348 + tmp88))/tmp21 - tmp6*(tmp1&
                          &17 + tmp15 + tmp9)) + tmp74*tmp948 - 10*tmp4*tmp949 + (tmp138*tmp949)/tmp526 + t&
                          &mp949*tmp982 + f2p*tmp34*(tmp307 + 130*tmp41 - tmp416 + (-34*ss + tmp34)*tmp6 + &
                          &tmp4*(tmp343 + tmp603) + tmp29*tmp8 + (tmp345 + 90*tmp6 - tmp986)/tmp526)) + C0t&
                          &P2P2LP0*Pi*tmp160*tmp2*tmp21*tmp22*tmp433*tmp48*((f1p*tmp1270*tmp2*tmp271)/tmp21&
                          & + tmp1*tmp1270*tmp2*tmp546 + (f2p*(tmp1478*(tmp1523 + tmp17*(tmp1215 + tmp1644 &
                          &+ (tmp1 + tmp1293)/tmp22) + (-9*tmp1515 + tmp1840 + (tmp1079*tmp1207)/tmp22 + tm&
                          &p327)/tmp22) + 16*tmp1512*tmp42 - 2*tmp4*(tmp1218*tmp163 + kappa*tmp460 + tmp122&
                          &5*tmp546) + kappa*tmp15*tmp30*tmp561 + tmp441*(tmp1215 + tmp1491 + tmp1870 + (12&
                          & + tmp1839)*tmp570) + (tmp1225*tmp357 + kappa*tmp460*(tmp437 + tmp563) + (tmp122&
                          &4 + kappa*tmp1676 + (2 + tmp1*(2 + tmp1003))*tmp524)*tmp88)/tmp21 + (tmp1226*tmp&
                          &185 + tmp163*(tmp1515 + tmp1524 + tmp1218*tmp17 - (tmp1 + tmp1519)/tmp22) + kapp&
                          &a*tmp18*tmp460 - 2*tmp1225*tmp986)/tmp526))/tmp533) + C0sF2P2FNP*f2p*Pi*tmp2*tmp&
                          &21*tmp22*tmp48*tmp481*tmp53*(tmp197 + (10/tmp21 + 1/tmp22 + tmp39)*tmp54 - tmp37&
                          &*(tmp17*(1/tmp22 + tmp38) + (tmp40 + tmp444 + tmp45)/tmp21 + tmp7) + tmp36*(tmp2&
                          &03 + (-12*ss + tmp1835 + tmp31)*tmp41 - tmp66 + ((tmp109 + tmp17 + tmp31)*tmp8)/&
                          &tmp21 + ss*tmp6*(tmp1606 + tmp17 + tmp845)) - ((-51/tmp22 + tmp31)*tmp41 + 14*tm&
                          &p42 + 2*(tmp34 + tmp38)*tmp43 + ss*tmp1478*(tmp45 + tmp56 + tmp638) + (tmp8*(tmp&
                          &44 + tmp55 + tmp96))/tmp21)/tmp526 + tmp4*(tmp284 + (ss*(tmp138 + tmp239 + tmp44&
                          &))/tmp21 - (tmp31 + tmp40)*tmp6 + (tmp39 + tmp57)*tmp99)) + 16*D0tsLL*Pi*tmp2*tm&
                          &p22*tmp3*tmp32*(tmp2*tmp26*tmp340 + f1p*tmp2*tmp340*tmp546 - (f2p*tmp440*(tmp307&
                          & + (tmp1479 + 1/tmp22)*tmp4 - 2*tmp41 - (tmp161 + ss*tmp271 + tmp308 + tmp448)/t&
                          &mp21 + (tmp1275 + (14*tmp1 + tmp12 + tmp17)/tmp21 + tmp27 + tmp428 - 6*tmp8)/tmp&
                          &526 + tmp6*(tmp427 + tmp94) + ss*(tmp1176 + tmp309 + tmp99)))/tmp22) + (DB0P2LP*&
                          &f2p*Pi*tmp1745*tmp2*tmp22*tmp48*tmp53*tmp533*(tmp62 + tmp69 + tmp4*(tmp162 + (tm&
                          &p434 + tmp56)/tmp21 + tmp70 + tmp77 + tmp95) + (tmp35*(tmp134 + ss*tmp270 + tmp5&
                          &8) + tmp426*tmp6 + tmp83 + (-5*tmp15 + tmp58 + tmp97)/tmp21)/tmp526 + tmp36*(tmp&
                          &354 - tmp6*(tmp40 + tmp94) + (tmp161 + tmp237 + tmp97)/tmp21 + tmp35*(tmp308 + t&
                          &mp95 + tmp99))))/tmp21 + 2*C0tF2F2NFN*Pi*tmp207*tmp21*tmp22*tmp32*tmp341*tmp342*&
                          &((ss*tmp1236)/tmp21 + tmp165/tmp22 + ss*tmp15*tmp249 + tmp349/tmp21 + tmp1674*tm&
                          &p37 + (256*ss*tmp37)/tmp21 + tmp1007*tmp15*tmp4 - (12*tmp15*tmp4)/tmp21 + (tmp37&
                          &7*tmp4)/tmp22 - 32*tmp15*tmp41 + 384*tmp4*tmp41 + tmp1108/tmp526 - (14*tmp132)/(&
                          &tmp21*tmp526) - (100*ss*tmp15)/(tmp21*tmp526) - (512*ss*tmp41)/tmp526 - (256*tmp&
                          &41)/(tmp22*tmp526) - (128*tmp54)/tmp21 - (16*tmp54)/tmp22 + 18*tmp132*tmp6 + (tm&
                          &p213*tmp6)/tmp22 - 256*ss*tmp4*tmp6 + (80*tmp4*tmp6)/tmp22 - (14*tmp15*tmp6)/tmp&
                          &526 + (tmp423*tmp6)/tmp526 - 32*tmp37*tmp74 - 64*tmp41*tmp74 + (192*tmp6*tmp74)/&
                          &tmp526 - (128*tmp4*tmp8)/tmp21 - (14*tmp15*tmp8)/tmp526 + 96*tmp37*tmp949 + tmp2&
                          &28*tmp4*tmp949 - (64*tmp8*tmp949)/tmp526 + (f2p*tmp34*((tmp10 + tmp1529)*tmp235 &
                          &+ tmp350 + tmp351 + (-2*tmp15 + tmp58)/tmp21 + tmp598 + tmp1478*tmp84 + (tmp117 &
                          &+ tmp15 + tmp199 + tmp352 + 2*tmp986)/tmp526))/tmp61 + tmp132*tmp99 + f1p*(tmp14&
                          &0*(1/tmp22 + tmp67) + tmp307*(tmp141*tmp86 + tmp944) - 4*tmp4*(96*tmp41 + (-64*s&
                          &s + tmp1073)*tmp6 + (tmp1426 + tmp1497 + tmp635)/tmp21 + tmp945) + (2*(tmp1670*t&
                          &mp29 - 128*tmp42 + tmp6*(tmp182 + tmp731 - 128*tmp8) + tmp182*tmp8 + (tmp182 + t&
                          &mp213 + 50*tmp74)*tmp949))/tmp526 - (tmp165 - 32*tmp29*tmp41 + tmp27*(tmp1429 + &
                          &tmp228/tmp22 + tmp9) + tmp15*tmp986 + tmp15*tmp99)/tmp22)) + f2p*logF2P2*Pi*tmp1&
                          &00*tmp1055*tmp2*tmp22*tmp271*tmp32*tmp526*(tmp196*tmp33 + (tmp1*tmp49*(tmp1829 +&
                          & tmp1830 + tmp436 + tmp452 + tmp28*tmp524))/tmp21 - (tmp54*(tmp18*tmp46 + tmp26*&
                          &(tmp56 + tmp639) + tmp7))/tmp48 + tmp37*(tmp1427 + tmp3*(tmp161 + tmp308 + tmp43&
                          &2) + (tmp1*(ss*tmp1602 + tmp1*tmp1632 + tmp275 + tmp3 + tmp430 + tmp559))/tmp21 &
                          &+ (tmp1060 + tmp56)*tmp598 + tmp352*(tmp428 + tmp429 + tmp586 + ss*tmp761)) - tm&
                          &p4*(tmp1525 - 2*(tmp1428 + tmp1675 + tmp213 + tmp3)*tmp41 + (tmp15*tmp1891 + tmp&
                          &308 + tmp357)*tmp452 - 8*tmp42*(tmp435 + tmp56) + tmp6*((tmp1 + tmp109)*tmp1526 &
                          &+ 64*tmp43 + tmp3*(tmp261 + tmp285 + tmp438) - 24*tmp1*tmp8) + (tmp1*(-32*tmp43 &
                          &+ tmp436 + ss*tmp1*(tmp435 + tmp437 + tmp438) + (tmp443 + tmp563)*tmp982))/tmp21&
                          &) + (16*tmp1056 - 16*tmp135*tmp431 + tmp42*(tmp1671 + tmp171 - 9*tmp3 - 28*tmp52&
                          &4) + tmp3*tmp8*(tmp432 + tmp74 + tmp8) + tmp41*((tmp1632 + tmp270 + tmp285)*tmp3&
                          & - 64*tmp43 + ss*(tmp449 + tmp450) + tmp1059*tmp8) + tmp6*(tmp1646 + kappa*tmp15&
                          &*tmp1669 + 56*tmp1*tmp43 + (tmp1666 + tmp433 + tmp434)*tmp452 + tmp1630*tmp735*t&
                          &mp8) + (tmp518*(tmp1666*tmp3 + 8*tmp43 + ss*tmp1*(-8*kappa*tmp15 + tmp435 + tmp5&
                          &7) + (tmp202 + tmp34)*tmp99))/tmp21)/tmp526) - (logP2L2*Pi*tmp1*tmp1055*tmp160*t&
                          &mp2*tmp22*tmp32*tmp526*tmp552*((tmp1094*tmp1827*tmp2*tmp317)/(tmp21*tmp526) + (f&
                          &1p*tmp1094*tmp1827*tmp2*tmp433)/(tmp21*tmp526) + (f2p*((tmp1828*tmp317*tmp41*(tm&
                          &p1829 + tmp1830 + ss*(tmp200 + tmp3) + tmp436))/tmp533 + 4*tmp1625*(tmp1833*tmp1&
                          &85 - (kappa*tmp342)/tmp22 + tmp359 + tmp1059*tmp1834*tmp6 + tmp1875*tmp949) - 2*&
                          &tmp1576*(1536*tmp135 - (kappa*tmp1831*tmp342)/tmp22 + tmp359*(tmp1060 + tmp138 +&
                          & (3 + tmp433)/tmp22) + tmp284*(tmp1832 + tmp312 + tmp1833*tmp444 + (-10 + (-18 +&
                          & kappa)*tmp1)*tmp524) + tmp317*((tmp1845/tmp22 + tmp317 + (-11 + kappa)*tmp428 +&
                          & tmp437)/tmp22 + tmp1834*tmp444)*tmp6 + (96*ss - 11*tmp127 + tmp1836/tmp22)*tmp5&
                          &12*tmp949) - 4*tmp54*(2560*tmp1582 + 64*tmp1056*(-31*tmp1 + tmp1835 + tmp444) + &
                          &4*tmp42*(tmp1616 + tmp1878 + ((5 + tmp1876)*tmp1885)/tmp22 + tmp213*(tmp1060 + (&
                          &11 + tmp433)/tmp22) + (-122 + tmp1*(-46 + tmp1294))*tmp498 + tmp512 + tmp17*(tmp&
                          &1860 + (43 + tmp1*(-20 + tmp1474))*tmp524 + (7 + tmp433)*tmp702)) + (tmp1354*tmp&
                          &18*tmp342*tmp8)/tmp22 + 16*tmp135*(-76*ss*tmp1 + tmp171 + 20*tmp3 + (81 + tmp161&
                          &7)*tmp524 + tmp134*(13 + tmp583) + ss*tmp1861*tmp841) + tmp46*tmp6*(160*tmp1834*&
                          &tmp43 + kappa*tmp15*(1/tmp22 + tmp317)*tmp561 + (tmp1847 + (-18 + tmp1*(-23 + tm&
                          &p1003))*tmp271)*tmp437*tmp8 + (11*tmp1515 + tmp1849 + (32 + 5*tmp1855)*tmp570)*t&
                          &mp88) + tmp41*(tmp1616*tmp1833 + tmp570*(tmp134*(2 + 3*tmp1490) + tmp1841 + tmp3&
                          & + (70 + tmp1*(32 + tmp1882))*tmp570) + ss*tmp437*(tmp1840 - 25*tmp3 + (44 - 12*&
                          &kappa)*tmp512 + (60 + tmp1839)*tmp747) + (tmp1844 + tmp312 + (-58 + tmp1*(-26 + &
                          &tmp1003))*tmp524)*tmp9) + ss*(-23*ss*tmp127 + tmp1852 + tmp1880 + (kappa*tmp1837&
                          &)/tmp22)*tmp512*tmp949) + (tmp36*(512*tmp1879 - 128*tmp1850*(tmp1007 + tmp271 + &
                          &(19 + tmp271)/tmp22) + (tmp1083*tmp1084*tmp29*tmp342)/tmp22 - tmp42*(512*tmp1658&
                          & + (tmp1526*(tmp15*(-6 + tmp1*(-18 + tmp1657)) + tmp449 + (64 + (2 + 53*kappa)*t&
                          &mp1)*tmp524))/tmp22 - 384*tmp1084*(tmp1629 + tmp563) + tmp15*tmp1515*tmp644 - 32&
                          &*tmp43*(-19*tmp3 + (-97 + tmp1859)*tmp524 + tmp1650*tmp702) + tmp301*(tmp1893 - &
                          &2*(76 + tmp1889)*tmp498 + 16*tmp512 + tmp1096*(109 + tmp840))) + tmp35*tmp41*(12&
                          &8*tmp1084*tmp1833 + 32*tmp43*(tmp1832 + tmp1885 + (-15 + tmp1*tmp1886)*tmp524) +&
                          & kappa*tmp15*tmp1795*(tmp1 + tmp658) + (tmp701*(tmp15*(-6 + tmp1889) + tmp1887*t&
                          &mp428 + (-26 + tmp1*(3 + tmp1294))*tmp747))/tmp22 + (tmp1832 + tmp1884 + tmp1677&
                          &*tmp1886 - 63*tmp3 - 40*tmp524)*tmp8*tmp841) + 32*tmp1582*(tmp1673 + tmp1880 + t&
                          &mp138*(tmp1881/tmp22 + tmp433) + (70 + tmp1490)*tmp524 + tmp859) - 8*tmp1056*(-4&
                          &*tmp1214*tmp132 + tmp1616 + (55 + tmp2*tmp271)*tmp321 + (76 + tmp1*(2 + tmp1882)&
                          &)*tmp498 + 4*tmp512 + tmp11*(tmp1673 + (-7 + tmp1)*tmp702 + (94 + tmp1855)*tmp74&
                          &7) + (27*tmp1 + (74 + tmp433)/tmp22)*tmp9) + tmp1795*tmp43*(ss*tmp1*tmp1474 + tm&
                          &p213 + (kappa*tmp671)/tmp22 + tmp1836*tmp88)*tmp949 + 2*tmp135*(1280*tmp1084 - 6&
                          &4*tmp43*(tmp1278 + (-34 + tmp433)/tmp22) + tmp11*(tmp1096*(113 + tmp1891) + tmp1&
                          &893 - 4*(46 + tmp1892)*tmp498 + 12*tmp512) + (tmp3 + (-2 + tmp1)*tmp474 + tmp524&
                          &*(199 + tmp840))*tmp9 + tmp524*(tmp1215 + (56*tmp1 + tmp1890)/tmp22 + tmp481 + (&
                          &-2 + tmp1652)*tmp95)) + tmp1*tmp6*tmp8*(32*tmp1834*tmp43 + ss*tmp437*(tmp1215 + &
                          &tmp15*tmp1848 + (-16 + 5*tmp1490)*tmp524) + (tmp1846/tmp22 + (-6 + tmp1*tmp1883)&
                          &*tmp271)*tmp8*tmp841 + kappa*tmp460*(tmp285 + tmp96))))/tmp526 + tmp353*(3840*tm&
                          &p1850 - 64*tmp1582*(45*tmp1 + tmp139*tmp1881 + tmp356) + (tmp108*tmp1603*tmp342*&
                          &tmp43)/tmp22 - 16*tmp1056*(tmp171 - 26*tmp3 + tmp190*(tmp1060 + tmp40) - 8*tmp15&
                          &*(17 + tmp433) + tmp570*(297 + kappa*tmp736)) + ss*tmp1*tmp1478*(120*tmp1834*tmp&
                          &43 + kappa*tmp460*tmp734 + (6*tmp15*tmp1848 + tmp1862 + 3*(-32 + tmp1065)*tmp524&
                          &)*tmp74 + (tmp1847 + (-22 + tmp1868)*tmp271)*tmp437*tmp8) - 8*tmp135*(-12*tmp121&
                          &4*tmp132 + tmp1599 + tmp1669 + tmp15*(184*tmp1 + (34 + tmp1387)*tmp3) + ((47 + t&
                          &mp1617)*tmp428)/tmp22 + tmp11*(-20*tmp3 + 3*tmp1866*tmp524 + tmp15*(44 + tmp840)&
                          &) + (tmp1060 + (47 + tmp271)/tmp22)*tmp9) + tmp41*(960*tmp1084*tmp1833 + 32*tmp4&
                          &3*(tmp1844 + tmp1871 + (-62 + tmp1872)*tmp524) + tmp15*tmp1515*(tmp261 + tmp712)&
                          & + ss*(tmp134*(2 + tmp1873) + (8 + kappa*tmp1753)*tmp3 + (68 + tmp1*(16 + tmp187&
                          &7))*tmp570)*tmp747 + tmp8*tmp841*(tmp1870 + 3*tmp1869*tmp524 + tmp1176*(34 + tmp&
                          &1*(-7 + tmp976)))) + tmp512*tmp8*tmp949*(tmp1581 + (tmp1*tmp1637 + tmp139*tmp183&
                          &6)*tmp28 + (tmp631*tmp976)/tmp22) + tmp63*(tmp1874 - 128*tmp43*(tmp1060 + (-9 + &
                          &tmp433)/tmp22) + tmp17*(tmp1875 + tmp1878 + (-112 + tmp1*(-18 + tmp1877))*tmp498&
                          & + (4 + tmp1876)*tmp3*tmp538) + (60*tmp15 + tmp449 + (115 + tmp1*tmp1289)*tmp524&
                          &)*tmp9 + tmp570*(2*(5*tmp1515 + tmp3) + (6 + tmp1*(18 + tmp1294))*tmp95 + tmp570&
                          &*(116 + tmp1*(28 + tmp976))))) + tmp196*(7680*tmp1056 + 128*tmp135*(-46*tmp1 + t&
                          &mp1579 + 5*tmp1629) + tmp1*tmp1478*(tmp1581*tmp1834 + ss*(tmp1847 + (-14 + tmp1*&
                          &(-39 + tmp1003))*tmp271)*tmp437 + (tmp1849 + tmp1877*tmp512 + (32 + tmp1*(36 + t&
                          &mp1341))*tmp570)/tmp22) + tmp1603*tmp342*tmp603*tmp74 + tmp165*(tmp1581 + 31*tmp&
                          &3 + (18 + tmp1863*tmp202)*tmp524 + (tmp1858 + (13 + tmp1630)/tmp22)*tmp56 + tmp1&
                          &34*(9 + tmp840)) + tmp512*tmp949*(-78*ss*tmp127 + 480*tmp8 + tmp1836*tmp97 + (tm&
                          &p1230*tmp976)/tmp22) + tmp598*(tmp1581*tmp1833 + tmp17*(tmp1844 + 36*tmp3 + (-54&
                          & + tmp1*(-58 + tmp1003))*tmp524) + (tmp1634 + tmp1832 - 10*tmp1639*tmp512 + tmp5&
                          &70*(68 + tmp1*(58 + tmp976)))/tmp22)) - 2*tmp4*(1536*tmp1879 - (kappa*tmp1084*tm&
                          &p154*tmp342)/tmp22 + 4*tmp1056*(tmp1853 + (tmp1631 + tmp1854)*tmp213 + (40*tmp15&
                          & + 17*tmp3 + 2*(67 + tmp1855)*tmp524)*tmp55 + (16*tmp1492 + tmp1176*(77 + tmp186&
                          &8) + (260 + tmp1*(22 + tmp1657))*tmp570)/tmp22) + tmp42*(1536*tmp1658 - 128*tmp1&
                          &084*(tmp1858 + (-7 + tmp1630)/tmp22) + tmp15*tmp1526*(-40*tmp1 + (-4 + tmp1856)*&
                          &tmp3 + tmp1872*tmp34 + tmp437) + (tmp301*(tmp1079*tmp1669 + tmp1840 - 51*tmp3 + &
                          &(10 - 21*kappa)*tmp321 - 108*tmp524))/tmp22 + tmp15*tmp1515*(tmp1746 + tmp639) -&
                          & 32*tmp43*(-11*tmp3 + (-137 + tmp1859)*tmp524 + (-3 + tmp433)*tmp702)) + ss*tmp3&
                          &54*(192*tmp1084*tmp1833 + tmp15*tmp1515*(tmp1 + tmp1864) + 8*tmp43*(tmp1844 - 36&
                          &*tmp3 + (-66 + tmp1*(38 + tmp1003))*tmp524) + ss*tmp524*(tmp1865 + tmp15*(8 + (-&
                          &6 + tmp1003)*tmp317) + (-72 + tmp1*tmp1856)*tmp524) + (tmp1*tmp1445 + tmp1840 + &
                          &tmp1884 - 87*tmp3 + (52 + tmp1474)*tmp321)*tmp437*tmp8) - 128*tmp1850*(tmp1851 +&
                          & (37 + tmp433)/tmp22 + tmp840) + 16*tmp1582*(tmp1628 + 10*tmp3 + (228 + tmp1872)&
                          &*tmp524 + tmp56*(tmp1602 + (47 + tmp583)/tmp22) + tmp15*(84 + tmp840)) - tmp135*&
                          &(4608*tmp1084 + tmp524*(tmp1862 - 8*tmp15*(-2 + tmp1*tmp1863) + (-176 + (-8 + tm&
                          &p1341)*tmp285)*tmp524) + tmp1426*(tmp1860 + tmp134*tmp1861 + (81 + tmp130)*tmp57&
                          &0) + ss*(tmp1840 + 2*(-60 + tmp1656)*tmp524 + (87 + tmp1891)*tmp561)*tmp841 - 12&
                          &8*tmp43*((-50 + tmp433)/tmp22 + tmp878)) + (tmp171 + kappa*ss*tmp1753 + tmp1852 &
                          &+ (tmp1474*tmp198)/tmp22)*tmp43*tmp512*tmp949 + tmp1*tmp1478*tmp8*(tmp132*tmp164&
                          &5*tmp3 + 48*tmp1834*tmp43 + (tmp1215 + tmp1849 + (-32 + tmp1*(12 + tmp1294))*tmp&
                          &524)*tmp88 + ((tmp1847 + (-26 + tmp1*(9 + tmp1003))*tmp271)*tmp99)/tmp22))))/tmp&
                          &533))/2. + C0tP2P2NP0*Pi*tmp1627*tmp21*tmp22*tmp48*(f1p*tmp1*tmp126*tmp2 + tmp12&
                          &6*tmp2*tmp46 + 2*f2p*(tmp128*tmp441 - 4*((-3 + tmp1*(-7 + tmp1075))/tmp22 + tmp1&
                          &28*tmp28)*tmp6 - (tmp974 - tmp18*tmp980 + 2*tmp128*tmp986)/tmp526 + tmp98*(1/tmp&
                          &22 + tmp128*tmp26 + tmp991) + tmp992 + (tmp131*tmp134 + tmp88*tmp979 + tmp993)/t&
                          &mp21)) + logF2L2*Pi*tmp1055*tmp2*tmp207*tmp21*tmp22*tmp32*tmp46*tmp526*((tmp1094&
                          &*tmp1744*tmp2*tmp317)/tmp21 + (f1p*tmp1094*tmp1744*tmp2*tmp433)/tmp21 + (f2p*(64&
                          &*tmp1624*(tmp352 + tmp46/tmp21) + tmp1576*(3840*tmp42 + tmp1670*((-1 + tmp1)*tmp&
                          &113 + tmp1851 + tmp46) + (tmp317*(tmp1581 + (-(tmp1221/tmp22) + (2 + tmp1)*tmp27&
                          &1)/tmp22 + tmp190*(tmp291 + tmp1351*tmp524)))/tmp21 + tmp1515*tmp621 + (tmp1516 &
                          &+ tmp1581 + tmp1634 + tmp1219*tmp321 + tmp55*(tmp285 + (-2 + tmp583)/tmp22))*tmp&
                          &7) - (16*tmp1625*(tmp176 + tmp1*(tmp12 + tmp1626 + tmp305 + tmp537) + (4*(1/tmp2&
                          &2 + tmp317 + tmp444 + tmp747))/tmp21))/tmp21 - tmp196*(5120*tmp135 + tmp240*(tmp&
                          &1630 + tmp1636 + tmp356) + tmp284*(104*ss*tmp1 + tmp1628 + tmp1687*(-3 + tmp271)&
                          & - 30*tmp3 + (10 + tmp1637)*tmp321 + tmp486 - 2*tmp15*(5 + tmp583)) + tmp1515*tm&
                          &p38*tmp621 + tmp352*(tmp1649 + tmp46*(tmp15*(-10 + tmp1652) + tmp428 + (9 + tmp1&
                          &883*tmp271)*tmp524) + tmp11*(tmp1640 + 38*tmp3 + (14 + tmp1843)*tmp570) + (288*t&
                          &mp1 - (80*tmp1654)/tmp22)*tmp8) + (tmp1*(tmp1638 + kappa*(-19/tmp22 + tmp285)*tm&
                          &p321 + ss*(tmp1221*tmp139 + (-10 + tmp1*tmp1639)*tmp271)*tmp437 + 80*((-1 + tmp1&
                          &*tmp1474)/tmp22 + tmp453)*tmp8))/tmp21) + tmp1828*tmp271*(16*tmp130*tmp42 + kapp&
                          &a*tmp1661*tmp43 - 4*tmp41*(tmp34 + tmp1*(1 + tmp129*tmp138 + tmp439)) + (tmp200 &
                          &+ tmp3 + tmp130*tmp377 + tmp17*(tmp427 + tmp44))*tmp6 + (tmp46*(ss*tmp427 + 16*t&
                          &mp129*tmp43 + tmp432 + kappa*tmp1078*tmp8))/tmp21)*tmp949 + tmp62*(1920*tmp1056 &
                          &+ (tmp271*(-240*tmp1084 + tmp1663 + ((tmp1293 + tmp1641 + tmp317 + tmp34)*tmp358&
                          &)/tmp22 + 160*tmp1*(-1 + kappa/tmp22)*tmp43 + (kappa*(tmp1 + tmp1540)*tmp452)/tm&
                          &p22))/tmp21 + (tmp1874 - 128*tmp1*(-13 + 10/tmp22)*tmp43 + tmp1096*(tmp1 + tmp16&
                          &32 + (-6 + tmp127)/tmp22 + tmp45) + tmp11*(tmp1875 + (5 + tmp1859)*tmp321 + 2*(-&
                          &6 + tmp1656)*tmp498) + tmp1426*(tmp1207 + tmp1644 + 2*(-5 + tmp1597)*tmp524))*tm&
                          &p6 + 5*tmp1515*tmp621*tmp8 - 4*tmp41*(tmp1599 + (-4 + tmp1635)*tmp498 + 2*tmp512&
                          & + (tmp1219*tmp512)/tmp22 + tmp17*(tmp134*tmp1643 + tmp481 + (-5 + tmp1642)*tmp5&
                          &24) + tmp1633*(5 + tmp437)*tmp8) - 32*tmp135*(tmp356 + tmp46*(25 + tmp841)) - 16&
                          &*tmp42*(18*tmp3 + ss*tmp1059*(9 + tmp34) + tmp377 + (-1 + tmp1642)*tmp524 + tmp2&
                          &82*tmp95)) + tmp4*(256*tmp1850 - 128*tmp1582*(tmp138 + tmp105*tmp34 + tmp563) + &
                          &5*tmp1084*tmp1515*tmp621 + (tmp317*tmp8*(tmp1646 + tmp1798*tmp31 - 16*(tmp1230 +&
                          & tmp1597/tmp22)*tmp43 + (kappa*tmp291*tmp452)/tmp22 + (tmp1648 + tmp1221/tmp22 +&
                          & tmp317)*tmp434*tmp8))/tmp21 + tmp28*tmp6*(-14*tmp132*tmp1515 + 128*tmp1658 - 8*&
                          &tmp43*(tmp1640 - 37*tmp3 + (-8 + tmp1*(30 + tmp1856))*tmp524) - 64*tmp1084*(tmp1&
                          &060 + (2 + tmp583)/tmp22) + (tmp1632 + (-8 + 17*tmp127)/tmp22 + tmp45)*tmp560*tm&
                          &p74 - 4*(tmp321*(-21 + (-3 + tmp1075)*tmp433) + 2*(2 + tmp1*(6 + tmp1657))*tmp49&
                          &8 - 8*tmp512)*tmp8) - 4*tmp135*(tmp1649 + tmp46*(tmp15*(-38 + tmp1652) + tmp481 &
                          &+ ((6 + tmp1075)*tmp3 + tmp583)/tmp22) + tmp56*(tmp1871 + (tmp1 + tmp2*tmp481)/t&
                          &mp22 + tmp15*(-4 + tmp840)) + tmp348*(tmp285 + tmp1650*tmp841)) + 16*tmp1056*(tm&
                          &p1581 - 15*tmp3 + (-2 + tmp1*(-14 + tmp1341))*tmp524 + tmp56*((3 + tmp1)*tmp34 +&
                          & tmp666) + (-5 + tmp583)*tmp95) + tmp42*(3840*tmp1084 - 512*tmp43*(tmp1114 + tmp&
                          &454) + (tmp321*(5 + tmp433) + 2*(10 + tmp1656)*tmp498 - 4*tmp512)*tmp55 + tmp321&
                          &*(tmp438 + tmp44 + (16 + tmp127)*tmp57) + tmp1426*(tmp1448 + (-13 + tmp1*tmp1653&
                          &)*tmp524 + tmp1654*tmp95)) - 4*tmp41*(384*tmp1658 + kappa*tmp132*tmp1669 + ss*tm&
                          &p1096*(tmp1632 + (8 + 27*tmp127)/tmp22 + tmp31) - 8*tmp43*(tmp134*tmp1660 + tmp1&
                          &664 + (-17 + (10 + tmp1645)*tmp271)*tmp524) - 64*tmp1084*((3 + tmp1659)/tmp22 + &
                          &tmp666) + ((-9 + tmp1*(-6 + tmp1075))*tmp321 + (-26 + (6 + 33*kappa)*tmp1)*tmp49&
                          &8 - 14*tmp512)*tmp982)) + (tmp36*(64*tmp1582*tmp1662 - 16*tmp1056*(tmp1661 + tmp&
                          &1007*tmp1662 + (-14 + tmp1645)*tmp321 + tmp15*(-2 + tmp433)) + ss*tmp441*(ss*((-&
                          &4 + tmp1220)*tmp1441 + tmp1667 + (-31 + tmp1630*tmp2)*tmp321) + 80*tmp1662*tmp43&
                          & + tmp357*(tmp1664 + tmp134*(1 + tmp433) + (-3 + 40*tmp1)*tmp524) + tmp321*(tmp1&
                          &632 + (3 + tmp127)*tmp46 + tmp1637*tmp524)) + tmp1084*tmp1515*tmp621 + (tmp433*t&
                          &mp8*(tmp1663 + 4*(tmp1626 + tmp310)*tmp43 + (kappa*tmp452)/(tmp22*tmp599) + ((tm&
                          &p1641 + tmp317 + tmp34 + (-1 + tmp1003)*tmp428)*tmp8)/tmp22))/tmp21 + tmp42*(-64&
                          &0*tmp1662*tmp43 + tmp321*(tmp1666 + tmp1*tmp1887 + (-8 + tmp1*tmp1003)*tmp57) + &
                          &tmp11*(tmp1667 + 4*(-1 + tmp1220)*tmp498 + tmp1096*(19 + tmp1518*tmp840)) + (24*&
                          &tmp15 + (-5 + 2*tmp1873)/tmp22 + tmp769)*tmp8*tmp915) + 4*tmp135*(tmp1669 + tmp1&
                          &662*tmp1880 + (22 + tmp1220)*tmp498 + tmp56*(tmp448 + (-1 + tmp1*(-12 + tmp1341)&
                          &)*tmp524 + tmp15*(-2 + tmp840)) + (tmp1005 + tmp1669*(1 + tmp976))/tmp22) + ss*t&
                          &mp1478*(14*tmp132*tmp1515 + 32*tmp1084*tmp1662 + 8*(tmp1224 + tmp1885 + (tmp1 + &
                          &(18 + kappa)*tmp3)/tmp22)*tmp43 + (tmp452*(tmp1059 + tmp1632 + tmp1668 + tmp44 +&
                          & tmp1354*tmp524))/tmp22 + (tmp1669 + tmp134*(tmp1 + tmp1219*tmp3) + tmp321*(-13 &
                          &+ tmp840*(-1 + tmp976)))*tmp99)))/tmp526 + tmp307*(768*tmp1582 - 32*tmp1056*(-22&
                          &*tmp1 + tmp1579 + (5 + tmp305)/tmp22) + 8*tmp135*(tmp1628 + tmp142*(tmp1 + tmp16&
                          &29) + (tmp1661*tmp2)/tmp22 + tmp134*(-5 + tmp271) - 28*tmp3 + tmp544) + 5*tmp151&
                          &5*tmp43*tmp621 + 4*tmp42*(tmp1599 + tmp17*(11*tmp3 + tmp15*(-6 + tmp433) + (-6 +&
                          & tmp1220)*tmp524) + tmp524*(tmp1594 + (-10 + tmp1635)/tmp22 + tmp583) + tmp348*(&
                          &tmp1659 + (-1 + tmp736)/tmp22)) + (ss*tmp1*(-192*tmp1084 + kappa*tmp132*tmp449 +&
                          & (tmp1351*(tmp1 + tmp270)*tmp452)/tmp22 + 40*tmp43*(tmp454 + tmp1474*tmp524) + t&
                          &mp1073*(tmp1627 + tmp1641 + tmp317 + tmp34)*tmp8))/tmp21 + tmp41*(-2304*tmp1084 &
                          &+ 64*(tmp1631 + (2 + 28*tmp1)/tmp22)*tmp43 + (tmp1526*(tmp1594 + tmp1630 - 14/tm&
                          &p22 + (-2 + tmp1657)*tmp524))/tmp22 + tmp1096*(tmp1632 + tmp127*tmp1864 + tmp271&
                          & + tmp31 + tmp847) + (6*tmp15*tmp1580 + tmp1673 + (-13 + tmp1873)*tmp524)*tmp9) &
                          &+ ss*tmp6*(768*tmp1084 - 32*(tmp1633 + tmp139*tmp1660)*tmp43 + tmp348*(tmp1634 +&
                          & tmp1640 + (-18 + (10 + 23*kappa)*tmp1)*tmp524) + ss*tmp1792*(tmp113 + tmp1627 +&
                          & tmp317 + tmp1653*tmp524) + tmp1096*(24*kappa*tmp15 + tmp31 + tmp848 + 55*tmp994&
                          &)))))/tmp61) + C0tF2F2NF0*Pi*tmp2*tmp207*tmp21*tmp22*tmp48*tmp481*(f1p*tmp1*tmp2&
                          &*tmp973 + tmp2*tmp46*tmp973 + (f2p*(-4*tmp4*(tmp974 + (tmp105*tmp443 + tmp975)/t&
                          &mp21 - tmp1529*tmp980) + (tmp128*tmp284 - 4*tmp6*((3 + tmp1635)/tmp22 + tmp975) &
                          &+ tmp26*(tmp1424*tmp2 + tmp128*tmp58 + tmp15*tmp979) + (tmp117 + tmp15 + tmp199)&
                          &*tmp980)/tmp526 + tmp350*(tmp980 + tmp990) + (tmp105*tmp83 + tmp30*tmp980 + tmp2&
                          &9*tmp546*(tmp105*tmp28 + tmp980) + tmp27*(tmp105*tmp56 + 3*tmp995))/tmp22))/tmp6&
                          &1) + D0IR6tsN*Pi*tmp1661*tmp2*tmp21*tmp22*tmp48*((tmp1*tmp2*tmp989)/tmp533 + (f1&
                          &p*tmp2*tmp46*tmp989)/tmp533 + f2p*(tmp4*((3 + tmp1490)*tmp163 + (tmp128*tmp138 +&
                          & (5 + tmp1855)/tmp22)*tmp26 - tmp603*tmp980) + tmp307*(1/tmp22 + tmp990 + tmp991&
                          &) - tmp36*(tmp1478*(ss*tmp1642 + tmp128*tmp270) + 24*tmp131*tmp41 + tmp992 + tmp&
                          &26*(tmp131*tmp15 + (1 + tmp1469)*tmp88 + tmp993)) - (tmp1478*((29 + tmp1292*tmp4&
                          &6)/tmp22 + tmp131*tmp56) + tmp128*tmp598 + tmp345*tmp980 + (tmp15*(1 + tmp1578) &
                          &+ tmp128*tmp429 + tmp138*tmp995)/tmp21)/tmp526)) + (log4*Pi*tmp1055*tmp160*tmp2*&
                          &tmp22*tmp3*tmp32*tmp552*((f1p*tmp1094*tmp1575*tmp2*tmp271)/tmp21 + tmp1*tmp1094*&
                          &tmp1575*tmp2*tmp546 + (f2p*(4*tmp1576*(tmp185 + tmp1515/tmp22 - 8*tmp198*tmp6 + &
                          &(tmp1661 + tmp271)*tmp949) + tmp197*((tmp1515*tmp1831)/tmp22 + 384*tmp42 + tmp28&
                          &4*(-(tmp1577/tmp22) + tmp444 + tmp453) - 4*((tmp1591 - 2*(tmp1 + (-7 + kappa)*tm&
                          &p3))/tmp22 + tmp198*tmp444)*tmp6 + tmp1*(ss*(24 + tmp1633) + (4 + tmp1*tmp1351)/&
                          &tmp22 + tmp1341*tmp3)*tmp949) + tmp54*(1920*tmp135 + tmp1478*(tmp1581*tmp198 + s&
                          &s*(tmp1605 + tmp271 + (54 + tmp1603)*tmp3)*tmp437 + (-25*tmp1515 + tmp1593 + (-2&
                          & + tmp1*(4 + tmp1341))*tmp524)/tmp22) + tmp165*(tmp1579 + (-9 + tmp1611)/tmp22 +&
                          & tmp632) + tmp598*(-72*ss*tmp1 + tmp1067*tmp15 + tmp1581 + (9 + 10*tmp1065)*tmp5&
                          &24 + (8 - 20*tmp130)*tmp74) + tmp1*(tmp1612 + tmp1645*tmp321 + 78*kappa*tmp452 +&
                          & tmp74*(40 + kappa*tmp761) - 120*tmp1580*tmp8)*tmp949 + tmp1515*tmp603*tmp97) + &
                          &(960*tmp1582 + (10*tmp108*tmp1515*tmp43)/tmp22 - 2*tmp42*(tmp1599 + (tmp1600/tmp&
                          &22 + tmp317)*tmp377 + ss*tmp437*(-11*tmp1 + (-30 + tmp130)/tmp22 + tmp1079*tmp44&
                          &9) + (tmp1607 + tmp1603*tmp512 + (52 + 3*tmp1855)*tmp524)/tmp22) - 16*tmp1056*(8&
                          &8*ss + (19 + tmp1068)*tmp139 + tmp616) + ss*tmp1478*(120*tmp198*tmp43 + (tmp1606&
                          & + tmp1615)*tmp498 + (tmp1607 + tmp1637*tmp512 + (42*tmp1 + tmp1058*tmp560)/tmp2&
                          &2)*tmp74 + (tmp1602 + tmp1605 + (22 + tmp1603)*tmp3)*tmp437*tmp8) + tmp41*(960*t&
                          &mp1084 - 32*tmp43*((-26 + tmp1617)/tmp22 + tmp453) + (tmp1608 + (6 + tmp1*(12 + &
                          &tmp1387))/tmp22)*tmp498 + (tmp301*((9 + tmp1610)*tmp305 + (-14 + tmp130)*tmp57))&
                          &/tmp22 + (tmp1618 + tmp1645*tmp512 + (30 + (8 + kappa)*tmp1)*tmp570)*tmp88) + tm&
                          &p1*tmp8*(tmp28*(tmp1608 + tmp1598*tmp434) + tmp1596*tmp57 - 60*tmp1580*tmp8)*tmp&
                          &949 - 4*tmp135*(-88*ss*tmp1 + (19 + tmp1595*tmp271)*tmp586 + 384*tmp8 + ss*(11 +&
                          & tmp1064)*tmp841 + tmp1869*tmp95))*tmp98 + tmp49*(128*tmp1582 + 32*tmp1056*(-((2&
                          &3 + tmp130)/tmp22) + tmp444 + tmp46) + tmp1515*tmp34*tmp66 - 2*tmp42*((tmp1215 +&
                          & tmp1593 + (tmp1602 + tmp1890)/tmp22)/tmp22 + (tmp1059 + (-26 + tmp130)/tmp22)*t&
                          &mp348 + 256*tmp43 + tmp335*(tmp1594 + (-44 + tmp1064)/tmp22 + tmp761)) + ss*tmp1&
                          &478*(tmp1585*tmp1764 + 16*tmp198*tmp43 + (tmp1593 + tmp1865 + (22*tmp1 + (4 + tm&
                          &p1637)*tmp3)/tmp22)*tmp74 + tmp437*tmp8*(tmp1591 + tmp1594 + tmp840)) + tmp41*(1&
                          &28*tmp1084 - 32*(tmp1589 + tmp317)*tmp43 + tmp1596*tmp498 + (tmp301*(tmp1080 + t&
                          &mp1586 + tmp555 + tmp769))/tmp22 + (tmp1515 + (44*tmp1 + (8 + tmp1354)*tmp3)/tmp&
                          &22 + tmp15*(8 + tmp129*tmp317))*tmp88) + tmp1*tmp8*(tmp209*(tmp1598/tmp22 + tmp3&
                          &1) + (tmp1587 + tmp44)/tmp22 + tmp8*(8 + tmp915))*tmp949 + 8*tmp135*(tmp406 + tm&
                          &p17*((30 + tmp130)/tmp22 + tmp453) + (23*tmp1 + 2*tmp1293 + 26/tmp22 + tmp991)/t&
                          &mp22)) - 4*tmp37*(640*tmp1056 + 16*tmp135*(tmp17 + (-24 + tmp1611)/tmp22 + tmp75&
                          &2) + tmp139*tmp1515*tmp18*tmp8 - tmp6*(160*tmp198*tmp43 + (tmp1615 + kappa*tmp16&
                          &61)*tmp498 + (tmp1605 + (7 + tmp1*(19 + tmp1354))*tmp271)*tmp437*tmp8 + (-11*tmp&
                          &1515 + tmp1593 + (6 + tmp1*(4 + tmp1003))*tmp524)*tmp88) + ss*tmp1*(tmp1612 + (k&
                          &appa*tmp1673)/tmp22 + ss*tmp1598*tmp434 + 23*kappa*tmp452 + (40 - 80*tmp1)*tmp8)&
                          &*tmp949 + tmp41*(tmp1616 + (tmp1618 + tmp1841 + (22 + tmp1*(8 + tmp1294))*tmp570&
                          &)/tmp22 + tmp348*((-14 + tmp1617)/tmp22 + tmp583) + ss*tmp437*(tmp1600*tmp34 + t&
                          &mp1*(5 + tmp433*(-10 + tmp976)))) + 4*tmp42*(tmp171 + tmp11*(tmp1 + (5 + tmp1071&
                          &)/tmp22) + (24*tmp1 + tmp1613 + 10*tmp1079*tmp3 + tmp996)/tmp22)) - (2*tmp36*(38&
                          &4*tmp1582 + 4*tmp135*(tmp171 + tmp274 + tmp1600*tmp335 + (78*tmp1 + tmp1620 + 88&
                          &/tmp22 + (-11 + tmp1003)*tmp428)/tmp22) + (kappa*tmp154*tmp1795*tmp43)/tmp22 - 1&
                          &6*tmp1056*((78 + tmp1617)/tmp22 + tmp56 + tmp583) + ss*tmp6*(tmp15*tmp1515 + 96*&
                          &tmp198*tmp43 + (tmp1515 - 8*tmp15*tmp1601 + (68*tmp1 + (16 - 13*kappa)*tmp3)/tmp&
                          &22)*tmp74 + (tmp1073 + tmp1745 + tmp1611/tmp22 + (9 + tmp1354)*tmp428)*tmp437*tm&
                          &p8) - tmp41*(384*tmp1084 + 16*(tmp1630 + (32 + tmp1611)/tmp22)*tmp43 + (tmp1623/&
                          &tmp22 + tmp44)*tmp498 + ss*((8 + tmp1597)/tmp22 + tmp1637*tmp3)*tmp524 + tmp437*&
                          &(46*tmp1 + tmp1621 + tmp1627 + tmp1287*tmp712)*tmp8) + tmp1*(tmp1598*tmp162 + tm&
                          &p15*tmp1623 + kappa*tmp321 + tmp1580*tmp429 + tmp1351*tmp452)*tmp8*tmp949 + tmp4&
                          &2*(tmp1853 + (tmp1862 + (-88 + (-16 + kappa)*tmp1)*tmp524 + tmp1601*tmp702)/tmp2&
                          &2 + ((46 + tmp130)/tmp22 + tmp453)*tmp9 + ss*tmp437*(tmp1639*tmp428 + tmp583 + t&
                          &mp841 + tmp996))))/tmp526))/tmp533))/2. - 4*C0tF2F2NFL*Pi*tmp2*tmp207*tmp21*tmp2&
                          &2*tmp32*tmp512*((4*f1p*tmp2*(tmp140*tmp1430 - tmp1444/tmp22 + tmp1435*tmp307 - 2&
                          &*tmp1455*tmp4 + (tmp1459*tmp1478 + tmp1494 + tmp1495 + tmp1496 + tmp1465*tmp26 +&
                          & tmp1456*tmp284 - 96*tmp42)/tmp526))/tmp21 + (4*tmp2*(tmp1444/tmp22 + tmp1435*tm&
                          &p353 - 16*tmp1430*tmp54 + tmp1455*tmp98 + (tmp1427 + tmp1459*tmp27 + tmp1734*tmp&
                          &310 - 16*tmp1456*tmp41 + tmp1465*tmp546 + ss*tmp1*tmp1470*tmp841 + tmp1457*tmp98&
                          &2)/tmp526))/tmp21 + (f2p*((tmp1329 + tmp127*tmp198 + (4*(tmp1287 + tmp1466))/tmp&
                          &21)*tmp350 + (-32*tmp42 + kappa*tmp1103*tmp74 + tmp1478*((4*tmp1287 + tmp1892)/t&
                          &mp22 + (tmp1469 + tmp2/tmp22)*tmp56 + tmp9) + tmp598*(tmp1469 + tmp56 + tmp976/t&
                          &mp22) + (ss*(tmp1287 + tmp1*tmp2)*tmp437 + (tmp1287 + tmp1469)*tmp58 + tmp1470*t&
                          &mp994)/tmp21)/tmp22 + (tmp127*tmp1522 + kappa*tmp240 - 16*(tmp1471 + tmp1472/tmp&
                          &22)*tmp41 + tmp352*(tmp1473 + tmp1477 + tmp15*tmp1619 + tmp31 + (-4 + tmp1645)*t&
                          &mp524) + tmp546*(tmp17*(tmp129*tmp237 + tmp31 + tmp1289*tmp524) + (3*tmp1287 + t&
                          &mp302)*tmp58 + (tmp1407 + tmp31 + tmp996)/tmp22))/tmp526 - 2*tmp4*(tmp127*tmp151&
                          &4 + kappa*tmp258 + (ss*tmp1075 + tmp1466 + tmp57)*tmp7 + (4*(tmp11*tmp1288 + tmp&
                          &31 + (1 + tmp1354)*tmp524 + tmp997))/tmp21)))/tmp61) + C0sF2P2FLP*f2p*Pi*tmp2*tm&
                          &p22*tmp48*tmp53*tmp915*(tmp197 + (tmp198 + tmp343 + tmp39)*tmp54 + tmp4*(-4*tmp4&
                          &1 + (tmp199 + tmp304 + ss*(tmp198 + tmp44))/tmp21 + tmp28*(tmp204 + tmp358 + ss*&
                          &tmp589) + (tmp11 + tmp139 + tmp202 + tmp45)*tmp6) - tmp37*(tmp1173 + tmp200 + tm&
                          &p352 + 20*tmp8 + (tmp285 + tmp45 + tmp86)/tmp21) - tmp36*(-(tmp41*(tmp271 + tmp3&
                          &1 + tmp34 + tmp56)) + (tmp35*(tmp201 + ss*(tmp31 + tmp433 + tmp437) + tmp58))/tm&
                          &p21 + tmp63 + tmp6*(tmp201 + ss*(tmp139 + tmp202 + tmp303) + tmp70) + tmp8*(tmp2&
                          &00 + tmp309 + tmp99)) + (tmp203 - tmp41*(tmp139 + tmp202 + tmp31 + tmp56) + tmp1&
                          &478*(tmp161 + tmp35*(tmp198 + tmp45) + tmp712/tmp22) + (tmp1271 + tmp204 + 5*tmp&
                          &8)*tmp982 + (ss*(tmp1*tmp1746 + tmp429 + ss*(tmp270 + tmp435 + tmp999)))/tmp21)/&
                          &tmp526) - 4*C0tP2P2NPL*Pi*tmp160*tmp2*tmp21*tmp22*tmp32*tmp512*(f1p*tmp1054*tmp2&
                          &*tmp26 + tmp1054*tmp2*tmp546 + (f2p*(kappa*ss*tmp1103*tmp15 + kappa*tmp1525 - 16&
                          &*(tmp1471 + (-28 + 17*kappa)/tmp22)*tmp42 + tmp441*((-15 + 8*kappa)*tmp134 + tmp&
                          &1473 + tmp1477 + tmp31 + (30 - 23*kappa)*tmp524) + tmp1478*(tmp17*(tmp1285*tmp15&
                          & + tmp1490/tmp22 + tmp31) + tmp1002*tmp58 + (tmp1645*tmp3 + 2*(9 - 11*kappa)*tmp&
                          &524 + tmp129*tmp702)/tmp22) + tmp98*(kappa*tmp185 + kappa*tmp498 - 8*tmp1002*tmp&
                          &6 + tmp26*tmp998) - (kappa*tmp359 + kappa*tmp258*(tmp28 + tmp46) + kappa*tmp18*t&
                          &mp498 - 8*tmp6*(tmp1001*tmp17 + tmp45 + kappa*ss*tmp840 + tmp15*tmp977 + tmp991)&
                          & + 2*tmp986*tmp998)/tmp526 + (kappa*(tmp109 + tmp202)*tmp498 + tmp357*tmp998 + t&
                          &mp88*(-(tmp1490/tmp22) + tmp997 + tmp999))/tmp21))/tmp533) + logF2N2*Pi*tmp1055*&
                          &tmp2*tmp207*tmp21*tmp22*tmp3*tmp32*tmp526*(f1p*tmp1*tmp1094*tmp1171*tmp2 + tmp10&
                          &94*tmp1171*tmp2*tmp46 + (f2p*((16*tmp1624)/tmp48 - 4*tmp1625*(tmp277 + (4*(tmp10&
                          &59 + tmp1626 + 1/tmp22 + tmp304 + tmp444))/tmp21 + tmp1*(tmp1629 + tmp537)) + tm&
                          &p196*(-3200*tmp42 + ss*tmp202*(tmp253 + ((-2 + tmp1065)/tmp22 + tmp45)/tmp22 + s&
                          &s*tmp437*(1 + tmp453)) + (tmp1638 + (-42*tmp1 + (1 + tmp1071)*tmp139)*tmp348 + s&
                          &s*(18*tmp1 + tmp1079*tmp1176 + tmp1067*tmp434)*tmp437 + 3*((-4 + tmp1065)/tmp22 &
                          &+ tmp45)*tmp524)/tmp21 - 16*tmp41*(152*ss - 50*tmp1 + (5 + tmp129*tmp712)/tmp22)&
                          & - 4*tmp6*(tmp17*(-38*tmp1 + tmp1866/tmp22) + (tmp1072 + tmp1668 + tmp1287*tmp17&
                          &53 + tmp563)/tmp22 + 672*tmp8)) + tmp62*(1760*tmp135 - 4*(tmp17*(tmp1 + (7 + tmp&
                          &1286)/tmp22) + (tmp1073 + 3*tmp1519 + tmp1620 + tmp317)/tmp22)*tmp41 + 8*tmp42*(&
                          &-55*tmp1 - (8*(1 + tmp1068))/tmp22 + tmp56) + ((tmp1090*tmp1426)/tmp22 + 896*tmp&
                          &43 + ss*(tmp1072 + (-8 + tmp1064)*tmp34 + tmp435)*tmp437 + ((20 + tmp1061)/tmp22&
                          & + tmp31)*tmp524)*tmp6 + tmp202*(tmp1070 + tmp1424 - 24*tmp8)*tmp8 + (tmp28*(240&
                          &*tmp43 + ((8 + tmp1061)/tmp22 + tmp31)*tmp524 + (tmp1*(-14 + tmp1*(-22 + tmp1003&
                          &)) + tmp1077)*tmp88 + tmp1*(-7 + 10*tmp1287)*tmp9))/tmp21) + tmp307*(1152*tmp105&
                          &6 - 8*tmp135*(36*tmp1 + tmp1082 + (9 + tmp1074)/tmp22) + tmp1*tmp43*(tmp1085 + (&
                          &tmp1042 + 1/tmp22)*tmp39 + tmp430) - 2*tmp42*((tmp1060 + tmp1076 + tmp1621 + tmp&
                          &1648)/tmp22 + tmp253 + (16*tmp1 + (-7 + tmp1064)/tmp22)*tmp55) + ss*(tmp1599 + (&
                          &tmp1060 + (-18 + 23*tmp130)/tmp22)*tmp348 + ((32 + tmp1065)/tmp22 + tmp45)*tmp52&
                          &4 + tmp335*((-9 + tmp1610)*tmp271 + (4 + tmp1064)*tmp57))*tmp6 + ((tmp1091 + ss*&
                          &(tmp1077 + (-9 + tmp1*(-19 + tmp1003))*tmp271)*tmp437 + 3*((4 + tmp1065)/tmp22 +&
                          & tmp45)*tmp524 + (tmp1078 + (-1 + tmp1071)*tmp139)*tmp58)*tmp8)/tmp21 + tmp41*(-&
                          &576*tmp43 + ((34 + tmp1065)/tmp22 + tmp45)*tmp524 + tmp335*(tmp1080 + tmp1586 + &
                          &32/tmp22 + tmp712) + (tmp271 + (-18 + tmp129*tmp435)/tmp22)*tmp9)) + tmp1576*(17&
                          &28*tmp41 + (4*(tmp1581 + (tmp1067/tmp22 + (-3 + tmp1)*tmp271)/tmp22 + tmp56*(tmp&
                          &1076 + tmp34 + tmp632)))/tmp21 + tmp1*(tmp1070 + (tmp1650*tmp190)/tmp22 - 240*tm&
                          &p8) + (40*ss + tmp1060 + tmp1577/tmp22)*tmp942) + tmp36**6*tmp528*tmp840*tmp949 &
                          &- (tmp49*(128*tmp1056 - 16*tmp135*(tmp228 + tmp271 + (1 + tmp1064)*tmp360) + (ss&
                          &*tmp1057 + tmp1069)*tmp43*tmp524 - tmp41*((tmp1078 + tmp1589)*tmp348 + 512*tmp43&
                          & + ss*((14 + tmp1089)/tmp22 + (5 + (7 + kappa)*tmp1)*tmp271)*tmp437 + (-((16 + t&
                          &mp1065)/tmp22) + tmp31)*tmp524) + 4*tmp42*(tmp1628 + tmp493 + (-3 + tmp1*(4 + tm&
                          &p1474))*tmp524 + tmp15*(-16 + tmp129*tmp639) + (40 - 52*tmp130)*tmp74) + (tmp8*(&
                          &tmp1088 + ss*(tmp1059 + tmp1080 + tmp113 + 10*tmp2*tmp3)*tmp437 + ((-1 + tmp1068&
                          &)/tmp22 + tmp305)*tmp9))/tmp21 + ss*tmp6*((tmp1526*(8 - 9*tmp1287 + (-9 + tmp129&
                          &4)*tmp305))/tmp22 + 128*tmp43 + ((8 + tmp130)/tmp22 + tmp840)*tmp9 + tmp524*((14&
                          & + tmp1092)/tmp22 + tmp999))))/tmp526 + tmp4*(832*tmp1582 - 16*tmp1056*(tmp1081 &
                          &+ tmp1082 + (-2 - 11*tmp130)/tmp22) + tmp1*tmp1084*(tmp1085 + tmp190*(tmp204 + 1&
                          &/tmp22) + tmp348) + tmp42*(tmp1616 + (21*tmp1 + (4 + 22*tmp130)/tmp22)*tmp348 + &
                          &tmp1501*(tmp1601*tmp34 + tmp1643*tmp46) + 3*((18 + tmp1065)/tmp22 + tmp45)*tmp52&
                          &4) + (4*tmp43*(tmp1088 + 16*tmp43 + tmp58*(tmp1076 + tmp113 + tmp639) + tmp74*(t&
                          &mp1077 + (-54 + 20*kappa)*tmp3 + tmp761)))/tmp21 + 4*tmp135*(tmp1*tmp1082 + tmp1&
                          &147*tmp1600 + (-9 + tmp1074)*tmp1687 + 2*(-1 + tmp1*(5 + tmp1083))*tmp524 + 336*&
                          &tmp8) + tmp11*tmp41*(304*tmp43 + ((-6 + tmp1065)/tmp22 + tmp45)*tmp524 + tmp301*&
                          &(tmp563 + tmp1090*tmp57) + ((20 + tmp1089)/tmp22 - 2*(tmp1 + (-9 + tmp1083)*tmp3&
                          &))*tmp88) + tmp6*tmp99*(tmp1091 + (tmp1526*(21 - 18*tmp1287 + tmp1059*tmp1595))/&
                          &tmp22 + (tmp1631 + (-30 + 29*tmp130)/tmp22)*tmp301 + tmp524*((20 + tmp1092)/tmp2&
                          &2 + tmp999)))))/tmp61)

  END FUNCTION MP2MPL_MP_FIN_D11D22

  FUNCTION MP2MPL_MP_FIN_D10D23(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_FIN_d10d23
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201

  tmp1 = sqrt(lambda)**4
  tmp2 = -1 + kappa
  tmp3 = tmp2**2
  tmp4 = 1/mm2
  tmp5 = -tt
  tmp6 = sqrt(lambda)**2
  tmp7 = -tmp6
  tmp8 = 4/tmp4
  tmp9 = tmp7 + tmp8
  tmp10 = 1/tmp9
  tmp11 = me2**2
  tmp12 = -ss
  tmp13 = tmp12 + 1/tmp4
  tmp14 = tmp13**2
  tmp15 = ss + 1/tmp4
  tmp16 = -2*me2*tmp15
  tmp17 = tmp11 + tmp14 + tmp16
  tmp18 = 1/tt
  tmp19 = -4/tmp4
  tmp20 = 4*me2
  tmp21 = tmp20 + tmp5
  tmp22 = 1/tmp21
  tmp23 = 2*me2
  tmp24 = tmp19 + tmp6
  tmp25 = tmp24**(-2)
  tmp26 = tmp4**(-2)
  tmp27 = sqrt(lambda)**6
  tmp28 = 2*ss
  tmp29 = 1/tmp18 + tmp28
  tmp30 = -16/tmp4
  tmp31 = sqrt(lambda)**8
  tmp32 = tmp4**(-3)
  tmp33 = 8*ss
  tmp34 = 1/tmp18 + tmp33
  tmp35 = 3/tmp18
  tmp36 = tmp18**(-2)
  tmp37 = 1/tmp18 + tmp7
  tmp38 = -(1/tmp4)
  tmp39 = ss + tmp38
  tmp40 = tmp6/tmp18
  tmp41 = 2/tmp18
  tmp42 = ss**2
  tmp43 = 4*ss
  tmp44 = -4*tmp6
  tmp45 = 1/tmp18 + tmp44
  tmp46 = tmp12/tmp18
  tmp47 = ss/tmp18
  tmp48 = ss*tmp44
  tmp49 = me2 + tmp13
  tmp50 = tmp5 + tmp8
  tmp51 = tmp20 + tmp6
  tmp52 = 1/tmp50
  tmp53 = -2*tmp6
  tmp54 = 1/tmp18 + tmp53
  tmp55 = 1/tmp18 + tmp19
  tmp56 = tmp55**(-2)
  tmp57 = 2*tmp6
  tmp58 = 6*ss
  tmp59 = 32*tmp32
  tmp60 = 2*tmp42
  tmp61 = 16*ss
  tmp62 = tmp5 + tmp61
  tmp63 = tmp62/tmp4
  tmp64 = 2/tmp4
  tmp65 = me2**3
  tmp66 = 16*tmp26
  tmp67 = -tmp40
  tmp68 = tmp28 + tmp35
  tmp69 = 1/me2
  tmp70 = 8/tmp4
  tmp71 = tmp6 + tmp8
  tmp72 = -3/tmp18
  tmp73 = tmp33 + tmp72
  tmp74 = tmp19*tmp73
  tmp75 = tmp1 + tmp66 + tmp67 + tmp74
  tmp76 = ss + tmp35
  tmp77 = -(tmp47*tmp76)
  tmp78 = 7*tmp36
  tmp79 = tmp68/tmp18
  tmp80 = 10*tmp42
  tmp81 = tmp4**(-4)
  tmp82 = 3*tmp6
  tmp83 = 1/tmp18 + tmp57
  tmp84 = 4*tmp6
  tmp85 = 5*tmp6
  tmp86 = tmp35 + tmp85
  tmp87 = 1/tmp18 + tmp82
  tmp88 = tmp60*tmp87
  tmp89 = 8*tmp42
  tmp90 = -2*tmp36
  tmp91 = tmp33*tmp6
  tmp92 = 4*tmp47
  tmp93 = 2*tmp36
  tmp94 = -10/tmp18
  tmp95 = -6*tmp1
  tmp96 = 24*tmp6
  tmp97 = tmp94 + tmp96
  tmp98 = tmp18**(-3)
  tmp99 = -4*tmp1
  tmp100 = 32*ss
  tmp101 = tmp58/tmp18
  tmp102 = 3*tmp36
  tmp103 = tmp101 + tmp102 + tmp60
  tmp104 = 15/tmp18
  tmp105 = -2*tmp42
  tmp106 = tmp37**2
  tmp107 = 1/tmp18 + tmp6
  tmp108 = 16*tmp42
  tmp109 = 16*tmp81
  tmp110 = tmp106*tmp42
  tmp111 = tmp107 + tmp43
  tmp112 = -8*tmp111*tmp32
  tmp113 = (-8*tmp107)/tmp4
  tmp114 = tmp106 + tmp113 + tmp66
  tmp115 = tmp11*tmp114
  tmp116 = 10*tmp40
  tmp117 = tmp107*tmp43
  tmp118 = tmp1 + tmp116 + tmp117 + tmp36
  tmp119 = (-2*ss*tmp118)/tmp4
  tmp120 = 30*tmp40
  tmp121 = tmp107*tmp61
  tmp122 = tmp1 + tmp108 + tmp120 + tmp121 + tmp36
  tmp123 = tmp122*tmp26
  tmp124 = 16*tmp32
  tmp125 = tmp28 + tmp5 + tmp7
  tmp126 = 8*tmp125*tmp26
  tmp127 = ss*tmp106
  tmp128 = -14*tmp40
  tmp129 = -8*ss*tmp107
  tmp130 = tmp1 + tmp128 + tmp129 + tmp36
  tmp131 = tmp130/tmp4
  tmp132 = tmp124 + tmp126 + tmp127 + tmp131
  tmp133 = (-2*tmp132)/tmp69
  tmp134 = tmp109 + tmp110 + tmp112 + tmp115 + tmp119 + tmp123 + tmp133
  tmp135 = tmp5 + tmp6
  tmp136 = 2*tmp47*tmp86
  tmp137 = tmp36*tmp86
  tmp138 = tmp136 + tmp137 + tmp88
  tmp139 = -7/tmp18
  tmp140 = -8*ss*tmp87
  tmp141 = -11*tmp6
  tmp142 = tmp104 + tmp141
  tmp143 = tmp142/tmp18
  tmp144 = 51*tmp6
  tmp145 = -9*tmp6
  tmp146 = tmp145 + 1/tmp18
  tmp147 = -5*tmp6
  tmp148 = tmp147 + 1/tmp18
  tmp149 = 27*tmp6
  tmp150 = tmp35 + tmp6
  tmp151 = 29*tmp6
  tmp152 = 7/tmp18
  tmp153 = ss*tmp84
  tmp154 = 5/tmp18
  tmp155 = tmp154 + tmp7
  tmp156 = tmp150/tmp18
  tmp157 = tmp104 + tmp6
  tmp158 = 9/tmp18
  tmp159 = -3*tmp1
  tmp160 = tmp102 + tmp116 + tmp159
  tmp161 = -tmp1
  tmp162 = -3*tmp36
  tmp163 = tmp104*tmp6
  tmp164 = -4/tmp18
  tmp165 = tmp4**(-5)
  tmp166 = 4/tmp18
  tmp167 = 14*tmp40
  tmp168 = 3*tmp1
  tmp169 = tmp166 + tmp82
  tmp170 = 14*tmp6
  tmp171 = tmp170 + 1/tmp18
  tmp172 = tmp1*tmp41
  tmp173 = 4*tmp1
  tmp174 = tmp82/tmp18
  tmp175 = 5*tmp36
  tmp176 = tmp159 + tmp167 + tmp175
  tmp177 = ss*tmp107
  tmp178 = tmp156 + tmp177
  tmp179 = tmp41 + tmp6
  tmp180 = tmp57/tmp18
  tmp181 = tmp1 + tmp175 + tmp180
  tmp182 = tmp164*tmp6
  tmp183 = 2*tmp177
  tmp184 = tmp156 + tmp183
  tmp185 = tmp6*tmp94
  tmp186 = 1/tmp56
  tmp187 = 8*tmp26
  tmp188 = -8*ss
  tmp189 = tmp179*tmp33
  tmp190 = 12*ss
  tmp191 = 32*tmp42
  tmp192 = 20*tmp40
  tmp193 = tmp108*tmp179
  tmp194 = 6*tmp40
  tmp195 = tmp154*tmp6
  tmp196 = 9*tmp36
  tmp197 = tmp164 + tmp43 + tmp6
  tmp198 = -2*tmp1
  tmp199 = 7*tmp1
  tmp200 = 12*tmp40
  tmp201 = 19*tmp36
  MP2MPL_MP_FIN_d10d23 = 16*D0IR6tsL*f2p*Pi*tmp1*tmp10*tmp106*tmp17*tmp18*tmp3 + 8*D0tsLN*f2p*Pi*tmp134*t&
                          &mp18*tmp25*tmp27*tmp3 + 8*D0tsNL*f2p*Pi*tmp134*tmp18*tmp25*tmp27*tmp3 + DB0sFP*f&
                          &2p*logmuF2*Pi*ss*tmp173*tmp3*tmp4 - 4*D0IR6tsN*f2p*Pi*tmp10*tmp17*tmp18*tmp186*t&
                          &mp27*tmp3*tmp4 + DB0sFP*f2p*logF2P2*Pi*tmp1*tmp28*tmp3*tmp4 + f2p*logmut*Pi*tmp1&
                          &61*tmp3*(tmp23 - 3*tmp29 + 6/tmp4)*tmp4 + C0tF2F20F0*f2p*Pi*tmp172*tmp22*tmp3*tm&
                          &p4*(4*tmp11 + tmp13/tmp18 - 2/(tmp18*tmp69)) - 2*C0tF2F2NFN*f2p*Pi*tmp22*tmp25*t&
                          &mp3*tmp31*tmp4*(ss*tmp36 + (tmp34*tmp38)/tmp18 - 8*tmp26*(tmp35 + tmp43) + (4*tm&
                          &p11)/tmp52 + tmp59 + (2*(40*tmp26 + tmp36 + (tmp164 + tmp33)/tmp4))/tmp69) + 16*&
                          &f2p*logmuL2*Pi*tmp1*tmp25*tmp3*(6*tmp26 + tmp40 + tmp38*(tmp35 + tmp57 + tmp58) &
                          &+ 1/(tmp4*tmp69)) + f2p*logbeN2*Pi*tmp10*tmp18*tmp22*tmp27*tmp3*tmp4*(-(tmp103/t&
                          &mp18) + (64*ss + 46/tmp18)*tmp26 - 64*tmp32 - 2*tmp11*(-15/tmp18 + tmp33 + 40/tm&
                          &p4) + (tmp166*(5*ss + tmp41))/tmp4 + 8*tmp65 - (4*(tmp105 + 46*tmp26 + tmp36 + (&
                          &-9/tmp18 + tmp43)/tmp4 + tmp47))/tmp69) + f2p*logmuN2*Pi*tmp25*tmp27*tmp3*tmp4*(&
                          &-16*tmp26 - 3*tmp29*tmp6 + (48*ss + 8/tmp18 + 6*tmp6)/tmp4 + (tmp30 + tmp6)/tmp6&
                          &9) - 2*D0tsNN*f2p*Pi*tmp25*tmp3*tmp31*tmp4*((-64*ss + 1/tmp18)*tmp26 + tmp11*(1/&
                          &tmp18 + tmp30) + 112*tmp32 - (2*ss*tmp34)/tmp4 + tmp42/tmp18 + (2*(64*tmp26 + tm&
                          &p46 + tmp63))/tmp69) + (8*f2p*logF2t*Pi*tmp1*tmp22*tmp3*tmp4*(-2*ss + tmp23 + tm&
                          &p5 + tmp64))/tmp69 + (f2p*logmuP2*Pi*tmp1*tmp25*tmp3*tmp4*(tmp1 + tmp66))/tmp69 &
                          &+ 8*C0tP2P2LP0*f2p*Pi*tmp1*tmp10*tmp18*tmp3*tmp37*tmp56*(tmp26*(tmp153 + tmp174 &
                          &- 49*tmp36 - 68*tmp47) - tmp178*tmp47 + tmp32*(90/tmp18 + tmp53) + tmp11*(-(tmp1&
                          &07/tmp18) + tmp155*tmp64) + (tmp184/tmp18 + 4*tmp157*tmp26 - (2*(tmp157/tmp18 + &
                          &tmp155*tmp28))/tmp4)/tmp69 + (tmp100*tmp36 + tmp155*tmp60 + tmp36*(tmp152 + tmp7&
                          &))/tmp4) + 2*f2p*logF2N2*Pi*tmp22*tmp25*tmp27*tmp3*tmp4*tmp69*(4*tmp11*(32*tmp26&
                          & - (2*(tmp33 + tmp35 + tmp57))/tmp4 + tmp29*tmp6) + 8*tmp65*(tmp7 + tmp70) - 8*t&
                          &mp13*tmp26*tmp71 + (tmp38*tmp75)/tmp69) + 2*f2p*logF2L2*Pi*tmp1*tmp22*tmp25*tmp3&
                          &*tmp69*((-128*tmp65)/tmp4 - 8*tmp11*((-8*(tmp28 + tmp37))/tmp4 + tmp37*tmp6 + tm&
                          &p66) + tmp13*tmp6*tmp70*tmp71 + (tmp6*tmp75)/tmp69) - 2*C0tP2P2NP0*f2p*Pi*tmp10*&
                          &tmp18*tmp27*tmp3*tmp4*(2*tmp32 - tmp26*(25/tmp18 + tmp43) + tmp11*(tmp5 + tmp64)&
                          & + tmp77 + (tmp60 + tmp61/tmp18 + tmp78)/tmp4 + (-4*tmp26 - (2*(tmp152 + tmp28))&
                          &/tmp4 + tmp79)/tmp69) + (C0tP2P20P0*f2p*Pi*tmp161*tmp3*tmp4*tmp56*(-((68*ss + 49&
                          &/tmp18)*tmp26) + 90*tmp32 + tmp11*(10/tmp4 + tmp5) + tmp77 + (60*tmp26 - (10*tmp&
                          &68)/tmp4 + tmp79)/tmp69 + (tmp100/tmp18 + tmp78 + tmp80)/tmp4))/tmp18 + (DB0tNN*&
                          &f2p*Pi*tmp22*tmp25*tmp3*tmp31*tmp4*tmp56*(-16*(tmp100 + 41/tmp18)*tmp32 - tmp103&
                          &*tmp36 + (ss*tmp166*(-4*ss + 1/tmp18))/tmp4 + (80*ss + 87/tmp18)*tmp26*tmp41 + 2&
                          &*tmp11*((tmp104 + tmp188)/tmp18 + 192*tmp26 + tmp30*(tmp158 + tmp43)) + (4*(400*&
                          &tmp32 - (tmp105 + tmp36 + tmp47)/tmp18 + (tmp108 + 33*tmp36 + 36*tmp47)/tmp4 - 2&
                          &*tmp26*(99/tmp18 + tmp61)))/tmp69 + 8*tmp65*(1/tmp18 + tmp70) + 512*tmp81))/2. +&
                          & C0tP2P2NPN*f2p*Pi*tmp25*tmp3*tmp31*tmp4*tmp56*(((228*ss + 97/tmp18)*tmp26)/tmp1&
                          &8 - 2*(192*ss + 221/tmp18)*tmp32 + (tmp11*(1/tmp18 - 10/tmp4))/tmp18 + ((-220*tm&
                          &p26)/tmp18 + 384*tmp32 + ((10*ss + 23/tmp18)*tmp64)/tmp18 - tmp36*tmp68)/tmp69 +&
                          & ss*tmp36*tmp76 + (tmp38*(48*tmp47 + tmp78 + tmp80))/tmp18 + 640*tmp81) + (32*C0&
                          &tF2F2LFL*f2p*Pi*tmp1*tmp22*tmp25*tmp3*(tmp172 + ss*tmp180 + tmp1*tmp28 + tmp12*t&
                          &mp36 + 4*tmp11*tmp37 + (tmp198 + tmp36 + tmp53/tmp18)/tmp4 - (2*(tmp36 + tmp67 +&
                          & tmp6*(tmp28 - 2/tmp4 + tmp82)))/tmp69))/tmp4 + (f2p*logbeL2*Pi*tmp10*tmp135*tmp&
                          &173*tmp18*tmp3*tmp56*(tmp138/tmp18 + 2*(tmp100*tmp150 + (tmp144 + 73/tmp18)/tmp1&
                          &8)*tmp26 - 32*tmp32*(tmp154 + tmp57) + 8*(16/tmp4 + tmp5 - 3*tmp6)*tmp65 - 2*tmp&
                          &11*(tmp140 + tmp143 + 128*tmp26 + (tmp139 + tmp6 + tmp61)*tmp70) + (4*(-2*(tmp10&
                          &0 + tmp149 + 17/tmp18)*tmp26 + tmp148*tmp36 + ((tmp151 + tmp152)/tmp18 + tmp150*&
                          &tmp190 + tmp191)/tmp4 + tmp146*tmp47 + tmp59 + tmp105*tmp87))/tmp69 + (tmp164*((&
                          &10*tmp107)/tmp18 + ss*(21/tmp18 + 11*tmp6) + tmp89))/tmp4))/(1/tmp18 - 4/tmp69) &
                          &+ (f2p*logP2t*Pi*tmp1*tmp3*tmp4*tmp56*((-46*tmp26)/tmp18 + tmp11*(-32/tmp4 + tmp&
                          &41) + tmp59 + (tmp162 - 6*tmp47 + tmp60)/tmp18 + (4*((tmp12 + 1/tmp18)/tmp18 + t&
                          &mp63))/tmp69 + tmp19*(-5*tmp36 + ss*tmp72 + tmp89)))/2. - 4*DB0tLN*f2p*Pi*tmp22*&
                          &tmp25*tmp27*tmp3*tmp56*(-(tmp138/tmp18) - 8*tmp32*(tmp100 + 31/tmp18 - 8*tmp6) +&
                          & 2*tmp11*(tmp140 + tmp143 + 160*tmp26 + (-25/tmp18 + tmp33 + tmp57)*tmp8) + 256*&
                          &tmp81 + (tmp166*(11*tmp177 + tmp60 + tmp154*tmp83))/tmp4 - 2*tmp26*((tmp139 + tm&
                          &p144)/tmp18 + tmp33*(1/tmp18 + tmp84)) + 8*tmp65*(tmp19 + tmp87) + (4*(184*tmp32&
                          & + 2*tmp26*(tmp149 - 41/tmp18 + tmp33) - tmp148*tmp36 - tmp146*tmp47 + tmp88 + t&
                          &mp38*((tmp151 - 13/tmp18)/tmp18 + tmp190*tmp6 + tmp89)))/tmp69) - (4*C0tF2F2NF0*&
                          &f2p*Pi*tmp10*tmp18*tmp22*tmp27*tmp3*tmp4*(tmp13*tmp36 + 4*tmp11*(1/tmp18 + tmp64&
                          &) + (tmp187 + tmp19*tmp29 + tmp90)/tmp69))/tmp52 + 16*C0tF2F2LF0*f2p*Pi*tmp1*tmp&
                          &10*tmp18*tmp22*tmp3*tmp37*(-2*tmp11*tmp179 + tmp36*tmp39 + (tmp40 + tmp39*tmp57 &
                          &+ tmp93)/tmp69) - 16*C0tF2F2NFL*f2p*Pi*tmp18*tmp22*tmp25*tmp27*tmp3*(-2*tmp11*(t&
                          &mp1 + tmp40 + tmp66 + tmp54*tmp8 + tmp90) + (tmp135*tmp47 - 4*tmp26*tmp83 + (tmp&
                          &36 + tmp152*tmp6 + tmp91 + tmp92)/tmp4)/tmp18 + (-32*tmp32 + tmp187*(tmp35 + tmp&
                          &43 + tmp57) - (2*(tmp1 + tmp163 + tmp90 + tmp91 + tmp92))/tmp4 - tmp37*(tmp40 + &
                          &ss*tmp57 + tmp93))/tmp69) + (f2p*log4*Pi*tmp18*tmp25*tmp27*tmp3*tmp4*tmp56*(896*&
                          &tmp165 - 2*tmp26*((tmp102 + tmp161 + tmp194)*tmp33 + (tmp144 + 28/tmp18)*tmp36 +&
                          & (96*tmp42)/tmp18) + ((tmp102 + tmp161 + tmp167)*tmp36 + 4*tmp160*tmp42 - 2*(tmp&
                          &168 + tmp195 + tmp36)*tmp47)*tmp64 + 2*tmp11*((-96*tmp26)/tmp18 + 192*tmp32 - 7*&
                          &tmp36*tmp6 + tmp160*tmp8) - 64*tmp81*(20*ss + tmp158 + tmp84) + 8*tmp32*(tmp1 + &
                          &tmp120 + 27*tmp36 + 48*tmp42 + tmp33*(tmp158 + tmp84)) + (2*((tmp160*tmp188 + (t&
                          &mp1 + tmp102 + tmp163)*tmp41)/tmp4 + tmp187*(tmp1 + tmp162 + 24*tmp47) + tmp36*(&
                          &14*ss + tmp139 + tmp57)*tmp6 + 384*tmp81 - 32*tmp32*(tmp190 + tmp35 + tmp84)))/t&
                          &mp69 + tmp36*tmp7*(tmp102 + tmp153 + 14*tmp42 + ss*tmp94)))/2. + DB0tLL*f2p*Pi*t&
                          &mp1*tmp25*tmp3*tmp70*(tmp166 + tmp164*tmp22*tmp51 + 8*tmp22*tmp49*tmp51 - 6*tmp4&
                          &9**2*tmp54*tmp56 + 8*tmp49*tmp52*(tmp15 + tmp5 - 1/tmp69) + tmp166*tmp52*(tmp12 &
                          &+ 1/tmp18 + tmp38 + 1/tmp69) + (2*tmp52*tmp54)/tmp69 + tmp41*tmp49*tmp56*(1/tmp1&
                          &8 - 6*tmp6 + tmp70) + 2*(-6/tmp4 + tmp58 - 2/tmp69 + tmp83) + (tmp56*(tmp83/tmp1&
                          &8 + (tmp84 + tmp94)/tmp4))/tmp18) + 8*C0tP2P2NPL*f2p*Pi*tmp18*tmp25*tmp27*tmp3*t&
                          &mp56*(32*tmp165 - tmp26*(tmp193 + (tmp168 + 81*tmp36 - 28*tmp40)/tmp18 + (tmp1 +&
                          & 45*tmp36 - 6*tmp40)*tmp43) - tmp178*tmp37*tmp47 + 2*tmp32*(tmp1 + tmp108 + 157*&
                          &tmp36 - 22*tmp40 + (tmp158 + tmp6)*tmp61) + (tmp33*tmp36*(tmp154 + tmp53) + tmp1&
                          &81*tmp60 + tmp36*(tmp1 + tmp182 + tmp78))/tmp4 - 16*(26/tmp18 + tmp43 + tmp6)*tm&
                          &p81 - (-4*tmp26*(tmp128 + tmp161 + tmp189 + 39*tmp36) - (tmp184*tmp37)/tmp18 + (&
                          &(tmp161 + tmp185 + tmp201)*tmp41 + tmp181*tmp43)/tmp4 + tmp59*(6/tmp18 + tmp28 +&
                          & tmp7) + 64*tmp81)/tmp69 + tmp11*(tmp1/tmp18 - 16*tmp179*tmp26 + tmp59 + tmp181*&
                          &tmp64 - tmp98)) + f2p*logN2L2*Pi*tmp18*tmp22*tmp25*tmp27*tmp3*tmp4*tmp56*tmp69*(&
                          &(-32*(tmp187 - (2*tmp107)/tmp4 + tmp40))/(tmp52*tmp69**4) - (16*tmp13*tmp186*tmp&
                          &26*tmp71)/tmp18 + 2*tmp11*(2048*tmp165 + tmp187*(tmp193 + (15*tmp1 + tmp195 + tm&
                          &p196)/tmp18 + (tmp1 + tmp194 - 4*tmp36)*tmp43) - 16*tmp32*(14*tmp1 + tmp191 + tm&
                          &p192 + 31*tmp36 + tmp43*(tmp154 + 8*tmp6)) + 128*tmp81*(tmp190 + tmp41 + tmp82) &
                          &+ tmp36*tmp6*(tmp108 + (tmp164 + tmp6)/tmp18 + ss*(-8/tmp18 + tmp84)) + ((-17*tm&
                          &p1 + tmp192 + tmp36)/tmp18 + tmp43*(tmp159 + tmp166*tmp6 + tmp78) - 32*tmp42*tmp&
                          &87)/(tmp18*tmp4)) + 8*tmp65*(tmp124*(-5/tmp18 + tmp61) + tmp36*tmp6*(tmp188 + tm&
                          &p35 + tmp7) + 128*tmp81 + (tmp168 - tmp36 - 18*tmp40 + tmp61*tmp87)/(tmp18*tmp4)&
                          & - 8*tmp26*(tmp1 + tmp189 + tmp6*tmp72 + tmp90)) + (2048/tmp4**6 + tmp164*tmp26*&
                          &(tmp193 + (16*tmp1 + tmp196 + 11*tmp40)/tmp18 + ss*(20*tmp1 - 72*tmp36 + 32*tmp4&
                          &0)) - 512*tmp165*(tmp154 + tmp43) + 64*(tmp147/tmp18 + tmp190/tmp18 + tmp198 + t&
                          &mp78)*tmp81 + (tmp36*(-2*ss*(-7*tmp1 + tmp200 + tmp201) + (tmp180 + tmp199 + tmp&
                          &36)/tmp18 + tmp108*tmp87))/tmp4 + tmp124*(tmp108/tmp18 + (11*tmp1 + tmp167 + tmp&
                          &196)/tmp18 + ss*(8*tmp1 - 38*tmp36 + tmp96/tmp18)) + ss*tmp197*tmp53*tmp98)/tmp6&
                          &9) + DB0sFPlogmut*f2p*Pi*ss*tmp3*tmp4*tmp99 + (32*D0tsLL*f2p*Pi*tmp1*tmp25*tmp3*&
                          &(-(tmp11*tmp45) - tmp26*tmp45 + ss*(6*tmp1 + tmp153 + tmp46) + (2*(tmp159 + tmp4&
                          &5/tmp4 + tmp47 + tmp48))/tmp69 + tmp64*(tmp47 + tmp48 + tmp99)))/tmp4 + f2p*logP&
                          &2L2*Pi*tmp1*tmp18*tmp25*tmp3*tmp4*tmp56*(ss*tmp197*tmp198*tmp36 - 64*tmp165*(tmp&
                          &166 - 7*tmp6) + tmp38*((tmp1 + tmp182 + tmp196)*tmp36 - 4*tmp176*tmp42 + 2*(tmp1&
                          &68 + 13*tmp36 + 8*tmp40)*tmp47)*tmp6 + 4*tmp11*(tmp124*tmp169 + tmp164*tmp171*tm&
                          &p26 + tmp198*tmp36 + ((14*tmp1)/tmp18 - 3*tmp27 + tmp175*tmp6)/tmp4) + tmp109*(t&
                          &mp185 + 23*tmp36 - 8*(tmp1 + ss*tmp85)) + (2*(-16*tmp32*(tmp102 + tmp173 + tmp17&
                          &4 + tmp169*tmp43) + ((-4*ss*tmp176 + (tmp1 + tmp200 + 11*tmp36)/tmp18)*tmp6)/tmp&
                          &4 + tmp1*tmp36*(tmp164 + tmp33 + tmp6) + 64*tmp81*(tmp41 + tmp82) + 4*tmp26*(ss*&
                          &tmp166*tmp171 + tmp172 + tmp27 + tmp147*tmp36 - 2*tmp98)))/tmp69 - 4*tmp26*(tmp3&
                          &6*(tmp199 - 6*tmp36 - 13*tmp40) + tmp166*tmp171*tmp42 - 2*ss*(tmp27 + tmp102*tmp&
                          &6 + tmp95/tmp18 + 6*tmp98)) + 4*tmp32*(tmp108*tmp169 + (26*tmp1)/tmp18 + tmp27 -&
                          & 19*tmp36*tmp6 - 40*tmp98 + tmp188*(tmp102 + tmp145/tmp18 + tmp99))) + (16*C0tP2&
                          &P2LPL*f2p*Pi*tmp1*tmp25*tmp3*tmp56*(tmp161*tmp36 + tmp36*tmp42 + tmp36*tmp48 + t&
                          &mp32*(-90/tmp18 + 184*tmp6) + tmp42*tmp95 + tmp47*tmp95 + tmp26*(49*tmp36 - 104*&
                          &tmp40 + 68*tmp47 - 144*ss*tmp6 + tmp95) + tmp11*(tmp36 + tmp95 + tmp97/tmp4) + (&
                          &(tmp173 - 7*tmp36 + 16*tmp40)/tmp18 + ss*(12*tmp1 - 32*tmp36 + 64*tmp40) + tmp42&
                          &*tmp97)/tmp4 + 3*ss*tmp98 + (tmp173/tmp18 + tmp1*tmp190 + tmp26*(-60/tmp18 + 112&
                          &*tmp6) + tmp36*tmp84 + ss*tmp90 - 3*tmp98 + (30*tmp36 - 56*tmp40 + 20*tmp47 - 48&
                          &*ss*tmp6 + tmp99)/tmp4)/tmp69))/tmp4

  END FUNCTION MP2MPL_MP_FIN_D10D23


  FUNCTION MP2MPL_MP_SIN_D10D20(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_SIN_d10d20
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9

  tmp1 = mm**2
  tmp2 = me**4
  tmp3 = -ss
  tmp4 = tmp1 + tmp3
  tmp5 = ss**2
  tmp6 = me**2
  tmp7 = mm**4
  tmp8 = 2*ss*tt
  tmp9 = tt**2
  MP2MPL_MP_SIN_d10d20 = (16*DB0sFP*Pi*(-4*me**6*ss - tmp2*(12*ss*tmp1 - 12*tmp5) - tmp4*(-8*tmp1*tmp5 + &
                          &4*ss*tmp7 + 2*ss*(2*tmp5 + tmp8 + tmp9)) - tmp6*(-24*tmp1*tmp5 + 12*ss*tmp7 + 2*&
                          &ss*(6*tmp5 + tmp8 + tmp9))))/((tmp2 + tmp4**2 - 2*(ss + tmp1)*tmp6)*tmp9)

  END FUNCTION MP2MPL_MP_SIN_D10D20

  FUNCTION MP2MPL_MP_SIN_D11D20(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_SIN_d11d20
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50

  tmp1 = mm**2
  tmp2 = sqrt(lambda)**2
  tmp3 = me**4
  tmp4 = 1 + kappa
  tmp5 = tmp2*tmp4
  tmp6 = 3 + tmp5
  tmp7 = sqrt(lambda)**4
  tmp8 = ss**2
  tmp9 = 3 + tmp2
  tmp10 = tmp9*tt
  tmp11 = -ss
  tmp12 = tmp1 + tmp11
  tmp13 = -3*ss*tmp2
  tmp14 = -2*ss*tmp7
  tmp15 = 3*ss*tt
  tmp16 = ss*tmp2*tt
  tmp17 = tmp13 + tmp14 + tmp15 + tmp16
  tmp18 = mm**4
  tmp19 = -3*tmp2
  tmp20 = -2*tmp7
  tmp21 = 3*tt
  tmp22 = tmp2*tt
  tmp23 = tmp19 + tmp20 + tmp21 + tmp22
  tmp24 = -tmp5
  tmp25 = -3 + tmp24
  tmp26 = tmp2*tmp25
  tmp27 = tmp10 + tmp26
  tmp28 = -(tmp2*tmp6)
  tmp29 = tmp10 + tmp28
  tmp30 = tt**2
  tmp31 = me**2
  tmp32 = mm**6
  tmp33 = 3*ss*tmp2
  tmp34 = 2*ss*tmp7
  tmp35 = -3*ss*tt
  tmp36 = tmp11*tmp22
  tmp37 = tmp33 + tmp34 + tmp35 + tmp36
  tmp38 = 4*ss*tmp27*tt
  tmp39 = 4*tmp2*tmp6
  tmp40 = -4*tmp10
  tmp41 = tmp39 + tmp40
  tmp42 = ss*tmp41*tt
  tmp43 = ss**3
  tmp44 = 4*tmp1
  tmp45 = -tt
  tmp46 = tmp44 + tmp45
  tmp47 = -tmp2
  tmp48 = tmp47 + tt
  tmp49 = me**6
  tmp50 = 2*ss*tt
  MP2MPL_MP_SIN_d11d20 = (16*DB0sFP*Pi*(f1p*tmp46*tmp48*(-4*ss*tmp49 - tmp3*(12*ss*tmp1 - 12*tmp8) - tmp1&
                          &2*(4*ss*tmp18 - 8*tmp1*tmp8 + 2*ss*(tmp30 + tmp50 + 2*tmp8)) - tmp31*(12*ss*tmp1&
                          &8 - 24*tmp1*tmp8 + 2*ss*(tmp30 + tmp50 + 6*tmp8))) - 3*tmp3*(16*tmp18*tmp37 + tm&
                          &p1*(tmp38 + 16*tmp23*tmp8) - 4*tmp29*tmp8*tt) + tmp49*(16*tmp1*tmp17 + tt*(ss*tm&
                          &p39 + (-12*ss - 4*ss*tmp2)*tt)) + tmp12*(16*tmp17*tmp32 + ss*(tmp42 + tmp30*(-2*&
                          &tmp10 + 2*tmp2*tmp6) - 4*tmp29*tmp8)*tt + tmp18*(-32*tmp23*tmp8 - 4*ss*tmp27*tt)&
                          & + tmp1*(8*ss*tmp23*tmp30 + 16*tmp23*tmp43 + 4*(6*tmp10 - 2*tmp2*(9 + (5 + kappa&
                          &)*tmp2))*tmp8*tt)) + tmp31*(-48*tmp32*tmp37 - 3*tmp18*(tmp38 + 32*tmp23*tmp8) + &
                          &ss*(tmp42 - 12*tmp29*tmp8 + 2*tmp30*(3*tmp2 + tmp4*tmp7 + tmp45*tmp9))*tt + 2*tm&
                          &p1*(4*ss*tmp23*tmp30 + 24*tmp23*tmp43 + 2*(10*tmp10 - 2*tmp2*(15 + (7 + 3*kappa)&
                          &*tmp2))*tmp8*tt))))/(tmp30*(tmp12**2 + tmp3 - 2*(ss + tmp1)*tmp31)*tmp46*tmp48)

  END FUNCTION MP2MPL_MP_SIN_D11D20

  FUNCTION MP2MPL_MP_SIN_D12D20(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_SIN_d12d20
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83

  tmp1 = mm**2
  tmp2 = sqrt(lambda)**2
  tmp3 = sqrt(lambda)**4
  tmp4 = me**4
  tmp5 = 1 + kappa
  tmp6 = 4*tmp2
  tmp7 = 3 + tmp3 + tmp6
  tmp8 = ss**2
  tmp9 = 2*tmp2
  tmp10 = 3 + tmp9
  tmp11 = tmp10*tt
  tmp12 = me**2
  tmp13 = ss*tmp6*tmp7
  tmp14 = -12*ss
  tmp15 = -8*ss*tmp2
  tmp16 = tmp14 + tmp15
  tmp17 = tmp16*tt
  tmp18 = tmp13 + tmp17
  tmp19 = mm**4
  tmp20 = -(tmp2*tmp7)
  tmp21 = tmp11 + tmp20
  tmp22 = -2*tmp2*tmp5
  tmp23 = -(kappa*tmp3)
  tmp24 = -3 + tmp22 + tmp23
  tmp25 = tmp2*tmp24
  tmp26 = tmp11 + tmp25
  tmp27 = 4*ss*tmp26*tt
  tmp28 = tmp5*tmp9
  tmp29 = kappa*tmp3
  tmp30 = 3 + tmp28 + tmp29
  tmp31 = -(tmp2*tmp30)
  tmp32 = tmp11 + tmp31
  tmp33 = tt**2
  tmp34 = 4*tmp11
  tmp35 = 3*kappa
  tmp36 = -ss
  tmp37 = tmp1 + tmp36
  tmp38 = mm**6
  tmp39 = -4*ss*tmp2*tmp7
  tmp40 = 12*ss*tt
  tmp41 = 8*ss*tmp2*tt
  tmp42 = tmp39 + tmp40 + tmp41
  tmp43 = 3*tmp2
  tmp44 = 2*tmp3*tmp5
  tmp45 = sqrt(lambda)**6
  tmp46 = kappa*tmp45
  tmp47 = -tmp11
  tmp48 = tmp43 + tmp44 + tmp46 + tmp47
  tmp49 = ss**3
  tmp50 = me**6
  tmp51 = tmp2*tmp5
  tmp52 = 2 + tmp51
  tmp53 = 1 + tmp2
  tmp54 = 2 + tmp2
  tmp55 = tmp54*tt
  tmp56 = -4*ss*tmp2*tmp53
  tmp57 = 4*ss*tt
  tmp58 = ss*tmp9*tt
  tmp59 = tmp56 + tmp57 + tmp58
  tmp60 = -tmp55
  tmp61 = tmp2*tmp52
  tmp62 = tmp60 + tmp61
  tmp63 = tmp2 + tmp3
  tmp64 = -2*tmp63
  tmp65 = tmp55 + tmp64
  tmp66 = 5 + kappa
  tmp67 = 12*ss*tmp2*tmp53
  tmp68 = -6*ss*tmp2
  tmp69 = tmp14 + tmp68
  tmp70 = tmp69*tt
  tmp71 = tmp67 + tmp70
  tmp72 = 2*tmp61
  tmp73 = -2*tmp55
  tmp74 = tmp72 + tmp73
  tmp75 = ss*tmp74*tt
  tmp76 = tmp3*tmp5
  tmp77 = tmp60 + tmp76 + tmp9
  tmp78 = -tmp61
  tmp79 = tmp55 + tmp78
  tmp80 = -4*tmp2*tmp53
  tmp81 = 2*tmp55
  tmp82 = tmp80 + tmp81
  tmp83 = 7 + tmp35
  MP2MPL_MP_SIN_d12d20 = (16*DB0sFP*Pi*(3*tmp4*(4*tmp18*tmp19 + tmp1*(tmp27 + 16*tmp21*tmp8) + 4*tmp48*tm&
                          &p8*tt) + tmp50*(-4*tmp1*tmp42 + tt*(-4*ss*tmp2*tmp30 + (12*ss + 8*ss*tmp2)*tt)) &
                          &+ tmp12*(12*tmp18*tmp38 + 3*tmp19*(tmp27 + 32*tmp21*tmp8) + ss*tt*(2*tmp32*tmp33&
                          & + 12*tmp32*tmp8 + ss*(-4*tmp2*tmp30 + tmp34)*tt) - 2*tmp1*(24*tmp21*tmp49 + ss*&
                          &tmp33*(tmp34 - 4*tmp2*tmp7) + 2*tmp8*(10*tmp11 - 2*tmp2*(15 + tmp3*(2 + tmp35) +&
                          & tmp83*tmp9))*tt)) + 2*f1p*(tmp50*(4*tmp1*tmp59 + tt*(ss*tmp72 + (-4*ss - 2*ss*t&
                          &mp2)*tt)) + tmp4*(-4*tmp19*tmp71 + 6*tmp79*tmp8*tt + tmp1*(-24*tmp65*tmp8 - 6*ss&
                          &*(tmp2*(-2 - tmp51) + tmp55)*tt)) + tmp37*(4*tmp38*tmp59 + ss*(tmp33*tmp62 + tmp&
                          &75 + 2*tmp77*tmp8)*tt + tmp19*(-16*tmp65*tmp8 + 2*ss*tmp62*tt) + tmp1*(8*tmp49*t&
                          &mp65 + 2*ss*tmp33*tmp82 + 4*(3*tmp55 + tmp2*(-6 - tmp2*tmp66))*tmp8*tt)) + tmp12&
                          &*(-4*tmp38*tmp71 + ss*(tmp75 + tmp33*tmp77 - 6*tmp79*tmp8)*tt + tmp19*(-48*tmp65&
                          &*tmp8 + 2*ss*(-3*tmp55 + 3*tmp61)*tt) + 2*tmp1*(12*tmp49*tmp65 + ss*tmp33*tmp82 &
                          &+ 2*tmp8*(5*tmp55 + tmp2*(-10 - tmp2*tmp83))*tt))) + tmp37*(-4*tmp38*tmp42 - tmp&
                          &19*(-32*tmp21*tmp8 - 4*ss*tmp26*tt) + tmp36*tt*(4*tmp48*tmp8 + ss*(-4*tmp11 + tm&
                          &p30*tmp6)*tt + tmp33*(tmp30*tmp9 + (-6 - 4*tmp2)*tt)) - tmp1*(16*tmp21*tmp49 + 4&
                          &*tmp8*tt*(-2*tmp2*(9 + (2 + kappa)*tmp3 + tmp66*tmp9) + 3*(6 + tmp6)*tt) + 8*ss*&
                          &tmp33*(-3*tmp2 - 4*tmp3 - tmp45 + 3*tt + tmp9*tt)))))/(tmp33*(-2*(ss + tmp1)*tmp&
                          &12 + tmp37**2 + tmp4)*(4*tmp1 - tt)*(-tmp2 + tt))

  END FUNCTION MP2MPL_MP_SIN_D12D20

  FUNCTION MP2MPL_MP_SIN_D13D20(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_SIN_d13d20
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39

  tmp1 = mm**2
  tmp2 = sqrt(lambda)**2
  tmp3 = me**4
  tmp4 = 1 + tmp2
  tmp5 = -tt
  tmp6 = sqrt(lambda)**3
  tmp7 = sqrt(lambda) + tmp6
  tmp8 = tmp7**2
  tmp9 = ss**2
  tmp10 = sqrt(lambda)**4
  tmp11 = kappa*tmp2
  tmp12 = 1 + tmp11
  tmp13 = -ss
  tmp14 = tmp1 + tmp13
  tmp15 = -4*ss*tmp8
  tmp16 = 4*ss*tt
  tmp17 = tmp16*tmp2
  tmp18 = tmp15 + tmp16 + tmp17
  tmp19 = kappa*tmp10
  tmp20 = tmp19 + tmp2 + tmp5
  tmp21 = mm**4
  tmp22 = tmp10 + tmp2 + tmp5
  tmp23 = tt**2
  tmp24 = me**2
  tmp25 = mm**6
  tmp26 = 12*ss*tmp8
  tmp27 = ss*tmp2
  tmp28 = ss + tmp27
  tmp29 = -12*tmp28*tt
  tmp30 = tmp26 + tmp29
  tmp31 = 4*tmp12*tmp2*tmp4
  tmp32 = -4*tmp4*tt
  tmp33 = tmp31 + tmp32
  tmp34 = ss*tmp33*tt
  tmp35 = ss**3
  tmp36 = tmp4**2
  tmp37 = -4*tmp2*tmp36
  tmp38 = 4*tmp4*tt
  tmp39 = tmp37 + tmp38
  MP2MPL_MP_SIN_d13d20 = (16*DB0sFP*(-1 + f1p)*Pi*(-(me**6*(4*tmp1*tmp18 + tt*(ss*tmp31 - 4*ss*tt - 4*tmp&
                          &27*tt))) - tmp3*(-4*tmp21*tmp30 - 12*tmp20*tmp4*tmp9*tt + tmp1*(48*tmp22*tmp4*tm&
                          &p9 - 12*ss*tt*(-(tmp12*tmp2*tmp4) + tmp4*tt))) - tmp14*(4*tmp18*tmp25 + tmp21*(t&
                          &mp16*(tmp12*tmp2*tmp4 + tmp4*tmp5) + 32*tmp22*tmp4*tmp9) + ss*tt*(tmp34 + 4*tmp2&
                          &0*tmp4*tmp9 + tmp23*(2*tmp12*tmp2*tmp4 - 2*tmp4*tt)) + tmp1*(2*ss*tmp23*tmp39 - &
                          &16*tmp22*tmp35*tmp4 + 4*tmp9*tt*(-2*tmp2*(3 + (2 + kappa)*tmp2)*tmp4 + 6*tmp4*tt&
                          &))) - tmp24*(-4*tmp25*tmp30 + ss*(tmp34 + 2*tmp20*tmp23*tmp4 + 12*tmp20*tmp4*tmp&
                          &9)*tt + tmp21*(96*tmp22*tmp4*tmp9 - 4*ss*tt*(-3*tmp12*tmp2*tmp4 + 3*tmp4*tt)) + &
                          &2*tmp1*(ss*tmp23*tmp39 - 24*tmp22*tmp35*tmp4 + 2*tmp9*tt*(-2*tmp2*(5 + (2 + 3*ka&
                          &ppa)*tmp2)*tmp4 + 10*tmp4*tt)))))/(tmp23*(tmp14**2 - 2*(ss + tmp1)*tmp24 + tmp3)&
                          &*(4*tmp1 + tmp5)*(-tmp2 + tt))

  END FUNCTION MP2MPL_MP_SIN_D13D20

  FUNCTION MP2MPL_MP_SIN_D10D21(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_SIN_d10d21
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11

  tmp1 = mm**2
  tmp2 = me**2
  tmp3 = -ss
  tmp4 = 4*tmp1
  tmp5 = -tt
  tmp6 = tmp4 + tmp5
  tmp7 = sqrt(lambda)**2
  tmp8 = -tmp7
  tmp9 = tmp8 + tt
  tmp10 = me**4
  tmp11 = tmp1 + tmp3
  MP2MPL_MP_SIN_d10d21 = (16*DB0sFP*Pi*(-8*(-1 + kappa)*sqrt(lambda)**4*ss*tmp1*(tmp11 + tmp2)*(2*tmp2 + tt) + &
                          &f2p*tmp6*tmp9*(-4*ss*tmp10 - 2*ss*tmp11*tt - 2*tmp2*(-2*ss**2 + 2*ss*tmp1 + ss*t&
                          &t))))/((tmp10 + tmp11**2 - 2*(ss + tmp1)*tmp2)*tmp6*tmp9*tt)

  END FUNCTION MP2MPL_MP_SIN_D10D21

  FUNCTION MP2MPL_MP_SIN_D11D21(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_SIN_d11d21
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14

  tmp1 = mm**2
  tmp2 = sqrt(lambda)**2
  tmp3 = me**2
  tmp4 = -ss
  tmp5 = tmp1 + tmp4
  tmp6 = sqrt(lambda)**4
  tmp7 = me**4
  tmp8 = 1 + tmp2
  tmp9 = 1 + kappa
  tmp10 = tmp2*tmp9
  tmp11 = 2 + tmp10
  tmp12 = tmp6*tmp9
  tmp13 = 2 + tmp2
  tmp14 = -(tmp13*tt)
  MP2MPL_MP_SIN_d11d21 = (32*DB0sFP*Pi*(4*(-1 + kappa)*ss*tmp1*(-f1p + tmp13)*(tmp3 + tmp5)*tmp6*(2*tmp3 &
                          &+ tt) + f2p*(tmp5*tt*(ss*(tmp12 + tmp14 + 2*tmp2)*tt + tmp1*(-8*ss*tmp2*tmp8 + 8&
                          &*ss*tt + 4*ss*tmp2*tt)) + tmp7*(4*tmp1*(-4*ss*tmp2*tmp8 + 4*ss*tt + 2*ss*tmp2*tt&
                          &) + tt*(2*ss*tmp11*tmp2 + (-4*ss - 2*ss*tmp2)*tt)) + tmp3*(-8*mm**4*(2*ss*tmp2*t&
                          &mp8 + (-2*ss + tmp2*tmp4)*tt) + ss*tt*(-2*ss*tmp11*tmp2 + (4*ss + tmp12 + 2*(1 +&
                          & ss)*tmp2)*tt - tmp13*tt**2) - 2*tmp1*(ss*(tmp14 - tmp2*(-2 + (-3 + kappa)*tmp2)&
                          &)*tt + 4*ss**2*(-2*(tmp2 + tmp6) + tmp13*tt))))))/((-2*(ss + tmp1)*tmp3 + tmp5**&
                          &2 + tmp7)*(4*tmp1 - tt)*tt*(-tmp2 + tt))

  END FUNCTION MP2MPL_MP_SIN_D11D21

  FUNCTION MP2MPL_MP_SIN_D12D21(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_SIN_d12d21
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17

  tmp1 = mm**2
  tmp2 = sqrt(lambda)**2
  tmp3 = me**2
  tmp4 = -ss
  tmp5 = me**4
  tmp6 = 1 + tmp2
  tmp7 = tmp1 + tmp4
  tmp8 = sqrt(lambda)**4
  tmp9 = -tt
  tmp10 = sqrt(lambda)**3
  tmp11 = sqrt(lambda) + tmp10
  tmp12 = tmp11**2
  tmp13 = ss*tmp2
  tmp14 = ss + tmp13
  tmp15 = kappa*tmp2
  tmp16 = 1 + tmp15
  tmp17 = 4*ss
  MP2MPL_MP_SIN_d12d21 = (16*DB0sFP*Pi*(8*(-1 + f1p)*(-1 + kappa)*ss*tmp1*tmp6*(tmp3 + tmp7)*tmp8*(2*tmp3&
                          & + tt) - f2p*(tmp7*tt*(2*ss*tmp6*(tmp2 + kappa*tmp8 + tmp9)*tt + tmp1*(-8*ss*tmp&
                          &12 + 8*tmp14*tt)) + tmp5*(tt*(tmp16*tmp17*tmp2*tmp6 - 4*ss*tt - 4*tmp13*tt) + 4*&
                          &tmp1*(-4*ss*tmp12 + tmp17*tt + tmp17*tmp2*tt)) + tmp3*(-8*mm**4*(2*ss*tmp12 - 2*&
                          &tmp14*tt) + ss*tt*(-4*tmp13*tmp16*tmp6 + (2*kappa*sqrt(lambda)**6 + tmp17 + (2 + tmp17&
                          &)*tmp2 + 2*(1 + kappa)*tmp8)*tt - 2*tmp6*tt**2) - 2*tmp1*(-8*ss**2*tmp6*(tmp2 + &
                          &tmp8 + tmp9) - 2*ss*tt*(tmp2*(-1 + (-2 + kappa)*tmp2)*tmp6 + tmp6*tt))))))/((-2*&
                          &(ss + tmp1)*tmp3 + tmp5 + tmp7**2)*(4*tmp1 + tmp9)*tt*(-tmp2 + tt))

  END FUNCTION MP2MPL_MP_SIN_D12D21

  FUNCTION MP2MPL_MP_SIN_D10D22(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_SIN_d10d22
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5

  tmp1 = mm**2
  tmp2 = -ss
  tmp3 = me**2
  tmp4 = me**4
  tmp5 = ss + tt
  MP2MPL_MP_SIN_d10d22 = (64*DB0sFP*f2p*(-1 + kappa)*sqrt(lambda)**4*Pi*ss*(tmp1 + tmp2 + tmp3)*(mm**4 + tmp4 +&
                          & ss*tmp5 - 2*tmp1*tmp5 - tmp3*(2*ss + 2*tmp1 + tt)))/(((tmp1 + tmp2)**2 - 2*(ss &
                          &+ tmp1)*tmp3 + tmp4)*(4*tmp1 - tt)*tt*(-sqrt(lambda)**2 + tt))

  END FUNCTION MP2MPL_MP_SIN_D10D22

  FUNCTION MP2MPL_MP_SIN_D11D22(me2, mm2, ss, tt)
  implicit none
  real MP2MPL_MP_SIN_d11d22
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6

  tmp1 = mm**2
  tmp2 = -ss
  tmp3 = me**2
  tmp4 = sqrt(lambda)**2
  tmp5 = me**4
  tmp6 = ss + tt
  MP2MPL_MP_SIN_d11d22 = (-64*DB0sFP*f2p*(-1 + kappa)*sqrt(lambda)**4*Pi*ss*(tmp1 + tmp2 + tmp3)*(1 + tmp4)*(mm&
                          &**4 + tmp5 + ss*tmp6 - 2*tmp1*tmp6 - tmp3*(2*ss + 2*tmp1 + tt)))/(((tmp1 + tmp2)&
                          &**2 - 2*(ss + tmp1)*tmp3 + tmp5)*(4*tmp1 - tt)*tt*(-tmp4 + tt))

  END FUNCTION MP2MPL_MP_SIN_D11D22

                          !!!!!!!!!!!!!!!!!!!!!!
                         END MODULE mue_mp2mp_mono
                          !!!!!!!!!!!!!!!!!!!!!!
