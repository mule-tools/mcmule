                 !!!!!!!!!!!!!!!!!!!!!!!!!
                     MODULE MISC_TEST
                 !!!!!!!!!!!!!!!!!!!!!!!!!
  use testtools
contains
  SUBROUTINE TESTMISC(ONLYFAST)
  logical onlyfast
  if(.not.onlyfast) call testenvegas
  call testenpoints
  END SUBROUTINE TESTMISC


  SUBROUTINE TESTENVEGAS
  xinormcut = 0.1
  xinormcut1 = 0.03
  xinormcut2 = 0.10
  flavour = 'e-S'

  call blockstart("e-n VEGAS test")
  call initflavour("mu-e", mys = 40.)
  musq = mm**2

  call test_INT("ee2nn0"             , 10, 10,  1.7427427928842846E+01)
  call test_INT("ee2nnF"             , 10, 10, -5.3513965446384510E+01)
  call test_INT("ee2nnR"             , 40, 10,  1.0107778958289048E+01)
  call test_INT("ee2nnS"             , 10, 10, -1.2604479734493798E+04)
  call test_INT("ee2nnI"             , 40, 10, -9.6442782555377380E+02)
  call test_INT("ee2nnRF"            ,  1,  5, -6.4689976873918340E+01)
  call test_INT("ee2nnRR"            , 40, 10,  2.3199971609719487E+01)
  call test_INT("ee2nnSS"            , 10, 10,  8.5048475754553765E+03)
  call test_INT("ee2nnCC"            , 10, 10, -2.6391720094237749E+03)
  call blockend(9)

  END SUBROUTINE

  SUBROUTINE TESTENPOINTS
  xinormcut = 1
  xinormcut1 = 1
  xinormcut2 = 1
  flavour = 'e-S'

  call blockstart("e-n pointwise test")
  call initflavour("mu-e", mys = 40.)
  musq = mm**2

  call test_INT("ee2nn0"             , ans= 1.3273981039182754E+01)
  call test_INT("ee2nnF"             , ans= 3.7309655489281944E+01)
  call test_INT("ee2nnR"             , ans=-1.0137072206035832E+01)
  call test_INT("ee2nnS"             , ans=-6.1571835627969676E+03)
  call test_INT("ee2nnI"             , ans= 3.3611551248451974E+02)
  call test_INT("ee2nnRF"            , ans=-2.3899974147807626E+01)
  call test_INT("ee2nnRR"            , ans= 1.3876695571446987E+01)
  call test_INT("ee2nnSS"            , ans= 2.5316888886776110E+03)
  call test_INT("ee2nnCC"            , ans=-1.9102859016345521E+03)

  call blockend(9)
  END SUBROUTINE



                 !!!!!!!!!!!!!!!!!!!!!!!!!
                   END MODULE MISC_TEST
                 !!!!!!!!!!!!!!!!!!!!!!!!!
