
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_T1BUBBC
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function T1BUBBC(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, & 
         np4,mu2)
!
      use global_def, only: pi, prec
      implicit none
!

    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
    
    complex(kind=prec) :: Bcache(4)

!
      real(kind=prec) T1BUBBC,EL,GF,MOU2,MIN2, & 
         p1p2,p1p4,p2p4,asym, & 
         np2,np3,np4,mu2, & 
         aa,bb
!
      aa=0._prec
      bb= & 
          -1._prec
!

!    
      call setlambda( & 
          -1._prec)
      call setdelta(0._prec)
      call setmudim(mu2)
      call setminmass(0._prec)
!     
    Bcache(1) = B0i(bb0,MIN2-2*p1p4,MIN2,0._prec)
    Bcache(2) = B0i(bb0,MOU2+2*p1p4-2*p2p4,MOU2,0._prec)
    Bcache(3) = B0i(bb1,MIN2-2*p1p4,MIN2,0._prec)
    Bcache(4) = B0i(bb1,MOU2+2*p1p4-2*p2p4,MOU2,0._prec)

!
      T1BUBBC=real(        ( & 
          -2*EL**4*GF**2*sqrt(MIN2)* & 
         ( & 
          -27*CMPLX(aa,bb)*asym*MIN2**2*p1p4  & 
          + 27*CMPLX(aa,bb)*asym*MIN2*MOU2*p1p4  & 
          +  & 
           27*MIN2**3*np4*p1p4  & 
          - 54*MIN2**2*MOU2*np4*p1p4  & 
          +  & 
           27*MIN2*MOU2**2*np4*p1p4  & 
          +  & 
           38*CMPLX(aa,bb)*asym*MIN2*p1p2*p1p4  & 
          - 34*MIN2**2.5*p1p2*p1p4  & 
          +  & 
           34*MIN2**1.5*MOU2*p1p2*p1p4  & 
          -  & 
           54*MIN2**2*np2*p1p2*p1p4  & 
          +  & 
           54*MIN2*MOU2*np2*p1p2*p1p4  & 
          -  & 
           71*MIN2**2*np4*p1p2*p1p4  & 
          +  & 
           71*MIN2*MOU2*np4*p1p2*p1p4  & 
          +  & 
           122*MIN2**1.5*p1p2**2*p1p4  & 
          -  & 
           54*sqrt(MIN2)*MOU2*p1p2**2*p1p4  & 
          +  & 
           88*MIN2*np2*p1p2**2*p1p4  & 
          +  & 
           44*MIN2*np4*p1p2**2*p1p4  & 
          -  & 
           88*sqrt(MIN2)*p1p2**3*p1p4  & 
          +  & 
           20*CMPLX(aa,bb)*asym*MIN2*p1p4**2  & 
          + 34*MIN2**2.5*p1p4**2  & 
          -  & 
           4*CMPLX(aa,bb)*asym*MOU2*p1p4**2  & 
          - 34*MIN2**1.5*MOU2*p1p4**2  & 
          +  & 
           54*MIN2**2*np2*p1p4**2  & 
          -  & 
           54*MIN2*MOU2*np2*p1p4**2  & 
          -  & 
           32*MIN2**2*np4*p1p4**2  & 
          +  & 
           32*MIN2*MOU2*np4*p1p4**2  & 
          - 20*CMPLX(aa,bb)*asym*p1p2*p1p4**2  & 
          -  & 
           115*MIN2**1.5*p1p2*p1p4**2  & 
          +  & 
           47*sqrt(MIN2)*MOU2*p1p2*p1p4**2  & 
          -  & 
           78*MIN2*np2*p1p2*p1p4**2  & 
          +  & 
           32*MIN2*np4*p1p2*p1p4**2  & 
          +  & 
           22*MOU2*np4*p1p2*p1p4**2  & 
          +  & 
           54*sqrt(MIN2)*p1p2**2*p1p4**2  & 
          +  & 
           44*np4*p1p2**2*p1p4**2  & 
          + 12*CMPLX(aa,bb)*asym*p1p4**3  & 
          +  & 
           32*MIN2**1.5*p1p4**3  & 
          -  & 
           32*sqrt(MIN2)*MOU2*p1p4**3  & 
          +  & 
           12*MIN2*np2*p1p4**3  & 
          - 22*MOU2*np2*p1p4**3  & 
          -  & 
           44*sqrt(MIN2)*p1p2*p1p4**3  & 
          -  & 
           44*np2*p1p2*p1p4**3  & 
          + 6*CMPLX(aa,bb)*asym*MIN2**2*p2p4  & 
          +  & 
           34*MIN2**3.5*p2p4  & 
          - 6*CMPLX(aa,bb)*asym*MIN2*MOU2*p2p4  & 
          -  & 
           34*MIN2**2.5*MOU2*p2p4  & 
          + 54*MIN2**3*np2*p2p4  & 
          -  & 
           54*MIN2**2*MOU2*np2*p2p4  & 
          - 54*MIN2**3*np4*p2p4  & 
          +  & 
           54*MIN2**2*MOU2*np4*p2p4  & 
          -  & 
           122*MIN2**2.5*p1p2*p2p4  & 
          +  & 
           54*MIN2**1.5*MOU2*p1p2*p2p4  & 
          -  & 
           88*MIN2**2*np2*p1p2*p2p4  & 
          +  & 
           142*MIN2**2*np4*p1p2*p2p4  & 
          -  & 
           54*MIN2*MOU2*np4*p1p2*p2p4  & 
          +  & 
           88*MIN2**1.5*p1p2**2*p2p4  & 
          -  & 
           88*MIN2*np4*p1p2**2*p2p4  & 
          -  & 
           38*CMPLX(aa,bb)*asym*MIN2*p1p4*p2p4  & 
          - 24*MIN2**2.5*p1p4*p2p4  & 
          +  & 
           24*MIN2**1.5*MOU2*p1p4*p2p4  & 
          -  & 
           37*MIN2**2*np2*p1p4*p2p4  & 
          +  & 
           27*MIN2*MOU2*np2*p1p4*p2p4  & 
          +  & 
           170*MIN2**2*np4*p1p4*p2p4  & 
          -  & 
           160*MIN2*MOU2*np4*p1p4*p2p4  & 
          -  & 
           27*MIN2**1.5*p1p2*p1p4*p2p4  & 
          +  & 
           27*sqrt(MIN2)*MOU2*p1p2*p1p4*p2p4  & 
          -  & 
           44*MIN2*np2*p1p2*p1p4*p2p4  & 
          -  & 
           236*MIN2*np4*p1p2*p1p4*p2p4  & 
          -  & 
           18*MOU2*np4*p1p2*p1p4*p2p4  & 
          +  & 
           132*sqrt(MIN2)*p1p2**2*p1p4*p2p4  & 
          -  & 
           24*np4*p1p2**2*p1p4*p2p4  & 
          + 14*CMPLX(aa,bb)*asym*p1p4**2*p2p4  & 
          +  & 
           50*MIN2**1.5*p1p4**2*p2p4  & 
          +  & 
           18*sqrt(MIN2)*MOU2*p1p4**2*p2p4  & 
          +  & 
           70*MIN2*np2*p1p4**2*p2p4  & 
          +  & 
           18*MOU2*np2*p1p4**2*p2p4  & 
          -  & 
           66*MIN2*np4*p1p4**2*p2p4  & 
          +  & 
           14*sqrt(MIN2)*p1p2*p1p4**2*p2p4  & 
          +  & 
           24*np2*p1p2*p1p4**2*p2p4  & 
          -  & 
           44*np4*p1p2*p1p4**2*p2p4  & 
          +  & 
           22*sqrt(MIN2)*p1p4**3*p2p4  & 
          +  & 
           44*np2*p1p4**3*p2p4  & 
          + 122*MIN2**2.5*p2p4**2  & 
          -  & 
           54*MIN2**1.5*MOU2*p2p4**2  & 
          +  & 
           88*MIN2**2*np2*p2p4**2  & 
          -  & 
           178*MIN2**2*np4*p2p4**2  & 
          +  & 
           90*MIN2*MOU2*np4*p2p4**2  & 
          -  & 
           176*MIN2**1.5*p1p2*p2p4**2  & 
          +  & 
           224*MIN2*np4*p1p2*p2p4**2  & 
          -  & 
           72*MIN2**1.5*p1p4*p2p4**2  & 
          -  & 
           18*sqrt(MIN2)*MOU2*p1p4*p2p4**2  & 
          -  & 
           44*MIN2*np2*p1p4*p2p4**2  & 
          +  & 
           222*MIN2*np4*p1p4*p2p4**2  & 
          -  & 
           68*sqrt(MIN2)*p1p2*p1p4*p2p4**2  & 
          +  & 
           24*np4*p1p2*p1p4*p2p4**2  & 
          -  & 
           46*sqrt(MIN2)*p1p4**2*p2p4**2  & 
          -  & 
           24*np2*p1p4**2*p2p4**2  & 
          + 88*MIN2**1.5*p2p4**3  & 
          -  & 
           136*MIN2*np4*p2p4**3  & 
          + 24*sqrt(MIN2)*p1p4*p2p4**3) & 
          *Bcache(1))/ & 
       (9.*p1p4**3*(p1p4  & 
          - p2p4)*Pi**2)  & 
          +  & 
      (2*EL**4*GF**2*MOU2* & 
         (27*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
          + 34*MIN2**3*p1p4  & 
          -  & 
           27*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
          -  & 
           68*MIN2**2*MOU2*p1p4  & 
          + 34*MIN2*MOU2**2*p1p4  & 
          +  & 
           54*MIN2**2.5*np2*p1p4  & 
          -  & 
           108*MIN2**1.5*MOU2*np2*p1p4  & 
          +  & 
           54*sqrt(MIN2)*MOU2**2*np2*p1p4  & 
          -  & 
           27*MIN2**2.5*np4*p1p4  & 
          +  & 
           54*MIN2**1.5*MOU2*np4*p1p4  & 
          -  & 
           27*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
          -  & 
           44*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
          -  & 
           156*MIN2**2*p1p2*p1p4  & 
          + 210*MIN2*MOU2*p1p2*p1p4  & 
          -  & 
           54*MOU2**2*p1p2*p1p4  & 
          -  & 
           142*MIN2**1.5*np2*p1p2*p1p4  & 
          +  & 
           142*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
          +  & 
           71*MIN2**1.5*np4*p1p2*p1p4  & 
          -  & 
           71*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
          +  & 
           210*MIN2*p1p2**2*p1p4  & 
          - 142*MOU2*p1p2**2*p1p4  & 
          +  & 
           88*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
          -  & 
           44*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
          -  & 
           88*p1p2**3*p1p4  & 
          - 10*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
          -  & 
           7*MIN2**2*p1p4**2  & 
          - 20*MIN2*MOU2*p1p4**2  & 
          +  & 
           27*MOU2**2*p1p4**2  & 
          - 64*MIN2**1.5*np2*p1p4**2  & 
          +  & 
           64*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
          -  & 
           12*MIN2**1.5*np4*p1p4**2  & 
          +  & 
           12*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
          +  & 
           61*MIN2*p1p2*p1p4**2  & 
          + 7*MOU2*p1p2*p1p4**2  & 
          +  & 
           98*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
          +  & 
           34*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
          -  & 
           54*p1p2**2*p1p4**2  & 
          + 12*MIN2*p1p4**3  & 
          -  & 
           12*MOU2*p1p4**3  & 
          + 10*sqrt(MIN2)*np2*p1p4**3  & 
          -  & 
           44*p1p2*p1p4**3  & 
          - 27*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
          -  & 
           34*MIN2**3*p2p4  & 
          + 27*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
          +  & 
           34*MIN2**2*MOU2*p2p4  & 
          - 54*MIN2**2.5*np2*p2p4  & 
          +  & 
           54*MIN2**1.5*MOU2*np2*p2p4  & 
          +  & 
           27*MIN2**2.5*np4*p2p4  & 
          -  & 
           27*sqrt(MIN2)*MOU2**2*np4*p2p4  & 
          +  & 
           44*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
          +  & 
           156*MIN2**2*p1p2*p2p4  & 
          - 88*MIN2*MOU2*p1p2*p2p4  & 
          +  & 
           142*MIN2**1.5*np2*p1p2*p2p4  & 
          -  & 
           54*sqrt(MIN2)*MOU2*np2*p1p2*p2p4  & 
          -  & 
           71*MIN2**1.5*np4*p1p2*p2p4  & 
          -  & 
           17*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
          -  & 
           210*MIN2*p1p2**2*p2p4  & 
          + 54*MOU2*p1p2**2*p2p4  & 
          -  & 
           88*sqrt(MIN2)*np2*p1p2**2*p2p4  & 
          +  & 
           44*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
          +  & 
           88*p1p2**3*p2p4  & 
          + 48*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
          +  & 
           119*MIN2**2*p1p4*p2p4  & 
          - 146*MIN2*MOU2*p1p4*p2p4  & 
          +  & 
           27*MOU2**2*p1p4*p2p4  & 
          +  & 
           216*MIN2**1.5*np2*p1p4*p2p4  & 
          -  & 
           206*sqrt(MIN2)*MOU2*np2*p1p4*p2p4  & 
          -  & 
           38*MIN2**1.5*np4*p1p4*p2p4  & 
          +  & 
           28*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
          -  & 
           359*MIN2*p1p2*p1p4*p2p4  & 
          +  & 
           223*MOU2*p1p2*p1p4*p2p4  & 
          -  & 
           284*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
          +  & 
           240*p1p2**2*p1p4*p2p4  & 
          - 40*MIN2*p1p4**2*p2p4  & 
          -  & 
           28*MOU2*p1p4**2*p2p4  & 
          -  & 
           118*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
          -  & 
           22*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
          +  & 
           118*p1p2*p1p4**2*p2p4  & 
          + 22*p1p4**3*p2p4  & 
          -  & 
           38*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
          -  & 
           112*MIN2**2*p2p4**2  & 
          + 44*MIN2*MOU2*p2p4**2  & 
          -  & 
           152*MIN2**1.5*np2*p2p4**2  & 
          +  & 
           54*sqrt(MIN2)*MOU2*np2*p2p4**2  & 
          +  & 
           50*MIN2**1.5*np4*p2p4**2  & 
          +  & 
           48*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
          +  & 
           298*MIN2*p1p2*p2p4**2  & 
          - 54*MOU2*p1p2*p2p4**2  & 
          +  & 
           186*sqrt(MIN2)*np2*p1p2*p2p4**2  & 
          -  & 
           34*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
          -  & 
           186*p1p2**2*p2p4**2  & 
          + 94*MIN2*p1p4*p2p4**2  & 
          -  & 
           48*MOU2*p1p4*p2p4**2  & 
          +  & 
           206*sqrt(MIN2)*np2*p1p4*p2p4**2  & 
          +  & 
           20*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
          -  & 
           172*p1p2*p1p4*p2p4**2  & 
          - 20*p1p4**2*p2p4**2  & 
          -  & 
           66*MIN2*p2p4**3  & 
          - 98*sqrt(MIN2)*np2*p2p4**3  & 
          +  & 
           2*sqrt(MIN2)*np4*p2p4**3  & 
          + 98*p1p2*p2p4**3  & 
          -  & 
           2*p1p4*p2p4**3)* & 
         Bcache(2))/ & 
       (9.*p1p4*(p1p4  & 
          - p2p4)**3*Pi**2)  & 
          +  & 
      (EL**4*GF**2*(3*CMPLX(aa,bb)*asym*MIN2**2.5*p1p4  & 
          -  & 
           3*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4  & 
          -  & 
           36*MIN2**3.5*np4*p1p4  & 
          +  & 
           72*MIN2**2.5*MOU2*np4*p1p4  & 
          -  & 
           36*MIN2**1.5*MOU2**2*np4*p1p4  & 
          +  & 
           34*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4  & 
          +  & 
           40*MIN2**3*p1p2*p1p4  & 
          -  & 
           30*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4  & 
          -  & 
           40*MIN2**2*MOU2*p1p2*p1p4  & 
          +  & 
           72*MIN2**2.5*np2*p1p2*p1p4  & 
          -  & 
           72*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
          +  & 
           92*MIN2**2.5*np4*p1p2*p1p4  & 
          -  & 
           92*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
          -  & 
           60*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p1p4  & 
          -  & 
           152*MIN2**2*p1p2**2*p1p4  & 
          +  & 
           72*MIN2*MOU2*p1p2**2*p1p4  & 
          -  & 
           112*MIN2**1.5*np2*p1p2**2*p1p4  & 
          -  & 
           56*MIN2**1.5*np4*p1p2**2*p1p4  & 
          +  & 
           112*MIN2*p1p2**3*p1p4  & 
          -  & 
           20*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2  & 
          - 40*MIN2**3*p1p4**2  & 
          +  & 
           10*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2  & 
          +  & 
           40*MIN2**2*MOU2*p1p4**2  & 
          -  & 
           72*MIN2**2.5*np2*p1p4**2  & 
          +  & 
           72*MIN2**1.5*MOU2*np2*p1p4**2  & 
          +  & 
           80*MIN2**2.5*np4*p1p4**2  & 
          -  & 
           116*MIN2**1.5*MOU2*np4*p1p4**2  & 
          +  & 
           36*sqrt(MIN2)*MOU2**2*np4*p1p4**2  & 
          +  & 
           10*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2  & 
          +  & 
           108*MIN2**2*p1p2*p1p4**2  & 
          -  & 
           28*MIN2*MOU2*p1p2*p1p4**2  & 
          +  & 
           24*MIN2**1.5*np2*p1p2*p1p4**2  & 
          +  & 
           72*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
          -  & 
           136*MIN2**1.5*np4*p1p2*p1p4**2  & 
          +  & 
           64*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2  & 
          +  & 
           80*MIN2*p1p2**2*p1p4**2  & 
          -  & 
           72*MOU2*p1p2**2*p1p4**2  & 
          +  & 
           112*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
          -  & 
           112*p1p2**3*p1p4**2  & 
          +  & 
           10*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3  & 
          - 40*MIN2**2*p1p4**3  & 
          +  & 
           76*MIN2*MOU2*p1p4**3  & 
          - 36*MOU2**2*p1p4**3  & 
          +  & 
           40*MIN2**1.5*np2*p1p4**3  & 
          -  & 
           24*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
          -  & 
           72*MIN2**1.5*np4*p1p4**3  & 
          +  & 
           72*sqrt(MIN2)*MOU2*np4*p1p4**3  & 
          +  & 
           16*MIN2*p1p2*p1p4**3  & 
          - 40*MOU2*p1p2*p1p4**3  & 
          +  & 
           128*sqrt(MIN2)*np4*p1p2*p1p4**3  & 
          +  & 
           72*MIN2*p1p4**4  & 
          - 72*MOU2*p1p4**4  & 
          -  & 
           16*sqrt(MIN2)*np2*p1p4**4  & 
          - 112*p1p2*p1p4**4  & 
          -  & 
           9*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4  & 
          - 40*MIN2**4*p2p4  & 
          +  & 
           9*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4  & 
          +  & 
           40*MIN2**3*MOU2*p2p4  & 
          - 72*MIN2**3.5*np2*p2p4  & 
          +  & 
           72*MIN2**2.5*MOU2*np2*p2p4  & 
          +  & 
           72*MIN2**3.5*np4*p2p4  & 
          -  & 
           72*MIN2**2.5*MOU2*np4*p2p4  & 
          +  & 
           24*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4  & 
          +  & 
           152*MIN2**3*p1p2*p2p4  & 
          -  & 
           72*MIN2**2*MOU2*p1p2*p2p4  & 
          +  & 
           112*MIN2**2.5*np2*p1p2*p2p4  & 
          -  & 
           184*MIN2**2.5*np4*p1p2*p2p4  & 
          +  & 
           72*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
          -  & 
           112*MIN2**2*p1p2**2*p2p4  & 
          +  & 
           112*MIN2**1.5*np4*p1p2**2*p2p4  & 
          -  & 
           34*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4  & 
          +  & 
           64*MIN2**3*p1p4*p2p4  & 
          +  & 
           30*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4  & 
          -  & 
           64*MIN2**2*MOU2*p1p4*p2p4  & 
          +  & 
           124*MIN2**2.5*np2*p1p4*p2p4  & 
          -  & 
           108*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
          -  & 
           290*MIN2**2.5*np4*p1p4*p2p4  & 
          +  & 
           274*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
          +  & 
           108*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4  & 
          -  & 
           116*MIN2**2*p1p2*p1p4*p2p4  & 
          +  & 
           36*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          -  & 
           56*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
          +  & 
           486*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
          -  & 
           54*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
          -  & 
           56*MIN2*p1p2**2*p1p4*p2p4  & 
          -  & 
           88*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4  & 
          +  & 
           2*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4  & 
          -  & 
           66*MIN2**2*p1p4**2*p2p4  & 
          -  & 
           14*MIN2*MOU2*p1p4**2*p2p4  & 
          -  & 
           110*MIN2**1.5*np2*p1p4**2*p2p4  & 
          -  & 
           18*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
          +  & 
           292*MIN2**1.5*np4*p1p4**2*p2p4  & 
          -  & 
           192*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4  & 
          -  & 
           120*MIN2*p1p2*p1p4**2*p2p4  & 
          +  & 
           72*MOU2*p1p2*p1p4**2*p2p4  & 
          -  & 
           136*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
          -  & 
           260*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
          +  & 
           224*p1p2**2*p1p4**2*p2p4  & 
          -  & 
           140*MIN2*p1p4**3*p2p4  & 
          + 192*MOU2*p1p4**3*p2p4  & 
          -  & 
           12*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
          -  & 
           112*sqrt(MIN2)*np4*p1p4**3*p2p4  & 
          +  & 
           272*p1p2*p1p4**3*p2p4  & 
          + 112*p1p4**4*p2p4  & 
          -  & 
           24*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2  & 
          - 152*MIN2**3*p2p4**2  & 
          +  & 
           72*MIN2**2*MOU2*p2p4**2  & 
          -  & 
           112*MIN2**2.5*np2*p2p4**2  & 
          +  & 
           220*MIN2**2.5*np4*p2p4**2  & 
          -  & 
           108*MIN2**1.5*MOU2*np4*p2p4**2  & 
          +  & 
           224*MIN2**2*p1p2*p2p4**2  & 
          -  & 
           272*MIN2**1.5*np4*p1p2*p2p4**2  & 
          -  & 
           54*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2  & 
          +  & 
           242*MIN2**2*p1p4*p2p4**2  & 
          -  & 
           54*MIN2*MOU2*p1p4*p2p4**2  & 
          +  & 
           168*MIN2**1.5*np2*p1p4*p2p4**2  & 
          -  & 
           496*MIN2**1.5*np4*p1p4*p2p4**2  & 
          +  & 
           108*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2  & 
          -  & 
           144*MIN2*p1p2*p1p4*p2p4**2  & 
          +  & 
           248*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
          +  & 
           92*MIN2*p1p4**2*p2p4**2  & 
          -  & 
           108*MOU2*p1p4**2*p2p4**2  & 
          +  & 
           24*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
          +  & 
           272*sqrt(MIN2)*np4*p1p4**2*p2p4**2  & 
          -  & 
           272*p1p2*p1p4**2*p2p4**2  & 
          - 272*p1p4**3*p2p4**2  & 
          -  & 
           112*MIN2**2*p2p4**3  & 
          + 160*MIN2**1.5*np4*p2p4**3  & 
          +  & 
           88*MIN2*p1p4*p2p4**3  & 
          -  & 
           160*sqrt(MIN2)*np4*p1p4*p2p4**3  & 
          +  & 
           160*p1p4**2*p2p4**3)*Bcache(3) & 
         )/(9.*p1p4**3*(p1p4  & 
          - p2p4)*Pi**2)  & 
          +  & 
      (EL**4*GF**2*(33*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4  & 
          +  & 
           40*MIN2**3*MOU2*p1p4  & 
          -  & 
           33*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p1p4  & 
          -  & 
           80*MIN2**2*MOU2**2*p1p4  & 
          + 40*MIN2*MOU2**3*p1p4  & 
          +  & 
           72*MIN2**2.5*MOU2*np2*p1p4  & 
          -  & 
           144*MIN2**1.5*MOU2**2*np2*p1p4  & 
          +  & 
           72*sqrt(MIN2)*MOU2**3*np2*p1p4  & 
          -  & 
           36*MIN2**2.5*MOU2*np4*p1p4  & 
          +  & 
           72*MIN2**1.5*MOU2**2*np4*p1p4  & 
          -  & 
           36*sqrt(MIN2)*MOU2**3*np4*p1p4  & 
          -  & 
           53*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4  & 
          -  & 
           192*MIN2**2*MOU2*p1p2*p1p4  & 
          +  & 
           264*MIN2*MOU2**2*p1p2*p1p4  & 
          -  & 
           72*MOU2**3*p1p2*p1p4  & 
          -  & 
           184*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
          +  & 
           184*sqrt(MIN2)*MOU2**2*np2*p1p2*p1p4  & 
          +  & 
           92*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
          -  & 
           92*sqrt(MIN2)*MOU2**2*np4*p1p2*p1p4  & 
          +  & 
           264*MIN2*MOU2*p1p2**2*p1p4  & 
          -  & 
           184*MOU2**2*p1p2**2*p1p4  & 
          +  & 
           112*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4  & 
          -  & 
           56*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4  & 
          -  & 
           112*MOU2*p1p2**3*p1p4  & 
          +  & 
           36*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2  & 
          + 40*MIN2**3*p1p4**2  & 
          -  & 
           49*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2  & 
          -  & 
           84*MIN2**2*MOU2*p1p4**2  & 
          +  & 
           8*MIN2*MOU2**2*p1p4**2  & 
          + 36*MOU2**3*p1p4**2  & 
          +  & 
           72*MIN2**2.5*np2*p1p4**2  & 
          -  & 
           232*MIN2**1.5*MOU2*np2*p1p4**2  & 
          +  & 
           160*sqrt(MIN2)*MOU2**2*np2*p1p4**2  & 
          -  & 
           36*MIN2**2.5*np4*p1p4**2  & 
          +  & 
           60*MIN2**1.5*MOU2*np4*p1p4**2  & 
          -  & 
           24*sqrt(MIN2)*MOU2**2*np4*p1p4**2  & 
          -  & 
           56*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2  & 
          -  & 
           192*MIN2**2*p1p2*p1p4**2  & 
          +  & 
           340*MIN2*MOU2*p1p2*p1p4**2  & 
          -  & 
           68*MOU2**2*p1p2*p1p4**2  & 
          -  & 
           184*MIN2**1.5*np2*p1p2*p1p4**2  & 
          +  & 
           312*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
          +  & 
           92*MIN2**1.5*np4*p1p2*p1p4**2  & 
          -  & 
           52*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2  & 
          +  & 
           264*MIN2*p1p2**2*p1p4**2  & 
          -  & 
           256*MOU2*p1p2**2*p1p4**2  & 
          +  & 
           112*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
          -  & 
           56*sqrt(MIN2)*np4*p1p2**2*p1p4**2  & 
          -  & 
           112*p1p2**3*p1p4**2  & 
          -  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3  & 
          - 4*MIN2**2*p1p4**3  & 
          -  & 
           20*MIN2*MOU2*p1p4**3  & 
          + 24*MOU2**2*p1p4**3  & 
          -  & 
           52*MIN2**1.5*np2*p1p4**3  & 
          +  & 
           68*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
          -  & 
           40*MIN2**1.5*np4*p1p4**3  & 
          +  & 
           40*sqrt(MIN2)*MOU2*np4*p1p4**3  & 
          +  & 
           40*MIN2*p1p2*p1p4**3  & 
          - 16*MOU2*p1p2*p1p4**3  & 
          +  & 
           56*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
          +  & 
           96*sqrt(MIN2)*np4*p1p2*p1p4**3  & 
          +  & 
           40*MIN2*p1p4**4  & 
          - 40*MOU2*p1p4**4  & 
          +  & 
           16*sqrt(MIN2)*np2*p1p4**4  & 
          - 112*p1p2*p1p4**4  & 
          -  & 
           33*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4  & 
          -  & 
           40*MIN2**3*MOU2*p2p4  & 
          +  & 
           33*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p2p4  & 
          +  & 
           40*MIN2**2*MOU2**2*p2p4  & 
          -  & 
           72*MIN2**2.5*MOU2*np2*p2p4  & 
          +  & 
           72*MIN2**1.5*MOU2**2*np2*p2p4  & 
          +  & 
           36*MIN2**2.5*MOU2*np4*p2p4  & 
          -  & 
           36*sqrt(MIN2)*MOU2**3*np4*p2p4  & 
          +  & 
           53*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p2p4  & 
          +  & 
           192*MIN2**2*MOU2*p1p2*p2p4  & 
          -  & 
           112*MIN2*MOU2**2*p1p2*p2p4  & 
          +  & 
           184*MIN2**1.5*MOU2*np2*p1p2*p2p4  & 
          -  & 
           72*sqrt(MIN2)*MOU2**2*np2*p1p2*p2p4  & 
          -  & 
           92*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
          -  & 
           20*sqrt(MIN2)*MOU2**2*np4*p1p2*p2p4  & 
          -  & 
           264*MIN2*MOU2*p1p2**2*p2p4  & 
          +  & 
           72*MOU2**2*p1p2**2*p2p4  & 
          -  & 
           112*sqrt(MIN2)*MOU2*np2*p1p2**2*p2p4  & 
          +  & 
           56*sqrt(MIN2)*MOU2*np4*p1p2**2*p2p4  & 
          +  & 
           112*MOU2*p1p2**3*p2p4  & 
          -  & 
           72*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4  & 
          -  & 
           80*MIN2**3*p1p4*p2p4  & 
          +  & 
           138*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4  & 
          +  & 
           260*MIN2**2*MOU2*p1p4*p2p4  & 
          -  & 
           216*MIN2*MOU2**2*p1p4*p2p4  & 
          +  & 
           36*MOU2**3*p1p4*p2p4  & 
          -  & 
           144*MIN2**2.5*np2*p1p4*p2p4  & 
          +  & 
           504*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
          -  & 
           344*sqrt(MIN2)*MOU2**2*np2*p1p4*p2p4  & 
          +  & 
           72*MIN2**2.5*np4*p1p4*p2p4  & 
          -  & 
           122*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
          +  & 
           34*sqrt(MIN2)*MOU2**2*np4*p1p4*p2p4  & 
          +  & 
           112*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4  & 
          +  & 
           384*MIN2**2*p1p2*p1p4*p2p4  & 
          -  & 
           828*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
           364*MOU2**2*p1p2*p1p4*p2p4  & 
          +  & 
           368*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
          -  & 
           624*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4  & 
          -  & 
           184*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
          +  & 
           72*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
          -  & 
           528*MIN2*p1p2**2*p1p4*p2p4  & 
          +  & 
           568*MOU2*p1p2**2*p1p4*p2p4  & 
          -  & 
           224*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4  & 
          +  & 
           112*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4  & 
          +  & 
           224*p1p2**3*p1p4*p2p4  & 
          +  & 
           94*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4  & 
          +  & 
           164*MIN2**2*p1p4**2*p2p4  & 
          -  & 
           210*MIN2*MOU2*p1p4**2*p2p4  & 
          -  & 
           34*MOU2**2*p1p4**2*p2p4  & 
          +  & 
           304*MIN2**1.5*np2*p1p4**2*p2p4  & 
          -  & 
           448*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
          -  & 
           16*MIN2**1.5*np4*p1p4**2*p2p4  & 
          -  & 
           28*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4  & 
          -  & 
           496*MIN2*p1p2*p1p4**2*p2p4  & 
          +  & 
           376*MOU2*p1p2*p1p4**2*p2p4  & 
          -  & 
           336*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
          -  & 
           100*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
          +  & 
           224*p1p2**2*p1p4**2*p2p4  & 
          - 80*MIN2*p1p4**3*p2p4  & 
          +  & 
           28*MOU2*p1p4**3*p2p4  & 
          -  & 
           76*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
          -  & 
           112*sqrt(MIN2)*np4*p1p4**3*p2p4  & 
          +  & 
           176*p1p2*p1p4**3*p2p4  & 
          + 112*p1p4**4*p2p4  & 
          +  & 
           36*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2  & 
          + 40*MIN2**3*p2p4**2  & 
          -  & 
           89*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4**2  & 
          -  & 
           176*MIN2**2*MOU2*p2p4**2  & 
          +  & 
           56*MIN2*MOU2**2*p2p4**2  & 
          +  & 
           72*MIN2**2.5*np2*p2p4**2  & 
          -  & 
           272*MIN2**1.5*MOU2*np2*p2p4**2  & 
          +  & 
           72*sqrt(MIN2)*MOU2**2*np2*p2p4**2  & 
          -  & 
           36*MIN2**2.5*np4*p2p4**2  & 
          +  & 
           62*MIN2**1.5*MOU2*np4*p2p4**2  & 
          +  & 
           102*sqrt(MIN2)*MOU2**2*np4*p2p4**2  & 
          -  & 
           56*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4**2  & 
          -  & 
           192*MIN2**2*p1p2*p2p4**2  & 
          +  & 
           488*MIN2*MOU2*p1p2*p2p4**2  & 
          -  & 
           72*MOU2**2*p1p2*p2p4**2  & 
          -  & 
           184*MIN2**1.5*np2*p1p2*p2p4**2  & 
          +  & 
           312*sqrt(MIN2)*MOU2*np2*p1p2*p2p4**2  & 
          +  & 
           92*MIN2**1.5*np4*p1p2*p2p4**2  & 
          -  & 
           20*sqrt(MIN2)*MOU2*np4*p1p2*p2p4**2  & 
          +  & 
           264*MIN2*p1p2**2*p2p4**2  & 
          -  & 
           312*MOU2*p1p2**2*p2p4**2  & 
          +  & 
           112*sqrt(MIN2)*np2*p1p2**2*p2p4**2  & 
          -  & 
           56*sqrt(MIN2)*np4*p1p2**2*p2p4**2  & 
          -  & 
           112*p1p2**3*p2p4**2  & 
          -  & 
           140*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2  & 
          -  & 
           316*MIN2**2*p1p4*p2p4**2  & 
          +  & 
           390*MIN2*MOU2*p1p4*p2p4**2  & 
          -  & 
           102*MOU2**2*p1p4*p2p4**2  & 
          -  & 
           452*MIN2**1.5*np2*p1p4*p2p4**2  & 
          +  & 
           580*sqrt(MIN2)*MOU2*np2*p1p4*p2p4**2  & 
          +  & 
           152*MIN2**1.5*np4*p1p4*p2p4**2  & 
          +  & 
           24*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2  & 
          +  & 
           872*MIN2*p1p2*p1p4*p2p4**2  & 
          -  & 
           560*MOU2*p1p2*p1p4*p2p4**2  & 
          +  & 
           504*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2  & 
          -  & 
           88*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
          -  & 
           448*p1p2**2*p1p4*p2p4**2  & 
          +  & 
           164*MIN2*p1p4**2*p2p4**2  & 
          -  & 
           24*MOU2*p1p4**2*p2p4**2  & 
          +  & 
           216*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
          +  & 
           176*sqrt(MIN2)*np4*p1p4**2*p2p4**2  & 
          -  & 
           128*p1p2*p1p4**2*p2p4**2  & 
          - 176*p1p4**3*p2p4**2  & 
          +  & 
           62*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**3  & 
          +  & 
           156*MIN2**2*p2p4**3  & 
          - 160*MIN2*MOU2*p2p4**3  & 
          +  & 
           200*MIN2**1.5*np2*p2p4**3  & 
          -  & 
           200*sqrt(MIN2)*MOU2*np2*p2p4**3  & 
          -  & 
           96*MIN2**1.5*np4*p2p4**3  & 
          -  & 
           36*sqrt(MIN2)*MOU2*np4*p2p4**3  & 
          -  & 
           416*MIN2*p1p2*p2p4**3  & 
          + 200*MOU2*p1p2*p2p4**3  & 
          -  & 
           224*sqrt(MIN2)*np2*p1p2*p2p4**3  & 
          +  & 
           92*sqrt(MIN2)*np4*p1p2*p2p4**3  & 
          +  & 
           224*p1p2**2*p2p4**3  & 
          - 248*MIN2*p1p4*p2p4**3  & 
          +  & 
           36*MOU2*p1p4*p2p4**3  & 
          -  & 
           268*sqrt(MIN2)*np2*p1p4*p2p4**3  & 
          -  & 
           16*sqrt(MIN2)*np4*p1p4*p2p4**3  & 
          +  & 
           176*p1p2*p1p4*p2p4**3  & 
          + 16*p1p4**2*p2p4**3  & 
          +  & 
           124*MIN2*p2p4**4  & 
          + 112*sqrt(MIN2)*np2*p2p4**4  & 
          -  & 
           48*sqrt(MIN2)*np4*p2p4**4  & 
          - 112*p1p2*p2p4**4  & 
          +  & 
           48*p1p4*p2p4**4)* & 
         Bcache(4))/ & 
       (9.*p1p4*(p1p4  & 
          - p2p4)**3*Pi**2))
!
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_T1BUBBC
                          !!!!!!!!!!!!!!!!!!!!!!
