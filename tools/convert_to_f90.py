#!/usr/bin/env python
import re

filenames = [
    "CTOSAC" , "CTOSAD" , "CTOSBC" , 
    "CTTBAC" , "CTTBAD" , "CTTBBC" ,
    "T1BOXAC", "T1BOXAD", "T1BOXBC",
    "T1BUBAC", "T1BUBAD", "T1BUBBC",
    "T1TRIAC", "T1TRIAD", "T1TRIBC",
    "T2BOXAC", "T2BOXAD", "T2BOXBC",
    "T2TRIAC", "T2TRIAD", "T2TRIBC",
    "T3TRIAC", "T3TRIAD", "T3TRIBC",
    "T4TRIAC", "T4TRIAD", "T4TRIBC",
    "TLEPS0" , "TLEPS1"
]

for filename in filenames:
    with open('radiative/%s.F' % filename) as fp:
        F = fp.read()

    # Remove c comments with ! comments
    reg = re.compile("^[cC]", re.M)
    f90 = reg.sub('!', F)

    # Change line continuation character
    f90 = re.sub("\n\ \ \ \ \ [^\ ]", " & \n    ", f90)

    # Remove empty lines
    reg = re.compile("^\ *&\ *\n", re.M)
    f90 = reg.sub('', f90)
    
    # Remove the definition of pi
    f90 = f90.replace("np2,np3,np4,Pi,mu2,", "np2,np3,np4,mu2,")
    f90 = re.sub("[\ \t]+PI\ *=dacos\([^\)]*\)", "", f90)

    # Add import
    f90 = f90.replace("      implicit none", "      use global_def, only: pi, prec\n      implicit none")

    # Change type to prec
    f90 = f90.replace("RealType", "real(kind=prec)")
    f90 = f90.replace(".d0", "._prec")
    f90 = f90.replace("dsqrt", "sqrt")

    # Cache loop functions
    loopfcts = re.findall("([ABCD]+)0(i?)\(([^\)]*)\)",f90)
    cache = ["",""]
    fcts = {'A':[],'B':[], 'DB':[],'C':[],'D':[]}
    for f,tens,args in loopfcts:
        fcts[f].append(args)

        cache[0] += '    %scache(%d) = %s0%s(%s)\n' % (
            f, len(fcts[f]), f, tens, re.sub('[\s&]+', '', args)
        )

        f90 = f90.replace("%s0%s(%s)" % (f,tens,args), f+"cache("+str(len(fcts[f]))+")")

    for key,value in fcts.iteritems():
        if len(value) > 0:
            cache[1] += "    complex(kind=prec) :: %scache(%d)\n" % (key,len(value))
    f90 = f90.replace("      "+filename+"=",cache[0]+"\n!\n      "+filename+"=")

    # Include relevant LoopToos headers
    f90 = f90.replace("#include \"looptools.h\"", """
    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
    """+"\n"+cache[1])

    # Rename function end
    f90 = f90.replace("end", "end function")
    
    # Add module header
    f90 = """
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_%s
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
%s
                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_%s
                          !!!!!!!!!!!!!!!!!!!!!!
""" % (filename, f90, filename) 

    with open('radiative/%s.f95' % filename, 'w') as fp:
        fp.write(f90)

