                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions
  use user_dummy, only: pass_cut, userweight, names, filenamesuffix

  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 2
  integer, parameter :: nr_bins = 40
  real, parameter :: &
     min_val(nr_q) = (/ 0., 0. /)
  real, parameter :: &
     max_val(nr_q) = (/ 1., 1. /)
  integer :: userdim = 0

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer, parameter :: namesLen=6
  integer, parameter :: filenamesuffixLen=10


!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains


  SUBROUTINE FIX_MU

  !! ==== Specify the scale mu AND musq==mu**2 ==== !!

  musq = mM**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  print*, "This is a generic user file that is not what you want.."
  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: gah(4), gas(4), tee, t13
  real (kind=prec) :: quant(nr_q)

  pol1 = (/ 0._prec, 0._prec, 0.85_prec, 0._prec /)

  quant = 0.
  pass_cut = .true.
  call fix_mu

  select case(which_piece)
    case ('m2enn0', 'm2ennF', 'm2ennR', &
          'm2ennRF', 'm2ennRR', 'm2ej0', 'm2ejF', 'm2ejR')
      if (cos_th(q2, pol1) < 0) then
        pass_cut = .false.
      endif

      names(1) = 'xe'
      quant(1) = 2* q2(4) / Mm
    case ('m2ejg0')
      if (cos_th(q2, pol1) < 0) then
        pass_cut = .false.
      endif
      if (q4(4) < 10.) pass_cut = .false.

    case ('m2enng0', 'm2enngV', 'm2enngR', 'm2enngC')

      if(q5(4) > q6(4)) then
        gah=q5;gas=q6
      else
        gah=q6;gas=q5
      endif
      if (gah(4) < 10.) pass_cut = .false.

      names(1) = 'xe'
      quant(1) = 2* q2(4) / Mm
      names(2) = 'xy'
      quant(2) = 2* gah(4) / Mm

    case ("em2em0", "em2emOLE", &
          "em2emV", "em2emC", &
          "em2emFEE", "em2emREE", "em2emREE15", "em2emREE35", "em2emREEco", &
          "em2emFMM", "em2emRMM", "em2emFEM", "em2emREM", &
          "em2emFFEEEE", &
          "em2emRFEEEE", "em2emRFEEEE15", "em2emRFEEEE35", "em2emRFEEEEco", &
          "em2emRREEEE", "em2emRREEEE1516", "em2emRREEEE3536", "em2emRREEEEc", &
          "em2emA", "em2emAA", "em2emAFEE", "em2emAREE", "em2emAREE15", "em2emAREE35", "em2emNFEE", &
          "em2emFFEEEEz", "em2emFFMIXDz", "em2emFFMMMM", "em2emFFz", &
          "em2emREM15", "em2emREM35", &
          "em2emRF", "em2emRF15", "em2emRF35", "em2emRFMIXD15", &
          "em2emRFMIXD35", "em2emRFMMMM", "em2emRRMMMM", "em2emNFEMCT",  &
          "mp2mp0", "mp2mpF", "mp2mpR", 'mp2mpR15', 'mp2mpR35', 'mp2mpRco', &
          "mp2mpFF", &
          "mp2mpRF", "mp2mpRF15", "mp2mpRF35", "mp2mpRFco", &
          "mp2mpRR", "mp2mpRR1516", "mp2mpRR3536", "mp2mpRRc", &
          "mp2mpA", "mp2mpAA", "mp2mpAF", "mp2mpAR", "mp2mpAR15", "mp2mpAR35", "mp2mpNF")

      tee = sq(q1-q3)
      if (tee > -1.02e+3) pass_cut = .false.

      names(1) = 'xte'
      quant(1) = tee / (-142894.)

    case ('m2ennFF', 'm2ennFFz', 'ee2nn0', 'ee2nnF', 'ee2nnR', 'ee2nnS', 'ee2nnI', &
          'ee2nnRF', 'ee2nnRR', 'ee2nnSS', 'ee2nnCC')
      pol1 = 0._prec

    case ('ee2ee0')
      t13 = sq(q1-q3)
      if (t13 > -10000) pass_cut = .false.
      if (t13 < -30000) pass_cut = .false.
      names(1) = 'xt13'
      quant(1) = t13 / (-30000)
    case ('m2ennee0', 'm2enneeV', 'm2enneeA', 'm2enneeC', 'm2enneeR', &
          't2mnnee0', 't2mnneeV', 't2mnneeA', 't2mnneeC', 't2mnneeR', &
          'm2ennee0b', 'm2enneeAi')
      pol1 = (/ 0._prec, 0._prec, -0.85_prec, 0._prec /)

    case default
      call crash("This which piece is not implemented")
  end select

  END FUNCTION QUANT


  SUBROUTINE USEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE USEREVENT


                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!
