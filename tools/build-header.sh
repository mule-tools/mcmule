#!/bin/sh

gcc=$(gcc --version | grep gcc | awk '{print $3}')
echo "Generating headers for gcc version $gcc"

tmpdir=$(mktemp -d)

gfortran -J$tmpdir -cpp -fsyntax-only -fdefault-real-8 -DHEADER_ONLY \
    pre/global_def.f95 \
    pre/functions.f95 \
    pre/phase_space.f95 \
    src/user_dummy.f95 \
    src/vegas_m.f95 \
    src/mcmule_header.f95

cp $tmpdir/mcmule.mod mcmule_$gcc.mod

rm -rf $tmpdir
