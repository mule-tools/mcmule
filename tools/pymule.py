#!/bin/python3
import sys
import os
os.environ['PYMULE_QUICK'] = '1'
file_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.join(file_path,'pymule'))
import pymule.__main__
pymule.__main__.main()
