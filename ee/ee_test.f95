
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                      MODULE EE_TEST
                 !!!!!!!!!!!!!!!!!!!!!!!!!
  use testtools

contains
  SUBROUTINE TESTEE(ONLYFAST)
  logical onlyfast

  call testeematel
  call testeetwoloop
  call testeetwoloopmatel
  call testeenfbx
  call testeesoftn1
  call testeesoftn2


  if(.not.onlyfast) call testeevegas
  call testeepointwise
  END SUBROUTINE TESTEE


  SUBROUTINE TESTEETWOLOOP
  use ee_twoloop2, only: twoloop
  implicit none
  real(kind=prec) :: x
  complex(kind=prec) :: eemmA0, exchA0
  complex(kind=prec) :: eemmA1(-2:2), eemmA2(-4:0)
  complex(kind=prec) :: exchA1(-2:2), exchA2(-4:0)


  ! Test implementation
  call twoloop(1.3, (0.,0.), eemma0, eemma1, eemma2, excha0, excha1, excha2)
  call check("Re[eemmA0EF(1.3)]", real(eemmA0), 562.17266668605)
  call check("Im[eemmA0EF(1.3)]", aimag(eemmA0)+zero, zero)
  call check("Re[eemmA1ED(1.3)]", real(eemmA1(-2)), -357.89023509694925)
  call check("Im[eemmA1ED(1.3)]", aimag(eemmA1(-2))+zero, zero)
  call check("Re[eemmA1ES(1.3)]", real(eemmA1(-1)), -12.047634363572854)
  call check("Im[eemmA1ES(1.3)]", aimag(eemmA1(-1))+zero, zero)
  call check("Re[eemmA1EF(1.3)]", real(eemmA1(0)), -1953.4220580410247)
  call check("Im[eemmA1EF(1.3)]", aimag(eemmA1(0)), -0.01806909721642569)
  call check("Re[eemmA1E1(1.3)]", real(eemmA1(1)), -3139.283183603187)
  call check("Im[eemmA1E1(1.3)]", aimag(eemmA1(1)), -6277.614725390262)
  call check("Re[eemmA1E2(1.3)]", real(eemmA1(2)), 4694.662451535822)
  call check("Im[eemmA1E2(1.3)]", aimag(eemmA1(2)), -13541.63554454113)
  call check("Re[eemmA2EQ(1.3)]", real(eemmA2(-4)), 113.92000000000002)
  call check("Im[eemmA2EQ(1.3)]", aimag(eemmA2(-4))+zero, zero)
  call check("Re[eemmA2ET(1.3)]", real(eemmA2(-3)), 7.669762246105677)
  call check("Im[eemmA2ET(1.3)]", aimag(eemmA2(-3)), -357.89023509694925)
  call check("Re[eemmA2ED(1.3)]", real(eemmA2(-2)), 4616.752199383621)
  call check("Im[eemmA2ED(1.3)]", aimag(eemmA2(-2)), -2210.7536418773198)
  call check("Re[eemmA2ES(1.3)]", real(eemmA2(-1)), 15916.916673849908)
  call check("Im[eemmA2ES(1.3)]", aimag(eemmA2(-1)), 9564.715014583073)
  call check("Re[eemmA2EF(1.3)]", real(eemmA2(0)), 13473.456141951841)
  call check("Im[eemmA2EF(1.3)]", aimag(eemmA2(0)), 43662.49971613211)
  call check("Re[exchA0EF(1.3)]", real(exchA0), -43.729939500211344)
  call check("Im[exchA0EF(1.3)]", aimag(exchA0)+zero, zero)
  call check("Re[exchA1ED(1.3)]", real(exchA1(-2)), 27.839344130272636)
  call check("Im[exchA1ED(1.3)]", aimag(exchA1(-2))+zero, zero)
  call check("Re[exchA1ES(1.3)]", real(exchA1(-1)), 0.9371539262935755)
  call check("Im[exchA1ES(1.3)]", aimag(exchA1(-1)), -814.7971633372695)
  call check("Re[exchA1EF(1.3)]", real(exchA1(0)), 172.84543239904608)
  call check("Im[exchA1EF(1.3)]", aimag(exchA1(0)), 77.12367689671734)
  call check("Re[exchA1E1(1.3)]", real(exchA1(1)), 286.204047298462)
  call check("Im[exchA1E1(1.3)]", aimag(exchA1(1)), -2539.9449346037836)
  call check("Re[exchA1E2(1.3)]", real(exchA1(2)), -5483.747244533373)
  call check("Im[exchA1E2(1.3)]", aimag(exchA1(2)), -859.5702396475497)
  call check("Re[exchA2EQ(1.3)]", real(exchA2(-4)), -8.861538461538464)
  call check("Im[exchA2EQ(1.3)]", aimag(exchA2(-4))+zero, zero)
  call check("Re[exchA2ET(1.3)]", real(exchA2(-3)), -0.5966107192303447)
  call check("Im[exchA2ET(1.3)]", aimag(exchA2(-3)), 27.83934413027263)
  call check("Re[exchA2ED(1.3)]", real(exchA2(-2)), -66.31692216124483)
  call check("Im[exchA2ED(1.3)]", aimag(exchA2(-2)), 1118.4809642521261)
  call check("Re[exchA2ES(1.3)]", real(exchA2(-1)), -9057.81698114514)
  call check("Im[exchA2ES(1.3)]", aimag(exchA2(-1)), -3955.791905512548)
  call check("Re[exchA2EF(1.3)]", real(exchA2(0)), 17060.77682068512)
  call check("Im[exchA2EF(1.3)]", aimag(exchA2(0)), -39260.713371888174)

  call twoloop(-1.3, (0.,0.), eemma0, eemma1, eemma2, excha0, excha1, excha2)
  call check("Re[eemmA0EF(-1.3)]", real(eemmA0), 2204.4748390273194)
  call check("Im[eemmA0EF(-1.3)]", aimag(eemmA0)+zero, zero)
  call check("Re[eemmA1ED(-1.3)]", real(eemmA1(-2)), -1403.4122702116324)
  call check("Im[eemmA1ED(-1.3)]", aimag(eemmA1(-2))+zero, zero)
  call check("Re[eemmA1ES(-1.3)]", real(eemmA1(-1)), -2905.8280603970557)
  call check("Im[eemmA1ES(-1.3)]", aimag(eemmA1(-1))+zero, zero)
  call check("Re[eemmA1EF(-1.3)]", real(eemmA1(0)), -6448.094893731779)
  call check("Im[eemmA1EF(-1.3)]", aimag(eemmA1(0)), -8809.151498079482)
  call check("Re[eemmA1E1(-1.3)]", real(eemmA1(1)), 2415.732324309985)
  call check("Im[eemmA1E1(-1.3)]", aimag(eemmA1(1)), -15332.878079264865)
  call check("Re[eemmA1E2(-1.3)]", real(eemmA1(2)), 18452.075800756043)
  call check("Im[eemmA1E2(-1.3)]", aimag(eemmA1(2)), -13747.431915941404)
  call check("Re[eemmA2EQ(-1.3)]", real(eemmA2(-4)), 446.72)
  call check("Im[eemmA2EQ(-1.3)]", aimag(eemmA2(-4))+zero, zero)
  call check("Re[eemmA2ET(-1.3)]", real(eemmA2(-3)), 1849.907598349304)
  call check("Im[eemmA2ET(-1.3)]", aimag(eemmA2(-3)), -1403.4122702116326)
  call check("Re[eemmA2ED(-1.3)]", real(eemmA2(-2)), 613.1789615604557)
  call check("Im[eemmA2ED(-1.3)]", aimag(eemmA2(-2)), 5440.103239533875)
  call check("Re[eemmA2ES(-1.3)]", real(eemmA2(-1)), -1731.1032556089913)
  call check("Im[eemmA2ES(-1.3)]", aimag(eemmA2(-1)), 2647.178322921944)
  call check("Re[eemmA2EF(-1.3)]", real(eemmA2(0)), 26041.313476083353)
  call check("Im[eemmA2EF(-1.3)]", aimag(eemmA2(0)), 59354.56379126423)
  call check("Re[exchA0EF(-1.3)]", real(exchA0), 2570.3486661790866)
  call check("Im[exchA0EF(-1.3)]", aimag(exchA0)+zero, zero)
  call check("Re[exchA1ED(-1.3)]", real(exchA1(-2)), -1636.3347827682462)
  call check("Im[exchA1ED(-1.3)]", aimag(exchA1(-2))+zero, zero)
  call check("Re[exchA1ES(-1.3)]", real(exchA1(-1)), -3388.104571192511)
  call check("Im[exchA1ES(-1.3)]", aimag(exchA1(-1)), -4701.902896689388)
  call check("Re[exchA1EF(-1.3)]", real(exchA1(0)), -19103.160953795705)
  call check("Im[exchA1EF(-1.3)]", aimag(exchA1(0)), -550.6417046603215)
  call check("Re[exchA1E1(-1.3)]", real(exchA1(1)), 33308.80580893187)
  call check("Im[exchA1E1(-1.3)]", aimag(exchA1(1)), 29748.788155321145)
  call check("Re[exchA1E2(-1.3)]", real(exchA1(2)), 202856.78085903294)
  call check("Im[exchA1E2(-1.3)]", aimag(exchA1(2)), -139065.48430676703)
  call check("Re[exchA2EQ(-1.3)]", real(exchA2(-4)), 520.8615384615383)
  call check("Im[exchA2EQ(-1.3)]", aimag(exchA2(-4))+zero, zero)
  call check("Re[exchA2ET(-1.3)]", real(exchA2(-3)), 2156.9343608701383)
  call check("Im[exchA2ET(-1.3)]", aimag(exchA2(-3)), -1636.3347827682462)
  call check("Re[exchA2ED(-1.3)]", real(exchA2(-2)), -8610.025092861906)
  call check("Im[exchA2ED(-1.3)]", aimag(exchA2(-2)), -4219.8600447073695)
  call check("Re[exchA2ES(-1.3)]", real(exchA2(-1)), 4881.907598734346)
  call check("Im[exchA2ES(-1.3)]", aimag(exchA2(-1)), -36144.56434807241)
  call check("Re[exchA2EF(-1.3)]", real(exchA2(0)), -1.0179496285900541e6)
  call check("Im[exchA2EF(-1.3)]", aimag(exchA2(0)), 332991.7058567754)
  END SUBROUTINE


  SUBROUTINE TESTEETWOLOOPMATEL
  use ee
  implicit none
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),x(2)
  real (kind=prec) :: quad, trip, doub, sing, fin, lin, weight

  call initflavour("mu-e", mys=40000._prec)
  call blockstart("e-e two-loop matrix elements")

  musq = Mmu**2
  x = (/0.75_prec,0.12_prec/)
  call psx2(x,q1,0.,q2,0.,q3,0.,q4,0.,weight)

  fin = ee2mml0(q1, q2, q3, q4, doub, sing, lin, quad)
  call check("ee2mml0, doub", doub, -158.59765024970423)
  call check("ee2mml0, sing", sing, 280.5026038820837)
  call check("ee2mml0, fin ", fin , 357.12347855033465)
  call check("ee2mml0, lin ", lin , -103.93152204123044)
  call check("ee2mml0, quad", quad, 559.7528991098193)

  fin = em2eml0(q1, q2, q3, q4, doub, sing, lin, quad)
  call check("em2eml0, doub", doub, -263.37451138855255)
  call check("em2eml0, sing", sing, 465.81545265232114)
  call check("em2eml0, fin ", fin , -170.80596528241824)
  call check("em2eml0, lin ", lin , -2294.6363394827654)
  call check("em2eml0, quad", quad, -3072.9918507600064)

  fin = eb2ebl0(q1, q2, q3, q4, doub, sing, lin, quad)
  call check("eb2ebl0, doub", doub, -415.39195302564696)
  call check("eb2ebl0, sing", sing, 734.6800174649841)
  call check("eb2ebl0, fin ", fin , 204.0374075123038)
  call check("eb2ebl0, lin ", lin , -2311.1127273642787)
  call check("eb2ebl0, quad", quad, -2242.2356399203723)

  fin = ee2eel0(q1, q2, q3, q4, doub, sing, lin, quad)
  call check("ee2eel0, doub", doub, -28846.66340455881)
  call check("ee2eel0, sing", sing, -71305.61145815668)
  call check("ee2eel0, fin ", fin , -161280.82568771878)
  call check("ee2eel0, lin ", lin , -189348.346321929)
  call check("ee2eel0, quad", quad, -315099.3742805749)


  fin = ee2mmll0(q1, q2, q3, q4, quad, trip, doub, sing)
  call check("ee2mmll0, quad", quad,   50.4832)
  call check("ee2mmll0, trip", trip, -178.573503831926)
  call check("ee2mmll0, doub", doub, - 69.43549013976539)
  call check("ee2mmll0, sing", sing,  428.48155540244045)
  call check("ee2mmll0, fin ", fin , -304.53619219766506)

  fin = em2emll0(q1, q2, q3, q4, quad, trip, doub, sing)
  call check("em2emll0, quad", quad, 83.83471074380162)
  call check("em2emll0, trip", trip, -296.5473274328225)
  call check("em2emll0, doub", doub, 370.98161341006545)
  call check("em2emll0, sing", sing, 1202.4183176809915)
  call check("em2emll0, fin ", fin , -1129.2924755651984)

  fin = eb2ebll0(q1, q2, q3, q4, quad, trip, doub, sing)
  call check("eb2ebll0, quad", quad, 132.22336528925615)
  call check("eb2ebll0, trip", trip, -467.71182548156906)
  call check("eb2ebll0, doub", doub, 283.7133455906613)
  call check("eb2ebll0, sing", sing, 1596.8267581568937)
  call check("eb2ebll0, fin ", fin , -1489.913741252969)

  fin = ee2eell0(q1, q2, q3, q4, quad, trip, doub, sing)
  call check("ee2eell0, quad", quad, 9182.178145087235)
  call check("ee2eell0, trip", trip, 45394.562135022905)
  call check("ee2eell0, doub", doub, 158779.6129633312)
  call check("ee2eell0, sing", sing, 367105.7357094453)
  call check("ee2eell0, fin ", fin , 773067.6371521488)


  call psx2(x,q1,me,q2,me,q3,me,q4,me,weight)
  fin = ee2eellz(q1, q2, q3, q4, doub, sing)
  call check("ee2eellz, doub, global", doub, 6.93611885490377e5, 1e-5)
  call check("ee2eellz, sing, global", sing, 9.53471732001411e6, 1e-5)
  call check("ee2eellz, fin , global", fin , 7.46204073496466e7, 1e-5)

  xieik2 = 0.4
  fin = ee2eeffz(q1, q2, q3, q4)
  call check('ee2eeffz, global', fin, 222078.09152968228, 1e-2)

  fin = eb2ebllz(q1, q2, q3, q4, doub, sing)
  call check("en2enllz, doub, global", doub, 2.2112043372658670e4, 5e-5)
  call check("en2enllz, sing, global", sing, 2.2508879759097495e5, 1e-5)
  call check("en2enllz, fin , global", fin , 1.4723352373935615e6, 1e-5)
  fin = eb2ebffz(q1, q2, q3, q4)
  call check('ee2ebffz, global', fin, 7990.396144254599, 1e-2)
  END SUBROUTINE TESTEETWOLOOPMATEL

  SUBROUTINE TESTEENFBX
  use ee_ee2ee_nfbx
  implicit none
  real (kind=prec) :: x(2)
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec) :: weight

  call blockstart("e-e nf-box matrix element")
  Nel = 1
  Nmu = 0
  Ntau = 0
  Nhad = 0

  print*, "----------------------------------------------"
  print*, "   Basis Functions"
  print*, "----------------------------------------------"

  print*, "small qq2:"
  qq2 = (0.5*(1.E-6)**2)/(1 - 1.E-6)
  call check("d1:",d1(1E-6,1000.,-100.,0.5),-2.000001000001)
  call check("d2:",d2(1E-6,1000.,-100.,0.5),-1.000001500002E-8)
  call check("d12:",d12(1E-6,1000.,-100.,0.5),0.0200000200000217)
  call check("d13:",d13(1E-6,1000.,-100.,0.5),-15215.031662114594)
  call check("d14:",d14(1E-6,1000.,-100.,0.5),16672.830457234144)
  call check("d123:",d123(1E-6,1000.,-100.,0.5),152.1503567013471)
  call check("d124:",d124(1E-6,1000.,-100.,0.5),-166.72834901683117)

  print*, "moderate qq2:"
  qq2 = (0.5*(0.3)**2)/(1 - 0.3)
  call check("d1:",d1(0.3,1000.,-100.,0.5),-2.4285714285714284)
  call check("d2:",d2(0.3,1000.,-100.,0.5),-0.005204081632653061)
  call check("d12:",d12(0.3,1000.,-100.,0.5),0.028879849410923447)
  call check("d13:",d13(0.3,1000.,-100.,0.5),-0.058691412520658806)
  call check("d14:",d14(0.3,1000.,-100.,0.5),0.0642771301217612)
  call check("d123:",d123(0.3,1000.,-100.,0.5),0.0006451932275647874)
  call check("d124:",d124(0.3,1000.,-100.,0.5),-0.0007073882761582393)

  print*, "high qq2:"
  qq2 = (0.5*(0.999)**2)/(1 - 0.999)
  call check("d1:",d1(0.999,1000.,-100.,0.5),-1001.)
  call check("d2:",d2(0.999,1000.,-100.,0.5),-1002.002002002002)
  call check("d12:",d12(0.999,1000.,-100.,0.5),2.2385973500671787)
  call check("d13:",d13(0.999,1000.,-100.,0.5),0.002008020714773969,3E-8)
  call check("d14:",d14(0.999,1000.,-100.,0.5),1.146790464093073)
  call check("d123:",d123(0.999,1000.,-100.,0.5),-5.032627063810619E-6,3E-8)
  call check("d124:",d124(0.999,1000.,-100.,0.5),-0.0028741579624413332)

  print*, "ultra-high qq2:"
  qq2 = (0.5*(0.9999999999)**2)/(1 - 0.9999999999)
  call check("d1:",d1(0.9999999999,1000.,-100.,0.5),-1.0000000001E10,1E-7)
  call check("d2:",d2(0.9999999999,1000.,-100.,0.5),-1.0000000002E10,1E-7)
  call check("d12:",d12(0.9999999999,1000.,-100.,0.5),2.0000000206000004)
  call check("d13:",d13(0.9999999999,1000.,-100.,0.5),2.000000200200027)
  call check("d14:",d14(0.9999999999,1000.,-100.,0.5),1.9999998206000216)
  call check("d123:",d123(0.9999999999,1000.,-100.,0.5),-4.0000004812000638E-10,1E-7)
  call check("d124:",d124(0.9999999999,1000.,-100.,0.5),-3.9999997220000377E-10,1E-7)

  print*, "qq2~-tt:"
  qq2 = (0.5*(0.9950494836207796)**2)/(1 - 0.9950494836207796)
  call check("d1:",d1(0.9950494836207796,1000.,-100.,0.5),-202.99912966604072)
  call check("d2:",d2(0.9950494836207796,1000.,-100.,0.5),-204.0090799578819)
  call check("d12:",d12(0.9950494836207796,1000.,-100.,0.5),21.83575401576958)
  call check("d13:",d13(0.9950494836207796,1000.,-100.,0.5),-0.4462178162876954)
  call check("d14:",d14(0.9950494836207796,1000.,-100.,0.5),0.52026559470153)
  call check("d123:",d123(0.9950494836207796,1000.,-100.,0.5),218.72444433209944)
  call check("d124:",d124(0.9950494836207796,1000.,-100.,0.5),-255.02075209124573)

  print*, "----------------------------------------------"
  print*, "   Kernels for nel"
  print*, "----------------------------------------------"

  print*, "small qq2:"
  qq2 = (0.5*(1.E-6)**2)/(1 - 1.E-6)
  call check("gijk_012:",gijk(0,1,2,1.E-6,1000.,-100.,0.5),-8.126802795994905E-16)
  call check("gijk_013:",gijk(0,1,3,1.E-6,1000.,-100.,0.5),6.182471910162576E-10)
  call check("gijk_014:",gijk(0,1,4,1.E-6,1000.,-100.,0.5),-6.774833484009138E-10)
  call check("gijk_023:",gijk(0,2,3,1.E-6,1000.,-100.,0.5),-8.126802795994905E-16)
  call check("gijk_024:",gijk(0,2,4,1.E-6,1000.,-100.,0.5),-8.126802795994905E-16)
  call check("gijk_123:",gijk(1,2,3,1.E-6,1000.,-100.,0.5),-3.468211837111311E-11)
  call check("gijk_124:",gijk(1,2,4,1.E-6,1000.,-100.,0.5),3.800511850111754E-11)
  call check("g012k_3:",g012k(3,1.E-6,1000.,-100.,0.5),-5.4888311713580805E-12)
  call check("g012k_4:",g012k(4,1.E-6,1000.,-100.,0.5),6.014732919942934E-12)
  call check("gp13:",gp13(1.E-6,1000.,-100.,0.5),-2.029669337932528E-19,2E-4)
  call check("gp14:",gp14(1.E-6,1000.,-100.,0.5),1.8264992314620234E-19,2E-4)
  call check("gp133:",gp133(1.E-6,1000.,-100.,0.5),8.14308493337195E-15)
  call check("gp134:",gp134(1.E-6,1000.,-100.,0.5),-9.029776331909572E-15)
  call check("gp2:",gp2(1.E-6,1000.,-100.,0.5),2.029669337932528E-19,2E-4)
  call check("gp4:",gp4(1.E-6,1000.,-100.,0.5),1.8264992314620234E-19,2E-4)
  call check("gij_013:",gij(0,1,3,1.E-6,1000.,-100.,0.5),-0.9117849417370932)
  call check("gij_014:",gij(0,1,4,1.E-6,1000.,-100.,0.5),-0.9117849417370932)
  call check("gij_023:",gij(0,2,3,1.E-6,1000.,-100.,0.5),-4.558929267614751E-9)
  call check("gij_024:",gij(0,2,4,1.E-6,1000.,-100.,0.5),-4.558929267614715E-9)
  call check("gij_033:",gij(0,3,3,1.E-6,1000.,-100.,0.5),-0.9117849417370932)
  call check("gij_044:",gij(0,4,4,1.E-6,1000.,-100.,0.5),-0.9117849417370932)
  call check("gij_123:",gij(1,2,3,1.E-6,1000.,-100.,0.5),-4.5589315644219715E-15)
  call check("gij_124:",gij(1,2,4,1.E-6,1000.,-100.,0.5),-4.5589315280783165E-15)
  call check("gij_133:",gij(1,3,3,1.E-6,1000.,-100.,0.5),3.46821092349774E-9)
  call check("gij_144:",gij(1,4,4,1.E-6,1000.,-100.,0.5),-3.800510837015817E-9)
  call check("gij_233:",gij(2,3,3,1.E-6,1000.,-100.,0.5),-4.5589315644219715E-15)
  call check("gij_244:",gij(2,4,4,1.E-6,1000.,-100.,0.5),-4.5589315280783165E-15)

  print*, "moderate qq2:"
  qq2 = (0.5*(0.3)**2)/(1 - 0.3)
  call check("gijk_012:",gijk(0,1,2,0.3,1000.,-100.,0.5),-0.00014703829668553651)
  call check("gijk_013:",gijk(0,1,3,0.3,1000.,-100.,0.5),0.00029882030215301934)
  call check("gijk_014:",gijk(0,1,4,0.3,1000.,-100.,0.5),-0.00032725931477203476)
  call check("gijk_023:",gijk(0,2,3,0.3,1000.,-100.,0.5),-0.00014703829668553651)
  call check("gijk_024:",gijk(0,2,4,0.3,1000.,-100.,0.5),-0.00014703829668553651)
  call check("gijk_123:",gijk(1,2,3,0.3,1000.,-100.,0.5),-0.000018697735513086997)
  call check("gijk_124:",gijk(1,2,4,0.3,1000.,-100.,0.5),0.000020500151470261908)
  call check("g012k_3:",g012k(3,0.3,1000.,-100.,0.5),-2.9069888701000527E-6)
  call check("g012k_4:",g012k(4,0.3,1000.,-100.,0.5),3.1872154848754556E-6)
  call check("gp13:",gp13(0.3,1000.,-100.,0.5),-0.01167083299674891)
  call check("gp14:",gp14(0.3,1000.,-100.,0.5),0.010496582309575384)
  call check("gp133:",gp133(0.3,1000.,-100.,0.5),0.0012408808249201768)
  call check("gp134:",gp134(0.3,1000.,-100.,0.5),-0.0013715269619255124)
  call check("gp2:",gp2(0.3,1000.,-100.,0.5),0.01167083299674891)
  call check("gp4:",gp4(0.3,1000.,-100.,0.5),0.010496582309575384)
  call check("gij_013:",gij(0,1,3,0.3,1000.,-100.,0.5),-1.0948032969094386)
  call check("gij_014:",gij(0,1,4,0.3,1000.,-100.,0.5),-1.0948007770452752)
  call check("gij_023:",gij(0,2,3,0.3,1000.,-100.,0.5),-0.0023472064863804603)
  call check("gij_024:",gij(0,2,4,0.3,1000.,-100.,0.5),-0.002344686622217245)
  call check("gij_033:",gij(0,3,3,0.3,1000.,-100.,0.5),-1.0948032969094386)
  call check("gij_044:",gij(0,4,4,0.3,1000.,-100.,0.5),-1.0948007770452752)
  call check("gij_123:",gij(1,2,3,0.3,1000.,-100.,0.5),-0.0008381416347019229)
  call check("gij_124:",gij(1,2,4,0.3,1000.,-100.,0.5),-0.0008356217705387075)
  call check("gij_133:",gij(1,3,3,0.3,1000.,-100.,0.5),0.0016996783922133012)
  call check("gij_144:",gij(1,4,4,0.3,1000.,-100.,0.5),-0.0018614369284439111)
  call check("gij_233:",gij(2,3,3,0.3,1000.,-100.,0.5),-0.0008381416347019229)
  call check("gij_244:",gij(2,4,4,0.3,1000.,-100.,0.5),-0.0008356217705387075)

  print*, "high qq2:"
  qq2 = (0.5*(0.999)**2)/(1 - 0.999)
  call check("gijk_012:",gijk(0,1,2,0.999,1000.,-100.,0.5),-1.399445487833029,4E-8)
  call check("gijk_013:",gijk(0,1,3,0.999,1000.,-100.,0.5),-0.0012553019097790654,4E-8)
  call check("gijk_014:",gijk(0,1,4,0.999,1000.,-100.,0.5),-0.716909068268501,4E-8)
  call check("gijk_023:",gijk(0,2,3,0.999,1000.,-100.,0.5),-1.399445487833029,4E-8)
  call check("gijk_024:",gijk(0,2,4,0.999,1000.,-100.,0.5),-1.399445487833029,4E-8)
  call check("gijk_123:",gijk(1,2,3,0.999,1000.,-100.,0.5),-0.0004250388689135175,4E-8)
  call check("gijk_124:",gijk(1,2,4,0.999,1000.,-100.,0.5),-0.242741779580593,4E-8)
  call check("g012k_3:",g012k(3,0.999,1000.,-100.,0.5),-6.765005570980833E-7,1E-7)
  call check("g012k_4:",g012k(4,0.999,1000.,-100.,0.5),-0.00038635278118685204,1E-7)
  call check("gp13:",gp13(0.999,1000.,-100.,0.5),-372.4465803235377,2E-8)
  call check("gp14:",gp14(0.999,1000.,-100.,0.5),266.19683101564857,2E-8)
  call check("gp133:",gp133(0.999,1000.,-100.,0.5),62.63954429241677,2E-8)
  call check("gp134:",gp134(0.999,1000.,-100.,0.5),-29.781229422048217,2E-8)
  call check("gp2:",gp2(0.999,1000.,-100.,0.5),372.4465803235377,2E-8)
  call check("gp4:",gp4(0.999,1000.,-100.,0.5),266.19683101564857,2E-8)
  call check("gij_013:",gij(0,1,3,0.999,1000.,-100.,0.5),169.20880953928068,1E-9)
  call check("gij_014:",gij(0,1,4,0.999,1000.,-100.,0.5),48.29263476578225,1E-9)
  call check("gij_023:",gij(0,2,3,0.999,1000.,-100.,0.5),169.3784000339227,1E-9)
  call check("gij_024:",gij(0,2,4,0.999,1000.,-100.,0.5),48.46222526042429,1E-9)
  call check("gij_033:",gij(0,3,3,0.999,1000.,-100.,0.5),169.20880953928068,1E-9)
  call check("gij_044:",gij(0,4,4,0.999,1000.,-100.,0.5),48.29263476578225,1E-9)
  call check("gij_123:",gij(1,2,3,0.999,1000.,-100.,0.5),188.85235891905288,1E-9)
  call check("gij_124:",gij(1,2,4,0.999,1000.,-100.,0.5),67.93618414555445,1E-9)
  call check("gij_133:",gij(1,3,3,0.999,1000.,-100.,0.5),-0.04250388689135175,3E-8)
  call check("gij_144:",gij(1,4,4,0.999,1000.,-100.,0.5),-24.2741779580593,1E-9)
  call check("gij_233:",gij(2,3,3,0.999,1000.,-100.,0.5),188.85235891905288,1E-9)
  call check("gij_244:",gij(2,4,4,0.999,1000.,-100.,0.5),67.93618414555445,1E-9)

  print*, "ultra-high qq2:"
  qq2 = (0.5*(0.9999999999)**2)/(1 - 0.9999999999)
  call check("gijk_012:",gijk(0,1,2,0.9999999999,1000.,-100.,0.5),-4.670415149170747,2E-8)
  call check("gijk_013:",gijk(0,1,3,0.9999999999,1000.,-100.,0.5),-4.670415568574084,2E-8)
  call check("gijk_014:",gijk(0,1,4,0.9999999999,1000.,-100.,0.5),-4.6704146821292865,2E-8)
  call check("gijk_023:",gijk(0,2,3,0.9999999999,1000.,-100.,0.5),-4.670415149170747,2E-8)
  call check("gijk_024:",gijk(0,2,4,0.9999999999,1000.,-100.,0.5),-4.670415149170747,2E-8)
  call check("gijk_123:",gijk(1,2,3,0.9999999999,1000.,-100.,0.5),-3.7586310486377794,2E-8)
  call check("gijk_124:",gijk(1,2,4,0.9999999999,1000.,-100.,0.5),-3.758630335249668,2E-8)
  call check("g012k_3:",g012k(3,0.9999999999,1000.,-100.,0.5),5.693692944667829E-10,1E-7)
  call check("g012k_4:",g012k(4,0.9999999999,1000.,-100.,0.5),5.693691864005002E-10,1E-7)
  call check("gp13:",gp13(0.9999999999,1000.,-100.,0.5),-1283.1965586229917,3E-6)
  call check("gp14:",gp14(0.9999999999,1000.,-100.,0.5),932.9154220498764,3E-6)
  call check("gp133:",gp133(0.9999999999,1000.,-100.,0.5),-233.52078618937887,2E-8)
  call check("gp134:",gp134(0.9999999999,1000.,-100.,0.5),-233.5207270930589,2E-8)
  call check("gp2:",gp2(0.9999999999,1000.,-100.,0.5),1283.1965586229917,3E-6)
  call check("gp4:",gp4(0.9999999999,1000.,-100.,0.5),932.9154220498764,3E-6)
  call check("gij_013:",gij(0,1,3,0.9999999999,1000.,-100.,0.5),-2251.4200229539047,5E-6)
  call check("gij_014:",gij(0,1,4,0.9999999999,1000.,-100.,0.5),1315.5205327118274,5E-6)
  call check("gij_023:",gij(0,2,3,0.9999999999,1000.,-100.,0.5),-2249.5407076549154,5E-6)
  call check("gij_024:",gij(0,2,4,0.9999999999,1000.,-100.,0.5),1317.3998480108164,5E-6)
  call check("gij_033:",gij(0,3,3,0.9999999999,1000.,-100.,0.5),-2251.4200229539047,5E-6)
  call check("gij_044:",gij(0,4,4,0.9999999999,1000.,-100.,0.5),1315.5205327118274,5E-6)
  call check("gij_123:",gij(1,2,3,0.9999999999,1000.,-100.,0.5),-2063.488490536726,5E-6)
  call check("gij_124:",gij(1,2,4,0.9999999999,1000.,-100.,0.5),1503.4520651290056,5E-6)
  call check("gij_133:",gij(1,3,3,0.9999999999,1000.,-100.,0.5),-375.86310486377795,5E-6)
  call check("gij_144:",gij(1,4,4,0.9999999999,1000.,-100.,0.5),-375.86303352496685,5E-6)
  call check("gij_233:",gij(2,3,3,0.9999999999,1000.,-100.,0.5),-2063.488490536726,5E-6)
  call check("gij_244:",gij(2,4,4,0.9999999999,1000.,-100.,0.5),1503.4520651290056,5E-6)

  print*, "qq2~-tt:"
  qq2 = (0.5*(0.9950494836207796)**2)/(1 - 0.9950494836207796)
  call check("gijk_012:",gijk(0,1,2,0.9950494836207796,1000.,-100.,0.5),-9.95479742182172)
  call check("gijk_013:",gijk(0,1,3,0.9950494836207796,1000.,-100.,0.5),0.20342819230990106)
  call check("gijk_014:",gijk(0,1,4,0.9950494836207796,1000.,-100.,0.5),-0.23718615794338996)
  call check("gijk_023:",gijk(0,2,3,0.9950494836207796,1000.,-100.,0.5),-9.95479742182172)
  call check("gijk_024:",gijk(0,2,4,0.9950494836207796,1000.,-100.,0.5),-9.95479742182172)
  call check("gijk_123:",gijk(1,2,3,0.9950494836207796,1000.,-100.,0.5),0.04662683910517633,1E-9)
  call check("gijk_124:",gijk(1,2,4,0.9950494836207796,1000.,-100.,0.5),-0.054364346941420075,1E-9)
  call check("g012k_3:",g012k(3,0.9950494836207796,1000.,-100.,0.5),0.0005508669514036149,1E-9)
  call check("g012k_4:",g012k(4,0.9950494836207796,1000.,-100.,0.5),-0.0006422807687459957,1E-9)
  call check("gp13:",gp13(0.9950494836207796,1000.,-100.,0.5),-909.3188563878351)
  call check("gp14:",gp14(0.9950494836207796,1000.,-100.,0.5),-39.89795950586903)
  call check("gp133:",gp133(0.9950494836207796,1000.,-100.,0.5),11.311562676824655)
  call check("gp134:",gp134(0.9950494836207796,1000.,-100.,0.5),-7.647451288435335)
  call check("gp2:",gp2(0.9950494836207796,1000.,-100.,0.5),909.3188563878351)
  call check("gp4:",gp4(0.9950494836207796,1000.,-100.,0.5),-39.89795950586903)
  call check("gij_013:",gij(0,1,3,0.9950494836207796,1000.,-100.,0.5),4.663211770461698,1E-9)
  call check("gij_014:",gij(0,1,4,0.9950494836207796,1000.,-100.,0.5),-5.43611286542888,1E-9)
  call check("gij_023:",gij(0,2,3,0.9950494836207796,1000.,-100.,0.5),4.663213923391117,1E-9)
  call check("gij_024:",gij(0,2,4,0.9950494836207796,1000.,-100.,0.5),-5.436110712499461,1E-9)
  call check("gij_033:",gij(0,3,3,0.9950494836207796,1000.,-100.,0.5),4.663211770461698,1E-9)
  call check("gij_044:",gij(0,4,4,0.9950494836207796,1000.,-100.,0.5),-5.43611286542888,1E-9)
  call check("gij_123:",gij(1,2,3,0.9950494836207796,1000.,-100.,0.5),4.66743389591013,1E-9)
  call check("gij_124:",gij(1,2,4,0.9950494836207796,1000.,-100.,0.5),-5.431890739980449,1E-9)
  call check("gij_133:",gij(1,3,3,0.9950494836207796,1000.,-100.,0.5),4.662683910517633,1E-9)
  call check("gij_144:",gij(1,4,4,0.9950494836207796,1000.,-100.,0.5),-5.436434694142007,1E-9)
  call check("gij_233:",gij(2,3,3,0.9950494836207796,1000.,-100.,0.5),4.66743389591013,1E-9)
  call check("gij_244:",gij(2,4,4,0.9950494836207796,1000.,-100.,0.5),-5.431890739980449,1E-9)


  print*, "----------------------------------------------"
  print*, "   matrix element"
  print*, "----------------------------------------------"
  call initflavour("mu-e", mys=40000._prec)
  x = (/0.75_prec,0.5_prec/)
  call psx2(x,q1,me,q2,me,q3,me,q4,me,weight)
  call check("z=0.01:",ee2ee_nfbx(0.01,q1,q2,q3,q4),-35.08063910910691,1E-7)
  call check("z=0.1:",ee2ee_nfbx(0.1,q1,q2,q3,q4),-314583.25180208264,3E-5)
  call check("z=0.2:",ee2ee_nfbx(0.2,q1,q2,q3,q4),-44391.015071814036,3E-7)
  call check("z=0.9:",ee2ee_nfbx(0.9,q1,q2,q3,q4),-10520.973491156681,2E-8)
  call check("z=0.999999:",ee2ee_nfbx(0.999999,q1,q2,q3,q4),-12261.822429444028)

  x = (/0.75,0.999/)
  call psx2(x,q1,me,q2,me,q3,me,q4,me,weight)
  call check("z=0.01:",ee2ee_nfbx(0.01,q1,q2,q3,q4),-5255.144098756852,4E-7)
  call check("z=0.1:",ee2ee_nfbx(0.1,q1,q2,q3,q4),-1.1900735635139713E8,4E-7)
  call check("z=0.2:",ee2ee_nfbx(0.2,q1,q2,q3,q4),-1.5848058499445662E7,4E-7)
  call check("z=0.9:",ee2ee_nfbx(0.9,q1,q2,q3,q4),753096.4007021476,4E-7)
  call check("z=0.999999:",ee2ee_nfbx(0.999999,q1,q2,q3,q4),732967.1791820877,4E-7)

  Nhad = 1
  Nhad = 1
  Nmu = 1
  Ntau = 1

  END SUBROUTINE TESTEENFBX



  SUBROUTINE TESTEEMATEL
  implicit none
  real (kind=prec) :: x(2),y(5), y8(8)
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q5(4)
  real (kind=prec) :: weight,pole,tt,img(2),mat
  real(kind=prec), parameter :: xicut = .8
  integer :: ido

  call blockstart("e-e matrix elements")
  Nel = 1
  Nmu = 0
  Ntau = 0
  Nhad = 0

  print*, "----------------------------------------------"
  print*, "   Test Born and NLO Matrixelements"
  print*, "----------------------------------------------"
  call initflavour("mu-e", mys=40000._prec)
  musq = mm**2
  x = (/0.75_prec,0.5_prec/)
  call psx2(x,q1,me,q2,me,q3,me,q4,me,weight)

  call check("ee2ee:",ee2ee(q1,q2,q3,q4),5684.9581122306845)
  mat = ee2eel(q1,q2,q3,q4,pole=pole,img=img)
  call checkcplx("ee2eel fin:",mat+imag*img(2),cmplx(221816.84997000592,-5947.221689801129))
  call checkcplx("ee2eel pol:",pole+imag*img(1),cmplx(34574.270918126174,11369.916225430448))

  mat = eb2ebl(q1,q2,q3,q4,pole=pole,img=img)
  call check("eb2eb:",eb2eb(q1,q2,q3,q4),1421.2395281653537)
  call checkcplx("eb2ebl fin:",mat+imag*img(2),cmplx(57773.03654587164,1171.4351145480273))
  call checkcplx("eb2ebl pol:",pole+imag*img(1),cmplx(9897.848244348135,-2842.479056572977))

  call check("ee2ee_a:",ee2ee_a(q1,q2,q3,q4),11556.749914309688)
  call check("ee2ee_aa:",ee2ee_aa(q1,q2,q3,q4),22003.209838423434)
  call check("ee2ee_al fin:",ee2ee_al(q1,q2,q3,q4,pole),516785.2504133776)
  call check("ee2ee_al sin:",pole,70284.81029802897)
  call check("ee2ee_nf:",ee2ee_nf(0.2,q1,q2,q3,q4),-165068.77190919596,1E-7)

  call check("eb2eb_a:",eb2eb_a(q1,q2,q3,q4),2889.1868723102534)
  call check("eb2eb_aa:",eb2eb_aa(q1,q2,q3,q4),5484.108415474398)

  x = (/0.75,0.999/)
  call psx2(x,q1,me,q2,me,q3,me,q4,me,weight)

  call check("ee2ee:",ee2ee(q1,q2,q3,q4),6.316724240508085E8)
  mat = ee2eel(q1,q2,q3,q4,pole=pole,img=img)
  call checkcplx("ee2eel fin:",mat+imag*img(2),cmplx(1.510198781026782E10,7.1142875965165415E9))
  call checkcplx("ee2eel pol:",pole+imag*img(1),cmplx(1.6262255823703682E9,1.2633448482092943E9))

  mat = eb2ebl(q1,q2,q3,q4,pole=pole,img=img)
  call check("eb2eb:",eb2eb(q1,q2,q3,q4),6.304097273203214E8)
  call checkcplx("eb2ebl fin:",mat+imag*img(2),cmplx(1.5086721590719173E10,-7.105962945951941E9))
  call checkcplx("eb2ebl pol:",pole+imag*img(1),cmplx(1.6237778534705365E9,-1.2608194547481048E9))

  call check("ee2ee_a:",ee2ee_a(q1,q2,q3,q4),4.5657991256334484E8)
  call check("ee2ee_aa:",ee2ee_aa(q1,q2,q3,q4),5.2402348039232075E8)
  call check("ee2ee_al fin:",ee2ee_al(q1,q2,q3,q4,pole),1.0921431709612024E10)
  call check("ee2ee_al sin:",pole,1.1754540897090263E9)
  call check("ee2ee_nf:",ee2ee_nf(0.1,q1,q2,q3,q4),-1.3377149786440775E8,5E-6)

  call check("eb2eb_a:",eb2eb_a(q1,q2,q3,q4),4.5474779193206525E8)
  call check("eb2eb_aa:",eb2eb_aa(q1,q2,q3,q4),5.2114705894641376E8)

  print*, "----------------------------------------------"
  print*, "   Test Real Matrixelement"
  print*, "----------------------------------------------"
  call initflavour("mu-e", mys=40000.)
  y = (/0.3,0.6,0.8,0.4,0.9/)
  call psx3_fks(y,q1,me,q2,me,q3,me,q4,me,q5,weight)
  call check("ee2eeg",ee2eeg(q1,q2,q3,q4,q5),434.98382653487744)
  call check("ee2eegnosymm",ee2eeg(q1,q2,q3,q4,q5),434.98382653487744)
  call check("eb2ebg",eb2ebg(q1,q2,q3,q4,q5),746.2083842238877006690351)

  call check("ee2eega:",ee2eeg_a(q1,q2,q3,q4,q5),719.4086750886686)
  call check("eb2ebga:",eb2ebg_a(q1,q2,q3,q4,q5),1210.646304794795)

  y = (/0.1,0.22,0.3,0.8,0.1/)
  call psx3_fks(y,q1,me,q2,me,q3,me,q4,me,q5,weight)
  call check("ee2eeg",ee2eeg(q1,q2,q3,q4,q5),6662.551234425908)
  call check("eb2ebg",eb2ebg(q1,q2,q3,q4,q5),1460.319691646053102750644845)

  call check("ee2eega:",ee2eeg_a(q1,q2,q3,q4,q5),11315.890773733303)
  call check("eb2ebga:",eb2ebg_a(q1,q2,q3,q4,q5),3146.448827918929)


  print*, "----------------------------------------------"
  print*, "   Test Real Virtual Matrixelement"
  print*, "----------------------------------------------"
  call initflavour("mu-e", mys=40000.0_prec)
  y = (/0.3,0.6,0.8,0.4,0.9/)
  call psx3_fks(y,q1,me,q2,me,q3,me,q4,me,q5,weight)
  call check("ee2eegl",ee2eegl(q1,q2,q3,q4,q5),16514.08549000298)
  call check("ee2eegl_nts",ee2eegl_nts(q1,q2,q3,q4,q5),-20340.6224405762639719928)
  call check("eb2ebgl",eb2ebgl(q1,q2,q3,q4,q5),28540.91168062856)
  call check("eb2ebgl_nts",eb2ebgl_nts(q1,q2,q3,q4,q5),27119.2886657658542548729)

  print*, "----------------------------------"
  print*, "   Test RR   Matrixelement"
  print*, "----------------------------------"

  ran_seed = 23312
  do ido = 1,8
    y8(ido) = ran2(ran_seed)
  enddo
  call psx4_fkss(y8,p1,me,p2,me,q1,me,q2,me,q3,q4,weight)
  call check("RR matrix element", ee2eegg(p1,p2,q1,q2,q3,q4), 282.35916275047657)

  call blockend(40)
  END SUBROUTINE



  SUBROUTINE TESTEESPEED
  use ee_mat_el
  use olinterface
  implicit none
  integer, parameter :: niter = 2000
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec) :: weight, arr(5), finite
  real (kind=prec) :: startt, endt, rate(4)
  integer k, i, j, ranseed


  call blockstart("e-e performance")

  ! On my notebook I observed the following event rates
  !   CDR (ref)   :  5.6 kEv/s
  !   FDF (MMA+O3): 53.0 kEv/s
  !   FDF (O3)    : 52.0 kEv/s
  ! It seems that O3 optimisation suffices!

  ranseed = 2332

  do k=1,4
    call cpu_time(startt)
    do j=1,niter
      do i=1,5
        arr(i) = ran2(ranseed)
      enddo
      call psx3_fks(arr,p1,me,p2,me,q1,me,q2,me,q3,weight)
      if (weight.lt.zero) cycle
      select case(k)
        case(1)
          finite=ee2eegl_coll(p1, p2, q1, q2, q3)
        case(2)
          finite=ee2eegl_pvred(p1, p2, q1, q2, q3)
        case(3)
          call openloops("ee2eeg", p1, p2, q1, q2, q3, fin=finite,reload=.true.)
        case(4)
          finite=ee2eegl_nts(p1, p2, q1, q2, q3)
      end select
    enddo
    call cpu_time(endt)
    rate(k)=niter/(endt-startt)
  enddo
  print*,"rate is",rate,"Ev/s"
  END SUBROUTINE




  SUBROUTINE TESTEESOFTN1
  implicit none
  call blockstart("e-e \xi->0")
  call initflavour("muone")
  musq = Me**2
  xinormcut1 = 0.3
  xinormcut2 = 0.3
  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 1

  call test_softlimit( &
    (/0.01,0.6,0.8,0.999,0.01/), &
    ["ee2eeR ", "ee2eeRF", "ee2eeAR", "eb2ebR ", "eb2ebRF"])
  END SUBROUTINE

  SUBROUTINE TESTEESOFTN2
  implicit none
  real (kind=prec) :: yold(8)
  integer ido

  call blockstart("e-e \xi_{1,2} -> 0")
  call initflavour('muone')

  ran_seed = 23312
  do ido = 1,8
    yold(ido) = ran2(ran_seed)
  enddo

  call test_softlimit2(yold, "ee2eeRR", (/9,9,9/))
  call test_softlimit2(yold, "eb2ebRR", (/9,9,9/))
  call blockend(6)
  ENDSUBROUTINE



  SUBROUTINE TESTEEVEGAS
  use olinterface
  call blockstart("ee -> ee VEGAS test")
  xinormcut = 0.8
  xinormcut1 = 0.2
  xinormcut2 = 0.3
  ! get muon mass
  call initflavour("mu-e", mys=40000._prec)
  call initflavour("prad")

  xinormcut = 0.8
  xinormcut1 = 0.2
  xinormcut2 = 0.3
  musq = Mm**2
  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 1

  call test_INT("ee2ee0"             , 10, 10,  1.4289914618053258E+03)
  call test_INT("ee2eeF"             , 10, 10,  3.2909994555182458E+01)
  call test_INT("ee2eeR"             , 10, 10, -2.1444195707390882E+01)
  call test_INT("ee2eeR125"          , 10, 10, -9.1567742562646937E-02)
  call test_INT("ee2eeR345"          , 10, 10, -2.1536219169372558E+01)
  call test_INT("ee2eeR35"           , 10, 10, -1.0768109584686279E+01)
  call test_INT("ee2eeR45"           , 10, 10, -7.3969124206065064E+00)
  call openloops("ee2eeg", reload=.true.)
  call test_INT("ee2eeRF"            ,  1,  2, -6.8542191587166634E-01)
  call test_INT("ee2eeRF125"         ,  1,  2,  3.7203625197693546E-01)
  call test_INT("ee2eeRF345"         ,  1,  2,  3.6962960760299218E-01)
  call test_INT("ee2eeRF35"          ,  1,  2,  1.8481480380149609E-01)
  call test_INT("ee2eeRF45"          ,  1,  2, -3.4447249214096937E-01)
  call test_INT("ee2eeRR"            ,  1,  2, -1.2290856440468495E-01)
  call test_INT("ee2eeRR15162526"    ,  1,  2, -2.3974923581631398E-02)
  !call test_INT('ee2eeRR35364546',  1, 2 , 1.0_prec)
  call test_INT("ee2eeFF"            , 10, 10,  2.2609014082076554E+06)

  call test_INT("ee2eeA"             , 10, 10,  9.5190233756337861E+00)
  call test_INT("ee2eeAA"            , 10, 10,  1.1389901836808138E+01)
  call test_INT("ee2eeAF"            , 10, 10,  2.4188560015328631E+00)
  call test_INT("ee2eeAR"            , 10, 10, -6.9229067528893413E+00)
  call test_INT("ee2eeAR125"         , 10, 10, -2.6813740225828706E+00)
  call test_INT("ee2eeAR345"         , 10, 10, -5.7402700762842365E+00)
  call test_INT("ee2eeNF"            , 10, 10, -6.2240905633714378E+00)

  call initflavour("daphne")

  call test_INT("eb2eb0"             , 10, 10,  8.0945992518332915E-04)
  call test_INT("eb2ebF"             , 10, 10,  5.1871103111354256E-03)
  call test_INT("eb2ebR"             , 10, 10,  1.0630664553967766E-03)
  call test_INT("eb2ebR125"          , 10, 10,  7.6485978868100022E-03)
  call test_INT("eb2ebR35"           , 10, 10, -2.5644583366607600E-03)
  call test_INT("eb2ebR45"           , 10, 10, -1.5114036270552141E-03)

  call test_INT("eb2ebFF"            , 10, 10,  1.8020462644881243E-02)
  call test_INT("eb2ebRF"            ,  1,  2,  4.1486712474390897E-04)
  call test_INT("eb2ebRF125"         ,  1,  2, -3.2412320477398027E-04)
  call test_INT("eb2ebRF35"          ,  1,  2, -1.1069999043810338E-03)
  call test_INT("eb2ebRF45"          ,  1,  2, -2.2987016078626975E-03)
  call test_INT("eb2ebRR"            ,  1,  2,  5.7413728345277279E-05)
  call test_INT("eb2ebRR15162526"    ,  1,  2,  1.4147810899139147E-05)
  call test_INT("eb2ebRR3536"        ,  1,  2, -1.1156814711452657E-04)
  call test_INT("eb2ebRR4546"        ,  1,  2,  4.7027890470754011E-07)

  call test_INT("eb2ebA"             , 10, 10,  1.6183818394014762E-03)
  call blockend(5)
  END SUBROUTINE

  SUBROUTINE TESTEEPOINTWISE
  use olinterface

  call blockstart("ee-> ee VEGAS pointwise")

  ! get muon mass
  call initflavour("mu-e", mys=40000._prec)
  call initflavour("prad")

  xinormcut = 1
  xinormcut1 = 1
  xinormcut2 = 1
  musq = Mm**2
  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 1

  call test_INT("ee2ee0"             , ans= 3.8522230623387523E+00)
  call test_INT("ee2eeF"             , ans= 1.2941312771727400E+01)
  call test_INT("ee2eeR"             , ans=-2.1357268220063963E-01)
  call test_INT("ee2eeR125"          , ans=-2.1357268220063963E-01)
  call test_INT("ee2eeR345"          , ans=-1.1785215657338800E+04)
  call test_INT("ee2eeR35"           , ans=-5.8926078286694001E+03)
  call test_INT("ee2eeR45"           , ans=-2.7848703527926677E-01)
  call openloops("ee2eeg", reload=.true.)
  call test_INT("ee2eeRF"            , ans=-7.1748464071994711E-01)
  call test_INT("ee2eeRF125"         , ans=-7.1748464071994711E-01)
  call test_INT("ee2eeRF345"         , ans=-2.0756951405778706E+04)
  call test_INT("ee2eeRF45"          , ans=-5.7617756608575732E-01)
  call test_INT("ee2eeRR"            , ans= 9.0786826768136847E-02)
  call test_INT("ee2eeRR15162526"    , ans= 6.0973074847888442E-02)
  call test_INT("ee2eeRR35364546"    , ans= 1.2073974387652875E+02)
  call test_INT("ee2eeFF"            , ans= 1.4076044863743970E+01)

  call test_INT("ee2eeAA"            , ans= 3.3624964059211004E+00)
  call test_INT("ee2eeAF"            , ans= 1.0634142659305713E+01)
  call test_INT("ee2eeAR"            , ans=-1.6031162161656387E-01)
  call test_INT("ee2eeAR125"         , ans=-1.6031162161656387E-01)
  call test_INT("ee2eeAR345"         , ans=-4.0965950367382711E+03)
  call test_INT("ee2eeNF"            , ans=-1.2782433491353886E+00)

  call initflavour("daphne")
  call test_INT("eb2eb0"             , ans= 1.4620459757339065E-02)
  call test_INT("eb2ebF"             , ans= 1.4353374703775482E-01)
  call test_INT("eb2ebR"             , ans= 1.0250523073099348E-04)
  call test_INT("eb2ebR125"          , ans= 1.7721202080861198E-03)
  call test_INT("eb2ebR35"           , ans= 8.3780141229648067E-05)
  call test_INT("eb2ebR45"           , ans= 2.2758832300081310E-02)

  call test_INT("eb2ebFF"            , ans= 7.3439414157749905E-01)
  call test_INT("eb2ebRF"            , ans= 1.6240887192621380E-03)
  call test_INT("eb2ebRF125"         , ans= 4.8424589058023715E-02)
  call test_INT("eb2ebRF35"          , ans= 1.4752157714180468E-03)
  call test_INT("eb2ebRF45"          , ans= 2.9463866508685277E-01)
  call test_INT("eb2ebRR"            , ans=-9.9945877760950798E-04)
  call test_INT("eb2ebRR15162526"    , ans= 2.9955158924461394E-04)
  call test_INT("eb2ebRR3536"        , ans=-1.3172063053934683E-04)

  call blockend(36)
  END SUBROUTINE


                 !!!!!!!!!!!!!!!!!!!!!!!!!
                    END MODULE EE_TEST
                 !!!!!!!!!!!!!!!!!!!!!!!!!
