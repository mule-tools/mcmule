                          !!!!!!!!!!!!!!!!!!!!!!
                                MODULE ee_ee2eegl_nts
                          !!!!!!!!!!!!!!!!!!!!!!
  use functions
  use collier
  implicit none

  real(kind=prec) :: ss,tt,m2,m,s15,s25,s35,uu,s1235
  real(kind=prec) :: betas,betat,betau
  real(kind=prec) :: logs,logt,logu,logs15,logs25,logs35,logs1235
  real(kind=prec) :: discbs,discbt,discbu
  real(kind=prec) :: scalarc0ir6s,scalarc0ir6t,scalarc0ir6u
  real(kind=prec) :: scalarc0s,scalarc0t,scalarc0u
  real(kind=prec) :: discbslogs,discbslogt,discbslogu,discbslogs15,discbslogs25,discbslogs35,discbslogs1235
  real(kind=prec) :: discbtlogt,discbtlogu,discbtlogs15,discbtlogs25,discbtlogs35,discbtlogs1235
  real(kind=prec) :: discbulogt,discbulogu,discbulogs15,discbulogs25,discbulogs35,discbulogs1235
  contains

  FUNCTION EE2EEGL_NTS(p1, p2, p3, p4, p5)
   !! e-(p1) e-(p2) -> e-(p3) e-(p4) g(p5)
   !! for massive electrons
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: ee2eegl_nts
  real(kind=prec) :: logmu

  ss = sq(p1+p2); tt = sq(p2-p4)
  m2 = sq(p1); m = sqrt(m2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  logmu = log(musq/m2)

  call init()

  ee2eegl_nts = soft() + soft_pole()*logmu + ntsoft() + ntsoft_pole()*logmu

  ee2eegl_nts = 2.**4*Pi**2*alpha**4*ee2eegl_nts
  END FUNCTION

  FUNCTION MYLOG(x,branchcut)
   complex(kind=prec) :: mylog
   real(kind=prec) :: x
   integer :: branchcut

   mylog = log(abs(x))
   if(x<0) mylog = mylog + branchcut*imag*pi
  END FUNCTION

  SUBROUTINE INIT()
   complex(kind=prec) :: logs_c,logt_c,logu_c,logs15_c,logs25_c,logs35_c,logs1235_c
   complex(kind=prec) :: discbs_c,discbt_c,discbu_c
  
   uu = 4*m2-ss-tt
   s1235 = s15+s25-s35
   betas = sqrt(1-4*m2/ss)
   betat = sqrt(1-4*m2/tt)
   betau = sqrt(1-4*m2/uu)

   logs_c = mylog(-m2/ss,1)
   logt_c = mylog(-m2/tt,1)
   logu_c = mylog(-m2/uu,1)
   logs15_c = mylog(m2/s15,1)
   logs25_c = mylog(m2/s25,1)
   logs35_c = mylog(-m2/s35,1)
   logs1235_c= mylog(-m2/s1235,1)

   discbs_c = discb_cplx(ss,m)
   discbt_c = discb_cplx(tt,m)
   discbu_c = discb_cplx(uu,m)

   logs = real(logs_c)
   logt = real(logt_c)
   logu = real(logu_c)
   logs15 = real(logs15_c)
   logs25 = real(logs25_c)
   logs35 = real(logs35_c)
   logs1235 = real(logs1235_c)

   discbs = real(discbs_c)
   discbt = real(discbt_c)
   discbu = real(discbu_c)

   discbslogs = real(discbs_c*logs_c)
   discbslogt = real(discbs_c*logt_c)
   discbslogu = real(discbs_c*logu_c)
   discbslogs15 = real(discbs_c*logs15_c)
   discbslogs25 = real(discbs_c*logs25_c)
   discbslogs35 = real(discbs_c*logs35_c)
   discbslogs1235 = real(discbs_c*logs1235_c)
   discbtlogt = real(discbt_c*logt_c)
   discbtlogu = real(discbt_c*logu_c)
   discbtlogs15 = real(discbt_c*logs15_c)
   discbtlogs25 = real(discbt_c*logs25_c)
   discbtlogs35 = real(discbt_c*logs35_c)
   discbtlogs1235 = real(discbt_c*logs1235_c)
   discbulogt = real(discbu_c*logt_c)
   discbulogu = real(discbu_c*logu_c)
   discbulogs15 = real(discbu_c*logs15_c)
   discbulogs25 = real(discbu_c*logs25_c)
   discbulogs35 = real(discbu_c*logs35_c)
   discbulogs1235 = real(discbu_c*logs1235_c)

   scalarc0ir6s = scalarc0ir6(ss,m)
   scalarc0ir6t = scalarc0ir6(tt,m)
   scalarc0ir6u = scalarc0ir6(uu,m)
  
   scalarc0s = scalarc0(ss,m)
   scalarc0t = scalarc0(tt,m)
   scalarc0u = scalarc0(uu,m)

  END SUBROUTINE

    FUNCTION SOFT_POLE()
  implicit none
  real SOFT_POLE
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59
  
  tmp1 = -s35
  tmp2 = s15 + s25 + tmp1
  tmp3 = s15 + tmp1
  tmp4 = tmp3**2
  tmp5 = ss**2
  tmp6 = tt**2
  tmp7 = s15**(-2)
  tmp8 = s25**(-2)
  tmp9 = tmp2**(-2)
  tmp10 = s35**(-2)
  tmp11 = -ss
  tmp12 = 1/tmp6
  tmp13 = -4*m2
  tmp14 = ss + tmp13 + tt
  tmp15 = tmp14**(-2)
  tmp16 = s25 + tmp1
  tmp17 = s25*tmp16
  tmp18 = s25 + s35
  tmp19 = s15*tmp18
  tmp20 = tmp17 + tmp19
  tmp21 = tmp20**2
  tmp22 = m2*tmp21*tmp4
  tmp23 = ss*tmp4
  tmp24 = -s25
  tmp25 = s15 + tmp24
  tmp26 = -2*s35
  tmp27 = s15 + s25 + tmp26
  tmp28 = tmp25*tmp27*tt
  tmp29 = tmp23 + tmp28
  tmp30 = s15*s35*tmp2*tmp24*tmp29
  tmp31 = tmp22 + tmp30
  tmp32 = m2**4
  tmp33 = 64*tmp32
  tmp34 = m2**3
  tmp35 = -6*ss
  tmp36 = tmp35 + tt
  tmp37 = 16*tmp34*tmp36
  tmp38 = ss*tt
  tmp39 = tmp38 + tmp5 + tmp6
  tmp40 = tmp39**2
  tmp41 = m2**2
  tmp42 = 13*tmp5
  tmp43 = 3*tmp38
  tmp44 = 3*tmp6
  tmp45 = tmp42 + tmp43 + tmp44
  tmp46 = 4*tmp41*tmp45
  tmp47 = ss**3
  tmp48 = 3*tmp47
  tmp49 = 3*tmp5*tt
  tmp50 = ss*tmp44
  tmp51 = tt**3
  tmp52 = 2*tmp51
  tmp53 = tmp48 + tmp49 + tmp50 + tmp52
  tmp54 = tmp13*tmp53
  tmp55 = tmp33 + tmp37 + tmp40 + tmp46 + tmp54
  tmp56 = 2*m2
  tmp57 = 4*m2
  tmp58 = -tt
  tmp59 = ss + tt
  SOFT_POLE = 128*tmp10*tmp12*tmp15*tmp31*tmp55*tmp7*tmp8*tmp9 - (128*discbs*tmp10*tmp12*tmp15&
               &*tmp31*tmp55*(tmp11 + tmp56)*tmp7*tmp8*tmp9)/(tmp11 + tmp57) + (128*discbt*tmp10&
               &*tmp12*tmp15*tmp31*tmp55*(tmp56 + tmp58)*tmp7*tmp8*tmp9)/(tmp57 + tmp58) - (128*&
               &discbu*tmp10*tmp12*tmp15*tmp31*tmp7*tmp8*tmp9*(128*m2**5 - tmp40*tmp59 + 8*tmp34&
               &*(13*tmp38 + 25*tmp5 + tmp6) - 32*tmp32*(8*ss + tt) - 4*tmp41*(19*tmp47 + 7*tmp5&
               &1 + 12*ss*tmp6 + 22*tmp5*tt) + tmp56*(7*ss**4 + 12*ss*tmp51 + 15*tmp5*tmp6 + 14*&
               &tmp47*tt + 5*tt**4)))/tmp59

  END FUNCTION SOFT_POLE

    FUNCTION NTSOFT_POLE()
  implicit none
  real NTSOFT_POLE
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206, tmp207, tmp208, tmp209, tmp210
  real tmp211, tmp212, tmp213, tmp214, tmp215
  real tmp216, tmp217, tmp218, tmp219, tmp220
  real tmp221, tmp222, tmp223, tmp224, tmp225
  real tmp226, tmp227, tmp228, tmp229, tmp230
  real tmp231, tmp232, tmp233, tmp234, tmp235
  real tmp236, tmp237, tmp238, tmp239, tmp240
  real tmp241, tmp242, tmp243, tmp244, tmp245
  real tmp246, tmp247, tmp248, tmp249, tmp250
  real tmp251, tmp252, tmp253, tmp254, tmp255
  real tmp256, tmp257, tmp258, tmp259, tmp260
  real tmp261, tmp262, tmp263, tmp264, tmp265
  real tmp266, tmp267, tmp268, tmp269, tmp270
  real tmp271, tmp272, tmp273, tmp274, tmp275
  real tmp276, tmp277, tmp278, tmp279, tmp280
  real tmp281, tmp282, tmp283, tmp284, tmp285
  real tmp286, tmp287, tmp288, tmp289, tmp290
  real tmp291, tmp292, tmp293, tmp294, tmp295
  real tmp296, tmp297, tmp298, tmp299, tmp300
  real tmp301, tmp302, tmp303, tmp304, tmp305
  real tmp306, tmp307, tmp308, tmp309, tmp310
  real tmp311, tmp312, tmp313, tmp314, tmp315
  real tmp316, tmp317, tmp318, tmp319, tmp320
  real tmp321, tmp322, tmp323, tmp324, tmp325
  real tmp326, tmp327, tmp328, tmp329, tmp330
  real tmp331, tmp332, tmp333, tmp334, tmp335
  real tmp336, tmp337, tmp338, tmp339, tmp340
  real tmp341, tmp342, tmp343, tmp344, tmp345
  real tmp346, tmp347, tmp348, tmp349, tmp350
  real tmp351, tmp352, tmp353, tmp354, tmp355
  real tmp356, tmp357, tmp358, tmp359, tmp360
  real tmp361, tmp362, tmp363, tmp364, tmp365
  real tmp366, tmp367, tmp368, tmp369, tmp370
  real tmp371, tmp372, tmp373, tmp374, tmp375
  real tmp376, tmp377, tmp378, tmp379, tmp380
  real tmp381, tmp382, tmp383, tmp384, tmp385
  real tmp386, tmp387, tmp388, tmp389, tmp390
  real tmp391, tmp392, tmp393, tmp394, tmp395
  real tmp396, tmp397, tmp398, tmp399, tmp400
  real tmp401, tmp402, tmp403, tmp404, tmp405
  real tmp406, tmp407, tmp408, tmp409, tmp410
  real tmp411, tmp412, tmp413, tmp414, tmp415
  real tmp416, tmp417, tmp418, tmp419, tmp420
  real tmp421, tmp422, tmp423, tmp424, tmp425
  real tmp426, tmp427, tmp428, tmp429, tmp430
  real tmp431, tmp432, tmp433, tmp434, tmp435
  real tmp436, tmp437, tmp438, tmp439, tmp440
  real tmp441, tmp442, tmp443, tmp444, tmp445
  real tmp446, tmp447, tmp448, tmp449, tmp450
  real tmp451, tmp452, tmp453, tmp454, tmp455
  real tmp456, tmp457, tmp458, tmp459, tmp460
  real tmp461, tmp462, tmp463, tmp464, tmp465
  real tmp466, tmp467, tmp468, tmp469, tmp470
  real tmp471, tmp472, tmp473, tmp474, tmp475
  real tmp476, tmp477, tmp478, tmp479, tmp480
  real tmp481, tmp482, tmp483, tmp484, tmp485
  real tmp486, tmp487, tmp488, tmp489, tmp490
  real tmp491, tmp492, tmp493, tmp494, tmp495
  real tmp496, tmp497, tmp498, tmp499, tmp500
  real tmp501, tmp502, tmp503, tmp504, tmp505
  real tmp506, tmp507, tmp508, tmp509, tmp510
  real tmp511, tmp512, tmp513, tmp514, tmp515
  real tmp516, tmp517, tmp518, tmp519, tmp520
  real tmp521, tmp522, tmp523, tmp524, tmp525
  real tmp526, tmp527, tmp528, tmp529, tmp530
  real tmp531, tmp532, tmp533, tmp534, tmp535
  real tmp536, tmp537, tmp538, tmp539, tmp540
  real tmp541, tmp542, tmp543, tmp544, tmp545
  real tmp546, tmp547, tmp548, tmp549, tmp550
  real tmp551, tmp552, tmp553, tmp554, tmp555
  real tmp556, tmp557, tmp558, tmp559, tmp560
  real tmp561, tmp562, tmp563, tmp564, tmp565
  real tmp566, tmp567, tmp568, tmp569, tmp570
  real tmp571, tmp572, tmp573, tmp574, tmp575
  real tmp576, tmp577, tmp578, tmp579, tmp580
  real tmp581, tmp582, tmp583, tmp584, tmp585
  real tmp586, tmp587, tmp588, tmp589, tmp590
  real tmp591, tmp592, tmp593, tmp594, tmp595
  real tmp596, tmp597, tmp598, tmp599, tmp600
  real tmp601, tmp602, tmp603, tmp604, tmp605
  real tmp606, tmp607, tmp608, tmp609, tmp610
  real tmp611, tmp612, tmp613, tmp614, tmp615
  real tmp616, tmp617, tmp618, tmp619, tmp620
  real tmp621, tmp622, tmp623, tmp624, tmp625
  real tmp626, tmp627, tmp628, tmp629, tmp630
  real tmp631, tmp632, tmp633, tmp634, tmp635
  
  tmp1 = -4*m2
  tmp2 = -s35
  tmp3 = s25 + tmp2
  tmp4 = s15 + tmp2
  tmp5 = s25*tmp3
  tmp6 = s25 + s35
  tmp7 = s15*tmp6
  tmp8 = tmp5 + tmp7
  tmp9 = s25**2
  tmp10 = s35**2
  tmp11 = s25**3
  tmp12 = s35**3
  tmp13 = tmp4**3
  tmp14 = tmp3**2
  tmp15 = s15**3
  tmp16 = s15**2
  tmp17 = s25**4
  tmp18 = s35**4
  tmp19 = -3*s35
  tmp20 = 6*tmp18
  tmp21 = -s15
  tmp22 = s35 + tmp21
  tmp23 = 51*s25*s35
  tmp24 = ss**2
  tmp25 = s15**4
  tmp26 = 12*tmp18
  tmp27 = tt**2
  tmp28 = -135*tmp17
  tmp29 = tt**3
  tmp30 = ss**3
  tmp31 = -13*tmp9
  tmp32 = -12*s15*s25
  tmp33 = 11*s15*s35
  tmp34 = 25*s25*s35
  tmp35 = -12*tmp10
  tmp36 = tmp16 + tmp31 + tmp32 + tmp33 + tmp34 + tmp35
  tmp37 = 12*tmp12
  tmp38 = ss**4
  tmp39 = -6*tmp10
  tmp40 = 7*tmp11
  tmp41 = 3*tmp12
  tmp42 = 84*tmp12
  tmp43 = tt**4
  tmp44 = 7*tmp9
  tmp45 = 10*s25*s35
  tmp46 = 6*tmp12
  tmp47 = -2*tmp11
  tmp48 = -4*tmp10
  tmp49 = 75*s25*tmp10
  tmp50 = -2*s35
  tmp51 = -2*tmp18
  tmp52 = 4*tmp18
  tmp53 = 80*tmp17
  tmp54 = 2*tmp18
  tmp55 = Pi**4
  tmp56 = s15 + s25
  tmp57 = tmp2 + tmp56
  tmp58 = -tt
  tmp59 = 4*m2
  tmp60 = -ss
  tmp61 = tmp58 + tmp59 + tmp60
  tmp62 = m2**3
  tmp63 = m2**2
  tmp64 = -s25
  tmp65 = 2*m2
  tmp66 = tmp58 + tmp65
  tmp67 = tmp58 + tmp59
  tmp68 = 32*tmp62
  tmp69 = -2*tmp30
  tmp70 = -3*tmp24*tt
  tmp71 = -3*ss*tmp27
  tmp72 = -tmp29
  tmp73 = -40*ss
  tmp74 = 4*tt
  tmp75 = tmp73 + tmp74
  tmp76 = tmp63*tmp75
  tmp77 = 4*tmp24
  tmp78 = 2*ss*tt
  tmp79 = tmp27 + tmp77 + tmp78
  tmp80 = tmp59*tmp79
  tmp81 = tmp68 + tmp69 + tmp70 + tmp71 + tmp72 + tmp76 + tmp80
  tmp82 = tmp50 + tmp56
  tmp83 = tmp22*tmp65*tmp8
  tmp84 = s25*ss*tmp4*tmp57
  tmp85 = s15*s25*tmp82*tt
  tmp86 = tmp83 + tmp84 + tmp85
  tmp87 = m2**6
  tmp88 = m2**5
  tmp89 = m2**4
  tmp90 = s15 + tmp64
  tmp91 = 48*tmp62
  tmp92 = -tmp30
  tmp93 = -44*ss
  tmp94 = 12*tt
  tmp95 = tmp93 + tmp94
  tmp96 = tmp63*tmp95
  tmp97 = 3*tmp24
  tmp98 = -2*tmp27
  tmp99 = tmp97 + tmp98
  tmp100 = tmp59*tmp99
  tmp101 = tmp100 + tmp29 + tmp91 + tmp92 + tmp96
  tmp102 = -2*m2*tmp4*tmp8
  tmp103 = s25*ss*tmp4
  tmp104 = s35*tmp90*tt
  tmp105 = tmp103 + tmp104
  tmp106 = tmp105*tmp57
  tmp107 = tmp102 + tmp106
  tmp108 = tmp106 + tmp83
  tmp109 = 2*ss*tmp4
  tmp110 = -4*s35
  tmp111 = 3*s15
  tmp112 = tmp4*tmp65*tmp8
  tmp113 = s15*ss*tmp22
  tmp114 = tmp57*tmp58*tmp90
  tmp115 = tmp113 + tmp114
  tmp116 = s35*tmp115
  tmp117 = tmp112 + tmp116
  tmp118 = 3*ss*tmp56
  tmp119 = tmp24*tmp56
  tmp120 = 5*tmp9
  tmp121 = -2*tmp10
  tmp122 = 2*s25
  tmp123 = s15*tmp3
  tmp124 = s25 + tmp19
  tmp125 = s25*tmp124
  tmp126 = tmp125 + tmp7
  tmp127 = 8*tmp63
  tmp128 = 5*tt
  tmp129 = ss + tmp128
  tmp130 = 2*s35
  tmp131 = s25 + tmp130
  tmp132 = 5*s15*s35
  tmp133 = s15 + s35
  tmp134 = tmp14*tmp60
  tmp135 = s35 + tmp64
  tmp136 = 24*tmp62
  tmp137 = 4*tmp29
  tmp138 = 56*tt
  tmp139 = tmp138 + tmp93
  tmp140 = tmp139*tmp63
  tmp141 = 2*tmp24
  tmp142 = -5*tmp27
  tmp143 = tmp141 + tmp142
  tmp144 = 6*m2*tmp143
  tmp145 = tmp136 + tmp137 + tmp140 + tmp144 + tmp92
  tmp146 = 2*tt
  tmp147 = ss + tmp146
  tmp148 = tmp147*tt
  tmp149 = -2*m2*tmp129
  tmp150 = tmp127 + tmp148 + tmp149
  tmp151 = 12*tmp63
  tmp152 = -16*m2*tt
  tmp153 = 3*tmp27
  tmp154 = tmp151 + tmp152 + tmp153
  tmp155 = 3*tt
  tmp156 = ss + tmp155
  tmp157 = -(tmp156*tmp27)
  tmp158 = -8*tmp129*tmp63
  tmp159 = m2*tmp129*tmp74
  tmp160 = tmp157 + tmp158 + tmp159 + tmp68
  tmp161 = 1/tmp55
  tmp162 = 1/tmp16
  tmp163 = 1/tmp9
  tmp164 = tmp57**(-2)
  tmp165 = 1/tmp10
  tmp166 = 1/tmp29
  tmp167 = tmp1 + tt
  tmp168 = tmp167**(-2)
  tmp169 = ss + tmp167
  tmp170 = tmp169**(-3)
  tmp171 = ss + tt
  tmp172 = tmp4**2
  tmp173 = -9*s25
  tmp174 = s25*s35
  tmp175 = m2**7
  tmp176 = 7*s25
  tmp177 = -7*tmp9
  tmp178 = 8*tmp10
  tmp179 = 2*tmp11
  tmp180 = 17*tmp17
  tmp181 = 3*tmp9
  tmp182 = 5*tmp11
  tmp183 = 3*s25
  tmp184 = 6*tmp9
  tmp185 = -8*tmp174
  tmp186 = 3*tmp11
  tmp187 = 20*s35*tmp9
  tmp188 = 34*tmp12
  tmp189 = 5*tmp17
  tmp190 = 21*tmp17
  tmp191 = -4*tmp18
  tmp192 = -(tmp14*tmp174)
  tmp193 = s35 + tmp122
  tmp194 = 4*tmp9
  tmp195 = 4*s25*tmp10
  tmp196 = -127*tmp9
  tmp197 = s35*tmp176
  tmp198 = -22*tmp10
  tmp199 = -16*tmp10
  tmp200 = 376*tmp11
  tmp201 = 200*tmp10*tmp9
  tmp202 = 18*s25
  tmp203 = -5*s35
  tmp204 = s25*tmp203
  tmp205 = -30*tmp18
  tmp206 = -18*tmp18
  tmp207 = ss**5
  tmp208 = s15**5
  tmp209 = s25**5
  tmp210 = s35**5
  tmp211 = tt**5
  tmp212 = 49*s25
  tmp213 = -tmp174
  tmp214 = 4*tmp12
  tmp215 = 16*tmp12
  tmp216 = -493*tmp10*tmp9
  tmp217 = ss**6
  tmp218 = 11*tmp11
  tmp219 = tmp122 + tmp2
  tmp220 = 43*tmp11
  tmp221 = 36*tmp174
  tmp222 = 9*tmp9
  tmp223 = -tmp10
  tmp224 = 8*tmp12
  tmp225 = tt**6
  tmp226 = -tmp9
  tmp227 = tmp16 + tmp226
  tmp228 = -3*tmp174
  tmp229 = 2*tmp10
  tmp230 = ss**7
  tmp231 = 6*tmp11
  tmp232 = 101*s25
  tmp233 = 31*s25
  tmp234 = 8*s35
  tmp235 = 62*s25*tmp10
  tmp236 = tt**7
  tmp237 = 5*s25
  tmp238 = 11*tmp174
  tmp239 = -34*tmp10
  tmp240 = 20*tmp11
  tmp241 = -45*s35
  tmp242 = 33*tmp174
  tmp243 = -44*tmp10
  tmp244 = 55*tmp11
  tmp245 = 128*tmp12
  tmp246 = -28*s35
  tmp247 = 2*tmp9
  tmp248 = 71*tmp174
  tmp249 = tmp10*tmp202
  tmp250 = -2*m2
  tmp251 = tmp171**2
  tmp252 = ss + tmp250
  tmp253 = ss + tmp1
  tmp254 = tmp253**2
  tmp255 = 64*tmp89
  tmp256 = 14*ss
  tmp257 = tmp155 + tmp256
  tmp258 = -8*tmp257*tmp62
  tmp259 = 18*tmp24
  tmp260 = 13*ss*tt
  tmp261 = tmp259 + tmp260 + tmp27
  tmp262 = 4*tmp261*tmp63
  tmp263 = 2*tmp30
  tmp264 = tmp155*tmp24
  tmp265 = ss*tmp153
  tmp266 = tmp263 + tmp264 + tmp265 + tmp29
  tmp267 = tmp171*tmp266
  tmp268 = 10*tmp30
  tmp269 = 15*tmp24*tt
  tmp270 = 9*ss*tmp27
  tmp271 = 3*tmp29
  tmp272 = tmp268 + tmp269 + tmp270 + tmp271
  tmp273 = tmp250*tmp272
  tmp274 = tmp255 + tmp258 + tmp262 + tmp267 + tmp273
  tmp275 = s15*s35*tmp4*tmp61*tmp81
  tmp276 = 128*tmp62
  tmp277 = -4*tmp30
  tmp278 = -9*tmp24*tt
  tmp279 = -6*ss*tmp27
  tmp280 = -2*tmp29
  tmp281 = 32*ss
  tmp282 = 19*tt
  tmp283 = tmp281 + tmp282
  tmp284 = -4*tmp283*tmp63
  tmp285 = 5*tmp24
  tmp286 = 7*ss*tt
  tmp287 = 2*tmp27
  tmp288 = tmp285 + tmp286 + tmp287
  tmp289 = 8*m2*tmp288
  tmp290 = tmp276 + tmp277 + tmp278 + tmp279 + tmp280 + tmp284 + tmp289
  tmp291 = tmp117*tmp290
  tmp292 = tmp275 + tmp291
  tmp293 = tmp112*tmp131
  tmp294 = tmp111 + tmp122 + tmp50
  tmp295 = tmp294*tmp4*tmp60
  tmp296 = -3*tmp16
  tmp297 = -2*s15*s25
  tmp298 = tmp132 + tmp213 + tmp296 + tmp297 + tmp9
  tmp299 = tmp298*tt
  tmp300 = tmp295 + tmp299
  tmp301 = tmp174*tmp300
  tmp302 = tmp293 + tmp301
  tmp303 = 3*tmp16
  tmp304 = tmp122 + tmp203
  tmp305 = s25*tmp135
  tmp306 = 1/tmp168
  tmp307 = ss*tmp4*tmp57
  tmp308 = s15*tmp82*tt
  tmp309 = tmp307 + tmp308
  tmp310 = s25*tmp309
  tmp311 = tmp102 + tmp310
  tmp312 = -96*tmp89
  tmp313 = -tmp38
  tmp314 = tmp92*tt
  tmp315 = ss*tmp29
  tmp316 = 17*ss
  tmp317 = tmp155 + tmp316
  tmp318 = 8*tmp317*tmp62
  tmp319 = 17*tmp24
  tmp320 = 8*ss*tt
  tmp321 = -7*tmp27
  tmp322 = tmp319 + tmp320 + tmp321
  tmp323 = -4*tmp322*tmp63
  tmp324 = 7*tmp30
  tmp325 = 6*tmp24*tt
  tmp326 = -4*ss*tmp27
  tmp327 = -5*tmp29
  tmp328 = tmp324 + tmp325 + tmp326 + tmp327
  tmp329 = tmp328*tmp65
  tmp330 = tmp312 + tmp313 + tmp314 + tmp315 + tmp318 + tmp323 + tmp329 + tmp43
  tmp331 = tmp122*tmp14
  tmp332 = tmp303*tmp6
  tmp333 = tmp120 + tmp121 + tmp228
  tmp334 = s15*tmp333
  tmp335 = tmp331 + tmp332 + tmp334
  tmp336 = ss*tmp131*tmp4
  tmp337 = s25*tmp304*tt
  tmp338 = s15*tmp193*tt
  tmp339 = tmp336 + tmp337 + tmp338
  tmp340 = tmp250*tmp335*tmp4
  tmp341 = s15*tmp339*tmp57
  tmp342 = tmp340 + tmp341
  tmp343 = s35*ss*tmp4
  tmp344 = s25*tmp82*tt
  tmp345 = tmp343 + tmp344
  tmp346 = tmp21*tmp345
  tmp347 = tmp112 + tmp346
  tmp348 = 192*tmp89
  tmp349 = tmp155*tmp30
  tmp350 = 2*tmp315
  tmp351 = 9*tt
  tmp352 = tmp256 + tmp351
  tmp353 = -16*tmp352*tmp62
  tmp354 = ss*tmp351
  tmp355 = tmp287 + tmp354 + tmp77
  tmp356 = ss*tmp1*tmp355
  tmp357 = 23*tmp24
  tmp358 = 33*ss*tt
  tmp359 = tmp287 + tmp357 + tmp358
  tmp360 = 4*tmp359*tmp63
  tmp361 = tmp348 + tmp349 + tmp350 + tmp353 + tmp356 + tmp360 + tmp38
  tmp362 = 52*tmp24
  tmp363 = 4*tmp30
  tmp364 = tmp24*tmp351
  tmp365 = 6*ss*tmp27
  tmp366 = 2*tmp29
  tmp367 = tmp363 + tmp364 + tmp365 + tmp366
  tmp368 = tmp135 + tmp21
  tmp369 = s15*tmp124
  tmp370 = tmp60 + tmp66
  tmp371 = -(tmp368*tmp370*tmp81*tmp86)
  tmp372 = tmp172*tmp65*tmp8
  tmp373 = tmp172*tmp60
  tmp374 = tmp16 + tmp174 + tmp369
  tmp375 = tmp374*tmp58
  tmp376 = tmp373 + tmp375
  tmp377 = s25*tmp376*tmp57
  tmp378 = tmp372 + tmp377
  tmp379 = 2*tmp38
  tmp380 = tmp128*tmp30
  tmp381 = 6*tmp24*tmp27
  tmp382 = 4*tmp315
  tmp383 = tmp255 + tmp258 + tmp262 + tmp273 + tmp379 + tmp380 + tmp381 + tmp382 +&
                 & tmp43
  tmp384 = -(tmp378*tmp383)
  tmp385 = tmp371 + tmp384
  tmp386 = tmp250 + tt
  tmp387 = -tmp106
  tmp388 = tmp112 + tmp387
  tmp389 = 96*tmp89
  tmp390 = tmp30*tt
  tmp391 = -tmp315
  tmp392 = -tmp43
  tmp393 = -8*tmp317*tmp62
  tmp394 = 4*tmp322*tmp63
  tmp395 = tmp250*tmp328
  tmp396 = tmp38 + tmp389 + tmp390 + tmp391 + tmp392 + tmp393 + tmp394 + tmp395
  tmp397 = tmp123*tmp56
  tmp398 = tmp174*tmp90
  tmp399 = tmp397 + tmp398
  tmp400 = 56*s15
  tmp401 = 44*s25
  tmp402 = tmp400 + tmp401
  tmp403 = tmp402*tmp63
  tmp404 = s15*tmp146
  tmp405 = tmp118 + tmp404
  tmp406 = -8*m2*tmp405
  tmp407 = s15*tmp27
  tmp408 = tmp119 + tmp407
  tmp409 = 3*tmp408
  tmp410 = tmp403 + tmp406 + tmp409
  tmp411 = tmp60 + tmp65
  tmp412 = tmp5*tmp56
  tmp413 = s35*tmp57*tmp90
  tmp414 = tmp412 + tmp413
  tmp415 = 14*s15
  tmp416 = 14*s25
  tmp417 = tmp19 + tmp415 + tmp416
  tmp418 = 4*tmp417*tmp63
  tmp419 = tmp146*tmp57
  tmp420 = tmp118 + tmp419
  tmp421 = -8*m2*tmp420
  tmp422 = tmp27*tmp57
  tmp423 = tmp119 + tmp422
  tmp424 = 3*tmp423
  tmp425 = tmp418 + tmp421 + tmp424
  tmp426 = ss + tmp386
  tmp427 = s25*tmp6
  tmp428 = tmp123 + tmp427
  tmp429 = tmp6*tt
  tmp430 = 44*s15
  tmp431 = 40*s25
  tmp432 = tmp110 + tmp430 + tmp431
  tmp433 = tmp432*tmp63
  tmp434 = 4*s25
  tmp435 = s35 + tmp111 + tmp434
  tmp436 = ss*tmp435
  tmp437 = tmp429 + tmp436
  tmp438 = -8*m2*tmp437
  tmp439 = s15 + tmp193
  tmp440 = tmp24*tmp439
  tmp441 = 2*ss*tmp429
  tmp442 = tmp27*tmp6
  tmp443 = tmp440 + tmp441 + tmp442
  tmp444 = 3*tmp443
  tmp445 = tmp433 + tmp438 + tmp444
  tmp446 = 1/ss
  tmp447 = 1/tmp254
  tmp448 = 36*tmp11
  tmp449 = 8*tmp9
  tmp450 = -7*tmp174
  tmp451 = -15*s35*tmp9
  tmp452 = 2*tmp12
  tmp453 = tmp10*tmp183
  tmp454 = 3*s35
  tmp455 = s25*tmp229
  tmp456 = 101*tmp11
  tmp457 = 56*tmp11
  tmp458 = 27*tmp9
  tmp459 = 8*tmp11
  tmp460 = 4*s35
  tmp461 = 14*tmp9
  tmp462 = -8*tmp10
  tmp463 = tmp10*tmp416
  tmp464 = 12*tmp10
  tmp465 = s25 + tmp50
  tmp466 = -tmp11
  tmp467 = -13*s35
  tmp468 = s35*tmp226
  tmp469 = -3*tmp11
  tmp470 = -12*tmp12
  tmp471 = 11*s25
  tmp472 = -34*tmp174
  tmp473 = s35*tmp14*tmp184
  tmp474 = -70*s35*tmp9
  tmp475 = tmp14*tmp39*tmp9
  tmp476 = -16*tmp174
  tmp477 = 187*s25*tmp10
  tmp478 = 19*tmp11
  tmp479 = -36*s35*tmp11
  tmp480 = -223*s25*tmp12
  tmp481 = -6*tmp18
  tmp482 = 10*tmp11
  tmp483 = 3*tmp17
  tmp484 = -9*tmp11
  tmp485 = -107*s35
  tmp486 = tmp467 + tmp471
  tmp487 = tmp16*tmp486
  tmp488 = tmp467*tmp9
  tmp489 = 24*s25*tmp10
  tmp490 = 11*tmp9
  tmp491 = 24*tmp10
  tmp492 = tmp472 + tmp490 + tmp491
  tmp493 = s15*tmp492
  tmp494 = tmp11 + tmp15 + tmp470 + tmp487 + tmp488 + tmp489 + tmp493
  tmp495 = -136*tmp10
  tmp496 = 34*tmp9
  tmp497 = -66*tmp10
  tmp498 = -170*s35*tmp9
  tmp499 = 34*tmp17
  tmp500 = 24*tmp9
  tmp501 = 312*tmp12
  tmp502 = -684*s25*tmp12
  tmp503 = s35*tmp14*tmp9
  tmp504 = -6*tmp174
  tmp505 = 15*s35*tmp9
  tmp506 = 10*s25*tmp10
  tmp507 = -282*s35*tmp9
  tmp508 = 17*tmp9
  tmp509 = 34*tmp11
  tmp510 = 68*tmp12
  tmp511 = 95*s25
  tmp512 = -92*tmp12
  tmp513 = 2*tmp15
  tmp514 = 10*tmp12
  tmp515 = s15*tmp122
  tmp516 = s15*tmp19
  tmp517 = tmp16 + tmp228 + tmp229 + tmp515 + tmp516 + tmp9
  tmp518 = tmp59 + tmp60
  tmp519 = tmp133*tmp64
  tmp520 = -2*s15
  tmp521 = tmp454 + tmp520
  tmp522 = s15*tmp521
  tmp523 = tmp519 + tmp522 + tmp9
  tmp524 = s15*tmp4
  tmp525 = s25*tmp133
  tmp526 = tmp524 + tmp525
  tmp527 = tmp14*tmp526*tmp65
  tmp528 = 2*tmp16
  tmp529 = tmp305 + tmp369 + tmp528
  tmp530 = tmp529*tmp61
  tmp531 = tmp134 + tmp530
  tmp532 = tmp174*tmp531
  tmp533 = tmp527 + tmp532
  tmp534 = s15*s35*tmp101*tmp344*tmp61
  tmp535 = tmp2*tmp311*tmp361
  tmp536 = tmp534 + tmp535
  tmp537 = tmp123 + tmp16 + tmp174
  tmp538 = tmp14*tmp537*tmp65
  tmp539 = -(tmp126*tmp61)
  tmp540 = tmp134 + tmp539
  tmp541 = s15*tmp540*tmp57
  tmp542 = tmp538 + tmp541
  tmp543 = s15*tmp101*tmp344*tmp61
  tmp544 = tmp347*tmp361
  tmp545 = tmp543 + tmp544
  tmp546 = 256*tmp89
  tmp547 = 24/tmp446
  tmp548 = tmp128 + tmp547
  tmp549 = -16*tmp548*tmp62
  tmp550 = (37*tt)/tmp446
  tmp551 = tmp362 + tmp550 + tmp98
  tmp552 = 4*tmp551*tmp63
  tmp553 = 12*tmp24
  tmp554 = (17*tt)/tmp446
  tmp555 = 4*tmp27
  tmp556 = tmp553 + tmp554 + tmp555
  tmp557 = (tmp1*tmp556)/tmp446
  tmp558 = tmp367/tmp446
  tmp559 = tmp546 + tmp549 + tmp552 + tmp557 + tmp558
  tmp560 = s15*tmp344*tmp61*tmp81
  tmp561 = -(tmp559*tmp86)
  tmp562 = tmp560 + tmp561
  tmp563 = 1/tmp251
  tmp564 = -6*s35*tmp9
  tmp565 = -tmp17
  tmp566 = 8*s25
  tmp567 = -19*tmp174
  tmp568 = 4*tmp11
  tmp569 = 11*tmp17
  tmp570 = 59*s25
  tmp571 = -46*tmp10
  tmp572 = 33*tmp12
  tmp573 = -319*tmp174
  tmp574 = 48*tmp11
  tmp575 = -227*s35*tmp11
  tmp576 = 12*tmp11
  tmp577 = -96*tmp10*tmp9
  tmp578 = -6*s35
  tmp579 = tmp511 + tmp578
  tmp580 = 44*tmp11
  tmp581 = 38*s25
  tmp582 = 69*tmp9
  tmp583 = -12*tmp18
  tmp584 = 21*s25
  tmp585 = 76*tmp10
  tmp586 = -16*s35
  tmp587 = -29*tmp174
  tmp588 = 7*tmp15
  tmp589 = s35*tmp9
  tmp590 = 34*tmp10
  tmp591 = -11*tmp174
  tmp592 = 3*tmp15
  tmp593 = -3*tmp10
  tmp594 = -60*tmp12
  tmp595 = -18*tmp10*tmp14*tmp9
  tmp596 = -18*tmp10
  tmp597 = tmp14*tmp462*tmp9
  tmp598 = -8*tmp18
  tmp599 = 31*tmp11
  tmp600 = -18*tmp589
  tmp601 = 2*tmp17
  tmp602 = 23*s35
  tmp603 = 16*tmp11
  tmp604 = -23*tmp10
  tmp605 = 23*tmp12
  tmp606 = -206*tmp589
  tmp607 = 43*tmp17
  tmp608 = 1523*s25*tmp10
  tmp609 = 16*tmp17
  tmp610 = 72*tmp12
  tmp611 = 6*tmp17
  tmp612 = 66*tmp12
  tmp613 = 23*tmp9
  tmp614 = 24*tmp17
  tmp615 = 29*tmp17
  tmp616 = -91*s35*tmp11
  tmp617 = 127*s25
  tmp618 = 99*s35*tmp11
  tmp619 = 14*tmp11
  tmp620 = -8*tmp147*tmp63
  tmp621 = -(tmp156*tmp251)
  tmp622 = tmp155/tmp446
  tmp623 = tmp24 + tmp287 + tmp622
  tmp624 = 6*m2*tmp623
  tmp625 = tmp620 + tmp621 + tmp624
  tmp626 = s15/tmp446
  tmp627 = -2*s25
  tmp628 = s15 + tmp627
  tmp629 = tmp628*tt
  tmp630 = tmp626 + tmp629
  tmp631 = tmp90*tt
  tmp632 = tmp626 + tmp631
  tmp633 = (53*tmp29)/tmp446
  tmp634 = 2/tmp446
  tmp635 = tmp155 + tmp634
  NTSOFT_POLE = -(tmp161*tmp162*tmp163*tmp164*tmp165*tmp166*tmp168*tmp170*tmp446*tmp447*tmp563*(&
                 &256*s15*s25*tmp306*tmp55*tmp56*tmp57*tmp61*tt*(tmp117*tmp24*tmp254*tmp274 + tmp2&
                 &51*tmp254*tmp292*tmp634 - (tmp117*tmp251*tmp252*tmp61*tmp81)/tmp446 + tmp117*tmp&
                 &251*tmp252*tmp59*tmp61*tmp81 + (tmp117*tmp254*tmp274*tt)/tmp446) + (256*tmp254*t&
                 &mp55*tmp589*tt*(2*tmp306*tmp330*tmp347*tmp422 - 2*tmp251*tmp306*tmp545*tmp57 + t&
                 &mp101*tmp251*tmp342*tmp59*tmp61*tmp66 + tmp306*tmp330*tmp347*tmp57*tmp634*tt + t&
                 &mp101*tmp251*tmp61*(-tmp341 + tmp335*tmp4*tmp65)*tmp66*tt))/tmp446 + 256*s35*tmp&
                 &254*tmp4*tmp55*tmp61*tmp626*(tmp27*tmp306*tmp385 - 2*tmp251*tmp306*tmp562*tmp57 &
                 &+ 8*m2*tmp251*tmp57*tmp61*tmp66*tmp81*tmp86 + (tmp306*tmp385*tt)/tmp446 - 2*tmp2&
                 &51*tmp57*tmp61*tmp66*tmp81*tmp86*tt) - 256*tmp171*tmp518*tmp55*tmp67*(256*tmp56*&
                 &tmp87*(52*tmp13*tmp3*tmp38*tmp8 + tmp22*tmp390*((45*s25 - 83*s35)*s35*tmp331 + (&
                 &57*s25*tmp10 + 24*tmp11 - 121*tmp12 + tmp187)*tmp528 + tmp15*(tmp585 + s25*tmp60&
                 &2 - 59*tmp9) + s15*(-225*s35*tmp11 - 168*s25*tmp12 + 107*tmp17 + 166*tmp18 + 120&
                 &*tmp10*tmp9)) - tmp24*tmp27*(tmp15*(301*s25*tmp10 + 208*tmp11 + tmp188 - 471*tmp&
                 &589) + (82*s25 - 49*s35)*tmp10*tmp14*tmp627 + tmp25*(-25*tmp174 + tmp497 + 55*tm&
                 &p9) + s15*s35*(15*s35*tmp11 + 378*s25*tmp12 - 98*tmp18 + tmp189 - 300*tmp10*tmp9&
                 &) + tmp16*(-637*s35*tmp11 - 752*s25*tmp12 + 153*tmp17 + 130*tmp18 + 1070*tmp10*t&
                 &mp9)) + (tmp280*(tmp16*(-97*s35*tmp11 - 138*s25*tmp12 + 30*tmp17 - 29*tmp18 + tm&
                 &p201) + s25*(41*s25 + tmp110)*tmp14*tmp223 + tmp15*(70*tmp12 + tmp457 + tmp49 - &
                 &133*tmp589) + tmp25*(-37*tmp10 - 23*tmp174 + 26*tmp9) + s15*s35*(-65*s35*tmp11 +&
                 & 82*s25*tmp12 + tmp190 + tmp191 + tmp239*tmp9)))/tmp446 + tmp43*tmp460*(tmp192 +&
                 & tmp15*tmp193 + tmp16*(tmp121 + tmp194 + tmp204) + s15*(tmp12 + tmp179 + tmp195 &
                 &- 7*tmp589))*tmp90) + tmp213*tmp251*tmp422*tmp56*tmp626*((s15 + tmp173 + tmp234)&
                 &*tmp30*tmp4 + tmp24*tmp58*(6*s15*s25 - 8*s15*s35 + tmp16 + tmp178 + tmp185 + tmp&
                 &9) + tmp27*tmp634*(-3*s15*s25 + 7*s15*s35 - 2*tmp16 + tmp174 + tmp48 + tmp9) + t&
                 &mp280*tmp82*tmp90)*(tmp24 + tmp27 + tt/tmp446) + 8192*m2**8*tmp4*tmp56*tmp8*(tmp&
                 &172*tmp24*tmp3 + s35*tmp422*tmp90 + tmp172*tmp3*tmp634*tt) - 1024*tmp175*tmp56*(&
                 &16*tmp13*tmp3*tmp30*tmp8 + tmp22*tmp27*tmp634*(s25*tmp14*(tmp183 + tmp2)*tmp460 &
                 &+ tmp15*(tmp181 + tmp450 + tmp462) + (tmp182 + tmp451 + tmp452 + tmp463)*tmp528 &
                 &+ s15*(-19*s35*tmp11 - 36*s25*tmp12 + 7*tmp17 + tmp52 + 44*tmp10*tmp9)) + s35*tm&
                 &p366*(tmp15*(s35 + tmp183) + tmp16*(tmp121 + tmp184 + tmp185) + tmp192 + s15*(6*&
                 &s25*tmp10 + tmp12 + tmp186 - 10*tmp589))*tmp90 + tmp22*tmp24*(s35*(-19*s35 + tmp&
                 &176)*tmp331 - 2*tmp16*(31*tmp12 + tmp179 + tmp455 - 32*tmp589) + (tmp174 + tmp17&
                 &7 + tmp178)*tmp592 + s15*(38*tmp18 + tmp180 + s35*tmp466 + tmp12*tmp566 - 62*tmp&
                 &10*tmp9))*tt) - 64*tmp56*tmp88*(88*tmp13*tmp207*tmp3*tmp8 - (2*tmp43*(s25*tmp14*&
                 &(tmp202 + tmp203)*tmp39 + 13*tmp25*(tmp181 + tmp204 + tmp39) + tmp15*(201*s25*tm&
                 &p10 + 108*tmp11 + 126*tmp12 - 227*tmp589) + s15*s35*(-237*s35*tmp11 + 166*s25*tm&
                 &p12 + 61*tmp17 + tmp205 + 40*tmp10*tmp9) + tmp16*(-161*s35*tmp11 - 332*s25*tmp12&
                 & + 69*tmp17 + tmp206 + 338*tmp10*tmp9)))/tmp446 + tmp24*tmp280*(s35*(60*s35*tmp1&
                 &1 - 283*s25*tmp12 - 22*tmp17 + 45*tmp18 + tmp201)*tmp520 + tmp15*(390*s25*tmp10 &
                 &+ 262*tmp12 + tmp200 - 830*tmp589) + tmp10*tmp14*(133*s25 + tmp241)*tmp627 + 11*&
                 &tmp25*(tmp199 + tmp504 + 13*tmp9) + tmp16*(-900*s35*tmp11 - 980*s25*tmp12 + 233*&
                 &tmp17 + tmp52 + 1544*tmp10*tmp9)) + tmp130*tmp211*((17*s25 + 13*s35)*tmp15 + s25&
                 &*tmp14*tmp467 + tmp16*(-26*tmp10 - 38*tmp174 + tmp496) + s15*(17*tmp11 + 13*tmp1&
                 &2 - 64*tmp589 + s25*tmp590))*tmp90 + tmp30*(3*tmp25*(tmp197 + tmp198 + tmp458) +&
                 & tmp15*(496*s25*tmp10 - 82*tmp12 + tmp200 - 862*tmp589) + tmp10*tmp14*(140*s25 +&
                 & tmp485)*tmp627 + s15*s35*(227*s35*tmp11 + 702*s25*tmp12 - 32*tmp17 - 214*tmp18 &
                 &- 683*tmp10*tmp9) + tmp16*(-1343*s35*tmp11 - 1433*s25*tmp12 + 295*tmp17 + 362*tm&
                 &p18 + 2155*tmp10*tmp9))*tmp98 + tmp22*tmp38*(tmp15*(164*tmp10 + tmp196 + tmp248)&
                 & + (113*s25 - 195*s35)*s35*tmp331 + tmp528*(271*s25*tmp10 + 68*tmp11 - 277*tmp12&
                 & - 116*tmp589) + s15*(-857*s35*tmp11 - 712*s25*tmp12 + 263*tmp17 + 390*tmp18 + 9&
                 &16*tmp10*tmp9))*tt) - 8*tmp62*(20*tmp13*tmp230*tmp3*tmp56*tmp8 + tmp130*tmp227*t&
                 &mp236*(tmp14*tmp185 + tmp15*(tmp233 + tmp234) + s15*(tmp224 + tmp235 - 101*tmp58&
                 &9 + tmp599) + tmp16*(-85*tmp174 + tmp199 + 62*tmp9)) + tmp29*tmp313*(s15*tmp589*&
                 &(-2648*s25*tmp10 - 75*tmp11 + 1586*tmp12 + 1137*tmp589) + tmp25*(2493*s25*tmp10 &
                 &+ 1402*tmp11 - 3960*tmp589 + tmp610) + (417*s25 - 266*s35)*tmp121*tmp14*tmp9 + t&
                 &mp208*(-302*tmp10 + s35*tmp237 + 290*tmp9) + tmp15*(-9074*s35*tmp11 - 6744*s25*t&
                 &mp12 + 1934*tmp17 + 762*tmp18 + 13129*tmp10*tmp9) + tmp16*(12305*tmp10*tmp11 - 5&
                 &184*s35*tmp17 + 4246*s25*tmp18 + 822*tmp209 - 532*tmp210 - 11664*tmp12*tmp9)) + &
                 &tmp30*tmp392*(tmp25*(2879*s25*tmp10 + 1351*tmp11 + 494*tmp12 - 4094*tmp589) + s1&
                 &5*(-1052*s25*tmp10 + 225*tmp11 + 1338*tmp12 - 511*tmp589)*tmp589 + (446*s25 - 21&
                 &5*s35)*tmp121*tmp14*tmp9 + tmp208*(-462*tmp10 - 475*tmp174 + 307*tmp9) + tmp15*(&
                 &-7398*s35*tmp11 - 5892*s25*tmp12 + 1781*tmp17 + 398*tmp18 + 11741*tmp10*tmp9) + &
                 &tmp16*(8781*tmp10*tmp11 - 3554*s35*tmp17 + 3488*s25*tmp18 + 737*tmp209 - 430*tmp&
                 &210 - 9652*tmp12*tmp9)) - 2*tmp211*tmp24*(tmp25*(1314*s25*tmp10 + 229*tmp11 + 15&
                 &4*tmp12 - 1279*tmp589) + s15*(224*tmp11 + 370*tmp12 + tmp49 - 669*tmp589)*tmp589&
                 & + tmp121*tmp14*(-59*s35 + tmp617)*tmp9 + tmp208*(tmp495 + tmp573 + 37*tmp9) + t&
                 &mp15*(-1613*s35*tmp11 - 1955*s25*tmp12 + 347*tmp17 + 100*tmp18 + 3539*tmp10*tmp9&
                 &) + tmp16*(1674*tmp10*tmp11 - 429*s35*tmp17 + 960*s25*tmp18 + 155*tmp209 - 118*t&
                 &mp210 - 2660*tmp12*tmp9)) + tmp225*tmp634*((45*tmp10 + 169*tmp174 + tmp184)*tmp2&
                 &08 - tmp25*(597*s25*tmp10 + 62*tmp12 + tmp482 - 449*tmp589) + tmp21*(167*s25*tmp&
                 &10 + 146*tmp11 + 119*tmp12 - 432*tmp589)*tmp589 + tmp10*tmp14*(73*s25 + tmp246)*&
                 &tmp9 + tmp15*(301*s35*tmp11 + 687*s25*tmp12 - 38*tmp17 - 11*tmp18 - 1159*tmp10*t&
                 &mp9) - tmp16*(158*tmp10*tmp11 + 125*s35*tmp17 + 259*s25*tmp18 + 22*tmp209 - 28*t&
                 &mp210 - 756*tmp12*tmp9)) + tmp207*tmp27*(tmp10*tmp14*tmp194*(-112*s35 + tmp232) &
                 &+ s15*(2019*s25*tmp10 + 82*tmp11 - 840*tmp12 - 1261*tmp589)*tmp589 + tmp25*(-185&
                 &2*s25*tmp10 - 445*tmp11 + 536*tmp12 + 1973*tmp589) + tmp208*(-169*tmp174 + tmp24&
                 &3 + tmp9) + tmp15*(5431*s35*tmp11 + 5101*s25*tmp12 - 893*tmp17 - 940*tmp18 - 848&
                 &7*tmp10*tmp9) + tmp16*(-8344*tmp10*tmp11 + 3371*s35*tmp17 - 3080*s25*tmp18 - 447&
                 &*tmp209 + 448*tmp210 + 7840*tmp12*tmp9)) + tmp217*tmp22*tmp56*(s35*tmp14*tmp416*&
                 &(6*s25 + tmp467) + tmp16*(579*s25*tmp10 - 280*tmp12 + tmp231 - 364*tmp589) + tmp&
                 &15*(98*tmp10 + s35*tmp212 - 88*tmp9) + s15*(-693*s35*tmp11 - 624*s25*tmp12 + 94*&
                 &tmp17 + 182*tmp18 + 1041*tmp10*tmp9))*tt) + 16*tmp89*(82*tmp13*tmp217*tmp3*tmp56&
                 &*tmp8 + tmp130*tmp225*tmp227*(-23*tmp14*tmp174 + tmp15*(tmp212 + tmp602) + s15*(&
                 &98*s25*tmp10 + 49*tmp11 + tmp498 + tmp605) + tmp528*(-62*tmp174 + tmp604 + 49*tm&
                 &p9)) + tmp27*tmp313*(tmp25*(2371*s25*tmp10 + 1375*tmp11 - 714*tmp12 - 3340*tmp58&
                 &9) + s15*tmp589*(-1246*s25*tmp10 - 161*tmp11 + 512*tmp12 + 895*tmp589) + (477*s2&
                 &5 - 437*s35)*tmp121*tmp14*tmp9 + tmp208*(-80*tmp10 + 221*tmp174 + 167*tmp9) + tm&
                 &p15*(-9252*s35*tmp11 - 7474*s25*tmp12 + 2249*tmp17 + 1668*tmp18 + 12501*tmp10*tm&
                 &p9) + tmp16*(11899*tmp10*tmp11 - 5852*s35*tmp17 + 4882*s25*tmp18 + 1041*tmp209 -&
                 & 874*tmp210 - 10788*tmp12*tmp9)) + tmp280*tmp30*(tmp25*(1031*s25*tmp10 + 1279*tm&
                 &p11 + 346*tmp12 - 2550*tmp589) + s15*(-308*s25*tmp10 + 538*tmp12 + tmp182 - 235*&
                 &tmp589)*tmp589 - 346*tmp10*tmp14*tmp219*tmp9 + tmp208*(-346*tmp10 - 71*tmp174 + &
                 &311*tmp9) + tmp15*(-5574*s35*tmp11 - 3228*s25*tmp12 + 1625*tmp17 + 346*tmp18 + 6&
                 &937*tmp10*tmp9) + tmp16*(6017*tmp10*tmp11 - 3090*s35*tmp17 + 2268*s25*tmp18 + 65&
                 &7*tmp209 - 346*tmp210 - 5612*tmp12*tmp9)) - 2*tmp24*tmp43*(2*tmp25*(480*s25*tmp1&
                 &0 + 370*tmp11 + 205*tmp12 - 858*tmp589) + s15*(588*s25*tmp10 + 191*tmp11 + 241*t&
                 &mp12 - 1020*tmp589)*tmp589 - 5*tmp10*tmp14*(-40*s35 + tmp232)*tmp9 + tmp208*(-30&
                 &5*tmp10 - 269*tmp174 + 180*tmp9) + tmp15*(-2834*s35*tmp11 - 1932*s25*tmp12 + 940&
                 &*tmp17 + 95*tmp18 + 4125*tmp10*tmp9) + tmp16*(2345*tmp10*tmp11 - 1196*s35*tmp17 &
                 &+ 1241*s25*tmp18 + 380*tmp209 - 200*tmp210 - 2964*tmp12*tmp9)) - (2*tmp211*((177&
                 &*s25 - 68*s35)*tmp10*tmp14*tmp226 + tmp208*(-109*tmp10 - 199*tmp174 + tmp461) + &
                 &2*tmp25*(319*s25*tmp10 + 75*tmp12 + tmp244 - 302*tmp589) + s15*(428*s25*tmp10 + &
                 &173*tmp11 + 81*tmp12 - 682*tmp589)*tmp589 + tmp15*(-574*s35*tmp11 - 860*s25*tmp1&
                 &2 + 178*tmp17 + 27*tmp18 + 1523*tmp10*tmp9) + tmp16*(271*tmp10*tmp11 + 421*s25*t&
                 &mp18 + 82*tmp209 - 68*tmp210 + tmp17*tmp460 - 1004*tmp12*tmp9)))/tmp446 + tmp207&
                 &*tmp22*tmp56*((139*s25 - 255*s35)*s35*tmp331 + tmp528*(549*s25*tmp10 + 62*tmp11 &
                 &- 371*tmp12 - 316*tmp589) + tmp15*(232*tmp10 + 113*tmp174 - 193*tmp9) + s15*(-14&
                 &87*s35*tmp11 - 1296*s25*tmp12 + 317*tmp17 + 510*tmp18 + 1956*tmp10*tmp9))*tt) + &
                 &tmp171*tmp250*tt*(s35*tmp227*tmp236*tmp515*tmp517 + s35*tmp225*tmp56*tmp634*((13&
                 &*s25 + s35)*tmp25 + tmp503 + (-22*tmp174 + tmp222 + tmp223)*tmp513 + tmp16*(39*s&
                 &25*tmp10 - 7*tmp11 + tmp12 - 19*tmp589) + s15*(tmp224 + tmp10*tmp471 + tmp576 - &
                 &31*tmp589)*tmp64) + tmp230*tmp4*(tmp10*tmp14*tmp247 + (tmp121 + tmp213 + tmp247)&
                 &*tmp25 + s15*(tmp464 + tmp508 + tmp587)*tmp589 + tmp15*(-19*s25*tmp10 + tmp214 +&
                 & tmp568 + 11*tmp589) + tmp16*(29*s35*tmp11 + 20*s25*tmp12 + tmp51 + tmp601 - 48*&
                 &tmp10*tmp9)) + tmp29*tmp313*(tmp208*(-47*tmp174 + tmp35 + tmp449) + tmp25*(393*s&
                 &25*tmp10 + 40*tmp11 + tmp224 - 390*tmp589) + s15*tmp589*(-446*s25*tmp10 + 264*tm&
                 &p12 + tmp218 + 171*tmp589) + tmp14*(tmp110 + tmp176)*tmp48*tmp9 + tmp15*(-660*s3&
                 &5*tmp11 - 690*s25*tmp12 + 56*tmp17 + 20*tmp18 + 1325*tmp10*tmp9) + tmp16*(1119*t&
                 &mp10*tmp11 - 306*s35*tmp17 + 344*s25*tmp18 + 24*tmp209 - 16*tmp210 - 1216*tmp12*&
                 &tmp9)) - tmp207*tmp27*(tmp25*(243*s25*tmp10 - 10*tmp12 + tmp240 - 255*tmp589) + &
                 &s15*tmp589*(-443*s25*tmp10 + 218*tmp12 + tmp484 + 234*tmp589) + tmp121*tmp14*(-7&
                 &*s35 + tmp566)*tmp9 + 2*tmp208*(tmp174 + tmp223 + tmp9) + tmp15*(-553*s35*tmp11 &
                 &- 533*s25*tmp12 + 26*tmp18 + tmp499 + 1024*tmp10*tmp9) + tmp16*(1029*tmp10*tmp11&
                 & - 305*s35*tmp17 + 288*s25*tmp18 + 16*tmp209 - 14*tmp210 - 1012*tmp12*tmp9)) + t&
                 &mp30*tmp392*((-10*tmp10 - 87*tmp174 + tmp181)*tmp208 + tmp25*(437*s25*tmp10 + tm&
                 &p478 + tmp514 - 372*tmp589) + s15*tmp589*(-270*s25*tmp10 + 210*tmp12 + tmp220 + &
                 &17*tmp589) - 10*tmp10*tmp14*tmp219*tmp9 + tmp15*(-460*s35*tmp11 - 610*s25*tmp12 &
                 &+ 10*tmp18 + tmp615 + 1125*tmp10*tmp9) + tmp16*(715*tmp10*tmp11 - 132*s35*tmp17 &
                 &+ 260*s25*tmp18 + 13*tmp209 - 10*tmp210 - 940*tmp12*tmp9)) + tmp141*tmp211*(tmp1&
                 &0*tmp14*tmp219*tmp247 + s15*tmp589*(-25*tmp11 - 48*tmp12 + tmp10*tmp233 + 42*tmp&
                 &589) + tmp25*(-147*s25*tmp10 + tmp11 - 2*tmp12 + 109*tmp589) + tmp208*(tmp221 + &
                 &tmp229 + tmp9) - tmp15*(-89*s35*tmp11 - 169*s25*tmp12 + tmp17 + tmp54 + 294*tmp1&
                 &0*tmp9) - tmp16*(107*tmp10*tmp11 + 9*s35*tmp17 + 58*s25*tmp18 + tmp209 - 2*tmp21&
                 &0 - 212*tmp12*tmp9)) + tmp217*(tmp15*(265*s35*tmp11 + 267*s25*tmp12 - 7*tmp17 - &
                 &20*tmp18 + tmp216) + tmp10*tmp14*tmp194*tmp465 + s15*(229*s25*tmp10 - 104*tmp12 &
                 &+ tmp179 - 127*tmp589)*tmp589 + tmp25*(-112*s25*tmp10 + tmp11 + tmp215 + 107*tmp&
                 &589) + tmp208*(tmp181 + tmp48 + tmp591) + tmp16*(-516*tmp10*tmp11 + 149*s35*tmp1&
                 &7 - 144*s25*tmp18 - 5*tmp209 + 8*tmp210 + 496*tmp12*tmp9))*tt) + 4*tmp63*((2*tmp&
                 &13*tmp3*tmp56*tmp8)/tmp446**8 + tmp38*tmp392*(tmp25*(2003*s25*tmp10 + 499*tmp11 &
                 &+ 102*tmp12 - 2351*tmp589) + s15*tmp589*(-1943*s25*tmp10 + 1250*tmp12 + tmp244 +&
                 & 638*tmp589) + (168*s25 - 95*s35)*tmp121*tmp14*tmp9 + tmp208*(-146*tmp10 - 210*t&
                 &mp174 + 103*tmp9) + tmp15*(-4397*s35*tmp11 - 3993*s25*tmp12 + 689*tmp17 + 234*tm&
                 &p18 + 7720*tmp10*tmp9) + tmp16*(6545*tmp10*tmp11 - 2201*s35*tmp17 + 2200*s25*tmp&
                 &18 + 293*tmp209 - 190*tmp210 - 6900*tmp12*tmp9)) + tmp207*tmp72*(tmp25*(1425*s25&
                 &*tmp10 + 318*tmp11 + tmp512 - 1686*tmp589) + s15*tmp589*(-2158*s25*tmp10 - 45*tm&
                 &p11 + 1084*tmp12 + 1119*tmp589) + tmp14*(56*s25 + tmp241)*tmp48*tmp9 + tmp208*(t&
                 &mp242 + tmp243 + 46*tmp9) + tmp15*(-3876*s35*tmp11 - 3442*s25*tmp12 + 498*tmp17 &
                 &+ 316*tmp18 + 6469*tmp10*tmp9) + tmp16*(6343*tmp10*tmp11 - 2202*s35*tmp17 + 1984&
                 &*s25*tmp18 + 226*tmp209 - 180*tmp210 - 6136*tmp12*tmp9)) + tmp236*tmp634*(tmp208&
                 &*(10*tmp10 + tmp247 + tmp248) + tmp10*tmp14*tmp247*(tmp176 + tmp50) + tmp21*(54*&
                 &s25*tmp10 + 63*tmp11 + 50*tmp12 - 167*tmp589)*tmp589 + tmp25*(-247*s25*tmp10 - 1&
                 &6*tmp12 + tmp179 + 178*tmp589) + tmp15*(88*s35*tmp11 + 246*s25*tmp12 - 2*tmp17 +&
                 & tmp54 - 417*tmp10*tmp9) - tmp16*(82*s35*tmp17 + 70*s25*tmp18 + 2*tmp209 - 4*tmp&
                 &210 + tmp10*tmp40 - 240*tmp12*tmp9)) + tmp141*tmp225*(tmp208*(31*tmp10 + 183*tmp&
                 &174 + tmp247) - tmp25*(751*s25*tmp10 + 22*tmp11 + tmp188 - 591*tmp589) + s15*tmp&
                 &589*(85*s25*tmp10 - 132*tmp11 - 231*tmp12 + 278*tmp589) + tmp10*tmp14*(tmp246 + &
                 &tmp570)*tmp9 + tmp15*(557*s35*tmp11 + 939*s25*tmp12 - 50*tmp17 - 25*tmp18 - 1637&
                 &*tmp10*tmp9) + tmp16*(-636*tmp10*tmp11 - 371*s25*tmp18 + s35*tmp180 - 26*tmp209 &
                 &+ 28*tmp210 + 1204*tmp12*tmp9)) + tmp217*tmp27*(tmp208*(-63*tmp174 + tmp239 + tm&
                 &p500) + tmp10*tmp14*tmp247*(-55*s35 + tmp581) + s15*(1123*s25*tmp10 - 486*tmp12 &
                 &+ tmp240 - 657*tmp589)*tmp589 + tmp25*(-738*s25*tmp10 - 38*tmp11 + 178*tmp12 + 6&
                 &71*tmp589) + tmp15*(1771*s35*tmp11 + 1837*s25*tmp12 - 148*tmp17 - 254*tmp18 - 31&
                 &33*tmp10*tmp9) + tmp16*(-3162*tmp10*tmp11 + 1057*s35*tmp17 - 1036*s25*tmp18 - 86&
                 &*tmp209 + 110*tmp210 + 3044*tmp12*tmp9)) + tmp211*(tmp25*(2209*s25*tmp10 + 292*t&
                 &mp11 + tmp245 - 2116*tmp589) + s15*(-996*s25*tmp10 + 235*tmp11 + 964*tmp12 - 203&
                 &*tmp589)*tmp589 + (67*s25 - 34*s35)*tmp14*tmp48*tmp9 + tmp208*(-132*tmp10 - 433*&
                 &tmp174 + 52*tmp9) + tmp15*(-2970*s35*tmp11 - 3420*s25*tmp12 + 428*tmp17 + 140*tm&
                 &p18 + 6335*tmp10*tmp9) + tmp16*(4059*tmp10*tmp11 - 1052*s35*tmp17 + 1644*s25*tmp&
                 &18 + 188*tmp209 - 136*tmp210 - 5216*tmp12*tmp9))*tmp92 + tmp22*tmp230*tmp56*(s35&
                 &*tmp331*(tmp237 + tmp586) - 2*tmp16*(-78*s25*tmp10 + 27*tmp12 + tmp182 + 52*tmp5&
                 &89) + tmp15*(22*tmp10 + tmp238 - 21*tmp9) + s15*(-169*s35*tmp11 - 156*s25*tmp12 &
                 &+ 32*tmp18 + tmp569 + 282*tmp10*tmp9))*tt + tmp130*tmp227*((9*s25 + s35)*tmp15 +&
                 & tmp192 + (tmp222 + tmp223 + s25*tmp467)*tmp528 + s15*(9*tmp11 + tmp12 + tmp249 &
                 &- 28*tmp589))*tt**8)) + 256*s35*tmp254*tmp55*tmp57*tmp626*tt*(s35*tmp287*tmp306*&
                 &tmp311*tmp330 + 2*tmp251*tmp306*tmp536 + tmp101*tmp251*tmp302*tmp59*tmp61*tmp66 &
                 &+ s35*tmp306*tmp311*tmp330*tmp634*tt + tmp101*tmp251*tmp61*tmp66*tt*(tmp131*tmp2&
                 &50*tmp4*tmp8 + tmp174*((tmp294*tmp4)/tmp446 + (tmp303 + s15*tmp304 + tmp305)*tt)&
                 &)) + 256*s15*tmp174*tmp55*tt*((tmp101*tmp107*tmp251*tmp252*tmp306*tmp56*tmp58*tm&
                 &p61)/tmp446 + tmp101*tmp107*tmp251*tmp254*tmp386*tmp58*tmp61*tmp626 + tmp101*tmp&
                 &107*tmp251*tmp254*tmp386*tmp59*tmp61*tmp626 + (tmp254*tmp27*tmp306*tmp388*tmp396&
                 &*tmp627)/tmp446 + m2*tmp101*tmp107*tmp251*tmp252*tmp306*tmp56*tmp61*tmp74 + tmp2&
                 &4*tmp254*tmp306*tmp388*tmp396*tmp627*tt - (2*tmp251*tmp254*tmp306*(tmp101*tmp399&
                 &*tmp57*tmp61*tt - tmp107*(-(tmp101*(s25*tmp146 + s15*tmp58 + s15*tmp59 - tmp626)&
                 &) + tmp410*tmp61*tt)))/tmp446) - 256*s15*s25*tmp55*tmp57*tt*((tmp101*tmp169*tmp2&
                 &51*tmp306*tmp411*tmp56*tmp58*(tmp102 + s35*(tmp4*tmp626 + tmp57*tmp631)))/tmp446&
                 & + (tmp101*tmp117*tmp251*tmp254*tmp57*tmp58*tmp61*tmp66)/tmp446 + (tmp101*tmp117&
                 &*tmp251*tmp254*tmp57*tmp59*tmp61*tmp66)/tmp446 + m2*tmp101*tmp117*tmp251*tmp306*&
                 &tmp411*tmp56*tmp61*tmp74 + (s35*tmp117*tmp254*tmp306*tmp396*tmp98)/tmp446 + tmp1&
                 &17*tmp24*tmp254*tmp306*tmp396*tmp50*tt - (2*tmp251*tmp254*tmp306*(s35*tmp101*tmp&
                 &414*tmp61*tt + tmp117*(-(tmp101*(s35*tmp146 + tmp57*tmp61)) + tmp425*tmp61*tt)))&
                 &/tmp446) + 256*s15*tmp174*tmp55*tmp61*(-(tmp108*tmp24*tmp254*tmp306*tmp426*tmp42&
                 &9*tmp81) - (tmp108*tmp254*tmp306*tmp426*tmp442*tmp81)/tmp446 + (tmp108*tmp251*tm&
                 &p252*tmp306*tmp56*tmp58*tmp61*tmp81)/tmp446 - (8*m2*tmp108*tmp251*tmp254*tmp4*tm&
                 &p61*tmp66*tmp81)/tmp446 + m2*tmp108*tmp251*tmp252*tmp306*tmp56*tmp61*tmp74*tmp81&
                 & + tmp108*tmp251*tmp254*tmp4*tmp61*tmp634*tmp66*tmp81*tt - (2*tmp251*tmp254*tmp3&
                 &06*(tmp4*tmp428*tmp57*tmp61*tmp81*tt - tmp108*(-((tmp429 + 2*tmp4*tmp61)*tmp81) &
                 &+ tmp445*tmp61*tt)))/tmp446)))/16. + (discbu*tmp161*tmp162*tmp163*tmp164*tmp165*&
                 &tmp166*tmp170*tmp563*(256*tmp174*tmp55*tmp57*tt*(m2*tmp347*tmp396*tmp434*tt + tm&
                 &p101*tmp171*tmp370*tmp520*tmp61*tmp82*tmp9*tt + 2*tmp347*(s25*tmp101*tmp625 + s2&
                 &5*tmp154*tmp171*tmp370*tmp61*tt)) + 512*s15*s35*tmp55*tmp57*tt*(m2*s35*tmp146*tm&
                 &p311*tmp396 + s35*tmp171*tmp370*tmp543 + tmp311*(s35*tmp101*tmp625 + s35*tmp154*&
                 &tmp171*tmp370*tmp61*tt)) + 256*s15*tmp174*tmp55*tt*(m2*tmp388*tmp396*tmp434*tt -&
                 & 2*tmp101*tmp171*tmp370*tmp399*tmp57*tmp61*tt + 2*tmp107*(-(tmp101*(tmp251*tmp63&
                 &0 + tmp127*tmp632 - 6*m2*tmp171*tmp632)) + tmp171*tmp370*tmp410*tmp61*tt)) + 256&
                 &*s15*tmp169*tmp174*tmp55*(tmp108*tmp250*tmp426*tmp429*tmp81 + tmp171*tmp370*tmp4&
                 &*tmp428*tmp57*tmp61*tmp81*tt - tmp108*((tmp171*tmp429*tmp61 - tmp370*(tmp171*tmp&
                 &429 - (-2*tmp171*tmp4 + tmp429)*tmp61))*tmp81 + tmp171*tmp370*tmp445*tmp61*tt)) &
                 &- 256*tmp55*tt*(s15*tmp171**3*tmp213*tmp56*(6*tmp38*tmp4*tmp517 + 2*tmp390*(13*s&
                 &25*tmp10 + 9*tmp15 + tmp47 + tmp470 + tmp528*(tmp176 + tmp586) + s15*(35*tmp10 +&
                 & tmp181 + tmp587) + tmp589) + tmp153*tmp24*((15*s25 - 29*s35)*tmp16 + tmp463 + t&
                 &mp469 + tmp470 + tmp588 + tmp589 + s15*(tmp120 - 32*tmp174 + tmp590)) + (6*tmp29&
                 &*(-4*tmp12 + tmp10*tmp237 + tmp16*(-9*s35 + tmp237) + tmp466 + tmp513 + s15*(11*&
                 &tmp10 + tmp247 + tmp591)))/tmp446 + tmp43*(19*s25*tmp10 + s15*(29*tmp10 - 35*tmp&
                 &174 + tmp222) + tmp47 + tmp470 + (-10*s35 + tmp176)*tmp528 - 5*tmp589 + tmp592))&
                 & - 1024*tmp175*tmp4*(s15*tmp10*tmp120*tmp135 + (tmp174 + tmp226 + tmp229)*tmp25 &
                 &- tmp15*(s25*tmp10 + tmp179 + tmp41 + tmp564) - tmp14*tmp589*tmp6 + tmp16*(s25*t&
                 &mp12 + tmp18 + s35*tmp231 + tmp565 - 9*tmp10*tmp9)) - 128*tmp56*tmp87*((4*(s25*t&
                 &mp10*tmp14*(tmp122 + tmp19) + (2*tmp174 + tmp223 + tmp247)*tmp25 + tmp15*(tmp11 &
                 &+ 5*tmp12 + s25*tmp35) + s15*s35*(-5*s25*tmp12 + 3*tmp18 + tmp11*tmp454 + tmp565&
                 &) - tmp16*(-18*s25*tmp12 + tmp17 + 7*tmp18 + s35*tmp469 + 10*tmp10*tmp9)))/tmp44&
                 &6 + tmp58*(s15*s35*(78*s25*tmp12 + s35*tmp182 + tmp54 + tmp569 + tmp577) + tmp15&
                 &*(91*s25*tmp10 + 38*tmp12 + tmp568 - 65*tmp589) + tmp25*(tmp181 + tmp567 + tmp59&
                 &6) + tmp10*tmp14*(s35 + tmp566)*tmp627 + tmp16*(-31*s35*tmp11 - 148*s25*tmp12 + &
                 &tmp17 - 22*tmp18 + 166*tmp10*tmp9))) + tmp250*tmp251*tmp56*(s15*s25*tmp392*((-73&
                 &*tmp10 + 46*tmp174 + tmp247)*tmp528 + s15*(-237*s25*tmp10 + 207*tmp12 + tmp179 +&
                 & 53*tmp589) + tmp15*(tmp122 + tmp602) + tmp2*(-129*s25*tmp10 + tmp42 + 29*tmp589&
                 & + tmp603)) - 2*tmp22*tmp38*(tmp16*(83*s25*tmp10 + tmp179 + tmp41 - 59*tmp589) +&
                 & 3*tmp14*tmp589 + s15*s25*(88*s25*tmp10 + tmp11 - 29*tmp589 + tmp594) + tmp15*(-&
                 &27*tmp174 + tmp593 + tmp9)) + tmp153*tmp24*(tmp25*(-64*tmp174 + tmp181 + tmp462)&
                 & + tmp513*(121*s25*tmp10 + tmp186 + tmp224 - 63*tmp589) + 10*s15*tmp174*(tmp10*t&
                 &mp173 + tmp186 + tmp514 - 4*tmp589) + tmp597 + tmp16*(-32*s35*tmp11 - 278*s25*tm&
                 &p12 + tmp483 + tmp598 + 246*tmp10*tmp9)) + tmp29*tmp634*(tmp475 + tmp15*(231*s25&
                 &*tmp10 + tmp37 + tmp568 - 135*tmp589) + s15*tmp174*(-121*s25*tmp10 + 108*tmp12 +&
                 & tmp599 + tmp600) + 2*tmp25*(-26*tmp174 + tmp593 + tmp9) + tmp16*(-52*s35*tmp11 &
                 &- 287*s25*tmp12 + tmp481 + tmp601 + 287*tmp10*tmp9)) + tmp390*(s15*s25*tmp454*(-&
                 &86*s25*tmp10 + 15*tmp11 + tmp42 + tmp488) + tmp15*(591*s25*tmp10 + 36*tmp12 + tm&
                 &p482 - 281*tmp589) + tmp595 + tmp25*(tmp120 - 165*tmp174 + tmp596) + tmp16*(-71*&
                 &s35*tmp11 - 678*s25*tmp12 + tmp189 + tmp206 + 584*tmp10*tmp9))) - 8*tmp56*tmp62*&
                 &(tmp392*(2*(19*tmp10 + 75*tmp174 + tmp194)*tmp25 + tmp14*(20*s25 + tmp2)*tmp455 &
                 &+ tmp15*(-593*s25*tmp10 - 74*tmp12 + 261*tmp589 + tmp619) + tmp16*(683*s25*tmp12&
                 & + 34*tmp18 + tmp11*tmp586 + tmp611 - 511*tmp10*tmp9) + s15*s35*(254*s35*tmp11 -&
                 & 238*s25*tmp12 - 131*tmp17 + tmp54 + 113*tmp10*tmp9)) - 2*tmp22*tmp38*(tmp14*tmp&
                 &174*(68*s25 + tmp2) + tmp16*(596*s25*tmp10 + 37*tmp11 - 468*tmp589 + tmp612) + t&
                 &mp15*(-67*tmp10 - 182*tmp174 + 18*tmp9) + s15*(-220*s35*tmp11 - 500*s25*tmp12 + &
                 &19*tmp17 + tmp18 + 700*tmp10*tmp9)) + 4*tmp390*(s25*tmp14*(109*s25 + tmp2)*tmp22&
                 &3 + tmp15*(1140*s25*tmp10 + 47*tmp11 + 215*tmp12 - 634*tmp589) + tmp25*(-108*tmp&
                 &10 - 299*tmp174 + tmp613) + s35*tmp21*(119*s35*tmp11 - 638*s25*tmp12 - 120*tmp17&
                 & + tmp18 + 638*tmp10*tmp9) + tmp16*(-217*s35*tmp11 - 1480*s25*tmp12 - 106*tmp18 &
                 &+ tmp614 + 1395*tmp10*tmp9)) + tmp153*tmp24*(tmp25*(-170*tmp10 - 483*tmp174 + tm&
                 &p458) + tmp513*(848*s25*tmp10 + 28*tmp11 + 169*tmp12 - 419*tmp589) + tmp10*tmp14&
                 &*(86*s25 + tmp2)*tmp627 + s15*s35*(-561*s35*tmp11 + 802*s25*tmp12 + 268*tmp17 + &
                 &tmp51 - 507*tmp10*tmp9) + tmp16*(-2017*s25*tmp12 - 166*tmp18 + tmp615 + tmp616 +&
                 & 1619*tmp10*tmp9)) + (tmp29*(tmp25*(-250*tmp10 - 767*tmp174 + tmp508) + tmp15*(2&
                 &677*s25*tmp10 + 38*tmp11 + 496*tmp12 - 1211*tmp589) + tmp10*tmp14*(tmp50 + tmp61&
                 &7)*tmp627 + s15*s35*(-1285*s35*tmp11 + 1092*s25*tmp12 + 551*tmp17 + tmp191 - 354&
                 &*tmp10*tmp9) + tmp16*(-3006*s25*tmp12 - 242*tmp18 + tmp190 + tmp618 + 2128*tmp10&
                 &*tmp9)))/tmp446) + 4*tmp171*tmp56*tmp63*(-2*tmp22*tmp38*(s15*s25*(347*s25*tmp10 &
                 &- 242*tmp12 + tmp40 - 112*tmp589) + tmp15*(-98*tmp174 + tmp44 + tmp604) + s35*tm&
                 &p14*tmp613 + tmp16*(310*s25*tmp10 - 233*tmp589 + tmp605 + tmp619)) + tmp392*((77&
                 &*tmp174 + tmp178 + tmp184)*tmp25 + tmp10*tmp14*tmp449 + tmp513*(-207*s25*tmp10 -&
                 & 8*tmp12 + tmp231 + 118*tmp589) + s25*tmp516*(-93*s25*tmp10 + tmp240 + tmp589 + &
                 &tmp610) + tmp16*(553*s25*tmp12 + 8*tmp18 + tmp611 + tmp618 - 575*tmp10*tmp9)) + &
                 &(tmp29*(4*(-21*tmp10 - 95*tmp174 + tmp194)*tmp25 + s15*tmp174*(-549*s25*tmp10 + &
                 &255*tmp11 + 676*tmp12 - 382*tmp589) + tmp15*(32*tmp11 + 168*tmp12 - 827*tmp589 +&
                 & tmp608) - 84*tmp10*tmp14*tmp9 + tmp16*(-192*s35*tmp11 - 1819*s25*tmp12 - 84*tmp&
                 &18 + tmp609 + 1631*tmp10*tmp9)))/tmp446 + tmp24*tmp27*(tmp15*(2573*s25*tmp10 + 8&
                 &6*tmp11 + 344*tmp12 - 1299*tmp589) + s15*tmp174*(-762*s25*tmp10 + 371*tmp11 + 11&
                 &00*tmp12 - 709*tmp589) - 172*tmp10*tmp14*tmp9 + tmp25*(-172*tmp10 - 723*tmp174 +&
                 & 43*tmp9) + tmp16*(-205*s35*tmp11 - 2950*s25*tmp12 - 172*tmp18 + tmp607 + 2432*t&
                 &mp10*tmp9)) + tmp390*(tmp15*(2257*s25*tmp10 + 70*tmp11 + 284*tmp12 - 1165*tmp589&
                 &) + s15*tmp174*(-1103*s25*tmp10 + 205*tmp11 + 1104*tmp12 + tmp606) - 142*tmp10*t&
                 &mp14*tmp9 + tmp25*(-142*tmp10 - 616*tmp174 + 35*tmp9) + tmp16*(-344*s35*tmp11 - &
                 &2745*s25*tmp12 + 35*tmp17 - 142*tmp18 + 2473*tmp10*tmp9))) - 32*tmp56*tmp88*(-2*&
                 &tmp22*tmp24*(tmp14*tmp174*(tmp467 + tmp570) + tmp15*(-83*tmp174 + tmp181 + tmp57&
                 &1) + tmp16*(305*s25*tmp10 + tmp478 + tmp572 - 231*tmp589) + s15*(-115*s35*tmp11 &
                 &- 284*s25*tmp12 + 13*tmp18 + tmp609 + 370*tmp10*tmp9)) + tmp27*((-86*tmp10 - 141&
                 &*tmp174 + tmp181)*tmp25 + s15*s35*(-281*s35*tmp11 + 270*s25*tmp12 + 113*tmp17 + &
                 &tmp481 + tmp577) + tmp15*(493*s25*tmp10 + 166*tmp12 + tmp576 - 223*tmp589) + tmp&
                 &10*tmp14*(46*s25 + tmp19)*tmp627 + tmp16*(-628*s25*tmp12 + 9*tmp17 - 74*tmp18 + &
                 &s35*tmp478 + 450*tmp10*tmp9)) + ((tmp15*(1309*s25*tmp10 + 398*tmp12 + tmp574 - 7&
                 &55*tmp589) + tmp10*tmp14*(104*s25 + tmp19)*tmp627 + tmp25*(-202*tmp10 + tmp573 +&
                 & 21*tmp9) + s15*s35*(886*s25*tmp12 + 189*tmp17 + tmp481 + tmp575 - 842*tmp10*tmp&
                 &9) + tmp16*(-259*s35*tmp11 - 1882*s25*tmp12 + 27*tmp17 - 190*tmp18 + 1804*tmp10*&
                 &tmp9))*tt)/tmp446) + 16*tmp56*tmp89*(tmp72*(tmp10*tmp14*tmp434*(tmp50 + tmp584) &
                 &+ tmp25*(188*tmp174 + tmp222 + tmp585) + tmp513*(-286*s25*tmp10 - 72*tmp12 + tmp&
                 &182 + 80*tmp589) + tmp16*(-180*s35*tmp11 + 572*s25*tmp12 + tmp17 + 60*tmp18 - 18&
                 &0*tmp10*tmp9) + s15*tmp460*(128*s35*tmp11 - 45*s25*tmp12 - 42*tmp17 + tmp54 - 43&
                 &*tmp10*tmp9)) + tmp22*tmp69*(tmp14*tmp174*tmp579 + tmp16*(613*s25*tmp10 + 83*tmp&
                 &12 + tmp580 - 491*tmp589) + tmp15*(-89*tmp10 - 179*tmp174 + 19*tmp9) + s15*(-229&
                 &*s35*tmp11 - 548*s25*tmp12 + 25*tmp17 + tmp20 + 746*tmp10*tmp9)) + (tmp27*(tmp15&
                 &*(2499*s25*tmp10 + 52*tmp11 + 664*tmp12 - 1119*tmp589) + tmp10*tmp14*(175*s25 + &
                 &tmp578)*tmp627 + tmp25*(-338*tmp10 - 730*tmp174 + 20*tmp9) + s15*s35*(-1212*s35*&
                 &tmp11 + 1216*s25*tmp12 + 499*tmp17 + tmp583 - 491*tmp10*tmp9) + tmp16*(86*s35*tm&
                 &p11 - 2997*s25*tmp12 + 32*tmp17 - 314*tmp18 + 2145*tmp10*tmp9)))/tmp446 + tmp24*&
                 &(s25*tmp14*tmp35*(tmp2 + tmp581) + tmp25*(-444*tmp10 - 902*tmp174 + tmp582) + tm&
                 &p15*(3509*s25*tmp10 + 150*tmp11 + 876*tmp12 - 1981*tmp589) + s15*s35*(-604*s35*t&
                 &mp11 + 2132*s25*tmp12 + 461*tmp17 + tmp583 - 1977*tmp10*tmp9) + tmp16*(-642*s35*&
                 &tmp11 - 4751*s25*tmp12 + 81*tmp17 - 420*tmp18 + 4455*tmp10*tmp9))*tt)) + 256*s15&
                 &*s25*tmp169*tmp55*tmp56*tmp57*tt*(tmp117*tmp250*tmp426*tmp81 + tmp171*tmp2*tmp37&
                 &0*tmp524*tmp61*tmp81 - tmp117*(tmp251*tmp367 + tmp250*(93*tmp24*tmp27 + 22*tmp38&
                 & + 76*tmp390 + 9*tmp43 + (48*tmp29)/tmp446) - 16*(11*tmp24 + 16*tmp27 + tmp358)*&
                 &tmp62 + 256*tmp88 - 32*tmp89*(4/tmp446 - 7*tt) + 4*tmp63*(23*tmp29 + 40*tmp30 + &
                 &(94*tmp27)/tmp446 + 113*tmp24*tt))) + 512*s15*s25*tmp55*tmp57*tt*(s35*tmp117*tmp&
                 &250*tmp396*tt + s35*tmp101*tmp171*tmp370*tmp414*tmp61*tt + tmp117*(tmp171*tmp370&
                 &*tmp425*tmp61*tt - tmp101*(s35*tmp146*tmp171*tmp370 + tmp171*tmp370*tmp57*tmp61 &
                 &+ s35*tmp250*tmp61*tt))) + 256*s15*s35*tmp169*tmp55*(tmp250*tmp4*(tmp378*tmp383 &
                 &+ tmp368*tmp370*tmp81*tmp86)*tt - tmp22*tmp57*(tmp171*tmp370*tmp560 + tmp86*(-(t&
                 &mp81*(tmp127*tmp635 + tmp251*tmp635 + tmp1*(tmp286 + tmp555 + tmp97))) + tmp171*&
                 &tmp370*tmp61*(8*m2*tmp171 - 3*tmp251 + 4*tmp63)*tt)) - tmp22*(s25*tmp171*tmp374*&
                 &tmp383*tmp57*tmp61*tt + tmp378*(-(tmp251*tmp558) - 4*tmp63*(184*tmp24*tmp27 + 76&
                 &*tmp38 + 211*tmp390 + 2*tmp43 + tmp633) + 256*tmp635*tmp88 + 8*tmp62*(11*tmp29 +&
                 & 100*tmp30 + (120*tmp27)/tmp446 + 221*tmp24*tt) + tmp65*(28*tmp207 + tmp211 + 64&
                 &*tmp24*tmp29 + 120*tmp27*tmp30 + (14*tmp43)/tmp446 + 97*tmp38*tt) - 32*tmp89*(32&
                 &*tmp24 + 14*tmp27 + (57*tt)/tmp446))))))/16. + (discbt*tmp161*tmp162*tmp163*tmp1&
                 &64*tmp165*tmp166*tmp168*tmp170*(-512*s15*s25*tmp292*tmp55*tmp56*tmp57*tmp61*tmp6&
                 &6*tmp67*tt + 256*tmp55*tmp56*tmp61*(1024*tmp13*tmp175*tmp3*tmp8 + s15*tmp174*tmp&
                 &29*(tmp36*tmp38*tmp4 + 4*(-6*s15*s25 + tmp132 + tmp16 + 13*tmp174 + tmp177 + tmp&
                 &39)*tmp390*tmp4 + tmp153*tmp24*(-26*s25*tmp10 + tmp15 + (-11*s25 + 9*s35)*tmp16 &
                 &+ s15*(tmp198 + tmp221 + tmp31) + tmp37 + tmp466 + tmp505) + tmp29*tmp36*tmp4*tm&
                 &p634 + tmp43*(-29*s25*tmp10 + tmp16*(5*s35 + tmp173) + tmp187 + tmp37 + tmp469 +&
                 & tmp513 + s15*(-19*tmp10 + 35*tmp174 - 14*tmp9))) + tmp250*tmp27*(s25*tmp36*tmp3&
                 &8*tmp454*tmp524 - tmp22*tmp390*(tmp15*(19*tmp174 + tmp39 + tmp44) + tmp473 + s15&
                 &*s25*(326*s25*tmp10 - 156*tmp12 + tmp40 - 177*tmp589) + tmp528*(tmp235 + tmp40 +&
                 & tmp41 - 82*tmp589)) + s15*s25*tmp43*(tmp16*(41*tmp10 - 73*tmp174 + tmp194) + (s&
                 &25 + 6*s35)*tmp513 + s15*(253*s25*tmp10 - 137*tmp12 + tmp179 - 104*tmp589) + s35&
                 &*(-199*s25*tmp10 - 19*tmp11 + tmp42 + 134*tmp589)) + tmp153*tmp24*(tmp25*(tmp120&
                 & + tmp45 + tmp48) + tmp513*(tmp182 + tmp214 + tmp249 - 38*tmp589) + s35*tmp515*(&
                 &-71*s25*tmp10 + tmp188 + tmp47 + 39*tmp589) + tmp14*tmp48*tmp9 + tmp16*(-90*s35*&
                 &tmp11 - 114*s25*tmp12 + tmp189 + tmp191 + 214*tmp10*tmp9)) + tmp29*tmp634*(tmp25&
                 &*(tmp184 + tmp238 + tmp39) + tmp475 + tmp513*(22*s25*tmp10 + tmp231 + tmp46 - 45&
                 &*tmp589) + s15*tmp174*(-173*s25*tmp10 - 6*tmp11 + tmp42 + 95*tmp589) + tmp16*(-1&
                 &39*s25*tmp12 + tmp481 + tmp11*tmp485 + tmp611 + 257*tmp10*tmp9))) + 8*tmp62*(tmp&
                 &13*tmp3*tmp379*tmp8 + tmp22*tmp390*(s35*tmp331*(tmp237 + tmp50) + (46*s25*tmp10 &
                 &+ tmp12 + tmp218 + tmp474)*tmp528 + (tmp121 + tmp181 + tmp197)*tmp592 + s15*(-15&
                 &9*s35*tmp11 - 132*s25*tmp12 + 13*tmp17 + tmp52 + 274*tmp10*tmp9)) + tmp43*(tmp14&
                 &*tmp455*(tmp2 + tmp584) + tmp15*(56*s25*tmp10 - 134*tmp11 - 78*tmp12 + 346*tmp58&
                 &9) + tmp25*(40*tmp10 - 69*tmp174 - 66*tmp9) + tmp16*(499*s35*tmp11 + 257*s25*tmp&
                 &12 - 68*tmp17 + 36*tmp18 - 819*tmp10*tmp9) + s15*s35*(-387*s35*tmp11 - 242*s25*t&
                 &mp12 + tmp53 + tmp54 + 547*tmp10*tmp9)) - 3*tmp24*tmp27*(s15*s35*(243*s35*tmp11 &
                 &+ 262*s25*tmp12 - 10*tmp17 + tmp216 + tmp51) + tmp513*(tmp220 + tmp10*tmp570 + t&
                 &mp572 - 176*tmp589) + tmp10*tmp14*(tmp2 + tmp202)*tmp627 + tmp25*(tmp239 + tmp24&
                 &2 + 42*tmp9) + tmp16*(-399*s35*tmp11 - 415*s25*tmp12 + 44*tmp17 + tmp205 + 841*t&
                 &mp10*tmp9)) + (tmp29*(tmp14*tmp455*(81*s25 + tmp50) - tmp15*(414*tmp11 + tmp453 &
                 &+ tmp501 - 1117*tmp589) + tmp25*(158*tmp10 - 147*tmp174 - 205*tmp9) + tmp16*(134&
                 &3*s35*tmp11 + 838*s25*tmp12 - 209*tmp17 + 150*tmp18 - 2316*tmp10*tmp9) + s15*s35&
                 &*(-649*s35*tmp11 + 71*tmp17 + tmp502 + tmp52 + 1258*tmp10*tmp9)))/tmp446) + tmp6&
                 &3*tmp74*(tmp174*tmp38*tmp524*(-20*s15*s25 + 17*s15*s35 - 20*tmp10 + 43*tmp174 + &
                 &tmp303 - 23*tmp9) + tmp43*(2*tmp25*(s35*tmp202 + tmp222 + tmp48) + tmp15*(tmp215&
                 & + tmp448 + tmp49 - 219*tmp589) + s15*tmp174*(-505*s25*tmp10 - 53*tmp11 + 216*tm&
                 &p12 + 342*tmp589) + tmp597 + tmp16*(-308*s35*tmp11 - 327*s25*tmp12 + 18*tmp17 + &
                 &tmp598 + 671*tmp10*tmp9)) + tmp390*(3*tmp25*(tmp39 + tmp44 + tmp45) + 6*tmp15*(3&
                 &2*s25*tmp10 + tmp40 + tmp46 - 56*tmp589) + s35*tmp515*(-291*s25*tmp10 + 146*tmp1&
                 &2 + tmp47 + 147*tmp589) + tmp595 + tmp16*(-370*s35*tmp11 - 514*s25*tmp12 + tmp19&
                 &0 + tmp206 + 914*tmp10*tmp9)) + (tmp29*((37*s25*tmp10 + 80*tmp11 + tmp507 + tmp5&
                 &10)*tmp513 + s15*tmp174*(-853*s25*tmp10 - 48*tmp11 + 428*tmp12 + 473*tmp589) - 6&
                 &8*tmp10*tmp14*tmp9 + tmp25*(-68*tmp10 + 85*tmp174 + 80*tmp9) + tmp16*(-697*s35*t&
                 &mp11 - 587*s25*tmp12 - 68*tmp18 + tmp53 + 1369*tmp10*tmp9)))/tmp446 + tmp24*tmp2&
                 &7*(tmp15*(239*s25*tmp10 + 158*tmp11 + tmp245 - 727*tmp589) + s15*tmp450*(157*s25&
                 &*tmp10 - 80*tmp12 + tmp186 - 80*tmp589) - 64*tmp10*tmp14*tmp9 + tmp25*(-64*tmp10&
                 & + 86*tmp174 + 79*tmp9) + tmp16*(-834*s35*tmp11 - 885*s25*tmp12 + 79*tmp17 - 64*&
                 &tmp18 + 1805*tmp10*tmp9))) + 128*tmp4*tmp87*((12*tmp135*tmp172*tmp8)/tmp446 + (s&
                 &35*(s35 + tmp176)*tmp331 + 6*tmp16*(tmp195 + tmp41 + tmp459 + tmp600) + tmp15*(9&
                 &*tmp174 + tmp199 + 25*tmp9) + s15*(-99*s35*tmp11 - 72*s25*tmp12 + 23*tmp17 + tmp&
                 &51 + 150*tmp10*tmp9))*tt) - 16*tmp89*(12*tmp13*tmp3*tmp30*tmp8 + tmp29*((84*tmp1&
                 &0 - 80*tmp174 + tmp196)*tmp25 + tmp10*tmp14*tmp434*(23*s25 + tmp50) + tmp15*(200&
                 &*s25*tmp10 - 262*tmp11 - 160*tmp12 + 468*tmp589) + tmp16*(632*s35*tmp11 + 68*tmp&
                 &18 + tmp28 + s25*tmp42 - 772*tmp10*tmp9) + s15*tmp460*(-57*s35*tmp11 - 49*s25*tm&
                 &p12 + tmp180 + tmp54 + 87*tmp10*tmp9)) + (tmp27*(tmp14*tmp455*tmp579 - 2*tmp15*(&
                 &79*s25*tmp10 + 241*tmp11 + 172*tmp12 - 676*tmp589) + tmp25*(178*tmp10 - 127*tmp1&
                 &74 - 235*tmp9) + tmp16*(1571*s35*tmp11 + 1133*s25*tmp12 - 247*tmp17 + 154*tmp18 &
                 &- 2795*tmp10*tmp9) + s15*s35*(-719*s35*tmp11 - 836*s25*tmp12 + 68*tmp17 + tmp26 &
                 &+ 1475*tmp10*tmp9)))/tmp446 + tmp22*tmp24*(s25*tmp14*tmp460*(tmp19 + tmp471) + t&
                 &mp16*(179*s25*tmp10 + 112*tmp11 + 20*tmp12 - 380*tmp589) + tmp15*(-32*tmp10 + tm&
                 &p23 + 50*tmp9) + s15*(-411*s35*tmp11 - 324*s25*tmp12 + 62*tmp17 + tmp26 + 661*tm&
                 &p10*tmp9))*tt) + 32*tmp88*(26*tmp13*tmp24*tmp3*tmp8 + tmp27*(tmp14*(tmp19 + tmp2&
                 &12)*tmp455 + tmp15*(55*s25*tmp10 - 264*tmp11 - 178*tmp12 + 571*tmp589) + tmp25*(&
                 &92*tmp10 - 55*tmp174 - 129*tmp9) + tmp16*(681*s35*tmp11 + 288*s25*tmp12 + 80*tmp&
                 &18 + tmp28 - 1006*tmp10*tmp9) + s15*s35*(-282*s25*tmp12 + tmp20 + tmp575 + tmp60&
                 &7 + 460*tmp10*tmp9)) + (tmp22*(s35*(tmp19 + tmp233)*tmp331 + tmp528*(92*tmp11 + &
                 &25*tmp12 + tmp49 - 234*tmp589) + tmp15*(-56*tmp10 + tmp23 + 89*tmp9) + s15*(-469&
                 &*s35*tmp11 - 352*s25*tmp12 + 95*tmp17 + tmp20 + 720*tmp10*tmp9))*tt)/tmp446)) - &
                 &256*s35*tmp524*tmp55*tmp57*tmp61*(tmp308*tmp61*tmp627*tmp66*tmp67*tmp81 + tmp59*&
                 &tmp61*tmp66*tmp81*tmp86 + 2*tmp86*(m2*tmp146*(-46*tmp24*tmp27 - 10*tmp38 - 46*tm&
                 &p390 + tmp43 - (10*tmp29)/tmp446) + tmp27*tmp558 + 4*tmp63*(148*tmp24*tmp27 + 8*&
                 &tmp38 + 78*tmp390 - 6*tmp43 + tmp633) + 2048*tmp87 - 384*(tmp128 + 8/tmp446)*tmp&
                 &88 - 8*tmp62*(tmp29 + 48*tmp30 + (158*tmp27)/tmp446 + 198*tmp24*tt) + 32*tmp89*(&
                 &20*tmp27 + tmp362 + (97*tt)/tmp446))) + 256*tmp174*tmp55*tt*(m2*tmp101*tmp342*tm&
                 &p61*tmp627*tmp66 + s15*s25*tmp101*tmp126*tmp57*tmp61*tmp66*tmp67*tt - tmp542*(tm&
                 &p101*tmp122*tmp150*tmp66 + s25*tmp145*tmp61*tmp67*tt) - tmp368*(tmp101*tmp308*tm&
                 &p61*tmp66*tmp67*tmp9 - tmp347*(tmp101*tmp160*tmp64 + s25*tmp154*tmp61*tmp66*tmp6&
                 &7*tt))) + 256*s15*tmp55*tmp57*tt*(s35*tmp101*tmp250*tmp302*tmp61*tmp66 + s25*tmp&
                 &10*tmp101*tmp523*tmp61*tmp66*tmp67*tt - tmp533*(tmp101*tmp130*tmp150*tmp66 + s35&
                 &*tmp145*tmp61*tmp67*tt) + tmp2*(s35*tmp543*tmp66*tmp67 + tmp311*(tmp101*tmp160*t&
                 &mp2 + s35*tmp154*tmp61*tmp66*tmp67*tt))) + 256*s15*tmp174*tmp55*tt*(m2*tmp101*tm&
                 &p107*tmp386*tmp520*tmp61 + tmp101*tmp399*tmp57*tmp61*tmp66*tmp67*tt - tmp107*(tm&
                 &p410*tmp61*tmp66*tmp67*tt - tmp101*(s25*tmp146*tmp66*tmp67 + s15*tmp61*(tmp127 +&
                 & tmp27 + tmp1*tt)))) + 256*s15*tmp174*tmp55*tmp61*(tmp108*tmp4*tmp59*tmp61*tmp66&
                 &*tmp81 + tmp4*tmp419*tmp428*tmp61*tmp66*tmp67*tmp81 - 2*tmp108*(tmp445*tmp61*tmp&
                 &66*tmp67*tt + tmp81*(-64*tmp4*tmp62 + tmp250*((5*tmp4)/tmp446 + tmp155*(tmp110 +&
                 & tmp111 + tmp64))*tt + tmp127*(tmp109 + (7*s15 - 8*s35 + tmp64)*tt) + tmp27*(tmp&
                 &109 + (2*s15 + tmp19 + tmp64)*tt)))) - 256*s15*s25*tmp55*tmp57*tt*(tmp101*tmp117&
                 &*tmp250*tmp57*tmp61*tmp66 + s35*tmp101*tmp414*tmp61*tmp66*tmp67*tt + tmp117*(tmp&
                 &425*tmp61*tmp66*tmp67*tt + tmp101*(tmp57*tmp61*tmp66*tt - tmp67*(tmp127*tmp57 + &
                 &s35*tmp98 + tmp250*(tmp57/tmp446 + (s15 + tmp124)*tt)))))))/16. - (discbs*tmp161&
                 &*tmp162*tmp163*tmp164*tmp165*tmp166*tmp170*tmp446*tmp447*((256*tmp411*tmp518*tmp&
                 &55*tmp589*tt*(-(tmp361*tmp542) + tmp545*tmp57 + s15*tmp101*tmp126*tmp57*tmp61*tt&
                 &))/tmp446 + 256*s35*tmp4*tmp411*tmp518*tmp55*tmp61*tmp626*(tmp378*tmp559 + tmp56&
                 &2*tmp57 + s25*tmp374*tmp57*tmp61*tmp81*tt) + 256*s35*tmp518*tmp55*tmp57*tmp626*t&
                 &t*(tmp252*tmp536 + tmp411*(-(tmp361*tmp533) + tmp101*tmp174*tmp523*tmp61*tt)) - &
                 &256*tmp55*tmp56*tmp61*tt*(s15*tmp174*tmp30*(tmp43*tmp494 + tmp38*tmp4*(12*s15*s2&
                 &5 - 17*s15*s35 + 5*tmp16 + tmp44 + tmp464 + tmp567) + 2*tmp390*(15*s25*tmp10 + s&
                 &15*(33*tmp10 + tmp120 - 31*tmp174) + 14*tmp16*tmp465 + tmp468 + tmp47 + tmp470 +&
                 & tmp588) + tmp381*(-6*tmp12 + tmp466 + tmp16*(tmp176 + tmp467) + tmp468 + s15*(1&
                 &6*tmp10 + tmp181 + tmp476) + tmp10*tmp566 + tmp592) + tmp29*(16*s25*tmp10 + 5*tm&
                 &p15 + 5*tmp16*(tmp183 + tmp203) + tmp468 + tmp469 + tmp470 + s15*(32*tmp10 + tmp&
                 &44 + tmp472))*tmp634) - 256*tmp3*tmp87*((tmp4*(s15*s25*(28*tmp10 + tmp120 - 24*t&
                 &mp174) + tmp15*(tmp237 + tmp460) + (tmp120 + tmp121 - 12*tmp174)*tmp528 + 4*tmp3&
                 &*tmp589))/tmp446 + s35*tmp631*(tmp15 + tmp174*tmp3 + tmp3*tmp528 + s15*(tmp10 + &
                 &tmp213 + tmp9))) + m2*tmp141*(s25*tmp43*tmp494*tmp516 + tmp22*tmp313*(tmp15*(-44&
                 &*tmp174 + tmp181 + tmp39) + tmp473 + s15*s25*(-120*tmp12 + tmp186 + tmp474 + tmp&
                 &477) + tmp16*(155*s25*tmp10 + tmp231 + tmp46 - 120*tmp589)) + s35*tmp29*tmp626*t&
                 &mp627*(92*s25*tmp10 + 19*tmp15 + tmp484 + tmp16*(69*s25 + tmp485) - 23*tmp589 + &
                 &tmp594 + s15*(148*tmp10 - 170*tmp174 + 41*tmp9)) + 2*tmp390*(tmp475 + tmp15*(tmp&
                 &231 + tmp37 + tmp477 - 103*tmp589) + s15*tmp174*(-89*s25*tmp10 + tmp42 + tmp478 &
                 &- 14*tmp589) + 3*tmp25*(tmp121 + tmp476 + tmp9) + tmp16*(tmp479 + tmp480 + tmp48&
                 &1 + tmp483 + 211*tmp10*tmp9)) + tmp24*tmp287*(tmp25*(-46*tmp174 + tmp181 + tmp39&
                 &) + tmp475 + tmp513*(tmp186 + tmp10*tmp232 + tmp46 - 61*tmp589) + s35*tmp515*(-6&
                 &3*s25*tmp10 + 51*tmp12 + tmp482 + 2*tmp589) + tmp16*(-56*s35*tmp11 - 258*s25*tmp&
                 &12 + tmp481 + tmp483 + 268*tmp10*tmp9))) + 1024*s35*tmp175*tmp4*(s25*tmp14 + s15&
                 &*(tmp213 + tmp223 + tmp247) + tmp16*tmp6)*tmp90 + 8*tmp62*((tmp29*tmp460*(tmp193&
                 &*tmp25 + tmp503 - 2*tmp15*(tmp10 + tmp449 + tmp504) + (9*tmp12 + s25*tmp198 + tm&
                 &p47 + tmp505)*tmp515 + tmp16*(-32*s25*tmp10 - 22*tmp11 + tmp12 + 56*tmp589)))/tm&
                 &p446 + tmp22*tmp313*(tmp15*(-282*tmp174 + tmp495 + tmp582) + tmp16*(1081*s25*tmp&
                 &10 + 138*tmp11 + 136*tmp12 - 1006*tmp589) + 136*tmp14*tmp589 + s15*s25*(69*tmp11&
                 & - 1004*tmp12 - 588*tmp589 + tmp608)) + 2*tmp390*(tmp25*(-143*tmp174 + tmp496 + &
                 &tmp497) + s15*tmp174*(-91*s25*tmp10 + 102*tmp11 + 234*tmp12 - 245*tmp589) + tmp5&
                 &13*(245*s25*tmp10 + tmp498 + tmp509 + tmp612) + tmp14*tmp497*tmp9 + tmp16*(-95*s&
                 &35*tmp11 - 581*s25*tmp12 - 66*tmp18 + tmp499 + 533*tmp10*tmp9)) + tmp24*tmp27*(t&
                 &mp25*(-42*tmp10 - 91*tmp174 + tmp500) + tmp15*(463*s25*tmp10 + tmp42 + tmp574 - &
                 &377*tmp589) + s15*tmp174*(-396*s25*tmp10 + 53*tmp11 + tmp501 + 31*tmp589) - 42*t&
                 &mp10*tmp14*tmp9 + tmp16*(-233*s35*tmp11 - 42*tmp18 + tmp502 + tmp614 + 826*tmp10&
                 &*tmp9)) + tmp130*tmp43*(tmp192 + tmp15*(s35 + tmp237) + (tmp120 + tmp223 + tmp45&
                 &0)*tmp528 + s15*(tmp12 + tmp182 + tmp506 - 16*tmp589))*tmp90) - 16*tmp89*(tmp27*&
                 &tmp634*((tmp121 + tmp194 + tmp228)*tmp25 + tmp15*(36*s25*tmp10 + tmp214 + tmp459&
                 & - 46*tmp589) + s15*s25*tmp10*(-61*tmp174 + tmp458 + tmp590) + tmp121*tmp14*tmp9&
                 & + tmp16*(-43*s35*tmp11 - 67*s25*tmp12 + 4*tmp17 + tmp51 + 107*tmp10*tmp9)) + tm&
                 &p146*tmp24*(tmp15*(199*s25*tmp10 + 100*tmp12 + tmp457 - 171*tmp589) + s15*tmp174&
                 &*(47*s25*tmp10 + 65*tmp11 + 94*tmp12 + tmp606) - 50*tmp10*tmp14*tmp9 + tmp25*(-5&
                 &0*tmp10 - 70*tmp174 + 28*tmp9) + tmp16*(28*tmp17 - 50*tmp18 + tmp479 + tmp480 + &
                 &189*tmp10*tmp9)) + s35*tmp366*(-4*tmp14*tmp174 + tmp15*(tmp176 + tmp460) + tmp16&
                 &*(-17*tmp174 + tmp461 + tmp462) + s15*(tmp214 + tmp40 + tmp463 - 25*tmp589))*tmp&
                 &90 + tmp22*(s15*s25*(1694*s25*tmp10 - 1120*tmp12 + tmp456 - 675*tmp589) + tmp528&
                 &*(548*s25*tmp10 + 95*tmp12 + tmp456 - 566*tmp589) + 190*tmp14*tmp589 + tmp15*(-1&
                 &90*tmp10 - 267*tmp174 + 101*tmp9))*tmp92) + 64*tmp88*(-(tmp22*tmp24*(tmp16*(272*&
                 &s25*tmp10 + 72*tmp11 + 59*tmp12 - 323*tmp589) + s15*s25*(481*s25*tmp10 - 310*tmp&
                 &12 + tmp448 - 207*tmp589) + 59*tmp14*tmp589 + tmp15*(-59*tmp10 - 57*tmp174 + 36*&
                 &tmp9))) + s35*tmp27*(tmp14*tmp228 + tmp15*(s25 + tmp454) + s15*(tmp11 + tmp41 + &
                 &tmp455 + tmp564) + tmp528*(tmp593 + tmp9))*tmp90 + ((tmp10*tmp14*tmp31 + tmp25*(&
                 &-13*tmp10 + tmp449 + tmp450) + s15*s25*tmp454*(tmp186 + tmp451 + tmp452 + tmp506&
                 &) + tmp15*(26*tmp12 + tmp453 - 21*tmp589 + tmp603) + tmp16*(8*tmp17 - 13*tmp18 +&
                 & tmp11*tmp203 + tmp12*tmp627))*tt)/tmp446) - 4*tmp63*(-(tmp207*tmp22*(s15*s25*(7&
                 &43*s25*tmp10 + 23*tmp11 - 484*tmp12 + tmp507) + tmp16*(571*s25*tmp10 + 46*tmp11 &
                 &+ 46*tmp12 - 484*tmp589) + 46*tmp14*tmp589 + tmp15*(-156*tmp174 + tmp571 + tmp61&
                 &3))) + s15*tmp174*tmp43*tmp634*(-25*s25*tmp10 + tmp16*(-7*s25 + tmp454) + tmp469&
                 & + tmp513 + tmp514 + 18*tmp589 + s15*(-15*tmp10 + 29*tmp174 - 12*tmp9)) + s15*s3&
                 &5*tmp24*tmp29*tmp627*(164*s25*tmp10 + 17*tmp15 + tmp469 + tmp16*(-129*s35 + tmp5&
                 &11) + tmp512 - 69*tmp589 + s15*(204*tmp10 - 262*tmp174 + 75*tmp9)) + tmp27*tmp30&
                 &*(tmp15*(705*s25*tmp10 + 88*tmp12 + tmp580 - 483*tmp589) + s15*tmp174*(-474*s25*&
                 &tmp10 + 83*tmp11 + 400*tmp12 - 9*tmp589) + tmp14*tmp243*tmp9 + tmp25*(-155*tmp17&
                 &4 + tmp243 + 22*tmp9) + tmp16*(-245*s35*tmp11 - 950*s25*tmp12 + 22*tmp17 - 44*tm&
                 &p18 + 1040*tmp10*tmp9)) + s35*tmp211*tmp515*tmp517*tmp90 + tmp379*(tmp25*(-123*t&
                 &mp174 + tmp239 + tmp508) + tmp15*(458*s25*tmp10 + tmp509 + tmp510 - 280*tmp589) &
                 &+ s15*tmp174*(-171*s25*tmp10 + 66*tmp11 + 214*tmp12 - 109*tmp589) + tmp14*tmp239&
                 &*tmp9 + tmp16*(-549*s25*tmp12 - 34*tmp18 + tmp180 + tmp616 + 517*tmp10*tmp9))*tt&
                 &)) + 256*s15*tmp174*tmp55*tmp61*(m2*tmp108*tmp146*tmp252*tmp56*tmp61*tmp81 + (tm&
                 &p4*tmp411*tmp428*tmp518*tmp57*tmp61*tmp81*tt - tmp108*(-((tmp411*tmp429*tmp518 +&
                 & 2*(tmp4*tmp411*tmp518 + m2*tmp56*tmp58)*tmp61)*tmp81) + tmp411*tmp445*tmp518*tm&
                 &p61*tt))/tmp446) + 256*s15*s25*tmp55*tmp56*tmp57*tmp61*tt*(tmp117*tmp252*tmp61*t&
                 &mp65*tmp81 - (s35*tmp411*tmp518*tmp524*tmp61*tmp81 + tmp117*(-(tmp24*tmp367) + (&
                 &32*tmp24*tmp27 + 34*tmp38 + 60*tmp390 + tmp43 + (10*tmp29)/tmp446)*tmp65 + 1280*&
                 &tmp88 - 128*(tmp128 + tmp316)*tmp89 - 4*tmp63*(8*tmp29 + 112*tmp30 + (48*tmp27)/&
                 &tmp446 + 139*tmp24*tt) + 8*tmp62*(178*tmp24 + 19*tmp27 + (130*tt)/tmp446)))/tmp4&
                 &46) - 256*s15*s25*tmp55*tmp57*tt*(m2*tmp101*tmp117*tmp146*tmp411*tmp56*tmp61 + (&
                 &s35*tmp101*tmp411*tmp414*tmp518*tmp61*tt + tmp117*(tmp411*tmp425*tmp518*tmp61*tt&
                 & - tmp101*(s35*tmp146*tmp411*tmp518 + tmp411*tmp518*tmp57*tmp61 + tmp250*tmp56*t&
                 &mp61*tt)))/tmp446) + 256*s15*tmp174*tmp55*tt*(m2*tmp101*tmp107*tmp146*tmp252*tmp&
                 &56*tmp61 + (tmp101*tmp399*tmp411*tmp518*tmp57*tmp61*tt - tmp107*(tmp410*tmp411*t&
                 &mp518*tmp61*tt - tmp101*(-(tmp24*tmp630) + s15*tmp68 + tmp65*(s15*tmp285 + tmp27&
                 &*tmp56 + ((4*s15 - 5*s25)*tt)/tmp446) - 8*tmp63*(tmp404 + 4*tmp626 + tmp64*tt)))&
                 &)/tmp446)))/16.

  END FUNCTION NTSOFT_POLE

    FUNCTION SOFT()
  implicit none
  real SOFT
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  
  tmp1 = 21*ss
  tmp2 = -s35
  tmp3 = s15 + s25 + tmp2
  tmp4 = s15 + tmp2
  tmp5 = tmp4**2
  tmp6 = s15**(-2)
  tmp7 = s25**(-2)
  tmp8 = tmp3**(-2)
  tmp9 = s35**(-2)
  tmp10 = 1/tt
  tmp11 = -4*m2
  tmp12 = ss + 1/tmp10 + tmp11
  tmp13 = s25 + tmp2
  tmp14 = s25*tmp13
  tmp15 = s25 + s35
  tmp16 = s15*tmp15
  tmp17 = tmp14 + tmp16
  tmp18 = tmp17**2
  tmp19 = m2*tmp18*tmp5
  tmp20 = ss*tmp5
  tmp21 = -s25
  tmp22 = s15 + tmp21
  tmp23 = -2*s35
  tmp24 = s15 + s25 + tmp23
  tmp25 = (tmp22*tmp24)/tmp10
  tmp26 = tmp20 + tmp25
  tmp27 = s15*s35*tmp21*tmp26*tmp3
  tmp28 = tmp19 + tmp27
  tmp29 = ss**3
  tmp30 = m2**2
  tmp31 = ss**2
  tmp32 = tmp10**(-3)
  tmp33 = tmp10**(-2)
  tmp34 = 4*m2
  tmp35 = 1/tmp12
  tmp36 = m2**3
  tmp37 = ss + 1/tmp10
  tmp38 = 1/tmp37
  tmp39 = 3*tmp31
  tmp40 = -(1/tmp10)
  tmp41 = tmp34 + tmp40
  tmp42 = 1/tmp41
  tmp43 = m2**4
  tmp44 = 2*ss
  tmp45 = 3*tmp33
  tmp46 = -ss
  tmp47 = tmp34 + tmp46
  tmp48 = 1/tmp7
  tmp49 = 1/tmp9
  tmp50 = tmp13**2
  tmp51 = s25**3
  tmp52 = s35**3
  tmp53 = 1/tmp6
  tmp54 = tmp48*tmp49*tmp50
  tmp55 = s15**4
  tmp56 = 6*s25*s35
  tmp57 = tmp48 + tmp49 + tmp56
  tmp58 = s15**3
  tmp59 = -tmp52
  tmp60 = s25**4
  tmp61 = -10*s35*tmp51
  tmp62 = s35**4
  tmp63 = tmp35**2
  tmp64 = ss**4
  tmp65 = 5*ss
  tmp66 = 3/tmp10
  tmp67 = tmp32*tmp46
  tmp68 = tmp10**(-4)
  tmp69 = -4*tmp33
  tmp70 = tmp44/tmp10
  tmp71 = ss*tmp33
  tmp72 = 1/tmp47
  tmp73 = 1/tmp33
  tmp74 = 64*tmp43
  tmp75 = -6*ss
  tmp76 = 1/tmp10 + tmp75
  tmp77 = ss/tmp10
  tmp78 = tmp31 + tmp33 + tmp77
  tmp79 = 3*tmp71
  tmp80 = 128*tmp43
  tmp81 = 4*tmp29
  tmp82 = 2*tmp32
  tmp83 = 4*tmp64
  tmp84 = 1/tmp10 + tmp44
  tmp85 = 3*tmp32
  tmp86 = -tmp77
  tmp87 = -2*m2*tmp17*tmp4
  tmp88 = 24*tmp30
  tmp89 = 3*ss
  tmp90 = 2/tmp10
  tmp91 = tmp89 + tmp90
  tmp92 = -2*m2*tmp91
  tmp93 = tmp86 + tmp88 + tmp92
  tmp94 = tmp40 + tmp47
  tmp95 = 8*tmp30
  tmp96 = ss/tmp38
  tmp97 = 4/tmp10
  tmp98 = tmp75 + tmp97
  tmp99 = m2*tmp98
  tmp100 = tmp95 + tmp96 + tmp99
  tmp101 = 2*m2*tmp17*tmp4
  tmp102 = s35*ss*tmp4
  tmp103 = (s25*tmp24)/tmp10
  tmp104 = tmp102 + tmp103
  tmp105 = -(s15*tmp104)
  tmp106 = tmp101 + tmp105
  tmp107 = -s15
  tmp108 = s35 + tmp107
  tmp109 = s15*ss*tmp108
  tmp110 = tmp22*tmp3*tmp40
  tmp111 = tmp109 + tmp110
  tmp112 = s35*tmp111
  tmp113 = tmp101 + tmp112
  tmp114 = s25*ss*tmp4
  tmp115 = (s35*tmp22)/tmp10
  tmp116 = tmp114 + tmp115
  tmp117 = tmp116*tmp3
  tmp118 = tmp117 + tmp87
  tmp119 = ss*tmp3*tmp4
  tmp120 = (s15*tmp24)/tmp10
  tmp121 = tmp119 + tmp120
  tmp122 = s25*tmp121
  tmp123 = tmp122 + tmp87
  tmp124 = 2*m2
  tmp125 = tmp124 + tmp46
  tmp126 = 16*tmp30
  tmp127 = tmp11/tmp38
  tmp128 = tmp126 + tmp127 + tmp86
  tmp129 = s25*tmp119
  tmp130 = s25*tmp120
  tmp131 = tmp129 + tmp130 + tmp87
  tmp132 = 256*tmp43
  tmp133 = 10*ss
  tmp134 = 1/tmp10 + tmp133
  tmp135 = 8*tmp31
  tmp136 = 11*tmp77
  tmp137 = 2*tmp29
  tmp138 = tmp31*tmp66
  tmp139 = -32*tmp134*tmp36
  tmp140 = tmp135 + tmp136 + tmp45
  tmp141 = tmp126*tmp140
  tmp142 = tmp137 + tmp138 + tmp32 + tmp79
  tmp143 = tmp142*tmp66
  tmp144 = 8*tmp29
  tmp145 = (36*tmp31)/tmp10
  tmp146 = 30*tmp71
  tmp147 = 13*tmp32
  tmp148 = tmp144 + tmp145 + tmp146 + tmp147
  tmp149 = -2*m2*tmp148
  tmp150 = tmp132 + tmp139 + tmp141 + tmp143 + tmp149
  SOFT = -32*scalarc0u*tmp10*tmp28*tmp35*tmp38*tmp6*(-(tmp31 - tmp33)**2 + (64*tmp36)/tmp&
          &10 - 8*tmp30*(tmp133/tmp10 + tmp39 + tmp45) + (tmp124*(1/tmp10 + tmp65))/tmp38**&
          &2)*tmp7*tmp8*tmp9 - 16*discbt*((s25*s35*tmp107*tmp118*tmp125*tmp128)/tmp10 + s15&
          &*s25*s35*tmp118*tmp150 + (s25*s35*tmp106*tmp125*tmp128*tmp3)/tmp10 + (s25*tmp107&
          &*tmp113*tmp125*tmp128*tmp3)/tmp10 + (s15*s35*tmp123*tmp125*tmp128*tmp3)/tmp10 + &
          &s15*s25*tmp113*tmp150*tmp3 + s35*tmp107*tmp131*tmp150*tmp3 + s35*tmp106*tmp150*t&
          &mp21*tmp3)*tmp35*tmp42*tmp6*tmp7*tmp73*tmp8*tmp9 - (64*scalarc0ir6s*tmp10*tmp35*&
          &tmp6*tmp7*((4*tmp18*tmp30*tmp5)/tmp38 - m2*(tmp31*tmp5*(2*s15*s25*(2*s25*s35 + t&
          &mp48 - 3*tmp49) + tmp48*tmp50 + tmp53*tmp57) + tmp33*(s15*s25*tmp23*(-9*s35*tmp4&
          &8 + 5*s25*tmp49 + 3*tmp51 + tmp52) + tmp54 + tmp55*tmp57 + 2*tmp58*(s35*tmp48 - &
          &9*s25*tmp49 + tmp51 + tmp59) + tmp53*(2*tmp48*tmp49 + 14*s25*tmp52 + tmp60 + tmp&
          &61 + tmp62)) + (-6*s15*s25*s35*(-3*s35*tmp48 + s25*tmp49 + tmp51 + tmp52) + tmp5&
          &4 + (10*s25*s35 + tmp48 + tmp49)*tmp55 + 2*tmp58*(3*s35*tmp48 - 15*s25*tmp49 + t&
          &mp51 + tmp59) + tmp53*(-6*tmp48*tmp49 + 26*s25*tmp52 + tmp60 + tmp61 + tmp62))*t&
          &mp77) + s15*s25*s35*tmp26*tmp3*tmp78)*tmp8*tmp9)/tmp72 + 32*scalarc0t*tmp10*tmp2&
          &8*tmp35*tmp42*tmp6*tmp7*tmp8*(tmp132 - 96*tmp36*(tmp44 + tmp66) + 32*tmp30*(tmp3&
          &1 + tmp45 + 7*tmp77) - (2*m2*(16*tmp31 + 7*tmp33 + 28*tmp77))/tmp10 + tmp33*tmp8&
          &4**2)*tmp9 + 32*discbu*tmp10*tmp28*tmp38*tmp6*tmp63*tmp7*tmp8*((-5*tmp29)/tmp10 &
          &- tmp31*tmp33 - 4*tmp64 + 32*tmp36*(tmp65 + tmp66) + 3*tmp68 + m2*(44*tmp29 + (3&
          &8*tmp31)/tmp10 - 22*tmp32 - 20*tmp71) - 8*tmp30*(19*tmp31 + tmp69 + 9*tmp77) + t&
          &mp32*tmp89)*tmp9 + 256*tmp28*tmp6*tmp63*tmp7*tmp73*tmp8*(tmp74 + 16*tmp36*tmp76 &
          &+ tmp78**2 + tmp11*(tmp138 + 3*tmp29 + tmp79 + tmp82) + 4*tmp30*(13*tmp31 + tmp4&
          &5 + tmp89/tmp10))*tmp9 - 16*discbs*tmp10*tmp35*tmp6*tmp7*tmp72*tmp8*tmp9*((s15*s&
          &25*s35*tmp118*tmp93)/tmp10 + (s15*s25*tmp113*tmp3*tmp93)/tmp10 + (s35*tmp107*tmp&
          &123*tmp3*tmp93)/tmp10 + s25*s35*tmp106*tmp3*tmp40*tmp93 + s15*s25*s35*tmp100*tmp&
          &118*tmp94 + s15*s25*tmp100*tmp113*tmp3*tmp94 + s35*tmp100*tmp107*tmp131*tmp3*tmp&
          &94 + s35*tmp100*tmp106*tmp21*tmp3*tmp94) + 32*logu*tmp10*tmp28*tmp35*tmp38*tmp6*&
          &tmp7*tmp8*tmp9*(tmp29 + tmp31/tmp10 + tmp32 + tmp71 - 2*m2*(tmp33 + tmp39 - 4*tm&
          &p77) + (ss - 3/tmp10)*tmp95) - 32*discbslogu*tmp10*tmp28*tmp6*tmp63*tmp7*tmp72*t&
          &mp8*tmp9*(-8*m2*(4*ss + 1/tmp10)*tmp31 - 16*(tmp1 + 1/tmp10)*tmp36 + tmp29*tmp40&
          & + 192*tmp43 + 2*tmp64 + tmp67 + ss*(tmp1 + 5/tmp10)*tmp95) + 32*scalarc0ir6t*tm&
          &p28*tmp35*tmp6*tmp7*tmp73*tmp8*tmp9*(-16*tmp134*tmp36 + tmp80 + ((5*tmp31)/tmp10&
          & + 7*tmp71 + tmp81 + tmp82)/tmp10 + tmp11*(tmp137 + (10*tmp31)/tmp10 + 4*tmp71 +&
          & tmp85) + (tmp135 + tmp136 - 2*tmp33)*tmp95) - 32*logt*tmp10*tmp28*tmp35*tmp42*t&
          &mp6*tmp7*tmp8*tmp9*(64*tmp36 + (2*tmp31 + tmp33 + tmp70)/tmp10 + tmp124*(4*tmp31&
          & - 5*tmp33 - 2*tmp77) + tmp76*tmp95) + 32*discbulogt*tmp28*tmp35*tmp38*tmp6*tmp7&
          &*tmp73*tmp8*tmp9*((12*tmp29)/tmp10 + 9*ss*tmp32 + 15*tmp31*tmp33 + 2*tmp68 + tmp&
          &80 + tmp83 - 112*tmp36*tmp84 + tmp11*(10*tmp29 + (20*tmp31)/tmp10 + 14*tmp71 + t&
          &mp85) + (18*tmp31 + 4*tmp33 + 21*tmp77)*tmp95) + 32*scalarc0ir6u*tmp10*tmp28*tmp&
          &6*tmp63*tmp7*tmp8*tmp9*((7*tmp29)/tmp10 + 4*tmp31*tmp33 - 16*(37*ss + 13/tmp10)*&
          &tmp36 + 448*tmp43 + tmp67 - 2*tmp68 + tmp11*(14*tmp29 + (19*tmp31)/tmp10 - 5*tmp&
          &32 + 2*tmp71) + tmp83 + (35*tmp31 + tmp69 + 29*tmp77)*tmp95) + 32*discbtlogu*tmp&
          &10*tmp28*tmp42*tmp6*tmp63*tmp7*tmp8*tmp9*((-48*ss + 80/tmp10)*tmp36 + (tmp29 - 2&
          &*tmp32 + tmp71)/tmp10 + tmp74 + (tmp31 - 10*tmp33 + tmp86)*tmp95 + m2*(-tmp31 + &
          &5*tmp33 + tmp70)*tmp97) + 32*discbslogt*tmp28*tmp35*tmp6*tmp7*tmp72*tmp73*tmp8*t&
          &mp9*(16*(-14*ss + 1/tmp10)*tmp36 + tmp80 + ss*(18*ss + 1/tmp10)*tmp95 + ss*tmp11&
          &*(10*tmp31 + tmp45 + ss*tmp97) + ss*(tmp32 + tmp79 + tmp81 + tmp31*tmp97))

  END FUNCTION SOFT

    FUNCTION NTSOFT()
  implicit none
  real NTSOFT
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206, tmp207, tmp208, tmp209, tmp210
  real tmp211, tmp212, tmp213, tmp214, tmp215
  real tmp216, tmp217, tmp218, tmp219, tmp220
  real tmp221, tmp222, tmp223, tmp224, tmp225
  real tmp226, tmp227, tmp228, tmp229, tmp230
  real tmp231, tmp232, tmp233, tmp234, tmp235
  real tmp236, tmp237, tmp238, tmp239, tmp240
  real tmp241, tmp242, tmp243, tmp244, tmp245
  real tmp246, tmp247, tmp248, tmp249, tmp250
  real tmp251, tmp252, tmp253, tmp254, tmp255
  real tmp256, tmp257, tmp258, tmp259, tmp260
  real tmp261, tmp262, tmp263, tmp264, tmp265
  real tmp266, tmp267, tmp268, tmp269, tmp270
  real tmp271, tmp272, tmp273, tmp274, tmp275
  real tmp276, tmp277, tmp278, tmp279, tmp280
  real tmp281, tmp282, tmp283, tmp284, tmp285
  real tmp286, tmp287, tmp288, tmp289, tmp290
  real tmp291, tmp292, tmp293, tmp294, tmp295
  real tmp296, tmp297, tmp298, tmp299, tmp300
  real tmp301, tmp302, tmp303, tmp304, tmp305
  real tmp306, tmp307, tmp308, tmp309, tmp310
  real tmp311, tmp312, tmp313, tmp314, tmp315
  real tmp316, tmp317, tmp318, tmp319, tmp320
  real tmp321, tmp322, tmp323, tmp324, tmp325
  real tmp326, tmp327, tmp328, tmp329, tmp330
  real tmp331, tmp332, tmp333, tmp334, tmp335
  real tmp336, tmp337, tmp338, tmp339, tmp340
  real tmp341, tmp342, tmp343, tmp344, tmp345
  real tmp346, tmp347, tmp348, tmp349, tmp350
  real tmp351, tmp352, tmp353, tmp354, tmp355
  real tmp356, tmp357, tmp358, tmp359, tmp360
  real tmp361, tmp362, tmp363, tmp364, tmp365
  real tmp366, tmp367, tmp368, tmp369, tmp370
  real tmp371, tmp372, tmp373, tmp374, tmp375
  real tmp376, tmp377, tmp378, tmp379, tmp380
  real tmp381, tmp382, tmp383, tmp384, tmp385
  real tmp386, tmp387, tmp388, tmp389, tmp390
  real tmp391, tmp392, tmp393, tmp394, tmp395
  real tmp396, tmp397, tmp398, tmp399, tmp400
  real tmp401, tmp402, tmp403, tmp404, tmp405
  real tmp406, tmp407, tmp408, tmp409, tmp410
  real tmp411, tmp412, tmp413, tmp414, tmp415
  real tmp416, tmp417, tmp418, tmp419, tmp420
  real tmp421, tmp422, tmp423, tmp424, tmp425
  real tmp426, tmp427, tmp428, tmp429, tmp430
  real tmp431, tmp432, tmp433, tmp434, tmp435
  real tmp436, tmp437, tmp438, tmp439, tmp440
  real tmp441, tmp442, tmp443, tmp444, tmp445
  real tmp446, tmp447, tmp448, tmp449, tmp450
  real tmp451, tmp452, tmp453, tmp454, tmp455
  real tmp456, tmp457, tmp458, tmp459, tmp460
  real tmp461, tmp462, tmp463, tmp464, tmp465
  real tmp466, tmp467, tmp468, tmp469, tmp470
  real tmp471, tmp472, tmp473, tmp474, tmp475
  real tmp476, tmp477, tmp478, tmp479, tmp480
  real tmp481, tmp482, tmp483, tmp484, tmp485
  real tmp486, tmp487, tmp488, tmp489, tmp490
  real tmp491, tmp492, tmp493, tmp494, tmp495
  real tmp496, tmp497, tmp498, tmp499, tmp500
  real tmp501, tmp502, tmp503, tmp504, tmp505
  real tmp506, tmp507, tmp508, tmp509, tmp510
  real tmp511, tmp512, tmp513, tmp514, tmp515
  real tmp516, tmp517, tmp518, tmp519, tmp520
  real tmp521, tmp522, tmp523, tmp524, tmp525
  real tmp526, tmp527, tmp528, tmp529, tmp530
  real tmp531, tmp532, tmp533, tmp534, tmp535
  real tmp536, tmp537, tmp538, tmp539, tmp540
  real tmp541, tmp542, tmp543, tmp544, tmp545
  real tmp546, tmp547, tmp548, tmp549, tmp550
  real tmp551, tmp552, tmp553, tmp554, tmp555
  real tmp556, tmp557, tmp558, tmp559, tmp560
  real tmp561, tmp562, tmp563, tmp564, tmp565
  real tmp566, tmp567, tmp568, tmp569, tmp570
  real tmp571, tmp572, tmp573, tmp574, tmp575
  real tmp576, tmp577, tmp578, tmp579, tmp580
  real tmp581, tmp582, tmp583, tmp584, tmp585
  real tmp586, tmp587, tmp588, tmp589, tmp590
  real tmp591, tmp592, tmp593, tmp594, tmp595
  real tmp596, tmp597, tmp598, tmp599, tmp600
  real tmp601, tmp602, tmp603, tmp604, tmp605
  real tmp606, tmp607, tmp608, tmp609, tmp610
  real tmp611, tmp612, tmp613, tmp614, tmp615
  real tmp616, tmp617, tmp618, tmp619, tmp620
  real tmp621, tmp622, tmp623, tmp624, tmp625
  real tmp626, tmp627, tmp628, tmp629, tmp630
  real tmp631, tmp632, tmp633, tmp634, tmp635
  real tmp636, tmp637, tmp638, tmp639, tmp640
  real tmp641, tmp642, tmp643, tmp644, tmp645
  real tmp646, tmp647, tmp648, tmp649, tmp650
  real tmp651, tmp652, tmp653, tmp654, tmp655
  real tmp656, tmp657, tmp658, tmp659, tmp660
  real tmp661, tmp662, tmp663, tmp664, tmp665
  real tmp666, tmp667, tmp668, tmp669, tmp670
  real tmp671, tmp672, tmp673, tmp674, tmp675
  real tmp676, tmp677, tmp678, tmp679, tmp680
  real tmp681, tmp682, tmp683, tmp684, tmp685
  real tmp686, tmp687, tmp688, tmp689, tmp690
  real tmp691, tmp692, tmp693, tmp694, tmp695
  real tmp696, tmp697, tmp698, tmp699, tmp700
  real tmp701, tmp702, tmp703, tmp704, tmp705
  real tmp706, tmp707, tmp708, tmp709, tmp710
  real tmp711, tmp712, tmp713, tmp714, tmp715
  real tmp716, tmp717, tmp718, tmp719, tmp720
  real tmp721, tmp722, tmp723, tmp724, tmp725
  real tmp726, tmp727, tmp728, tmp729, tmp730
  real tmp731, tmp732, tmp733, tmp734, tmp735
  real tmp736, tmp737, tmp738, tmp739, tmp740
  real tmp741, tmp742, tmp743, tmp744, tmp745
  real tmp746, tmp747, tmp748, tmp749, tmp750
  real tmp751, tmp752, tmp753, tmp754, tmp755
  real tmp756, tmp757, tmp758, tmp759, tmp760
  real tmp761, tmp762, tmp763, tmp764, tmp765
  real tmp766, tmp767, tmp768, tmp769, tmp770
  real tmp771, tmp772, tmp773, tmp774, tmp775
  real tmp776, tmp777, tmp778, tmp779, tmp780
  real tmp781, tmp782, tmp783, tmp784, tmp785
  real tmp786, tmp787, tmp788, tmp789, tmp790
  real tmp791, tmp792, tmp793, tmp794, tmp795
  real tmp796, tmp797, tmp798, tmp799, tmp800
  real tmp801, tmp802, tmp803, tmp804, tmp805
  real tmp806, tmp807, tmp808, tmp809, tmp810
  real tmp811, tmp812, tmp813, tmp814, tmp815
  real tmp816, tmp817, tmp818, tmp819, tmp820
  real tmp821, tmp822, tmp823, tmp824, tmp825
  real tmp826, tmp827, tmp828, tmp829, tmp830
  real tmp831, tmp832, tmp833, tmp834, tmp835
  real tmp836, tmp837, tmp838, tmp839, tmp840
  real tmp841, tmp842, tmp843, tmp844, tmp845
  real tmp846, tmp847, tmp848, tmp849, tmp850
  real tmp851, tmp852, tmp853, tmp854, tmp855
  real tmp856, tmp857, tmp858, tmp859, tmp860
  real tmp861, tmp862, tmp863, tmp864, tmp865
  real tmp866, tmp867, tmp868, tmp869, tmp870
  real tmp871, tmp872, tmp873, tmp874, tmp875
  real tmp876, tmp877, tmp878, tmp879, tmp880
  real tmp881, tmp882, tmp883, tmp884, tmp885
  real tmp886, tmp887, tmp888, tmp889, tmp890
  real tmp891, tmp892, tmp893, tmp894, tmp895
  real tmp896, tmp897, tmp898, tmp899, tmp900
  real tmp901, tmp902, tmp903, tmp904, tmp905
  real tmp906, tmp907, tmp908, tmp909, tmp910
  real tmp911, tmp912, tmp913, tmp914, tmp915
  real tmp916, tmp917, tmp918, tmp919, tmp920
  real tmp921, tmp922, tmp923, tmp924, tmp925
  real tmp926, tmp927, tmp928, tmp929, tmp930
  real tmp931, tmp932, tmp933, tmp934, tmp935
  real tmp936, tmp937, tmp938, tmp939, tmp940
  real tmp941, tmp942, tmp943, tmp944, tmp945
  real tmp946, tmp947, tmp948, tmp949, tmp950
  real tmp951, tmp952, tmp953, tmp954, tmp955
  real tmp956, tmp957, tmp958, tmp959, tmp960
  real tmp961, tmp962, tmp963, tmp964, tmp965
  real tmp966, tmp967, tmp968, tmp969, tmp970
  real tmp971, tmp972, tmp973, tmp974, tmp975
  real tmp976, tmp977, tmp978, tmp979, tmp980
  real tmp981, tmp982, tmp983, tmp984, tmp985
  real tmp986, tmp987, tmp988, tmp989, tmp990
  real tmp991, tmp992, tmp993, tmp994, tmp995
  real tmp996, tmp997, tmp998, tmp999, tmp1000
  real tmp1001, tmp1002, tmp1003, tmp1004, tmp1005
  real tmp1006, tmp1007, tmp1008, tmp1009, tmp1010
  real tmp1011, tmp1012, tmp1013, tmp1014, tmp1015
  real tmp1016, tmp1017, tmp1018, tmp1019, tmp1020
  real tmp1021, tmp1022, tmp1023, tmp1024, tmp1025
  real tmp1026, tmp1027, tmp1028, tmp1029, tmp1030
  real tmp1031, tmp1032, tmp1033, tmp1034, tmp1035
  real tmp1036, tmp1037, tmp1038, tmp1039, tmp1040
  real tmp1041, tmp1042, tmp1043, tmp1044, tmp1045
  real tmp1046, tmp1047, tmp1048, tmp1049, tmp1050
  real tmp1051, tmp1052, tmp1053, tmp1054, tmp1055
  real tmp1056, tmp1057, tmp1058, tmp1059, tmp1060
  real tmp1061, tmp1062, tmp1063, tmp1064, tmp1065
  real tmp1066, tmp1067, tmp1068, tmp1069, tmp1070
  real tmp1071, tmp1072, tmp1073, tmp1074, tmp1075
  real tmp1076, tmp1077, tmp1078, tmp1079, tmp1080
  real tmp1081, tmp1082, tmp1083, tmp1084, tmp1085
  real tmp1086, tmp1087, tmp1088, tmp1089, tmp1090
  real tmp1091, tmp1092, tmp1093, tmp1094, tmp1095
  real tmp1096, tmp1097, tmp1098, tmp1099, tmp1100
  real tmp1101, tmp1102, tmp1103, tmp1104, tmp1105
  real tmp1106, tmp1107, tmp1108, tmp1109, tmp1110
  real tmp1111, tmp1112, tmp1113, tmp1114, tmp1115
  real tmp1116, tmp1117, tmp1118, tmp1119, tmp1120
  real tmp1121, tmp1122, tmp1123, tmp1124, tmp1125
  real tmp1126, tmp1127, tmp1128, tmp1129, tmp1130
  real tmp1131, tmp1132, tmp1133, tmp1134, tmp1135
  real tmp1136, tmp1137, tmp1138, tmp1139, tmp1140
  real tmp1141, tmp1142, tmp1143, tmp1144, tmp1145
  real tmp1146, tmp1147, tmp1148, tmp1149, tmp1150
  real tmp1151, tmp1152, tmp1153, tmp1154, tmp1155
  real tmp1156, tmp1157, tmp1158, tmp1159, tmp1160
  real tmp1161, tmp1162, tmp1163, tmp1164, tmp1165
  real tmp1166, tmp1167, tmp1168, tmp1169, tmp1170
  real tmp1171, tmp1172, tmp1173, tmp1174, tmp1175
  real tmp1176, tmp1177, tmp1178, tmp1179, tmp1180
  real tmp1181, tmp1182, tmp1183, tmp1184, tmp1185
  real tmp1186, tmp1187, tmp1188, tmp1189, tmp1190
  real tmp1191, tmp1192, tmp1193, tmp1194, tmp1195
  real tmp1196, tmp1197, tmp1198, tmp1199, tmp1200
  real tmp1201, tmp1202, tmp1203, tmp1204, tmp1205
  real tmp1206, tmp1207, tmp1208, tmp1209, tmp1210
  real tmp1211, tmp1212, tmp1213, tmp1214, tmp1215
  real tmp1216, tmp1217, tmp1218, tmp1219, tmp1220
  real tmp1221, tmp1222, tmp1223, tmp1224, tmp1225
  real tmp1226, tmp1227, tmp1228, tmp1229, tmp1230
  real tmp1231, tmp1232, tmp1233, tmp1234, tmp1235
  real tmp1236, tmp1237, tmp1238, tmp1239, tmp1240
  real tmp1241, tmp1242, tmp1243, tmp1244, tmp1245
  real tmp1246, tmp1247, tmp1248, tmp1249, tmp1250
  real tmp1251, tmp1252, tmp1253, tmp1254, tmp1255
  real tmp1256, tmp1257, tmp1258, tmp1259, tmp1260
  real tmp1261, tmp1262, tmp1263, tmp1264, tmp1265
  real tmp1266, tmp1267, tmp1268, tmp1269, tmp1270
  real tmp1271, tmp1272, tmp1273, tmp1274, tmp1275
  real tmp1276, tmp1277, tmp1278, tmp1279, tmp1280
  real tmp1281, tmp1282, tmp1283, tmp1284, tmp1285
  real tmp1286, tmp1287, tmp1288, tmp1289, tmp1290
  real tmp1291, tmp1292, tmp1293, tmp1294, tmp1295
  real tmp1296, tmp1297, tmp1298, tmp1299, tmp1300
  real tmp1301, tmp1302, tmp1303, tmp1304, tmp1305
  real tmp1306, tmp1307, tmp1308, tmp1309, tmp1310
  real tmp1311, tmp1312, tmp1313, tmp1314, tmp1315
  real tmp1316, tmp1317, tmp1318, tmp1319, tmp1320
  real tmp1321, tmp1322, tmp1323, tmp1324, tmp1325
  real tmp1326, tmp1327, tmp1328, tmp1329, tmp1330
  real tmp1331, tmp1332, tmp1333, tmp1334, tmp1335
  real tmp1336, tmp1337, tmp1338, tmp1339, tmp1340
  real tmp1341, tmp1342, tmp1343, tmp1344, tmp1345
  real tmp1346, tmp1347, tmp1348, tmp1349, tmp1350
  real tmp1351, tmp1352, tmp1353, tmp1354, tmp1355
  real tmp1356, tmp1357, tmp1358, tmp1359, tmp1360
  real tmp1361, tmp1362, tmp1363, tmp1364, tmp1365
  real tmp1366, tmp1367, tmp1368, tmp1369, tmp1370
  real tmp1371, tmp1372, tmp1373, tmp1374, tmp1375
  real tmp1376, tmp1377, tmp1378, tmp1379, tmp1380
  real tmp1381, tmp1382, tmp1383, tmp1384, tmp1385
  real tmp1386, tmp1387, tmp1388, tmp1389, tmp1390
  real tmp1391, tmp1392, tmp1393, tmp1394, tmp1395
  real tmp1396, tmp1397, tmp1398, tmp1399, tmp1400
  real tmp1401, tmp1402, tmp1403, tmp1404, tmp1405
  real tmp1406, tmp1407, tmp1408, tmp1409, tmp1410
  real tmp1411, tmp1412, tmp1413, tmp1414, tmp1415
  real tmp1416, tmp1417, tmp1418, tmp1419, tmp1420
  real tmp1421, tmp1422, tmp1423, tmp1424, tmp1425
  real tmp1426, tmp1427, tmp1428, tmp1429, tmp1430
  real tmp1431, tmp1432, tmp1433, tmp1434, tmp1435
  real tmp1436, tmp1437, tmp1438, tmp1439, tmp1440
  real tmp1441, tmp1442, tmp1443, tmp1444, tmp1445
  real tmp1446, tmp1447, tmp1448, tmp1449, tmp1450
  real tmp1451, tmp1452, tmp1453, tmp1454, tmp1455
  real tmp1456, tmp1457, tmp1458, tmp1459, tmp1460
  real tmp1461, tmp1462, tmp1463, tmp1464, tmp1465
  real tmp1466, tmp1467, tmp1468, tmp1469, tmp1470
  real tmp1471, tmp1472, tmp1473, tmp1474, tmp1475
  real tmp1476, tmp1477, tmp1478, tmp1479, tmp1480
  real tmp1481, tmp1482, tmp1483, tmp1484, tmp1485
  real tmp1486, tmp1487, tmp1488, tmp1489, tmp1490
  real tmp1491, tmp1492, tmp1493, tmp1494, tmp1495
  real tmp1496, tmp1497, tmp1498, tmp1499, tmp1500
  real tmp1501, tmp1502, tmp1503, tmp1504, tmp1505
  real tmp1506, tmp1507, tmp1508, tmp1509, tmp1510
  real tmp1511, tmp1512, tmp1513, tmp1514, tmp1515
  real tmp1516, tmp1517, tmp1518, tmp1519, tmp1520
  real tmp1521, tmp1522, tmp1523, tmp1524, tmp1525
  real tmp1526, tmp1527, tmp1528, tmp1529, tmp1530
  real tmp1531, tmp1532, tmp1533, tmp1534, tmp1535
  real tmp1536, tmp1537, tmp1538, tmp1539, tmp1540
  real tmp1541, tmp1542, tmp1543, tmp1544, tmp1545
  real tmp1546, tmp1547, tmp1548, tmp1549, tmp1550
  real tmp1551, tmp1552, tmp1553, tmp1554, tmp1555
  real tmp1556, tmp1557, tmp1558, tmp1559, tmp1560
  real tmp1561, tmp1562, tmp1563, tmp1564, tmp1565
  real tmp1566, tmp1567, tmp1568, tmp1569, tmp1570
  real tmp1571, tmp1572, tmp1573, tmp1574, tmp1575
  real tmp1576, tmp1577, tmp1578, tmp1579, tmp1580
  real tmp1581, tmp1582, tmp1583, tmp1584, tmp1585
  real tmp1586, tmp1587, tmp1588, tmp1589, tmp1590
  real tmp1591, tmp1592, tmp1593, tmp1594, tmp1595
  real tmp1596, tmp1597, tmp1598, tmp1599, tmp1600
  real tmp1601, tmp1602, tmp1603, tmp1604, tmp1605
  real tmp1606, tmp1607, tmp1608, tmp1609, tmp1610
  real tmp1611, tmp1612, tmp1613, tmp1614, tmp1615
  real tmp1616, tmp1617, tmp1618, tmp1619, tmp1620
  real tmp1621, tmp1622, tmp1623, tmp1624, tmp1625
  real tmp1626, tmp1627, tmp1628, tmp1629, tmp1630
  real tmp1631, tmp1632, tmp1633, tmp1634, tmp1635
  real tmp1636, tmp1637, tmp1638, tmp1639, tmp1640
  real tmp1641, tmp1642, tmp1643, tmp1644, tmp1645
  real tmp1646, tmp1647, tmp1648, tmp1649, tmp1650
  real tmp1651, tmp1652, tmp1653, tmp1654, tmp1655
  real tmp1656, tmp1657, tmp1658, tmp1659, tmp1660
  real tmp1661, tmp1662, tmp1663, tmp1664, tmp1665
  real tmp1666, tmp1667, tmp1668, tmp1669, tmp1670
  real tmp1671, tmp1672, tmp1673, tmp1674, tmp1675
  real tmp1676, tmp1677, tmp1678, tmp1679, tmp1680
  real tmp1681, tmp1682, tmp1683, tmp1684, tmp1685
  real tmp1686, tmp1687, tmp1688, tmp1689, tmp1690
  real tmp1691, tmp1692, tmp1693, tmp1694, tmp1695
  real tmp1696, tmp1697, tmp1698, tmp1699, tmp1700
  real tmp1701, tmp1702, tmp1703, tmp1704, tmp1705
  real tmp1706, tmp1707, tmp1708, tmp1709, tmp1710
  real tmp1711, tmp1712, tmp1713, tmp1714, tmp1715
  real tmp1716, tmp1717, tmp1718, tmp1719, tmp1720
  real tmp1721, tmp1722, tmp1723, tmp1724, tmp1725
  real tmp1726, tmp1727, tmp1728, tmp1729, tmp1730
  real tmp1731, tmp1732, tmp1733, tmp1734, tmp1735
  real tmp1736, tmp1737, tmp1738, tmp1739, tmp1740
  real tmp1741, tmp1742, tmp1743, tmp1744, tmp1745
  real tmp1746, tmp1747, tmp1748, tmp1749, tmp1750
  real tmp1751, tmp1752, tmp1753, tmp1754, tmp1755
  real tmp1756, tmp1757, tmp1758, tmp1759, tmp1760
  real tmp1761, tmp1762, tmp1763, tmp1764, tmp1765
  real tmp1766, tmp1767, tmp1768, tmp1769, tmp1770
  real tmp1771, tmp1772, tmp1773, tmp1774, tmp1775
  real tmp1776, tmp1777, tmp1778, tmp1779, tmp1780
  real tmp1781, tmp1782, tmp1783, tmp1784, tmp1785
  real tmp1786, tmp1787, tmp1788, tmp1789, tmp1790
  real tmp1791, tmp1792, tmp1793, tmp1794, tmp1795
  real tmp1796, tmp1797, tmp1798, tmp1799, tmp1800
  real tmp1801, tmp1802, tmp1803, tmp1804, tmp1805
  real tmp1806, tmp1807, tmp1808, tmp1809, tmp1810
  real tmp1811, tmp1812, tmp1813, tmp1814, tmp1815
  real tmp1816, tmp1817, tmp1818, tmp1819, tmp1820
  real tmp1821, tmp1822, tmp1823, tmp1824, tmp1825
  real tmp1826, tmp1827, tmp1828, tmp1829, tmp1830
  real tmp1831, tmp1832, tmp1833, tmp1834, tmp1835
  real tmp1836, tmp1837, tmp1838, tmp1839, tmp1840
  real tmp1841, tmp1842, tmp1843, tmp1844, tmp1845
  real tmp1846, tmp1847, tmp1848, tmp1849, tmp1850
  real tmp1851, tmp1852, tmp1853, tmp1854, tmp1855
  real tmp1856, tmp1857, tmp1858, tmp1859, tmp1860
  real tmp1861, tmp1862, tmp1863, tmp1864, tmp1865
  real tmp1866, tmp1867, tmp1868, tmp1869, tmp1870
  real tmp1871, tmp1872, tmp1873, tmp1874, tmp1875
  real tmp1876, tmp1877, tmp1878, tmp1879, tmp1880
  real tmp1881, tmp1882, tmp1883, tmp1884, tmp1885
  real tmp1886, tmp1887, tmp1888, tmp1889, tmp1890
  real tmp1891, tmp1892, tmp1893, tmp1894, tmp1895
  real tmp1896, tmp1897, tmp1898, tmp1899, tmp1900
  real tmp1901, tmp1902, tmp1903, tmp1904, tmp1905
  real tmp1906, tmp1907, tmp1908, tmp1909, tmp1910
  real tmp1911, tmp1912, tmp1913, tmp1914, tmp1915
  real tmp1916, tmp1917, tmp1918, tmp1919, tmp1920
  real tmp1921, tmp1922, tmp1923, tmp1924, tmp1925
  real tmp1926, tmp1927, tmp1928, tmp1929, tmp1930
  real tmp1931, tmp1932, tmp1933, tmp1934, tmp1935
  real tmp1936, tmp1937, tmp1938, tmp1939, tmp1940
  real tmp1941, tmp1942, tmp1943, tmp1944, tmp1945
  real tmp1946, tmp1947, tmp1948, tmp1949, tmp1950
  real tmp1951, tmp1952, tmp1953, tmp1954, tmp1955
  real tmp1956, tmp1957, tmp1958, tmp1959, tmp1960
  real tmp1961, tmp1962, tmp1963, tmp1964, tmp1965
  real tmp1966, tmp1967, tmp1968, tmp1969, tmp1970
  real tmp1971, tmp1972, tmp1973, tmp1974, tmp1975
  real tmp1976, tmp1977, tmp1978, tmp1979, tmp1980
  real tmp1981, tmp1982, tmp1983, tmp1984, tmp1985
  real tmp1986, tmp1987, tmp1988, tmp1989, tmp1990
  real tmp1991, tmp1992, tmp1993, tmp1994, tmp1995
  real tmp1996, tmp1997, tmp1998, tmp1999, tmp2000
  real tmp2001, tmp2002, tmp2003, tmp2004, tmp2005
  real tmp2006, tmp2007, tmp2008, tmp2009, tmp2010
  real tmp2011, tmp2012, tmp2013, tmp2014, tmp2015
  real tmp2016, tmp2017, tmp2018, tmp2019, tmp2020
  real tmp2021, tmp2022, tmp2023, tmp2024, tmp2025
  real tmp2026, tmp2027, tmp2028, tmp2029, tmp2030
  real tmp2031, tmp2032, tmp2033, tmp2034, tmp2035
  real tmp2036, tmp2037, tmp2038, tmp2039, tmp2040
  real tmp2041, tmp2042, tmp2043, tmp2044, tmp2045
  real tmp2046, tmp2047, tmp2048, tmp2049, tmp2050
  real tmp2051, tmp2052, tmp2053, tmp2054, tmp2055
  real tmp2056, tmp2057, tmp2058, tmp2059, tmp2060
  real tmp2061, tmp2062, tmp2063, tmp2064, tmp2065
  real tmp2066, tmp2067, tmp2068, tmp2069, tmp2070
  real tmp2071, tmp2072, tmp2073, tmp2074, tmp2075
  real tmp2076, tmp2077, tmp2078, tmp2079, tmp2080
  real tmp2081, tmp2082, tmp2083, tmp2084, tmp2085
  real tmp2086, tmp2087, tmp2088, tmp2089, tmp2090
  real tmp2091, tmp2092, tmp2093, tmp2094, tmp2095
  real tmp2096, tmp2097, tmp2098, tmp2099, tmp2100
  real tmp2101, tmp2102, tmp2103, tmp2104, tmp2105
  real tmp2106, tmp2107, tmp2108, tmp2109, tmp2110
  real tmp2111, tmp2112, tmp2113, tmp2114, tmp2115
  real tmp2116, tmp2117, tmp2118, tmp2119, tmp2120
  real tmp2121, tmp2122, tmp2123, tmp2124, tmp2125
  real tmp2126, tmp2127, tmp2128, tmp2129, tmp2130
  real tmp2131, tmp2132, tmp2133, tmp2134, tmp2135
  real tmp2136, tmp2137, tmp2138, tmp2139, tmp2140
  real tmp2141, tmp2142, tmp2143, tmp2144, tmp2145
  real tmp2146, tmp2147, tmp2148, tmp2149, tmp2150
  real tmp2151, tmp2152, tmp2153, tmp2154, tmp2155
  real tmp2156, tmp2157, tmp2158, tmp2159, tmp2160
  real tmp2161, tmp2162, tmp2163, tmp2164, tmp2165
  real tmp2166, tmp2167, tmp2168, tmp2169, tmp2170
  real tmp2171, tmp2172, tmp2173, tmp2174, tmp2175
  real tmp2176, tmp2177, tmp2178, tmp2179, tmp2180
  real tmp2181, tmp2182, tmp2183, tmp2184, tmp2185
  real tmp2186, tmp2187, tmp2188, tmp2189, tmp2190
  real tmp2191, tmp2192, tmp2193, tmp2194, tmp2195
  real tmp2196, tmp2197, tmp2198, tmp2199, tmp2200
  real tmp2201, tmp2202, tmp2203, tmp2204, tmp2205
  real tmp2206, tmp2207, tmp2208, tmp2209, tmp2210
  real tmp2211, tmp2212, tmp2213, tmp2214, tmp2215
  real tmp2216, tmp2217, tmp2218, tmp2219, tmp2220
  real tmp2221, tmp2222, tmp2223, tmp2224, tmp2225
  real tmp2226, tmp2227, tmp2228, tmp2229, tmp2230
  real tmp2231, tmp2232, tmp2233, tmp2234, tmp2235
  real tmp2236, tmp2237, tmp2238, tmp2239, tmp2240
  real tmp2241, tmp2242, tmp2243, tmp2244, tmp2245
  real tmp2246, tmp2247, tmp2248, tmp2249, tmp2250
  real tmp2251, tmp2252, tmp2253, tmp2254, tmp2255
  real tmp2256, tmp2257, tmp2258, tmp2259, tmp2260
  real tmp2261, tmp2262, tmp2263, tmp2264, tmp2265
  real tmp2266, tmp2267, tmp2268, tmp2269, tmp2270
  real tmp2271, tmp2272, tmp2273, tmp2274, tmp2275
  real tmp2276, tmp2277, tmp2278, tmp2279, tmp2280
  real tmp2281, tmp2282, tmp2283, tmp2284, tmp2285
  real tmp2286, tmp2287, tmp2288, tmp2289, tmp2290
  real tmp2291, tmp2292, tmp2293, tmp2294, tmp2295
  real tmp2296, tmp2297, tmp2298, tmp2299, tmp2300
  real tmp2301, tmp2302, tmp2303, tmp2304, tmp2305
  real tmp2306, tmp2307, tmp2308, tmp2309, tmp2310
  real tmp2311, tmp2312, tmp2313, tmp2314, tmp2315
  real tmp2316, tmp2317, tmp2318, tmp2319, tmp2320
  real tmp2321, tmp2322, tmp2323, tmp2324, tmp2325
  real tmp2326, tmp2327, tmp2328, tmp2329, tmp2330
  real tmp2331, tmp2332, tmp2333, tmp2334, tmp2335
  real tmp2336, tmp2337, tmp2338, tmp2339, tmp2340
  real tmp2341, tmp2342, tmp2343, tmp2344, tmp2345
  real tmp2346, tmp2347, tmp2348, tmp2349, tmp2350
  real tmp2351, tmp2352, tmp2353, tmp2354, tmp2355
  real tmp2356, tmp2357, tmp2358, tmp2359, tmp2360
  real tmp2361, tmp2362, tmp2363, tmp2364, tmp2365
  real tmp2366, tmp2367, tmp2368, tmp2369, tmp2370
  real tmp2371, tmp2372, tmp2373, tmp2374, tmp2375
  real tmp2376, tmp2377, tmp2378, tmp2379, tmp2380
  real tmp2381, tmp2382, tmp2383, tmp2384, tmp2385
  real tmp2386, tmp2387, tmp2388, tmp2389, tmp2390
  real tmp2391, tmp2392, tmp2393, tmp2394, tmp2395
  real tmp2396, tmp2397, tmp2398, tmp2399, tmp2400
  real tmp2401, tmp2402, tmp2403, tmp2404, tmp2405
  real tmp2406, tmp2407, tmp2408, tmp2409, tmp2410
  real tmp2411, tmp2412, tmp2413, tmp2414, tmp2415
  real tmp2416, tmp2417, tmp2418, tmp2419, tmp2420
  real tmp2421, tmp2422, tmp2423, tmp2424, tmp2425
  real tmp2426, tmp2427, tmp2428, tmp2429, tmp2430
  real tmp2431, tmp2432, tmp2433, tmp2434, tmp2435
  real tmp2436, tmp2437, tmp2438, tmp2439, tmp2440
  real tmp2441, tmp2442, tmp2443, tmp2444, tmp2445
  real tmp2446, tmp2447, tmp2448, tmp2449, tmp2450
  real tmp2451, tmp2452, tmp2453, tmp2454, tmp2455
  real tmp2456, tmp2457, tmp2458, tmp2459, tmp2460
  real tmp2461, tmp2462, tmp2463, tmp2464, tmp2465
  real tmp2466, tmp2467, tmp2468, tmp2469, tmp2470
  real tmp2471, tmp2472, tmp2473, tmp2474, tmp2475
  real tmp2476, tmp2477, tmp2478, tmp2479, tmp2480
  real tmp2481, tmp2482, tmp2483, tmp2484, tmp2485
  real tmp2486, tmp2487, tmp2488, tmp2489, tmp2490
  real tmp2491, tmp2492, tmp2493, tmp2494, tmp2495
  real tmp2496, tmp2497, tmp2498, tmp2499, tmp2500
  real tmp2501, tmp2502, tmp2503, tmp2504, tmp2505
  real tmp2506, tmp2507, tmp2508, tmp2509, tmp2510
  real tmp2511, tmp2512, tmp2513, tmp2514, tmp2515
  real tmp2516, tmp2517, tmp2518, tmp2519, tmp2520
  real tmp2521, tmp2522, tmp2523, tmp2524, tmp2525
  real tmp2526, tmp2527, tmp2528, tmp2529, tmp2530
  real tmp2531, tmp2532, tmp2533, tmp2534, tmp2535
  real tmp2536, tmp2537, tmp2538, tmp2539, tmp2540
  real tmp2541, tmp2542, tmp2543, tmp2544, tmp2545
  real tmp2546, tmp2547, tmp2548, tmp2549, tmp2550
  real tmp2551, tmp2552, tmp2553, tmp2554, tmp2555
  real tmp2556, tmp2557, tmp2558, tmp2559, tmp2560
  real tmp2561, tmp2562, tmp2563, tmp2564, tmp2565
  real tmp2566, tmp2567, tmp2568, tmp2569, tmp2570
  real tmp2571, tmp2572, tmp2573, tmp2574, tmp2575
  real tmp2576, tmp2577, tmp2578, tmp2579, tmp2580
  real tmp2581, tmp2582, tmp2583, tmp2584, tmp2585
  real tmp2586, tmp2587, tmp2588, tmp2589, tmp2590
  real tmp2591, tmp2592, tmp2593, tmp2594, tmp2595
  real tmp2596, tmp2597, tmp2598, tmp2599, tmp2600
  real tmp2601, tmp2602, tmp2603, tmp2604, tmp2605
  real tmp2606, tmp2607, tmp2608, tmp2609, tmp2610
  real tmp2611, tmp2612, tmp2613, tmp2614, tmp2615
  real tmp2616, tmp2617, tmp2618, tmp2619, tmp2620
  real tmp2621, tmp2622, tmp2623, tmp2624, tmp2625
  real tmp2626, tmp2627, tmp2628, tmp2629, tmp2630
  real tmp2631, tmp2632, tmp2633, tmp2634, tmp2635
  real tmp2636, tmp2637, tmp2638, tmp2639, tmp2640
  real tmp2641, tmp2642, tmp2643, tmp2644, tmp2645
  real tmp2646, tmp2647, tmp2648, tmp2649, tmp2650
  real tmp2651, tmp2652, tmp2653, tmp2654, tmp2655
  real tmp2656, tmp2657, tmp2658, tmp2659, tmp2660
  real tmp2661, tmp2662, tmp2663, tmp2664, tmp2665
  real tmp2666, tmp2667, tmp2668, tmp2669, tmp2670
  real tmp2671, tmp2672, tmp2673, tmp2674, tmp2675
  real tmp2676, tmp2677, tmp2678, tmp2679, tmp2680
  real tmp2681, tmp2682, tmp2683, tmp2684, tmp2685
  real tmp2686, tmp2687, tmp2688, tmp2689, tmp2690
  real tmp2691, tmp2692, tmp2693, tmp2694, tmp2695
  real tmp2696, tmp2697, tmp2698, tmp2699, tmp2700
  real tmp2701, tmp2702, tmp2703, tmp2704, tmp2705
  real tmp2706, tmp2707, tmp2708, tmp2709, tmp2710
  real tmp2711, tmp2712, tmp2713, tmp2714, tmp2715
  real tmp2716, tmp2717, tmp2718, tmp2719, tmp2720
  real tmp2721, tmp2722, tmp2723, tmp2724, tmp2725
  real tmp2726, tmp2727, tmp2728, tmp2729, tmp2730
  real tmp2731, tmp2732, tmp2733, tmp2734, tmp2735
  real tmp2736, tmp2737, tmp2738, tmp2739, tmp2740
  real tmp2741, tmp2742, tmp2743, tmp2744, tmp2745
  real tmp2746, tmp2747, tmp2748, tmp2749, tmp2750
  real tmp2751, tmp2752, tmp2753, tmp2754, tmp2755
  real tmp2756, tmp2757, tmp2758, tmp2759, tmp2760
  real tmp2761, tmp2762, tmp2763, tmp2764, tmp2765
  real tmp2766, tmp2767, tmp2768, tmp2769, tmp2770
  real tmp2771, tmp2772, tmp2773, tmp2774, tmp2775
  real tmp2776, tmp2777, tmp2778, tmp2779, tmp2780
  real tmp2781, tmp2782, tmp2783, tmp2784, tmp2785
  real tmp2786, tmp2787, tmp2788, tmp2789, tmp2790
  real tmp2791, tmp2792, tmp2793, tmp2794, tmp2795
  real tmp2796, tmp2797, tmp2798, tmp2799, tmp2800
  real tmp2801, tmp2802, tmp2803, tmp2804, tmp2805
  real tmp2806, tmp2807, tmp2808, tmp2809, tmp2810
  real tmp2811, tmp2812, tmp2813, tmp2814, tmp2815
  real tmp2816, tmp2817, tmp2818, tmp2819, tmp2820
  real tmp2821, tmp2822, tmp2823, tmp2824, tmp2825
  real tmp2826, tmp2827, tmp2828, tmp2829, tmp2830
  real tmp2831, tmp2832, tmp2833, tmp2834, tmp2835
  real tmp2836, tmp2837, tmp2838, tmp2839, tmp2840
  real tmp2841, tmp2842, tmp2843, tmp2844, tmp2845
  real tmp2846, tmp2847, tmp2848, tmp2849, tmp2850
  real tmp2851, tmp2852, tmp2853, tmp2854, tmp2855
  real tmp2856, tmp2857, tmp2858, tmp2859, tmp2860
  real tmp2861, tmp2862, tmp2863, tmp2864, tmp2865
  real tmp2866, tmp2867, tmp2868, tmp2869, tmp2870
  real tmp2871, tmp2872, tmp2873, tmp2874, tmp2875
  real tmp2876, tmp2877, tmp2878, tmp2879, tmp2880
  real tmp2881, tmp2882, tmp2883, tmp2884, tmp2885
  real tmp2886, tmp2887, tmp2888, tmp2889, tmp2890
  real tmp2891, tmp2892, tmp2893, tmp2894, tmp2895
  real tmp2896, tmp2897, tmp2898, tmp2899, tmp2900
  real tmp2901, tmp2902, tmp2903, tmp2904, tmp2905
  real tmp2906, tmp2907, tmp2908, tmp2909, tmp2910
  real tmp2911, tmp2912, tmp2913, tmp2914, tmp2915
  real tmp2916, tmp2917, tmp2918, tmp2919, tmp2920
  real tmp2921, tmp2922, tmp2923, tmp2924, tmp2925
  real tmp2926, tmp2927, tmp2928, tmp2929, tmp2930
  real tmp2931, tmp2932, tmp2933, tmp2934, tmp2935
  real tmp2936, tmp2937, tmp2938, tmp2939, tmp2940
  real tmp2941, tmp2942, tmp2943, tmp2944, tmp2945
  real tmp2946, tmp2947, tmp2948, tmp2949, tmp2950
  real tmp2951, tmp2952, tmp2953, tmp2954, tmp2955
  real tmp2956, tmp2957, tmp2958, tmp2959, tmp2960
  real tmp2961, tmp2962, tmp2963, tmp2964, tmp2965
  real tmp2966, tmp2967, tmp2968, tmp2969, tmp2970
  real tmp2971, tmp2972, tmp2973, tmp2974, tmp2975
  real tmp2976, tmp2977, tmp2978, tmp2979, tmp2980
  real tmp2981, tmp2982, tmp2983, tmp2984, tmp2985
  real tmp2986, tmp2987, tmp2988, tmp2989, tmp2990
  real tmp2991, tmp2992, tmp2993, tmp2994, tmp2995
  real tmp2996, tmp2997, tmp2998, tmp2999, tmp3000
  real tmp3001, tmp3002, tmp3003, tmp3004, tmp3005
  real tmp3006, tmp3007, tmp3008, tmp3009, tmp3010
  real tmp3011, tmp3012, tmp3013, tmp3014, tmp3015
  real tmp3016, tmp3017, tmp3018, tmp3019, tmp3020
  real tmp3021, tmp3022, tmp3023, tmp3024, tmp3025
  real tmp3026, tmp3027, tmp3028, tmp3029, tmp3030
  real tmp3031, tmp3032, tmp3033, tmp3034, tmp3035
  real tmp3036, tmp3037, tmp3038, tmp3039, tmp3040
  real tmp3041, tmp3042, tmp3043, tmp3044, tmp3045
  real tmp3046, tmp3047, tmp3048, tmp3049, tmp3050
  real tmp3051, tmp3052, tmp3053, tmp3054, tmp3055
  real tmp3056, tmp3057, tmp3058, tmp3059, tmp3060
  real tmp3061, tmp3062, tmp3063, tmp3064, tmp3065
  real tmp3066, tmp3067, tmp3068, tmp3069, tmp3070
  real tmp3071, tmp3072, tmp3073, tmp3074, tmp3075
  real tmp3076, tmp3077, tmp3078, tmp3079, tmp3080
  real tmp3081, tmp3082, tmp3083, tmp3084, tmp3085
  real tmp3086, tmp3087, tmp3088, tmp3089, tmp3090
  real tmp3091, tmp3092, tmp3093, tmp3094, tmp3095
  real tmp3096, tmp3097, tmp3098, tmp3099, tmp3100
  real tmp3101, tmp3102, tmp3103, tmp3104, tmp3105
  real tmp3106, tmp3107, tmp3108, tmp3109, tmp3110
  real tmp3111, tmp3112, tmp3113, tmp3114, tmp3115
  real tmp3116, tmp3117, tmp3118, tmp3119, tmp3120
  real tmp3121, tmp3122, tmp3123, tmp3124, tmp3125
  real tmp3126, tmp3127, tmp3128, tmp3129, tmp3130
  real tmp3131, tmp3132, tmp3133, tmp3134, tmp3135
  real tmp3136, tmp3137, tmp3138, tmp3139, tmp3140
  real tmp3141, tmp3142, tmp3143, tmp3144, tmp3145
  real tmp3146, tmp3147, tmp3148, tmp3149, tmp3150
  real tmp3151, tmp3152, tmp3153, tmp3154, tmp3155
  real tmp3156, tmp3157, tmp3158, tmp3159, tmp3160
  real tmp3161, tmp3162, tmp3163, tmp3164, tmp3165
  real tmp3166, tmp3167, tmp3168, tmp3169, tmp3170
  real tmp3171, tmp3172, tmp3173, tmp3174, tmp3175
  real tmp3176, tmp3177, tmp3178, tmp3179, tmp3180
  real tmp3181, tmp3182, tmp3183, tmp3184, tmp3185
  real tmp3186, tmp3187, tmp3188, tmp3189, tmp3190
  real tmp3191, tmp3192, tmp3193, tmp3194, tmp3195
  real tmp3196, tmp3197, tmp3198, tmp3199, tmp3200
  real tmp3201, tmp3202, tmp3203, tmp3204, tmp3205
  real tmp3206, tmp3207, tmp3208, tmp3209, tmp3210
  real tmp3211, tmp3212, tmp3213, tmp3214, tmp3215
  real tmp3216, tmp3217, tmp3218, tmp3219, tmp3220
  real tmp3221, tmp3222, tmp3223, tmp3224, tmp3225
  real tmp3226
  
  tmp1 = -4*m2
  tmp2 = -s35
  tmp3 = s15 + tmp2
  tmp4 = m2**2
  tmp5 = ss**2
  tmp6 = tt**2
  tmp7 = s15 + s25
  tmp8 = tmp2 + tmp7
  tmp9 = 1/tmp8
  tmp10 = 1/s35
  tmp11 = 1/ss
  tmp12 = tmp1 + 1/tmp11
  tmp13 = tmp12**(-2)
  tmp14 = 1/tmp6
  tmp15 = tmp12 + tt
  tmp16 = tmp15**(-2)
  tmp17 = s25 + tmp2
  tmp18 = s25*tmp17
  tmp19 = s25 + 1/tmp10
  tmp20 = s15*tmp19
  tmp21 = tmp18 + tmp20
  tmp22 = 2*m2*tmp21*tmp3
  tmp23 = tmp3/tmp11
  tmp24 = -2/tmp10
  tmp25 = tmp24 + tmp7
  tmp26 = tmp25*tt
  tmp27 = tmp23 + tmp26
  tmp28 = -(s15*s25*tmp27)
  tmp29 = tmp22 + tmp28
  tmp30 = m2**4
  tmp31 = 64*tmp30
  tmp32 = m2**3
  tmp33 = -6/tmp11
  tmp34 = tmp33 + tt
  tmp35 = 16*tmp32*tmp34
  tmp36 = tt/tmp11
  tmp37 = tmp36 + tmp5 + tmp6
  tmp38 = tmp37**2
  tmp39 = 13*tmp5
  tmp40 = 3*tmp36
  tmp41 = 3*tmp6
  tmp42 = tmp39 + tmp40 + tmp41
  tmp43 = 4*tmp4*tmp42
  tmp44 = tmp11**(-3)
  tmp45 = 3*tmp44
  tmp46 = 3*tmp5*tt
  tmp47 = tmp41/tmp11
  tmp48 = tt**3
  tmp49 = 2*tmp48
  tmp50 = tmp45 + tmp46 + tmp47 + tmp49
  tmp51 = tmp1*tmp50
  tmp52 = tmp31 + tmp35 + tmp38 + tmp43 + tmp51
  tmp53 = s15**(-2)
  tmp54 = 1/s25
  tmp55 = 1/s15
  tmp56 = tmp54**2
  tmp57 = 1/tmp11 + tt
  tmp58 = tmp57**(-2)
  tmp59 = tmp15**(-3)
  tmp60 = tmp55**(-2)
  tmp61 = 1/tmp56
  tmp62 = tmp3**2
  tmp63 = -3/tmp10
  tmp64 = 1/tmp54 + tmp63
  tmp65 = 1/tmp48
  tmp66 = tmp1 + tt
  tmp67 = tmp66**(-2)
  tmp68 = 2*m2*tmp21*tmp62
  tmp69 = 4*m2
  tmp70 = tmp10**(-2)
  tmp71 = tmp24 + 1/tmp54
  tmp72 = 1/(tmp10*tmp54)
  tmp73 = 2*tmp70
  tmp74 = tmp55**(-3)
  tmp75 = 7/tmp10
  tmp76 = 2/tmp54
  tmp77 = -tmp70
  tmp78 = 5/tmp10
  tmp79 = 1/tmp54 + tmp78
  tmp80 = 2*tmp61
  tmp81 = -13*tmp70
  tmp82 = tmp10**(-3)
  tmp83 = tmp54**(-3)
  tmp84 = tmp63 + tmp76
  tmp85 = -(1/tmp54)
  tmp86 = 1/tmp10 + tmp85
  tmp87 = tmp72*tmp86
  tmp88 = -4*tmp72
  tmp89 = tmp11**(-4)
  tmp90 = 6*tmp82
  tmp91 = 6*tmp72
  tmp92 = -3*tmp70
  tmp93 = tt**4
  tmp94 = -(1/tmp11)
  tmp95 = tmp69 + tmp94
  tmp96 = 1/tmp95
  tmp97 = -tt
  tmp98 = tmp69 + tmp97
  tmp99 = 1/tmp98
  tmp100 = 1/tmp57
  tmp101 = tmp3*tmp5
  tmp102 = 2*tmp3*tmp36
  tmp103 = -2*tmp70
  tmp104 = tmp103 + tmp61 + tmp72
  tmp105 = -7*tmp72
  tmp106 = 9*tmp70
  tmp107 = 3*tmp74
  tmp108 = 5/tmp54
  tmp109 = 6*tmp70
  tmp110 = 1/tmp10 + tmp76
  tmp111 = 4/tmp10
  tmp112 = 8*tmp61
  tmp113 = tmp108/tmp10
  tmp114 = -6*tmp70
  tmp115 = tmp113 + tmp114 + tmp61
  tmp116 = tmp115*tmp72
  tmp117 = 7/tmp54
  tmp118 = tmp111 + tmp117
  tmp119 = tmp118*tmp74
  tmp120 = tmp76/tmp10
  tmp121 = tmp120 + tmp61 + tmp92
  tmp122 = tmp121*tmp72
  tmp123 = tmp110*tmp60
  tmp124 = tmp77 + tmp80 + tmp88
  tmp125 = tmp124/tmp55
  tmp126 = tmp123 + tmp125 + tmp87
  tmp127 = 1/tmp10 + tmp108
  tmp128 = 2/tmp10
  tmp129 = -22*tmp72
  tmp130 = (-15*tmp61)/tmp10
  tmp131 = (21*tmp70)/tmp54
  tmp132 = tmp117 + tmp128
  tmp133 = tmp132*tmp74
  tmp134 = -19*tmp72
  tmp135 = tmp85/tmp10
  tmp136 = (-17*tmp61)/tmp10
  tmp137 = tmp127*tmp60
  tmp138 = 5*tmp61
  tmp139 = -10*tmp72
  tmp140 = tmp138 + tmp139 + tmp77
  tmp141 = tmp140/tmp55
  tmp142 = tmp137 + tmp141 + tmp87
  tmp143 = tmp11**(-5)
  tmp144 = 3/tmp10
  tmp145 = 3*tmp82
  tmp146 = 25/tmp54
  tmp147 = tmp144 + tmp146
  tmp148 = tmp147*tmp74
  tmp149 = 11*tmp61
  tmp150 = -15*tmp72
  tmp151 = 2*tmp82
  tmp152 = 10/tmp54
  tmp153 = 1/tmp10 + tmp152
  tmp154 = tmp153*tmp74
  tmp155 = 3*tmp61
  tmp156 = tt**5
  tmp157 = Pi**4
  tmp158 = tmp94 + tmp98
  tmp159 = tmp23/tmp54
  tmp160 = 1/tmp55 + tmp85
  tmp161 = (tmp160*tt)/tmp10
  tmp162 = tmp159 + tmp161
  tmp163 = -(tmp162/tmp9)
  tmp164 = tmp163 + tmp22
  tmp165 = 48*tmp32
  tmp166 = -tmp44
  tmp167 = -44/tmp11
  tmp168 = 12*tt
  tmp169 = tmp167 + tmp168
  tmp170 = tmp169*tmp4
  tmp171 = 3*tmp5
  tmp172 = -2*tmp6
  tmp173 = tmp171 + tmp172
  tmp174 = tmp173*tmp69
  tmp175 = tmp165 + tmp166 + tmp170 + tmp174 + tmp48
  tmp176 = 1/(tmp11*tmp55)
  tmp177 = tmp7*tt
  tmp178 = tmp176 + tmp177
  tmp179 = 2/tmp55
  tmp180 = tmp6*tmp7
  tmp181 = tmp177 + tmp23
  tmp182 = tmp179 + tmp2 + 1/tmp54
  tmp183 = tmp108*tmp17
  tmp184 = -2*tmp72
  tmp185 = 9/tmp54
  tmp186 = 11*tmp83
  tmp187 = -tmp82
  tmp188 = tmp128 + 1/tmp54
  tmp189 = 3/tmp54
  tmp190 = -4*tmp70
  tmp191 = 3*tmp70
  tmp192 = 5*tmp83
  tmp193 = -2*tmp82
  tmp194 = -5/tmp10
  tmp195 = 2*tmp83
  tmp196 = 6*tmp61
  tmp197 = tmp194/tmp54
  tmp198 = 11/tmp10
  tmp199 = 33*tmp61
  tmp200 = 13*tmp61
  tmp201 = tmp105 + tmp114 + tmp200
  tmp202 = 96*tmp61
  tmp203 = 17/tmp10
  tmp204 = -26*tmp72
  tmp205 = 41/tmp10
  tmp206 = -(tmp61/tmp10)
  tmp207 = -4*tmp61
  tmp208 = tmp189/tmp10
  tmp209 = 23/tmp54
  tmp210 = 10/tmp10
  tmp211 = tmp11**(-6)
  tmp212 = 131*tmp83
  tmp213 = -23*tmp72
  tmp214 = 15/tmp54
  tmp215 = tt**6
  tmp216 = -31*tmp70
  tmp217 = 11/tmp54
  tmp218 = 9*tmp61
  tmp219 = 276*tmp61
  tmp220 = 32*tmp82
  tmp221 = tmp11**(-7)
  tmp222 = -71*tmp72
  tmp223 = tt**7
  tmp224 = -9*tmp70
  tmp225 = 33*tmp72
  tmp226 = 10*tmp61
  tmp227 = 3*tmp83
  tmp228 = tmp11**(-8)
  tmp229 = -7*tmp70
  tmp230 = 43*tmp83
  tmp231 = 103*tmp61
  tmp232 = 7*tmp70
  tmp233 = tmp214/tmp10
  tmp234 = -71/tmp10
  tmp235 = 9*tmp83
  tmp236 = -28*tmp72
  tmp237 = tt**8
  tmp238 = -tmp61
  tmp239 = tmp238 + tmp60
  tmp240 = 1/tmp157
  tmp241 = 2*m2
  tmp242 = tmp241 + tmp97
  tmp243 = -(1/tmp55)
  tmp244 = 1/tmp10 + tmp243
  tmp245 = tmp176*tmp244
  tmp246 = (tmp160*tmp97)/tmp9
  tmp247 = tmp245 + tmp246
  tmp248 = tmp247/tmp10
  tmp249 = tmp22 + tmp248
  tmp250 = 1/(tmp11*tmp9)
  tmp251 = tmp177 + tmp250
  tmp252 = m2**5
  tmp253 = 14/tmp11
  tmp254 = 2*tmp89
  tmp255 = 5*tmp44*tt
  tmp256 = 6*tmp5*tmp6
  tmp257 = (4*tmp48)/tmp11
  tmp258 = m2**8
  tmp259 = m2**7
  tmp260 = 4/tmp54
  tmp261 = 32*tmp61
  tmp262 = -93*tmp70
  tmp263 = -38*tmp70
  tmp264 = -3*tmp83
  tmp265 = (-53*tmp61)/tmp10
  tmp266 = tmp208*tmp86
  tmp267 = tmp144 + 1/tmp54
  tmp268 = tmp267*tmp60
  tmp269 = tmp184 + tmp61 + tmp92
  tmp270 = tmp269/tmp55
  tmp271 = tmp266 + tmp268 + tmp270
  tmp272 = m2**6
  tmp273 = 13/tmp54
  tmp274 = 163/tmp54
  tmp275 = 30*tmp61
  tmp276 = -8*tmp72
  tmp277 = (14*tmp70)/tmp54
  tmp278 = (tmp128*tmp86)/tmp54
  tmp279 = tmp188*tmp60
  tmp280 = tmp103 + tmp184 + tmp61
  tmp281 = tmp280/tmp55
  tmp282 = tmp278 + tmp279 + tmp281
  tmp283 = 2*tmp5*tt
  tmp284 = (2*tmp6)/tmp11
  tmp285 = tmp283 + tmp284 + tmp44 + tmp48
  tmp286 = tmp117/tmp10
  tmp287 = -13*tmp61
  tmp288 = 8*tmp72
  tmp289 = -3*tmp61
  tmp290 = tmp17*tmp217
  tmp291 = 164*tmp61
  tmp292 = 149*tmp72
  tmp293 = 34*tmp82
  tmp294 = (tmp203*tmp86)/tmp54
  tmp295 = tmp203 + tmp273
  tmp296 = tmp295*tmp60
  tmp297 = -17*tmp70
  tmp298 = tmp200 + tmp204 + tmp297
  tmp299 = tmp298/tmp55
  tmp300 = tmp294 + tmp296 + tmp299
  tmp301 = -21*tmp72
  tmp302 = 49*tmp87
  tmp303 = 49/tmp10
  tmp304 = tmp209 + tmp303
  tmp305 = tmp304*tmp60
  tmp306 = 23*tmp61
  tmp307 = -46*tmp72
  tmp308 = -49*tmp70
  tmp309 = tmp306 + tmp307 + tmp308
  tmp310 = tmp309/tmp55
  tmp311 = tmp302 + tmp305 + tmp310
  tmp312 = 29*tmp72
  tmp313 = -34*tmp70
  tmp314 = -87*tmp72
  tmp315 = 31*tmp87
  tmp316 = 8/tmp54
  tmp317 = 31/tmp10
  tmp318 = tmp316 + tmp317
  tmp319 = tmp318*tmp60
  tmp320 = -16*tmp72
  tmp321 = tmp112 + tmp216 + tmp320
  tmp322 = tmp321/tmp55
  tmp323 = tmp315 + tmp319 + tmp322
  tmp324 = tmp198 + 1/tmp54
  tmp325 = -437*tmp70
  tmp326 = tmp317/tmp54
  tmp327 = 41/tmp54
  tmp328 = -8*tmp70
  tmp329 = 70*tmp61
  tmp330 = 24*tmp61
  tmp331 = (32*tmp70)/tmp54
  tmp332 = (tmp185*tmp86)/tmp10
  tmp333 = 9/tmp10
  tmp334 = tmp333 + 1/tmp54
  tmp335 = tmp334*tmp60
  tmp336 = tmp184 + tmp224 + tmp61
  tmp337 = tmp336/tmp55
  tmp338 = tmp332 + tmp335 + tmp337
  tmp339 = tmp11**(-9)
  tmp340 = tmp17*tmp70*tmp76
  tmp341 = 35/tmp10
  tmp342 = -9*tmp72
  tmp343 = tmp273/tmp10
  tmp344 = 34*tmp70
  tmp345 = -11*tmp72
  tmp346 = tmp289 + tmp72 + tmp73
  tmp347 = -59*tmp61
  tmp348 = 21*tmp83
  tmp349 = 13/tmp10
  tmp350 = 60*tmp61
  tmp351 = 16*tmp61
  tmp352 = 8*tmp70
  tmp353 = 7*tmp83
  tmp354 = 4*tmp61
  tmp355 = tmp352/tmp54
  tmp356 = tt**9
  tmp357 = 1/tmp70
  tmp358 = -2*tmp61
  tmp359 = 5*tmp70
  tmp360 = 19/tmp10
  tmp361 = -9*tmp61
  tmp362 = 73*tmp61
  tmp363 = 175*tmp83
  tmp364 = 83/tmp54
  tmp365 = 37*tmp61
  tmp366 = -19*tmp70
  tmp367 = (-52*tmp70)/tmp54
  tmp368 = 12*tmp82
  tmp369 = 4*tmp70
  tmp370 = 77*tmp83
  tmp371 = 8*tmp82
  tmp372 = 7*tmp60
  tmp373 = 3*tmp60
  tmp374 = tmp76*tmp86
  tmp375 = tmp17*tmp76
  tmp376 = tmp349 + 1/tmp54
  tmp377 = 23/tmp10
  tmp378 = 26/tmp10
  tmp379 = 70*tmp72
  tmp380 = 55*tmp72
  tmp381 = 85/tmp54
  tmp382 = 29*tmp61
  tmp383 = 284*tmp61
  tmp384 = -12*tmp70
  tmp385 = 4*tmp82
  tmp386 = tmp360/tmp54
  tmp387 = tmp217*tmp70
  tmp388 = 64*tmp72
  tmp389 = -98*tmp70
  tmp390 = -49*tmp72
  tmp391 = -79*tmp70
  tmp392 = 689*tmp72
  tmp393 = 155*tmp61
  tmp394 = 45*tmp72
  tmp395 = 3*tt
  tmp396 = tmp253 + tmp395
  tmp397 = -8*tmp32*tmp396
  tmp398 = 18*tmp5
  tmp399 = 13*tmp36
  tmp400 = tmp398 + tmp399 + tmp6
  tmp401 = 4*tmp4*tmp400
  tmp402 = 10*tmp44
  tmp403 = 15*tmp5*tt
  tmp404 = (9*tmp6)/tmp11
  tmp405 = 3*tmp48
  tmp406 = tmp402 + tmp403 + tmp404 + tmp405
  tmp407 = -2*m2*tmp406
  tmp408 = tmp254 + tmp255 + tmp256 + tmp257 + tmp31 + tmp397 + tmp401 + tmp407 + &
            &tmp93
  tmp409 = 256*tmp252
  tmp410 = tmp100**(-2)
  tmp411 = tmp9**2
  tmp412 = (tmp17*tmp61)/tmp10
  tmp413 = tmp19*tmp74
  tmp414 = tmp135 + tmp77 + tmp80
  tmp415 = tmp414*tmp60
  tmp416 = tmp135 + tmp61 + tmp73
  tmp417 = tmp416/(tmp54*tmp55)
  tmp418 = tmp412 + tmp413 + tmp415 + tmp417
  tmp419 = -17*tmp72
  tmp420 = -5*tmp70
  tmp421 = 14*tmp61
  tmp422 = 7*tmp61
  tmp423 = -250*tmp72
  tmp424 = 2*tmp412
  tmp425 = 2*tmp413
  tmp426 = -13*tmp72
  tmp427 = 15*tmp70
  tmp428 = -tmp48
  tmp429 = 12*tmp70
  tmp430 = -134*tmp72
  tmp431 = -23*tmp70
  tmp432 = 53*tmp72
  tmp433 = 4/tmp11
  tmp434 = 21/tmp11
  tmp435 = 63*tmp36
  tmp436 = 192*tmp30
  tmp437 = tmp166*tt
  tmp438 = tmp428/tmp11
  tmp439 = tmp433 + tt
  tmp440 = -8*m2*tmp439*tmp5
  tmp441 = tmp434 + tt
  tmp442 = -16*tmp32*tmp441
  tmp443 = 5*tt
  tmp444 = tmp434 + tmp443
  tmp445 = (8*tmp4*tmp444)/tmp11
  tmp446 = tmp254 + tmp436 + tmp437 + tmp438 + tmp440 + tmp442 + tmp445
  tmp447 = 384*tmp252
  tmp448 = 8/tmp11
  tmp449 = tmp395 + tmp448
  tmp450 = -96*tmp30*tmp449
  tmp451 = 63*tmp5
  tmp452 = 2*tmp6
  tmp453 = tmp435 + tmp451 + tmp452
  tmp454 = 8*tmp32*tmp453
  tmp455 = 37*tmp5
  tmp456 = 10*tmp6
  tmp457 = tmp435 + tmp455 + tmp456
  tmp458 = (-4*tmp4*tmp457)/tmp11
  tmp459 = tmp6*tmp94
  tmp460 = tmp44 + tmp459 + tmp46 + tmp48
  tmp461 = -(tmp460*tmp5)
  tmp462 = 5*tmp44
  tmp463 = tmp168*tmp5
  tmp464 = tmp284 + tmp462 + tmp463 + tmp48
  tmp465 = m2*tmp433*tmp464
  tmp466 = tmp447 + tmp450 + tmp454 + tmp458 + tmp461 + tmp465
  tmp467 = -2*m2*tmp21*tmp3
  tmp468 = tmp162/tmp9
  tmp469 = tmp467 + tmp468
  tmp470 = tmp5/tmp55
  tmp471 = -tmp180
  tmp472 = 21/tmp54
  tmp473 = 20/tmp55
  tmp474 = tmp395*tmp5*tmp7
  tmp475 = tmp48*tmp7
  tmp476 = 11/tmp55
  tmp477 = 2*tmp177
  tmp478 = 37/tmp55
  tmp479 = 37/tmp54
  tmp480 = tmp443*tmp7
  tmp481 = tmp5/tmp9
  tmp482 = tmp120 + tmp61 + tmp77
  tmp483 = tmp482/(tmp54*tmp55)
  tmp484 = tmp120 + tmp77 + tmp80
  tmp485 = tmp484*tmp60
  tmp486 = tmp412 + tmp413 + tmp483 + tmp485
  tmp487 = 2*tmp36
  tmp488 = (tmp17*tmp422)/tmp10
  tmp489 = 7*tmp413
  tmp490 = -73*tmp72
  tmp491 = 80*tmp70
  tmp492 = 5*tmp412
  tmp493 = 5*tmp413
  tmp494 = 54*tmp70
  tmp495 = tmp105 + tmp77 + tmp80
  tmp496 = tmp495*tmp60
  tmp497 = tmp105 + tmp352 + tmp61
  tmp498 = tmp497/(tmp54*tmp55)
  tmp499 = tmp412 + tmp413 + tmp496 + tmp498
  tmp500 = tmp139 + tmp77 + tmp80
  tmp501 = tmp500*tmp60
  tmp502 = 11*tmp70
  tmp503 = tmp139 + tmp502 + tmp61
  tmp504 = tmp503/(tmp54*tmp55)
  tmp505 = tmp412 + tmp413 + tmp501 + tmp504
  tmp506 = -61*tmp72
  tmp507 = -14*tmp72
  tmp508 = 23*tmp70
  tmp509 = -37*tmp72
  tmp510 = 42*tmp70
  tmp511 = -55*tmp72
  tmp512 = 4*tmp412
  tmp513 = 4*tmp413
  tmp514 = -25*tmp72
  tmp515 = 4*tmp5
  tmp516 = 2*tmp5
  tmp517 = tmp487 + tmp516 + tmp6
  tmp518 = 2/tmp11
  tmp519 = tmp21*tmp241*tmp244
  tmp520 = (tmp250*tmp3)/tmp54
  tmp521 = tmp26/(tmp54*tmp55)
  tmp522 = tmp519 + tmp520 + tmp521
  tmp523 = 28*tmp36
  tmp524 = 2*tmp44
  tmp525 = 15*tmp5*tmp6
  tmp526 = tmp518 + tt
  tmp527 = 1/tmp67
  tmp528 = 128*tmp30
  tmp529 = 4*tmp89
  tmp530 = tmp168*tmp44
  tmp531 = (9*tmp48)/tmp11
  tmp532 = 2*tmp93
  tmp533 = -112*tmp32*tmp526
  tmp534 = tmp434*tt
  tmp535 = 4*tmp6
  tmp536 = tmp398 + tmp534 + tmp535
  tmp537 = 8*tmp4*tmp536
  tmp538 = 20*tmp5*tt
  tmp539 = tmp253*tmp6
  tmp540 = tmp402 + tmp405 + tmp538 + tmp539
  tmp541 = tmp1*tmp540
  tmp542 = tmp525 + tmp528 + tmp529 + tmp530 + tmp531 + tmp532 + tmp533 + tmp537 +&
            & tmp541
  tmp543 = 64*tmp32
  tmp544 = 8*tmp34*tmp4
  tmp545 = -2*tmp36
  tmp546 = -5*tmp6
  tmp547 = tmp515 + tmp545 + tmp546
  tmp548 = tmp241*tmp547
  tmp549 = tmp517*tt
  tmp550 = tmp543 + tmp544 + tmp548 + tmp549
  tmp551 = 256*tmp30
  tmp552 = tmp526**2
  tmp553 = tmp552*tmp6
  tmp554 = tmp395 + tmp518
  tmp555 = -96*tmp32*tmp554
  tmp556 = 7*tmp36
  tmp557 = tmp41 + tmp5 + tmp556
  tmp558 = 32*tmp4*tmp557
  tmp559 = 16*tmp5
  tmp560 = 7*tmp6
  tmp561 = tmp523 + tmp559 + tmp560
  tmp562 = -2*m2*tmp561*tt
  tmp563 = tmp551 + tmp553 + tmp555 + tmp558 + tmp562
  tmp564 = tmp468 + tmp519
  tmp565 = 10*tmp5
  tmp566 = tmp433*tt
  tmp567 = 4*tmp44
  tmp568 = -14/tmp11
  tmp569 = tmp568 + tt
  tmp570 = 16*tmp32*tmp569
  tmp571 = 18/tmp11
  tmp572 = tmp571 + tt
  tmp573 = tmp4*tmp448*tmp572
  tmp574 = tmp41 + tmp565 + tmp566
  tmp575 = (tmp1*tmp574)/tmp11
  tmp576 = tmp515*tt
  tmp577 = tmp47 + tmp48 + tmp567 + tmp576
  tmp578 = tmp577/tmp11
  tmp579 = tmp528 + tmp570 + tmp573 + tmp575 + tmp578
  tmp580 = 6/tmp54
  tmp581 = 1/tmp13
  tmp582 = tmp516 + tmp566 + tmp6
  tmp583 = tmp17*tmp260
  tmp584 = -5*tmp61
  tmp585 = 13*tmp83
  tmp586 = (-35*tmp61)/tmp10
  tmp587 = 39/tmp10
  tmp588 = (-229*tmp61)/tmp10
  tmp589 = 51/tmp54
  tmp590 = 15*tmp83
  tmp591 = tmp580*tmp70
  tmp592 = 19/tmp54
  tmp593 = 6/tmp10
  tmp594 = tmp592 + tmp593
  tmp595 = -21*tmp70
  tmp596 = -18*tmp70
  tmp597 = -121*tmp61
  tmp598 = 65*tmp61
  tmp599 = 61*tmp83
  tmp600 = tmp494/tmp54
  tmp601 = tmp17*tmp592
  tmp602 = -5/tmp54
  tmp603 = tmp375/tmp10
  tmp604 = 1/tmp10 + tmp117
  tmp605 = tmp146/tmp10
  tmp606 = -16*tmp70
  tmp607 = 82*tmp61
  tmp608 = -11*tmp70
  tmp609 = -43*tmp72
  tmp610 = tmp341/tmp54
  tmp611 = 35*tmp83
  tmp612 = 25/tmp10
  tmp613 = 45*tmp61
  tmp614 = 23*tmp83
  tmp615 = (-127*tmp61)/tmp10
  tmp616 = (tmp143*tmp62*tmp76)/tmp9
  tmp617 = 9*tmp74
  tmp618 = 56*tmp70
  tmp619 = -175*tmp72
  tmp620 = 20*tmp61
  tmp621 = 86*tmp61
  tmp622 = tmp364/tmp10
  tmp623 = 53*tmp61
  tmp624 = 4*tmp48
  tmp625 = (tmp17*tmp7)/tmp55
  tmp626 = tmp160*tmp72
  tmp627 = tmp625 + tmp626
  tmp628 = -3*tmp72
  tmp629 = tmp61 + tmp628 + tmp73
  tmp630 = tmp108 + tmp593
  tmp631 = 61/tmp10
  tmp632 = tmp17**2
  tmp633 = 65*tmp72
  tmp634 = (-6*tmp61)/tmp10
  tmp635 = 44*tmp83
  tmp636 = tmp55**(-4)
  tmp637 = 21*tmp61
  tmp638 = (30*tmp70)/tmp54
  tmp639 = -12*tmp82
  tmp640 = -11*tmp83
  tmp641 = tmp54**(-4)
  tmp642 = tmp82/tmp54
  tmp643 = tmp10**(-4)
  tmp644 = -39*tmp72
  tmp645 = 284*tmp72
  tmp646 = 75*tmp83
  tmp647 = 122*tmp83
  tmp648 = -3*tmp82
  tmp649 = tmp287/tmp10
  tmp650 = (-16*tmp61)/tmp10
  tmp651 = tmp152*tmp70
  tmp652 = 8*tmp83
  tmp653 = -17/tmp54
  tmp654 = -46*tmp70
  tmp655 = tmp103/tmp54
  tmp656 = 4*tmp83
  tmp657 = tmp369/tmp54
  tmp658 = -7/tmp54
  tmp659 = tmp63/tmp55
  tmp660 = -6*tmp72
  tmp661 = (-33*tmp61)/tmp10
  tmp662 = 9*tmp82
  tmp663 = tmp238*tmp632*tmp70
  tmp664 = (-11*tmp61)/tmp10
  tmp665 = -tmp643
  tmp666 = tmp378/tmp54
  tmp667 = 6*tmp83
  tmp668 = 5*tmp82
  tmp669 = 3*tmp641
  tmp670 = tmp179/tmp54
  tmp671 = tmp60 + tmp629 + tmp659 + tmp670
  tmp672 = 10*tmp70
  tmp673 = tmp429/tmp54
  tmp674 = 10*tmp82
  tmp675 = 9*tmp641
  tmp676 = 100*tmp72
  tmp677 = 30*tmp83
  tmp678 = (68*tmp70)/tmp54
  tmp679 = 22*tmp72
  tmp680 = tmp155 + tmp679 + tmp77
  tmp681 = tmp636*tmp680
  tmp682 = -9*tmp83
  tmp683 = tmp595/tmp54
  tmp684 = 8*tmp4
  tmp685 = 1/tmp16
  tmp686 = 4*tt
  tmp687 = (tmp17*tmp7)/tmp54
  tmp688 = tmp160/(tmp10*tmp9)
  tmp689 = tmp687 + tmp688
  tmp690 = 2*tt
  tmp691 = tmp250*tmp3
  tmp692 = tmp26/tmp55
  tmp693 = tmp691 + tmp692
  tmp694 = tmp693/tmp54
  tmp695 = tmp467 + tmp694
  tmp696 = 24*tmp4
  tmp697 = tmp97/tmp11
  tmp698 = 3/tmp11
  tmp699 = tmp690 + tmp698
  tmp700 = -2*m2*tmp699
  tmp701 = tmp696 + tmp697 + tmp700
  tmp702 = m2*tmp33
  tmp703 = tmp5 + tmp684 + tmp702
  tmp704 = tmp23/tmp10
  tmp705 = tmp26/tmp54
  tmp706 = tmp704 + tmp705
  tmp707 = tmp243*tmp706
  tmp708 = tmp22 + tmp707
  tmp709 = 1/(tmp100*tmp11)
  tmp710 = tmp33 + tmp686
  tmp711 = m2*tmp710
  tmp712 = tmp684 + tmp709 + tmp711
  tmp713 = tmp241 + tmp94
  tmp714 = 32*tmp32
  tmp715 = -2*tmp44
  tmp716 = -3*tmp5*tt
  tmp717 = (-3*tmp6)/tmp11
  tmp718 = -40/tmp11
  tmp719 = tmp686 + tmp718
  tmp720 = tmp4*tmp719
  tmp721 = tmp487 + tmp515 + tmp6
  tmp722 = tmp69*tmp721
  tmp723 = tmp428 + tmp714 + tmp715 + tmp716 + tmp717 + tmp720 + tmp722
  tmp724 = (6*tmp6)/tmp11
  tmp725 = 8*tmp32*tmp569
  tmp726 = 6*tmp6
  tmp727 = tmp398 + tmp556 + tmp726
  tmp728 = 4*tmp4*tmp727
  tmp729 = tmp443*tmp5
  tmp730 = tmp49 + tmp524 + tmp724 + tmp729
  tmp731 = tmp730/tmp11
  tmp732 = tmp39*tt
  tmp733 = tmp402 + tmp404 + tmp405 + tmp732
  tmp734 = -2*m2*tmp733
  tmp735 = tmp31 + tmp725 + tmp728 + tmp731 + tmp734
  tmp736 = (tmp686*tmp7)/tmp11
  tmp737 = 96*tmp30
  tmp738 = tmp44*tt
  tmp739 = (-2*tmp48)/tmp11
  tmp740 = -72/tmp11
  tmp741 = 88*tt
  tmp742 = tmp740 + tmp741
  tmp743 = tmp32*tmp742
  tmp744 = 9*tmp5
  tmp745 = -19*tmp36
  tmp746 = -12*tmp6
  tmp747 = tmp744 + tmp745 + tmp746
  tmp748 = 4*tmp4*tmp747
  tmp749 = -10*tmp44
  tmp750 = 8*tmp5*tt
  tmp751 = (24*tmp6)/tmp11
  tmp752 = 6*tmp48
  tmp753 = tmp749 + tmp750 + tmp751 + tmp752
  tmp754 = m2*tmp753
  tmp755 = tmp737 + tmp738 + tmp739 + tmp743 + tmp748 + tmp754 + tmp89
  tmp756 = tmp395*tmp7
  tmp757 = tmp177*tmp448
  tmp758 = 8*tmp180
  tmp759 = 1024*tmp21*tmp259*tmp62
  tmp760 = tmp217 + tmp75
  tmp761 = -20*tmp61
  tmp762 = tmp217/tmp10
  tmp763 = tmp377*tmp61
  tmp764 = tmp17*tmp273
  tmp765 = 27/tmp54
  tmp766 = -11*tmp61
  tmp767 = tmp333/tmp54
  tmp768 = 33*tmp83
  tmp769 = (-39*tmp61)/tmp10
  tmp770 = 31/tmp54
  tmp771 = -37*tmp83
  tmp772 = 18*tmp83
  tmp773 = -85*tmp70
  tmp774 = 57*tmp83
  tmp775 = -18*tmp82
  tmp776 = 35/tmp54
  tmp777 = 78*tmp61
  tmp778 = -58*tmp70
  tmp779 = -17*tmp61
  tmp780 = -8*tmp82
  tmp781 = 2*tmp60
  tmp782 = tmp24/tmp55
  tmp783 = tmp189*tmp86
  tmp784 = tmp210 + 1/tmp54
  tmp785 = 1/tmp54 + tmp75
  tmp786 = tmp155 + tmp190 + tmp72
  tmp787 = (-23*tmp61)/tmp10
  tmp788 = 16*tmp82
  tmp789 = tmp765/tmp10
  tmp790 = tmp260/tmp10
  tmp791 = -6*tmp82
  tmp792 = 43/tmp54
  tmp793 = 77*tmp72
  tmp794 = -111*tmp70
  tmp795 = 47*tmp83
  tmp796 = (-133*tmp61)/tmp10
  tmp797 = (-124*tmp70)/tmp54
  tmp798 = 24/tmp54
  tmp799 = 59/tmp10
  tmp800 = 50*tmp61
  tmp801 = -57*tmp70
  tmp802 = tmp587/tmp54
  tmp803 = 26*tmp83
  tmp804 = -7/(tmp10*tmp55)
  tmp805 = 14*tmp72
  tmp806 = tmp128*tmp61
  tmp807 = 76/tmp10
  tmp808 = 12/tmp54
  tmp809 = 79/tmp10
  tmp810 = -24*tmp61
  tmp811 = -12*tmp83
  tmp812 = 24*tmp32
  tmp813 = 56*tt
  tmp814 = tmp167 + tmp813
  tmp815 = tmp4*tmp814
  tmp816 = tmp516 + tmp546
  tmp817 = 6*m2*tmp816
  tmp818 = tmp166 + tmp624 + tmp812 + tmp815 + tmp817
  tmp819 = 1/tmp11 + tmp690
  tmp820 = tmp819*tt
  tmp821 = 1/tmp11 + tmp443
  tmp822 = -2*m2*tmp821
  tmp823 = tmp684 + tmp820 + tmp822
  tmp824 = 8/tmp55
  tmp825 = tmp7*tmp716
  tmp826 = tmp17/tmp55
  tmp827 = 16/tmp10
  tmp828 = -10*tmp70
  tmp829 = tmp60*tmp64
  tmp830 = tmp17*tmp72
  tmp831 = 1/tmp10 + tmp189
  tmp832 = 77/tmp54
  tmp833 = 77*tmp61
  tmp834 = -203*tmp70
  tmp835 = -26*tmp70
  tmp836 = -35*tmp72
  tmp837 = -45*tmp70
  tmp838 = tmp120 + tmp289 + tmp70
  tmp839 = tmp260 + tmp75
  tmp840 = tmp86/tmp54
  tmp841 = tmp17*tmp189
  tmp842 = tmp360*tmp840
  tmp843 = 1/tmp54 + tmp827
  tmp844 = tmp243/tmp10
  tmp845 = tmp18 + tmp60 + tmp844
  tmp846 = 63*tmp72
  tmp847 = 65*tmp83
  tmp848 = 43/tmp10
  tmp849 = tmp472/tmp10
  tmp850 = 47*tmp61
  tmp851 = 47*tmp72
  tmp852 = 12*tmp83
  tmp853 = tmp111 + 1/tmp54
  tmp854 = tmp74*tmp853
  tmp855 = tmp207 + tmp208 + tmp70
  tmp856 = tmp584/tmp10
  tmp857 = -76*tmp70
  tmp858 = tmp234*tmp61
  tmp859 = 17/tmp54
  tmp860 = 17*tmp61
  tmp861 = 42*tmp72
  tmp862 = -65*tmp61
  tmp863 = 67*tmp72
  tmp864 = 17*tmp83
  tmp865 = 44*tmp61
  tmp866 = (-54*tmp70)/tmp54
  tmp867 = 5*tmp60
  tmp868 = (-4*tmp156*tmp239)/(tmp10*tmp9)
  tmp869 = 33*tmp36
  tmp870 = tmp744*tt
  tmp871 = tmp49 + tmp567 + tmp724 + tmp870
  tmp872 = (tmp61*tmp632)/tmp10
  tmp873 = tmp103 + tmp197 + tmp354
  tmp874 = tmp359/tmp54
  tmp875 = 57*tmp72
  tmp876 = 13*tmp70
  tmp877 = (-31*tmp61)/tmp10
  tmp878 = (-56*tmp70)/tmp54
  tmp879 = 10*tmp83
  tmp880 = (-101*tmp61)/tmp10
  tmp881 = tmp191*tmp61*tmp632
  tmp882 = (-21*tmp61)/tmp10
  tmp883 = tmp70*tmp76
  tmp884 = 3*tmp643
  tmp885 = -30*tmp61*tmp70
  tmp886 = 4*tmp642
  tmp887 = tmp196*tmp632*tmp70
  tmp888 = (-206*tmp61)/tmp10
  tmp889 = 6*tmp643
  tmp890 = (-22*tmp70)/tmp54
  tmp891 = (-25*tmp61)/tmp10
  tmp892 = 8*tmp641
  tmp893 = -13/(tmp10*tmp55)
  tmp894 = 44/tmp10
  tmp895 = tmp189 + tmp894
  tmp896 = -91*tmp70
  tmp897 = (-56*tmp61)/tmp10
  tmp898 = (53*tmp70)/tmp54
  tmp899 = 47*tmp82
  tmp900 = -19*tmp61
  tmp901 = 86*tmp72
  tmp902 = -67*tmp70
  tmp903 = tmp327*tmp70
  tmp904 = 46/tmp10
  tmp905 = tmp189 + tmp904
  tmp906 = (38*tmp70)/tmp54
  tmp907 = 66*tmp82
  tmp908 = 19*tmp60
  tmp909 = 28*tmp70
  tmp910 = (-152*tmp61)/tmp10
  tmp911 = tmp632*tmp70*tmp80
  tmp912 = 143*tmp72
  tmp913 = (-30*tmp61)/tmp10
  tmp914 = -4*tmp82
  tmp915 = 2*tmp643
  tmp916 = 12*tmp61
  tmp917 = 24*tmp83
  tmp918 = tmp900/tmp10
  tmp919 = 2*tmp641
  tmp920 = tmp289/tmp10
  tmp921 = (-15*tmp70)/tmp54
  tmp922 = tmp384/tmp54
  tmp923 = 46*tmp61
  tmp924 = (-187*tmp70)/tmp54
  tmp925 = 123/tmp10
  tmp926 = 22/tmp54
  tmp927 = 22*tmp83
  tmp928 = (196*tmp70)/tmp54
  tmp929 = (-28*tmp70)/tmp54
  tmp930 = 33/tmp10
  tmp931 = -9/tmp10
  tmp932 = 1/tmp10 + 1/tmp55
  tmp933 = tmp64/tmp55
  tmp934 = 3*tmp738
  tmp935 = tmp48*tmp518
  tmp936 = 9*tt
  tmp937 = tmp253 + tmp936
  tmp938 = -16*tmp32*tmp937
  tmp939 = tmp936/tmp11
  tmp940 = tmp452 + tmp515 + tmp939
  tmp941 = (tmp1*tmp940)/tmp11
  tmp942 = 23*tmp5
  tmp943 = tmp452 + tmp869 + tmp942
  tmp944 = 4*tmp4*tmp943
  tmp945 = tmp436 + tmp89 + tmp934 + tmp935 + tmp938 + tmp941 + tmp944
  tmp946 = tmp60 + tmp72 + tmp826
  tmp947 = tmp632*tmp94
  tmp948 = tmp64/tmp54
  tmp949 = tmp20 + tmp948
  tmp950 = tmp60 + tmp72 + tmp933
  tmp951 = tmp62*tmp94
  tmp952 = 24/tmp11
  tmp953 = tmp443 + tmp952
  tmp954 = -16*tmp32*tmp953
  tmp955 = 52*tmp5
  tmp956 = 37*tmp36
  tmp957 = tmp172 + tmp955 + tmp956
  tmp958 = 4*tmp4*tmp957
  tmp959 = 12*tmp5
  tmp960 = 17*tmp36
  tmp961 = tmp535 + tmp959 + tmp960
  tmp962 = (tmp1*tmp961)/tmp11
  tmp963 = tmp871/tmp11
  tmp964 = tmp551 + tmp954 + tmp958 + tmp962 + tmp963
  tmp965 = (tmp158*tmp704*tmp713*tmp723)/(tmp55*tmp96)
  tmp966 = -2*m2
  tmp967 = 1/tmp11 + tmp966
  tmp968 = tmp967**2
  tmp969 = 44/tmp54
  tmp970 = tmp698*tmp7
  tmp971 = tmp5*tmp7
  tmp972 = (tmp158*tmp175*tmp36*tmp689*tmp713)/(tmp10*tmp96)
  tmp973 = 14/tmp55
  tmp974 = 14/tmp54
  tmp975 = tmp63 + tmp973 + tmp974
  tmp976 = 4*tmp4*tmp975
  tmp977 = tmp690/tmp9
  tmp978 = tmp970 + tmp977
  tmp979 = -8*m2*tmp978
  tmp980 = tmp6/tmp9
  tmp981 = tmp971 + tmp980
  tmp982 = 3*tmp981
  tmp983 = tmp976 + tmp979 + tmp982
  tmp984 = tmp19/tmp54
  tmp985 = tmp826 + tmp984
  tmp986 = 40/tmp54
  tmp987 = -4/tmp10
  tmp988 = tmp19*tt
  tmp989 = 2048*tmp21*tmp259*tmp3*tmp7
  tmp990 = tmp208 + tmp584 + tmp73
  tmp991 = -2*tmp17*tmp946*tt
  tmp992 = -50*tmp72
  tmp993 = -2*tmp60
  tmp994 = -(tmp60*tmp831)
  tmp995 = tmp289 + tmp70 + tmp91
  tmp996 = tmp995/tmp55
  tmp997 = tmp830 + tmp994 + tmp996
  tmp998 = tmp6*tmp997
  tmp999 = 281*tmp72
  tmp1000 = 255*tmp61
  tmp1001 = -25*tmp70
  tmp1002 = -45*tmp72
  tmp1003 = -18*tmp72
  tmp1004 = 12/tmp10
  tmp1005 = -15*tmp70
  tmp1006 = tmp358 + tmp70 + tmp72
  tmp1007 = tmp75*tmp840
  tmp1008 = tmp60*tmp839
  tmp1009 = tmp229 + tmp276 + tmp354
  tmp1010 = tmp1009/tmp55
  tmp1011 = tmp1007 + tmp1008 + tmp1010
  tmp1012 = -4*tmp1011*tmp48
  tmp1013 = 53/tmp10
  tmp1014 = -519*tmp72
  tmp1015 = -41*tmp70
  tmp1016 = 280*tmp83
  tmp1017 = 71/tmp54
  tmp1018 = -30*tmp72
  tmp1019 = -73*tmp61
  tmp1020 = tmp381/tmp10
  tmp1021 = -89*tmp72
  tmp1022 = (74*tmp70)/tmp54
  tmp1023 = -6/tmp10
  tmp1024 = tmp1023 + 1/tmp54
  tmp1025 = tmp1024/tmp55
  tmp1026 = tmp78*tmp840
  tmp1027 = tmp60*tmp79
  tmp1028 = tmp184 + tmp420 + tmp61
  tmp1029 = tmp1028/tmp55
  tmp1030 = tmp1026 + tmp1027 + tmp1029
  tmp1031 = -4*tmp1030*tmp7*tmp93
  tmp1032 = 32/tmp54
  tmp1033 = 20/tmp10
  tmp1034 = tmp2 + tmp76
  tmp1035 = tmp1034*tmp60
  tmp1036 = tmp70 + tmp80 + tmp88
  tmp1037 = tmp1036/tmp55
  tmp1038 = tmp1035 + tmp1037 + tmp87
  tmp1039 = -79*tmp72
  tmp1040 = -61*tmp70
  tmp1041 = 61*tmp61
  tmp1042 = -62*tmp72
  tmp1043 = -121*tmp72
  tmp1044 = 157*tmp72
  tmp1045 = (-345*tmp61)/tmp10
  tmp1046 = (tmp158*tmp175*tmp250*tmp627*tmp713*tt)/tmp96
  tmp1047 = 56/tmp55
  tmp1048 = tmp1047 + tmp969
  tmp1049 = tmp1048*tmp4
  tmp1050 = tmp690/tmp55
  tmp1051 = tmp1050 + tmp970
  tmp1052 = -8*m2*tmp1051
  tmp1053 = tmp6/tmp55
  tmp1054 = tmp1053 + tmp971
  tmp1055 = 3*tmp1054
  tmp1056 = tmp1049 + tmp1052 + tmp1055
  tmp1057 = -2/tmp54
  tmp1058 = 4/tmp55
  tmp1059 = (tmp158*tmp691*tmp713*tmp723*tmp985*tt)/tmp96
  tmp1060 = 44/tmp55
  tmp1061 = tmp1060 + tmp986 + tmp987
  tmp1062 = tmp1061*tmp4
  tmp1063 = 3/tmp55
  tmp1064 = 1/tmp10 + tmp1063 + tmp260
  tmp1065 = tmp1064/tmp11
  tmp1066 = tmp1065 + tmp988
  tmp1067 = -8*m2*tmp1066
  tmp1068 = tmp76 + tmp932
  tmp1069 = tmp1068*tmp5
  tmp1070 = tmp518*tmp988
  tmp1071 = tmp19*tmp6
  tmp1072 = tmp1069 + tmp1070 + tmp1071
  tmp1073 = 3*tmp1072
  tmp1074 = tmp1062 + tmp1067 + tmp1073
  tmp1075 = tmp117*tmp17
  tmp1076 = 51/tmp10
  tmp1077 = tmp210*tmp840
  tmp1078 = tmp60*tmp784
  tmp1079 = tmp184 + tmp61 + tmp828
  tmp1080 = tmp1079/tmp55
  tmp1081 = tmp17*tmp580
  tmp1082 = tmp785/tmp55
  tmp1083 = tmp1082 + tmp18
  tmp1084 = tmp72 + tmp77 + tmp80
  tmp1085 = tmp1084*tmp74
  tmp1086 = tmp193 + tmp195 + tmp856 + tmp874
  tmp1087 = tmp1086/(tmp54*tmp55)
  tmp1088 = tmp655 + tmp656 + tmp82 + tmp856
  tmp1089 = tmp1088*tmp60
  tmp1090 = tmp1085 + tmp1087 + tmp1089 + tmp872
  tmp1091 = 1024*tmp1090*tmp244*tmp259
  tmp1092 = tmp17*tmp593*tmp61
  tmp1093 = tmp117 + tmp593
  tmp1094 = tmp1093*tmp74
  tmp1095 = tmp135 + tmp422 + tmp92
  tmp1096 = tmp1095*tmp781
  tmp1097 = tmp184 + tmp352 + tmp422
  tmp1098 = tmp1097/(tmp54*tmp55)
  tmp1099 = tmp1092 + tmp1094 + tmp1096 + tmp1098
  tmp1100 = tmp1099*tmp3*tmp94
  tmp1101 = tmp61*tmp70*tmp86
  tmp1102 = tmp110*tmp636
  tmp1103 = tmp74*tmp873
  tmp1104 = tmp281*tmp72
  tmp1105 = tmp195 + tmp82 + tmp856 + tmp874
  tmp1106 = tmp1105*tmp60
  tmp1107 = tmp1101 + tmp1102 + tmp1103 + tmp1104 + tmp1106
  tmp1108 = tmp1107*tt
  tmp1109 = tmp1100 + tmp1108
  tmp1110 = -256*tmp1109*tmp17*tmp272
  tmp1111 = -13*tmp872
  tmp1112 = tmp226 + tmp875 + tmp876
  tmp1113 = tmp1112*tmp74
  tmp1114 = 20*tmp83
  tmp1115 = -13*tmp82
  tmp1116 = tmp1114 + tmp1115 + tmp877 + tmp878
  tmp1117 = tmp1116*tmp60
  tmp1118 = (89*tmp70)/tmp54
  tmp1119 = tmp1118 + tmp151 + tmp879 + tmp880
  tmp1120 = tmp1119/(tmp54*tmp55)
  tmp1121 = tmp1111 + tmp1113 + tmp1117 + tmp1120
  tmp1122 = tmp101*tmp1121
  tmp1123 = tmp191 + tmp286 + tmp80
  tmp1124 = tmp1123*tmp636
  tmp1125 = tmp138/tmp10
  tmp1126 = (-27*tmp70)/tmp54
  tmp1127 = tmp1125 + tmp1126 + tmp656 + tmp791
  tmp1128 = tmp1127*tmp74
  tmp1129 = tmp235 + tmp674 + tmp882 + tmp883
  tmp1130 = (tmp1129*tmp844)/tmp54
  tmp1131 = tmp640/tmp10
  tmp1132 = tmp384*tmp61
  tmp1133 = 30*tmp642
  tmp1134 = tmp1131 + tmp1132 + tmp1133 + tmp884 + tmp919
  tmp1135 = tmp1134*tmp60
  tmp1136 = tmp1124 + tmp1128 + tmp1130 + tmp1135 + tmp881
  tmp1137 = tmp1136*tmp36
  tmp1138 = tmp196 + tmp72 + tmp92
  tmp1139 = -(tmp1138*tmp636)
  tmp1140 = tmp191/tmp54
  tmp1141 = tmp1140 + tmp763 + tmp791 + tmp811
  tmp1142 = tmp1141*tmp74
  tmp1143 = tmp355 + tmp791 + tmp83 + tmp920
  tmp1144 = (tmp1143*tmp72)/tmp55
  tmp1145 = -6*tmp641
  tmp1146 = tmp612*tmp83
  tmp1147 = tmp1145 + tmp1146 + tmp884 + tmp885 + tmp886
  tmp1148 = tmp1147*tmp60
  tmp1149 = tmp1139 + tmp1142 + tmp1144 + tmp1148 + tmp881
  tmp1150 = tmp1149*tmp6
  tmp1151 = tmp1122 + tmp1137 + tmp1150
  tmp1152 = 64*tmp1151*tmp252
  tmp1153 = -12*tmp872
  tmp1154 = 267*tmp72
  tmp1155 = tmp1154 + tmp429 + tmp833
  tmp1156 = tmp1155*tmp74
  tmp1157 = (-473*tmp61)/tmp10
  tmp1158 = (424*tmp70)/tmp54
  tmp1159 = -28*tmp82
  tmp1160 = tmp1157 + tmp1158 + tmp1159 + tmp370
  tmp1161 = tmp1160/(tmp54*tmp55)
  tmp1162 = (-97*tmp61)/tmp10
  tmp1163 = (-152*tmp70)/tmp54
  tmp1164 = tmp1162 + tmp1163 + tmp370 + tmp791
  tmp1165 = tmp1164*tmp781
  tmp1166 = tmp1153 + tmp1156 + tmp1161 + tmp1165
  tmp1167 = tmp1166*tmp3*tmp44
  tmp1168 = tmp112 + tmp191 + tmp610
  tmp1169 = 2*tmp1168*tmp636
  tmp1170 = 32*tmp83
  tmp1171 = (-199*tmp70)/tmp54
  tmp1172 = tmp1170 + tmp1171 + tmp639 + tmp856
  tmp1173 = tmp1172*tmp74
  tmp1174 = (135*tmp70)/tmp54
  tmp1175 = tmp1174 + tmp847 + tmp888 + tmp90
  tmp1176 = (tmp1175*tmp844)/tmp54
  tmp1177 = 16*tmp641
  tmp1178 = (-140*tmp83)/tmp10
  tmp1179 = 75*tmp61*tmp70
  tmp1180 = 135*tmp642
  tmp1181 = tmp1177 + tmp1178 + tmp1179 + tmp1180 + tmp889
  tmp1182 = tmp1181*tmp60
  tmp1183 = tmp1169 + tmp1173 + tmp1176 + tmp1182 + tmp887
  tmp1184 = tmp1183*tmp5*tmp690
  tmp1185 = tmp114*tmp61*tmp632
  tmp1186 = tmp112 + tmp114 + tmp628
  tmp1187 = tmp1186*tmp636
  tmp1188 = (tmp1140*tmp346)/tmp55
  tmp1189 = tmp649 + tmp652 + tmp90
  tmp1190 = 2*tmp1189*tmp74
  tmp1191 = (-23*tmp83)/tmp10
  tmp1192 = tmp508*tmp61
  tmp1193 = tmp648/tmp54
  tmp1194 = -6*tmp643
  tmp1195 = tmp1191 + tmp1192 + tmp1193 + tmp1194 + tmp892
  tmp1196 = tmp1195*tmp60
  tmp1197 = tmp1185 + tmp1187 + tmp1188 + tmp1190 + tmp1196
  tmp1198 = (tmp1197*tmp172)/tmp11
  tmp1199 = tmp207*tmp632*tmp70
  tmp1200 = tmp112 + tmp190 + tmp286
  tmp1201 = tmp1200*tmp636
  tmp1202 = -7*tmp83
  tmp1203 = tmp637/tmp10
  tmp1204 = tmp1202 + tmp1203 + tmp371 + tmp890
  tmp1205 = (tmp1204*tmp72)/tmp55
  tmp1206 = 16*tmp83
  tmp1207 = tmp1206 + tmp371 + tmp683 + tmp891
  tmp1208 = tmp1207*tmp74
  tmp1209 = (-39*tmp83)/tmp10
  tmp1210 = 40*tmp61*tmp70
  tmp1211 = 6*tmp642
  tmp1212 = -4*tmp643
  tmp1213 = tmp1209 + tmp1210 + tmp1211 + tmp1212 + tmp892
  tmp1214 = tmp1213*tmp60
  tmp1215 = tmp1199 + tmp1201 + tmp1205 + tmp1208 + tmp1214
  tmp1216 = -2*tmp1215*tmp48
  tmp1217 = tmp1167 + tmp1184 + tmp1198 + tmp1216
  tmp1218 = -16*tmp1217*tmp30
  tmp1219 = (5*tmp3*tmp89)/tmp9
  tmp1220 = tmp185/tmp55
  tmp1221 = -16/(tmp10*tmp55)
  tmp1222 = tmp106 + tmp1220 + tmp1221 + tmp345 + tmp372 + tmp80
  tmp1223 = 2*tmp1222*tmp738
  tmp1224 = tmp1058/tmp54
  tmp1225 = tmp1224 + tmp197 + tmp369 + tmp373 + tmp61 + tmp804
  tmp1226 = tmp1225*tmp5*tmp726
  tmp1227 = tmp824/tmp54
  tmp1228 = tmp1227 + tmp155 + tmp345 + tmp352 + tmp867 + tmp893
  tmp1229 = tmp1228*tmp935
  tmp1230 = (tmp160*tmp93)/tmp9
  tmp1231 = tmp1219 + tmp1223 + tmp1226 + tmp1229 + tmp1230
  tmp1232 = (tmp1231*tmp160*tmp44*tmp844)/tmp54
  tmp1233 = tmp74*tmp895
  tmp1234 = tmp196 + tmp342 + tmp896
  tmp1235 = tmp1234*tmp60
  tmp1236 = 50*tmp72
  tmp1237 = -47*tmp70
  tmp1238 = tmp1236 + tmp1237 + tmp289
  tmp1239 = tmp1238*tmp72
  tmp1240 = tmp227 + tmp897 + tmp898 + tmp899
  tmp1241 = tmp1240/tmp55
  tmp1242 = tmp1233 + tmp1235 + tmp1239 + tmp1241
  tmp1243 = tmp1242*tmp89
  tmp1244 = tmp107*tmp843
  tmp1245 = -115*tmp70
  tmp1246 = tmp1245 + tmp196 + tmp286
  tmp1247 = tmp1246*tmp60
  tmp1248 = tmp900 + tmp901 + tmp902
  tmp1249 = tmp1248*tmp72
  tmp1250 = (-60*tmp61)/tmp10
  tmp1251 = 67*tmp82
  tmp1252 = tmp1250 + tmp1251 + tmp227 + tmp903
  tmp1253 = tmp1252/tmp55
  tmp1254 = tmp1244 + tmp1247 + tmp1249 + tmp1253
  tmp1255 = 2*tmp1254*tmp738
  tmp1256 = tmp74*tmp905
  tmp1257 = -56*tmp70
  tmp1258 = tmp1257 + tmp155 + tmp790
  tmp1259 = tmp1258*tmp781
  tmp1260 = 33*tmp70
  tmp1261 = tmp1260 + tmp226 + tmp609
  tmp1262 = (tmp1057*tmp1261)/tmp10
  tmp1263 = (-58*tmp61)/tmp10
  tmp1264 = tmp1263 + tmp227 + tmp906 + tmp907
  tmp1265 = tmp1264/tmp55
  tmp1266 = tmp1256 + tmp1259 + tmp1262 + tmp1265
  tmp1267 = tmp1266*tmp516*tmp6
  tmp1268 = 28/(tmp54*tmp55)
  tmp1269 = -47/(tmp10*tmp55)
  tmp1270 = tmp1268 + tmp1269 + tmp218 + tmp509 + tmp908 + tmp909
  tmp1271 = (tmp1270*tmp160*tmp935)/tmp10
  tmp1272 = tmp160**2
  tmp1273 = (tmp1272*tmp144*tmp93)/tmp9
  tmp1274 = tmp1243 + tmp1255 + tmp1267 + tmp1271 + tmp1273
  tmp1275 = m2*tmp1274*tmp5*tmp670
  tmp1276 = -2*tmp872
  tmp1277 = 282*tmp72
  tmp1278 = tmp1277 + tmp598 + tmp73
  tmp1279 = tmp1278*tmp74
  tmp1280 = (-436*tmp61)/tmp10
  tmp1281 = (391*tmp70)/tmp54
  tmp1282 = -20*tmp82
  tmp1283 = tmp1280 + tmp1281 + tmp1282 + tmp847
  tmp1284 = tmp1283/(tmp54*tmp55)
  tmp1285 = 130*tmp83
  tmp1286 = (-325*tmp70)/tmp54
  tmp1287 = tmp1285 + tmp1286 + tmp193 + tmp910
  tmp1288 = tmp1287*tmp60
  tmp1289 = tmp1276 + tmp1279 + tmp1284 + tmp1288
  tmp1290 = tmp1289*tmp3*tmp89
  tmp1291 = tmp275 + tmp73 + tmp912
  tmp1292 = tmp1291*tmp636
  tmp1293 = 60*tmp83
  tmp1294 = (-376*tmp70)/tmp54
  tmp1295 = tmp1293 + tmp1294 + tmp913 + tmp914
  tmp1296 = tmp1295*tmp74
  tmp1297 = -102*tmp83
  tmp1298 = (359*tmp61)/tmp10
  tmp1299 = (-265*tmp70)/tmp54
  tmp1300 = tmp1297 + tmp1298 + tmp1299 + tmp371
  tmp1301 = (tmp1300*tmp72)/tmp55
  tmp1302 = 30*tmp641
  tmp1303 = (-275*tmp83)/tmp10
  tmp1304 = 193*tmp61*tmp70
  tmp1305 = 225*tmp642
  tmp1306 = tmp1302 + tmp1303 + tmp1304 + tmp1305 + tmp915
  tmp1307 = tmp1306*tmp60
  tmp1308 = tmp1292 + tmp1296 + tmp1301 + tmp1307 + tmp911
  tmp1309 = 2*tmp1308*tmp738
  tmp1310 = 91*tmp72
  tmp1311 = tmp109 + tmp1310 + tmp916
  tmp1312 = tmp1311*tmp636
  tmp1313 = (-211*tmp70)/tmp54
  tmp1314 = tmp1313 + tmp639 + tmp917 + tmp918
  tmp1315 = tmp1314*tmp74
  tmp1316 = -53*tmp83
  tmp1317 = (221*tmp61)/tmp10
  tmp1318 = (-180*tmp70)/tmp54
  tmp1319 = tmp1316 + tmp1317 + tmp1318 + tmp368
  tmp1320 = (tmp1319*tmp72)/tmp55
  tmp1321 = 12*tmp641
  tmp1322 = (-163*tmp83)/tmp10
  tmp1323 = 146*tmp61*tmp70
  tmp1324 = 108*tmp642
  tmp1325 = tmp1321 + tmp1322 + tmp1323 + tmp1324 + tmp889
  tmp1326 = tmp1325*tmp60
  tmp1327 = tmp1312 + tmp1315 + tmp1320 + tmp1326 + tmp887
  tmp1328 = tmp1327*tmp5*tmp6
  tmp1329 = (tmp1057*tmp110*tmp632)/(tmp10*tmp55)
  tmp1330 = tmp484*tmp636
  tmp1331 = tmp114/tmp54
  tmp1332 = tmp1331 + tmp195 + tmp82
  tmp1333 = 2*tmp1332*tmp74
  tmp1334 = tmp1023*tmp83
  tmp1335 = tmp207*tmp70
  tmp1336 = tmp808*tmp82
  tmp1337 = tmp1334 + tmp1335 + tmp1336 + tmp665 + tmp919
  tmp1338 = tmp1337*tmp60
  tmp1339 = tmp1329 + tmp1330 + tmp1333 + tmp1338 + tmp663
  tmp1340 = (-4*tmp1339*tmp48)/tmp11
  tmp1341 = tmp113 + tmp77 + tmp80
  tmp1342 = tmp1341*tmp636
  tmp1343 = tmp151 + tmp656 + tmp920 + tmp921
  tmp1344 = tmp1343*tmp74
  tmp1345 = -5*tmp83
  tmp1346 = (15*tmp61)/tmp10
  tmp1347 = tmp1345 + tmp1346 + tmp151 + tmp922
  tmp1348 = (tmp1347*tmp72)/tmp55
  tmp1349 = (-13*tmp83)/tmp10
  tmp1350 = tmp61*tmp672
  tmp1351 = 8*tmp642
  tmp1352 = tmp1349 + tmp1350 + tmp1351 + tmp665 + tmp919
  tmp1353 = tmp1352*tmp60
  tmp1354 = tmp1342 + tmp1344 + tmp1348 + tmp1353 + tmp663
  tmp1355 = -2*tmp1354*tmp93
  tmp1356 = tmp1290 + tmp1309 + tmp1328 + tmp1340 + tmp1355
  tmp1357 = 8*tmp1356*tmp32
  tmp1358 = 156/tmp10
  tmp1359 = tmp1358 + tmp209
  tmp1360 = tmp1359*tmp74
  tmp1361 = -331*tmp70
  tmp1362 = tmp1361 + tmp490 + tmp923
  tmp1363 = tmp1362*tmp60
  tmp1364 = -23*tmp83
  tmp1365 = (206*tmp61)/tmp10
  tmp1366 = tmp1364 + tmp1365 + tmp385 + tmp924
  tmp1367 = tmp1366/tmp10
  tmp1368 = (-252*tmp61)/tmp10
  tmp1369 = (237*tmp70)/tmp54
  tmp1370 = 171*tmp82
  tmp1371 = tmp1368 + tmp1369 + tmp1370 + tmp614
  tmp1372 = tmp1371/tmp55
  tmp1373 = tmp1360 + tmp1363 + tmp1367 + tmp1372
  tmp1374 = tmp1373*tmp143
  tmp1375 = tmp859 + tmp925
  tmp1376 = tmp1375*tmp74
  tmp1377 = 34*tmp61
  tmp1378 = -308*tmp70
  tmp1379 = tmp1377 + tmp1378 + tmp660
  tmp1380 = tmp1379*tmp60
  tmp1381 = -66*tmp83
  tmp1382 = (259*tmp61)/tmp10
  tmp1383 = (-197*tmp70)/tmp54
  tmp1384 = tmp1381 + tmp1382 + tmp1383 + tmp385
  tmp1385 = tmp1384/tmp10
  tmp1386 = (-195*tmp61)/tmp10
  tmp1387 = (137*tmp70)/tmp54
  tmp1388 = 181*tmp82
  tmp1389 = tmp1386 + tmp1387 + tmp1388 + tmp864
  tmp1390 = tmp1389/tmp55
  tmp1391 = tmp1376 + tmp1380 + tmp1385 + tmp1390
  tmp1392 = tmp1391*tmp690*tmp89
  tmp1393 = 155/tmp10
  tmp1394 = tmp1393 + tmp926
  tmp1395 = tmp1394*tmp74
  tmp1396 = -381*tmp70
  tmp1397 = tmp1396 + tmp419 + tmp865
  tmp1398 = tmp1397*tmp60
  tmp1399 = -83*tmp83
  tmp1400 = (333*tmp61)/tmp10
  tmp1401 = (-262*tmp70)/tmp54
  tmp1402 = tmp1399 + tmp1400 + tmp1401 + tmp368
  tmp1403 = tmp1402/tmp10
  tmp1404 = (-255*tmp61)/tmp10
  tmp1405 = 214*tmp82
  tmp1406 = tmp1404 + tmp1405 + tmp927 + tmp928
  tmp1407 = tmp1406/tmp55
  tmp1408 = tmp1395 + tmp1398 + tmp1403 + tmp1407
  tmp1409 = tmp1408*tmp44*tmp6
  tmp1410 = 17*tmp74
  tmp1411 = (27*tmp61)/tmp10
  tmp1412 = 1/tmp54 + tmp930
  tmp1413 = -(tmp1412*tmp60)
  tmp1414 = -21*tmp61
  tmp1415 = tmp1414 + tmp429 + tmp666
  tmp1416 = tmp1415/tmp55
  tmp1417 = tmp1410 + tmp1411 + tmp1413 + tmp1416 + tmp264 + tmp385 + tmp929
  tmp1418 = (tmp1417*tmp48*tmp516)/tmp10
  tmp1419 = -2*tmp74
  tmp1420 = tmp117 + tmp931
  tmp1421 = tmp1420/(tmp10*tmp55)
  tmp1422 = tmp70/tmp54
  tmp1423 = tmp333 + tmp602
  tmp1424 = tmp1423*tmp60
  tmp1425 = tmp1419 + tmp1421 + tmp1422 + tmp1424 + tmp151 + tmp227 + tmp634
  tmp1426 = (tmp1425*tmp532)/(tmp10*tmp11)
  tmp1427 = tmp156*tmp160*tmp24*tmp671
  tmp1428 = tmp1374 + tmp1392 + tmp1409 + tmp1418 + tmp1426 + tmp1427
  tmp1429 = (-4*tmp1428*tmp4)/(tmp54*tmp55)
  tmp1430 = tmp1091 + tmp1110 + tmp1152 + tmp1218 + tmp1232 + tmp1275 + tmp1357 + &
            &tmp1429
  tmp1431 = (tmp158*tmp175*tmp705)/(tmp55*tmp9)
  tmp1432 = (tmp158*tmp175*tmp243*tmp949*tt)/tmp9
  tmp1433 = tmp241*tmp632*tmp946
  tmp1434 = -(tmp158*tmp949)
  tmp1435 = tmp1434 + tmp947
  tmp1436 = tmp1435/(tmp55*tmp9)
  tmp1437 = tmp1433 + tmp1436
  tmp1438 = tmp1437*tmp945
  tmp1439 = (tmp708*tmp945)/tmp9
  tmp1440 = tmp1431 + tmp1432 + tmp1438 + tmp1439
  tmp1441 = (tmp158*tmp705*tmp723)/(tmp55*tmp9)
  tmp1442 = (tmp158*tmp723*tmp950*tmp97)/(tmp54*tmp9)
  tmp1443 = -((tmp522*tmp964)/tmp9)
  tmp1444 = tmp950*tmp97
  tmp1445 = tmp1444 + tmp951
  tmp1446 = tmp1445/(tmp54*tmp9)
  tmp1447 = tmp1446 + tmp68
  tmp1448 = -(tmp1447*tmp964)
  tmp1449 = tmp1441 + tmp1442 + tmp1443 + tmp1448
  tmp1450 = tmp158*tmp241*tmp249*tmp713*tmp723
  tmp1451 = 1280*tmp252
  tmp1452 = 17/tmp11
  tmp1453 = tmp1452 + tmp443
  tmp1454 = -128*tmp1453*tmp30
  tmp1455 = 178*tmp5
  tmp1456 = 130*tmp36
  tmp1457 = 19*tmp6
  tmp1458 = tmp1455 + tmp1456 + tmp1457
  tmp1459 = 8*tmp1458*tmp32
  tmp1460 = -(tmp5*tmp871)
  tmp1461 = 112*tmp44
  tmp1462 = 139*tmp5*tt
  tmp1463 = (48*tmp6)/tmp11
  tmp1464 = 8*tmp48
  tmp1465 = tmp1461 + tmp1462 + tmp1463 + tmp1464
  tmp1466 = -4*tmp1465*tmp4
  tmp1467 = 34*tmp89
  tmp1468 = 60*tmp738
  tmp1469 = 32*tmp5*tmp6
  tmp1470 = (10*tmp48)/tmp11
  tmp1471 = tmp1467 + tmp1468 + tmp1469 + tmp1470 + tmp93
  tmp1472 = tmp1471*tmp241
  tmp1473 = tmp1451 + tmp1454 + tmp1459 + tmp1460 + tmp1466 + tmp1472
  tmp1474 = (tmp1473*tmp249)/tmp11
  tmp1475 = tmp1450 + tmp1474 + tmp965
  tmp1476 = tmp158*tmp175*tmp177*tmp469*tmp713*tmp966
  tmp1477 = (tmp1056*tmp158*tmp713*tt)/tmp96
  tmp1478 = tmp714/tmp55
  tmp1479 = tmp1057 + 1/tmp55
  tmp1480 = tmp1479*tt
  tmp1481 = tmp1480 + tmp176
  tmp1482 = -(tmp1481*tmp5)
  tmp1483 = tmp1058/tmp11
  tmp1484 = tmp97/tmp54
  tmp1485 = tmp1050 + tmp1483 + tmp1484
  tmp1486 = -8*tmp1485*tmp4
  tmp1487 = 5*tmp470
  tmp1488 = tmp1058 + tmp602
  tmp1489 = tmp1488*tmp36
  tmp1490 = tmp1487 + tmp1489 + tmp180
  tmp1491 = tmp1490*tmp241
  tmp1492 = tmp1478 + tmp1482 + tmp1486 + tmp1491
  tmp1493 = -(tmp1492*tmp175)
  tmp1494 = tmp1477 + tmp1493
  tmp1495 = tmp1494*tmp469*tmp94
  tmp1496 = tmp1046 + tmp1476 + tmp1495
  tmp1497 = m2*tmp158*tmp175*tmp249*tmp690*tmp7*tmp713
  tmp1498 = (tmp158*tmp713)/(tmp9*tmp96)
  tmp1499 = (tmp690*tmp713)/(tmp10*tmp96)
  tmp1500 = tmp158*tmp177*tmp966
  tmp1501 = tmp1498 + tmp1499 + tmp1500
  tmp1502 = -(tmp1501*tmp175)
  tmp1503 = (tmp158*tmp713*tmp983*tt)/tmp96
  tmp1504 = tmp1502 + tmp1503
  tmp1505 = (tmp1504*tmp249)/tmp11
  tmp1506 = tmp1497 + tmp1505 + tmp972
  tmp1507 = tmp1500*tmp564*tmp713*tmp723
  tmp1508 = (tmp713*tmp988)/tmp96
  tmp1509 = (tmp3*tmp713)/tmp96
  tmp1510 = -(m2*tmp177)
  tmp1511 = tmp1509 + tmp1510
  tmp1512 = 2*tmp1511*tmp158
  tmp1513 = tmp1508 + tmp1512
  tmp1514 = -(tmp1513*tmp723)
  tmp1515 = (tmp1074*tmp158*tmp713*tt)/tmp96
  tmp1516 = tmp1514 + tmp1515
  tmp1517 = tmp1516*tmp564*tmp94
  tmp1518 = tmp1059 + tmp1507 + tmp1517
  tmp1519 = tmp226 + tmp419 + tmp420
  tmp1520 = tmp1519*tmp60
  tmp1521 = 22*tmp70
  tmp1522 = tmp138 + tmp1521 + tmp419
  tmp1523 = tmp1522/(tmp54*tmp55)
  tmp1524 = tmp1520 + tmp1523 + tmp492 + tmp493
  tmp1525 = tmp628 + tmp77 + tmp80
  tmp1526 = tmp1525*tmp60
  tmp1527 = tmp369 + tmp61 + tmp628
  tmp1528 = tmp1527/(tmp54*tmp55)
  tmp1529 = tmp1526 + tmp1528 + tmp412 + tmp413
  tmp1530 = 17*tmp70
  tmp1531 = -32*tmp70
  tmp1532 = tmp479/tmp10
  tmp1533 = 3*tmp412
  tmp1534 = tmp859/tmp10
  tmp1535 = tmp6/tmp11
  tmp1536 = -2*tmp48
  tmp1537 = tmp1535 + tmp1536 + tmp44
  tmp1538 = -48/tmp11
  tmp1539 = 80*tt
  tmp1540 = tmp1538 + tmp1539
  tmp1541 = tmp1540*tmp32
  tmp1542 = -10*tmp6
  tmp1543 = tmp1542 + tmp5 + tmp697
  tmp1544 = tmp1543*tmp684
  tmp1545 = -tmp5
  tmp1546 = 5*tmp6
  tmp1547 = tmp1545 + tmp1546 + tmp487
  tmp1548 = m2*tmp1547*tmp686
  tmp1549 = tmp1537*tt
  tmp1550 = tmp1541 + tmp1544 + tmp1548 + tmp1549 + tmp31
  tmp1551 = 9*tmp6
  tmp1552 = 5*tmp48
  tmp1553 = (tmp1550*tmp158*tmp705)/(tmp55*tmp99)
  tmp1554 = 1024*tmp272
  tmp1555 = tmp433 + tmp443
  tmp1556 = -256*tmp1555*tmp252
  tmp1557 = 5*tmp5
  tmp1558 = tmp1551 + tmp1557 + tmp960
  tmp1559 = tmp1558*tmp31
  tmp1560 = tmp5*tt
  tmp1561 = tmp1560 + tmp48 + tmp567 + tmp724
  tmp1562 = tmp1*tmp1561*tmp6
  tmp1563 = tmp404 + tmp49 + tmp524 + tmp732
  tmp1564 = tmp1563*tmp684*tt
  tmp1565 = tmp1552 + tmp44 + tmp459 + tmp46
  tmp1566 = tmp1535*tmp1565
  tmp1567 = 16*tmp1560
  tmp1568 = 29*tmp1535
  tmp1569 = tmp1552 + tmp1567 + tmp1568 + tmp524
  tmp1570 = -16*tmp1569*tmp32
  tmp1571 = tmp1554 + tmp1556 + tmp1559 + tmp1562 + tmp1564 + tmp1566 + tmp1570
  tmp1572 = 6/tmp55
  tmp1573 = 7/tmp55
  tmp1574 = 5/tmp55
  tmp1575 = 33/tmp54
  tmp1576 = tmp524/tmp55
  tmp1577 = tmp44/tmp9
  tmp1578 = -9/tmp55
  tmp1579 = -9/tmp54
  tmp1580 = 8/tmp10
  tmp1581 = 19/tmp55
  tmp1582 = 15/tmp10
  tmp1583 = (tmp158*tt)/tmp9
  tmp1584 = tmp158/tmp9
  tmp1585 = tmp690/tmp10
  tmp1586 = tmp1584 + tmp1585
  tmp1587 = -(tmp1586/tmp99)
  tmp1588 = tmp1583 + tmp1587
  tmp1589 = -43*tmp70
  tmp1590 = 43*tmp61
  tmp1591 = -151*tmp72
  tmp1592 = -29*tmp72
  tmp1593 = -34*tmp72
  tmp1594 = 22*tmp61
  tmp1595 = 25*tmp70
  tmp1596 = -58*tmp72
  tmp1597 = 47*tmp70
  tmp1598 = -115*tmp72
  tmp1599 = tmp17*tmp61*tmp827
  tmp1600 = 16*tmp413
  tmp1601 = 95*tmp70
  tmp1602 = 31*tmp61
  tmp1603 = -193*tmp72
  tmp1604 = 4*tmp1535
  tmp1605 = tmp1604 + tmp48 + tmp567 + tmp750
  tmp1606 = tmp1545*tmp1605
  tmp1607 = tmp752/tmp11
  tmp1608 = 15/tmp55
  tmp1609 = 35/tmp55
  tmp1610 = 30/tmp54
  tmp1611 = -3*tmp1422
  tmp1612 = tmp17**3
  tmp1613 = tmp1580*tmp61
  tmp1614 = tmp214 + tmp360
  tmp1615 = tmp1589/tmp54
  tmp1616 = (-89*tmp83)/tmp10
  tmp1617 = -13/tmp10
  tmp1618 = -55*tmp61
  tmp1619 = -83*tmp72
  tmp1620 = -48*tmp70
  tmp1621 = 102*tmp82
  tmp1622 = 139*tmp61
  tmp1623 = 206*tmp642
  tmp1624 = -22*tmp70
  tmp1625 = 340*tmp82
  tmp1626 = 12*tmp643
  tmp1627 = 188*tmp72
  tmp1628 = -42*tmp641
  tmp1629 = tmp100**(-3)
  tmp1630 = tmp382/tmp10
  tmp1631 = tmp377/tmp54
  tmp1632 = 7*tmp74
  tmp1633 = -6*tmp61
  tmp1634 = -2*tmp83
  tmp1635 = tmp422/tmp10
  tmp1636 = 127*tmp83
  tmp1637 = -218*tmp1422
  tmp1638 = 29/tmp54
  tmp1639 = 229*tmp83
  tmp1640 = 670*tmp83
  tmp1641 = 4*tmp643
  tmp1642 = 134*tmp83
  tmp1643 = (-267*tmp61)/tmp10
  tmp1644 = 46*tmp82
  tmp1645 = -58*tmp1422
  tmp1646 = (-50*tmp61)/tmp10
  tmp1647 = 35*tmp82
  tmp1648 = 17*tmp641
  tmp1649 = 34*tmp83
  tmp1650 = tmp61/tmp10
  tmp1651 = 25*tmp82
  tmp1652 = 236*tmp642
  tmp1653 = -2*tmp643
  tmp1654 = 37*tmp1650
  tmp1655 = -62*tmp83
  tmp1656 = -tmp641
  tmp1657 = (-34*tmp83)/tmp10
  tmp1658 = 41*tmp83
  tmp1659 = 72*tmp82
  tmp1660 = -1590*tmp1422
  tmp1661 = 95*tmp72
  tmp1662 = 166*tmp82
  tmp1663 = 384*tmp30
  tmp1664 = 36*tmp44
  tmp1665 = 512*tmp252
  tmp1666 = 32/tmp11
  tmp1667 = 100*tmp5
  tmp1668 = 26*tmp1535
  tmp1669 = tmp1666 + tmp936
  tmp1670 = -32*tmp1669*tmp30
  tmp1671 = 71*tmp36
  tmp1672 = tmp1667 + tmp1671 + tmp726
  tmp1673 = 8*tmp1672*tmp32
  tmp1674 = 76*tmp44
  tmp1675 = 97*tmp1560
  tmp1676 = -5*tmp48
  tmp1677 = tmp1668 + tmp1674 + tmp1675 + tmp1676
  tmp1678 = -4*tmp1677*tmp4
  tmp1679 = tmp428 + tmp46 + tmp567
  tmp1680 = -(tmp1679*tmp410)
  tmp1681 = 28*tmp89
  tmp1682 = 55*tmp738
  tmp1683 = 30*tmp5*tmp6
  tmp1684 = -4*tmp93
  tmp1685 = tmp1681 + tmp1682 + tmp1683 + tmp1684 + tmp739
  tmp1686 = tmp1685*tmp241
  tmp1687 = tmp1665 + tmp1670 + tmp1673 + tmp1678 + tmp1680 + tmp1686
  tmp1688 = tmp713 + tmp97
  tmp1689 = -64*tmp32*tt
  tmp1690 = 5/tmp11
  tmp1691 = tmp1690 + tt
  tmp1692 = tmp1691*tmp410*tmp966
  tmp1693 = -tmp6
  tmp1694 = tmp1693 + tmp5
  tmp1695 = tmp1694**2
  tmp1696 = 10*tmp36
  tmp1697 = tmp1696 + tmp171 + tmp41
  tmp1698 = tmp1697*tmp684
  tmp1699 = tmp1689 + tmp1692 + tmp1695 + tmp1698
  tmp1700 = 11*tmp1535
  tmp1701 = -256*tmp30*tmp6
  tmp1702 = 1/tmp11 + tmp97
  tmp1703 = tmp100**(-5)
  tmp1704 = -(tmp1702*tmp1703)
  tmp1705 = 7*tmp44
  tmp1706 = 10*tmp1560
  tmp1707 = 7*tmp1535
  tmp1708 = -4*tmp48
  tmp1709 = tmp1705 + tmp1706 + tmp1707 + tmp1708
  tmp1710 = tmp1709*tmp241*tmp410
  tmp1711 = 6*tmp1560
  tmp1712 = tmp1700 + tmp1711 + tmp45 + tmp624
  tmp1713 = tmp1712*tmp714
  tmp1714 = 11*tmp738
  tmp1715 = tmp1714 + tmp525 + tmp529 + tmp531 + tmp93
  tmp1716 = -16*tmp1715*tmp4
  tmp1717 = tmp1701 + tmp1704 + tmp1710 + tmp1713 + tmp1716
  tmp1718 = tmp179 + tmp194 + tmp76
  tmp1719 = tmp967 + tt
  tmp1720 = tmp7/tmp11
  tmp1721 = 36/tmp54
  tmp1722 = 18*tmp61
  tmp1723 = -55*tmp1650
  tmp1724 = -5*tmp82
  tmp1725 = tmp584*tmp632*tmp70
  tmp1726 = -27*tmp1650
  tmp1727 = -5*tmp643
  tmp1728 = -22*tmp82
  tmp1729 = 101*tmp83
  tmp1730 = -521*tmp1650
  tmp1731 = 88*tmp83
  tmp1732 = 14*tmp70
  tmp1733 = 57*tmp61
  tmp1734 = 26*tmp70
  tmp1735 = 10*tmp1650
  tmp1736 = 48*tmp1422
  tmp1737 = 24*tmp82
  tmp1738 = 4*tmp641
  tmp1739 = tmp904/tmp54
  tmp1740 = 19*tmp74
  tmp1741 = -10*tmp61
  tmp1742 = -8*tmp1422
  tmp1743 = -tmp1027
  tmp1744 = 5*tmp74
  tmp1745 = -20*tmp1422
  tmp1746 = -7*tmp61
  tmp1747 = tmp1033/tmp54
  tmp1748 = 56*tmp83
  tmp1749 = 48*tmp83
  tmp1750 = tmp1638*tmp70
  tmp1751 = 276*tmp82
  tmp1752 = 4*tmp1650
  tmp1753 = 6*tmp641
  tmp1754 = tmp289*tmp632*tmp70
  tmp1755 = -16*tmp1422
  tmp1756 = -3*tmp643
  tmp1757 = 50*tmp83
  tmp1758 = tmp361*tmp632*tmp70
  tmp1759 = 26*tmp61
  tmp1760 = tmp925/tmp54
  tmp1761 = -57*tmp1650
  tmp1762 = -9*tmp643
  tmp1763 = tmp1132*tmp632
  tmp1764 = -12*tmp643
  tmp1765 = tmp358*tmp632*tmp70
  tmp1766 = -24*tmp82
  tmp1767 = 6*tmp89
  tmp1768 = 72/tmp11
  tmp1769 = 78*tmp5
  tmp1770 = 27*tmp36
  tmp1771 = tmp85*tmp932
  tmp1772 = -2/tmp55
  tmp1773 = tmp144 + tmp1772
  tmp1774 = tmp1773/tmp55
  tmp1775 = tmp1771 + tmp1774 + tmp61
  tmp1776 = tmp158*tmp175*tmp1775*tmp72*tt
  tmp1777 = tmp3/tmp55
  tmp1778 = tmp932/tmp54
  tmp1779 = tmp1777 + tmp1778
  tmp1780 = tmp1779*tmp241*tmp632
  tmp1781 = tmp781 + tmp840 + tmp933
  tmp1782 = tmp158*tmp1781
  tmp1783 = tmp1782 + tmp947
  tmp1784 = tmp1783*tmp72
  tmp1785 = tmp1780 + tmp1784
  tmp1786 = -(tmp1785*tmp945)
  tmp1787 = -384*tmp252
  tmp1788 = tmp395 + tmp433
  tmp1789 = tmp1788*tmp737
  tmp1790 = 19*tmp5
  tmp1791 = tmp172 + tmp1770 + tmp1790
  tmp1792 = tmp1791*tmp4*tmp433
  tmp1793 = 27*tmp5
  tmp1794 = tmp1770 + tmp1793 + tmp452
  tmp1795 = -8*tmp1794*tmp32
  tmp1796 = tmp1604 + tmp1705 + tmp403 + tmp49
  tmp1797 = (tmp1796*tmp966)/tmp11
  tmp1798 = tmp284 + tmp44 + tmp46 + tmp624
  tmp1799 = tmp1798*tmp5
  tmp1800 = tmp1787 + tmp1789 + tmp1792 + tmp1795 + tmp1797 + tmp1799
  tmp1801 = tmp1666 + tmp443
  tmp1802 = -32*tmp1801*tmp30
  tmp1803 = 76*tmp5
  tmp1804 = tmp1608 + tmp2 + tmp974
  tmp1805 = tmp1063 + tmp316 + tmp78
  tmp1806 = tmp1805*tmp44
  tmp1807 = tmp108 + tmp111 + 1/tmp55
  tmp1808 = 3*tmp1560*tmp1807
  tmp1809 = 2*tmp158*tmp3
  tmp1810 = tmp1809 + tmp988
  tmp1811 = 2*tmp475
  tmp1812 = tmp179 + tmp85
  tmp1813 = 20/tmp54
  tmp1814 = -11/tmp10
  tmp1815 = tmp179 + tmp84
  tmp1816 = -41*tmp72
  tmp1817 = 46*tmp70
  tmp1818 = tmp129 + tmp77 + tmp80
  tmp1819 = tmp1818*tmp60
  tmp1820 = tmp129 + tmp508 + tmp61
  tmp1821 = tmp1820/(tmp54*tmp55)
  tmp1822 = tmp1819 + tmp1821 + tmp412 + tmp413
  tmp1823 = tmp110 + tmp179
  tmp1824 = -(tmp1584/tmp100)
  tmp1825 = tmp1719*tmp24*tt
  tmp1826 = tmp1824 + tmp1825
  tmp1827 = -3*tt
  tmp1828 = 1/tmp11 + tmp1827
  tmp1829 = tmp1828*tmp684
  tmp1830 = -4*tmp36
  tmp1831 = tmp171 + tmp1830 + tmp6
  tmp1832 = tmp1831*tmp966
  tmp1833 = tmp1535 + tmp1560 + tmp1829 + tmp1832 + tmp44 + tmp48
  tmp1834 = -3*tmp6
  tmp1835 = tmp1834 + tmp487 + tmp5
  tmp1836 = 16*tmp1835*tmp4
  tmp1837 = tmp1694 + tmp487
  tmp1838 = tmp1837*tmp410
  tmp1839 = -3*tmp48
  tmp1840 = tmp1839 + tmp44 + tmp459 + tmp46
  tmp1841 = -8*m2*tmp1840
  tmp1842 = tmp1836 + tmp1838 + tmp1841
  tmp1843 = tmp260 + 1/tmp55
  tmp1844 = tmp1572 + 1/tmp54
  tmp1845 = tmp189 + 1/tmp55
  tmp1846 = tmp1573 + tmp580
  tmp1847 = 3*tmp413
  tmp1848 = tmp630*tmp74
  tmp1849 = -31*tmp72
  tmp1850 = -59*tmp1650
  tmp1851 = 115*tmp61
  tmp1852 = -59*tmp72
  tmp1853 = -25*tmp82
  tmp1854 = 560*tmp61
  tmp1855 = -97*tmp72
  tmp1856 = tmp848/tmp54
  tmp1857 = tmp1521/tmp54
  tmp1858 = 353*tmp72
  tmp1859 = -69*tmp70
  tmp1860 = -45*tmp61
  tmp1861 = tmp1856 + tmp1860 + tmp73
  tmp1862 = 45/tmp10
  tmp1863 = 39/tmp54
  tmp1864 = -30*tmp70
  tmp1865 = 25*tmp61
  tmp1866 = tmp144 + tmp260
  tmp1867 = -7*tmp82
  tmp1868 = 16/tmp54
  tmp1869 = -481*tmp61
  tmp1870 = 122*tmp61
  tmp1871 = -73*tmp70
  tmp1872 = 53/tmp54
  tmp1873 = 119/tmp10
  tmp1874 = 36*tmp61
  tmp1875 = -39*tmp70
  tmp1876 = 73*tmp72
  tmp1877 = 55*tmp83
  tmp1878 = -238*tmp1650
  tmp1879 = -11*tmp1422
  tmp1880 = 97/tmp54
  tmp1881 = 236/tmp54
  tmp1882 = 77/tmp10
  tmp1883 = -119*tmp72
  tmp1884 = 130*tmp1422
  tmp1885 = 58*tmp61
  tmp1886 = 14*tmp6
  tmp1887 = (tmp1584*tmp3*tmp408*tmp985*tt)/tmp100
  tmp1888 = tmp144 + tmp476 + tmp974
  tmp1889 = -8*tmp1888*tmp32
  tmp1890 = 12*tmp1535*tmp19
  tmp1891 = tmp19*tmp624
  tmp1892 = 23/tmp55
  tmp1893 = tmp1721 + tmp1892 + tmp349
  tmp1894 = tmp1893/tmp11
  tmp1895 = tmp128 + tmp273 + tmp476
  tmp1896 = tmp1895*tt
  tmp1897 = tmp1894 + tmp1896
  tmp1898 = 4*tmp1897*tmp4
  tmp1899 = tmp1068*tmp1557
  tmp1900 = tmp108 + tmp144 + tmp179
  tmp1901 = (tmp1900*tmp690)/tmp11
  tmp1902 = 3*tmp1071
  tmp1903 = tmp1899 + tmp1901 + tmp1902
  tmp1904 = -6*m2*tmp1903
  tmp1905 = tmp1806 + tmp1808 + tmp1889 + tmp1890 + tmp1891 + tmp1898 + tmp1904
  tmp1906 = tmp1612*tmp72
  tmp1907 = tmp74*tmp984
  tmp1908 = tmp1611 + tmp187 + tmp206 + tmp227
  tmp1909 = tmp1908*tmp60
  tmp1910 = tmp264/tmp10
  tmp1911 = tmp1910 + tmp643 + tmp919
  tmp1912 = tmp1911/tmp55
  tmp1913 = tmp1906 + tmp1907 + tmp1909 + tmp1912
  tmp1914 = 3*tmp1422*tmp1612
  tmp1915 = (tmp188*tmp636)/tmp54
  tmp1916 = tmp1613 + tmp648 + tmp83
  tmp1917 = -(tmp1916*tmp74)
  tmp1918 = tmp1582*tmp83
  tmp1919 = tmp1624*tmp61
  tmp1920 = tmp668/tmp54
  tmp1921 = tmp1656 + tmp1918 + tmp1919 + tmp1920 + tmp884
  tmp1922 = tmp1921/(tmp10*tmp55)
  tmp1923 = tmp192/tmp10
  tmp1924 = tmp70*tmp761
  tmp1925 = tmp1923 + tmp1924 + tmp886 + tmp889 + tmp919
  tmp1926 = -(tmp1925*tmp60)
  tmp1927 = tmp1914 + tmp1915 + tmp1917 + tmp1922 + tmp1926
  tmp1928 = tmp1927*tmp433
  tmp1929 = -2*tmp1422*tmp1612
  tmp1930 = (tmp1614*tmp636)/tmp54
  tmp1931 = tmp1170 + tmp1615 + tmp1723 + tmp193
  tmp1932 = tmp1931*tmp74
  tmp1933 = -11*tmp641
  tmp1934 = tmp83*tmp848
  tmp1935 = -36*tmp61*tmp70
  tmp1936 = tmp1211 + tmp1653 + tmp1933 + tmp1934 + tmp1935
  tmp1937 = tmp1936/(tmp10*tmp55)
  tmp1938 = tmp621*tmp70
  tmp1939 = tmp1868*tmp82
  tmp1940 = tmp1616 + tmp1641 + tmp1648 + tmp1938 + tmp1939
  tmp1941 = tmp1940*tmp60
  tmp1942 = tmp1929 + tmp1930 + tmp1932 + tmp1937 + tmp1941
  tmp1943 = tmp1942*tt
  tmp1944 = tmp1928 + tmp1943
  tmp1945 = 83/tmp10
  tmp1946 = tmp1945 + tmp792
  tmp1947 = 13*tmp82
  tmp1948 = 181/tmp54
  tmp1949 = 319/tmp10
  tmp1950 = tmp1948 + tmp1949
  tmp1951 = -781*tmp1422
  tmp1952 = -189*tmp641
  tmp1953 = (755*tmp83)/tmp10
  tmp1954 = tmp1644/tmp54
  tmp1955 = 6*tmp1422*tmp1612
  tmp1956 = 141/tmp10
  tmp1957 = tmp1956 + tmp364
  tmp1958 = -217*tmp1650
  tmp1959 = -113*tmp641
  tmp1960 = 70/tmp54
  tmp1961 = 179/tmp10
  tmp1962 = tmp1960 + tmp1961
  tmp1963 = -172*tmp1650
  tmp1964 = -217*tmp1422
  tmp1965 = 375/tmp54
  tmp1966 = 902/tmp10
  tmp1967 = tmp1965 + tmp1966
  tmp1968 = -24*tmp643
  tmp1969 = -461*tmp641
  tmp1970 = 12*tmp1422*tmp1612
  tmp1971 = -499*tmp641
  tmp1972 = tmp1721*tmp82
  tmp1973 = 188/tmp10
  tmp1974 = tmp1973 + tmp381
  tmp1975 = 77*tmp641
  tmp1976 = 9*tmp60
  tmp1977 = tmp194/tmp55
  tmp1978 = 27/tmp10
  tmp1979 = tmp1978 + tmp76
  tmp1980 = 165/tmp10
  tmp1981 = tmp1980 + tmp273
  tmp1982 = 210*tmp82
  tmp1983 = 64/tmp10
  tmp1984 = tmp108 + tmp1983
  tmp1985 = 15*tmp61
  tmp1986 = 94*tmp82
  tmp1987 = -135*tmp70
  tmp1988 = -31*tmp61
  tmp1989 = -83*tmp70
  tmp1990 = tmp377 + tmp76
  tmp1991 = -16*tmp61
  tmp1992 = 15*tmp1422
  tmp1993 = 39*tmp82
  tmp1994 = tmp303 + tmp316
  tmp1995 = -205*tmp83
  tmp1996 = -157*tmp72
  tmp1997 = -371*tmp83
  tmp1998 = -255*tmp83
  tmp1999 = 68*tmp83
  tmp2000 = tmp198 + tmp76
  tmp2001 = tmp1632*tmp2000
  tmp2002 = -60*tmp83
  tmp2003 = 14*tmp83
  tmp2004 = 129*tmp82
  tmp2005 = tmp117 + tmp378
  tmp2006 = 97*tmp83
  tmp2007 = 299/tmp10
  tmp2008 = tmp2007 + tmp381
  tmp2009 = -233*tmp1650
  tmp2010 = 394*tmp642
  tmp2011 = -120*tmp641
  tmp2012 = 143/tmp54
  tmp2013 = 483/tmp10
  tmp2014 = tmp2012 + tmp2013
  tmp2015 = 233/tmp54
  tmp2016 = 767/tmp10
  tmp2017 = tmp2015 + tmp2016
  tmp2018 = 1280*tmp61*tmp70
  tmp2019 = -551*tmp641
  tmp2020 = 2*tmp1422*tmp1612
  tmp2021 = 75/tmp10
  tmp2022 = tmp2021 + tmp209
  tmp2023 = 44*tmp641
  tmp2024 = 257*tmp61*tmp70
  tmp2025 = -131*tmp641
  tmp2026 = 18*tmp642
  tmp2027 = -((tmp158*tmp1777*tmp408)/(tmp10*tmp100))
  tmp2028 = -7*tt
  tmp2029 = tmp2028 + tmp433
  tmp2030 = -32*tmp2029*tmp30
  tmp2031 = 11*tmp5
  tmp2032 = 16*tmp6
  tmp2033 = tmp2031 + tmp2032 + tmp869
  tmp2034 = -16*tmp2033*tmp32
  tmp2035 = tmp410*tmp871
  tmp2036 = 40*tmp44
  tmp2037 = 113*tmp1560
  tmp2038 = 94*tmp1535
  tmp2039 = 23*tmp48
  tmp2040 = tmp2036 + tmp2037 + tmp2038 + tmp2039
  tmp2041 = 4*tmp2040*tmp4
  tmp2042 = 22*tmp89
  tmp2043 = tmp1674*tt
  tmp2044 = 93*tmp5*tmp6
  tmp2045 = (48*tmp48)/tmp11
  tmp2046 = 9*tmp93
  tmp2047 = tmp2042 + tmp2043 + tmp2044 + tmp2045 + tmp2046
  tmp2048 = tmp2047*tmp966
  tmp2049 = tmp2030 + tmp2034 + tmp2035 + tmp2041 + tmp2048 + tmp409
  tmp2050 = (tmp1584*tmp408*tmp705)/(tmp100*tmp55)
  tmp2051 = (tmp1824*tmp408*tmp950*tt)/tmp54
  tmp2052 = tmp409*tmp554
  tmp2053 = 32*tmp5
  tmp2054 = 57*tmp36
  tmp2055 = tmp1886 + tmp2053 + tmp2054
  tmp2056 = -32*tmp2055*tmp30
  tmp2057 = tmp2035*tmp94
  tmp2058 = 100*tmp44
  tmp2059 = 221*tmp1560
  tmp2060 = 120*tmp1535
  tmp2061 = 11*tmp48
  tmp2062 = tmp2058 + tmp2059 + tmp2060 + tmp2061
  tmp2063 = 8*tmp2062*tmp32
  tmp2064 = 76*tmp89
  tmp2065 = 211*tmp738
  tmp2066 = 184*tmp5*tmp6
  tmp2067 = (53*tmp48)/tmp11
  tmp2068 = tmp2064 + tmp2065 + tmp2066 + tmp2067 + tmp532
  tmp2069 = -4*tmp2068*tmp4
  tmp2070 = 28*tmp143
  tmp2071 = 97*tmp89*tt
  tmp2072 = 120*tmp44*tmp6
  tmp2073 = 64*tmp48*tmp5
  tmp2074 = tmp253*tmp93
  tmp2075 = tmp156 + tmp2070 + tmp2071 + tmp2072 + tmp2073 + tmp2074
  tmp2076 = tmp2075*tmp241
  tmp2077 = tmp2052 + tmp2056 + tmp2057 + tmp2063 + tmp2069 + tmp2076
  tmp2078 = tmp241*tmp408*tmp564*tmp988
  tmp2079 = (tmp158*tmp1905*tt)/tmp100
  tmp2080 = tmp988/tmp100
  tmp2081 = (-2*tmp3)/tmp100
  tmp2082 = tmp2081 + tmp988
  tmp2083 = -(tmp158*tmp2082)
  tmp2084 = tmp2080 + tmp2083
  tmp2085 = tmp2084*tmp408
  tmp2086 = tmp2079 + tmp2085
  tmp2087 = tmp2086*tmp564
  tmp2088 = tmp1887 + tmp2078 + tmp2087
  tmp2089 = 79*tmp1650*tmp17
  tmp2090 = 79*tmp413
  tmp2091 = 158*tmp61
  tmp2092 = 79*tmp61
  tmp2093 = -229*tmp72
  tmp2094 = 38*tmp61
  tmp2095 = 19*tmp61
  tmp2096 = 65*tmp70
  tmp2097 = -47*tmp72
  tmp2098 = 79*tmp70
  tmp2099 = -62*tmp70
  tmp2100 = 62*tmp61
  tmp2101 = -299*tmp72
  tmp2102 = 35*tmp1650*tmp17
  tmp2103 = 35*tmp413
  tmp2104 = -35*tmp70
  tmp2105 = 35*tmp61
  tmp2106 = -141*tmp72
  tmp2107 = 8*tmp1650*tmp17
  tmp2108 = 8*tmp413
  tmp2109 = 31*tmp70
  tmp2110 = 90*tmp61
  tmp2111 = -158*tmp72
  tmp2112 = 61*tmp1650*tmp17
  tmp2113 = 61*tmp413
  tmp2114 = -178*tmp72
  tmp2115 = 76*tmp61
  tmp2116 = 8*tmp44
  tmp2117 = tmp2032/tmp11
  tmp2118 = tmp1552 + tmp2116 + tmp2117 + tmp538
  tmp2119 = tmp1866 + 1/tmp55
  tmp2120 = tmp3**3
  tmp2121 = -51*tmp72
  tmp2122 = 52/tmp54
  tmp2123 = 157*tmp83
  tmp2124 = 69*tmp61
  tmp2125 = 116*tmp82
  tmp2126 = 4*tmp60
  tmp2127 = 27*tmp70
  tmp2128 = -tmp83
  tmp2129 = tmp765/tmp55
  tmp2130 = -52*tmp72
  tmp2131 = tmp808/tmp55
  tmp2132 = tmp149 + tmp213 + tmp2131 + tmp429 + tmp60 + tmp893
  tmp2133 = -169*tmp1650
  tmp2134 = 7*tmp82
  tmp2135 = -41*tmp1650
  tmp2136 = 18*tmp82
  tmp2137 = tmp184 + tmp61 + tmp77
  tmp2138 = 11*tmp82
  tmp2139 = 11*tmp641
  tmp2140 = -12*tmp72
  tmp2141 = 85*tmp1422
  tmp2142 = 19*tmp83
  tmp2143 = -8*tmp1650
  tmp2144 = -63*tmp1422
  tmp2145 = tmp108 + tmp128
  tmp2146 = -14*tmp70
  tmp2147 = 98*tmp1422
  tmp2148 = 7*tmp641
  tmp2149 = 1/tmp10 + tmp592
  tmp2150 = -40*tmp70
  tmp2151 = 10*tmp641
  tmp2152 = 133*tmp61
  tmp2153 = -1033*tmp1650
  tmp2154 = -188*tmp643
  tmp2155 = 71*tmp641
  tmp2156 = -69*tmp72
  tmp2157 = -64*tmp70
  tmp2158 = -68*tmp643
  tmp2159 = -31/(tmp10*tmp55)
  tmp2160 = -53*tmp72
  tmp2161 = 19*tmp70
  tmp2162 = 168*tmp1422
  tmp2163 = -16*tmp641
  tmp2164 = 72*tmp70
  tmp2165 = -57*tmp61
  tmp2166 = -57*tmp641
  tmp2167 = tmp1721/tmp10
  tmp2168 = 54*tmp1650
  tmp2169 = 139*tmp1422
  tmp2170 = 213*tmp1422
  tmp2171 = 52*tmp82
  tmp2172 = 128*tmp32
  tmp2173 = 61*tmp36
  tmp2174 = 28*tmp1535
  tmp2175 = tmp632/tmp54
  tmp2176 = 19*tt
  tmp2177 = 224*tmp30
  tmp2178 = 2*tmp738
  tmp2179 = -tmp93
  tmp2180 = 27/tmp11
  tmp2181 = tmp2176 + tmp2180
  tmp2182 = -8*tmp2181*tmp32
  tmp2183 = 35*tmp36
  tmp2184 = tmp1693 + tmp2183 + tmp559
  tmp2185 = 4*tmp2184*tmp4
  tmp2186 = tmp1567 + tmp1604 + tmp1676 + tmp45
  tmp2187 = tmp2186*tmp966
  tmp2188 = tmp2177 + tmp2178 + tmp2179 + tmp2182 + tmp2185 + tmp2187 + tmp438
  tmp2189 = 34/tmp54
  tmp2190 = -21/tmp10
  tmp2191 = 99/tmp54
  tmp2192 = -8/tmp10
  tmp2193 = -19/tmp10
  tmp2194 = tmp1058 + tmp260 + tmp63
  tmp2195 = tmp1034 + tmp179
  tmp2196 = tmp1063 + tmp189 + tmp2
  tmp2197 = -6*m2*tt
  tmp2198 = tmp2197 + tmp6 + tmp684
  tmp2199 = tmp158*tmp3*tt
  tmp2200 = tmp6*tmp959
  tmp2201 = 6*tmp93
  tmp2202 = -32*tmp30*tmp937
  tmp2203 = 20*tmp6
  tmp2204 = tmp2203 + tmp869 + tmp942
  tmp2205 = 8*tmp2204*tmp32
  tmp2206 = tmp47 + tmp48 + tmp524
  tmp2207 = tmp1693*tmp2206
  tmp2208 = 18*tmp1560
  tmp2209 = 13*tmp48
  tmp2210 = tmp1568 + tmp2116 + tmp2208 + tmp2209
  tmp2211 = -4*tmp2210*tmp4
  tmp2212 = tmp1470 + tmp2200 + tmp2201 + tmp89 + tmp934
  tmp2213 = tmp2212*tmp241
  tmp2214 = tmp2202 + tmp2205 + tmp2207 + tmp2211 + tmp2213 + tmp447
  tmp2215 = 896*tmp252
  tmp2216 = 34/tmp11
  tmp2217 = 21*tt
  tmp2218 = tmp2216 + tmp2217
  tmp2219 = -32*tmp2218*tmp30
  tmp2220 = 59*tmp5
  tmp2221 = 81*tmp36
  tmp2222 = 40*tmp6
  tmp2223 = tmp2220 + tmp2221 + tmp2222
  tmp2224 = 8*tmp2223*tmp32
  tmp2225 = tmp283 + tmp47 + tmp48 + tmp567
  tmp2226 = tmp1693*tmp2225
  tmp2227 = 22*tmp44
  tmp2228 = 48*tmp1560
  tmp2229 = 63*tmp1535
  tmp2230 = 19*tmp48
  tmp2231 = tmp2227 + tmp2228 + tmp2229 + tmp2230
  tmp2232 = -4*tmp2231*tmp4
  tmp2233 = 3*tmp89
  tmp2234 = tmp44*tmp936
  tmp2235 = 28*tmp5*tmp6
  tmp2236 = tmp48*tmp571
  tmp2237 = tmp2201 + tmp2233 + tmp2234 + tmp2235 + tmp2236
  tmp2238 = tmp2237*tmp241
  tmp2239 = tmp2215 + tmp2219 + tmp2224 + tmp2226 + tmp2232 + tmp2238
  tmp2240 = 118/tmp55
  tmp2241 = 99/tmp55
  tmp2242 = 124/tmp55
  tmp2243 = 64/tmp55
  tmp2244 = 45/tmp54
  tmp2245 = tmp1063 + 1/tmp54
  tmp2246 = tmp1058 + 1/tmp54
  tmp2247 = tmp179 + 1/tmp54
  tmp2248 = 1024*tmp17*tmp21*tmp2120*tmp259
  tmp2249 = -9*tmp1650
  tmp2250 = 26*tmp17*tmp21*tmp2120*tmp5
  tmp2251 = -23*tmp1422
  tmp2252 = tmp1813*tmp82
  tmp2253 = -151*tmp1422
  tmp2254 = 43*tmp641
  tmp2255 = (-193*tmp83)/tmp10
  tmp2256 = 12*tmp17*tmp21*tmp2120*tmp44
  tmp2257 = tmp1983*tmp61
  tmp2258 = 76*tmp642
  tmp2259 = 127*tmp72
  tmp2260 = 51*tmp83
  tmp2261 = 68*tmp641
  tmp2262 = 48*tmp642
  tmp2263 = 57*tmp641
  tmp2264 = 80*tmp72
  tmp2265 = 78*tmp83
  tmp2266 = (-45*tmp83)/tmp10
  tmp2267 = tmp1574/tmp54
  tmp2268 = tmp360 + 1/tmp54
  tmp2269 = 20*tmp70
  tmp2270 = tmp74*tmp784
  tmp2271 = tmp232 + tmp342 + tmp80
  tmp2272 = -14*tmp1650
  tmp2273 = 11*tmp60
  tmp2274 = 1/tmp54 + tmp593
  tmp2275 = tmp384 + tmp61
  tmp2276 = 9*tmp1422
  tmp2277 = -116*tmp1650
  tmp2278 = 83*tmp1422
  tmp2279 = 85/tmp10
  tmp2280 = 61*tmp1422
  tmp2281 = -129*tmp70
  tmp2282 = tmp17*tmp21*tmp2120*tmp254
  tmp2283 = 17*tmp1650
  tmp2284 = tmp82*tmp974
  tmp2285 = tmp61*tmp876
  tmp2286 = 90*tmp83
  tmp2287 = -73*tmp1650
  tmp2288 = 47*tmp641
  tmp2289 = 156*tmp61*tmp70
  tmp2290 = tmp151*tmp2175
  tmp2291 = 25*tmp83
  tmp2292 = -118*tmp1422
  tmp2293 = 80*tmp641
  tmp2294 = 26*tmp641
  tmp2295 = (-151*tmp83)/tmp10
  tmp2296 = -2*tt
  tmp2297 = tmp2296 + tmp69
  tmp2298 = (tmp158*tmp2297)/tmp55
  tmp2299 = tmp690/(tmp54*tmp99)
  tmp2300 = tmp2298 + tmp2299
  tmp2301 = -(tmp175*tmp2300*tmp242)
  tmp2302 = tmp217 + tmp824
  tmp2303 = 8*tmp2302*tmp32
  tmp2304 = tmp44/tmp55
  tmp2305 = tmp1708/tmp55
  tmp2306 = tmp808/tmp11
  tmp2307 = (25*tt)/tmp55
  tmp2308 = tmp217*tt
  tmp2309 = tmp176 + tmp2306 + tmp2307 + tmp2308
  tmp2310 = -4*tmp2309*tmp4
  tmp2311 = tmp243 + 1/tmp54
  tmp2312 = tmp2311*tmp5
  tmp2313 = tmp1574*tmp6
  tmp2314 = tmp2312 + tmp2313 + tmp736
  tmp2315 = 6*m2*tmp2314
  tmp2316 = tmp2303 + tmp2304 + tmp2305 + tmp2310 + tmp2315 + tmp825
  tmp2317 = (tmp158*tmp2316*tt)/tmp99
  tmp2318 = tmp2301 + tmp2317
  tmp2319 = tmp1588*tmp175*tmp242
  tmp2320 = tmp144 + tmp316 + tmp824
  tmp2321 = 8*tmp2320*tmp32
  tmp2322 = tmp1708/tmp9
  tmp2323 = tmp324 + 1/tmp55
  tmp2324 = tmp2323/tmp11
  tmp2325 = 25/tmp55
  tmp2326 = -14/tmp10
  tmp2327 = tmp146 + tmp2325 + tmp2326
  tmp2328 = tmp2327*tt
  tmp2329 = tmp2324 + tmp2328
  tmp2330 = -4*tmp2329*tmp4
  tmp2331 = tmp25*tmp5
  tmp2332 = tmp1830*tmp7
  tmp2333 = -5*tmp980
  tmp2334 = tmp2331 + tmp2332 + tmp2333
  tmp2335 = -6*m2*tmp2334
  tmp2336 = tmp1577 + tmp2321 + tmp2322 + tmp2330 + tmp2335 + tmp825
  tmp2337 = (tmp158*tmp2336*tt)/tmp99
  tmp2338 = tmp2319 + tmp2337
  tmp2339 = (tmp158*tmp818*tt)/(tmp54*tmp99)
  tmp2340 = tmp175*tmp242*tmp76*tmp823
  tmp2341 = tmp2339 + tmp2340
  tmp2342 = (tmp158*tmp818*tt)/(tmp10*tmp99)
  tmp2343 = tmp128*tmp175*tmp242*tmp823
  tmp2344 = tmp2342 + tmp2343
  tmp2345 = 1024*tmp1913*tmp259*tmp3
  tmp2346 = tmp369*tmp61
  tmp2347 = 39*tmp1650
  tmp2348 = 43*tmp1422
  tmp2349 = (-43*tmp83)/tmp10
  tmp2350 = 3*tmp1650
  tmp2351 = 13*tmp643
  tmp2352 = tmp1949/tmp54
  tmp2353 = 52*tmp83
  tmp2354 = (587*tmp83)/tmp10
  tmp2355 = tmp1594*tmp70
  tmp2356 = 49/tmp54
  tmp2357 = tmp1956/tmp54
  tmp2358 = tmp1961/tmp54
  tmp2359 = 51*tmp70
  tmp2360 = 13*tmp641
  tmp2361 = 41*tmp61
  tmp2362 = tmp1966/tmp54
  tmp2363 = 70*tmp83
  tmp2364 = 144*tmp83
  tmp2365 = 152*tmp70
  tmp2366 = 71*tmp83
  tmp2367 = (tmp160*tmp1767*tmp3)/tmp9
  tmp2368 = -20/tmp10
  tmp2369 = -4*tmp1422
  tmp2370 = 16*tmp70
  tmp2371 = 4*tmp74
  tmp2372 = tmp1814 + tmp189
  tmp2373 = tmp2372*tmp60
  tmp2374 = tmp155 + tmp328 + tmp72
  tmp2375 = tmp2374*tmp243
  tmp2376 = tmp1634 + tmp1635 + tmp187 + tmp2369 + tmp2371 + tmp2373 + tmp2375
  tmp2377 = tmp2376*tmp48*tmp698
  tmp2378 = 13*tmp1650
  tmp2379 = -29*tmp1650
  tmp2380 = tmp1980/tmp54
  tmp2381 = -45*tmp83
  tmp2382 = tmp1734/tmp54
  tmp2383 = tmp2346*tmp632
  tmp2384 = -15*tmp83
  tmp2385 = -26*tmp82
  tmp2386 = 31*tmp83
  tmp2387 = -105*tmp1650
  tmp2388 = 56*tmp1422
  tmp2389 = 113*tmp1422
  tmp2390 = -32*tmp82
  tmp2391 = tmp1746/tmp10
  tmp2392 = -112*tmp1422
  tmp2393 = -9*tmp82
  tmp2394 = 91*tmp1422
  tmp2395 = 616*tmp72
  tmp2396 = 21*tmp641
  tmp2397 = 27*tmp61
  tmp2398 = tmp2356*tmp70
  tmp2399 = -26*tmp1650
  tmp2400 = 63*tmp1422
  tmp2401 = 182*tmp72
  tmp2402 = tmp61*tmp909
  tmp2403 = 27*tmp641
  tmp2404 = 823*tmp1422
  tmp2405 = tmp2016/tmp54
  tmp2406 = 114*tmp83
  tmp2407 = 443*tmp1422
  tmp2408 = 53*tmp1560
  tmp2409 = -5*tt
  tmp2410 = tmp2409 + tmp253
  tmp2411 = -8*tmp2410*tmp32
  tmp2412 = tmp171*tmp6
  tmp2413 = tmp48*tmp698
  tmp2414 = tmp1827/tmp11
  tmp2415 = tmp2414 + tmp398 + tmp546
  tmp2416 = 4*tmp2415*tmp4
  tmp2417 = tmp402 + tmp405 + tmp459 + tmp729
  tmp2418 = tmp2417*tmp966
  tmp2419 = tmp2411 + tmp2412 + tmp2413 + tmp2416 + tmp2418 + tmp254 + tmp31 + tmp&
            &93 + tmp934
  tmp2420 = 13/tmp55
  tmp2421 = 78/tmp54
  tmp2422 = tmp1846 + tmp2
  tmp2423 = tmp1719**2
  tmp2424 = tmp684/tmp9
  tmp2425 = (-6*m2)/(tmp100*tmp9)
  tmp2426 = -6*tmp1535
  tmp2427 = -8*tmp93
  tmp2428 = 12*tmp4
  tmp2429 = -16*m2*tt
  tmp2430 = tmp2428 + tmp2429 + tmp41
  tmp2431 = (-6*m2)/tmp100
  tmp2432 = tmp2431 + tmp40 + tmp452 + tmp5 + tmp684
  tmp2433 = tmp4*tmp824
  tmp2434 = tmp2431/tmp55
  tmp2435 = tmp160*tt
  tmp2436 = tmp176 + tmp2435
  tmp2437 = (12*tmp21*tmp62*tmp86)/tmp11
  tmp2438 = tmp2175*tmp73
  tmp2439 = tmp103 + tmp149 + tmp767
  tmp2440 = tmp2439*tmp74
  tmp2441 = tmp151 + tmp2249 + tmp879 + tmp922
  tmp2442 = tmp2441*tmp781
  tmp2443 = tmp70*tmp916
  tmp2444 = tmp1191 + tmp1653 + tmp2443 + tmp675 + tmp886
  tmp2445 = tmp2444/tmp55
  tmp2446 = tmp2438 + tmp2440 + tmp2442 + tmp2445
  tmp2447 = tmp2446*tt
  tmp2448 = tmp2437 + tmp2447
  tmp2449 = 128*tmp2448*tmp272*tmp3
  tmp2450 = tmp103*tmp2175
  tmp2451 = tmp1534 + tmp218 + tmp73
  tmp2452 = tmp2451*tmp74
  tmp2453 = tmp1650 + tmp193 + tmp2251 + tmp879
  tmp2454 = tmp2453*tmp781
  tmp2455 = tmp2193*tmp83
  tmp2456 = tmp2146*tmp61
  tmp2457 = tmp2139 + tmp2252 + tmp2455 + tmp2456 + tmp915
  tmp2458 = tmp2457/tmp55
  tmp2459 = tmp2450 + tmp2452 + tmp2454 + tmp2458
  tmp2460 = tmp2414*tmp2459*tmp3
  tmp2461 = -4*tmp83
  tmp2462 = 82*tmp82
  tmp2463 = 170*tmp642
  tmp2464 = tmp2175*tmp384
  tmp2465 = tmp1534 + tmp369 + tmp80
  tmp2466 = tmp107*tmp2465
  tmp2467 = -133*tmp1422
  tmp2468 = tmp1766 + tmp2257 + tmp2467 + tmp917
  tmp2469 = tmp2468*tmp60
  tmp2470 = 18*tmp641
  tmp2471 = -95*tmp61*tmp70
  tmp2472 = tmp1131 + tmp1626 + tmp2258 + tmp2470 + tmp2471
  tmp2473 = tmp2472/tmp55
  tmp2474 = tmp2464 + tmp2466 + tmp2469 + tmp2473
  tmp2475 = tmp1560*tmp244*tmp2474
  tmp2476 = tmp144 + tmp316
  tmp2477 = tmp614/tmp10
  tmp2478 = -80*tmp72
  tmp2479 = tmp1063/tmp54
  tmp2480 = tmp987/tmp55
  tmp2481 = -17/(tmp10*tmp55)
  tmp2482 = 2*tmp74
  tmp2483 = tmp189 + tmp194
  tmp2484 = tmp2369*tmp632
  tmp2485 = tmp238 + tmp369 + tmp849
  tmp2486 = tmp2485*tmp74
  tmp2487 = -26*tmp1422
  tmp2488 = tmp2283 + tmp2487 + tmp83 + tmp914
  tmp2489 = tmp2488*tmp781
  tmp2490 = -44*tmp61*tmp70
  tmp2491 = tmp1032*tmp82
  tmp2492 = tmp1641 + tmp1923 + tmp2490 + tmp2491 + tmp669
  tmp2493 = tmp2492/tmp55
  tmp2494 = tmp2484 + tmp2486 + tmp2489 + tmp2493
  tmp2495 = tmp244*tmp2494*tmp738
  tmp2496 = -140*tmp1650
  tmp2497 = -26*tmp643
  tmp2498 = -147*tmp72
  tmp2499 = -92*tmp643
  tmp2500 = 1/tmp10 + tmp969
  tmp2501 = 64*tmp61
  tmp2502 = 63*tmp83
  tmp2503 = tmp2268*tmp74
  tmp2504 = 48*tmp82
  tmp2505 = -122*tmp1422
  tmp2506 = -8*tmp61
  tmp2507 = -173*tmp1650
  tmp2508 = 8*tmp643
  tmp2509 = -19*tmp83
  tmp2510 = 35*tmp1422
  tmp2511 = -32*tmp643
  tmp2512 = 3*tmp2270
  tmp2513 = tmp1741/tmp10
  tmp2514 = 98*tmp82
  tmp2515 = -16*tmp643
  tmp2516 = 36*tmp83
  tmp2517 = 317*tmp1422
  tmp2518 = 29*tmp70
  tmp2519 = 58*tmp82
  tmp2520 = 11*tmp6
  tmp2521 = 13*tmp1535
  tmp2522 = -8*tmp44
  tmp2523 = tmp60/tmp10
  tmp2524 = tmp135 + tmp61 + tmp77
  tmp2525 = tmp2524/tmp55
  tmp2526 = tmp2175 + tmp2523 + tmp2525
  tmp2527 = 46/tmp55
  tmp2528 = 47/tmp54
  tmp2529 = 38/tmp54
  tmp2530 = tmp1063 + tmp189 + tmp24
  tmp2531 = (3*tmp1535)/tmp9
  tmp2532 = tmp117 + tmp1573 + tmp987
  tmp2533 = tmp158*tmp3
  tmp2534 = tmp2533 + tmp988
  tmp2535 = -(tmp2534/tmp99)
  tmp2536 = tmp2199 + tmp2535
  tmp2537 = tmp1479 + tmp63
  tmp2538 = -7/tmp10
  tmp2539 = tmp966 + tt
  tmp2540 = tmp2539**2
  tmp2541 = tmp1777/tmp11
  tmp2542 = tmp2435/tmp9
  tmp2543 = tmp2541 + tmp2542
  tmp2544 = tmp2543/tmp10
  tmp2545 = tmp2544 + tmp467
  tmp2546 = (tmp1584*tmp175*tmp242*tmp949*tt)/(tmp54*tmp55)
  tmp2547 = tmp1437*tmp2214*tmp85
  tmp2548 = tmp2546 + tmp2547
  tmp2549 = m2*tmp1058
  tmp2550 = tmp243/tmp11
  tmp2551 = tmp243*tt
  tmp2552 = tmp690/tmp54
  tmp2553 = tmp2549 + tmp2550 + tmp2551 + tmp2552
  tmp2554 = -115*tmp1650
  tmp2555 = -124*tmp82
  tmp2556 = 19*tmp82
  tmp2557 = 141*tmp83
  tmp2558 = 76*tmp82
  tmp2559 = -281*tmp1650
  tmp2560 = -60*tmp643
  tmp2561 = -95*tmp70
  tmp2562 = 89*tmp83
  tmp2563 = 178*tmp83
  tmp2564 = 705*tmp1422
  tmp2565 = tmp1956*tmp61
  tmp2566 = 48*tmp61
  tmp2567 = -42*tmp70
  tmp2568 = -8*tmp83
  tmp2569 = -274*tmp1422
  tmp2570 = 48*tmp641
  tmp2571 = -242*tmp642
  tmp2572 = -24/tmp10
  tmp2573 = -18*tmp1650
  tmp2574 = 40*tmp1422
  tmp2575 = -21*tmp82
  tmp2576 = -63*tmp72
  tmp2577 = 44*tmp70
  tmp2578 = tmp1610/tmp10
  tmp2579 = 41*tmp70
  tmp2580 = 37*tmp83
  tmp2581 = -71*tmp70
  tmp2582 = 69*tmp83
  tmp2583 = 69*tmp82
  tmp2584 = 69*tmp641
  tmp2585 = -6*tmp83
  tmp2586 = 55*tmp82
  tmp2587 = (-85*tmp83)/tmp10
  tmp2588 = 173*tmp61*tmp70
  tmp2589 = -8*tmp643
  tmp2590 = tmp1608/tmp54
  tmp2591 = 407*tmp61*tmp70
  tmp2592 = tmp2506*tmp632*tmp70
  tmp2593 = 131*tmp1422
  tmp2594 = tmp1004/tmp54
  tmp2595 = -304*tmp1650
  tmp2596 = 246*tmp1422
  tmp2597 = tmp1575*tmp70
  tmp2598 = 117*tmp1650
  tmp2599 = 10/tmp11
  tmp2600 = 8*tmp5
  tmp2601 = 108*tmp1535
  tmp2602 = 35*tmp48
  tmp2603 = tmp2599 + tt
  tmp2604 = -32*tmp2603*tmp32
  tmp2605 = 11*tmp36
  tmp2606 = tmp2600 + tmp2605 + tmp41
  tmp2607 = 16*tmp2606*tmp4
  tmp2608 = tmp2206 + tmp46
  tmp2609 = tmp2608*tmp395
  tmp2610 = 36*tmp1560
  tmp2611 = 30*tmp1535
  tmp2612 = tmp2116 + tmp2209 + tmp2610 + tmp2611
  tmp2613 = tmp2612*tmp966
  tmp2614 = tmp2604 + tmp2607 + tmp2609 + tmp2613 + tmp551
  tmp2615 = 13*tt
  tmp2616 = 24*tmp44
  tmp2617 = 16*tmp44
  tmp2618 = 16*tmp4
  tmp2619 = tmp709*tt
  tmp2620 = tmp1/tmp100
  tmp2621 = tmp2618 + tmp2620 + tmp697
  tmp2622 = 6*tmp36
  tmp2623 = tmp175*tmp1785*tmp242
  tmp2624 = (tmp2188*tmp695)/tmp10
  tmp2625 = tmp2623 + tmp2624
  tmp2626 = -128*tmp32*tmp819
  tmp2627 = (tmp1693*tmp819)/tmp11
  tmp2628 = -8*m2*tmp37*tt
  tmp2629 = tmp1546 + tmp2622 + tmp5
  tmp2630 = tmp2618*tmp2629
  tmp2631 = tmp2626 + tmp2627 + tmp2628 + tmp2630 + tmp551
  tmp2632 = tmp1437*tmp175*tmp242
  tmp2633 = (tmp2188*tmp708)/tmp9
  tmp2634 = tmp2632 + tmp2633
  tmp2635 = 9/tmp55
  tmp2636 = 1/tmp55 + tmp580 + tmp78
  tmp2637 = -3/tmp55
  tmp2638 = -3/tmp54
  tmp2639 = tmp1574 + 1/tmp54
  tmp2640 = tmp516/tmp55
  tmp2641 = tmp628 + tmp80 + tmp92
  tmp2642 = tmp2641*tmp60
  tmp2643 = tmp135*tmp629
  tmp2644 = tmp151 + tmp657 + tmp83 + tmp856
  tmp2645 = tmp2644/tmp55
  tmp2646 = tmp2642 + tmp2643 + tmp2645 + tmp413
  tmp2647 = -27*tmp72
  tmp2648 = -20*tmp70
  tmp2649 = -47*tmp1650
  tmp2650 = 20*tmp82
  tmp2651 = tmp353/tmp10
  tmp2652 = 40*tmp83
  tmp2653 = -44*tmp1650
  tmp2654 = -41*tmp1422
  tmp2655 = tmp1880*tmp70
  tmp2656 = 33*tmp1650
  tmp2657 = 5*tmp641
  tmp2658 = -146*tmp1650
  tmp2659 = 133*tmp1422
  tmp2660 = -30*tmp82
  tmp2661 = 15*tmp82
  tmp2662 = 61*tmp82
  tmp2663 = 153*tmp641
  tmp2664 = 114*tmp72
  tmp2665 = -59*tmp70
  tmp2666 = -205*tmp1650
  tmp2667 = -38*tmp1422
  tmp2668 = 21*tmp82
  tmp2669 = -19*tmp1422
  tmp2670 = tmp144 + tmp76
  tmp2671 = 9*tmp1650
  tmp2672 = tmp1814/tmp55
  tmp2673 = -15/tmp10
  tmp2674 = 28*tmp72
  tmp2675 = -42*tmp1650
  tmp2676 = tmp1531/tmp54
  tmp2677 = tmp632*tmp70*tmp766
  tmp2678 = -17*tmp83
  tmp2679 = 44*tmp82
  tmp2680 = -11*tmp643
  tmp2681 = tmp798/tmp10
  tmp2682 = -16*tmp83
  tmp2683 = tmp234*tmp83
  tmp2684 = tmp1746*tmp632*tmp70
  tmp2685 = -37*tmp1650
  tmp2686 = 13*tmp1422
  tmp2687 = 15*tmp641
  tmp2688 = -7*tmp643
  tmp2689 = tmp113 + tmp354 + tmp92
  tmp2690 = 11*tmp1650
  tmp2691 = tmp2283*tmp632
  tmp2692 = -34*tmp82
  tmp2693 = 82*tmp83
  tmp2694 = 17*tmp82
  tmp2695 = tmp2665*tmp61*tmp632
  tmp2696 = 59*tmp82
  tmp2697 = -58*tmp83
  tmp2698 = tmp1728/tmp54
  tmp2699 = -59*tmp643
  tmp2700 = tmp2191/tmp10
  tmp2701 = 40*tmp82
  tmp2702 = -39*tmp83
  tmp2703 = 65*tmp82
  tmp2704 = 2*tmp642
  tmp2705 = tmp628 + tmp70 + tmp80
  tmp2706 = 15*tmp5
  tmp2707 = tmp1696 + tmp1834 + tmp2706
  tmp2708 = -11*tmp48
  tmp2709 = 17/tmp55
  tmp2710 = 61/tmp54
  tmp2711 = 59/tmp54
  tmp2712 = 26/tmp55
  tmp2713 = (-128*tmp32)/tmp100
  tmp2714 = tmp2707*tmp684
  tmp2715 = -34*tmp44
  tmp2716 = -30*tmp1560
  tmp2717 = 18*tmp1535
  tmp2718 = 22*tmp48
  tmp2719 = tmp2715 + tmp2716 + tmp2717 + tmp2718
  tmp2720 = m2*tmp2719
  tmp2721 = tmp2179 + tmp438 + tmp738 + tmp89
  tmp2722 = 3*tmp2721
  tmp2723 = tmp2713 + tmp2714 + tmp2720 + tmp2722
  tmp2724 = tmp2172/tmp100
  tmp2725 = -8*tmp2707*tmp4
  tmp2726 = 17*tmp44
  tmp2727 = -9*tmp1535
  tmp2728 = tmp2708 + tmp2726 + tmp2727 + tmp403
  tmp2729 = tmp241*tmp2728
  tmp2730 = -tmp89
  tmp2731 = tmp48/tmp11
  tmp2732 = tmp2730 + tmp2731 + tmp437 + tmp93
  tmp2733 = 3*tmp2732
  tmp2734 = tmp2724 + tmp2725 + tmp2729 + tmp2733
  tmp2735 = tmp44 + tmp46 + tmp49
  tmp2736 = -512*tmp30*tmp410
  tmp2737 = (-3*tmp2735*tmp410)/tmp11
  tmp2738 = 19*tmp44
  tmp2739 = 50*tmp1560
  tmp2740 = 41*tmp1535
  tmp2741 = 12*tmp48
  tmp2742 = tmp2738 + tmp2739 + tmp2740 + tmp2741
  tmp2743 = tmp2742*tmp714
  tmp2744 = 32*tmp89
  tmp2745 = 109*tmp738
  tmp2746 = 117*tmp5*tmp6
  tmp2747 = 49*tmp2731
  tmp2748 = 5*tmp93
  tmp2749 = tmp2744 + tmp2745 + tmp2746 + tmp2747 + tmp2748
  tmp2750 = -8*tmp2749*tmp4
  tmp2751 = 23*tmp143
  tmp2752 = 128*tmp44*tmp6
  tmp2753 = 70*tmp48*tmp5
  tmp2754 = tmp1452*tmp93
  tmp2755 = tmp156 + tmp2071 + tmp2751 + tmp2752 + tmp2753 + tmp2754
  tmp2756 = tmp241*tmp2755
  tmp2757 = tmp2736 + tmp2737 + tmp2743 + tmp2750 + tmp2756
  tmp2758 = tmp1702*tmp69
  tmp2759 = -tmp709
  tmp2760 = tmp2758 + tmp2759
  tmp2761 = tmp316 + tmp63 + tmp824
  tmp2762 = tmp1063 + tmp831
  tmp2763 = (tmp2419*tmp522)/tmp9
  tmp2764 = tmp1447*tmp408
  tmp2765 = tmp2763 + tmp2764
  tmp2766 = tmp713*tmp988
  tmp2767 = -tmp177
  tmp2768 = Pi**2
  tmp2769 = -(betas*betau)
  tmp2770 = 6*betas*betat
  tmp2771 = -5*betat*betau
  tmp2772 = tmp2769 + tmp2770 + tmp2771
  tmp2773 = -betau
  tmp2774 = betas + tmp2773
  tmp2775 = betas*betau
  tmp2776 = 3*betas*betat
  tmp2777 = -2*tmp2775
  tmp2778 = tmp348/tmp10
  tmp2779 = tmp389*tmp61
  tmp2780 = 2*betas*betat
  tmp2781 = -2*betat*betau
  tmp2782 = 9*tmp2775
  tmp2783 = -22*betat*betau
  tmp2784 = betas*betat
  tmp2785 = 7*tmp2775
  tmp2786 = betat + betau
  tmp2787 = 8*betat*betau
  tmp2788 = -24*tmp1422
  tmp2789 = 11*tmp413
  tmp2790 = 24*tmp1422
  tmp2791 = tmp108 + tmp931
  tmp2792 = tmp418*tmp48
  tmp2793 = 50*tmp70
  tmp2794 = 50*tmp643
  tmp2795 = 12*betat*betau
  tmp2796 = 9*tmp2784
  tmp2797 = 7*betat
  tmp2798 = 18*betau
  tmp2799 = tmp2797 + tmp2798
  tmp2800 = betas*tmp2797
  tmp2801 = -12*betat*betau
  tmp2802 = 8*tmp2784
  tmp2803 = -40*betat*betau
  tmp2804 = 13*tmp2784
  tmp2805 = 58*betat*betau
  tmp2806 = 208*betat*betau
  tmp2807 = 49*betat
  tmp2808 = 11*tmp2784
  tmp2809 = 4*betat*betau
  tmp2810 = 5*tmp2784
  tmp2811 = 58*tmp83
  tmp2812 = 29*tmp641
  tmp2813 = tmp2385/tmp54
  tmp2814 = (-31*tmp83)/tmp10
  tmp2815 = 12*tmp2784
  tmp2816 = -3*tmp2775
  tmp2817 = 4*tmp2784
  tmp2818 = -14*tmp2775
  tmp2819 = -6*betat*betau
  tmp2820 = 3*tmp2775
  tmp2821 = betat*betau
  tmp2822 = 57*tmp2775
  tmp2823 = -3*tmp2821
  tmp2824 = tmp2769 + tmp2817 + tmp2823
  tmp2825 = -4*tmp2775
  tmp2826 = -9*tmp2821
  tmp2827 = tmp2804 + tmp2825 + tmp2826
  tmp2828 = -39*tmp2775
  tmp2829 = -4*tmp2821
  tmp2830 = tmp2769 + tmp2810 + tmp2829
  tmp2831 = tmp2824/tmp54
  tmp2832 = -13*tmp2775
  tmp2833 = -29*tmp2775
  tmp2834 = tmp2777 + tmp2802 + tmp2819
  tmp2835 = -58*tmp2784
  tmp2836 = -124*tmp2821
  tmp2837 = -37*tmp2775
  tmp2838 = betat + tmp2773
  tmp2839 = -betat
  tmp2840 = betau + tmp2839
  tmp2841 = tmp1422*tmp17
  tmp2842 = 31*tmp413
  tmp2843 = -79*tmp61
  tmp2844 = 141*tmp70
  tmp2845 = 21*tmp70
  tmp2846 = -280*tmp82
  tmp2847 = (113*tmp83)/tmp10
  tmp2848 = 140*tmp643
  tmp2849 = 90*tmp2784
  tmp2850 = -8*tmp2821
  tmp2851 = 19*betat
  tmp2852 = 100*betau
  tmp2853 = tmp2851 + tmp2852
  tmp2854 = -180*tmp2821
  tmp2855 = 27*tmp2775
  tmp2856 = 300*tmp2775
  tmp2857 = 57*tmp2784
  tmp2858 = betas*tmp2851
  tmp2859 = 9*betas
  tmp2860 = 9*betat
  tmp2861 = 43*betau
  tmp2862 = tmp2860 + tmp2861
  tmp2863 = 129*tmp2775
  tmp2864 = 124*tmp2784
  tmp2865 = 15*tmp2775
  tmp2866 = 2*betat
  tmp2867 = 13*betau
  tmp2868 = tmp2866 + tmp2867
  tmp2869 = betau*tmp2866
  tmp2870 = 39*tmp2775
  tmp2871 = 96*tmp1422
  tmp2872 = tmp226 + tmp286 + tmp420
  tmp2873 = tmp2872*tmp636
  tmp2874 = -29*tmp61
  tmp2875 = -29*tmp1422
  tmp2876 = 38*tmp82
  tmp2877 = -295*tmp72
  tmp2878 = -65*tmp70
  tmp2879 = -553*tmp1650
  tmp2880 = 15*tmp413
  tmp2881 = 114*tmp641
  tmp2882 = 25*betas
  tmp2883 = 366*tmp2784
  tmp2884 = -47*tmp2821
  tmp2885 = 10*betau
  tmp2886 = betat + tmp2885
  tmp2887 = -588*tmp2821
  tmp2888 = 17*betat
  tmp2889 = 56*tmp2775
  tmp2890 = 90*betau
  tmp2891 = tmp2888 + tmp2890
  tmp2892 = betas*tmp2888
  tmp2893 = 270*tmp2775
  tmp2894 = -1488*tmp2821
  tmp2895 = 39*tmp2784
  tmp2896 = 93*betau
  tmp2897 = tmp2888 + tmp2896
  tmp2898 = -1116*tmp2821
  tmp2899 = 34*tmp2784
  tmp2900 = 279*tmp2775
  tmp2901 = 11*betat
  tmp2902 = 18*tmp2821
  tmp2903 = 449*tmp2784
  tmp2904 = 23*betau
  tmp2905 = tmp2901 + tmp2904
  tmp2906 = betau*tmp2860
  tmp2907 = 69*tmp2775
  tmp2908 = 53*tmp83
  tmp2909 = -114*tmp1650
  tmp2910 = tmp2365/tmp54
  tmp2911 = -91*tmp82
  tmp2912 = 242*tmp82
  tmp2913 = -34*tmp1422
  tmp2914 = -33*tmp1422
  tmp2915 = tmp674/tmp54
  tmp2916 = 247*tmp82
  tmp2917 = 725*tmp61
  tmp2918 = 98*tmp61
  tmp2919 = 40*tmp70
  tmp2920 = tmp2528*tmp70
  tmp2921 = -235*tmp61
  tmp2922 = -5*betau
  tmp2923 = 336*tmp2784
  tmp2924 = 33*tmp2784
  tmp2925 = 16*betat
  tmp2926 = 315*betau
  tmp2927 = tmp2925 + tmp2926
  tmp2928 = -429*tmp2821
  tmp2929 = 945*tmp2775
  tmp2930 = 45*betat
  tmp2931 = 311*betau
  tmp2932 = tmp2930 + tmp2931
  tmp2933 = betas*tmp2930
  tmp2934 = -1545*tmp2821
  tmp2935 = 933*tmp2775
  tmp2936 = 13*betat
  tmp2937 = 72*betau
  tmp2938 = tmp2936 + tmp2937
  tmp2939 = -3954*tmp2821
  tmp2940 = 31*betat
  tmp2941 = 91*betau
  tmp2942 = tmp2940 + tmp2941
  tmp2943 = betas*tmp2940
  tmp2944 = -1850*tmp2821
  tmp2945 = 273*tmp2775
  tmp2946 = 122*tmp2821
  tmp2947 = 3*betat
  tmp2948 = 4*betau
  tmp2949 = tmp2947 + tmp2948
  tmp2950 = 275*tmp2821
  tmp2951 = 2*betau
  tmp2952 = betat + tmp2951
  tmp2953 = 66*tmp61
  tmp2954 = -93*tmp1650
  tmp2955 = -19*tmp82
  tmp2956 = -74*tmp1650
  tmp2957 = 66*tmp641
  tmp2958 = 26*tmp82
  tmp2959 = 111*tmp1650
  tmp2960 = -100*tmp1422
  tmp2961 = 130*tmp61*tmp70
  tmp2962 = -13*tmp643
  tmp2963 = 23*tmp413
  tmp2964 = tmp2356/tmp10
  tmp2965 = -36*tmp70
  tmp2966 = -158*tmp1422
  tmp2967 = 36*tmp82
  tmp2968 = 252*tmp82
  tmp2969 = 424*tmp61
  tmp2970 = 121*tmp70
  tmp2971 = 326*tmp61
  tmp2972 = -129*tmp72
  tmp2973 = -128*tmp82
  tmp2974 = -8*tmp2784
  tmp2975 = 6*tmp2821
  tmp2976 = tmp2820 + tmp2974 + tmp2975
  tmp2977 = -73*tmp2821
  tmp2978 = -70*tmp2784
  tmp2979 = 67*tmp2821
  tmp2980 = 299*tmp2775
  tmp2981 = 54*tmp2775
  tmp2982 = 8*tmp2775
  tmp2983 = betau*tmp2839
  tmp2984 = -74*tmp2784
  tmp2985 = 59*tmp2821
  tmp2986 = tmp2855 + tmp2984 + tmp2985
  tmp2987 = -197*tmp2784
  tmp2988 = 71*tmp2775
  tmp2989 = 158*tmp2821
  tmp2990 = tmp2987 + tmp2988 + tmp2989
  tmp2991 = 150*tmp2775
  tmp2992 = -231*tmp2784
  tmp2993 = 75*tmp2775
  tmp2994 = 197*tmp2821
  tmp2995 = tmp2992 + tmp2993 + tmp2994
  tmp2996 = -25*tmp2775
  tmp2997 = 250*tmp2821
  tmp2998 = betas*tmp2951
  tmp2999 = -134*tmp2784
  tmp3000 = 50*tmp2775
  tmp3001 = 114*tmp2821
  tmp3002 = tmp2999 + tmp3000 + tmp3001
  tmp3003 = 22*tmp2821
  tmp3004 = 41*tmp2821
  tmp3005 = betas*tmp2786
  tmp3006 = -38*tmp2784
  tmp3007 = 26*tmp2775
  tmp3008 = tmp3003 + tmp3006 + tmp3007
  tmp3009 = betas*tmp2838*tmp76
  tmp3010 = -31*tmp2775
  tmp3011 = betau*tmp2936
  tmp3012 = betas*tmp2838*tmp354
  tmp3013 = -23*tmp2775
  tmp3014 = 21*tmp2821
  tmp3015 = betas*tmp195*tmp2838
  tmp3016 = 29*tmp2821
  tmp3017 = -58*tmp2821
  tmp3018 = -51*tmp2775
  tmp3019 = 337*tmp61
  tmp3020 = -709*tmp72
  tmp3021 = -127*tmp72
  tmp3022 = 232*tmp61
  tmp3023 = -64*tmp61
  tmp3024 = 36*tmp641
  tmp3025 = 49*betau
  tmp3026 = betat + tmp3025
  tmp3027 = -338*tmp2821
  tmp3028 = 320*tmp2784
  tmp3029 = 3*betas
  tmp3030 = 73*tmp2784
  tmp3031 = 6*betat
  tmp3032 = 61*betau
  tmp3033 = tmp3031 + tmp3032
  tmp3034 = 20*tmp2821
  tmp3035 = -1651*tmp2821
  tmp3036 = 183*tmp2775
  tmp3037 = betat*tmp2885
  tmp3038 = -((betas*tmp2868)/tmp10)
  tmp3039 = 13*betas
  tmp3040 = 8*betat
  tmp3041 = betau*tmp2925
  tmp3042 = -3025*tmp2821
  tmp3043 = 86*tmp2784
  tmp3044 = 29*betau
  tmp3045 = tmp3040 + tmp3044
  tmp3046 = -2541*tmp2821
  tmp3047 = betas*tmp2925
  tmp3048 = 87*tmp2775
  tmp3049 = 5*betat
  tmp3050 = 9*betau
  tmp3051 = tmp3049 + tmp3050
  tmp3052 = -813*tmp2821
  tmp3053 = -(tmp3005/tmp10)
  tmp3054 = -261*tmp2775
  tmp3055 = betau*tmp2947
  tmp3056 = betat*tmp2937
  tmp3057 = -114*tmp2775
  tmp3058 = 519*tmp2775
  tmp3059 = -75*tmp2775
  tmp3060 = 84*tmp641
  tmp3061 = -44*tmp643
  tmp3062 = tmp1741*tmp632*tmp70
  tmp3063 = -147*tmp1422
  tmp3064 = 147*tmp1650
  tmp3065 = (-129*tmp83)/tmp10
  tmp3066 = -10*tmp643
  tmp3067 = 127*tmp413
  tmp3068 = 131*tmp70
  tmp3069 = 13*tmp413
  tmp3070 = 171*tmp413
  tmp3071 = tmp2015/tmp10
  tmp3072 = 202*tmp82
  tmp3073 = -149*tmp70
  tmp3074 = tmp2965/tmp54
  tmp3075 = 10*tmp643
  tmp3076 = 80*tmp2784
  tmp3077 = -70*tmp2821
  tmp3078 = 37*tmp2775
  tmp3079 = -210*tmp2821
  tmp3080 = 124*tmp2821
  tmp3081 = (betat*tmp2774)/tmp54
  tmp3082 = -236*tmp2775
  tmp3083 = -471*tmp2821
  tmp3084 = 18*betas
  tmp3085 = 15*tmp2784
  tmp3086 = betat + tmp2798
  tmp3087 = -254*tmp2775
  tmp3088 = 2*betas
  tmp3089 = betat + tmp3050
  tmp3090 = -1165*tmp2821
  tmp3091 = betau*tmp3049
  tmp3092 = -4*betau
  tmp3093 = betas + tmp3092
  tmp3094 = betat + tmp2948
  tmp3095 = -1413*tmp2821
  tmp3096 = 6*tmp2775
  tmp3097 = -453*tmp2775
  tmp3098 = 5*betas
  tmp3099 = -26*tmp2821
  tmp3100 = 5*betau
  tmp3101 = tmp2866 + tmp3100
  tmp3102 = -857*tmp2821
  tmp3103 = 36*tmp2821
  tmp3104 = -100*tmp2821
  tmp3105 = 18*betat
  tmp3106 = tmp2786*tmp3088*tmp82
  tmp3107 = 63*tmp2784
  tmp3108 = 248*tmp2821
  tmp3109 = tmp2776 + tmp2816 + tmp2869
  tmp3110 = 32*tmp2784
  tmp3111 = tmp762 + tmp77 + tmp80
  tmp3112 = tmp151 + tmp2656 + tmp2788 + tmp640
  tmp3113 = (tmp3112*tmp72)/tmp55
  tmp3114 = -betas
  tmp3115 = betat + tmp3114
  tmp3116 = tmp3115*tmp36
  tmp3117 = tmp632/tmp11
  tmp3118 = tmp158*tmp1775
  tmp3119 = tmp3117 + tmp3118
  tmp3120 = tmp135*tmp3119
  tmp3121 = tmp1780 + tmp3120
  tmp3122 = -2*tmp89
  tmp3123 = -4*tmp738
  tmp3124 = 4*betat
  tmp3125 = -4*tmp2784
  tmp3126 = betas/tmp11
  tmp3127 = tmp2839*tt
  tmp3128 = tmp3126 + tmp3127
  tmp3129 = (tmp158*tmp175*tmp705)/(tmp10*tmp55)
  tmp3130 = -384*tmp30
  tmp3131 = 28/tmp11
  tmp3132 = tmp3131 + tmp936
  tmp3133 = 16*tmp3132*tmp32
  tmp3134 = 46*tmp5
  tmp3135 = 41*tmp36
  tmp3136 = -9*tmp6
  tmp3137 = tmp3134 + tmp3135 + tmp3136
  tmp3138 = -4*tmp3137*tmp4
  tmp3139 = tmp1839 + tmp2116 + tmp463
  tmp3140 = tmp3139*tmp69
  tmp3141 = tmp3122 + tmp3123 + tmp3130 + tmp3133 + tmp3138 + tmp3140 + tmp438 + t&
            &mp93
  tmp3142 = tmp3124*tmp4
  tmp3143 = 4*tmp3126
  tmp3144 = (-2*betat)/tmp11
  tmp3145 = betat*tmp2296
  tmp3146 = tmp3143 + tmp3144 + tmp3145
  tmp3147 = m2*tmp3146
  tmp3148 = tmp3116 + tmp3142 + tmp3147
  tmp3149 = tmp241*tmp3128
  tmp3150 = tmp3116 + tmp3149
  tmp3151 = tmp158*tmp949
  tmp3152 = tmp3117 + tmp3151
  tmp3153 = (tmp243*tmp3152)/tmp9
  tmp3154 = tmp1433 + tmp3153
  tmp3155 = (tmp175*tmp3151*tt)/(tmp54*tmp55*tmp9)
  tmp3156 = betat*tmp242*tmp2768*tmp713
  tmp3157 = tmp2768 + tmp2866
  tmp3158 = tmp3157*tmp69
  tmp3159 = tmp2768 + tmp3124
  tmp3160 = tmp3159*tmp97
  tmp3161 = tmp3158 + tmp3160
  tmp3162 = tmp3126*tmp3161
  tmp3163 = tmp3156 + tmp3162
  tmp3164 = tmp2768*tmp3115
  tmp3165 = tmp3125 + tmp3164
  tmp3166 = tmp3165*tmp36
  tmp3167 = tmp2768*tmp3149
  tmp3168 = tmp3166 + tmp3167
  tmp3169 = tmp1*tmp1702
  tmp3170 = tmp3169 + tmp709
  tmp3171 = -7*betau
  tmp3172 = 4*betas
  tmp3173 = -10*betau
  tmp3174 = tmp448*tt
  tmp3175 = 7*betas
  tmp3176 = -11*betau
  tmp3177 = -2*betau
  tmp3178 = 6/tmp11
  tmp3179 = 8*betau
  tmp3180 = tmp2768 + tmp3179
  tmp3181 = tmp241*tmp3126*tmp3180
  tmp3182 = betau*tmp158*tmp2768*tmp713
  tmp3183 = tmp2768 + tmp2948
  tmp3184 = -((tmp3126*tmp3183)/tmp100)
  tmp3185 = tmp3181 + tmp3182 + tmp3184
  tmp3186 = 7*tt
  tmp3187 = tmp3186 + tmp952
  tmp3188 = tmp62/tmp11
  tmp3189 = tmp950*tt
  tmp3190 = tmp3188 + tmp3189
  tmp3191 = (tmp3190*tmp85)/tmp9
  tmp3192 = tmp3191 + tmp68
  tmp3193 = (tmp1584*tmp3189*tmp723)/tmp54
  tmp3194 = tmp1551*tmp5
  tmp3195 = 5*tmp2731
  tmp3196 = -16*tmp3187*tmp32
  tmp3197 = 47*tmp36
  tmp3198 = tmp1834 + tmp3197 + tmp955
  tmp3199 = 4*tmp3198*tmp4
  tmp3200 = 12*tmp44
  tmp3201 = tmp2217*tmp5
  tmp3202 = tmp3200 + tmp3201 + tmp48 + tmp724
  tmp3203 = tmp1*tmp3202
  tmp3204 = tmp1714 + tmp3194 + tmp3195 + tmp3196 + tmp3199 + tmp3203 + tmp529 + t&
            &mp551 + tmp93
  tmp3205 = (-32*tmp4)/tmp100
  tmp3206 = tmp37*tmp69
  tmp3207 = tmp2619 + tmp3205 + tmp3206 + tmp543
  tmp3208 = 96*tmp32
  tmp3209 = tmp3178 + tmp443
  tmp3210 = -8*tmp3209*tmp4
  tmp3211 = 6*tmp5
  tmp3212 = tmp2622 + tmp3211 + tmp535
  tmp3213 = m2*tmp3212
  tmp3214 = tmp2619 + tmp3208 + tmp3210 + tmp3213
  tmp3215 = -tmp250
  tmp3216 = 1/tmp10 + tmp2637 + tmp2638
  tmp3217 = tmp1585/tmp11
  tmp3218 = tmp158*tmp2767
  tmp3219 = tmp176 + tmp2767
  tmp3220 = tmp452*tmp7
  tmp3221 = tmp518/tmp55
  tmp3222 = betau*tmp3172
  tmp3223 = tmp2768*tmp2774
  tmp3224 = tmp3222 + tmp3223
  tmp3225 = tmp1068/tmp11
  tmp3226 = tmp3*tmp518
  NTSOFT = 256*discbtlogs25*tmp10*tmp16*tmp3192*tmp4*tmp52*tmp55*tmp56*tmp65*tmp67*tmp9 - 2&
            &56*discbslogs15*tmp10*tmp11*tmp13*tmp14*tmp16*tmp29*tmp4*tmp52*tmp53*tmp54*tmp7*&
            &tmp9 + 256*discbslogs25*tmp10*tmp11*tmp13*tmp14*tmp16*tmp29*tmp4*tmp52*tmp55*tmp&
            &56*tmp7*tmp9 + (discbslogs35*tmp11*tmp13*tmp14*tmp240*tmp357*tmp54*tmp55*tmp59*t&
            &mp9*(-256*tmp1506*tmp157 - 256*tmp1475*tmp157*tmp158*tmp7 + (256*tmp157*tmp713*(&
            &(tmp158*tmp175*tmp2542)/tmp10 + tmp249*tmp945))/(tmp10*tmp11*tmp96) + 256*tmp157&
            &*tmp158*(16*tmp30*tmp7*(tmp1012 + tmp44*(tmp60*(229/tmp10 + tmp832) + (-180*tmp7&
            &2 + tmp833 + tmp834)/tmp55 + (-77*tmp61 + 103*tmp72 + tmp835)/tmp10) - 2*tmp1535&
            &*((tmp112 + tmp384 + tmp426)/tmp55 + (tmp316 + tmp333)*tmp60 + tmp144*tmp838) + &
            &2*tmp1560*(16*tmp268 + (tmp351 + tmp836 + tmp837)/tmp55 + (tmp2964 - 46*tmp61 + &
            &tmp92)/tmp10)) + (tmp44*tmp7*(tmp2178*(tmp372 + tmp374 + tmp804) + tmp3211*tmp6*&
            &(tmp373 + tmp659 + tmp840) + 2*tmp2731*(tmp1977 + tmp783 + tmp867) + 5*tmp1777*t&
            &mp89 + tmp845*tmp93))/tmp10 + (tmp2178*(tmp1063*(tmp184 + tmp606 + tmp61) + tmp8&
            &42 + tmp373*tmp843) + tmp3*tmp89*(tmp841 + tmp895/tmp55) + tmp516*tmp6*((tmp155 &
            &+ tmp654 + tmp660)/tmp55 + (tmp1813*tmp86)/tmp10 + tmp60*tmp905) + tmp128*tmp273&
            &1*(tmp2193/tmp55 + 9*tmp840 + tmp908) + tmp144*tmp845*tmp93)*tmp966*tmp971 + 4*t&
            &mp4*(tmp143*((tmp103 + tmp605 - 23*tmp61)*tmp72 + (154/tmp10 + tmp209)*tmp74 + t&
            &mp781*(tmp306 + tmp432 + tmp857) + (-127*tmp1422 + tmp193 + tmp614 + tmp858)/tmp&
            &55) + tmp868 + tmp690*(tmp74*(120/tmp10 + tmp859) + tmp781*(tmp2665 + tmp860 + t&
            &mp861) + tmp72*(tmp103 + tmp862 + tmp863) + (-51*tmp1422 + tmp193 + tmp864 + tmp&
            &880)/tmp55)*tmp89 + tmp44*tmp6*((tmp1020 + tmp114 + tmp2843)*tmp72 + tmp60*(tmp1&
            &661 - 139*tmp70 + tmp865) + tmp74*(145/tmp10 + tmp926) + (-129*tmp1650 + tmp791 &
            &+ tmp866 + tmp927)/tmp55) + tmp1720*tmp24*(tmp1025 + tmp584 + tmp70 + tmp790 + t&
            &mp867)*tmp93 + (tmp3*tmp624*(tmp1572 + tmp86)*tmp971)/tmp10) + tmp989 - 256*tmp2&
            &72*tmp7*((tmp19*tmp372 + (tmp2140 + tmp224 + tmp422)/tmp55 + (tmp113 + tmp1746 +&
            & tmp73)/tmp10)/tmp11 + tmp991) - 8*tmp32*(tmp1031 - 8*tmp2731*(tmp60*(tmp208 + t&
            &mp420 + tmp80) + tmp854 + tmp72*tmp855 + (tmp655 + tmp82 + tmp83 + tmp856)/tmp55&
            &) + tmp5*tmp6*(tmp60*(tmp308 + tmp330 + tmp605) + tmp74*(tmp631 + tmp808) + tmp7&
            &2*(tmp384 - 35*tmp61 + tmp851) + (tmp639 + tmp655 + tmp852 + tmp858)/tmp55) + ((&
            &268/tmp10 + 65/tmp54)*tmp74 + tmp781*(tmp598 - 128*tmp70 + tmp846) + (-179*tmp14&
            &22 - 207*tmp1650 + tmp639 + tmp847)/tmp55 + tmp72*(tmp384 + tmp793 + tmp862))*tm&
            &p89 + tmp2178*((-160*tmp1650 + tmp2251 + tmp677 + tmp791)/tmp55 + tmp107*(tmp152&
            & + tmp848) + tmp373*(tmp1015 + tmp620 + tmp849) + (tmp1057*(tmp191 + tmp850 + tm&
            &p992))/tmp10)) - 128*tmp252*tmp7*(tmp5*((tmp114 + tmp584 + tmp762)/tmp10 + tmp60&
            &*(tmp108 + tmp827) + (tmp138 + tmp320 + tmp828)/tmp55) + tmp36*(tmp416/tmp55 + t&
            &mp829 + (tmp155 + tmp70 + tmp88)/tmp10) + tmp998))))/8. + (discbulogu*tmp240*tmp&
            &357*tmp411*tmp53*tmp56*tmp58*tmp59*tmp65*((256*tmp15*tmp157*tmp2088*tmp72)/tmp55&
            & + (256*tmp15*tmp157*tmp1777*(tmp2050 + tmp2051 - tmp1447*tmp2077 + (m2*tmp164*t&
            &mp2296*tmp408)/tmp54 - (tmp2077*tmp522)/tmp9))/tmp10 + (256*tmp15*tmp157*tmp177*&
            &(tmp2027 - tmp2049*tmp249 + tmp241*tmp249*tmp408))/(tmp54*tmp55*tmp9) - 256*tmp1&
            &57*(tmp2345*tmp7 + 128*tmp1944*tmp272*tmp7 - 32*tmp252*tmp7*(tmp3*tmp516*(-13*tm&
            &p1906 + (tmp1302 + tmp2255 + tmp2962 + tmp1766/tmp54 + 200*tmp61*tmp70)/tmp55 + &
            &(tmp1946*tmp74)/tmp54 + tmp60*(-89*tmp1422 - 123*tmp1650 + tmp1947 + 73*tmp83)) &
            &+ tmp6*(tmp1955 + (tmp1957*tmp636)/tmp54 + tmp60*(tmp1764 + tmp1975 + 264*tmp642&
            & + 354*tmp61*tmp70 - (459*tmp83)/tmp10) + (tmp1959 - 268*tmp61*tmp70 + tmp1057*t&
            &mp82 + (377*tmp83)/tmp10 + tmp889)/(tmp10*tmp55) + tmp74*(-397*tmp1422 + tmp1958&
            & + 160*tmp83 + tmp90)) + tmp36*(tmp1955 + (tmp1950*tmp636)/tmp54 + tmp60*(tmp176&
            &4 + 175*tmp641 + 422*tmp642 + 992*tmp61*tmp70 - (1077*tmp83)/tmp10) + (tmp1952 +&
            & tmp1953 + tmp1954 - 618*tmp61*tmp70 + tmp889)/(tmp10*tmp55) + tmp74*(-581*tmp16&
            &50 + tmp1951 + 356*tmp83 + tmp90))) + 16*tmp30*tmp7*(tmp1535*(tmp1970 + (365/tmp&
            &10 + 159/tmp54)*tmp636*tmp76 + tmp74*(-1923*tmp1422 - 809*tmp1650 + tmp368 + 624&
            &*tmp83) + tmp60*(tmp1968 + 306*tmp641 + 1169*tmp642 + 1611*tmp61*tmp70 - (2014*t&
            &mp83)/tmp10) + (tmp1626 + tmp1971 + tmp1972 - 1337*tmp61*tmp70 + (1788*tmp83)/tm&
            &p10)/(tmp10*tmp55)) + tmp1560*(tmp1970 + (tmp1967*tmp636)/tmp54 + tmp74*(-2189*t&
            &mp1422 - 1115*tmp1650 + tmp368 + 738*tmp83) + tmp60*(tmp1968 + 363*tmp641 + 1223&
            &*tmp642 + 2169*tmp61*tmp70 - (2454*tmp83)/tmp10) + (tmp1626 + tmp1969 + tmp2258 &
            &- 1551*tmp61*tmp70 + (1924*tmp83)/tmp10)/(tmp10*tmp55)) + tmp3*tmp524*(-6*tmp190&
            &6 + (tmp1962*tmp74)/tmp54 + (tmp1194 + tmp2813 + 64*tmp641 + 313*tmp61*tmp70 - (&
            &345*tmp83)/tmp10)/tmp55 + tmp60*(tmp1642 + tmp1963 + tmp1964 + tmp90)) + tmp48*(&
            &8*tmp1422*tmp1612 + (tmp1974*tmp636)/tmp54 + tmp2482*(tmp1401 - 96*tmp1650 + tmp&
            &385 + 81*tmp83) + tmp60*(tmp1975 + tmp2515 + 324*tmp642 + 420*tmp61*tmp70 - (532&
            &*tmp83)/tmp10) + (tmp1058*(tmp1628 + tmp1920 - 105*tmp61*tmp70 + (140*tmp83)/tmp&
            &10 + tmp915))/tmp10)) + (tmp1629*tmp239*tmp72*(tmp2412*(tmp155 + tmp2481 + tmp37&
            &2 + tmp426 + tmp152/tmp55 + tmp672) + tmp2178*(tmp1976 + tmp426 + tmp502 + tmp47&
            &6/tmp54 + tmp2368/tmp55 + tmp80) + tmp3178*tmp48*(tmp191 + tmp1977 + tmp2479 + t&
            &mp61 + tmp781 + tmp88) + (tmp1767*tmp3)/tmp9 + (tmp105 + tmp2267 + tmp359 + tmp3&
            &73 + tmp2192/tmp55 + tmp80)*tmp93))/tmp55 + (tmp1224*tmp4*tmp7*(tmp529*((76*tmp1&
            &422 - 82*tmp1650 + tmp2586 + tmp652)/tmp55 + tmp60*(tmp351 + tmp514 - 105*tmp70)&
            & + tmp1994*tmp74 + (tmp2568 + tmp2953/tmp10 + tmp2665/tmp54 + tmp82)/tmp10) + tm&
            &p738*((-837*tmp1422 + 1034*tmp1650 + tmp1995 + tmp371)/tmp10 + tmp60*(214*tmp61 &
            &- 1429*tmp70 - 231*tmp72) + (616/tmp10 + 107/tmp54)*tmp74 + (863*tmp1422 - 1052*&
            &tmp1650 + 805*tmp82 + 107*tmp83)/tmp55) + tmp5*tmp6*((-1118*tmp1422 + 1477*tmp16&
            &50 + tmp1997 + tmp368)/tmp10 + tmp60*(tmp1996 + 258*tmp61 - 1805*tmp70) + tmp107&
            &*(241/tmp10 + tmp792) + (904*tmp1422 - 1251*tmp1650 + 1070*tmp82 + 129*tmp83)/tm&
            &p55) + tmp2731*((-651*tmp1422 + 898*tmp1650 + tmp1998 + tmp371)/tmp10 + tmp60*(t&
            &mp514 + 136*tmp61 - 1007*tmp70) + (421*tmp1422 - 660*tmp1650 + tmp1999 + 619*tmp&
            &82)/tmp55 + tmp2371*(95/tmp10 + tmp859)) + (tmp2001 + (-145*tmp1422 + 201*tmp165&
            &0 + tmp2002 + tmp385)/tmp10 + (-137*tmp1650 + tmp2003 + tmp2004 + tmp2141)/tmp55&
            & + 14*tmp60*(tmp1005 + tmp80))*tmp93))/tmp100 + (m2*tmp1772*tmp410*tmp7*(tmp738*&
            &((tmp2673*(tmp155 + tmp1732 + tmp419))/tmp54 + (172*tmp1422 + tmp1958 + tmp1982 &
            &+ tmp585)/tmp55 + tmp60*(tmp105 + tmp1759 - 375*tmp70) + tmp1981*tmp74) + tmp241&
            &2*((tmp1057*(tmp1042 + tmp1597 + tmp1985))/tmp10 + (-84*tmp1650 + tmp192 + tmp19&
            &86 + tmp600)/tmp55 + tmp1984*tmp74 + (tmp113 + tmp138 + tmp391)*tmp781) + tmp254&
            &*(tmp60*(tmp1257 + tmp354 + tmp660) + (tmp326 + tmp358 - 29*tmp70)*tmp72 + tmp19&
            &79*tmp74 + (tmp195 + tmp2597 + tmp586 + 29*tmp82)/tmp55) + 2*tmp2731*(tmp2371*tm&
            &p376 + (tmp112 + tmp1987 + tmp233)*tmp60 + (tmp1988 + tmp1989 + tmp2664)*tmp72 +&
            & (37*tmp1422 - 68*tmp1650 + tmp656 + 83*tmp82)/tmp55) + ((tmp2099 + tmp288 + tmp&
            &354)*tmp60 + (tmp1875 + tmp1991 + tmp380)*tmp72 + tmp1990*tmp74 + (tmp195 + tmp1&
            &992 + tmp1993 + tmp877)/tmp55)*tmp93))/tmp54 - 8*tmp32*tmp7*(tmp254*tmp3*(-tmp19&
            &06 + (tmp1632*tmp2005)/tmp54 + tmp60*(tmp1637 - 111*tmp1650 + tmp2006 + tmp82) +&
            & (tmp2024 + tmp2570 - 12*tmp642 + tmp665 - (292*tmp83)/tmp10)/tmp55) + 4*tmp738*&
            &(tmp1422*tmp1612 + (tmp2008*tmp636)/tmp54 + tmp74*(-705*tmp1422 + tmp2009 + tmp8&
            &2 + 169*tmp83) + tmp60*(tmp1653 + tmp2010 + tmp3060 + 558*tmp61*tmp70 - (650*tmp&
            &83)/tmp10) + (tmp2011 + tmp1947/tmp54 + tmp643 - 448*tmp61*tmp70 + (554*tmp83)/t&
            &mp10)/(tmp10*tmp55)) + tmp2731*(4*tmp1422*tmp1612 + (tmp2017*tmp636)/tmp54 + tmp&
            &74*(-2041*tmp1422 - 425*tmp1650 + tmp385 + 462*tmp83) + tmp60*(tmp2018 + tmp2589&
            & + 229*tmp641 + 1234*tmp642 - (1735*tmp83)/tmp10) + (tmp1641 + tmp2019 + tmp2679&
            &/tmp54 - 1418*tmp61*tmp70 + (1921*tmp83)/tmp10)/(tmp10*tmp55)) + tmp2412*(tmp202&
            &0 + (tmp2014*tmp636)/tmp54 + tmp2482*(-608*tmp1422 - 161*tmp1650 + tmp82 + 142*t&
            &mp83) + tmp60*(tmp1212 + 141*tmp641 + 717*tmp642 + 841*tmp61*tmp70 - (1069*tmp83&
            &)/tmp10) + (tmp2026 - 268*tmp641 - 793*tmp61*tmp70 + (1041*tmp83)/tmp10 + tmp915&
            &)/(tmp10*tmp55)) + (tmp2020 + tmp2022*tmp636*tmp76 + tmp60*(tmp1212 + tmp2023 + &
            &tmp2024 + tmp2916/tmp54 - (348*tmp83)/tmp10) + tmp74*(-413*tmp1422 + tmp151 + tm&
            &p2286 + tmp858) + (tmp2025 + tmp2026 - 323*tmp61*tmp70 + (434*tmp83)/tmp10 + tmp&
            &915)/(tmp10*tmp55))*tmp93))*tt))/16. + (discbslogs1235*tmp10*tmp11*tmp13*tmp240*&
            &tmp411*tmp54*tmp55*tmp59*tmp65*(256*tmp1518*tmp157*tmp158 - (256*tmp157*tmp36*tm&
            &p713*(tmp158*tmp175*tmp2542*tmp72 + tmp469*tmp85*tmp945))/tmp96 + (256*tmp157*tm&
            &p2533*tmp713*((tmp158*tmp2542*tmp723)/tmp10 - tmp564*tmp964))/(tmp11*tmp96) + 25&
            &6*tmp1496*tmp157*tt - 256*tmp157*tmp158*(tmp44*tmp7*(tmp1219/tmp54 + (tmp123 + t&
            &mp125 + tmp266)*tmp3211*tmp6 + tmp2178*(tmp1007 + (tmp103 + tmp138 + tmp139)/tmp&
            &55 + tmp2145*tmp60) + 2*tmp2731*(tmp1026 + tmp2670*tmp60 + (tmp80 + tmp88 + tmp9&
            &2)/tmp55) + tmp1038*tmp93) + tmp966*(tmp2178*((tmp1032 + tmp360)*tmp60 + (tmp261&
            & + tmp366 - 64*tmp72)/tmp55 + (48*tmp840)/tmp10) + 2*tmp2731*((tmp224 + tmp226 +&
            & tmp2368/tmp54)/tmp55 + (tmp152 + tmp333)*tmp60 + tmp842) + tmp516*tmp6*((tmp159&
            &6 + tmp2648 + tmp382)/tmp55 + (tmp1033 + tmp1638)*tmp60 + tmp1739*tmp86) + 3*tmp&
            &1038*tmp93 + tmp3*tmp89*(tmp17*tmp969 + (tmp144 + tmp969)/tmp55))*tmp971 + 4*tmp&
            &4*((tmp532*(tmp2271*tmp60 + tmp1024*tmp74 + tmp72*(tmp354 + tmp628 + tmp77) + (t&
            &mp1650 + tmp187 + tmp657 + tmp83)/tmp55))/tmp11 + tmp143*(tmp60*(tmp366 + 312*tm&
            &p61 - 293*tmp72) + tmp107*(tmp2122 + tmp75) + (tmp1057*(tmp1039 + tmp70 + tmp777&
            &))/tmp10 + (-470*tmp1650 + tmp193 + tmp2169 + 156*tmp83)/tmp55) + tmp868 + tmp69&
            &0*(tmp60*(tmp1040 + tmp314 + 148*tmp61) + (tmp1057*(tmp1041 + tmp1042 + tmp70))/&
            &tmp10 + (63/tmp10 + 74/tmp54)*tmp74 + (-272*tmp1650 + tmp193 + tmp2400 + 74*tmp8&
            &3)/tmp55)*tmp89 + tmp44*tmp6*((tmp1044 + tmp114 - 151*tmp61)*tmp72 + (73/tmp10 +&
            & 94/tmp54)*tmp74 + (tmp1045 + 90*tmp1422 + tmp791 + 94*tmp83)/tmp55 + tmp60*(tmp&
            &1043 + 188*tmp61 + tmp902)) + ((tmp117 + tmp2)*tmp3*tmp624*tmp971)/tmp9) - 8*tmp&
            &32*(tmp1031 + tmp5*tmp6*(tmp60*(tmp1021 + tmp608 + 100*tmp61) + (tmp1022 - 185*t&
            &mp1650 + tmp1757 + tmp639)/tmp55 + (tmp1019 + tmp1020 + tmp384)*tmp72 + (tmp377 &
            &+ 50/tmp54)*tmp74) + tmp2178*((tmp2638*(tmp2097 + tmp613 + tmp73))/tmp10 + (88/t&
            &mp10 + tmp1017)*tmp74 + (tmp1015 + tmp1018 + 71*tmp61)*tmp781 + (-283*tmp1650 + &
            &tmp2366 + tmp2711*tmp70 + tmp791)/tmp55) + tmp2192*tmp2731*tmp7*(tmp1025 + tmp83&
            &8 + tmp867) + tmp89*((tmp1014 + tmp1015 + tmp1854)*tmp60 + (tmp1016 + 251*tmp142&
            &2 - 852*tmp1650 + tmp639)/tmp55 + (tmp1013 + 280/tmp54)*tmp74 + ((tmp191 + tmp32&
            &9 + tmp490)*tmp987)/tmp54)) + tmp989 - 256*tmp272*tmp7*(((tmp108 + tmp333)*tmp60&
            & + (tmp138 + tmp276 + tmp608)/tmp55 + tmp990/tmp10)/tmp11 + tmp991) - 128*tmp252&
            &*tmp7*(tmp5*(tmp24*(tmp149 + tmp191 + tmp507) + tmp60*(tmp2 + tmp926) + (tmp1594&
            & + tmp232 + tmp992)/tmp55) + (tmp36*(tmp197 + tmp20 + tmp354 + tmp70 + tmp993))/&
            &tmp10 + tmp998) + 16*tmp30*tmp7*(tmp1012 - 2*tmp1535*(tmp1006*tmp144 + (tmp1005 &
            &+ tmp105 + tmp138)/tmp55 + (tmp1004 + tmp108)*tmp60) + 2*tmp1560*((tmp1002 + tmp&
            &2150 + tmp637)/tmp55 + tmp60*(tmp472 + tmp848) + tmp63*(tmp1003 + tmp70 + tmp860&
            &)) + tmp44*(51*tmp137 + (tmp1000 + tmp1001 - 536*tmp72)/tmp55 + (-255*tmp61 + tm&
            &p835 + tmp999)/tmp10)))*tt))/8. + 256*discbtlogs15*tmp10*tmp16*tmp4*tmp52*tmp53*&
            &tmp54*tmp65*tmp67*tmp9*(tmp68 + (tmp951 + (tmp20 - tmp60 + tmp84/tmp54)*tt)/(tmp&
            &10*tmp55)) - 64*logs25*tmp10*tmp100*tmp11*tmp52*tmp55*tmp56*tmp59*tmp65*tmp9*tmp&
            &96*tmp99*((tmp101 + tmp102 + tmp180)*tmp21*tmp3*tmp31 + (tmp2619*((tmp25*tmp475)&
            &/tmp55 + tmp1577*tmp62 + tmp1560*(tmp104/tmp10 + tmp107 + (tmp105 + tmp106 + tmp&
            &61)/tmp55 + (tmp194 + tmp76)*tmp781) + tmp1535*(tmp107 + tmp2791*tmp60 + tmp603 &
            &+ (tmp105 + tmp109 + tmp80)/tmp55)))/tmp54 - 32*tmp32*(tmp126*tmp475 + tmp44*(tm&
            &p375 + tmp110/tmp55)*tmp62 + tmp1535*(tmp119 + tmp122 + (tmp134 + tmp226 + tmp22&
            &9)*tmp60 + (tmp145 + tmp1992 + tmp227 + tmp650)/tmp55) + tmp1560*(tmp116 + tmp11&
            &9 + (tmp112 + tmp129 + tmp328)*tmp60 + (tmp130 + tmp131 + tmp385 + tmp83)/tmp55)&
            &) + 4*tmp4*(4*tmp738*(tmp116 + tmp133 + (tmp112 + tmp129 + tmp190)*tmp60 + (tmp1&
            &30 + tmp131 + tmp151 + tmp83)/tmp55) + (tmp183 + tmp127/tmp55)*tmp62*tmp89 + tmp&
            &516*tmp6*((tmp113 + tmp384 + tmp422)*tmp72 + tmp74*(tmp209 + tmp75) + tmp60*(tmp&
            &222 + tmp261 + tmp81) + (60*tmp1422 + tmp235 + tmp265 + tmp90)/tmp55) + 4*tmp273&
            &1*(tmp133 + tmp414*tmp72 + (tmp136 + tmp192 + tmp673 + tmp82)/tmp55 + tmp60*(tmp&
            &134 + tmp916 + tmp92)) + tmp142*tmp7*tmp93) + tmp966*((tmp156*tmp25*tmp7)/(tmp54&
            &*tmp55) + tmp48*tmp5*(tmp148 + (tmp151 + tmp1850 + tmp590 + tmp600)/tmp55 + (tmp&
            &114 + tmp149 + tmp197)*tmp72 + (tmp112 + tmp150 + tmp77)*tmp867) + tmp44*tmp6*(t&
            &mp148 + (tmp1039 + tmp114 + tmp199)*tmp60 + (tmp1005 + tmp112 + tmp286)*tmp72 + &
            &(69*tmp1422 + tmp145 + tmp652 + tmp897)/tmp55) + (tmp143*tmp62)/(tmp54*tmp9) + (&
            &(tmp1533 + tmp154 + tmp2479*(tmp155 + tmp276 + tmp369) + tmp60*(tmp204 + tmp2095&
            & + tmp77))*tmp93)/tmp11 + (tmp154 + (tmp103 + tmp149 + tmp1849)*tmp60 + (tmp224 &
            &+ tmp288 + tmp61)*tmp72 + (tmp638 + tmp82 + tmp83 + tmp882)/tmp55)*tmp89*tt)) - &
            &64*logs15*tmp10*tmp100*tmp11*tmp52*tmp53*tmp54*tmp59*tmp65*tmp9*tmp96*tmp99*(tmp&
            &21*tmp3*tmp31*(tmp101 + tmp102 + tmp471) + (tmp2619*((tmp44*tmp62)/tmp10 + (tmp1&
            &693*(tmp208*tmp71 + tmp60*tmp71 + (tmp61 + tmp72 + tmp73)/tmp55))/tmp11 + (tmp15&
            &60*(tmp208 + tmp358 + tmp373 + tmp73 + tmp243*tmp79))/tmp10 + tmp25*tmp475*tmp85&
            &))/tmp55 - 32*tmp32*(-(tmp126*tmp475) + tmp44*(tmp18 + tmp188/tmp55)*tmp62 + tmp&
            &1560*(4*tmp2841 + (tmp109 + tmp1741 + tmp233)/(tmp10*tmp55) + tmp60*(tmp354 + tm&
            &p426 + tmp81) + tmp74*tmp839) + tmp1535*(tmp122 - tmp2523*(tmp185 + tmp75) + tmp&
            &854 + tmp243*(6*tmp1650 + tmp648 + tmp83 + tmp921))) + 4*tmp4*(tmp142*tmp2179*tm&
            &p7 + tmp516*tmp6*(tmp116 + (tmp189 + tmp360)*tmp74 + tmp60*(tmp213 + tmp216 + tm&
            &p80) + tmp243*(tmp2567/tmp54 + tmp639 + tmp763 + tmp83)) + tmp62*(tmp18 + tmp79/&
            &tmp55)*tmp89 + 4*tmp738*(tmp340 + tmp74*(tmp75 + tmp76) + tmp60*(tmp105 + tmp80 &
            &+ tmp81) + (tmp2276 + tmp634 + tmp90)/tmp55) - 4*tmp2731*(tmp2128/tmp10 + tmp642&
            & + tmp781*(tmp72 + tmp73 + tmp80) + tmp74*tmp84 + (tmp187 + tmp195 + tmp806 + tm&
            &p922)/tmp55)) + tmp966*((tmp143*tmp62)/(tmp10*tmp55) + (tmp156*tmp243*tmp25*tmp7&
            &)/tmp54 + tmp44*tmp6*(3*tmp2841 + tmp60*(tmp150 + tmp263 + tmp61) + (tmp377 + 1/&
            &tmp54)*tmp74 + (tmp225 + tmp427 + tmp761)/(tmp10*tmp55)) + tmp48*tmp5*(tmp104*tm&
            &p72 + (tmp203 + tmp602)*tmp74 - tmp60*(tmp112 + tmp508 + tmp767) + (tmp136 + 42*&
            &tmp1422 + tmp264 + tmp90)/tmp55) + ((tmp412 + (tmp144 - 6/tmp54)*tmp74 + (tmp429&
            & + tmp584 + tmp790)/(tmp54*tmp55) + tmp60*(tmp766 + tmp91 + tmp92))*tmp93)/tmp11&
            & + tmp89*(tmp2270 + tmp2841 + tmp60*(tmp366 + tmp61 + tmp88) + (tmp106 + tmp207 &
            &+ tmp91)/(tmp10*tmp55))*tt)) + 256*discbulogs15*tmp10*tmp14*tmp4*tmp52*tmp53*tmp&
            &54*tmp58*tmp59*tmp9*(tmp241*tmp3*(tmp2175 + (tmp155 + tmp184 + tmp77)/tmp55 + tm&
            &p19*tmp781) + (tmp243*(tmp19*tmp23 + tmp988/tmp55 + tmp948*tt))/tmp9) - (discbtl&
            &ogt*tmp240*tmp357*tmp411*tmp53*tmp56*tmp59*tmp65*tmp67*(256*tmp157*tmp72*(-(tmp1&
            &437*tmp2341) + m2*tmp158*tmp164*tmp175*tmp242*tmp670 - (tmp2341*tmp708 + (tmp158&
            &*tmp175*tmp242*tmp61*tmp692)/tmp99)/tmp9 + (tmp242*tmp3155)/tmp99)*tt + (256*tmp&
            &157*tmp72*(tmp2318*tmp469 + m2*tmp158*tmp175*tmp1772*tmp242*tmp469 + (tmp1584*tm&
            &p175*tmp242*tmp627*tmp97)/tmp99)*tt)/tmp55 - 256*tmp15*tmp157*tmp7*(tmp2248 + tm&
            &p2449 + 32*tmp252*(tmp2250 + tmp2460 + tmp1693*((tmp109 + tmp1602 + tmp380)*tmp6&
            &36 + tmp60*(tmp2255 + tmp2961 + 37*tmp641 + 100*tmp642 + 18*tmp643) + tmp74*(-83&
            &*tmp1650 + tmp1999 + tmp2253 + tmp775) + tmp844*(tmp2254 + tmp2915 + tmp2164*tmp&
            &61 - (131*tmp83)/tmp10 + tmp889) + tmp2175*tmp90)) - 16*tmp30*(tmp2256 + tmp2475&
            & + (tmp1693*(tmp2175*tmp368 + (tmp2259 + tmp429 + tmp613)*tmp636 + tmp2482*(-173&
            &*tmp1422 + tmp2260 + tmp2653 + tmp775) + tmp60*(tmp2263 + 255*tmp642 + 36*tmp643&
            & + 143*tmp61*tmp70 - (307*tmp83)/tmp10) + (tmp1626 + tmp2261 + tmp2262 + 87*tmp6&
            &1*tmp70 - (215*tmp83)/tmp10)*tmp844))/tmp11 + tmp428*(tmp2175*tmp371 + (tmp2105 &
            &+ tmp2264 + tmp352)*tmp636 + (-248*tmp1422 - 52*tmp1650 + tmp1766 + tmp2265)*tmp&
            &74 + tmp60*(tmp2254 + 196*tmp642 + 24*tmp643 + tmp2115*tmp70 - (216*tmp83)/tmp10&
            &) + tmp2480*(tmp1648 + tmp2266 + tmp1530*tmp61 + tmp662/tmp54 + tmp915))) + 8*tm&
            &p32*(tmp2282 + tmp2495 - tmp2731*(tmp2175*tmp385 + tmp636*(tmp1590 + tmp369 + 14&
            &7*tmp72) + (-393*tmp1422 + tmp2286 + tmp2287 + tmp639)*tmp74 + tmp60*(tmp1626 + &
            &tmp2288 + tmp2289 + 278*tmp642 - (299*tmp83)/tmp10) + (tmp1641 + tmp1972 + tmp21&
            &55 + 142*tmp61*tmp70 - (253*tmp83)/tmp10)*tmp844) + tmp1834*tmp5*(tmp2290 + tmp2&
            &482*(tmp1615 - 2*tmp1650 + tmp353 + tmp648) + tmp636*(tmp196 + tmp225 + tmp73) +&
            & tmp60*(tmp2285 + tmp2703/tmp54 - (51*tmp83)/tmp10 + tmp889 + tmp892) + tmp844*(&
            &tmp1209 + tmp2151 + tmp2284 + tmp2285 + tmp915)) + tmp2179*(tmp2290 + tmp2482*(t&
            &mp1650 + tmp2291 + tmp2292 + tmp648) + tmp636*(tmp330 + 69*tmp72 + tmp73) + tmp6&
            &0*(tmp2294 + tmp2295 + 187*tmp642 + tmp2397*tmp70 + tmp889) + tmp844*(tmp2293 + &
            &tmp231*tmp70 - (207*tmp83)/tmp10 + tmp915 + tmp82*tmp926))) + (tmp48*tmp626*(tmp&
            &2412*tmp671 + tmp1577*tmp3*tmp686 + (tmp3226*tmp48)/tmp9 + (tmp3*tmp89)/tmp9 + (&
            &tmp155 + tmp2267 + tmp276 + tmp359 + tmp781 + tmp804)*tmp93))/tmp55 + (m2*tmp177&
            &2*tmp6*(tmp128*tmp160*tmp2731*(tmp1530 + tmp196 + tmp213 + tmp2273 + tmp2709/tmp&
            &54 - 28/(tmp10*tmp55)) + tmp2233*tmp3*tmp688 + tmp738*(tmp2503 + tmp135*(tmp2269&
            & + tmp301 + tmp61) + tmp60*(tmp1875 + tmp628 + tmp80) + (tmp1857 + tmp2650 + tmp&
            &787 + tmp83)/tmp55) + tmp2412*(tmp2270 + (tmp1057*tmp2271)/tmp10 + tmp2275*tmp78&
            &1 + (tmp2272 + tmp651 + 14*tmp82 + tmp83)/tmp55) + (tmp2274*tmp2482 + tmp60*(tmp&
            &1589 + tmp354 + tmp762) + (tmp1422 + tmp195 + tmp2368*tmp61 + 31*tmp82)/tmp55 + &
            &tmp72*(tmp1236 + tmp216 + tmp900))*tmp93))/tmp54 + tmp1224*tmp4*(tmp738*(tmp2512&
            & + tmp24*(tmp130 + tmp195 + tmp2276 + tmp385) + 6*tmp2275*tmp60 + (-34*tmp1650 +&
            & tmp227 + tmp277 + 50*tmp82)/tmp55) + tmp5*tmp6*(-((tmp2277 + tmp2278 + tmp348 +&
            & tmp368)/tmp10) + tmp60*(tmp2647 + tmp275 - 205*tmp70) + (86/tmp10 + tmp214)*tmp&
            &74 + (tmp1118 - 134*tmp1650 + tmp590 + 131*tmp82)/tmp55) + tmp2731*(-((117*tmp14&
            &22 + tmp1749 + tmp2507 + tmp371)/tmp10) + tmp60*(tmp288 + tmp330 - 226*tmp70) + &
            &tmp74*(tmp2279 + tmp808) + (-125*tmp1650 + tmp2280 + 149*tmp82 + tmp852)/tmp55) &
            &+ (tmp1225*tmp3*tmp89)/tmp10 + ((18/tmp10 + tmp108)*tmp2482 - (81*tmp1422 - 138*&
            &tmp1650 + tmp2908 + tmp385)/tmp10 + tmp60*(tmp1534 + tmp2281 + tmp620) + (-72*tm&
            &p1650 + tmp387 + 97*tmp82 + tmp879)/tmp55)*tmp93)*tt) + (256*tmp157*tt*(-(tmp178&
            &5*tmp2344) + tmp158*tmp175*tmp241*tmp242*tmp2545*tmp72 + (-(tmp2344*tmp695) + (t&
            &mp242*tmp3129)/tmp99)/tmp10 + (tmp1422*tmp175*tmp242*tmp3118*tt)/tmp99))/(tmp55*&
            &tmp9) + (256*tmp157*tt*(tmp2338*tmp249 + tmp1584*tmp175*tmp242*tmp249*tmp966 + (&
            &tmp158*tmp175*tmp242*tmp689*tt)/(tmp10*tmp99)))/(tmp54*tmp55*tmp9)))/16. - 256*d&
            &iscbulogs25*tmp10*tmp14*tmp4*tmp52*tmp55*tmp56*tmp58*tmp59*tmp9*(tmp19*tmp21*tmp&
            &241*tmp3 + tmp72*(tmp182*tmp3*tmp94 + (tmp1063/tmp10 + tmp135 + tmp243/tmp54 + t&
            &mp61 + tmp993)*tt)) - (discbslogs*tmp11*tmp13*tmp240*tmp357*tmp411*tmp53*tmp56*t&
            &mp59*tmp65*(-256*tmp1430*tmp157*tmp158*tmp177 - (256*tmp1475*tmp157*tmp1584*tmp1&
            &77)/(tmp54*tmp55) + (256*tmp1518*tmp157*tmp158*tmp72)/tmp55 + (256*tmp1449*tmp15&
            &7*tmp158*tmp2541*tmp713)/(tmp10*tmp96) + (256*tmp1440*tmp157*tmp1650*tmp36*tmp71&
            &3)/tmp96 + (256*tmp1496*tmp157*tmp72*tt)/tmp55 - (256*tmp1506*tmp157*tt)/(tmp54*&
            &tmp55*tmp9) + (256*tmp157*tmp250*tmp713*tmp72*tt*(tmp21*tmp3*tmp447 - 4*tmp4*(tm&
            &p3*tmp44*(tmp17*tmp316 + tmp318/tmp55) + 3*tmp1560*tmp3*(tmp1081 + (tmp203 + tmp&
            &580)/tmp55) + tmp2209*tmp688 + tmp1535*((tmp216 + tmp276 + tmp354)/tmp55 + (tmp2&
            &60 + tmp317)*tmp60 + (tmp1638*tmp86)/tmp10)) + tmp241*(2*tmp1030*tmp2731 + tmp22&
            &01*tmp688 + (tmp5*tmp535*(tmp2126 + tmp2480 + tmp783))/tmp10 + tmp3*(tmp18 + tmp&
            &334/tmp55)*tmp89 + tmp1083*tmp3*tmp934) - 32*tmp3*tmp30*(tmp518*(tmp1075 + (tmp1&
            &17 + tmp210)/tmp55) + tmp21*tmp936) + ((-3*tmp1230)/tmp11 + tmp1577*tmp160*tmp17&
            &2 + tmp244*tmp2640*tmp48 + (tmp143*tmp244)/tmp55 - (tmp156*tmp160)/tmp9 + (tmp22&
            &33*tmp244*tt)/tmp55)/tmp10 + 8*tmp32*((tmp1077 + tmp1078 + tmp1080)*tmp452 + tmp&
            &101*(tmp17*tmp209 + (tmp1076 + tmp209)/tmp55) + tmp3*(tmp290 + (tmp203 + tmp217)&
            &/tmp55)*tmp698*tt)))/(tmp55*tmp96)))/16. + (discbulogs35*tmp14*tmp240*tmp357*tmp&
            &54*tmp55*tmp58*tmp59*tmp9*(256*tmp15*tmp157*tmp7*(tmp2049*tmp249 + (tmp2533*tmp4&
            &08)/(tmp10*tmp100*tmp55) + tmp249*tmp408*tmp966) + 256*tmp157*(1024*tmp259*(tmp4&
            &13 + (tmp1611 - 4*tmp1650 + tmp187 + tmp195)/tmp55 + tmp1006*tmp72 + (tmp60*tmp8&
            &31)/tmp54) + 16*tmp30*(tmp1535*(tmp60*(tmp1739 + 624*tmp61 - 742*tmp70) + tmp72*&
            &(tmp1869 + tmp596 + 499*tmp72) + (730/tmp10 + 318/tmp54)*tmp74 + (-159*tmp1422 -&
            & 1135*tmp1650 + tmp368 + 306*tmp83)/tmp55) + tmp48*((-336*tmp1650 + tmp1857 + tm&
            &p370 + tmp371)/tmp55 + tmp1974*tmp74 + (tmp197 + tmp389 + 81*tmp61)*tmp781 + (tm&
            &p1057*(tmp369 - 81*tmp72 + tmp833))/tmp10) + tmp1560*(tmp60*(tmp388 + 738*tmp61 &
            &- 914*tmp70) + tmp1967*tmp74 + (-309*tmp1422 - 1231*tmp1650 + tmp368 + 363*tmp83&
            &)/tmp55 + (tmp1579*(tmp390 + tmp73 + tmp850))/tmp10) + tmp524*((tmp3023 + tmp366&
            & + tmp622)*tmp72 + tmp60*(134*tmp61 - 185*tmp70 + tmp72) + tmp1962*tmp74 + (tmp1&
            &958 + tmp367 + 64*tmp83 + tmp90)/tmp55)) - 32*tmp252*(tmp516*(((tmp196 + tmp345 &
            &+ tmp359)*tmp602)/tmp10 + (-128*tmp1650 + tmp1947 + tmp2510 + tmp677)/tmp55 + tm&
            &p60*(tmp2160 + tmp362 - 96*tmp70) + tmp1946*tmp74) + tmp36*(tmp60*(356*tmp61 - 3&
            &25*tmp70 - 95*tmp72) + tmp72*(tmp103 - 159*tmp61 + 161*tmp72) + tmp1950*tmp74 + &
            &(-565*tmp1650 + tmp363 + tmp367 + tmp90)/tmp55) + tmp6*(tmp60*(tmp1592 + 160*tmp&
            &61 - 147*tmp70) + (tmp2638*(tmp365 + tmp644 + tmp73))/tmp10 + tmp1957*tmp74 + (-&
            &269*tmp1650 + tmp370 + tmp890 + tmp90)/tmp55)) + (tmp1629*tmp7*(tmp1767*tmp1777 &
            &+ tmp2178*(tmp1578/tmp10 + tmp1976 + tmp374) + tmp2412*(tmp372 + tmp783 + tmp804&
            &) + tmp3178*tmp48*(tmp781 + tmp782 + tmp840) + (tmp373 + tmp374 + tmp659)*tmp93)&
            &)/tmp10 + tmp410*tmp7*(tmp254*tmp3*(tmp375 + tmp1979/tmp55) + 2*tmp2731*(tmp315 &
            &+ tmp2126*tmp376 + tmp1058*(tmp184 + tmp61 + tmp81)) + tmp738*(tmp1981*tmp60 + (&
            &tmp200 + tmp204 - 165*tmp70)/tmp55 + (tmp2244*tmp86)/tmp10) + tmp2412*((tmp138 +&
            & tmp139 + tmp2157)/tmp55 + tmp1984*tmp60 + tmp2578*tmp86) + (tmp1990*tmp60 + (tm&
            &p1868*tmp86)/tmp10 + (tmp431 + tmp80 + tmp88)/tmp55)*tmp93)*tmp966 - 8*tmp32*(tm&
            &p2412*((-185*tmp1422 + tmp151 - 560*tmp1650 + tmp2557)/tmp55 + (tmp1057*(tmp191 &
            &+ tmp430 + 131*tmp61))/tmp10 + tmp60*(tmp383 - 485*tmp70 + 177*tmp72) + tmp2014*&
            &tmp74) + tmp2731*((-144*tmp1422 + tmp1639 + tmp2153 + tmp385)/tmp55 + tmp60*(462&
            &*tmp61 - 771*tmp70 + 253*tmp72) + tmp72*(tmp384 - 535*tmp61 + 547*tmp72) + tmp20&
            &17*tmp74) + tmp254*(tmp1632*tmp2005 + tmp60*(tmp379 + 97*tmp61 - 183*tmp70) + (t&
            &mp229 + tmp380 - 48*tmp61)*tmp72 + (tmp1749 + tmp2392 + tmp82 + tmp910)/tmp55) +&
            & (tmp2022*tmp2482 + (tmp151 + tmp387 + tmp588 + tmp635)/tmp55 + (tmp103 + tmp176&
            &0 + tmp597)*tmp72 + tmp781*(tmp386 + tmp613 + tmp857))*tmp93 + 4*tmp738*(tmp60*(&
            &169*tmp61 - 300*tmp70 + 115*tmp72) + tmp2008*tmp74 + (-295*tmp1650 + tmp2966 + t&
            &mp82 + 84*tmp83)/tmp55 + ((tmp1018 + tmp382 + tmp70)*tmp987)/tmp54)) + 128*tmp27&
            &2*(tmp433*((tmp104*tmp128)/tmp54 + (tmp145 + tmp1634 + tmp206 + tmp387)/tmp55 - &
            &tmp60*(tmp288 + tmp359 + tmp61) + tmp188*tmp74) + ((tmp150 + tmp261 + tmp297)*tm&
            &p60 + tmp72*(tmp286 + tmp361 + tmp73) + tmp1614*tmp74 + (tmp193 + tmp2649 + tmp6&
            &55 + tmp864)/tmp55)*tt) + 4*tmp4*(2*tmp143*(tmp1994*tmp2482 + (tmp351 + tmp390 +&
            & tmp391)/(tmp54*tmp55) + (tmp261 + tmp388 + tmp389)*tmp60 + tmp135*(tmp351 + tmp&
            &419 + tmp70)) + tmp44*tmp6*((236*tmp61 - 733*tmp70 - 1058*tmp72)/(tmp54*tmp55) +&
            & (tmp1057*(tmp191 + 286*tmp61 - 289*tmp72))/tmp10 + tmp60*(472*tmp61 - 1339*tmp7&
            &0 + 847*tmp72) + (1339/tmp10 + tmp1881)*tmp74) + tmp156*(tmp2001 + tmp60*(tmp394&
            & + 28*tmp61 - 77*tmp70) + (tmp421 + tmp608 - 90*tmp72)/(tmp54*tmp55) + (tmp1885*&
            &tmp86)/tmp10) + tmp48*tmp5*(tmp60*(tmp392 + 394*tmp61 - 1103*tmp70) + (197*tmp61&
            & - 447*tmp70 - 1030*tmp72)/(tmp54*tmp55) + (1103/tmp10 + 197/tmp54)*tmp74 + ((tm&
            &p393 + tmp70 - 156*tmp72)*tmp987)/tmp54) + (tmp93*((tmp607 - 124*tmp70 - 485*tmp&
            &72)/(tmp54*tmp55) + tmp72*(tmp103 - 311*tmp61 + 313*tmp72) + (457/tmp10 + 82/tmp&
            &54)*tmp74 + tmp60*(tmp291 - 457*tmp70 + tmp999)))/tmp11 + ((tmp1622 - 557*tmp70 &
            &- 521*tmp72)/(tmp54*tmp55) + tmp72*(tmp190 + tmp2921 + 239*tmp72) + tmp60*(278*t&
            &mp61 - 812*tmp70 + 522*tmp72) + (812/tmp10 + 139/tmp54)*tmp74)*tmp89*tt))))/8. +&
            & (discbtlogs35*tmp240*tmp357*tmp54*tmp55*tmp59*tmp65*tmp67*tmp9*(-256*tmp157*(tm&
            &p2344*tmp249 + (tmp158*tmp175*tmp241*tmp242*tmp249)/tmp10 + (tmp158*tmp175*tmp24&
            &2*tmp2542*tmp70)/tmp99)*tt - 256*tmp157*(-(tmp2338*tmp249) + tmp1584*tmp175*tmp2&
            &41*tmp242*tmp249 + (tmp158*tmp175*tmp242*tmp689*tmp97)/(tmp10*tmp99))*tt - 256*t&
            &mp15*tmp157*(tmp759 - 16*tmp30*(tmp524*(tmp1081 + (tmp360 + tmp580)/tmp55)*tmp62&
            & - tmp1560*((tmp346*tmp593)/tmp54 + tmp74*(tmp580 + tmp75) + (22*tmp1650 + tmp30&
            &63 + tmp772 + tmp775)/tmp55 + tmp60*(tmp330 + tmp502 + tmp793)) + (tmp1693*((103&
            &/tmp10 + tmp2244)*tmp74 + tmp60*(tmp380 + 102*tmp61 + tmp773) + (-113*tmp1422 + &
            &tmp2658 + tmp774 + tmp775)/tmp55 + tmp790*(tmp191 + tmp779 + tmp805)))/tmp11 + t&
            &mp428*(tmp74*(66/tmp10 + tmp776) + tmp60*(tmp2681 + tmp777 + tmp778) + (tmp233 +&
            & tmp73 + tmp779)*tmp790 + (-126*tmp1650 + tmp230 + tmp780 + tmp866)/tmp55)) + 32&
            &*tmp252*(tmp516*tmp62*((tmp273 + tmp612)/tmp55 + tmp764) + tmp697*(tmp74*(tmp612&
            & + tmp765) + tmp208*(tmp73 + tmp766 + tmp767) + (-108*tmp1422 + tmp193 + tmp768 &
            &+ tmp769)/tmp55 + tmp60*(tmp350 + tmp431 + tmp789)) + tmp6*(tmp60*(tmp113 + tmp1&
            &597 - 68*tmp61) + (tmp114 + tmp1590 + tmp509)*tmp72 - tmp74*(tmp1013 + tmp770) +&
            & (18*tmp1422 + 113*tmp1650 + tmp771 + tmp90)/tmp55)) + 8*tmp32*(tmp1083*tmp254*t&
            &mp62 + tmp1834*tmp5*((tmp377 + tmp580)*tmp74 + tmp60*(tmp297 + tmp421 + tmp789) &
            &+ (tmp128*(tmp584 + tmp70 + tmp790))/tmp54 + (tmp2272 + tmp2654 + tmp652 + tmp79&
            &1)/tmp55) + tmp738*(tmp74*tmp785 + tmp72*tmp786 + (66*tmp1422 + tmp264 + tmp787 &
            &+ tmp788)/tmp55 - tmp60*(tmp508 + tmp80 + tmp802)) + tmp2179*(tmp74*(tmp798 + tm&
            &p799) + tmp60*(tmp326 + tmp800 + tmp801) + (tmp128*(-40*tmp61 + tmp70 + tmp802))&
            &/tmp54 + (-112*tmp1650 + tmp193 + tmp2669 + tmp803)/tmp55) - tmp2731*(tmp60*(tmp&
            &2110 + tmp793 + tmp794) + (tmp639 + tmp795 + tmp796 + tmp797)/tmp55 + tmp72*(tmp&
            &369 - 71*tmp61 + tmp863) + tmp74*(tmp792 + tmp925))) + (tmp475*((tmp3226*tmp48)/&
            &tmp55 + tmp2304*tmp3*tmp686 + tmp1777*tmp89 + (tmp160*tmp2412)/tmp9 + (tmp781 + &
            &tmp782 + tmp783)*tmp93))/tmp10 + tmp180*((tmp1777*tmp2233)/tmp10 + tmp3*(tmp18 +&
            & tmp2268/tmp55)*tmp738 + tmp128*tmp2731*(tmp2273 + tmp2672 + 6*tmp840) + tmp2412&
            &*(tmp1078 + tmp1080 + tmp111*tmp840) + (tmp179*(tmp114 + tmp184 + tmp61) + tmp22&
            &74*tmp781 + tmp842)*tmp93)*tmp966 - 128*tmp272*(4*tmp3188*((tmp111 + tmp189)/tmp&
            &55 + tmp841) + (tmp72*(tmp218 + tmp345 + tmp73) - tmp74*tmp760 + tmp60*(tmp106 +&
            & tmp761 + tmp762) + (tmp193 + tmp591 + tmp682 + tmp763)/tmp55)*tt) - 4*tmp4*((tm&
            &p143*tmp179*tmp62)/tmp10 + tmp48*tmp5*(21*tmp1650*tmp17 + (tmp2278 + tmp2347 + t&
            &mp2384 + tmp385)/tmp55 + tmp60*(tmp1042 + tmp2164 - 30*tmp61) - tmp74*(tmp214 + &
            &tmp807)) - tmp156*((tmp108 + tmp203)*tmp2482 + (tmp2156 + tmp226 + tmp502)/(tmp5&
            &4*tmp55) + (tmp226 + tmp297 + tmp767)*tmp781 + 53*tmp1650*tmp86) + (((tmp17*tmp2&
            &566)/tmp10 - tmp74*(tmp808 + tmp809) + tmp60*(77*tmp70 - 65*tmp72 + tmp810) + (t&
            &mp151 + tmp2257 + tmp2398 + tmp811)/tmp55)*tmp93)/tmp11 + tmp44*tmp6*(tmp512 - (&
            &tmp1033 + tmp189)*tmp74 + (tmp264 + tmp806 + tmp90 + tmp906)/tmp55 + (tmp155 + t&
            &mp229 + tmp805)*tmp993) + ((tmp207 + tmp369 + tmp373 + tmp602/tmp55 + tmp767 + t&
            &mp804)*tmp89*tt)/(tmp10*tmp55)))))/8. - (logs35*tmp100*tmp11*tmp240*tmp357*tmp54&
            &*tmp55*tmp59*tmp65*tmp9*tmp96*tmp99*((256*tmp157*tmp158*tmp175*tmp242*tmp249*tmp&
            &2619)/(tmp10*tmp96) + (256*tmp157*tmp158*tmp175*tmp249*(tmp251*tmp36 + tmp251*tm&
            &p684 + (tmp180 + tmp481 + (tmp2195*tmp690)/tmp11)*tmp966)*tt)/tmp100 + (256*tmp1&
            &57*tmp158*tmp180*tmp249*(tmp409 + 8*tmp32*(20*tmp36 + tmp41 + 40*tmp5) - 4*tmp4*&
            &(31*tmp1560 + tmp2521 + 30*tmp44 + tmp624) + tmp241*(7*tmp2731 + tmp525 + 20*tmp&
            &738 + 12*tmp89 + tmp93) + (tmp254 + tmp255 + tmp256 + tmp257 + tmp93)*tmp94 - 32&
            &*tmp30*(tmp253 + tt)))/tmp99 - 256*tmp157*(8192*tmp21*tmp258*tmp3*(tmp101 + tmp1&
            &02 + tmp3220) - 1024*tmp259*(tmp271*tmp624*tmp7 + tmp1560*((126*tmp1422 + tmp164&
            &4 - 75*tmp1650 + tmp264)/tmp55 + (tmp1598 + tmp261 + tmp262)*tmp60 + (tmp155 + t&
            &mp263 + tmp610)*tmp72 + tmp74*(47/tmp10 + tmp776)) + tmp567*tmp62*(tmp583 + (tmp&
            &260 + tmp78)/tmp55) + 2*tmp1535*((tmp213 + tmp216 + tmp275)*tmp60 + (tmp103 + tm&
            &p233 + tmp287)*tmp72 + tmp74*(tmp612 + tmp859) + (tmp265 + tmp2370/tmp54 + tmp58&
            &5 + tmp90)/tmp55)) + 256*tmp272*(4*tmp2731*((tmp145 - 66*tmp1650 + tmp2003 + tmp&
            &277)/tmp55 + (tmp263 + tmp275 + tmp276)*tmp60 + tmp135*(tmp236 + tmp2397 + tmp70&
            &) + (tmp1868 + tmp341)*tmp74) + tmp529*tmp62*((21/tmp10 + tmp273)/tmp55 + tmp764&
            &) + tmp5*tmp6*((tmp1883 + tmp2115 + tmp3073)*tmp373 + tmp72*(tmp347 - 66*tmp70 +&
            & 125*tmp72) + (301/tmp10 + tmp274)*tmp74 + (406*tmp1422 - 505*tmp1650 + 146*tmp8&
            &2 + tmp847)/tmp55) + tmp738*((tmp1045 + 600*tmp1422 + tmp2678 + tmp2912)/tmp55 +&
            & tmp60*(tmp1014 + 132*tmp61 - 493*tmp70) + (251/tmp10 + 149/tmp54)*tmp74 + tmp72&
            &*(tmp292 - 166*tmp70 + tmp860)) + 8*tmp282*tmp7*tmp93) + tmp966*((tmp179*tmp339*&
            &tmp62)/tmp10 + tmp215*tmp44*((-463*tmp1650 + tmp2125 + tmp2596 + tmp614)/tmp55 +&
            & tmp60*(tmp1880/tmp10 + tmp350 - 597*tmp70) + tmp72*(-163*tmp61 + 32*tmp70 + 131&
            &*tmp72) + 37*tmp376*tmp74) + tmp237*tmp518*(tmp2482*(tmp1582 + 1/tmp54) + (tmp21&
            &6 + tmp354 + tmp386)*tmp60 + tmp72*(tmp1631 + tmp70 + tmp810) + (tmp195 + tmp355&
            & + tmp586 + tmp82)/tmp55) + tmp211*tmp48*(tmp60*(tmp2972 + tmp330 - 563*tmp70) +&
            & (tmp109 + tmp138 + tmp345)*tmp72 + (333/tmp10 + tmp209)*tmp74 + (314*tmp1422 + &
            &tmp2009 + 230*tmp82 + tmp83)/tmp55) + tmp223*tmp516*((tmp352 + tmp380 - 63*tmp61&
            &)*tmp72 + (tmp185 + tmp1873)*tmp74 + (tmp1736 + tmp353 + tmp615 + tmp788)/tmp55 &
            &+ tmp60*(tmp1987 + tmp351 + tmp851)) + tmp156*(tmp60*(tmp288 + 68*tmp61 - 832*tm&
            &p70) + (610/tmp10 + tmp2528)*tmp74 + (388*tmp1422 - 532*tmp1650 + tmp348 + 222*t&
            &mp82)/tmp55 + (tmp128*(tmp1530 + tmp347 + tmp861))/tmp54)*tmp89 + (tmp111*tmp239&
            &*tmp356)/tmp9 + tmp221*tmp6*((tmp121*tmp128)/tmp54 + tmp243*(tmp2115/tmp10 + tmp&
            &2555 + tmp797 + tmp83) + tmp617*tmp843 + tmp2126*(tmp320 + tmp80 + tmp902)) + tm&
            &p143*tmp93*(tmp2594*tmp346 + tmp60*(-807*tmp70 - 103*tmp72 + tmp800) + (425*tmp1&
            &422 - 422*tmp1650 + 274*tmp82 + tmp879)/tmp55 + tmp74*(533/tmp10 + tmp986)) + tm&
            &p228*(tmp340 + (tmp2506 + tmp343 + tmp344)/(tmp10*tmp55) + tmp74*(tmp341 + tmp76&
            &) + tmp60*(tmp1859 + tmp342 + tmp80))*tt) + 16*tmp30*(2*tmp211*(tmp17*tmp327 + (&
            &129/tmp10 + tmp327)/tmp55)*tmp62 + 4*tmp215*tmp311*tmp7 + tmp48*tmp524*((tmp2190&
            &*(tmp149 + tmp301 + tmp672))/tmp54 + tmp60*(668*tmp61 - 2049*tmp70 - 1219*tmp72)&
            & + (1387/tmp10 + 507/tmp54)*tmp74 + (1848*tmp1422 - 1965*tmp1650 + 662*tmp82 + 1&
            &61*tmp83)/tmp55) + tmp6*(tmp60*(916*tmp61 - 4527*tmp70 - 3157*tmp72) + tmp72*(tm&
            &p623 - 674*tmp70 + 621*tmp72) + (2597/tmp10 + 895/tmp54)*tmp74 + (4422*tmp1422 -&
            & 3097*tmp1650 + tmp348 + 1930*tmp82)/tmp55)*tmp89 + tmp156*tmp433*(tmp72*(tmp595&
            & - 145*tmp61 + 166*tmp72) + (235/tmp10 + 112/tmp54)*tmp74 + (tmp2130 + 95*tmp61 &
            &- 141*tmp70)*tmp781 + (140*tmp1422 - 416*tmp1650 + tmp2265 + tmp899)/tmp55) + tm&
            &p515*((167/tmp10 + tmp1017)*tmp107 + tmp60*(tmp2971 - 659*tmp70 - 307*tmp72) + (&
            &tmp1057*(107*tmp61 + 36*tmp70 - 143*tmp72))/tmp10 + (447*tmp1422 - 792*tmp1650 +&
            & 158*tmp82 + 113*tmp83)/tmp55)*tmp93 + tmp143*(tmp107*(451/tmp10 + 157/tmp54) + &
            &tmp208*(tmp1044 + tmp200 - 170*tmp70) + tmp60*(432*tmp61 - 2643*tmp70 - 1741*tmp&
            &72) + (2132*tmp1422 - 1255*tmp1650 + tmp2702 + 1290*tmp82)/tmp55)*tt) + 4*tmp4*(&
            &2*tmp228*(tmp18 + tmp324/tmp55)*tmp62 + 4*tmp237*tmp338*tmp7 + tmp215*tmp516*(tm&
            &p208*(tmp1019 + tmp191 + tmp379) + (481/tmp10 + 84/tmp54)*tmp74 + (tmp233 + tmp3&
            &29 - 287*tmp70)*tmp781 + (276*tmp1422 - 586*tmp1650 + tmp1748 + 93*tmp82)/tmp55)&
            & + tmp211*tmp6*((tmp128*(tmp112 + tmp1875 + tmp326))/tmp54 + tmp60*(tmp2918 - 13&
            &03*tmp70 - 495*tmp72) + (713/tmp10 + 104/tmp54)*tmp74 + (-524*tmp1650 + tmp2404 &
            &+ tmp2585 + 590*tmp82)/tmp55) + tmp143*tmp48*(tmp60*(tmp3022 - 2143*tmp70 - 769*&
            &tmp72) + (1309/tmp10 + 206/tmp54)*tmp74 + tmp72*(tmp1876 - 15*tmp61 + tmp778) + &
            &(1506*tmp1422 - 1201*tmp1650 + tmp803 + 834*tmp82)/tmp55) + tmp156*tmp44*(tmp60*&
            &(416*tmp61 - 2079*tmp70 - 213*tmp72) + tmp72*(tmp328 - 451*tmp61 + 459*tmp72) + &
            &(1579/tmp10 + 276/tmp54)*tmp74 + (1116*tmp1422 - 1735*tmp1650 + 500*tmp82 + 140*&
            &tmp83)/tmp55) + tmp223*tmp433*(tmp72*(tmp1618 + tmp432 + tmp73) + tmp60*(tmp330 &
            &+ tmp605 + tmp773) + tmp74*(tmp273 + tmp809) + (tmp186 + tmp2387 + tmp331 + tmp9&
            &0)/tmp55) + (tmp1632*(242/tmp10 + tmp327) + tmp60*(384*tmp61 - 2470*tmp70 - 598*&
            &tmp72) + tmp72*(tmp313 - 231*tmp61 + 265*tmp72) + (1543*tmp1422 - 1713*tmp1650 +&
            & tmp2006 + 776*tmp82)/tmp55)*tmp89*tmp93 + tmp221*(tmp60*(tmp275 + tmp325 - 125*&
            &tmp72) + (tmp1531 + tmp326 + tmp61)*tmp72 + tmp74*(223/tmp10 + tmp770) + tmp243*&
            &(-166*tmp1422 + 101*tmp1650 - 214*tmp82 + tmp83))*tt) - 64*tmp252*(8*tmp143*(tmp&
            &290 + (24/tmp10 + tmp217)/tmp55)*tmp62 + 4*tmp156*tmp300*tmp7 + tmp524*tmp6*(tmp&
            &107*(187/tmp10 + tmp364) + (tmp1057*(tmp1021 + tmp218 + tmp491))/tmp10 + tmp60*(&
            &tmp383 - 927*tmp70 - 741*tmp72) + (967*tmp1422 - 794*tmp1650 + tmp611 + 366*tmp8&
            &2)/tmp55) + (tmp532*((169/tmp10 + tmp1880)*tmp74 + (-329*tmp1650 + tmp2147 + tmp&
            &293 + 67*tmp83)/tmp55 + tmp60*(tmp1855 + tmp291 + tmp834) + tmp72*(tmp292 - 123*&
            &tmp61 + tmp835)))/tmp11 + tmp48*tmp516*(tmp60*(292*tmp61 - 550*tmp70 - 314*tmp72&
            &) + (418/tmp10 + 191/tmp54)*tmp74 + (440*tmp1422 - 710*tmp1650 + tmp1729 + 132*t&
            &mp82)/tmp55 + (tmp1057*(83*tmp61 - 111*tmp72 + tmp909))/tmp10) + (tmp60*(316*tmp&
            &61 - 1477*tmp70 - 1271*tmp72) + (tmp1858 + tmp365 - 390*tmp70)*tmp72 + (755/tmp1&
            &0 + 353/tmp54)*tmp74 + (1516*tmp1422 - 877*tmp1650 + tmp771 + 722*tmp82)/tmp55)*&
            &tmp89*tt) + (tmp285*tmp36*(tmp143*tmp179*tmp62 + (tmp532*(tmp1632 + (tmp120 + tm&
            &p1633 + tmp70)/tmp55 + tmp2126*tmp71 + tmp838/tmp54))/tmp11 + tmp48*tmp5*(tmp174&
            &0 + tmp346/tmp54 + (tmp109 + tmp287 + tmp288)/tmp55 + (tmp194 + 1/tmp54)*tmp867)&
            & + (4*tmp156*tmp239)/tmp9 + tmp44*tmp6*(tmp629/tmp54 + 14*tmp74 + (tmp352 + tmp3&
            &61 + tmp762)/tmp55 + tmp324*tmp993) + (tmp89*(tmp109 + tmp207 + tmp286 + tmp372 &
            &+ tmp2638/tmp55 + tmp893)*tt)/tmp55))/tmp10 - 8*tmp32*(2*tmp221*(tmp152*tmp17 + &
            &(tmp1076 + tmp152)/tmp55)*tmp62 + 4*tmp223*tmp323*tmp7 + tmp143*tmp6*((tmp210*(t&
            &mp138 + tmp312 + tmp313))/tmp54 + tmp60*(426*tmp61 - 3233*tmp70 - 1753*tmp72) + &
            &(1803/tmp10 + 437/tmp54)*tmp74 + (2637*tmp1422 - 1736*tmp1650 + tmp640 + 1430*tm&
            &p82)/tmp55) + tmp156*tmp516*((931/tmp10 + 281/tmp54)*tmp74 + (222*tmp61 - 589*tm&
            &p70 - 137*tmp72)*tmp781 + tmp72*(-389*tmp61 + 446*tmp72 + tmp801) + (676*tmp1422&
            & - 1290*tmp1650 + tmp2916 + 163*tmp83)/tmp55) + tmp48*((tmp602*(tmp1602 + tmp314&
            & + tmp618))/tmp10 + tmp60*(800*tmp61 - 4051*tmp70 - 2009*tmp72) + (2575/tmp10 + &
            &666/tmp54)*tmp74 + (3392*tmp1422 + tmp1642 - 2983*tmp1650 + 1476*tmp82)/tmp55)*t&
            &mp89 + tmp215*tmp518*(tmp2482*(193/tmp10 + 58/tmp54) + tmp60*(tmp150 + tmp325 + &
            &204*tmp61) + tmp72*(tmp420 - 244*tmp61 + 249*tmp72) + (-589*tmp1650 + tmp1731 + &
            &51*tmp82 + tmp928)/tmp55) + tmp44*(tmp107*(869/tmp10 + 247/tmp54) + tmp60*(1052*&
            &tmp61 - 3639*tmp70 - 1317*tmp72) + (tmp2638*(211*tmp61 + 76*tmp70 - 287*tmp72))/&
            &tmp10 + (2510*tmp1422 - 3297*tmp1650 + 1032*tmp82 + 311*tmp83)/tmp55)*tmp93 + tm&
            &p211*(tmp60*(162*tmp61 - 1417*tmp70 - 657*tmp72) + (725/tmp10 + 172/tmp54)*tmp74&
            & + (831*tmp1422 - 498*tmp1650 + 692*tmp82 - 10*tmp83)/tmp55 + (tmp128*(tmp138 + &
            &tmp896 + tmp901))/tmp54)*tt))))/8. + (discbulogs1235*tmp10*tmp240*tmp411*tmp54*t&
            &mp55*tmp58*tmp59*tmp65*(-256*tmp15*tmp157*tmp2088 + 256*tmp15*tmp157*tmp3*(-((tm&
            &p158*tmp2542*tmp408)/(tmp10*tmp100)) + tmp2077*tmp564 + m2*tmp2296*tmp408*tmp564&
            &) - 256*tmp157*tt*(1024*tmp259*(tmp1847 + tmp135*tmp2705 + (tmp138 + tmp190 + tm&
            &p197)*tmp60 + (tmp1140 + tmp195 + tmp2143 + tmp82)/tmp55) - 32*tmp252*(tmp6*(tmp&
            &1861*tmp208 + tmp60*(tmp1855 + 212*tmp61 - 99*tmp70) + (105/tmp10 + 103/tmp54)*t&
            &mp74 + (-349*tmp1650 + tmp1857 + tmp791 + 109*tmp83)/tmp55) + tmp36*(tmp60*(tmp1&
            &854 - 155*tmp70 - 341*tmp72) + tmp72*(tmp109 - 313*tmp61 + 307*tmp72) + (157/tmp&
            &10 + 279/tmp54)*tmp74 + (-819*tmp1650 + tmp193 + tmp2574 + 281*tmp83)/tmp55) + t&
            &mp516*(tmp1744*(1/tmp10 + tmp185) + (tmp1422 + tmp1853 + tmp1963 + tmp2363)/tmp5&
            &5 + (tmp1851 + tmp1852 + tmp2269)*tmp60 + tmp72*(-70*tmp61 + tmp875 + tmp876))) &
            &+ 16*tmp30*(tmp1535*((tmp128*(tmp109 + tmp1858 - 359*tmp61))/tmp54 + tmp60*(1044&
            &*tmp61 - 445*tmp70 - 527*tmp72) + (463/tmp10 + 513/tmp54)*tmp74 + (177*tmp1422 -&
            & 1738*tmp1650 + tmp775 + 531*tmp83)/tmp55) + tmp1560*(tmp1744*(81/tmp10 + 152/tm&
            &p54) + tmp60*(1538*tmp61 - 387*tmp70 - 1039*tmp72) + (tmp128*(tmp109 - 445*tmp61&
            & + 439*tmp72))/tmp54 + (327*tmp1422 - 2364*tmp1650 + tmp775 + 778*tmp83)/tmp55) &
            &+ tmp524*(tmp72*(tmp109 - 173*tmp61 + 167*tmp72) + (tmp1862 + 154/tmp54)*tmp74 +&
            & (-494*tmp1650 + tmp2394 + tmp2955 + 173*tmp83)/tmp55 + tmp60*(327*tmp61 - 251*t&
            &mp72 + tmp835)) + tmp48*((146/tmp10 + tmp364)*tmp74 + tmp1861*tmp790 + tmp781*(t&
            &mp1859 + 87*tmp61 + tmp790) + (-334*tmp1650 + tmp780 + 91*tmp83 + tmp890)/tmp55)&
            &) + tmp1629*tmp7*(tmp2178*(tmp332 + (tmp103 + tmp422 + tmp507)/tmp55 + tmp132*tm&
            &p60) + tmp3178*tmp48*(tmp278 + tmp2137/tmp55 + tmp19*tmp60) + (tmp1767*tmp3)/(tm&
            &p54*tmp9) + tmp2412*(tmp1007 + tmp1866*tmp60 + (tmp276 + tmp354 + tmp92)/tmp55) &
            &+ (tmp266 + tmp279 + tmp281)*tmp93) - 8*tmp32*(tmp254*(tmp60*(tmp313 + 355*tmp61&
            & - 305*tmp72) + tmp72*(-181*tmp61 + tmp70 + 180*tmp72) + (tmp205 + 174/tmp54)*tm&
            &p74 + (-535*tmp1650 + tmp1867 + tmp1884 + 181*tmp83)/tmp55) + tmp2412*(tmp60*(69&
            &8*tmp61 - 250*tmp70 - 424*tmp72) + tmp72*(tmp1869 + 479*tmp72 + tmp73) + (256/tm&
            &p10 + 346/tmp54)*tmp74 + (197*tmp1422 - 1169*tmp1650 + tmp791 + 352*tmp83)/tmp55&
            &) + tmp2731*((tmp1816 + tmp1870 + tmp1871)*tmp372 + tmp72*(tmp369 - 763*tmp61 + &
            &759*tmp72) + (523/tmp10 + 421/tmp54)*tmp74 + (-1589*tmp1650 + tmp2162 + tmp639 +&
            & 433*tmp83)/tmp55) + 4*tmp738*(tmp2126*(129*tmp61 - 27*tmp70 - 98*tmp72) + tmp72&
            &*(-298*tmp61 + tmp70 + 297*tmp72) + 16*tmp74*(tmp1868 + tmp75) + (167*tmp1422 - &
            &807*tmp1650 + 260*tmp83 + tmp914)/tmp55) + ((tmp1877 + tmp1878 + tmp1879 + tmp19&
            &3)/tmp55 + (tmp128*(tmp1876 - 74*tmp61 + tmp70))/tmp54 + (tmp1872 + tmp1873)*tmp&
            &74 + tmp373*(tmp1874 + tmp1875 + tmp762))*tmp93) - 128*tmp272*(tmp433*(tmp1848 +&
            & (tmp155 + tmp197 + tmp420)*tmp781 + (tmp2513 + tmp355 + tmp385 + tmp83)/tmp55 +&
            & tmp135*(tmp191 + tmp61 + tmp88)) + ((tmp103 + tmp1414 + tmp1631)*tmp72 + (tmp19&
            &8 + tmp209)*tmp74 + tmp60*(tmp1849 + tmp81 + tmp865) + (tmp151 + tmp1850 + tmp34&
            &8 + tmp883)/tmp55)*tmp97) + tmp410*tmp7*tmp966*(tmp254*tmp3*(tmp17*tmp765 + (tmp&
            &128 + tmp765)/tmp55) + tmp2412*((30/tmp10 + tmp1863)*tmp60 + (tmp1864 + 39*tmp61&
            & - 78*tmp72)/tmp55 + tmp1983*tmp840) + tmp738*((tmp1862 + 133/tmp54)*tmp60 + (tm&
            &p2152 - 266*tmp72 + tmp837)/tmp55 + tmp2380*tmp86) + ((tmp1003 + tmp218 + tmp606&
            &)/tmp55 + tmp60*(tmp185 + tmp827) + tmp1631*tmp86)*tmp93 + 2*tmp2731*((tmp146 + &
            &tmp317)*tmp60 + (tmp2122*tmp86)/tmp10 + (tmp1865 + tmp216 + tmp992)/tmp55)) + 4*&
            &tmp4*(2*tmp143*tmp3*(tmp17*tmp2918 + (tmp1582 + tmp1880)*tmp60 + (195*tmp61 + tm&
            &p70 - 82*tmp72)/tmp55) + tmp44*tmp6*(-1339*tmp1650*tmp17 + tmp2126*(496*tmp61 - &
            &140*tmp70 - 351*tmp72) + (566/tmp10 + 989/tmp54)*tmp74 + (751*tmp1422 - 3315*tmp&
            &1650 + tmp791 + 995*tmp83)/tmp55) + tmp156*((tmp382 + tmp502 - 131*tmp72)/(tmp54&
            &*tmp55) + 29*tmp188*tmp74 + tmp60*(tmp1885 + tmp778 + tmp790) + tmp1882*tmp61*tm&
            &p86) + tmp48*tmp5*(-1103*tmp1650*tmp17 + tmp2126*(333*tmp61 + tmp619 - 153*tmp70&
            &) + 8*(tmp1882 + tmp364)*tmp74 + (459*tmp1422 - 2423*tmp1650 + 668*tmp83 + tmp91&
            &4)/tmp55) + ((tmp60*(tmp1883 + 438*tmp61 - 307*tmp70) + (309/tmp10 + 218/tmp54)*&
            &tmp74 + (-887*tmp1650 + tmp1884 + tmp193 + 220*tmp83)/tmp55 + 457*tmp1650*tmp86)&
            &*tmp93)/tmp11 + tmp89*(tmp107*(tmp1881 + tmp1882) + tmp60*(1420*tmp61 - 227*tmp7&
            &0 - 1181*tmp72) + 812*tmp1650*tmp86 + (569*tmp1422 - 2228*tmp1650 + 712*tmp83 + &
            &tmp914)/tmp55)*tt))))/8. + (logs1235*tmp10*tmp100*tmp11*tmp240*tmp411*tmp54*tmp5&
            &5*tmp59*tmp65*tmp96*tmp99*((256*tmp157*tmp158*tmp164*tmp175*tmp242*tmp2619)/(tmp&
            &54*tmp96) - (256*tmp157*tmp164*tmp2533*tmp36*tmp408)/(tmp96*tmp99) + (256*tmp157&
            &*tmp158*tmp164*tmp175*(tmp178*tmp36 + tmp178*tmp684 + (tmp180 + tmp470 + (tmp224&
            &7*tmp690)/tmp11)*tmp966)*tt)/tmp100 + (256*tmp157*tmp158*tmp164*tmp723*(tmp181*t&
            &mp684 + tmp181*tmp709 + (tmp180 + tmp171*tmp3 + (tmp182*tmp690)/tmp11)*tmp966)*t&
            &t)/tmp99 + 256*tmp157*(8192*(tmp101 + tmp102 - 2*tmp180)*tmp21*tmp258*tmp3 + 256&
            &*tmp272*(tmp529*(tmp17*tmp472 + (tmp349 + tmp472)/tmp55)*tmp62 + tmp2427*tmp282*&
            &tmp7 + ((828*tmp1422 - 649*tmp1650 + tmp1662 + tmp1877)/tmp55 + tmp107*(tmp2191 &
            &+ tmp631) + tmp60*(352*tmp61 - 349*tmp70 - 883*tmp72) + (tmp218 + tmp3071 - 242*&
            &tmp70)*tmp72)*tmp738 - 4*tmp2731*((tmp1745 + tmp186 + tmp187 + tmp2135)/tmp55 + &
            &(tmp191 + tmp312 - 32*tmp61)*tmp72 + (tmp185 + tmp378)*tmp74 + tmp60*(tmp1001 + &
            &tmp620 + tmp849)) + tmp5*tmp6*(tmp1632*(1/tmp10 + tmp214) + tmp60*(tmp1871 + tmp&
            &202 - 599*tmp72) + (tmp342 + tmp393 - 146*tmp70)*tmp72 + (646*tmp1422 - 239*tmp1&
            &650 + tmp682 + tmp907)/tmp55)) - 1024*tmp259*((tmp183 + (tmp108 + tmp111)/tmp55)&
            &*tmp567*tmp62 + tmp1708*tmp271*tmp7 + tmp1560*((150*tmp1422 - 113*tmp1650 + tmp2&
            &876 + tmp353)/tmp55 + (tmp1996 + tmp350 + tmp391)*tmp60 + (tmp394 + tmp61 + tmp6&
            &54)*tmp72 + (tmp1872 + tmp205)*tmp74) - 2*tmp1535*((tmp117 + tmp198)*tmp74 + tmp&
            &72*(tmp109 + tmp343 + tmp900) + tmp373*(tmp113 + tmp196 + tmp92) + (tmp186 + tmp&
            &193 + tmp787 + tmp929)/tmp55)) + tmp966*(tmp223*tmp516*(tmp60*(tmp2098 + tmp330 &
            &+ tmp619) + (tmp231 + tmp314 + tmp606)*tmp72 + (tmp214 + tmp234)*tmp74 + (120*tm&
            &p1422 + tmp1635 + tmp235 + tmp780)/tmp55) + tmp215*tmp44*(tmp60*(tmp291 + 227*tm&
            &p70 - 831*tmp72) + (365*tmp61 - 116*tmp70 - 249*tmp72)*tmp72 + 39*tmp2483*tmp74 &
            &+ (690*tmp1422 - 187*tmp1650 + tmp2390 + tmp795)/tmp55) + tmp211*tmp48*(tmp60*(t&
            &mp232 + tmp2969 - 1099*tmp72) + (tmp2259 + tmp231 - 230*tmp70)*tmp72 + (tmp2 + 3&
            &13/tmp54)*tmp74 + (1022*tmp1422 - 771*tmp1650 + tmp791 + 111*tmp83)/tmp55) + tmp&
            &237*tmp518*((-25/tmp10 + 1/tmp54)*tmp74 + tmp72*(tmp236 + tmp382 + tmp77) + (tmp&
            &1722/tmp10 + tmp187 + tmp277 + tmp83)/tmp55 + tmp781*(tmp1003 + tmp61 + tmp876))&
            & + tmp156*((1156*tmp1422 - 554*tmp1650 + tmp2562 + tmp2692)/tmp55 + (-152/tmp10 &
            &+ 251/tmp54)*tmp74 + (170*tmp61 + 93*tmp70 - 641*tmp72)*tmp781 + (tmp128*(tmp161&
            &9 + 194*tmp61 + tmp794))/tmp54)*tmp89 + (tmp339*tmp62*tmp76)/tmp9 + tmp221*tmp6*&
            &((tmp1580 + tmp274)*tmp74 + (tmp229 + tmp231 - 258*tmp72)*tmp781 + (tmp138 + tmp&
            &216 + tmp666)*tmp790 + (478*tmp1422 - 374*tmp1650 + tmp230 + tmp90)/tmp55) + tmp&
            &143*((1319*tmp1422 + tmp1636 - 849*tmp1650 + tmp1766)/tmp55 + (tmp233 + 259*tmp6&
            &1 - 274*tmp70)*tmp72 + (-60/tmp10 + 347/tmp54)*tmp74 + (tmp3020 + tmp510 + 237*t&
            &mp61)*tmp781)*tmp93 + (tmp239*tmp356*tmp987)/tmp9 + tmp228*((109*tmp1422 + tmp15&
            &1 - 77*tmp1650 + tmp227)/tmp55 + (tmp225 + tmp313 + tmp61)*tmp72 + (tmp128 + tmp&
            &479)*tmp74 + tmp2126*(tmp226 + tmp236 + tmp77))*tt) - 8*tmp32*(2*tmp221*(tmp17*t&
            &mp589 + (tmp210 + tmp589)/tmp55)*tmp62 - 4*tmp223*tmp323*tmp7 + tmp156*tmp516*((&
            &-83/tmp10 + tmp1032)*tmp2371 + tmp60*(tmp329 + 275*tmp70 - 1353*tmp72) + (tmp592&
            &*(tmp1874 + tmp213 + tmp81))/tmp10 + (1246*tmp1422 + tmp2697 + tmp661 + 57*tmp82&
            &)/tmp55) + tmp143*tmp6*(tmp60*(2538*tmp61 - 730*tmp70 - 6368*tmp72) + tmp72*(373&
            &*tmp61 - 1430*tmp70 + 1057*tmp72) + 10*(193/tmp54 + tmp587)*tmp74 + (5907*tmp142&
            &2 + tmp1625 - 4615*tmp1650 + 608*tmp83)/tmp55) - (2*tmp215*((tmp2359 - 335*tmp61&
            & + tmp645)*tmp72 + tmp60*(tmp1885 - 234*tmp70 + 424*tmp72) + (239/tmp10 + tmp214&
            &)*tmp74 + (-334*tmp1422 + tmp1724 + tmp230 + tmp888)/tmp55))/tmp11 + tmp48*(tmp6&
            &0*(2564*tmp61 - 405*tmp70 - 7419*tmp72) + tmp72*(1099*tmp61 - 1476*tmp70 + 377*t&
            &mp72) + (125/tmp10 + 1894/tmp54)*tmp74 + (6980*tmp1422 + tmp1640 - 4689*tmp1650 &
            &+ 280*tmp82)/tmp55)*tmp89 + tmp44*(tmp60*(1092*tmp61 + 177*tmp70 - 5173*tmp72) +&
            & tmp208*(525*tmp61 - 344*tmp70 - 181*tmp72) + (-405/tmp10 + 961/tmp54)*tmp74 + (&
            &4922*tmp1422 - 1933*tmp1650 + tmp212 + 228*tmp82)/tmp55)*tmp93 + tmp211*(tmp60*(&
            &954*tmp61 - 374*tmp70 - 2492*tmp72) + tmp72*(tmp199 - 692*tmp70 + 659*tmp72) + (&
            &192/tmp10 + 823/tmp54)*tmp74 + (2361*tmp1422 - 1777*tmp1650 + tmp212 + 182*tmp82&
            &)/tmp55)*tt) + 16*tmp30*(2*tmp211*((129*tmp17)/tmp54 + (tmp205 + 129/tmp54)/tmp5&
            &5)*tmp62 - 4*tmp215*tmp311*tmp7 + tmp48*tmp524*(tmp60*(928*tmp61 - 189*tmp70 - 3&
            &339*tmp72) + (tmp2576 + tmp2917 - 662*tmp70)*tmp72 + (tmp2190 + 727/tmp54)*tmp74&
            & + (3204*tmp1422 - 1721*tmp1650 + tmp1982 + 201*tmp83)/tmp55) - (4*tmp156*((tmp1&
            &637 + tmp2133 + tmp2575 + tmp599)/tmp55 + tmp60*(88*tmp61 - 103*tmp70 + 211*tmp7&
            &2) + tmp74*(124/tmp10 + tmp765) + (tmp2528*tmp855)/tmp10))/tmp11 + tmp6*(tmp60*(&
            &3368*tmp61 - 1401*tmp70 - 8735*tmp72) + tmp72*(667*tmp61 - 1930*tmp70 + 1263*tmp&
            &72) + (727/tmp10 + 2549/tmp54)*tmp74 + (8190*tmp1422 - 6191*tmp1650 + 674*tmp82 &
            &+ 819*tmp83)/tmp55)*tmp89 + tmp515*((tmp2189 + tmp234)*tmp2482 + (tmp1655 + tmp1&
            &659 + tmp206 + tmp2564)/tmp55 + tmp60*(tmp196 + 70*tmp70 - 716*tmp72) + (343*tmp&
            &61 - 158*tmp70 - 185*tmp72)*tmp72)*tmp93 + tmp143*(tmp60*(1868*tmp61 - 1059*tmp7&
            &0 - 4761*tmp72) + tmp208*(tmp637 - 430*tmp70 + 409*tmp72) + (549/tmp10 + 1579/tm&
            &p54)*tmp74 + (4472*tmp1422 - 3447*tmp1650 + 510*tmp82 + 289*tmp83)/tmp55)*tt) + &
            &tmp285*tmp36*(tmp616 + tmp48*tmp5*((tmp193 + tmp195 + tmp331 + tmp649)/tmp55 + t&
            &mp201*tmp72 + (tmp194 + tmp580)*tmp74 + tmp60*(tmp112 + tmp232 + tmp836)) + tmp8&
            &68 + tmp44*tmp6*(tmp60*(tmp191 + tmp351 + tmp609) + (tmp2 + tmp217)*tmp74 + (tmp&
            &128*tmp786)/tmp54 + (tmp192 + tmp193 + tmp903 + tmp913)/tmp55) + (tmp532*(tmp72*&
            &(tmp196 + tmp197 + tmp77) + tmp60*(tmp345 + tmp359 + tmp80) + (tmp187 + tmp206 +&
            & tmp355 + tmp83)/tmp55 + tmp74*(1/tmp54 + tmp987)))/tmp11 + ((tmp115/tmp10 + tmp&
            &2126*(tmp189 + tmp2538) + (tmp155 + tmp1595 + tmp301)/tmp55 + tmp617)*tmp89*tt)/&
            &tmp54) - 64*tmp252*(-4*tmp156*tmp300*tmp7 + 8*tmp143*tmp62*(tmp17*tmp798 + (tmp1&
            &98 + tmp798)/tmp55) + tmp524*tmp6*(tmp2482*(71/tmp10 + 216/tmp54) + (tmp219 - 15&
            &1*tmp70 - 817*tmp72)*tmp781 + tmp208*(tmp598 - 122*tmp70 + tmp875) + tmp1574*(-2&
            &11*tmp1650 + tmp220 + tmp2517 + tmp917)) + tmp48*tmp516*(tmp60*(tmp2566 + tmp494&
            & - 674*tmp72) + (-110/tmp10 + 73/tmp54)*tmp74 + (668*tmp1422 - 90*tmp1650 + 56*t&
            &mp82 - 25*tmp83)/tmp55 + (tmp201*tmp926)/tmp10) - (2*((-181*tmp1650 + tmp2385 + &
            &tmp2502 + tmp2505)/tmp55 + tmp72*(tmp344 - 135*tmp61 + 101*tmp72) + tmp60*(tmp20&
            &2 + tmp2581 + 111*tmp72) + (97/tmp10 + tmp1575)*tmp74)*tmp93)/tmp11 + (tmp60*(10&
            &72*tmp61 - 817*tmp70 - 2687*tmp72) + (tmp199 + tmp392 - 722*tmp70)*tmp72 + (427/&
            &tmp10 + 897/tmp54)*tmp74 + (2512*tmp1422 - 1969*tmp1650 + tmp363 + 390*tmp82)/tm&
            &p55)*tmp89*tt) + 4*tmp4*(2*tmp228*(tmp290 + (1/tmp10 + tmp217)/tmp55)*tmp62 - 4*&
            &tmp237*tmp338*tmp7 + tmp156*tmp44*(tmp60*(556*tmp61 + 435*tmp70 - 2867*tmp72) + &
            &(1079*tmp61 - 500*tmp70 - 579*tmp72)*tmp72 + (-443/tmp10 + 464/tmp54)*tmp74 + (2&
            &592*tmp1422 - 837*tmp1650 + tmp371 + 92*tmp83)/tmp55) + tmp211*tmp6*(tmp2126*(tm&
            &p1000 + tmp1589 - 637*tmp72) + tmp72*(123*tmp61 - 590*tmp70 + 467*tmp72) + (94/t&
            &mp10 + 789/tmp54)*tmp74 + (2359*tmp1422 - 1851*tmp1650 + 78*tmp82 + 231*tmp83)/t&
            &mp55) + tmp143*tmp48*(tmp60*(1532*tmp61 - 101*tmp70 - 4111*tmp72) + tmp72*(475*t&
            &mp61 - 834*tmp70 + 359*tmp72) + (3834*tmp1422 - 2787*tmp1650 + tmp2519 + 410*tmp&
            &83)/tmp55 + tmp74*(1122/tmp54 + tmp848)) + tmp223*tmp433*((tmp114 + tmp362 - 67*&
            &tmp72)*tmp72 + (-57/tmp10 + 1/tmp54)*tmp74 + tmp2523*(-95/tmp54 + tmp799) + tmp2&
            &43*(tmp151 + tmp769 + tmp83 + tmp878)) + tmp215*tmp516*((tmp262 + tmp2877 + 388*&
            &tmp61)*tmp72 + (-228/tmp10 + 67/tmp54)*tmp74 + tmp60*(237*tmp70 - 719*tmp72 + tm&
            &p777) + (582*tmp1422 + tmp186 + tmp2393 + tmp918)/tmp55) + ((tmp1016 + 3769*tmp1&
            &422 - 2102*tmp1650 + tmp293)/tmp55 + (tmp128*(tmp222 + 459*tmp61 - 388*tmp70))/t&
            &mp54 + tmp60*(1180*tmp61 + 163*tmp70 - 4027*tmp72) + (-197/tmp10 + 900/tmp54)*tm&
            &p74)*tmp89*tmp93 + tmp221*((712*tmp1422 + tmp1730 + tmp220 + tmp2386)/tmp55 + tm&
            &p60*(tmp219 + tmp2878 - 743*tmp72) + tmp72*(tmp218 - 214*tmp70 + 205*tmp72) + tm&
            &p74*(245/tmp54 + tmp930))*tt))))/8. + (scalarc0s*tmp11*tmp240*tmp357*tmp411*tmp5&
            &3*tmp56*tmp59*tmp65*tmp96*(256*tmp1430*tmp157*tmp158*tmp177 - (256*tmp1449*tmp15&
            &7*tmp158*tmp2541*tmp713)/(tmp10*tmp96) - (256*tmp1440*tmp157*tmp1650*tmp36*tmp71&
            &3)/tmp96 + (256*tmp157*tmp1584*tmp177*(tmp965 + (tmp249*(tmp1607 + tmp1663 + tmp&
            &1767 + tmp2200 - 24*tmp3187*tmp32 + 4*(tmp1769 + tmp2520 + 64*tmp36)*tmp4 + 14*t&
            &mp738 + tmp93 + (55*tmp1560 + tmp1664 + tmp1668 + tmp752)*tmp966))/(tmp11*tmp96)&
            & + tmp158*tmp249*tmp723*tmp968))/(tmp54*tmp55) + (256*tmp157*tmp250*tmp713*(tmp1&
            &776 + tmp1786 + tmp3129 - (tmp695*tmp945)/tmp10)*tt)/(tmp10*tmp55*tmp96) - (256*&
            &tmp157*tmp72*tt*(tmp1046 + tmp175*tmp3218*tmp469*tmp968 + (tmp469*tmp94*(-(tmp17&
            &5*(tmp180 + tmp2433 + tmp2434 + tmp1812*tmp36 + tmp470)) + tmp1056*tmp158*tmp713&
            &*tt))/tmp96))/tmp55 - (256*tmp157*tmp158*tmp72*(tmp1059 + tmp3218*tmp564*tmp723*&
            &tmp968 + (tmp564*tmp94*(-((tmp2766 + tmp158*(tmp2767 + 2*tmp3*tmp713))*tmp723) +&
            & tmp1074*tmp158*tmp713*tt))/tmp96))/tmp55 + (256*tmp157*tt*(tmp158*tmp175*tmp177&
            &*tmp249*tmp968 + tmp972 + (tmp249*(-(tmp175*(tmp180 + tmp2424 + tmp2425 + tmp181&
            &5*tmp36 + tmp481)) + tmp158*tmp713*tmp983*tt))/(tmp11*tmp96)))/(tmp54*tmp55*tmp9&
            &)))/16. - (discbs*tmp11*tmp13*tmp240*tmp357*tmp411*tmp53*tmp56*tmp59*tmp65*((256&
            &*tmp1053*tmp157*(m2*tmp158*tmp249*tmp3214*tmp690*tmp7 + tmp158*tmp1720*tmp249*tm&
            &p755 + tmp158*tmp249*tmp697*(tmp251*tmp5 + tmp1823*tmp714 - 16*tmp4*((tmp188 + 1&
            &/tmp55)/tmp11 + tmp756) + m2*(-2*tmp1718*tmp5 + tmp757 + tmp758)) + (tmp249*tmp4&
            &46)/(tmp10*tmp11*tmp96) + (tmp36*tmp685*tmp689*tmp701)/(tmp10*tmp96) - (tmp158*t&
            &mp175*tmp249*tmp7*tmp713)/tmp96))/(tmp54*tmp9) + 256*tmp1053*tmp157*tmp72*(m2*tm&
            &p469*tmp685*tmp690*tmp7*tmp701 + tmp158*tmp1720*tmp469*tmp755 + tmp158*tmp697*(t&
            &mp469*(tmp178*tmp5 + (tmp179 + tmp189)*tmp714 - 16*tmp4*(tmp1845/tmp11 + tmp756)&
            & + m2*(tmp5*(-4/tmp55 + tmp580) + tmp757 + tmp758)) + (tmp1584*tmp627*tmp701)/tm&
            &p96) + (tmp446*tmp469)/(tmp11*tmp54*tmp96) - (tmp158*tmp175*tmp469*tmp7*tmp713)/&
            &tmp96) + (256*tmp157*tmp1584*tmp177*(m2*tmp249*tmp685*tmp690*tmp712 + (tmp158*tm&
            &p249*tmp735)/tmp11 + tmp36*tmp685*(tmp249*((-8*m2)/tmp100 + tmp2618 + tmp5) + (t&
            &mp1777*tmp712)/(tmp10*tmp96)) - (tmp158*tmp249*tmp713*tmp723)/tmp96))/(tmp54*tmp&
            &55) - (256*tmp1535*tmp157*((tmp446*tmp695)/tmp10 + tmp158*((tmp695*tmp703)/tmp10&
            & + (tmp158*tmp25*tmp701*tmp72)/tmp55)*tmp97))/(tmp10*tmp55*tmp9*tmp96) + (256*tm&
            &p157*tmp1584*tmp2541*(-(tmp158*tmp522*tmp579) + (tmp1693*tmp25*tmp685*tmp712)/(t&
            &mp54*tmp55) + tmp522*tmp685*tmp703*tt))/(tmp10*tmp96) - (256*tmp1535*tmp157*tmp1&
            &650*(tmp446*tmp708 + tmp158*((tmp158*tmp25*tmp701)/(tmp54*tmp55) + tmp703*(tmp46&
            &7 + tmp706/tmp55))*tt))/(tmp9*tmp96) + 256*tmp157*tmp158*tmp177*((2048*tmp259*tm&
            &p2646)/(tmp54*tmp55) - 512*tmp272*((tmp3221*(tmp1848 + (tmp131 + tmp192 + tmp213&
            &8 + tmp2399)/tmp55 + (tmp150 + tmp226 + tmp297)*tmp60 + (tmp1868/tmp10 + tmp584 &
            &+ tmp608)*tmp72))/tmp54 + (tmp17*tmp2435*((tmp135 + tmp61 + tmp70)/tmp55 + tmp74&
            & + tmp17*tmp781 + tmp830))/tmp10) + 128*tmp252*((tmp697*(tmp636*(1/tmp10 + tmp65&
            &3) + (-43*tmp1650 + tmp2142 + tmp277 + tmp674)/(tmp54*tmp55) + (tmp103 + tmp1988&
            & + tmp633)*tmp74 + tmp60*(tmp1645 + tmp1874/tmp10 + tmp192 + tmp82) + tmp872))/t&
            &mp10 + (tmp470*((184*tmp1422 + tmp1621 - 225*tmp1650 + tmp1658)/tmp55 + tmp60*(t&
            &mp607 - 163*tmp70 - 123*tmp72) + (tmp327 + tmp631)*tmp74 + tmp72*(-41*tmp61 - 10&
            &2*tmp70 + tmp912)))/tmp54 + (tmp160*tmp6*((tmp2638*tmp632)/tmp10 + tmp267*tmp74 &
            &+ (tmp145 + tmp634 + tmp83 + tmp883)/tmp55 + tmp781*(tmp61 + tmp92)))/tmp10) + 8&
            &*tmp32*(tmp738*(tmp2684 + tmp636*(tmp229 + 75*tmp61 + tmp645) + tmp2482*(-490*tm&
            &p1422 + 56*tmp1650 + tmp2134 + tmp646) + tmp1772*tmp72*(103*tmp1422 - 302*tmp165&
            &0 + tmp647 + 77*tmp82) + tmp60*(tmp2688 + 75*tmp641 + 850*tmp642 - 150*tmp61*tmp&
            &70 - (416*tmp83)/tmp10)) + tmp5*tmp6*(tmp2482*(-198*tmp1422 - 100*tmp1650 + tmp2&
            &582 + tmp648) + tmp636*(tmp191 + tmp2124 + 160*tmp72) + (tmp670*(-169*tmp1422 + &
            &210*tmp1650 + tmp2661 - 56*tmp83))/tmp10 + tmp881 + tmp60*(tmp1623 + tmp2584 + 4&
            &26*tmp61*tmp70 - (472*tmp83)/tmp10 + tmp884)) + tmp1227*((tmp1022 - 87*tmp1650 +&
            & tmp2504 + tmp585)/tmp55 + tmp60*(tmp1759 + tmp1989 + tmp644) + (tmp1620 + tmp27&
            &10/tmp10 + tmp287)*tmp72 + (tmp273 + tmp341)*tmp74)*tmp89 + 8*tmp2731*(((-44*tmp&
            &1422 + 47*tmp1650 + tmp371 + tmp640)*tmp72)/tmp55 + tmp636*(tmp155 + tmp343 + tm&
            &p73) + tmp911 + tmp74*(-25*tmp1422 + tmp649 + tmp667 + tmp914) + tmp60*(tmp1817*&
            &tmp61 + tmp669 + tmp771/tmp10 + tmp886 + tmp915)) + tmp111*tmp160*(-(tmp2175/tmp&
            &10) + tmp127*tmp74 + (tmp105 + tmp138 + tmp77)*tmp781 + (tmp192 + tmp650 + tmp65&
            &1 + tmp82)/tmp55)*tmp93) - 32*tmp30*(2*tmp1535*(2*(tmp155 + tmp191 + tmp286)*tmp&
            &636 + tmp74*(tmp639 + tmp683 + tmp852 + tmp877) + tmp887 + tmp60*(tmp1753 + tmp6&
            &42 + tmp2124*tmp70 - (56*tmp83)/tmp10 + tmp889) + (tmp72*(-49*tmp1422 + tmp2168 &
            &+ tmp640 + tmp90))/tmp55) + tmp1560*(tmp663 + tmp1772*(tmp1647 + tmp2260 + tmp22&
            &77 + tmp638)*tmp72 + tmp636*(tmp637 + tmp676 + tmp77) + tmp60*(tmp2396 + 344*tmp&
            &642 + tmp665 - 132*tmp61*tmp70 - (112*tmp83)/tmp10) + tmp2482*(tmp348 + tmp1862*&
            &tmp61 + tmp82 + tmp924)) + tmp2304*tmp76*((217*tmp1422 - 261*tmp1650 + tmp2004 +&
            & tmp635)/tmp55 + tmp72*(tmp2281 - 44*tmp61 + 173*tmp72) + tmp781*(-107*tmp70 - 6&
            &6*tmp72 + tmp865) + tmp74*(tmp2279 + tmp969)) + tmp128*tmp160*tmp48*(tmp119 + (t&
            &mp328 + tmp419 + tmp421)*tmp60 + (tmp277 + tmp353 + tmp385 + tmp891)/tmp55 + tmp&
            &2175*tmp987)) + tmp470*tmp72*(-4*tmp156*tmp160*tmp671 + tmp44*tmp6*(tmp1726 + tm&
            &p651 + tmp652 + tmp60*(57/tmp10 + tmp653) + (tmp149 + tmp233 + tmp654)/tmp55 + t&
            &mp662 - 20*tmp74) + (tmp532*(tmp145 + (tmp155 + tmp286 + tmp606)/tmp55 + tmp656 &
            &+ tmp657 + tmp60*(tmp360 + tmp658) + tmp664 - 6*tmp74))/tmp11 + tmp1708*tmp5*(tm&
            &p1613 + tmp1744 + tmp264 + (tmp105 + tmp1732 + tmp358)/tmp55 + tmp648 + tmp655 +&
            & (tmp189 + tmp2192)*tmp781) - (4*tmp143*tmp160*tmp3)/tmp9 + (tmp136 + tmp145 + t&
            &mp651 + tmp656 + tmp60*(tmp317 + tmp658) + (tmp1624 + tmp218 + tmp72)/tmp55 - 12&
            &*tmp74)*tmp89*tt) + m2*tmp518*((tmp156*tmp1580*tmp160*tmp671)/(tmp54*tmp55) + (t&
            &mp532*(tmp663 + tmp681 + tmp60*(tmp1657 + tmp2876/tmp54 + tmp665 + tmp669 + tmp1&
            &722*tmp70) + tmp2482*(-31*tmp1422 + tmp227 + tmp2350 + tmp82) + (tmp670*(tmp1630&
            & + tmp682 + tmp683 + tmp82))/tmp10))/tmp11 + tmp1224*tmp143*(tmp135*(tmp345 + tm&
            &p61 + tmp672) + tmp334*tmp74 + tmp60*(tmp366 + tmp628 + tmp80) + (tmp649 + tmp67&
            &3 + tmp674 + tmp83)/tmp55) + tmp5*tmp624*(tmp663 + tmp681 + tmp1772*(tmp151 - 22&
            &*tmp1650 + tmp2686 + tmp353)*tmp72 + tmp2482*(tmp1752 + tmp227 + tmp2676 + tmp82&
            &) + tmp60*(tmp1954 + tmp2346 + tmp665 + tmp669 - (28*tmp83)/tmp10)) + tmp44*tmp6&
            &*(tmp1754 + tmp60*(tmp1652 + tmp1756 + tmp2687 + tmp1414*tmp70 - (115*tmp83)/tmp&
            &10) + ((-159*tmp1650 + tmp1993 + tmp2353 + tmp678)*tmp844)/tmp54 + tmp74*(-297*t&
            &mp1422 + tmp1654 + tmp677 + tmp90) + tmp636*(tmp1985 + tmp676 + tmp92)) + (tmp66&
            &3 + tmp60*(tmp1616 + 172*tmp642 + tmp665 + tmp675 + tmp289*tmp70) + tmp636*(tmp2&
            &18 + tmp2264 + tmp77) + tmp74*(-227*tmp1422 + tmp151 + 31*tmp1650 + tmp772) + ((&
            &tmp1651 + tmp2652 + tmp678 + tmp796)*tmp844)/tmp54)*tmp89*tt) - 4*tmp4*((tmp1224&
            &*tmp156*tmp160*tmp671)/tmp10 + tmp1227*tmp143*((tmp1750 + tmp2668 + tmp656 + tmp&
            &661)/tmp55 + (tmp207 + tmp595 + tmp605)*tmp72 + (tmp203 + tmp260)*tmp74 + (tmp35&
            &4 + tmp366 + tmp660)*tmp781) + tmp44*tmp6*(tmp1758 + (tmp1627 + tmp1733 + tmp224&
            &)*tmp636 + tmp1772*(107*tmp1422 - 183*tmp1650 + tmp2136 + tmp2811)*tmp72 + tmp24&
            &82*(-273*tmp1422 + tmp2379 + tmp662 + tmp774) + tmp60*(tmp1762 + tmp2010 + tmp22&
            &63 + tmp2289 - (362*tmp83)/tmp10)) + tmp5*tmp624*(tmp663 + tmp1772*(tmp1646 + tm&
            &p1867 + tmp2577/tmp54 + tmp585)*tmp72 + tmp636*(tmp2167 + tmp218 + tmp77) + tmp2&
            &482*(tmp1615 + tmp235 + tmp664 + tmp82) + tmp60*(tmp1972 + tmp665 + tmp675 + 84*&
            &tmp61*tmp70 - (84*tmp83)/tmp10)) + (tmp532*(tmp61*tmp632*tmp70 + tmp636*(tmp155 &
            &+ tmp666 + tmp70) + (tmp193 + tmp2157/tmp54 + tmp667)*tmp74 + (tmp670*(43*tmp165&
            &0 + tmp3074 + tmp668 + tmp811))/tmp10 + tmp60*(28*tmp642 + tmp643 + tmp669 + tmp&
            &2566*tmp70 - (50*tmp83)/tmp10)))/tmp11 + (tmp1725 + tmp636*(tmp420 + tmp613 + 21&
            &2*tmp72) + tmp2482*(-332*tmp1422 + 30*tmp1650 + tmp668 + 45*tmp83) + tmp1772*tmp&
            &72*(87*tmp1422 - 202*tmp1650 + 43*tmp82 + 72*tmp83) + tmp60*(tmp1727 + 45*tmp641&
            & + 538*tmp642 - (296*tmp83)/tmp10 + tmp885))*tmp89*tt)) + (256*tmp157*tmp158*tmp&
            &72*(m2*tmp3220*tmp564*tmp685*tmp712 + (tmp2533*tmp564*tmp579)/(tmp11*tmp96) + (t&
            &mp3218*tmp564*tmp713*tmp723)/tmp96 + tmp158*tmp1720*tmp564*tmp735*tt + tmp685*tm&
            &p697*(-(tmp564*(-32*tmp3*tmp32 + tmp2618*(tmp177 + tmp3226) + tmp181*tmp5 + (tmp&
            &1557*tmp3 + tmp535*tmp7 + tmp736)*tmp966)) + (tmp3*tmp712*tmp985*tt)/(tmp9*tmp96&
            &))))/tmp55))/16. + (discbtlogs1235*tmp10*tmp240*tmp411*tmp54*tmp55*tmp59*tmp65*t&
            &mp67*(-256*tmp157*(tmp2341*tmp469 + (tmp158*tmp175*tmp241*tmp242*tmp469)/tmp54 +&
            & (tmp135*tmp158*tmp175*tmp242*tmp2542)/tmp99)*tt - 256*tmp157*tt*(-(tmp2318*tmp4&
            &69) + (tmp158*tmp175*tmp241*tmp242*tmp469)/tmp55 + (tmp1584*tmp175*tmp242*tmp627&
            &*tt)/tmp99) - 256*tmp15*tmp157*(tmp759 - 16*tmp30*(tmp524*(tmp594/tmp55 + tmp601&
            &)*tmp62 + tmp1535*(20*tmp119 + (-413*tmp1650 + tmp2593 + tmp368 + tmp647)/tmp55 &
            &+ tmp60*(262*tmp61 - 92*tmp70 - 242*tmp72) + tmp72*(tmp596 + tmp597 + 139*tmp72)&
            &) + tmp1560*((165*tmp1422 + tmp1643 + tmp368 + tmp370)/tmp55 + tmp1744*tmp594 + &
            &(tmp1856 + tmp596 - 25*tmp61)*tmp72 + (tmp1043 + tmp595 + tmp621)*tmp781) + tmp4&
            &8*((tmp1057*(tmp1816 + tmp365 + tmp369))/tmp10 + (-224*tmp1650 + tmp371 + tmp599&
            & + tmp600)/tmp55 + (tmp1002 + tmp2567 + tmp598)*tmp781 + tmp74*(69/tmp54 + tmp80&
            &7))) - tmp475*((tmp1577*tmp3*tmp686)/tmp54 + tmp2412*tmp688 + tmp2179*(tmp603 + &
            &(tmp184 + tmp191 + tmp61)/tmp55 + tmp829) + (tmp3226*tmp48)/(tmp54*tmp9) + (tmp3&
            &*tmp89)/(tmp54*tmp9)) + 32*tmp252*(tmp516*(tmp146*tmp17 + (tmp146 + tmp349)/tmp5&
            &5)*tmp62 + tmp6*(tmp1063*(tmp151 - 51*tmp1650 + tmp590 + tmp591) + tmp60*(tmp202&
            & - 55*tmp70 - 57*tmp72) + (tmp114 + tmp347 + tmp633)*tmp72 + (tmp303 + tmp589)*t&
            &mp74) + tmp36*((tmp103 + tmp312 - 27*tmp61)*tmp72 + tmp74*(tmp587 + tmp832) + tm&
            &p60*(152*tmp61 - 171*tmp72 + tmp837) + (tmp2871 + tmp588 + tmp646 + tmp90)/tmp55&
            &)) + 8*tmp32*(tmp254*(tmp1075 + tmp604/tmp55)*tmp62 + tmp738*(tmp1632*tmp604 + t&
            &mp60*(tmp607 + tmp608 - 135*tmp72) + (tmp361 + tmp605 + tmp606)*tmp72 + (102*tmp&
            &1422 - 131*tmp1650 + tmp385 + tmp768)/tmp55) + tmp2731*(tmp107*(tmp2356 + tmp612&
            &) + (tmp2638*(tmp369 + tmp390 + tmp613))/tmp10 + tmp60*(tmp391 + 282*tmp61 - 259&
            &*tmp72) + (148*tmp1422 - 453*tmp1650 + tmp385 + 135*tmp83)/tmp55) + tmp2412*((tm&
            &p114 + tmp2874 + tmp610)*tmp72 + (tmp1004 + tmp327)*tmp74 + (tmp2094 + tmp229 + &
            &tmp609)*tmp781 + (tmp151 - 119*tmp1650 + tmp611 + tmp898)/tmp55) + ((tmp151 + tm&
            &p2161/tmp54 + tmp614 + tmp615)/tmp55 + 12*tmp60*(tmp229 + tmp354 + tmp72) + (82/&
            &tmp10 + tmp146)*tmp74 + tmp72*(tmp103 - 61*tmp61 + tmp846))*tmp93) + m2*tmp3220*&
            &(tmp2412*(tmp1077 + (tmp190 + tmp422 + tmp507)/tmp55 + tmp118*tmp60) + tmp3*(tmp&
            &2149/tmp55 + tmp601)*tmp738 + 2*tmp2731*((tmp114 + tmp138 + tmp139)/tmp55 + tmp6&
            &0*tmp630 + tmp198*tmp840) + (tmp2233*tmp3)/(tmp54*tmp9) + ((tmp366 + tmp210/tmp5&
            &4 + tmp584)/tmp55 + tmp60*(tmp360 + tmp602) + tmp2594*tmp86)*tmp93) - 4*tmp4*(tm&
            &p616 - tmp156*(34*tmp1650*tmp17 + (tmp138 + tmp2674 + tmp502)/(tmp54*tmp55) + tm&
            &p60*(tmp1852 + tmp226 + 53*tmp70) + (-53/tmp10 + tmp108)*tmp74) + tmp44*tmp6*((t&
            &mp1057*(tmp191 + tmp200 + tmp320))/tmp10 + (tmp199 + tmp618 - 112*tmp72)/(tmp54*&
            &tmp55) + tmp60*(tmp190 + 72*tmp61 - 88*tmp72) + (tmp111 + tmp1863)*tmp74) + ((12&
            &*tmp60*(tmp190 + tmp218 + tmp660) + (tmp623 + 55*tmp70 - 199*tmp72)/(tmp54*tmp55&
            &) + (tmp103 - 81*tmp61 + tmp622)*tmp72 + (48/tmp10 + 55/tmp54)*tmp74)*tmp93)/tmp&
            &11 + tmp48*tmp5*(tmp60*(tmp595 + 176*tmp61 + tmp619) + (tmp1601 + tmp621 - 272*t&
            &mp72)/(tmp54*tmp55) + tmp107*(tmp1610 + tmp75) + ((tmp301 + tmp620 + tmp70)*tmp9&
            &87)/tmp54) + (((tmp138 + tmp213 + tmp2845)/tmp55 - (tmp197 + tmp369 + tmp61)/tmp&
            &10 + tmp617 + (tmp117 + tmp1617)*tmp781)*tmp89*tt)/tmp54) - 128*tmp272*(4*tmp318&
            &8*(tmp1866/tmp55 + tmp583) + ((tmp193 + tmp585 + tmp586 + tmp591)/tmp55 + (tmp13&
            &4 + tmp330 + tmp420)*tmp60 + tmp74*tmp760 + tmp72*tmp990)*tt))))/8. + (discbslog&
            &t*tmp11*tmp13*tmp16*tmp240*tmp357*tmp411*tmp53*tmp56*tmp65*((-256*tmp157*tmp177*&
            &(tmp158*tmp241*tmp249*tmp579 + tmp249*tmp518*(tmp1451 + tmp1606 - 4*(tmp1461 + t&
            &mp1464 + 117*tmp1560 + tmp2174)*tmp4 + 8*tmp32*(tmp1455 + 106*tmp36 + 15*tmp6) -&
            & 128*tmp30*(tmp1452 + tmp686) + tmp241*(tmp1467 + tmp1607 + tmp1790*tmp6 + 52*tm&
            &p738 + tmp93)) + (tmp158*tmp2541*tmp579)/(tmp10*tmp96)))/(tmp54*tmp55*tmp9) + (2&
            &56*tmp157*tmp2541*(-2*(tmp1606 + tmp1665 + tmp1802 + 8*tmp32*(tmp1667 + tmp172 +&
            & 43*tmp36) - (4*tmp4*(tmp1803 + tmp435 + tmp452))/tmp11 + m2*tmp433*(tmp1604 + 1&
            &4*tmp44 + tmp48 + tmp2176*tmp5))*tmp522 + (tmp158*tmp579*tmp705)/tmp55))/(tmp10*&
            &tmp9*tmp96) + 256*tmp157*tmp17*tmp177*tmp3*(2048*tmp259*tmp418 + tmp1666*tmp30*(&
            &tmp452*(tmp512 + tmp513 + (tmp135 + tmp354 + tmp359)/(tmp54*tmp55) + (tmp112 + t&
            &mp135 + tmp190)*tmp60) + tmp515*(tmp2102 + tmp2103 + (tmp2104 + tmp329 + tmp430)&
            &*tmp60 + (tmp2105 + tmp430 + 169*tmp70)/(tmp54*tmp55)) + tmp36*(tmp2089 + tmp209&
            &0 + tmp60*(tmp2091 + tmp391 + tmp619) + (tmp2092 + tmp619 + 254*tmp70)/(tmp54*tm&
            &p55))) - 64*tmp252*(tmp418*tmp6 + tmp516*(43*tmp1650*tmp17 + 43*tmp413 + tmp60*(&
            &tmp1589 + tmp1591 + tmp621) + (tmp1590 + tmp1591 + 194*tmp70)/(tmp54*tmp55)) + t&
            &mp36*(tmp1410*tmp19 + tmp17*tmp2283 + (tmp1377 + tmp1592 + tmp297)*tmp60 + (tmp1&
            &592 + tmp1817 + tmp860)/(tmp54*tmp55))) - (16*tmp32*(4*tmp2792 + 14*tmp44*(tmp48&
            &8 + tmp489 + (tmp1593 + tmp2579 + tmp422)/(tmp54*tmp55) + (tmp1593 + tmp229 + tm&
            &p421)*tmp60) + 9*tmp1560*(tmp17*tmp2690 + tmp2789 + tmp60*(tmp1593 + tmp1594 + t&
            &mp608) + (tmp149 + tmp1593 + 45*tmp70)/(tmp54*tmp55)) + 3*tmp1535*(tmp1092 + 6*t&
            &mp413 + (tmp134 + tmp1595 + tmp196)/(tmp54*tmp55) + tmp60*(tmp114 + tmp134 + tmp&
            &916))))/tmp11 + tmp4*tmp433*(8*tmp2731*tmp499 + tmp529*(tmp1599 + tmp1600 + (tmp&
            &1598 + tmp3068 + tmp351)/(tmp54*tmp55) + tmp60*(tmp1598 + tmp261 + tmp606)) + tm&
            &p5*tmp6*(31*tmp1650*tmp17 + tmp2842 + (tmp1603 + tmp2100 + tmp216)*tmp60 + (tmp1&
            &602 + tmp1603 + 224*tmp70)/(tmp54*tmp55)) + 6*(tmp1599 + tmp1600 + (tmp1039 + tm&
            &p1601 + tmp351)/(tmp54*tmp55) + tmp60*(tmp1039 + tmp261 + tmp606))*tmp738 + tmp4&
            &18*tmp93) + tmp1*tmp5*(tmp2178*(tmp512 + tmp513 + tmp60*(tmp112 + tmp190 + tmp60&
            &9) + (tmp1597 + tmp354 + tmp609)/(tmp54*tmp55)) + tmp5*tmp6*(tmp512 + tmp513 + (&
            &tmp112 + tmp190 + tmp511)*tmp60 + (tmp354 + tmp511 + 59*tmp70)/(tmp54*tmp55)) + &
            &tmp2731*(tmp412 + tmp413 + (tmp1734 + tmp514 + tmp61)/(tmp54*tmp55) + tmp60*(tmp&
            &514 + tmp77 + tmp80)) + (tmp512 + tmp513 + (tmp112 + tmp1596 + tmp190)*tmp60 + t&
            &mp670*(tmp1592 + tmp2109 + tmp80))*tmp89 + (tmp2638*tmp93)/(tmp10*tmp55*tmp9)) +&
            & 256*tmp272*(tmp433*tmp499 + tmp418*tmp97) + (tmp2304*tmp2638*(tmp257 + tmp529 +&
            & tmp5*tmp560 + tmp93 + tmp2116*tt))/(tmp10*tmp9)) + (256*tmp157*tmp72*(m2*tmp158&
            &*tmp2296*tmp564*tmp579*tmp7 + (tmp250*tmp2533*tmp579*tmp985*tt)/tmp96 + (tmp564*&
            &(tmp579*(tmp158*(tmp177 + (2*tmp3)/tmp96) + tmp988/tmp96) + (tmp158*tt*(-16*tmp1&
            &804*tmp32 + tmp3225*tmp41 + tmp475 + tmp1064*tmp567 + ((1/tmp10 + tmp1609 + tmp1&
            &721)/tmp11 + tmp177)*tmp684 + tmp1*(3*tmp180 + (tmp111 + tmp1610 + tmp2712)*tmp5&
            & + (tmp2119*tmp690)/tmp11) + tmp1068*tmp3211*tt))/tmp96))/tmp11))/tmp55))/16. - &
            &(scalarc0t*tmp240*tmp357*tmp411*tmp53*tmp56*tmp59*tmp65*tmp67*((256*tmp157*tmp72&
            &*((tmp158*tmp164*tmp175*tmp2540)/(tmp54*tmp55) + tmp2548/tmp99 + (((tmp158*tmp17&
            &5*tmp242*tmp705)/tmp55 + tmp2214*tmp708)*tmp85)/(tmp9*tmp99))*tt)/tmp99 + (256*t&
            &mp157*tt*((tmp158*tmp175*tmp2540*tmp2545)/tmp54 - (tmp1785*tmp2214)/tmp99 + (tmp&
            &242*tmp3129 - (tmp2214*tmp695)/tmp10)/tmp99 + (tmp175*tmp242*tmp3118*tmp72*tt)/t&
            &mp99))/(tmp10*tmp55*tmp9*tmp99) + (256*tmp157*tmp158*tmp72*tt*(tmp242*tmp2533*tm&
            &p563*tmp564 - tmp564*(tmp2536*tmp563 + (tmp158*tmp97*(tmp2537*tmp3208 + tmp241*(&
            &tmp3174*(tmp1063 + tmp2538 - 4/tmp54) + tmp3*tmp559 - 7*tmp2119*tmp6) - 32*tmp4*&
            &((tmp1057 + tmp1574 + tmp2538)/tmp11 + (tmp1093 + 1/tmp55)*tmp97) + tmp526*tmp68&
            &6*(tmp244/tmp11 + tmp988)))/tmp99) + (tmp2533*tmp563*tmp985*tt)/(tmp9*tmp99)))/t&
            &mp55 + (256*tmp157*tmp158*tmp7*(tmp2248 + tmp2449 + 32*tmp252*(tmp2250 + tmp2460&
            & + tmp6*((tmp144 + tmp1868)*tmp2450 + (tmp263 + tmp511 + tmp61)*tmp636 - tmp60*(&
            &tmp2386/tmp10 + tmp2657 + tmp2794 + 356*tmp642 - 350*tmp61*tmp70) + (247*tmp1422&
            & - 141*tmp1650 + tmp2461 + tmp2462)*tmp74 + (tmp2254 + tmp2463 - 184*tmp61*tmp70&
            & - (35*tmp83)/tmp10 + tmp889)/(tmp10*tmp55))) - 16*tmp30*(tmp2256 + tmp2475 + (t&
            &mp1693*(tmp2175*tmp2476*tmp369 + (tmp200 + tmp2259 + tmp2577)*tmp636 + tmp60*(tm&
            &p585/tmp10 + 25*tmp641 + 703*tmp642 + 68*tmp643 - 625*tmp61*tmp70) + (tmp1764 + &
            &tmp2477 - 68*tmp641 - 304*tmp642 + 361*tmp61*tmp70)/(tmp10*tmp55) + tmp2482*(-26&
            &9*tmp1422 + 116*tmp1650 + tmp2142 - 50*tmp82)))/tmp11 + tmp48*((tmp128 + tmp146)&
            &*tmp2484 + tmp636*(tmp2478 + tmp598 - 108*tmp70) + tmp2482*(256*tmp1422 - 306*tm&
            &p1650 + tmp599 + 112*tmp82) + tmp60*(tmp2263 - 924*tmp642 - 124*tmp643 + 1316*tm&
            &p61*tmp70 - (448*tmp83)/tmp10) + (tmp1058*(tmp1648 + tmp2778 + 125*tmp642 - 165*&
            &tmp61*tmp70 + tmp915))/tmp10)) + (tmp48*tmp72*(tmp2731*tmp3*(tmp200 + tmp236 + t&
            &mp2481 + tmp2590 + tmp427 + tmp781) + 4*tmp3*tmp738*(tmp191 + tmp197 + tmp2479 +&
            & tmp2480 + tmp60 + tmp80) + tmp2412*(tmp2128 + tmp277 + (tmp1814 + tmp185)*tmp60&
            & + (tmp422 + tmp2572/tmp54 + 18*tmp70)/tmp55 + tmp74 + tmp780 + tmp856) + (tmp16&
            &0*tmp3*tmp89)/tmp9 + (tmp1125 + tmp1422 + tmp2482 + tmp264 + tmp503/tmp55 + tmp6&
            &48 + tmp2483*tmp781)*tmp93))/tmp55 + 8*tmp32*(tmp2282 + tmp2495 + tmp2731*((1/tm&
            &p10 + tmp1813)*tmp2484 + tmp636*(tmp2498 + tmp365 - 84*tmp70) + tmp74*(909*tmp14&
            &22 - 763*tmp1650 + tmp2363 + 172*tmp82) + tmp60*(tmp2499 + 33*tmp641 - 1470*tmp6&
            &42 + 1872*tmp61*tmp70 - (537*tmp83)/tmp10) + (tmp1641 + tmp2155 + 712*tmp642 - 1&
            &050*tmp61*tmp70 + (263*tmp83)/tmp10)/(tmp10*tmp55)) + tmp5*tmp6*(tmp1866*tmp2450&
            & - (tmp1732 + tmp226 + tmp2700)*tmp636 + tmp74*(378*tmp1422 + tmp2496 + tmp293 -&
            & 26*tmp83) + tmp60*(tmp2163 + tmp2497 - 451*tmp642 + 369*tmp61*tmp70 + tmp83/tmp&
            &10) + (tmp1302 + tmp227/tmp10 + 178*tmp642 - 217*tmp61*tmp70 + tmp889)/(tmp10*tm&
            &p55)) + (tmp2450*tmp2500 + tmp636*(tmp2156 + tmp2501 - 90*tmp70) + tmp2482*(244*&
            &tmp1422 - 303*tmp1650 + tmp2502 + 91*tmp82) + tmp60*(62*tmp641 - 867*tmp642 - 94&
            &*tmp643 + 1257*tmp61*tmp70 - (453*tmp83)/tmp10) + (tmp2293 + 450*tmp642 - 577*tm&
            &p61*tmp70 + tmp1862*tmp83 + tmp915)/(tmp10*tmp55))*tmp93) + tmp4*tmp686*(tmp1693&
            &*tmp5*(tmp1991*tmp632*tmp70 + tmp636*(tmp606 + tmp61 - 86*tmp72) + (493*tmp1422 &
            &- 325*tmp1650 + tmp195 + tmp220)*tmp74 + (tmp72*(-525*tmp1422 + 172*tmp1650 + tm&
            &p348 + 332*tmp82))/tmp55 + tmp60*(tmp2515 + tmp641 - 739*tmp642 + 871*tmp61*tmp7&
            &0 - (218*tmp83)/tmp10)) + tmp2731*(tmp1419*(tmp2504 + tmp2516 + tmp2517 + tmp259&
            &5) + tmp2566*tmp632*tmp70 + tmp636*(tmp1020 - 36*tmp61 + 48*tmp70) + tmp60*(-36*&
            &tmp641 + 1061*tmp642 + 48*tmp643 - 1451*tmp61*tmp70 + (475*tmp83)/tmp10) + ((-79&
            &5*tmp1422 + 235*tmp1650 + tmp1749 + 512*tmp82)*tmp844)/tmp54) + tmp1225*tmp1777*&
            &tmp72*tmp89 + (tmp2518*tmp61*tmp632 + (tmp72*(tmp1316 + 205*tmp1422 + 24*tmp1650&
            & - 176*tmp82))/tmp55 - tmp74*(243*tmp1422 - 247*tmp1650 + tmp2519 + 38*tmp83) + &
            &tmp60*(-19*tmp641 + 383*tmp642 + 29*tmp643 - 505*tmp61*tmp70 + (158*tmp83)/tmp10&
            &) + tmp636*(tmp2167 + tmp2518 + tmp900))*tmp93 + (tmp2304*(tmp1133 + tmp2461/tmp&
            &10 + tmp2511 + tmp2512 + (tmp1645 + tmp227 + tmp2513 + tmp2514)/tmp55 + tmp196*t&
            &mp70 + 6*tmp60*(tmp606 + tmp61 + tmp790))*tt)/tmp54) + tmp6*tmp966*(tmp1693*tmp5&
            &*(tmp1199 + (tmp1018 + tmp190 + tmp61)*tmp636 + ((-139*tmp1422 + 51*tmp1650 + tm&
            &p2462 + tmp667)*tmp670)/tmp10 + tmp2482*(114*tmp1422 - 86*tmp1650 + tmp385 + tmp&
            &83) + tmp60*(tmp1212 + tmp641 - 362*tmp642 + 462*tmp61*tmp70 - (130*tmp83)/tmp10&
            &)) + tmp2731*(tmp352*tmp61*tmp632 + tmp636*(tmp2506 + tmp352 + tmp679) - tmp74*(&
            &tmp1206 + 185*tmp1422 + tmp2507 + tmp788) + tmp60*(tmp2508 - 8*tmp641 + 308*tmp6&
            &42 - 425*tmp61*tmp70 + (139*tmp83)/tmp10) + (tmp844*(-240*tmp1422 + tmp1945*tmp6&
            &1 + 145*tmp82 + tmp852))/tmp54) + tmp2479*tmp3*tmp688*tmp89 + ((tmp191 + tmp238 &
            &+ tmp2594)*tmp636 + (tmp72*(tmp1750 + tmp2509 + tmp763 - 33*tmp82))/tmp55 + tmp8&
            &81 + tmp60*(tmp1656 + tmp2142/tmp10 + tmp2779 + 91*tmp642 + tmp884) + tmp1419*(t&
            &mp145 + tmp2510 + tmp83 + tmp891))*tmp93 + (tmp2304*(tmp2503 + tmp60*(tmp394 - 8&
            &7*tmp70 + tmp80) - (tmp1411 - 76*tmp1422 + tmp2504 + tmp83)/tmp10 + (tmp1865/tmp&
            &10 + tmp2125 + tmp2505 + tmp83)/tmp55)*tt)/tmp54)))/tmp99 - (256*tmp1053*tmp157*&
            &tmp1584*tmp7*((tmp2533*tmp563)/(tmp10*tmp55) + tmp249*(512*tmp30 + 32*tmp4*(tmp1&
            &696 + tmp2520 + tmp5) + (tmp3174 + tmp41 + tmp515)*tmp6 + m2*tmp2296*(48*tmp36 +&
            & tmp559 + 29*tmp6) - 32*tmp32*(tmp448 + 25*tt))))/(tmp54*tmp99) - (256*tmp157*tm&
            &p72*tt*((tmp158*tmp175*tmp2540*tmp469)/tmp55 + (tmp1584*tmp175*tmp242*tmp627*tt &
            &- tmp469*(-(tmp175*tmp242*tmp2553) + tmp158*tmp2316*tt))/tmp99))/(tmp55*tmp99) +&
            & (256*tmp157*tmp2533*tt*(-(tmp158*tmp242*tmp522*tmp563) + (tmp158*tmp563*tmp705)&
            &/(tmp55*tmp99) - tmp522*((-4096*tmp252)/tmp100 + 4096*tmp272 + m2*(tmp1464 + tmp&
            &1535 - 16*tmp1560 + tmp2522)*tmp452 - tmp156*tmp526 - 32*tmp32*(43*tmp1535 + 28*&
            &tmp1560 + tmp567 + tmp624) + tmp2618*(tmp1708 + tmp2208 + tmp2521 + tmp567)*tt +&
            & tmp551*(tmp1557 + tmp726 + tmp253*tt))))/(tmp10*tmp55*tmp9) + (256*tmp157*tt*(-&
            &(tmp1584*tmp175*tmp249*tmp2540) + (tmp158*tmp175*tmp242*tmp2526*tt)/(tmp10*tmp99&
            &) - (tmp249*(tmp1693*(tmp2531 + tmp2530*tmp44 + tmp474 + tmp2196*tmp48) + tmp241&
            &*(tmp2412*tmp2532 + 2*tmp2731*(tmp194 + tmp217 + tmp476) + tmp2761*tmp532 + tmp2&
            &194*tmp738 + tmp89/tmp9) + tmp447/tmp9 + 8*tmp32*(tmp36*(-33/tmp10 + tmp2527 + 4&
            &6/tmp54) + (tmp2368 + tmp2528 + 47/tmp55)*tmp6 + tmp942/tmp9) - 4*tmp4*(tmp48*(t&
            &mp1617 + tmp2529 + 38/tmp55) + tmp1535*(-29/tmp10 + 54/tmp54 + 54/tmp55) + tmp21&
            &16/tmp9 + tmp2194*tmp3211*tt) - 32*tmp30*(tmp253/tmp9 + (tmp931 + tmp973 + tmp97&
            &4)*tt)))/tmp99))/(tmp54*tmp55*tmp9*tmp99)))/16. + (scalarc0ir6u*tmp100*tmp240*tm&
            &p357*tmp411*tmp53*tmp56*tmp59*tmp65*(256*tmp157*tmp177*(tmp2345 - 32*tmp252*(tmp&
            &244*tmp516*(((tmp1617 + tmp1872)*tmp2175)/tmp10 + (tmp2151 + tmp2351 + tmp2847 -&
            & 56*tmp642 - 80*tmp61*tmp70)/tmp55 - (tmp155 + tmp2919 + tmp622)*tmp74 + tmp60*(&
            &tmp1118 + tmp2350 + tmp353 + 27*tmp82)) + tmp36*(tmp2438*(79/tmp54 + tmp63) + (t&
            &mp2352 + tmp2365 + tmp382)*tmp636 + tmp74*(-949*tmp1422 + 195*tmp1650 + tmp2353 &
            &- 298*tmp82) + tmp60*(tmp2848 + 23*tmp641 + 1062*tmp642 - 424*tmp61*tmp70 - (301&
            &*tmp83)/tmp10) + (tmp1952 + tmp2354 + tmp2355 - 426*tmp642 + tmp889)/(tmp10*tmp5&
            &5)) + tmp6*(tmp2438*(tmp2356 + tmp63) + tmp636*(tmp2357 + tmp361 + 92*tmp70) + t&
            &mp60*(-15*tmp641 + 784*tmp642 + 80*tmp643 - 702*tmp61*tmp70 + tmp1882*tmp83) + (&
            &tmp1959 - 354*tmp642 + 252*tmp61*tmp70 + (209*tmp83)/tmp10 + tmp889)/(tmp10*tmp5&
            &5) - tmp74*(565*tmp1422 - 319*tmp1650 + 178*tmp82 + tmp917))) + 16*tmp30*(tmp153&
            &5*((tmp1575 + tmp2)*tmp2175*tmp429 + tmp636*(-66*tmp61 + 384*tmp70 + 730*tmp72) &
            &- tmp74*(2691*tmp1422 - 1495*tmp1650 + tmp2364 + 756*tmp82) + tmp60*(-78*tmp641 &
            &+ 3473*tmp642 + 360*tmp643 - 2997*tmp61*tmp70 + (290*tmp83)/tmp10) + (tmp1626 + &
            &tmp1971 - 1500*tmp642 + 967*tmp61*tmp70 + (1020*tmp83)/tmp10)/(tmp10*tmp55)) + t&
            &mp1560*(tmp2438*(tmp1023 + 173/tmp54) + tmp636*(tmp2361 + tmp2362 + 334*tmp70) +&
            & tmp74*(-2717*tmp1422 + 749*tmp1650 + tmp2363 - 656*tmp82) + tmp60*(tmp2812 + 29&
            &47*tmp642 + 310*tmp643 - 1419*tmp61*tmp70 - (590*tmp83)/tmp10) + (tmp1626 + tmp1&
            &969 + tmp2588 - 1120*tmp642 + (1396*tmp83)/tmp10)/(tmp10*tmp55)) + tmp48*((tmp18&
            &13 + tmp2)*tmp2175*tmp352 + (tmp1627 + tmp2365 - 67*tmp61)*tmp636 + tmp1419*(382&
            &*tmp1422 - 328*tmp1650 + tmp2366 + 148*tmp82) + tmp60*(-75*tmp641 + 1108*tmp642 &
            &+ 136*tmp643 - 1212*tmp61*tmp70 + (316*tmp83)/tmp10) + (tmp1058*(tmp1628 - 131*t&
            &mp642 + 91*tmp61*tmp70 + (80*tmp83)/tmp10 + tmp915))/tmp10) + tmp3*tmp524*((tmp2&
            &638*(tmp24 + tmp592)*tmp632)/tmp10 + (tmp2095 + tmp2358 + tmp2359)*tmp74 + (tmp1&
            &194 + tmp2258 + tmp2360 + 160*tmp61*tmp70 - (243*tmp83)/tmp10)/tmp55 + tmp60*(tm&
            &p1170 + tmp1964 - 45*tmp82 + tmp918))) + (4*tmp4*(tmp254*tmp3*((tmp2003 + tmp239&
            &1 + tmp2392 + tmp2393)*tmp60 + tmp2249*tmp632 + (tmp106 + tmp422 + 98*tmp72)*tmp&
            &74 + (tmp2394 + tmp2909 + tmp353 + tmp788)/(tmp54*tmp55)) + tmp738*(tmp1938*tmp6&
            &32 + tmp636*(tmp2395 + tmp637 + 86*tmp70) + (tmp72*(656*tmp1650 + tmp1995 + tmp2&
            &394 - 542*tmp82))/tmp55 + tmp74*(-1807*tmp1422 + 491*tmp1650 - 172*tmp82 + 42*tm&
            &p83) + tmp60*(tmp2396 + 1733*tmp642 + 86*tmp643 - 787*tmp61*tmp70 - (330*tmp83)/&
            &tmp10)) - tmp2731*(tmp2561*tmp61*tmp632 + tmp636*(tmp2397 + tmp2561 - 380*tmp72)&
            & + tmp74*(1181*tmp1422 - 529*tmp1650 + 190*tmp82 + 54*tmp83) + (tmp72*(-724*tmp1&
            &650 + tmp2389 + 356*tmp82 + 255*tmp83))/tmp55 + tmp60*(tmp2403 - 1157*tmp642 - 9&
            &5*tmp643 + 671*tmp61*tmp70 + (106*tmp83)/tmp10)) + (tmp5*tmp6*(129*tmp1650*tmp63&
            &2 + (971*tmp61 - 258*tmp70 - 2417*tmp72)*tmp74 + (364*tmp1422 + 865*tmp1650 + tm&
            &p1997 - 858*tmp82)/(tmp54*tmp55) + tmp60*(2552*tmp1422 - 1706*tmp1650 + tmp2004 &
            &- 123*tmp83) + 3*tmp636*(241/tmp54 + tmp848)))/tmp10 + (tmp1419*(tmp1114 + tmp23&
            &99 + tmp2400 + tmp293) + tmp1377*tmp632*tmp70 + tmp636*(tmp344 + tmp761 + tmp793&
            &) + tmp60*(tmp2587 - 20*tmp641 + 34*tmp643 + tmp2152*tmp70 + tmp1638*tmp82) + (t&
            &mp1977*(tmp1761 + tmp2398 + tmp852 + tmp914))/tmp54)*tmp93))/tmp100 - 8*tmp32*(t&
            &mp254*tmp3*(-(((tmp1032 + tmp2)*tmp2175)/tmp10) + tmp60*(tmp1637 + tmp2573 + tmp&
            &2660 + tmp611) + (tmp1722 + tmp2109 + tmp2401)*tmp74 + (tmp1648 + 50*tmp642 + tm&
            &p665 + tmp291*tmp70 - (230*tmp83)/tmp10)/tmp55) + tmp2731*(tmp2175*tmp369*(tmp2 &
            &+ 87/tmp54) + tmp636*(tmp2405 - 111*tmp61 + 344*tmp70) - tmp74*(2719*tmp1422 - 1&
            &629*tmp1650 + 684*tmp82 + 226*tmp83) + (tmp1641 + tmp2019 - 1322*tmp642 + 626*tm&
            &p61*tmp70 + (1243*tmp83)/tmp10)/(tmp10*tmp55) + tmp60*(-115*tmp641 + 3278*tmp642&
            & + 336*tmp643 - 2818*tmp61*tmp70 + tmp1949*tmp83)) + tmp1834*tmp5*((tmp2 + tmp24&
            &21)*tmp2450 + tmp60*(tmp2360 + tmp2477 - 1885*tmp642 - 150*tmp643 + 1373*tmp61*t&
            &mp70) + tmp636*(tmp149 - 154*tmp70 - 483*tmp72) + (tmp1653 + 268*tmp641 + 720*tm&
            &p642 - 375*tmp61*tmp70 - (611*tmp83)/tmp10)/(tmp10*tmp55) + tmp2482*(-362*tmp165&
            &0 + tmp2404 + 153*tmp82 + tmp852)) + tmp2178*(tmp2175*(tmp2012 + tmp24)*tmp70 + &
            &tmp636*(tmp2844 + tmp382 + 598*tmp72) + (-1731*tmp1422 + 419*tmp1650 + tmp1748 +&
            & tmp2846)*tmp74 + tmp60*(tmp2403 + 1712*tmp642 + 137*tmp643 - 693*tmp61*tmp70 - &
            &(415*tmp83)/tmp10) + (tmp2402 - 240*tmp641 - 577*tmp642 + (787*tmp83)/tmp10 + tm&
            &p915)/(tmp10*tmp55)) + ((tmp2 + tmp2122)*tmp2438 + tmp60*(tmp2286/tmp10 - 58*tmp&
            &641 + 511*tmp642 + 98*tmp643 - 445*tmp61*tmp70) + tmp636*(-56*tmp61 + 102*tmp70 &
            &+ 150*tmp72) - (-367*tmp1650 + tmp2406 + tmp2407 + tmp3072)*tmp74 + (tmp2025 + t&
            &mp2665*tmp61 - 216*tmp642 + (404*tmp83)/tmp10 + tmp915)/(tmp10*tmp55))*tmp93) + &
            &tmp410*(tmp2179*(tmp1725 + (tmp155 + tmp213 + tmp420)*tmp636 + tmp2482*(tmp1635 &
            &+ tmp227 + tmp651 + tmp668) + ((tmp1162 + tmp1206 + tmp2389 + tmp2390)*tmp72)/tm&
            &p55 + tmp60*(tmp1727 + tmp2908/tmp10 + tmp1647/tmp54 + tmp669 + tmp61*tmp794)) +&
            & tmp254*tmp3*((tmp187 + tmp195 + tmp206 + tmp2875)*tmp60 - tmp1650*tmp632 + tmp7&
            &4*(tmp61 + tmp70 + tmp789) + (tmp151 + tmp2379 + tmp2382 + tmp83)/(tmp54*tmp55))&
            & - tmp2731*(tmp1758 + ((tmp2136 + tmp2386 + tmp2387 + tmp2388)*tmp670)/tmp10 + t&
            &mp60*(tmp1762 + tmp2693/tmp10 + tmp641 - 220*tmp642 + tmp1377*tmp70) + tmp636*(t&
            &mp224 + tmp61 - 104*tmp72) + tmp2482*(144*tmp1422 + tmp2675 + tmp662 + tmp83)) +&
            & tmp738*(tmp1350*tmp632 + tmp636*(tmp155 + tmp2380 + tmp672) + ((tmp2381 + tmp23&
            &82 + tmp2973 + tmp3064)*tmp72)/tmp55 + (tmp1282 - 483*tmp1422 + tmp2565 + tmp667&
            &)*tmp74 + tmp60*(tmp3075 + 446*tmp642 + tmp669 - 212*tmp61*tmp70 - (69*tmp83)/tm&
            &p10)) + tmp2412*(tmp2383 + (tmp369 + tmp388 + tmp61)*tmp636 + ((tmp1422 + 40*tmp&
            &1650 + tmp2384 + tmp2385)*tmp670)/tmp10 + tmp60*(tmp1641 + tmp641 + 190*tmp642 -&
            & 102*tmp61*tmp70 + tmp2572*tmp83) + tmp2482*(-101*tmp1422 + tmp2105/tmp10 + tmp8&
            &3 + tmp914)))*tmp966 + (tmp1629*tmp72*(tmp2367 + tmp2377 + tmp2412*(tmp1632 + tm&
            &p1735 + tmp2369 + tmp264 + (tmp197 + tmp207 + tmp2370)/tmp55 + (tmp2368 + tmp580&
            &)*tmp60 + tmp648) + tmp2178*(tmp1422 + tmp1634 + tmp1635 + (tmp289 + tmp345 + tm&
            &p508)/tmp55 + (-26/tmp10 + tmp316)*tmp60 + tmp617 + tmp791) + tmp93*(tmp107 - 17&
            &*tmp1422 + tmp1634 + tmp2378 + (tmp229 + tmp361 + tmp386)/tmp55 + tmp90 + tmp110&
            &*tmp993)))/tmp55 - 128*tmp272*(-4*tmp23*((tmp1034*tmp2638*tmp632)/tmp10 + (tmp60&
            &*(tmp120 + tmp1746 + tmp73))/tmp54 + (tmp120 + tmp191 + tmp358)*tmp74 + (tmp1756&
            & + tmp2346 - 5*tmp641 + tmp886)/tmp55) + (tmp19*tmp2450 - (tmp149 + tmp369 + tmp&
            &386)*tmp636 + tmp74*(tmp2347 + tmp2348 + tmp674 - 24*tmp83) - tmp60*(tmp2360 + t&
            &mp2508 + tmp1737/tmp54 + tmp2100*tmp70 - (73*tmp83)/tmp10) + (tmp2139 + tmp2349 &
            &+ tmp2402 + tmp2704 + tmp915)/(tmp10*tmp55))*tt)) - (512*tmp157*tt*((tmp175*tmp2&
            &423*tmp249*tt)/tmp10 + (tmp158*tmp1688*tmp175*tmp689*tt)/(tmp10*tmp100) + (tmp24&
            &9*(-(tmp175*(tmp2424 + tmp2425 + (tmp250 + tmp26)/tmp100)) + tmp158*tmp1688*tmp9&
            &83*tt))/tmp100))/(tmp54*tmp55*tmp9) + (256*tmp15*tmp157*tmp177*(tmp1688*tmp2419*&
            &tmp249 + (tmp2419*tmp2533)/(tmp10*tmp100*tmp55) + (2*tmp249*(-4*(tmp1768 + tmp26&
            &15)*tmp32 + tmp436 - m2*(22*tmp1535 + tmp1664 + tmp2408 + tmp49) + tmp410*(tmp17&
            &1 + tmp36 + tmp6) + tmp4*tmp433*(39/tmp11 + 28*tt)))/tmp100))/(tmp54*tmp55*tmp9)&
            & - (512*tmp157*tmp72*tt*((tmp175*tmp2423*tmp469*tt)/tmp54 - (tmp1584*tmp1688*tmp&
            &175*tmp627*tt - tmp469*(-(tmp175*(tmp2433 + tmp2434 + tmp2436/tmp100)) + tmp1056&
            &*tmp158*tmp1688*tt))/tmp100))/tmp55 - (512*tmp157*tt*((tmp175*tmp2423*tmp695*tmp&
            &97)/tmp10 + (tmp1688*tmp3129 + tmp695*(-((tmp175*tmp2432)/tmp10) + (tmp158*tmp16&
            &88*tmp2430*tt)/tmp10))/tmp100))/(tmp10*tmp55*tmp9) - (512*tmp157*tmp72*tt*(tmp14&
            &84*tmp175*tmp2423*tmp708 - (tmp158*tmp1688*tmp175*tmp61*tmp692 - tmp708*(tmp175*&
            &tmp2432*tmp85 + (tmp158*tmp1688*tmp2430*tt)/tmp54))/tmp100))/tmp9 + (256*tmp15*t&
            &mp157*tmp72*((tmp2419*tmp2533*tmp97*tmp985)/(tmp100*tmp9) + tmp1688*tmp2419*tmp5&
            &64*tmp988 - (tmp564*(tmp1665*tmp3 - 4*tmp4*(tmp2061*tmp244 + tmp1674*tmp3 + 2*tm&
            &p1535*(tmp144 + tmp2325 + 28/tmp54) + tmp1560*(-61/tmp10 + tmp2421 + 139/tmp55))&
            & - tmp410*(tmp1560*tmp2422 + tmp3220/tmp11 + tmp1068*tmp48 + tmp3*tmp567) + 8*tm&
            &p32*(tmp1667*tmp3 + tmp36*(-31/tmp10 + 72/tmp54 + 103/tmp55) + (tmp1063 + tmp210&
            & + tmp273)*tmp6) + tmp241*(tmp1681*tmp3 + (tmp128 + tmp2311)*tmp532 + tmp5*(-10/&
            &tmp10 + tmp1872 + 63/tmp55)*tmp6 + (-41/tmp10 + tmp1721 + 77/tmp55)*tmp738 + 2*t&
            &mp2731*(tmp1572 + tmp217 + tmp78)) - 32*tmp30*(tmp1666*tmp3 + (tmp2 + tmp2420 + &
            &tmp808)*tt)))/tmp100))/tmp55 + (256*tmp15*tmp157*tmp1777*((tmp1447*tmp1687 + (tm&
            &p1584*tmp3189*tmp408)/tmp54)/tmp100 + tmp1688*tmp2765*tt - ((tmp158*tmp2419*tmp7&
            &05)/tmp55 - tmp522*(tmp1665 + 8*tmp32*(tmp1542 + tmp1667 + 31*tmp36) - 4*(61*tmp&
            &1560 + tmp1674 + tmp2426 + tmp2708)*tmp4 - tmp410*(tmp1560 + tmp428 + tmp567) + &
            &m2*(tmp2427 - 20*tmp2731 + tmp2203*tmp5 + 82*tmp738 + 56*tmp89) - 32*tmp30*(tmp1&
            &666 + tt)))/(tmp100*tmp9)))/tmp10))/16. + (discbu*tmp240*tmp357*tmp411*tmp53*tmp&
            &56*tmp58*tmp59*tmp65*(256*tmp15*tmp157*tmp177*((512*tmp2646*tmp272)/(tmp54*tmp55&
            &) - 8*tmp32*(tmp1560*(tmp1040*tmp61*tmp632 + tmp1772*(307*tmp1422 - 294*tmp1650 &
            &+ tmp2265 + tmp2911)*tmp72 + tmp636*(tmp1040 + 153*tmp61 + 216*tmp72) + tmp2482*&
            &(-264*tmp1422 - 258*tmp1650 + tmp2662 + 153*tmp83) + tmp60*(tmp2663 + 130*tmp642&
            & - 61*tmp643 + 974*tmp61*tmp70 - (888*tmp83)/tmp10)) + tmp1535*(tmp2695 + (tmp26&
            &64 + tmp2665 + 105*tmp61)*tmp636 + (tmp670*(tmp1299 + tmp1316 + 211*tmp1650 + 10&
            &7*tmp82))/tmp10 + tmp2482*(-119*tmp1422 + tmp2666 + tmp2696 + 105*tmp83) + tmp60&
            &*(tmp2699 + 105*tmp641 - 90*tmp642 + 834*tmp61*tmp70 - (630*tmp83)/tmp10)) + tmp&
            &244*tmp715*((tmp230 + tmp2658 + tmp2659 + tmp2660)/(tmp54*tmp55) + (tmp1985*tmp6&
            &32)/tmp10 + (tmp1005 + tmp1590 + 60*tmp72)*tmp74 + tmp60*(-88*tmp1422 + tmp2661 &
            &+ 86*tmp83 + tmp880)) + tmp624*(tmp1185 + ((tmp1203 + tmp2461 + tmp2667 + tmp266&
            &8)*tmp72)/tmp55 + (tmp1206 + tmp1611 + tmp2685 + tmp368)*tmp74 + tmp636*(tmp112 &
            &+ tmp114 + tmp790) + tmp60*(tmp1194 + tmp2266 + tmp2698 + 71*tmp61*tmp70 + tmp89&
            &2))) + 32*tmp30*(tmp6*(tmp3113 + tmp663 + tmp60*(tmp2252 + tmp2355 + tmp2657 + t&
            &mp2814 + tmp665) + tmp636*(tmp138 + tmp762 + tmp77) + tmp74*(tmp151 + tmp2249 + &
            &tmp2914 + tmp879)) - 2*tmp244*tmp5*((tmp2652 + tmp2653 + tmp2654 + tmp385)*tmp60&
            & + tmp1752*tmp632 + (tmp190 + tmp605 + tmp620)*tmp74 + (tmp1114 - 65*tmp1650 + t&
            &mp780 + tmp898)/(tmp54*tmp55)) + tmp36*((tmp191 + tmp2397 + tmp380)*tmp636 + tmp&
            &881 + tmp60*(tmp1322 + tmp2403 + 109*tmp642 + 109*tmp61*tmp70 + tmp884) + (tmp84&
            &4*(-151*tmp1650 + tmp1749 + tmp2655 + tmp90))/tmp54 + tmp2482*(-79*tmp1422 + tmp&
            &648 + 27*tmp83 + tmp913))) + 4*tmp4*(tmp244*tmp2730*(tmp2691 + (155*tmp1422 - 16&
            &2*tmp1650 + tmp1658 + tmp2692)/(tmp54*tmp55) + (-104*tmp1422 - 99*tmp1650 + tmp2&
            &693 + tmp2694)*tmp60 + (tmp2264 + tmp2361 + tmp297)*tmp74) + tmp738*(tmp2695 + (&
            &(-335*tmp1422 + 277*tmp1650 + tmp2125 + tmp2697)*tmp670)/tmp10 + tmp636*(tmp2665&
            & + 119*tmp61 + 208*tmp72) + tmp2482*(-209*tmp1422 - 237*tmp1650 + tmp2696 + 119*&
            &tmp83) + tmp60*(tmp2698 + tmp2699 + 119*tmp641 + 1028*tmp61*tmp70 - (798*tmp83)/&
            &tmp10)) + tmp516*tmp6*(tmp2482*(-144*tmp1650 + tmp2363 + tmp2701 + tmp2561/tmp54&
            &) + tmp2150*tmp61*tmp632 + (tmp2150 + tmp2700 + tmp329)*tmp636 + (tmp72*(-423*tm&
            &p1422 + 335*tmp1650 + 164*tmp82 - 76*tmp83))/tmp55 + tmp60*(70*tmp641 - 73*tmp64&
            &2 - 40*tmp643 + 635*tmp61*tmp70 - (463*tmp83)/tmp10)) + 2*tmp2731*((-55*tmp1422 &
            &- 70*tmp1650 + tmp1651 + tmp1658)*tmp2482 + tmp1001*tmp61*tmp632 + (tmp1001 + tm&
            &p1856 + tmp2361)*tmp636 + ((-162*tmp1422 + 136*tmp1650 + tmp2702 + tmp2703)*tmp7&
            &2)/tmp55 + tmp60*(tmp2704 + 41*tmp641 - 25*tmp643 + 263*tmp61*tmp70 - (222*tmp83&
            &)/tmp10)) + 4*(tmp1754 + tmp1650*tmp1772*tmp2705 + tmp60*(tmp1756 + tmp2284 + tm&
            &p2657 + tmp352*tmp61 - (18*tmp83)/tmp10) + tmp2482*(tmp145 + tmp192 + tmp1579*tm&
            &p70 + tmp856) + tmp636*(tmp138 + tmp790 + tmp92))*tmp93) - 64*tmp252*((tmp3221*(&
            &(tmp1722 + tmp216 + tmp2647)*tmp60 + (tmp2648 + tmp312 + tmp361)*tmp72 + (tmp185&
            & + tmp198)*tmp74 + (tmp235 + tmp2649 + tmp2650 + tmp906)/tmp55))/tmp54 + (tmp259&
            &2 + (tmp328 + tmp345 + tmp61)*tmp636 + tmp60*(tmp2589 + tmp2651 + tmp2269*tmp61 &
            &+ tmp641 - 38*tmp642) + tmp74*(tmp130 + tmp195 + tmp2597 + tmp788) + (tmp72*(tmp&
            &186 + tmp591 + tmp661 + tmp788))/tmp55)*tmp97) + (tmp966*(tmp738*(tmp2677 + tmp6&
            &36*(tmp379 + tmp608 + tmp637) + ((102*tmp1650 + tmp2678 + tmp2679 + tmp2281/tmp5&
            &4)*tmp670)/tmp10 + tmp60*(tmp2396 + tmp2680 - 50*tmp642 + 340*tmp61*tmp70 - (220&
            &*tmp83)/tmp10) + tmp2482*(tmp1263 + tmp2138 + tmp348 + tmp866)) + tmp244*tmp2730&
            &*((tmp136 + tmp145 + tmp2003 + tmp2676)*tmp60 + tmp2350*tmp632 + (tmp2675 + tmp3&
            &53 + tmp791 + tmp903)/(tmp54*tmp55) + tmp74*(tmp2674 + tmp422 + tmp92)) + tmp241&
            &2*(tmp1754 + ((-84*tmp1422 + tmp1651 + tmp2682 + tmp2021*tmp61)*tmp72)/tmp55 + t&
            &mp74*(-45*tmp1422 + tmp2003 + tmp877 + tmp90) + tmp60*(tmp1756 + tmp2148 + tmp26&
            &83 + 99*tmp61*tmp70 + tmp914/tmp54) + tmp636*(tmp2681 + tmp422 + tmp92)) + tmp93&
            &*(tmp1185 + 2*tmp2689*tmp636 + (tmp1206 + tmp2144 + tmp2690 + tmp368)*tmp74 + (t&
            &mp844*(tmp2350 + tmp2668 + tmp2913 + tmp879))/tmp54 + tmp60*(tmp1194 + 74*tmp642&
            & + tmp1618*tmp70 + tmp892 + tmp83*tmp931)) + tmp2731*(tmp2684 + tmp2482*(tmp1650&
            & + tmp2134 + tmp2581/tmp54 + tmp590) + tmp1772*(tmp1206 + tmp2685 + tmp2686 + tm&
            &p371)*tmp72 + tmp60*(tmp2687 + tmp2688 + 118*tmp642 + tmp2506*tmp70 - (70*tmp83)&
            &/tmp10) + tmp636*(tmp1985 + tmp229 + tmp986/tmp10))))/tmp100 + (tmp410*tmp72*(2*&
            &tmp160*tmp2731*(tmp139 + tmp155 + tmp2126 + tmp232 + tmp2672 + tmp1573/tmp54) + &
            &(tmp160*tmp3*tmp529)/tmp9 + (tmp1634 + tmp2393 + tmp2482 + tmp277 + (tmp1521 + t&
            &mp2647 + tmp422)/tmp55 + (tmp217 + tmp2673)*tmp60 + tmp920)*tmp93 + tmp2412*(tmp&
            &1634 + tmp1742 + tmp2371 + tmp2671 + (tmp113 + tmp369 + tmp584)/tmp55 + tmp82 + &
            &tmp60*(1/tmp54 + tmp931)) + tmp2178*(tmp1346 + tmp1634 + tmp1744 + tmp2669 + (tm&
            &p420 + tmp766 + tmp849)/tmp55 + tmp90 + tmp2670*tmp993)))/tmp55) + (256*tmp157*(&
            &(tmp158*tmp25*tmp2551*tmp2734)/(tmp100*tmp54) + tmp158*tmp1688*tmp175*tmp2296*tm&
            &p695 - tmp2757*tmp695 + m2*tmp2723*tmp690*tmp695)*tmp70*tt)/(tmp55*tmp9) + (256*&
            &tmp157*tmp1650*((tmp158*tmp2734*tmp705)/(tmp100*tmp55) + tmp158*tmp1688*tmp175*t&
            &mp2296*tmp708 - tmp2757*tmp708 + m2*tmp2723*tmp690*tmp708)*tt)/tmp9 + (256*tmp15&
            &7*tmp72*tt*(m2*tmp164*tmp2552*tmp2723 + tmp158*tmp1688*tmp175*tmp2552*tmp469 - t&
            &mp469*(-3*tmp178*tmp2735*tmp410 + tmp2736/tmp55 - 8*tmp4*(tmp2731*(tmp2711 + 108&
            &/tmp55) + tmp2744/tmp55 + tmp5*(tmp2710 + 178/tmp55)*tmp6 + (128/tmp55 + tmp592)&
            &*tmp738 + (tmp2712 + tmp472)*tmp93) + (tmp241*(tmp2178*(tmp185 + tmp2527) + 16*(&
            &tmp1058 + tmp189)*tmp2731 + 54*tmp2247*tmp5*tmp6 + tmp1892*tmp89 + (tmp2325 + tm&
            &p798)*tmp93))/tmp100 + tmp714*((tmp108 + tmp2709)*tmp48 + tmp1560*(tmp117 + 57/t&
            &mp55) + tmp2738/tmp55 + tmp1535*(55/tmp55 + tmp974))) + (tmp1584*tmp2734*tmp627*&
            &tt)/tmp100))/tmp55 + (256*tmp15*tmp157*tmp177*(-(tmp158*tmp2419*tmp249) + (tmp24&
            &9*tmp408)/tmp11 + m2*tmp2296*tmp249*tmp2760*tmp713 + tmp249*tmp408*tt + tt*((tmp&
            &2533*tmp2760*tmp713)/(tmp10*tmp100*tmp55) - tmp249*(m2*(tmp1536 + 30*tmp1560 + t&
            &mp2116 + tmp2203/tmp11) - 16*tmp4*(tmp5 + tmp566 + tmp6) + tmp410*tmp819*tmp94 +&
            & tmp543*tt))))/(tmp54*tmp55*tmp9) + (256*tmp15*tmp157*((tmp164*tmp1693*tmp3*tmp4&
            &08)/tmp54 - (tmp2533*tmp522*tmp542)/(tmp100*tmp9) + tmp2533*tmp2765*tmp97 + tmp2&
            &760*tmp3*tmp522*tmp713*tmp966*tmp980 + tmp159*tmp408*tmp469*tt + (tmp713*tmp97*(&
            &(tmp2533*tmp2760*tmp705)/(tmp100*tmp55) + tmp522*(tmp2760*tmp3*(tmp41 + tmp5 + t&
            &mp566 + tmp1*tmp819) + (tmp158*tmp244*(1/tmp11 + tmp69)*tt)/tmp100)))/tmp9))/(tm&
            &p10*tmp55) + (256*tmp15*tmp157*tmp72*(tmp1071*tmp408*tmp564 + (tmp2533*tmp542*tm&
            &p564)/tmp100 + tmp1071*tmp2760*tmp564*tmp713*tmp966 - tmp158*tmp2419*tmp564*tmp9&
            &88 + (tmp408*tmp564*tmp988)/tmp11 + tt*((tmp2533*tmp2760*tmp713*tmp97*tmp985)/(t&
            &mp100*tmp9) + tmp564*(-(tmp2760*(tmp2766/tmp100 - tmp158*(tmp2766 - (tmp2767 + t&
            &mp3*tmp713)/tmp100))) + (tmp158*(tmp177 + tmp1*tmp182 + tmp3225)*tmp713*tt)/tmp1&
            &00))))/tmp55 + (256*tmp157*tt*(tmp158*tmp1585*tmp1688*tmp175*tmp249 + (m2*tmp229&
            &6*tmp249*tmp2723)/tmp10 + (tmp158*tmp2734*tmp689*tmp97)/(tmp10*tmp100) - tmp249*&
            &(-(tmp2734*(tmp1584/tmp100 + tmp1585/tmp100 + tmp36/tmp10 + tmp6/tmp10 + m2*tmp9&
            &87*tt)) + (tmp158*tt*((-128*tmp32)/tmp10 - 6*m2*((tmp2761*tmp690)/tmp11 + tmp5*(&
            &12/tmp55 + tmp78 + tmp808) + tmp6*(tmp1814 + tmp316 + tmp824)) + 3*(tmp2531 + tm&
            &p2762*tmp44 + tmp474 + tmp48*(tmp1063 + tmp189 + tmp987)) + tmp2618*(tmp1690*tmp&
            &1823 + tmp2761*tt)))/tmp100)))/(tmp54*tmp55*tmp9)))/16. + (logu*tmp11*tmp13*tmp1&
            &4*tmp16*tmp240*tmp357*tmp411*tmp53*tmp56*tmp58*tmp67*((-256*tmp157*tmp17*tmp2533&
            &*tmp7*(128*tmp252*tmp418*(tmp1834 + tmp5 + tmp545) + (16*tmp32*(-6*tmp1529*tmp48&
            & + tmp418*tmp567 + tmp1560*(tmp492 + tmp493 + (tmp138 + tmp1592 + tmp344)/(tmp54&
            &*tmp55) + (tmp1592 + tmp226 + tmp420)*tmp60) + (tmp1693*(tmp492 + tmp493 + (tmp1&
            &38 + tmp1816 + tmp1817)/(tmp54*tmp55) + (tmp1816 + tmp226 + tmp420)*tmp60))/tmp1&
            &1))/tmp11 - (4*tmp4*(tmp254*tmp418 + tmp505*tmp532 + tmp1529*tmp3211*tmp6 + 5*tm&
            &p499*tmp738 - 3*tmp2731*(tmp412 + tmp413 + (tmp342 + tmp61 + tmp672)/(tmp54*tmp5&
            &5) + tmp60*(tmp342 + tmp77 + tmp80))))/tmp11 - 32*tmp30*(-tmp2792 + tmp418*tmp46&
            &2 + (tmp1834*(tmp488 + tmp489 + (tmp134 + tmp1734 + tmp422)/(tmp54*tmp55) + (tmp&
            &134 + tmp229 + tmp421)*tmp60))/tmp11 + tmp1560*(tmp412 + tmp413 + (tmp1732 + tmp&
            &426 + tmp61)/(tmp54*tmp55) + tmp60*(tmp426 + tmp77 + tmp80))) + (m2*tmp2296*(tmp&
            &2179*tmp499 + tmp1693*tmp418*tmp5 - tmp1822*tmp738 - tmp2731*(tmp412 + tmp413 + &
            &(tmp1530 + tmp320 + tmp61)/(tmp54*tmp55) + tmp60*(tmp320 + tmp77 + tmp80)) + (tm&
            &p1767*tmp72)/(tmp55*tmp9)))/tmp11 + (tmp2479*(tmp1535 + tmp1560 + tmp44 + tmp48)&
            &*tmp5*tmp980)/tmp10))/(tmp100*tmp96*tmp99) + (256*tmp157*tmp250*tmp581*(-(tmp527&
            &*((tmp158*tmp1833*tmp705)/(tmp10*tmp100*tmp55) - (tmp1842*tmp695*tmp713)/tmp10))&
            & + m2*tmp1550*tmp410*tmp695*tmp987 + (tmp1550*tmp410*tmp695*tt)/tmp10 + (tmp1699&
            &*tmp527*tmp695*tt)/tmp10))/(tmp10*tmp55) + 256*tmp157*tmp1650*tmp250*tmp581*(tmp&
            &1*tmp1550*tmp410*tmp708 + tmp527*((tmp158*tmp1833*tmp705)/(tmp100*tmp55) + tmp18&
            &42*tmp708*tmp713) + tmp1550*tmp410*tmp708*tt + tmp1699*tmp527*tmp708*tt) - (256*&
            &tmp157*((tmp249*tmp2767*tmp410*tmp446*tmp527)/tmp11 + (tmp1699*tmp249*tmp36*tmp5&
            &27*tmp581)/tmp10 + (tmp158*tmp1833*tmp2619*tmp527*tmp581*tmp689)/tmp10 + m2*tmp2&
            &49*tmp410*tmp446*tmp527*tmp686*tmp7 + (m2*tmp1550*tmp249*tmp410*tmp433*tmp581)/t&
            &mp9 + tmp1550*tmp249*tmp3215*tmp410*tmp581*tt + tmp249*tmp527*tmp581*tmp94*(-(tm&
            &p1826*tmp1833) + (tmp158*tt*(tmp3217 + tmp1823*tmp5 + (tmp1057 + tmp1773)*tmp6 +&
            & tmp2194*tmp684 + tmp1*((tmp108 + tmp1574 + tmp24)/tmp11 + tmp3216*tt)))/tmp100)&
            &))/(tmp54*tmp55*tmp9) - (256*tmp157*tmp72*((tmp2767*tmp410*tmp446*tmp469*tmp527)&
            &/tmp11 + (tmp1550*tmp2549*tmp410*tmp469*tmp581)/tmp11 + (tmp1550*tmp2551*tmp410*&
            &tmp469*tmp581)/tmp11 + (tmp1699*tmp36*tmp469*tmp527*tmp581)/tmp54 + m2*tmp410*tm&
            &p446*tmp469*tmp527*tmp686*tmp7 + tmp527*tmp581*tmp94*((tmp1584*tmp1833*tmp627*tt&
            &)/tmp100 + tmp469*(-(tmp178*tmp1838) - 16*tmp4*(tmp1576 + tmp1560*tmp1844 + 2*tm&
            &p1535*tmp1845 + tmp48*(tmp1772 + 1/tmp54)) + (tmp241*(5*tmp2304 + tmp1560*(tmp16&
            &08 + tmp260) + tmp1846*tmp428 + tmp1535*(tmp1063 + tmp974)))/tmp100 + tmp714*(tm&
            &p470 + tmp1843*tmp6 + tmp3221*tt)))))/tmp55))/16. + (discbulogt*tmp16*tmp240*tmp&
            &357*tmp411*tmp53*tmp56*tmp58*tmp65*((256*tmp157*tmp177*(tmp241*tmp249*tmp542 + (&
            &tmp2533*tmp542*tmp844)/tmp100 - tmp249*(tmp1665 + tmp2118*tmp410 - 64*tmp30*(tmp&
            &1827 + tmp433) - 32*tmp32*(tmp2031 + 29*tmp36 + 17*tmp6) + (115*tmp1560 + tmp203&
            &6 + tmp2601 + tmp2602)*tmp684 + tmp1*(tmp2042 + 64*tmp2731 + tmp1539*tmp44 + 107&
            &*tmp5*tmp6 + 15*tmp93))))/(tmp54*tmp55*tmp9) + 256*tmp157*tmp17*tmp177*tmp3*(512&
            &*tmp272*tmp418 - 32*tmp30*(tmp1524*tmp2600 + tmp452*(tmp1740*tmp19 + (tmp17*tmp2&
            &095)/tmp10 + (tmp2095 + tmp2096 + tmp307)/(tmp54*tmp55) + (tmp2094 + tmp307 + tm&
            &p366)*tmp60) + tmp36*(tmp2089 + tmp2090 + (tmp2091 + tmp2093 + tmp391)*tmp60 + (&
            &tmp2092 + tmp2093 + 308*tmp70)/(tmp54*tmp55))) - (4*tmp4*(tmp3200*(tmp512 + tmp5&
            &13 + (tmp2127 + tmp213 + tmp354)/(tmp54*tmp55) + (tmp112 + tmp190 + tmp213)*tmp6&
            &0) + (tmp2107 + tmp2108 + (tmp112 + tmp2109 + tmp213)/(tmp54*tmp55) + (tmp213 + &
            &tmp328 + tmp351)*tmp60)*tmp624 + 3*tmp1535*(tmp2102 + tmp2103 + (tmp2104 + tmp21&
            &06 + tmp329)*tmp60 + (tmp2105 + tmp2106 + 176*tmp70)/(tmp54*tmp55)) + 2*tmp1560*&
            &((tmp17*tmp2100)/tmp10 + 62*tmp413 + tmp60*(tmp2099 + tmp2101 + 124*tmp61) + (tm&
            &p2100 + tmp2101 + 361*tmp70)/(tmp54*tmp55))))/tmp100 + tmp241*tmp410*(tmp1552*tm&
            &p499 + 2*tmp1535*(tmp2107 + tmp2108 + (tmp112 + tmp2098 + tmp222)/(tmp54*tmp55) &
            &+ (tmp222 + tmp328 + tmp351)*tmp60) + 4*tmp1560*(tmp492 + tmp493 + (tmp2097 + tm&
            &p226 + tmp420)*tmp60 + (tmp138 + tmp2097 + 52*tmp70)/(tmp54*tmp55)) + tmp567*(tm&
            &p424 + tmp425 + (tmp103 + tmp213 + tmp354)*tmp60 + (tmp1595 + tmp213 + tmp80)/(t&
            &mp54*tmp55))) + 16*tmp32*(tmp48*((tmp17*tmp2094)/tmp10 + 38*tmp413 + (tmp1619 + &
            &tmp2094 + tmp2970)/(tmp54*tmp55) + (tmp1619 + tmp2115 + tmp263)*tmp60) + 2*tmp15&
            &35*(tmp2112 + tmp2113 + (tmp1040 + tmp1870 + tmp2114)*tmp60 + (tmp1041 + tmp2114&
            & + 239*tmp70)/(tmp54*tmp55)) + 3*tmp1560*(45*tmp413 + tmp17*tmp1862*tmp61 + (tmp&
            &2111 + tmp613 + 203*tmp70)/(tmp54*tmp55) + tmp60*(tmp2110 + tmp2111 + tmp837)) +&
            & 50*tmp44*(tmp412 + tmp413 + tmp124*tmp60 + (tmp359 + tmp61 + tmp88)/(tmp54*tmp5&
            &5))) + (tmp1629*tmp2479*(tmp1707 + tmp49 + tmp567 + tmp750))/(tmp10*tmp9) + 64*t&
            &mp252*(tmp3178*(tmp412 + tmp413 + (tmp109 + tmp197 + tmp61)/(tmp54*tmp55) + tmp6&
            &0*(tmp197 + tmp77 + tmp80)) + (tmp17*tmp2378 + tmp3069 + (tmp200 + tmp2793 + tmp&
            &509)/(tmp54*tmp55) + tmp60*(tmp1759 + tmp509 + tmp81))*tt)) + (256*tmp157*tmp177&
            &7*(m2*tmp522*tmp542*tmp690 + (tmp158*tmp542*tmp705)/(tmp100*tmp55) - tmp522*(16*&
            &tmp32*(168*tmp1535 + 241*tmp1560 + tmp2058 + 29*tmp48) + tmp1665*tmp554 - 64*tmp&
            &30*(tmp2053 + tmp2173 + 22*tmp6) - 8*tmp4*(tmp2064 + 93*tmp2731 + 236*tmp5*tmp6 &
            &+ 229*tmp738 + 8*tmp93) + tmp2118*tmp410*tmp94 + tmp69*(tmp156 + tmp2070 + 91*tm&
            &p48*tmp5 + 144*tmp44*tmp6 + tmp93*tmp952 + 104*tmp89*tt))))/(tmp10*tmp9) + (256*&
            &tmp157*tmp72*(tmp241*tmp542*tmp564*tmp988 + (tmp2533*tmp542*tmp985*tt)/(tmp100*t&
            &mp9) + tmp564*(tmp2084*tmp542 + (tmp158*tt*(-112*tmp1068*tmp32 + tmp48*(tmp1580 &
            &+ tmp185 + 1/tmp55) + 3*tmp1535*(tmp152 + tmp333 + 1/tmp55) + tmp2119*tmp567 + t&
            &mp1*((tmp128 + tmp1845)*tmp565 + (tmp686*(tmp1063 + tmp152 + tmp75))/tmp11 + tmp&
            &6*(tmp1574 + tmp333 + tmp974)) + tmp2636*tmp3211*tt + tmp684*(tmp698*(tmp1574 + &
            &tmp75 + tmp808) + (tmp1580 + tmp2420 + tmp472)*tt)))/tmp100)))/tmp55))/16. - (sc&
            &alarc0ir6s*tmp240*tmp357*tmp411*tmp53*tmp56*tmp59*tmp65*(256*tmp11*tmp15*tmp157*&
            &tmp177*tmp96*(-1024*tmp1090*tmp244*tmp259 + 256*tmp1109*tmp17*tmp272 + 16*tmp30*&
            &(tmp1536*(tmp1763 + (tmp286 + tmp351 + tmp384)*tmp636 + tmp60*(tmp1177 + tmp1764&
            & + tmp2683 - 10*tmp642 + 88*tmp61*tmp70) + ((tmp1202 + tmp1203 + tmp1737 + tmp26&
            &67)*tmp72)/tmp55 + (tmp1170 + tmp1737 + tmp1761 + tmp683)*tmp74) + 2*tmp1535*((t&
            &mp1422*(tmp1619 + tmp1733 + tmp1734))/tmp55 + tmp1732*tmp61*tmp632 + (tmp1732 + &
            &tmp1991 + tmp208)*tmp636 + tmp60*(tmp2163 + tmp2651 - 77*tmp642 + 14*tmp643 + tm&
            &p362*tmp70) + tmp74*(tmp1159 + tmp1735 + tmp1736 - 32*tmp83)) + 2*tmp1560*(tmp19&
            &19*tmp632 + (-165*tmp1650 + tmp1731 + tmp2253 + tmp2679)*tmp74 + (tmp72*(-287*tm&
            &p1422 + 254*tmp1650 + tmp2514 - 65*tmp83))/tmp55 + tmp60*(tmp2023 - 22*tmp643 + &
            &387*tmp61*tmp70 + tmp653*tmp82 - (300*tmp83)/tmp10) + tmp636*(tmp1624 + tmp379 +&
            & tmp865)) + tmp166*tmp244*(12*tmp1650*tmp632 + (tmp1154 + tmp384 + 101*tmp61)*tm&
            &p74 + (496*tmp1422 + tmp1729 + tmp1730 - 76*tmp82)/(tmp54*tmp55) + tmp781*(tmp11&
            &63 + tmp1729 + tmp796 + tmp90))) - 64*tmp252*(tmp36*(tmp1725 + tmp2873 + (tmp263&
            &8*(tmp193 + tmp227 + tmp2391 + tmp591))/(tmp10*tmp55) + tmp60*(tmp1727 + tmp2151&
            & + tmp2284 + tmp2349 + tmp1874*tmp70) + (tmp1114 + tmp1126 + tmp1726 + tmp674)*t&
            &mp74) + tmp101*(-5*tmp1650*tmp632 + (-117*tmp1650 + tmp2389 + tmp772 - 14*tmp82)&
            &/(tmp54*tmp55) + tmp74*(tmp1722 + tmp359 + tmp875) + tmp60*(tmp1723 + tmp1724 + &
            &tmp2516 + tmp878)) + tmp6*(tmp502*tmp61*tmp632 - tmp636*(tmp421 + tmp608 + tmp72&
            &) + tmp60*(tmp2252 - 14*tmp641 + 11*tmp643 - 78*tmp61*tmp70 + tmp774/tmp10) + tm&
            &p74*(tmp1140 + 55*tmp1650 + tmp1728 - 28*tmp83) + (tmp72*(tmp1728 + tmp2790 + tm&
            &p83 + tmp920))/tmp55)) + 4*tmp4*(tmp156*tmp1772*tmp626*tmp671 + tmp44*tmp6*(tmp1&
            &763 + (tmp1377 + tmp384 + tmp1393/tmp54)*tmp636 + ((tmp1399 - 718*tmp1422 + 549*&
            &tmp1650 + tmp2968)*tmp72)/tmp55 + (-165*tmp1422 + tmp1737 + tmp1999 + tmp2559)*t&
            &mp74 + tmp60*(tmp1764 + tmp2571 + 34*tmp641 + 916*tmp61*tmp70 - (519*tmp83)/tmp1&
            &0)) + tmp690*(tmp1758 + (tmp1759 + tmp1760 + tmp224)*tmp636 + ((tmp1381 - 359*tm&
            &p1422 + 331*tmp1650 + tmp1986)*tmp72)/tmp55 + tmp2482*(tmp1761 + tmp2292 + tmp66&
            &2 + tmp803) + tmp60*(tmp1762 + tmp2294 + tmp2591 + tmp2556/tmp54 - (303*tmp83)/t&
            &mp10))*tmp89 - tmp143*tmp244*(2*tmp1650*tmp632 + (tmp103 + tmp1865 + tmp1358/tmp&
            &54)*tmp74 + (193*tmp1422 - 210*tmp1650 + tmp2291 + tmp780)/(tmp54*tmp55) + tmp60&
            &*(-175*tmp1422 + tmp151 + tmp1757 + tmp897)) + tmp1536*tmp5*(tmp1765 + (65*tmp16&
            &50 + tmp385 + tmp1875/tmp54 + tmp656)*tmp74 + tmp636*(tmp103 + tmp419 + tmp80) +&
            & (tmp2479*(tmp1766 + tmp2388 + tmp661 + tmp83))/tmp10 + tmp60*(tmp1653 + 128*tmp&
            &642 - 230*tmp61*tmp70 + tmp2279*tmp83 + tmp919)) - (2*(tmp482*tmp636 + tmp663 + &
            &(tmp151 + tmp195 + tmp2378 + tmp683)*tmp74 + tmp60*(tmp641 + tmp665 - 37*tmp61*t&
            &mp70 + tmp770*tmp82 + tmp1580*tmp83) + (tmp2638*tmp632*tmp853)/(tmp10*tmp55))*tm&
            &p93)/tmp11) - 8*tmp32*(-4*tmp2731*(tmp1725 + (tmp120 + tmp196 + tmp420)*tmp636 +&
            & tmp2482*(-18*tmp1422 + tmp1752 + tmp667 + tmp668) + tmp60*(tmp1727 + tmp1753 + &
            &tmp195/tmp10 + tmp2171/tmp54 - 52*tmp61*tmp70) + tmp1772*(tmp1745 + tmp195 + tmp&
            &2671 + tmp662)*tmp72) + tmp5*tmp6*(tmp1185 + (tmp114 + tmp1310 + tmp330)*tmp636 &
            &+ ((tmp1316 - 684*tmp1422 + 461*tmp1650 + tmp1751)*tmp72)/tmp55 + (-307*tmp1650 &
            &+ tmp1749 + tmp1750 + tmp368)*tmp74 + tmp60*(tmp1194 + 24*tmp641 - 396*tmp642 + &
            &938*tmp61*tmp70 - (451*tmp83)/tmp10)) + tmp244*tmp2730*(tmp1735*tmp632 + (427*tm&
            &p1422 - 460*tmp1650 + tmp370 - 44*tmp82)/(tmp54*tmp55) + tmp60*(tmp1286 - 188*tm&
            &p1650 + tmp674 + 154*tmp83) + tmp74*(tmp1277 + tmp828 + tmp833)) + tmp2178*(tmp2&
            &482*(-140*tmp1422 + tmp1737 + tmp1748 + tmp2554) + tmp632*tmp70*tmp810 + (tmp72*&
            &(tmp1297 - 509*tmp1422 + 455*tmp1650 + 156*tmp82))/tmp55 + tmp60*(tmp1968 + tmp2&
            &955/tmp54 + 56*tmp641 + 637*tmp61*tmp70 - (475*tmp83)/tmp10) + tmp636*(56*tmp61 &
            &- 24*tmp70 + tmp912)) - 2*(tmp1754 + tmp2689*tmp636 + tmp60*(tmp1738 + tmp1756 +&
            & tmp2355 + tmp2190*tmp83 + tmp886) + (tmp72*(tmp1345 + tmp1346 + tmp1755 + tmp90&
            &))/tmp55 + tmp74*(tmp652 + tmp664 + tmp90 + tmp921))*tmp93) + tmp2304*tmp72*(tmp&
            &1219*tmp160 + tmp3211*tmp6*(tmp107 + tmp151 + tmp1635 + tmp1742 + tmp1743 + tmp2&
            &128 + (tmp1580 + tmp602)/(tmp54*tmp55)) + 2*tmp2731*(tmp1744 + tmp1745 + tmp2283&
            & + tmp264 - tmp60*(tmp189 + tmp75) + (tmp1747 + tmp190 + tmp766)/tmp55 + tmp90) &
            &+ tmp2178*(tmp145 + tmp1632 + tmp1634 + 14*tmp1650 - tmp376*tmp60 + (tmp1741 + t&
            &mp191 + tmp805)/tmp55 + tmp921) + (tmp1125 + (tmp1746 + tmp1747 + tmp384)/tmp55 &
            &+ tmp74 + tmp60*(tmp658 + tmp78) + tmp83 + tmp90 + tmp922)*tmp93) + tmp5*(tmp124&
            &3/(tmp54*tmp55) + tmp3221*tmp48*tmp72*(73*tmp1650 + tmp1740 + tmp2960 + tmp2967 &
            &+ tmp682 + (tmp1618 - 44*tmp70 + 118*tmp72)/tmp55 - tmp60*(tmp198 + tmp765)) + t&
            &mp2178*(tmp663 + ((-117*tmp1422 + 110*tmp1650 + tmp2509 + tmp2958)*tmp72)/tmp55 &
            &+ tmp636*(tmp354 + 48*tmp72 + tmp77) + tmp60*(tmp1738 + tmp2694/tmp54 + tmp665 +&
            & 119*tmp61*tmp70 - (88*tmp83)/tmp10) + tmp74*(-91*tmp1422 + tmp151 + tmp652 + tm&
            &p882)) + tmp516*tmp6*(tmp663 + tmp636*(tmp1739 + tmp354 + tmp77) + tmp60*(tmp173&
            &8 + tmp1282/tmp54 + tmp665 + 170*tmp61*tmp70 - (104*tmp83)/tmp10) + (tmp2480*(-3&
            &2*tmp1650 + tmp192 - 11*tmp82 + tmp906))/tmp54 + tmp2482*(tmp2104/tmp54 + tmp656&
            & + tmp82 + tmp918)) + (tmp2479*(tmp1635 + tmp1755 + tmp371 + (tmp361 + tmp606 + &
            &tmp666)/tmp55 + tmp74 + tmp60*(tmp1579 + tmp75) + tmp83)*tmp93)/tmp10)*tmp966) +&
            & 256*tmp157*tmp72*(tmp1584*tmp25*tmp2551*tmp61*tmp755 + tmp713*(tmp3155 + tmp143&
            &8*tmp85) + (tmp1800*tmp708)/(tmp54*tmp9))*tt + (256*tmp157*((tmp1800*tmp695)/tmp&
            &10 + (tmp1776 + tmp1786)*tmp713 + (tmp158*tmp705*tmp755)/(tmp10*tmp55))*tt)/(tmp&
            &10*tmp55*tmp9) - (256*tmp15*tmp157*tmp177*((tmp2533*tmp735)/(tmp10*tmp55) + tmp1&
            &1*tmp158*tmp249*tmp713*tmp735*tmp96 + tmp249*(tmp1663 + tmp1767 + tmp2741/tmp11 &
            &+ tmp532 + 21*tmp5*tmp6 + 18*tmp738 + 4*tmp4*(tmp1457 + tmp1769 + tmp741/tmp11) &
            &+ (50*tmp1535 + 73*tmp1560 + tmp1664 + 10*tmp48)*tmp966 - 8*tmp32*(tmp1768 + 29*&
            &tt))))/(tmp54*tmp55*tmp9) - (256*tmp15*tmp157*tmp1777*((tmp1584*tmp25*tmp2551*tm&
            &p735)/tmp54 + tmp713*(tmp3193 + tmp1447*tmp964) + (tmp522*(tmp1665 + tmp1802 + 8&
            &*tmp32*(tmp1667 + tmp172 + 55*tmp36) - (4*(tmp1803 + tmp1886 + 87*tmp36)*tmp4)/t&
            &mp11 + m2*(tmp1668 + tmp2408 + 28*tmp44 + tmp49)*tmp518 + tmp1545*(tmp567 + tmp2&
            &599*tmp6 + tmp624 + tmp2031*tt)))/tmp9))/tmp10 + (256*tmp11*tmp13*tmp157*tt*((tm&
            &p158*tmp36*tmp581*tmp689*tmp755)/tmp10 + tmp158*tmp177*tmp249*tmp703*tmp755 + tm&
            &p249*tmp581*tmp94*(tmp1586*tmp755 + tmp158*tmp97*(tmp1811 + tmp166*tmp2762 + 8*t&
            &mp32*(tmp1813 + tmp1814 + tmp473) + tmp825 + tmp2426/tmp9 + tmp241*(tmp1815*tmp3&
            &174 + tmp1834*(tmp267 + 1/tmp55) + tmp5*(tmp1581 + tmp592 + tmp987)) - 4*tmp4*((&
            &tmp2193 + tmp478 + tmp479)/tmp11 + (tmp108 + tmp1574 + tmp2572)*tt)))))/(tmp54*t&
            &mp55*tmp9) + (256*tmp157*tmp72*tt*(tmp11*tmp164*tmp3218*tmp713*tmp755*tmp96 + tm&
            &p1584*tmp627*tmp755*tmp97 - tmp469*(tmp2553*tmp755 + tmp158*tmp97*(tmp1811 + tmp&
            &166*(tmp1063 + tmp260) + 8*tmp32*(tmp185 + tmp473) + tmp241*(tmp1834*tmp1843 + t&
            &mp1812*tmp3174 + (tmp1581 + tmp214)*tmp5) + tmp2426/tmp55 + tmp825 - 4*tmp4*(tmp&
            &478/tmp11 + tmp571/tmp54 + tmp1574*tt - (19*tt)/tmp54)))))/tmp55 + (256*tmp15*tm&
            &p157*tmp72*(tmp11*tmp158*tmp164*tmp177*tmp713*tmp735*tmp96 + (tmp2533*tmp735*tmp&
            &985*tt)/tmp9 + tmp564*(tmp1810*tmp735 + tmp158*tt*(tmp1806 + tmp1808 + tmp1811 -&
            & 8*tmp1804*tmp32 + tmp3225*tmp726 + (tmp1551*tmp19 + (tmp1610 + tmp2709 + tmp349&
            &)*tmp5 + ((tmp1058 + tmp273 + tmp333)*tmp690)/tmp11)*tmp966 + 4*tmp4*((tmp1721 +&
            & 29/tmp55 + tmp75)/tmp11 + (tmp1004 + tmp117 - 5/tmp55)*tt)))))/tmp55))/16. + (d&
            &iscbtlogu*tmp14*tmp240*tmp357*tmp411*tmp53*tmp56*tmp59*tmp67*((256*tmp157*tmp165&
            &0*(tmp1553 + tmp1571*tmp708 + tmp1550*tmp158*tmp241*tmp708))/tmp9 - (256*tmp157*&
            &tmp70*(tmp1553 - tmp1571*tmp695 + tmp1550*tmp158*tmp695*tmp966))/(tmp55*tmp9) - &
            &256*tmp157*tmp17*tmp2533*tmp7*(768*tmp272*tmp418 + (tmp1537*tmp2638*tmp48)/(tmp1&
            &0*tmp55*tmp9) + tmp4*tmp686*(tmp418*tmp44 + 2*tmp1560*(tmp512 + tmp513 + (tmp153&
            &0 + tmp354 + tmp426)/(tmp54*tmp55) + (tmp112 + tmp190 + tmp426)*tmp60) + tmp1535&
            &*(tmp412 + tmp413 + (tmp312 + tmp61 - 28*tmp70)/(tmp54*tmp55) + tmp60*(tmp312 + &
            &tmp77 + tmp80)) + tmp752*(tmp1533 + tmp1847 + (tmp1534 + tmp155 + tmp2146)/(tmp5&
            &4*tmp55) + tmp60*(tmp1534 + tmp196 + tmp92))) + tmp6*(3*tmp1529*tmp1560 + (tmp16&
            &93*tmp499)/tmp11 + tmp44*tmp499 + tmp48*(tmp492 + tmp493 + (tmp138 + tmp1531 + t&
            &mp1532)/(tmp54*tmp55) + (tmp1532 + tmp226 + tmp420)*tmp60))*tmp966 - 16*tmp32*(t&
            &mp1529*tmp171 + tmp36*(tmp488 + tmp489 + (tmp139 + tmp1530 + tmp422)/(tmp54*tmp5&
            &5) + (tmp139 + tmp229 + tmp421)*tmp60) + tmp1546*(tmp424 + tmp425 + (tmp103 + tm&
            &p343 + tmp354)*tmp60 + (tmp343 + tmp608 + tmp80)/(tmp54*tmp55)))*tt - 64*tmp252*&
            &((9*tmp418)/tmp11 + tmp1524*tt) + tmp737*(tmp418*tmp5 + (tmp17/tmp10 + tmp20)*tm&
            &p452*tmp7**2 + tmp1529*tmp698*tt)) + (256*tmp157*tmp72*((tmp1550*tmp158*tmp241*t&
            &mp469)/tmp55 + (tmp1550*tmp1584*tmp627*tt)/tmp99 - tmp469*(tmp1565*tmp1693*tmp17&
            &8 - 64*tmp30*(tmp1487 + (tmp1573 + tmp260)*tmp41 + tmp36*(1/tmp54 + 18/tmp55)) +&
            & 16*tmp32*(tmp1576 + tmp1535*(tmp1060 + tmp214) + (tmp2527 + tmp327)*tmp48 + tmp&
            &1567/tmp55) - (1024*tmp272)/tmp55 + m2*tmp535*(tmp1560*tmp1846 + 3*tmp1535*tmp22&
            &45 + tmp2639*tmp44 + tmp48*(tmp1608 + tmp974)) - 8*tmp4*(tmp1576 + (tmp1575 + tm&
            &p1609)*tmp48 + tmp1560*(tmp1581 + tmp580) + 3*tmp1535*(tmp108 + tmp824))*tt + tm&
            &p409*(tmp1483 + tmp1844*tt))))/tmp55 - (256*tmp157*(tmp1550*tmp1584*tmp249*tmp96&
            &6 + (tmp1550*tmp158*tmp689*tt)/(tmp10*tmp99) + tmp249*(tmp1550*tmp1588 + (tmp158&
            &*tt*(tmp1577 + tmp2531 + (tmp1578 + tmp1579 + tmp1580)*tmp48 + tmp1*((tmp2296*tm&
            &p2530)/tmp11 + tmp481 + (tmp1582 - 13/tmp54 - 13/tmp55)*tmp6) + 16*tmp32*(tmp194&
            & + tmp316 + tmp824) + tmp825 - 8*tmp4*(tmp2196/tmp11 + (tmp1581 + tmp2368 + tmp5&
            &92)*tt)))/tmp99)))/(tmp54*tmp55*tmp9)))/16. + (discbslogu*tmp11*tmp13*tmp14*tmp2&
            &40*tmp357*tmp411*tmp53*tmp56*tmp59*((256*tmp157*tmp250*(tmp24*tmp466*tmp695 + (t&
            &mp158*tmp446*tmp705)/(tmp10*tmp55)))/(tmp10*tmp55*tmp96) - (256*tmp157*tmp1650*t&
            &mp250*((tmp158*tmp446*tmp705)/tmp55 + 2*tmp466*tmp708))/tmp96 + 256*tmp157*tmp17&
            &*tmp2533*tmp7*(-16*tmp32*tmp5*(tmp418*tmp455 + tmp36*(tmp2112 + tmp2113 + (tmp10&
            &40 + tmp1870 + tmp423)*tmp60 + (tmp1041 + tmp423 + 311*tmp70)/(tmp54*tmp55)) + t&
            &mp41*(tmp424 + tmp425 + (tmp103 + tmp354 + tmp426)*tmp60 + (tmp426 + tmp427 + tm&
            &p80)/(tmp54*tmp55))) + tmp4*tmp433*(tmp2179*tmp418 + tmp2792*tmp433 + tmp1693*tm&
            &p5*(tmp412 + tmp413 + (tmp432 + tmp61 - 52*tmp70)/(tmp54*tmp55) + tmp60*(tmp432 &
            &+ tmp77 + tmp80)) + 20*tmp418*tmp89 + tmp2178*(23*tmp1650*tmp17 + tmp2963 + (tmp&
            &306 + tmp430 + 157*tmp70)/(tmp54*tmp55) + tmp60*(tmp430 + tmp431 + tmp923))) + t&
            &mp1*tmp5*(tmp2792/tmp11 + tmp1693*tmp486*tmp5 + tmp418*tmp89 + (tmp2479*tmp93)/(&
            &tmp10*tmp9) + (tmp412 + tmp413 + (tmp345 + tmp429 + tmp61)/(tmp54*tmp55) + tmp60&
            &*(tmp345 + tmp77 + tmp80))*tmp934) + tmp1666*tmp30*(tmp418*tmp451 + tmp452*(tmp4&
            &24 + tmp425 + (tmp197 + tmp232 + tmp80)/(tmp54*tmp55) + tmp60*tmp873) + (tmp488 &
            &+ tmp489 + (tmp213 + tmp229 + tmp421)*tmp60 + (tmp213 + tmp422 + 30*tmp70)/(tmp5&
            &4*tmp55))*tmp939) + 768*tmp272*tmp418*(tmp518 + tmp97) + (tmp2304*tmp2638*(-tmp1&
            &560 + tmp428 + tmp524)*tt)/(tmp10*tmp9) - 64*tmp252*(tmp1693*tmp418 + 48*tmp418*&
            &tmp5 + tmp1524*tmp698*tt)) + (256*tmp157*(m2*tmp158*tmp249*tmp446*tmp690*tmp7 + &
            &(tmp158*tmp36*tmp446*tmp689)/(tmp10*tmp96) + tmp249*tmp94*((tmp158*(tmp1834*tmp2&
            &50 + (1/tmp10 + tmp1578 + tmp1579)*tmp44 + 16*tmp32*(1/tmp10 + tmp1813 + tmp473)&
            & + tmp474 + tmp475 + m2*tmp448*((1/tmp10 + tmp217 + tmp476)/tmp11 + tmp477) - 8*&
            &tmp4*(tmp480 + (tmp478 + tmp479 + tmp78)/tmp11))*tmp97)/tmp96 + tmp446*(tmp2414/&
            &tmp10 + tmp471 + tmp481 + tmp2618/tmp9 + m2*(-8*tmp250 + tmp1004*tt)))))/(tmp54*&
            &tmp55*tmp9) - (256*tmp157*tmp72*(m2*tmp158*tmp2296*tmp446*tmp469*tmp7 + (tmp1584&
            &*tmp36*tmp446*tmp627)/tmp96 + tmp469*tmp94*((tmp158*(tmp1535*tmp2637 + tmp166*(t&
            &mp2635 + tmp316) + 16*tmp32*(tmp472 + tmp473) + tmp474 + tmp475 - 8*tmp4*(tmp480&
            & + (tmp478 + 42/tmp54)/tmp11) + m2*tmp448*(tmp477 + (tmp476 + tmp808)/tmp11))*tt&
            &)/tmp96 - tmp446*(tmp2638*tmp36 + tmp470 + tmp471 + tmp2618/tmp55 + m2*(-8*tmp17&
            &6 + tmp808*tt)))))/tmp55))/16. + (logt*tmp14*tmp16*tmp240*tmp357*tmp411*tmp53*tm&
            &p56*tmp58*((256*tmp157*tmp1777*tmp67*((tmp522*tmp527*tmp542)/tmp11 + tmp158*tmp4&
            &10*tmp522*tmp563 + tmp410*tmp522*(1024*tmp252 - (1024*tmp30)/tmp100 + m2*tmp2296&
            &*(-6*tmp1560 + tmp2522 + tmp48) - 16*tmp4*(14*tmp1560 + tmp1700 + tmp48 + tmp524&
            &) + tmp1535*tmp582 + (tmp1551 + tmp523 + tmp565)*tmp714) + (tmp158*tmp25*tmp2551&
            &*tmp410*tmp550)/(tmp54*tmp99) + tmp522*tmp527*tmp542*tt))/(tmp10*tmp9) + (256*tm&
            &p11*tmp157*tmp17*tmp177*tmp3*tmp96*tmp99*(4096*tmp259*tmp418 - 512*tmp272*(tmp25&
            &99 + tmp395)*tmp418 - (16*tmp32*(-2*tmp1535*tmp499 - 6*tmp48*tmp499 + tmp3200*tm&
            &p505 + 3*tmp1560*(tmp492 + tmp493 + (tmp138 + tmp390 + tmp494)/(tmp54*tmp55) + (&
            &tmp226 + tmp390 + tmp420)*tmp60)))/tmp11 + 1024*tmp252*tmp486*tmp709 + 32*tmp30*&
            &(tmp2792 + 2*tmp1535*tmp499 + 2*tmp1560*(tmp488 + tmp489 + (tmp422 + tmp490 + tm&
            &p491)/(tmp54*tmp55) + (tmp229 + tmp421 + tmp490)*tmp60) + tmp2116*(tmp424 + tmp4&
            &25 + (tmp103 + tmp354 + tmp419)*tmp60 + (tmp2161 + tmp419 + tmp80)/(tmp54*tmp55)&
            &)) + (tmp1560*tmp2479*tmp410*tmp517)/(tmp10*tmp9) + tmp241*tmp709*(tmp2178*tmp50&
            &5 + tmp5*tmp6*(tmp512 + tmp513 + (tmp2096 + tmp354 + tmp506)/(tmp54*tmp55) + (tm&
            &p112 + tmp190 + tmp506)*tmp60) + 3*tmp2731*(tmp412 + tmp413 + (tmp427 + tmp507 +&
            & tmp61)/(tmp54*tmp55) + tmp60*(tmp507 + tmp77 + tmp80)) + (tmp2594*tmp89)/(tmp55&
            &*tmp9) + tmp499*tmp93) + tmp4*tmp433*(tmp1822*tmp529 + tmp1834*tmp5*(tmp492 + tm&
            &p493 + (tmp138 + tmp509 + tmp510)/(tmp54*tmp55) + (tmp226 + tmp420 + tmp509)*tmp&
            &60) - 3*tmp2731*(tmp488 + tmp489 + (tmp229 + tmp421 + tmp511)*tmp60 + (tmp422 + &
            &tmp511 + 62*tmp70)/(tmp54*tmp55)) - 2*(tmp512 + tmp513 + (tmp2518 + tmp354 + tmp&
            &514)/(tmp54*tmp55) + (tmp112 + tmp190 + tmp514)*tmp60)*tmp93 - (90*tmp2304*tmp72&
            &*tt)/tmp9)))/tmp100 + (256*tmp11*tmp13*tmp157*tmp7*tmp99*((tmp1*tmp158*tmp249*tm&
            &p410*tmp579)/tmp99 + (tmp158*tmp249*tmp410*tmp579)/(tmp11*tmp99) + (tmp249*tmp36&
            &*tmp542*tmp581)/tmp99 + (tmp249*tmp5*tmp542*tmp581)/tmp99 + tmp36*tmp410*tmp581*&
            &((tmp2533*tmp550)/(tmp10*tmp55) + tmp249*(tmp2172 + m2*(tmp172 + tmp2600) - 8*tm&
            &p4*(tmp443 + tmp448) + tmp582*tt))))/(tmp54*tmp55*tmp9) + (256*tmp157*tmp72*((tm&
            &p19*tmp542*tmp564)/tmp11 - tmp2533*tmp410*tmp563*tmp564*tmp67 + tmp13*tmp158*tmp&
            &410*tmp564*tmp579*tmp7 + tmp1*tmp11*tmp13*tmp158*tmp410*tmp564*tmp579*tmp7 + tmp&
            &542*tmp564*tmp988 + (tmp2533*tmp410*tmp550*(tmp86/tmp55 - tmp984)*tmp99*tt)/tmp9&
            & + tmp410*tmp564*tmp67*(tmp2536*tmp550 + (tmp158*tt*(tmp1830*tmp19 + tmp3*tmp516&
            & + tmp2537*tmp6 + tmp2422*tmp684 + tmp1*((tmp1574 + tmp2 + tmp260)/tmp11 + (tmp1&
            &058 + tmp194 + tmp85)*tt)))/tmp99)))/tmp55))/16. + (scalarc0u*tmp240*tmp411*tmp5&
            &3*tmp58*tmp59*tmp65*((-256*tmp157*tmp177*tmp357*tmp56*(tmp2345 + 128*tmp1944*tmp&
            &272 - 32*tmp252*(tmp244*tmp516*((tmp2175*(tmp1617 + 1/tmp54))/tmp10 + (tmp1618 +&
            & tmp1619 + tmp429)*tmp74 + tmp60*(tmp1118 + 159*tmp1650 + tmp1853 - 97*tmp83) + &
            &(tmp1628 + tmp2262 + tmp2351 - 236*tmp61*tmp70 + (217*tmp83)/tmp10)/tmp55) + tmp&
            &36*(tmp114*tmp2175*tmp604 + (tmp1620 + tmp2352 + 229*tmp61)*tmp636 + tmp74*(tmp1&
            &621 - 773*tmp1650 + tmp1951 + 452*tmp83) + tmp60*(tmp2018 + tmp2560 + 223*tmp641&
            & + 326*tmp642 - (1269*tmp83)/tmp10) + (tmp1952 + tmp1953 + 142*tmp642 - 714*tmp6&
            &1*tmp70 + tmp889)/(tmp10*tmp55)) + tmp6*(tmp147*tmp2450 + (tmp1257 + tmp1622 + t&
            &mp2357)*tmp636 + tmp74*(-301*tmp1422 - 537*tmp1650 + 118*tmp82 + 272*tmp83) + tm&
            &p60*(tmp2158 + 133*tmp641 - 40*tmp642 + 978*tmp61*tmp70 - (779*tmp83)/tmp10) + (&
            &tmp1623 + tmp1959 - 572*tmp61*tmp70 + (473*tmp83)/tmp10 + tmp889)/(tmp10*tmp55))&
            &) + 16*tmp30*(tmp244*tmp715*(tmp128*tmp2175*tmp2476 + (tmp1624 + tmp2358 + 92*tm&
            &p61)*tmp74 + tmp60*(tmp1878 + tmp1964 + tmp2563 + 28*tmp82) + (tmp1194 + 86*tmp6&
            &41 - 70*tmp642 + 379*tmp61*tmp70 - (389*tmp83)/tmp10)/tmp55) + tmp1560*(tmp153*t&
            &mp2464 + tmp636*(tmp2362 + 507*tmp61 - 132*tmp70) + tmp74*(-2117*tmp1422 - 1715*&
            &tmp1650 + tmp1751 + 1002*tmp83) + tmp60*(495*tmp641 + 815*tmp642 - 156*tmp643 + &
            &3177*tmp61*tmp70 - (3054*tmp83)/tmp10) + (tmp1626 + tmp1969 + 412*tmp642 - 1959*&
            &tmp61*tmp70 + (1996*tmp83)/tmp10)/(tmp10*tmp55)) + tmp1535*(tmp2484*(tmp144 + tm&
            &p2529) + 2*tmp636*(241*tmp61 - 82*tmp70 + 365*tmp72) + tmp74*(-1587*tmp1422 + tm&
            &p1625 - 1801*tmp1650 + 952*tmp83) + tmp60*(tmp2154 + 470*tmp641 + 169*tmp642 + 3&
            &603*tmp61*tmp70 - (3006*tmp83)/tmp10) + (tmp1626 + tmp1971 + 700*tmp642 - 2337*t&
            &mp61*tmp70 + (2124*tmp83)/tmp10)/(tmp10*tmp55)) + tmp48*(tmp2482*(-178*tmp1422 -&
            & 332*tmp1650 + tmp2123 + 80*tmp82) + tmp60*(tmp2499 + tmp2663 - 164*tmp642 + 138&
            &0*tmp61*tmp70 - (1004*tmp83)/tmp10) + tmp636*(tmp1627 + 161*tmp61 + tmp857) + tm&
            &p2484*(tmp128 + tmp859) + (tmp1058*(tmp1628 + 85*tmp642 - 227*tmp61*tmp70 + (182&
            &*tmp83)/tmp10 + tmp915))/tmp10)) + (4*tmp4*(tmp244*tmp2730*((263*tmp1422 - 282*t&
            &mp1650 + tmp1658 + tmp1728)/(tmp54*tmp55) + tmp2671*tmp632 + tmp60*(-224*tmp1422&
            & - 95*tmp1650 + tmp2693 + tmp662) + (tmp224 + tmp2361 + 196*tmp72)*tmp74) + tmp5&
            &*tmp6*(tmp263*tmp61*tmp632 + tmp636*(tmp263 + 167*tmp61 + 723*tmp72) + (tmp72*(1&
            &675*tmp1650 + tmp1660 + tmp1997 + 286*tmp82))/tmp55 + tmp74*(-1607*tmp1422 - 507&
            &*tmp1650 + tmp2558 + 334*tmp83) + tmp60*(167*tmp641 + 598*tmp642 - 38*tmp643 + 1&
            &726*tmp61*tmp70 - (1601*tmp83)/tmp10)) + tmp738*(tmp2965*tmp61*tmp632 + (tmp2395&
            & + tmp2965 + 143*tmp61)*tmp636 + (tmp72*(-1089*tmp1422 + 1124*tmp1650 + tmp1995 &
            &+ 170*tmp82))/tmp55 + tmp74*(-1339*tmp1422 - 465*tmp1650 + tmp1659 + 286*tmp83) &
            &+ tmp60*(143*tmp641 + 553*tmp642 - 36*tmp643 + 1349*tmp61*tmp70 - (1286*tmp83)/t&
            &mp10)) + tmp2731*(tmp1199 + ((-959*tmp1422 + 1048*tmp1650 + tmp1662 + tmp1998)*t&
            &mp72)/tmp55 + (-857*tmp1422 - 191*tmp1650 + tmp2364 + tmp371)*tmp74 + 4*tmp636*(&
            &tmp1661 + tmp1722 + tmp77) + tmp60*(tmp1212 + 72*tmp641 + 311*tmp642 + 895*tmp61&
            &*tmp70 - (826*tmp83)/tmp10)) + (tmp422*tmp632*tmp70 + ((-215*tmp1422 + 243*tmp16&
            &50 + tmp2002 + tmp220)*tmp72)/tmp55 + 7*tmp636*(tmp61 + tmp70 + tmp762) + tmp60*&
            &(tmp2148 + tmp2295 + 7*tmp643 + 169*tmp61*tmp70 + tmp2711*tmp82) + 14*tmp74*(tmp&
            &187 + tmp206 + tmp83 + tmp922))*tmp93))/tmp100 + (tmp1629*tmp72*(tmp2367 + tmp23&
            &77 + tmp738*(tmp145 + tmp1630 + tmp2461 + (tmp1414 + tmp1631 + tmp2370)/tmp55 + &
            &(-37/tmp10 + 1/tmp54)*tmp60 + 18*tmp74 + tmp929) + (tmp107 + tmp145 + tmp1634 + &
            &tmp1735 + tmp1743 + tmp1879 + tmp243*(tmp139 + tmp196 + tmp70))*tmp93 + tmp2412*&
            &(tmp1632 + 12*tmp1650 + tmp1742 + tmp187 + tmp264 + (tmp1633 + tmp429 + tmp72)/t&
            &mp55 + tmp781*(tmp76 + tmp931))))/tmp55 + tmp410*(tmp2731*(tmp2383 + 4*tmp636*(t&
            &mp61 + tmp666 + tmp70) + tmp74*(-261*tmp1422 + tmp1654 + tmp652 + tmp780) + (tmp&
            &72*(-176*tmp1422 + 237*tmp1650 + tmp1655 + tmp82))/tmp55 + tmp60*(tmp1641 + tmp1&
            &738 + tmp3065 + 156*tmp642 + tmp70*tmp833)) + tmp738*(tmp1199 + ((-272*tmp1422 +&
            & tmp1647 + 282*tmp1650 + tmp2381)*tmp72)/tmp55 + (-348*tmp1422 + tmp1646 + tmp16&
            &49 + tmp371)*tmp74 + tmp60*(tmp1212 + tmp1648 + 148*tmp642 + 277*tmp61*tmp70 - (&
            &260*tmp83)/tmp10) + tmp636*(tmp190 + tmp2380 + tmp860)) + tmp5*tmp6*(tmp1765 + (&
            &-453*tmp1422 + tmp1649 + tmp1650 + tmp385)*tmp74 + (tmp72*(-328*tmp1422 + 393*tm&
            &p1650 + tmp1651 - 90*tmp83))/tmp55 + tmp60*(tmp1648 + tmp1652 + tmp1653 + 237*tm&
            &p61*tmp70 - (281*tmp83)/tmp10) + tmp636*(tmp103 + 192*tmp72 + tmp860)) + tmp244*&
            &tmp2730*((tmp192 + tmp193 + tmp2280 + tmp3023/tmp10)/(tmp54*tmp55) + tmp74*(tmp1&
            &38 + 54*tmp72 + tmp77) + tmp872 + tmp60*(tmp1645 + tmp664 + tmp82 + tmp879)) + (&
            &(tmp1631 + tmp191 + tmp238)*tmp636 + ((70*tmp1650 + tmp2144 + tmp2682 + tmp662)*&
            &tmp72)/tmp55 + tmp881 + tmp60*(tmp1656 + tmp1657 + tmp2661/tmp54 + tmp510*tmp61 &
            &+ tmp884) - tmp74*(tmp195 + tmp2920 + tmp856 + tmp90))*tmp93)*tmp966 - 8*tmp32*(&
            &4*tmp738*((tmp1851 + tmp1864 + tmp2007/tmp54)*tmp636 + (tmp2011 + tmp2354 + 106*&
            &tmp642 + tmp643 - 574*tmp61*tmp70)/(tmp10*tmp55) + (-672*tmp1422 + tmp1639 - 386&
            &*tmp1650 + tmp2662)*tmp74 + (1/tmp10 + tmp1638)*tmp2175*tmp77 + tmp60*(tmp2511 +&
            & tmp2881 + 268*tmp642 + 837*tmp61*tmp70 - (803*tmp83)/tmp10)) + tmp2731*((1/tmp1&
            &0 + tmp146)*tmp2484 + tmp636*(tmp2405 + tmp3019 - 104*tmp70) + tmp74*(-1597*tmp1&
            &422 + tmp1640 - 1285*tmp1650 + 212*tmp82) + tmp60*(333*tmp641 + 138*tmp642 - 112&
            &*tmp643 + 3236*tmp61*tmp70 - (2595*tmp83)/tmp10) + (tmp1641 + tmp2019 + 696*tmp6&
            &42 - 2514*tmp61*tmp70 + (2365*tmp83)/tmp10)/(tmp10*tmp55)) + tmp5*tmp6*(tmp2450*&
            &(tmp144 + tmp364) + tmp636*(601*tmp61 - 172*tmp70 + 1449*tmp72) + tmp2482*(-1061&
            &*tmp1650 + tmp1660 + 175*tmp82 + 598*tmp83) + tmp60*(595*tmp641 + 871*tmp642 - 1&
            &84*tmp643 + 4959*tmp61*tmp70 - (4363*tmp83)/tmp10) + (-804*tmp641 + 866*tmp642 -&
            & 3659*tmp61*tmp70 + (3591*tmp83)/tmp10 + tmp889)/(tmp10*tmp55)) + (tmp153*tmp245&
            &0 + 2*(tmp1377 + tmp2021/tmp54 + tmp608)*tmp636 + (-305*tmp1422 + tmp1642 + tmp1&
            &643 + tmp1644)*tmp74 + tmp60*(tmp2497 + tmp2957 + tmp1115/tmp54 + 713*tmp61*tmp7&
            &0 - (544*tmp83)/tmp10) + (tmp2025 + tmp2463 - 583*tmp61*tmp70 + (542*tmp83)/tmp1&
            &0 + tmp915)/(tmp10*tmp55))*tmp93 + tmp244*tmp3122*((tmp1005 + tmp2401 + tmp2501)&
            &*tmp74 + tmp60*(tmp1636 + tmp1637 - 156*tmp1650 + tmp788) + (63*tmp641 - 42*tmp6&
            &42 + tmp665 + 302*tmp61*tmp70 - (322*tmp83)/tmp10)/tmp55 + (tmp2175*(1/tmp10 + t&
            &mp974))/tmp10))))/tmp100 + (256*tmp15*tmp157*tmp56*tt*((tmp158*tmp1699*tmp25*tmp&
            &2551)/(tmp100*tmp54) + tmp1717*tmp695 + tmp1688*tmp1699*tmp695*tt))/(tmp55*tmp9)&
            & + (256*tmp10*tmp15*tmp157*tt*((tmp158*tmp1699*tmp705)/(tmp100*tmp55) + tmp1717*&
            &tmp708 + tmp1688*tmp1699*tmp708*tt))/tmp9 + (256*tmp10*tmp15*tmp157*tmp1777*tmp5&
            &6*(-((tmp1447*tmp1687)/tmp100) + tmp2050 + tmp2051 - (tmp1687*tmp522)/(tmp100*tm&
            &p9) + (tmp164*tmp2423*tmp723*tt)/tmp54))/tmp100 + (256*tmp10*tmp15*tmp157*tmp54*&
            &(tmp1887 - tmp1688*tmp408*tmp564*tmp988 + (tmp564*(tmp1810*tmp408 + tmp158*tmp19&
            &05*tt))/tmp100))/(tmp100*tmp55) + (256*tmp15*tmp157*tmp177*tmp357*tmp54*(tmp2027&
            & - tmp1688*tmp249*tmp408 - (tmp249*(tmp1663 + 3*tmp410*tmp517 + (tmp1546 + 16*tm&
            &p36 + tmp39)*tmp696 + (54*tmp1535 + 75*tmp1560 + tmp1664 + 14*tmp48)*tmp966 - 8*&
            &tmp32*(tmp1768 + 37*tt)))/tmp100))/(tmp100*tmp55*tmp9) + (256*tmp10*tmp15*tmp157&
            &*tmp54*tt*(tmp1484*tmp1688*tmp1699*tmp469 + (tmp1584*tmp1699*tmp627*tt)/tmp100 +&
            & tmp469*(tmp1699*(tmp158*tmp2436 + tt/(tmp100*tmp54)) + (tmp158*tmp690*(tmp1478 &
            &- 8*tmp4*(tmp2638/tmp11 + tmp3221 + tmp2409/tmp54 + tmp2296/tmp55) - (m2*(tmp148&
            &3 + tmp214/tmp11 + tmp3186/tmp54 + tmp1058*tt))/tmp100 + 2*tmp1694*(tmp1720 + tt&
            &/tmp55)))/tmp100)))/tmp55 + (256*tmp15*tmp157*tmp357*tmp54*tt*((tmp1688*tmp1699*&
            &tmp249*tmp97)/tmp10 + (tmp158*tmp1699*tmp689*tmp97)/(tmp10*tmp100) + tmp249*(-(t&
            &mp1699*tmp1826) + (tmp158*tmp690*(tmp714/tmp9 - 8*tmp4*(tmp1718/tmp11 + (tmp179 &
            &+ tmp2670)*tmp97) - (m2*((tmp1058 + tmp198 + tmp260)/tmp11 + (tmp1058 + tmp1866)&
            &*tt))/tmp100 + 2*tmp1694*(tmp1720 + tt/tmp9)))/tmp100)))/(tmp55*tmp9)))/16. - (d&
            &iscbt*tmp240*tmp357*tmp411*tmp53*tmp56*tmp59*tmp65*tmp67*(256*tmp157*tmp158*tmp1&
            &77*(256*tmp244*tmp272*((204*tmp1422 + tmp2554 + tmp2555 + tmp611)/(tmp54*tmp55) &
            &+ (tmp2094*tmp632)/tmp10 + (tmp2105 + tmp263 + tmp628)*tmp74 + (-78*tmp1650 + tm&
            &p2556 + tmp2127/tmp54 + tmp611)*tmp781) - 32*tmp30*(tmp1545*tmp244*(95*tmp1650*t&
            &mp632 + (tmp2561 + tmp419 + 89*tmp61)*tmp74 + (tmp1157 + 886*tmp1422 + tmp2562 -&
            & 502*tmp82)/(tmp54*tmp55) + tmp60*(335*tmp1422 - 585*tmp1650 + tmp2563 + 95*tmp8&
            &2)) + tmp36*(-305*tmp61*tmp632*tmp70 + tmp636*(283*tmp61 - 305*tmp70 - 42*tmp72)&
            & + (tmp72*(-1711*tmp1422 + 480*tmp1650 + tmp768 + 1198*tmp82))/tmp55 + tmp74*(-1&
            &753*tmp1650 + tmp2564 + 610*tmp82 + 566*tmp83) + tmp60*(283*tmp641 - 1861*tmp642&
            & - 305*tmp643 + 3497*tmp61*tmp70 - (1678*tmp83)/tmp10)) + tmp6*(tmp636*(tmp197 +&
            & 171*tmp61 - 167*tmp70) - 167*tmp61*tmp632*tmp70 + (tmp72*(-636*tmp1422 + tmp192&
            & + tmp2565 + 490*tmp82))/tmp55 + tmp74*(171*tmp1422 - 845*tmp1650 + 334*tmp82 + &
            &342*tmp83) + tmp60*(171*tmp641 - 656*tmp642 - 167*tmp643 + 1486*tmp61*tmp70 - (8&
            &35*tmp83)/tmp10))) + 8*tmp32*(tmp244*tmp715*((-199*tmp1650 + tmp2516 + tmp2556 +&
            & tmp2910)*tmp60 + (tmp2095*tmp632)/tmp10 + (tmp105 + tmp1722 + tmp366)*tmp74 + (&
            &337*tmp1422 + tmp2507 + tmp772 - 182*tmp82)/(tmp54*tmp55)) + tmp1535*(tmp636*(tm&
            &p1596 + 411*tmp61 - 433*tmp70) - 433*tmp61*tmp632*tmp70 + (tmp670*(-1571*tmp1422&
            & + 505*tmp1650 + tmp768 + 1033*tmp82))/tmp10 + tmp2482*(691*tmp1422 - 1455*tmp16&
            &50 + 433*tmp82 + 411*tmp83) + tmp60*(411*tmp641 - 3390*tmp642 - 433*tmp643 + 611&
            &8*tmp61*tmp70 - (2786*tmp83)/tmp10)) + tmp1560*(tmp636*(tmp2478 + 271*tmp61 - 29&
            &5*tmp70) - 295*tmp61*tmp632*tmp70 + (tmp670*(-1497*tmp1422 + 554*tmp1650 + tmp77&
            &2 + 925*tmp82))/tmp10 + tmp2482*(728*tmp1422 - 1190*tmp1650 + 295*tmp82 + 271*tm&
            &p83) + tmp60*(271*tmp641 - 3226*tmp642 - 295*tmp643 + 5410*tmp61*tmp70 - (2264*t&
            &mp83)/tmp10)) + tmp624*(tmp2567*tmp61*tmp632 + (tmp2566 + tmp2567 + tmp288)*tmp6&
            &36 + ((tmp1370 + tmp2568 + tmp2569 + tmp2959)*tmp72)/tmp55 + tmp74*(-271*tmp1650&
            & + tmp2400 + 84*tmp82 + 96*tmp83) + tmp60*(tmp2570 + tmp2571 - 42*tmp643 + 537*t&
            &mp61*tmp70 - (287*tmp83)/tmp10))) + 4*tmp4*(tmp1777*tmp254*tmp72*(tmp2097 + tmp3&
            &06 - 25/(tmp10*tmp55) + tmp60 + 24*tmp70 + tmp798/tmp55) - 2*tmp2731*(tmp2482*(1&
            &92*tmp1422 - 331*tmp1650 + tmp2582 + tmp2583) + tmp1859*tmp61*tmp632 + tmp636*(t&
            &mp1859 + tmp2124 + tmp72) + (tmp72*(-878*tmp1422 + 354*tmp1650 + tmp186 + 513*tm&
            &p82))/tmp55 + tmp60*(tmp2584 - 898*tmp642 - 69*tmp643 + 1551*tmp61*tmp70 - (652*&
            &tmp83)/tmp10)) + tmp738*(tmp2579*tmp61*tmp632 + (tmp2578 + tmp2579 - 37*tmp61)*t&
            &mp636 + tmp1419*(279*tmp1422 - 323*tmp1650 + tmp2580 + 41*tmp82) + tmp1772*tmp72&
            &*(-515*tmp1422 + 225*tmp1650 + tmp227 + 287*tmp82) + tmp60*(-37*tmp641 + 1102*tm&
            &p642 + 41*tmp643 - 1682*tmp61*tmp70 + (610*tmp83)/tmp10)) + tmp172*tmp5*(tmp2581&
            &*tmp61*tmp632 + (tmp1592 + tmp2581 + tmp598)*tmp636 + (tmp72*(-1079*tmp1422 + 42&
            &7*tmp1650 + tmp772 + 634*tmp82))/tmp55 + tmp60*(65*tmp641 - 1173*tmp642 - 71*tmp&
            &643 + 1867*tmp61*tmp70 - (723*tmp83)/tmp10) + tmp2482*(284*tmp1422 - 385*tmp1650&
            & + 71*tmp82 + tmp847)) - 6*(tmp2592 + 2*(tmp138 + tmp190 + tmp208)*tmp636 + tmp6&
            &0*(tmp2151 + tmp2587 + tmp2588 + tmp2589 - 82*tmp642) + ((-106*tmp1422 + tmp1733&
            &/tmp10 + tmp2585 + tmp2586)*tmp72)/tmp55 + tmp74*(tmp1114 + tmp131 + tmp2287 + t&
            &mp788))*tmp93) + m2*tmp690*(tmp2178*(tmp1725 + (tmp342 + tmp354 + tmp420)*tmp636&
            & + tmp2482*(75*tmp1422 + tmp2956 + tmp656 + tmp668) + (tmp72*(-256*tmp1422 + 114&
            &*tmp1650 + tmp227 + 139*tmp82))/tmp55 + tmp60*(tmp1727 + tmp1738 + tmp2591 + tmp&
            &2846/tmp54 - (136*tmp83)/tmp10)) - tmp244*tmp2731*(tmp2691 + (tmp1649 - 309*tmp1&
            &650 + tmp2596 + tmp2694)*tmp60 + tmp74*(tmp2594 + tmp297 + tmp860) + (579*tmp142&
            &2 + tmp2595 - 292*tmp82 + tmp864)/(tmp54*tmp55)) + (tmp2480*tmp3*(tmp1221 + tmp1&
            &592 + tmp2590 + tmp421 + tmp427 + tmp60)*tmp89)/tmp54 + tmp2412*(tmp2592 + tmp74&
            &*(tmp1206 - 155*tmp1650 + tmp2593 + tmp788) + (tmp72*(-246*tmp1422 + 107*tmp1650&
            & + tmp656 + 135*tmp82))/tmp55 + tmp636*(tmp112 + tmp328 + tmp88) + tmp60*(tmp258&
            &9 - 262*tmp642 + 405*tmp61*tmp70 - (147*tmp83)/tmp10 + tmp892)) + (tmp1185 + (tm&
            &p1206 + tmp2597 + tmp2954 + tmp368)*tmp74 + (tmp72*(-190*tmp1422 + tmp2598 + 87*&
            &tmp82 - 14*tmp83))/tmp55 + tmp60*(tmp1194 - 134*tmp642 + 269*tmp61*tmp70 - (121*&
            &tmp83)/tmp10 + tmp892) + 2*tmp636*(tmp286 + tmp354 + tmp92))*tmp93) + 64*tmp252*&
            &(tmp395*(tmp636*(tmp1733 + tmp197 - 60*tmp70) - 60*tmp61*tmp632*tmp70 + tmp74*(t&
            &mp2400 + tmp2406 + tmp2559 + 120*tmp82) + (tmp72*(-206*tmp1422 + tmp192 + tmp265&
            &6 + 168*tmp82))/tmp55 + tmp60*(tmp2263 + tmp2560 - 226*tmp642 + 492*tmp61*tmp70 &
            &- (271*tmp83)/tmp10)) + tmp244*(152*tmp1650*tmp632 + (tmp419 + 141*tmp61 - 152*t&
            &mp70)*tmp74 + (158*tmp1422 - 361*tmp1650 + tmp2557 + tmp2558)*tmp781 + (1004*tmp&
            &1422 + tmp2557 + tmp2879 - 592*tmp82)/(tmp54*tmp55))*tmp94) + (tmp1693*tmp72*(tm&
            &p3*tmp3122*(tmp106 + tmp112 + tmp1220 + tmp419 - 10/(tmp10*tmp55) + tmp60) + tmp&
            &3226*tmp48*(tmp1532 + tmp2709/tmp10 - 18/(tmp54*tmp55) + tmp596 + tmp60 + tmp900&
            &) + tmp2296*tmp44*(tmp2128 + tmp2573 + tmp2574 + tmp2575 + (tmp2576 + tmp2577 + &
            &tmp620)/tmp55 + tmp74 + tmp60*(tmp2572 + tmp926)) + (tmp1346 + tmp1634 + tmp2482&
            & + tmp60*(tmp144 + tmp658) + tmp662 + (tmp2146 + tmp766 + tmp789)/tmp55 + tmp890&
            &)*tmp93 + 51*tmp101*tmp86*tmp980))/tmp55) - (256*tmp157*tmp2533*(tmp158*tmp242*t&
            &mp522*tmp690*tmp723 - tmp522*(8192*tmp272 - 64*tmp32*(65*tmp1535 + 92*tmp1560 + &
            &tmp2616 + tmp48) + ((252*tmp1535 + 144*tmp1560 + tmp2617 + 101*tmp48)*tmp684)/tm&
            &p11 + 3*tmp1535*tmp871 + m2*tmp2296*(tmp2045 + 156*tmp5*tmp6 + 152*tmp738 + 36*t&
            &mp89 + tmp93) - 512*tmp252*(tmp2615 + tmp952) + tmp528*(89*tmp36 + 12*tmp6 + tmp&
            &955)) + tmp158*tmp2614*tmp522*tmp966 + (tmp158*tmp2614*tmp705)/(tmp55*tmp99)))/(&
            &tmp10*tmp55*tmp9) + (256*tmp157*tt*((tmp241*tmp2621*tmp685*tmp695*tmp713)/tmp10 &
            &- tmp158*tmp713*(-((tmp2631*tmp695)/tmp10) + (tmp158*tmp2621*tmp705)/(tmp10*tmp5&
            &5*tmp99)) + (tmp158*tmp175*tmp242*tmp2545)/(tmp54*tmp99) + tmp158*tmp2625*tt + (&
            &tmp1550*tmp695*tt)/(tmp10*tmp99)))/(tmp10*tmp55*tmp9) + 256*tmp157*tmp1650*tt*((&
            &tmp241*tmp2621*tmp685*tmp708*tmp713)/tmp9 + tmp1584*tmp713*(tmp2631*tmp708 + (tm&
            &p158*tmp2621*tmp705)/(tmp55*tmp99)) + (tmp158*tmp164*tmp175*tmp242)/(tmp55*tmp99&
            &) + tmp158*tmp2634*tt + (tmp1550*tmp708*tt)/(tmp9*tmp99)) + (256*tmp157*tmp1584*&
            &tmp177*((tmp2533*tmp2614)/(tmp10*tmp55) + tmp249*(1024*tmp30 + tmp2618*(52*tmp36&
            & + 20*tmp5 + 23*tmp6) + tmp395*tmp871 + (96*tmp1560 + tmp2601 + tmp2602 + tmp261&
            &7)*tmp966 - 32*tmp32*(tmp1666 + 31*tt))))/(tmp54*tmp55*tmp99) + (256*tmp157*tmp1&
            &58*tmp72*(tmp242*tmp2533*tmp564*tmp690*tmp723 + tmp2533*tmp2614*tmp564*tmp966 + &
            &(tmp2533*tmp2614*tmp97*tmp985)/(tmp9*tmp99) + tmp564*(tmp2614*(tmp2199 - tmp1810&
            &/tmp99) + (tmp158*tt*(6*m2*(-4*tmp2537*tmp5 + (tmp152 + tmp2637 + tmp349)*tmp6 +&
            & (tmp2636*tmp686)/tmp11) + (tmp153 + tmp2635)*tmp714 + 3*(-6*tmp1560*tmp19 + 3*t&
            &mp1535*tmp2537 + tmp3*tmp524 + tmp48*(tmp2638 + 1/tmp55 + tmp987)) - 16*tmp4*((t&
            &mp1574 + tmp1868 + tmp198)/tmp11 + (tmp1574 + tmp217 + tmp593)*tt)))/tmp99)))/tm&
            &p55 - (256*tmp157*tt*(tmp1584*tmp249*tmp3207*tmp713*tmp966 + tmp1584*tmp2188*tmp&
            &249*tmp97 + (tmp1584*tmp175*tmp242*tmp249)/tmp99 + (tmp1550*tmp249*tt)/(tmp10*tm&
            &p99) + (tmp2621*tmp685*tmp689*tmp713*tt)/(tmp10*tmp99) - tmp158*tmp249*((tmp158*&
            &(m2*tmp111 + tmp177 + tmp3215)*tmp713*tmp97)/tmp99 - tmp2621*(tmp1584*tmp713*tt &
            &- (tmp3218 + tmp1584*tmp713 + (tmp713*tt)/tmp10)/tmp99))))/(tmp54*tmp55*tmp9) + &
            &(256*tmp157*tmp72*tt*((tmp241*tmp2621*tmp469*tmp685*tmp713)/tmp55 + (tmp1484*tmp&
            &1550*tmp469)/tmp99 + (tmp158*tmp175*tmp242*tmp243*tmp469)/tmp99 + (tmp158*tmp218&
            &8*tmp469*tt)/tmp55 + tmp158*((tmp1584*tmp2621*tmp627*tmp713*tt)/tmp99 + tmp469*(&
            &(tmp158*(tmp177 + tmp2550 + m2*tmp260)*tmp713*tmp97)/tmp99 - tmp2621*((-32*tmp32&
            &)/tmp55 + (tmp180 + tmp2640 + tmp1063*tmp36)*tt + tmp684*(tmp1063/tmp11 + tmp263&
            &9*tt) + tmp966*(tmp2640 + tmp2247*tmp41 + (tmp2599*tt)/tmp55))))))/tmp55))/16. -&
            & (scalarc0ir6t*tmp240*tmp357*tmp411*tmp53*tmp56*tmp59*tmp65*tmp67*((-512*tmp157*&
            &tmp1584*tmp177*tmp242*tmp527*(tmp249*(tmp1536 - 9*tmp1560 + tmp2172 + tmp2426 - &
            &4*(tmp1666 + tmp2176)*tmp4 - 4*tmp44 + 8*m2*(tmp1557 + tmp452 + tmp556)) + (tmp2&
            &533*tmp723)/(tmp10*tmp55)))/(tmp54*tmp55) + 256*tmp157*tmp72*(-(tmp2548*tmp527) &
            &+ (tmp158*tmp2198*tmp2634)/tmp54 + (tmp527*((tmp158*tmp2188*tmp705)/tmp55 + tmp2&
            &239*tmp708))/(tmp54*tmp9))*tt + (512*tmp157*tmp158*tmp72*(tmp2198*tmp242*tmp2533&
            &*tmp564*tmp723 + (tmp242*tmp2533*tmp527*tmp723*tmp985*tt)/tmp9 - tmp527*tmp564*(&
            &-((tmp2199 + tmp1810*tmp242)*tmp723) + tmp1074*tmp158*tmp242*tt)))/tmp55 + (256*&
            &tmp157*tt*(tmp158*tmp2198*tmp2625 - tmp527*(-((tmp2239*tmp695)/tmp10) + (tmp158*&
            &tmp2188*tmp705)/(tmp10*tmp55)) - tmp527*(-(tmp1785*tmp2214) + tmp175*tmp242*tmp3&
            &118*tmp72*tt)))/(tmp10*tmp55*tmp9) - (256*tmp157*tmp158*tmp7*(tmp2248 - 16*tmp30&
            &*(tmp2256 + tmp1535*((tmp144 + tmp2122)*tmp2484 + tmp636*(tmp3021 + 163*tmp61 - &
            &220*tmp70) + tmp2482*(-642*tmp1650 + tmp2123 + tmp2407 + 226*tmp82) + tmp60*(151&
            &*tmp641 - 1751*tmp642 - 244*tmp643 + 2725*tmp61*tmp70 - (1065*tmp83)/tmp10) + (t&
            &mp1626 + tmp2261 + 1004*tmp642 - 1409*tmp61*tmp70 + (325*tmp83)/tmp10)/(tmp10*tm&
            &p55)) - tmp1560*tmp244*(tmp128*tmp2175*(tmp146 + tmp593) + tmp60*(445*tmp1422 - &
            &526*tmp1650 + 74*tmp82 + 76*tmp83) + (tmp1764 + 32*tmp641 - 488*tmp642 + 869*tmp&
            &61*tmp70 - (401*tmp83)/tmp10)/tmp55 + tmp74*(tmp2099 + tmp2121 + tmp865)) + tmp4&
            &8*(tmp2175*(1/tmp10 + tmp273)*tmp328 + tmp636*(tmp2124 + tmp2478 - 112*tmp70) + &
            &tmp60*(61*tmp641 - 404*tmp642 - 128*tmp643 + 548*tmp61*tmp70 - (200*tmp83)/tmp10&
            &) + tmp2482*(124*tmp1422 - 182*tmp1650 + tmp2125 + tmp847) + (tmp1058*(tmp1648 +&
            & tmp2266 + tmp2104*tmp61 + tmp2710*tmp82 + tmp915))/tmp10)) + m2*tmp452*(tmp2731&
            &*(tmp2482*(tmp1162 + tmp186 + tmp2138 + tmp2655) + tmp2677 + 11*tmp2137*tmp636 +&
            & (tmp1224*(-66*tmp1422 + tmp227 + tmp2701 + tmp763))/tmp10 + tmp60*(tmp2139 + tm&
            &p2680 - 332*tmp642 + 470*tmp61*tmp70 - (160*tmp83)/tmp10)) + tmp2412*(tmp1725 + &
            &(tmp139 + tmp354 + tmp420)*tmp636 + tmp2482*(tmp2135 + tmp2348 + tmp656 + tmp668&
            &) + (tmp1224*(tmp2136 + tmp2690 + tmp1864/tmp54 + tmp83))/tmp10 + tmp60*(tmp1727&
            & + tmp1738 - 148*tmp642 + 206*tmp61*tmp70 - (68*tmp83)/tmp10)) - tmp244*tmp738*(&
            &7*tmp1650*tmp632 + (tmp134 + tmp196 + tmp229)*tmp74 + (301*tmp1422 - 143*tmp1650&
            & + tmp667 - 164*tmp82)/(tmp54*tmp55) + tmp60*(170*tmp1422 + tmp2133 + tmp2134 + &
            &tmp852)) + (tmp1777*tmp2132*tmp2638*tmp89)/tmp10 + (tmp1725 + (tmp155 + tmp2140 &
            &+ tmp420)*tmp636 + ((tmp2142 + tmp2143 + tmp2144 + tmp2171)*tmp72)/tmp55 + (tmp2&
            &141 + tmp2287 + tmp667 + tmp674)*tmp74 + tmp60*(tmp1727 - 125*tmp642 + tmp669 + &
            &tmp393*tmp70 - (42*tmp83)/tmp10))*tmp93) + tmp4*tmp686*(tmp738*(tmp1419*(tmp1206&
            & - 170*tmp1650 + tmp2162 + tmp2556) + tmp2161*tmp61*tmp632 + (tmp1991 + tmp2161 &
            &+ tmp2578)*tmp636 + tmp1772*tmp72*(tmp195 + tmp2569 + tmp2598 + 155*tmp82) + tmp&
            &60*(tmp2163 + 616*tmp642 + 19*tmp643 - 892*tmp61*tmp70 + (306*tmp83)/tmp10)) + t&
            &mp2731*(tmp2124*tmp632*tmp70 + tmp636*(tmp1020 + tmp2165 + 69*tmp70) + tmp1419*(&
            &248*tmp1422 - 277*tmp1650 + tmp2583 + tmp774) + tmp60*(tmp2166 + 827*tmp642 + 69&
            &*tmp643 - 1163*tmp61*tmp70 + (421*tmp83)/tmp10) + ((-561*tmp1422 + 97*tmp1650 + &
            &tmp1749 + 416*tmp82)*tmp844)/tmp54) + tmp5*tmp6*(tmp2164*tmp61*tmp632 - tmp74*(6&
            &67*tmp1422 - 723*tmp1650 + tmp2406 + 144*tmp82) + tmp60*(tmp2166 + 1199*tmp642 +&
            & 72*tmp643 - 1729*tmp61*tmp70 + (616*tmp83)/tmp10) + ((-985*tmp1422 + 346*tmp165&
            &0 + tmp348 + 618*tmp82)*tmp844)/tmp54 + tmp636*(tmp2164 + tmp2165 + tmp901)) + t&
            &mp1777*tmp72*tmp89*(tmp1268 + tmp1865 + tmp2159 + tmp2160 + tmp373 + tmp909) + (&
            &(tmp1734 + tmp1991 + tmp2167)*tmp636 + tmp1759*tmp632*tmp70 - (tmp1170 + tmp2170&
            & + tmp2171 + tmp2666)*tmp74 + (tmp72*(tmp1316 + tmp2168 + tmp2169 - 140*tmp82))/&
            &tmp55 + tmp60*(tmp2163 + 317*tmp642 + 26*tmp643 - 397*tmp61*tmp70 + (116*tmp83)/&
            &tmp10))*tmp93) + 8*tmp32*(tmp2282 - tmp244*tmp738*(tmp128*tmp2145*tmp2175 + (tmp&
            &149 + tmp2146 + tmp301)*tmp74 + (-104*tmp1650 + tmp2147 + tmp235 + tmp662)*tmp78&
            &1 + (tmp1212 + tmp2148 - 196*tmp642 + 362*tmp61*tmp70 - (169*tmp83)/tmp10)/tmp55&
            &) + tmp2731*(tmp2484*tmp2500 + tmp636*(tmp2152 + tmp2498 - 180*tmp70) + tmp74*(7&
            &95*tmp1422 + tmp2153 + 364*tmp82 + 262*tmp83) + tmp60*(tmp2154 + 129*tmp641 - 14&
            &34*tmp642 + 2106*tmp61*tmp70 - (807*tmp83)/tmp10) + (tmp1641 + tmp2155 + 790*tmp&
            &642 - 1014*tmp61*tmp70 + (149*tmp83)/tmp10)/(tmp10*tmp55)) + tmp2412*(tmp2149*tm&
            &p2450 + tmp636*(tmp2150 + tmp261 - 33*tmp72) + tmp74*(294*tmp1422 - 356*tmp1650 &
            &+ tmp2462 + 62*tmp83) + tmp60*(tmp1302 + tmp3061 - 557*tmp642 + 839*tmp61*tmp70 &
            &- (309*tmp83)/tmp10) + (tmp2151 + 298*tmp642 - 479*tmp61*tmp70 + (169*tmp83)/tmp&
            &10 + tmp915)/(tmp10*tmp55)) + (tmp2482*(tmp2496 + tmp2580 + tmp2659 + tmp2703) +&
            & (tmp2094 + tmp2156 + tmp2157)*tmp636 + tmp2450*(1/tmp10 + tmp770) + tmp60*(tmp2&
            &158 + tmp3024 - 371*tmp642 + 435*tmp61*tmp70 - (127*tmp83)/tmp10) + (tmp2293 + 1&
            &76*tmp642 - 81*tmp61*tmp70 - (177*tmp83)/tmp10 + tmp915)/(tmp10*tmp55))*tmp93) +&
            & 32*tmp252*(tmp2250 + tmp6*(tmp114*tmp2175*(1/tmp10 + 18/tmp54) + tmp74*(271*tmp&
            &1422 - 469*tmp1650 + 234*tmp82 + 148*tmp83) + tmp60*(tmp2155 - 556*tmp642 - 126*&
            &tmp643 + 878*tmp61*tmp70 - (359*tmp83)/tmp10) + tmp636*(tmp511 - 114*tmp70 + tmp&
            &833) + (tmp1131 + tmp2254 + 346*tmp642 - 384*tmp61*tmp70 + tmp889)/(tmp10*tmp55)&
            &) + tmp244*tmp697*((tmp2121 + tmp623 - 86*tmp70)*tmp74 + (tmp1643 + tmp1644 + tm&
            &p1757 + tmp2170)*tmp781 + (tmp1194 + tmp2288 - 508*tmp642 + 858*tmp61*tmp70 - (3&
            &91*tmp83)/tmp10)/tmp55 + tmp128*tmp2175*(tmp144 + tmp986))) + (tmp48*tmp72*(tmp2&
            &412*(tmp1857 + tmp2128 + tmp2249 + (tmp2673 + tmp273)*tmp60 + tmp639 + (tmp149 +&
            & tmp1734 - 36*tmp72)/tmp55 + tmp74) + tmp2731*tmp3*(tmp1865 + tmp2127 + tmp2129 &
            &+ tmp2130 - 29/(tmp10*tmp55) + tmp781) + tmp2132*tmp3*tmp89 + (7*tmp1422 + tmp24&
            &82 + tmp264 + (tmp134 + tmp1530 + tmp354)/tmp55 + (tmp1617 + tmp185)*tmp60 + tmp&
            &791 + tmp806)*tmp93 + tmp3*tmp738*(tmp2126 + tmp2127 + tmp2129 + tmp2159 + tmp30&
            &6 + tmp992)))/tmp55 - 128*tmp272*tmp3*(12*tmp17*tmp21*tmp3188 + (tmp128*(tmp152 &
            &+ tmp2)*tmp2175 + (tmp103 + tmp135 + tmp61)*tmp617 + tmp781*(tmp1860/tmp10 + tmp&
            &371 + tmp1721*tmp70 + tmp879) + (tmp2139 - 92*tmp642 + 144*tmp61*tmp70 - (65*tmp&
            &83)/tmp10 + tmp915)/tmp55)*tt)))/tmp99 + (512*tmp157*tmp2533*(-(tmp158*tmp2198*t&
            &mp242*tmp522*tmp723) + (tmp158*tmp242*tmp527*tmp705*tmp723)/tmp55 - tmp522*tmp52&
            &7*(tmp1665 - 4*(60*tmp1560 + tmp1676 + tmp2174 + tmp2616)*tmp4 + tmp450 + m2*(tm&
            &p2427 - 4*tmp2731 + 44*tmp5*tmp6 + 42*tmp738 + 8*tmp89) + 8*tmp32*(tmp2173 + tmp&
            &726 + tmp955) + (tmp3122 + tmp3123 + tmp93 + tmp935)*tt)))/(tmp10*tmp55*tmp9) - &
            &(256*tmp157*tt*(-(tmp1584*tmp2188*tmp242*tmp249) + (tmp158*tmp2188*tmp2526*tt)/(&
            &tmp10*tmp99) - (tmp249*(8*tmp32*(tmp36*(-81/tmp10 + tmp2240 + 118/tmp54) + (-40/&
            &tmp10 + tmp2191 + tmp2241)*tmp6 + tmp2220/tmp9) + tmp2215/tmp9 + tmp241*(tmp2196&
            &*tmp2201 + tmp2532*tmp5*tmp560 + tmp2233/tmp9 + 2*tmp2731*(tmp1892 + tmp209 + tm&
            &p931) + tmp2194*tmp934) + tmp1693*(tmp1535*tmp2194 + 2*tmp1560*(tmp1058 + tmp2 +&
            & tmp260) + tmp2195*tmp48 + tmp44*(tmp1572 + tmp580 + tmp987)) - 4*tmp4*(tmp48*(t&
            &mp2193 + tmp2243 + 64/tmp54) + tmp1535*(-63/tmp10 + tmp2242 + 124/tmp54) + tmp22&
            &27/tmp9 + tmp3211*(tmp217 + tmp2192 + tmp476)*tt) - 32*tmp30*(tmp2216/tmp9 + (tm&
            &p2189 + tmp2190 + 34/tmp55)*tt)))/tmp99))/(tmp54*tmp55*tmp9*tmp99) + (256*tmp157&
            &*tmp72*tt*((tmp158*tmp2188*tmp2198*tmp469)/tmp55 + tmp527*(tmp1584*tmp2188*tmp62&
            &7*tt - tmp469*((-896*tmp252)/tmp55 - 8*tmp32*(tmp36*(tmp2240 + tmp479) + tmp2220&
            &/tmp55 + (tmp2241 + tmp2711)*tmp6) + tmp966*(tmp2233/tmp55 + (tmp1573 + tmp189)*&
            &tmp5*tmp560 + tmp2201*(tmp1063 + tmp76) + tmp2246*tmp934 + 2*tmp2731*(tmp1892 + &
            &tmp974)) + 4*tmp4*(22*tmp2304 + tmp1535*(tmp2242 + tmp2710) + (tmp2243 + tmp2244&
            &)*tmp48 + tmp3211*(tmp189 + tmp476)*tt) + 32*tmp30*(tmp2615/tmp54 + tmp2216/tmp5&
            &5 + (34*tt)/tmp55) + tmp6*(tmp1535*tmp2246 + tmp2247*tmp48 + tmp2245*tmp524 + tm&
            &p5*(tmp580*tt + tmp824*tt))))))/tmp55))/16. + (tmp11**2*tmp13*tmp240*tmp357*tmp4&
            &11*tmp53*tmp56*tmp58*tmp67*((256*betas*tmp1053*tmp157*tmp1584*tmp2821*tmp5*((tmp&
            &2734*tmp36*tmp527*tmp695*tmp70)/tmp96 + (tmp2734*tmp527*tmp6*tmp695*tmp70)/tmp96&
            & + (m2*tmp2621*tmp369*tmp410*tmp685*tmp695*tmp713)/tmp96 + (tmp158*tmp1833*tmp52&
            &7*tmp695*tmp70*tmp97)/(tmp100*tmp96) + (tmp2621*tmp410*tmp685*tmp695*tmp70*tmp71&
            &3*tmp97)/tmp96 + tmp2768*tmp2784*tmp410*(-(tmp3150*((tmp158*tmp175*tmp241*tmp312&
            &1)/tmp10 + (tmp1776 + tmp3121*tmp3141)/(tmp10*tmp99))) + (tmp158*tmp175*tmp3121*&
            &tmp3148*tt)/tmp10) + (tmp2784*tmp410*(-(tmp3168*((tmp158*tmp175*tmp241*tmp695)/t&
            &mp10 + (tmp3129 + (tmp3141*tmp695)/tmp10)/tmp99)) + (tmp158*tmp175*tmp3163*tmp69&
            &5*tt)/tmp10))/tmp10))/tmp96 + (256*betas*tmp157*tmp158*tmp2821*tmp5*tmp6*tmp72*(&
            &(tmp1484*tmp2621*tmp410*tmp685*tmp708*tmp713)/(tmp9*tmp96) + (m2*tmp260*tmp2621*&
            &tmp410*tmp685*tmp708*tmp713)/(tmp9*tmp96) + (tmp2734*tmp527*tmp708*tmp980)/(tmp5&
            &4*tmp96) + (tmp1824*tmp1833*tmp527*tmp708*tt)/(tmp54*tmp96) + (tmp250*tmp2734*tm&
            &p527*tmp708*tt)/(tmp54*tmp96) + tmp2768*tmp2784*tmp410*(-(tmp3150*((tmp158*tmp17&
            &5*tmp241*tmp3154)/tmp54 + (tmp3155 + (tmp3141*tmp3154)/tmp54)/tmp99)) + (tmp158*&
            &tmp175*tmp3148*tmp3154*tt)/tmp54) + (tmp2784*tmp410*(-(tmp3168*((tmp158*tmp175*t&
            &mp241*tmp708)/tmp54 + (((tmp158*tmp175*tmp705)/tmp55 - tmp3141*tmp708)*tmp85)/tm&
            &p99)) + (tmp158*tmp175*tmp3163*tmp708*tt)/tmp54))/tmp9))/tmp96 - (256*tmp157*tmp&
            &7*((65536*betau*m2**10*tmp21*tmp2768*tmp3*(betas*tmp17*tmp5*tmp62 + betat*tmp6*(&
            &tmp1035 + tmp1037 + tmp830)))/tmp100 + (tmp1629*tmp2304*tmp48*tmp72*(betau*tmp3*&
            &tmp3047*tmp38*tmp86 + tmp160*tmp2768*(2*tmp2731*((tmp2775 - 9*tmp2784 + tmp2787)&
            &/tmp10 + 3*tmp3081 + tmp2772/tmp55) + tmp2412*(tmp128*(tmp2775 - 5*tmp2784 + tmp&
            &2809) + (tmp2769 + tmp2776 + tmp2781)/tmp54 + (tmp2769 + tmp2800 + tmp2819)/tmp5&
            &5) + tmp2178*((-11*tmp2784 + tmp2906 + tmp2998)/tmp10 + 2*tmp3081 + (tmp2777 + t&
            &mp2796 + betat*tmp3171)/tmp55) + tmp2772*tmp3*tmp89 + ((tmp2840*tmp3098)/tmp10 +&
            & (tmp2780 + tmp2816 + tmp2821)/tmp54 + (tmp2776 + tmp2777 + tmp2983)/tmp55)*tmp9&
            &3)))/tmp9 - 8192*m2**9*(tmp2768*(betat*tmp2731*tmp2951*(tmp3062 + 5*tmp636*(tmp1&
            &03 + tmp155 + tmp72) + (-49*tmp1650 + tmp2650 + tmp677 + tmp683)*tmp74 + (tmp72*&
            &(tmp1202 + tmp1346 + tmp2788 + tmp788))/tmp55 + tmp60*(tmp2687 + tmp3066 + tmp29&
            &53*tmp70 - (61*tmp83)/tmp10)) + tmp101*tmp1693*(tmp128*tmp2175*(tmp3053 + (betat&
            &*(betas - 8*betau))/tmp54) + ((tmp2780 + tmp2783 + tmp2785)*tmp61 + tmp2951*(bet&
            &as + tmp3040)*tmp70 + (tmp2780 + tmp2781 + tmp2782)*tmp72)*tmp74 + tmp826*(-2*tm&
            &p1422*(tmp2784 + tmp2785 + tmp2795) - 2*tmp1650*(tmp2784 + tmp2820 + tmp2850) + &
            &tmp193*tmp3005 + (tmp2782 + tmp2783 + tmp2817)*tmp83) + tmp781*(tmp1422*(-3*tmp2&
            &784 + tmp2787 + tmp2818) + tmp187*(tmp2787 + betas*tmp2952) - tmp1650*(tmp3005 +&
            & tmp3099) + (tmp2776 + tmp2783 + tmp2982)*tmp83)) + betau*tmp17*tmp21*tmp2120*tm&
            &p3084*tmp89 + betau*tmp3124*(tmp1330 + tmp1772*tmp1906 + tmp663 + tmp60*(tmp1350&
            & + tmp2704 + tmp665 - (10*tmp83)/tmp10 + tmp919) + tmp2482*(tmp1611 + tmp195 + t&
            &mp82 + tmp920))*tmp93 + tmp244*tmp2775*tmp738*(tmp2175*tmp2648 + (tmp2262 + tmp2&
            &778 + tmp2779 + 20*tmp643 + tmp675)/tmp55 + tmp74*(tmp2269 + tmp766 + tmp767) + &
            &(23*tmp1422 + tmp2650 + tmp586 + tmp83)*tmp993)) + tmp17*tmp21*tmp2850*tmp3*tmp3&
            &126*(tmp5*tmp62 + tmp3188*tmp690 + tmp6*tmp946)*tt) + 16*tmp30*(betat*tmp3*tmp32&
            &22*tmp36*tmp86*(78*tmp223*tmp418 + 61*tmp21*tmp221*tmp62 + tmp156*tmp5*(119*tmp4&
            &13 + (tmp2538*(tmp2164 + tmp3021 + 55*tmp61))/tmp54 + tmp60*(-266*tmp61 - 623*tm&
            &p70 + 1265*tmp72) + (-1650*tmp1422 + 1769*tmp1650 + 504*tmp82 - 385*tmp83)/tmp55&
            &) + tmp215*tmp518*(147*tmp413 + tmp60*(tmp1532 + tmp3022 - 209*tmp70) + (tmp2099&
            & + tmp213 + 85*tmp61)*tmp72 + (99*tmp1650 + tmp1736 + 62*tmp82 + 85*tmp83)/tmp55&
            &) - 4*tmp143*tmp6*(52*tmp413 + tmp60*(389*tmp61 + 233*tmp70 - 862*tmp72) + (tmp3&
            &019 + 285*tmp70 - 622*tmp72)*tmp72 + (1199*tmp1422 - 1147*tmp1650 - 285*tmp82 + &
            &337*tmp83)/tmp55) + tmp2730*tmp48*(915*tmp413 + tmp113*(tmp3020 + 446*tmp61 + 26&
            &3*tmp70) + tmp60*(3145*tmp61 + 400*tmp70 - 5237*tmp72) + (7467*tmp1422 - 6552*tm&
            &p1650 - 1315*tmp82 + 2230*tmp83)/tmp55) + tmp715*(345*tmp413 + tmp60*(1178*tmp61&
            & + 143*tmp70 - 1823*tmp72) + (833*tmp61 + 488*tmp70 - 1321*tmp72)*tmp72 + (2656*&
            &tmp1422 - 2311*tmp1650 - 488*tmp82 + 833*tmp83)/tmp55)*tmp93 + tmp211*(tmp3070 +&
            & tmp60*(-136*tmp61 - 649*tmp70 + 573*tmp72) + tmp72*(-307*tmp61 - 478*tmp70 + 78&
            &5*tmp72) + (-880*tmp1422 + 1051*tmp1650 + 478*tmp82 - 307*tmp83)/tmp55)*tt) + tm&
            &p2768*(24*tmp17*tmp21*tmp2120*tmp2775*tmp339 + (betat*tmp237*tmp2951*(tmp2490*tm&
            &p632 + 4*tmp636*(tmp608 + tmp637 + tmp802) + (tmp72*(-283*tmp1422 + 468*tmp1650 &
            &+ tmp674 - 195*tmp83))/tmp55 + tmp74*(-585*tmp1422 - 63*tmp1650 + 88*tmp82 + 168&
            &*tmp83) + tmp60*(tmp3060 + tmp3061 + 419*tmp642 + 151*tmp61*tmp70 - (414*tmp83)/&
            &tmp10)))/tmp11 + tmp221*tmp6*(tmp2175*(tmp3081 + (tmp3026*tmp3114)/tmp10)*tmp369&
            & + tmp636*((-137*tmp2775 + 324*tmp2784 + tmp3027)*tmp61 + (49*betas + betat)*tmp&
            &3092*tmp70 + (-403*tmp2775 + 378*tmp2784 - 541*tmp2821)*tmp790) + tmp60*(tmp2704&
            &*(-2120*tmp2775 + 852*tmp2784 - 1177*tmp2821) + (tmp2461*(-383*tmp2775 + 717*tmp&
            &2784 - 899*tmp2821))/tmp10 + tmp1212*(147*tmp2775 + tmp2780 + tmp2821) + (-333*t&
            &mp2775 + tmp3027 + tmp3028)*tmp641 + (842*tmp2775 + 1344*tmp2784 - 1703*tmp2821)&
            &*tmp70*tmp80) + (tmp17*tmp1772*(tmp1650*(296*tmp2775 - 956*tmp2784 + 1293*tmp282&
            &1) + 12*tmp1422*(67*tmp2775 + tmp2776 + tmp2823) + tmp3026*tmp3088*tmp82 + (-196&
            &*tmp2775 + 162*tmp2784 - 171*tmp2821)*tmp83))/tmp10 + tmp2482*(tmp1650*(-8*betas&
            &*(65*betat + 54*betau) + 545*tmp2821) + tmp1422*(2318*tmp2775 - 1640*tmp2784 + 2&
            &295*tmp2821) + tmp151*(betas*(betat + 147*betau) + tmp2869) + (-235*tmp2775 + 32&
            &2*tmp2784 + tmp3027)*tmp83)) + tmp215*tmp44*(tmp2175*tmp369*((tmp3051*tmp3114)/t&
            &mp10 + (betat*(-61*betau + tmp3098))/tmp54) + tmp636*((-673*tmp2775 + 971*tmp278&
            &4 + tmp3052)*tmp61 + (61*betat + tmp2859)*tmp3092*tmp70 + (-2736*tmp2775 + 3850*&
            &tmp2784 - 6417*tmp2821)*tmp72) + (tmp1650*(-410*tmp2775 - 1284*tmp2784 + 913*tmp&
            &2821) + tmp1422*(8574*tmp2775 - 10300*tmp2784 + 15173*tmp2821) + tmp195*(-691*tm&
            &p2775 + 961*tmp2784 + tmp3052) + (tmp2810 + tmp2855 + tmp2946)*tmp385)*tmp74 + t&
            &mp60*(tmp2704*(-3137*tmp2775 + 3139*tmp2784 - 4072*tmp2821) + tmp1212*(10*tmp278&
            &4 + tmp2855 + betat*tmp3032) + (-709*tmp2775 + 951*tmp2784 + tmp3052)*tmp641 + (&
            &-727*tmp2775 + 2759*tmp2784 - 4521*tmp2821)*tmp70*tmp80 + ((5100*tmp2775 - 7886*&
            &tmp2784 + 10769*tmp2821)*tmp83)/tmp10) + tmp17*(4*tmp1422*(127*tmp2775 + 53*tmp2&
            &784 - 153*tmp2821) + 2*tmp1650*(2483*tmp2775 - 3437*tmp2784 + 5478*tmp2821) + tm&
            &p3051*tmp3172*tmp82 + (-2702*tmp2775 + 2792*tmp2784 - 3439*tmp2821)*tmp83)*tmp84&
            &4) + tmp211*tmp48*(tmp2175*tmp369*((tmp3033*tmp3114)/tmp10 + (tmp2866*(tmp2922 +&
            & tmp3029))/tmp54) + tmp636*((-920*tmp2775 + 1572*tmp2784 + tmp3035)*tmp61 + (61*&
            &betas + 10*betat)*tmp3092*tmp70 + (-4924*tmp2775 + 6726*tmp2784 - 9867*tmp2821)*&
            &tmp72) + tmp74*(tmp1650*(634*tmp2775 - 4312*tmp2784 + 4077*tmp2821) + tmp1422*(1&
            &2894*tmp2775 - 15428*tmp2784 + 22261*tmp2821) + (tmp2770 + tmp3034 + tmp3036)*tm&
            &p385 + (-2084*tmp2775 + 3120*tmp2784 - 3302*tmp2821)*tmp83) + tmp60*(tmp2704*(-4&
            &859*tmp2775 + 4229*tmp2784 - 5995*tmp2821) + tmp1212*(tmp2815 + tmp3036 + tmp303&
            &7) + (-1164*tmp2775 + 1548*tmp2784 + tmp3035)*tmp641 + (-1031*tmp2775 + 5787*tmp&
            &2784 - 7416*tmp2821)*tmp70*tmp80 + ((7588*tmp2775 - 13234*tmp2784 + 16955*tmp282&
            &1)*tmp83)/tmp10) + tmp17*(2*tmp1650*(2237*tmp2775 - 4643*tmp2784 + 6681*tmp2821)&
            & + 4*tmp1422*(559*tmp2775 - 101*tmp2821 + tmp3030) + tmp3033*tmp3172*tmp82 + (-1&
            &542*tmp2775 + 2244*tmp2784 - 3011*tmp2821)*tmp83)*tmp844) + tmp223*tmp516*(tmp24&
            &38*(tmp3053 + (betat*(betas - 46*betau))/tmp54) + tmp636*((-72*tmp2775 + 74*tmp2&
            &784 + tmp3056)*tmp61 + (betas + 46*betat)*tmp3177*tmp70 + (304*tmp2784 - 393*tmp&
            &2821 + tmp3054)*tmp72) + (tmp151*(betas*(betat + 3*betau) + 92*tmp2821) + tmp142&
            &2*(924*tmp2775 - 833*tmp2784 + 487*tmp2821) + tmp195*(73*betas*tmp2838 + tmp3056&
            &) + tmp1650*(-71*tmp2784 + betau*tmp2901 + tmp3057))*tmp74 + tmp60*(tmp1653*(tmp&
            &2780 + tmp2820 + 46*tmp2821) + (-74*tmp2775 + 72*tmp2784 + tmp3056)*tmp641 + (-6&
            &99*tmp2775 + 505*tmp2784 + tmp2902)*tmp642 + (427*tmp2784 - 812*tmp2821 + tmp305&
            &9)*tmp61*tmp70 + ((-622*tmp2784 + 401*tmp2821 + tmp3058)*tmp83)/tmp10) + tmp17*(&
            &tmp1650*(591*tmp2775 - 585*tmp2784 + 686*tmp2821) + 4*tmp1422*(tmp2800 - 28*tmp2&
            &821 + betas*tmp2885) + tmp3106 + (-368*tmp2775 + 251*tmp2784 + tmp3055)*tmp83)*t&
            &mp844) + betat*tmp2951*tmp356*(tmp3062 + tmp60*(tmp3065 + tmp3066 + 20*tmp641 + &
            &100*tmp61*tmp70 + tmp2421*tmp82) + tmp636*(tmp2964 + tmp620 + tmp828) + (tmp72*(&
            &tmp2292 + tmp2650 + tmp3064 - 49*tmp83))/tmp55 + tmp74*(tmp2650 + tmp2652 + tmp3&
            &063 + tmp877)) + tmp156*(tmp2175*tmp369*((tmp3045*tmp3114)/tmp10 + (betat*(8*bet&
            &as - 55*betau))/tmp54) + tmp636*((-1456*tmp2775 + 2430*tmp2784 + tmp3046)*tmp61 &
            &+ (29*betas + 55*betat)*tmp3092*tmp70 + (-6237*tmp2775 + 9574*tmp2784 - 15181*tm&
            &p2821)*tmp72) + tmp74*(tmp1650*(726*tmp2775 - 4319*tmp2784 + 3351*tmp2821) + tmp&
            &1422*(17572*tmp2775 - 24549*tmp2784 + 37175*tmp2821) + (tmp2802 + 110*tmp2821 + &
            &tmp3048)*tmp385 + (-3028*tmp2775 + 4828*tmp2784 - 5082*tmp2821)*tmp83) + tmp60*(&
            &tmp1212*(55*tmp2821 + tmp3047 + tmp3048) + (-1572*tmp2775 + 2398*tmp2784 + tmp30&
            &46)*tmp641 + (-12431*tmp2775 + 14695*tmp2784 - 21078*tmp2821)*tmp642 + (-4771*tm&
            &p2775 + 14517*tmp2784 - 20804*tmp2821)*tmp61*tmp70 + ((11313*tmp2775 - 19542*tmp&
            &2784 + 26701*tmp2821)*tmp83)/tmp10) + tmp17*(tmp1650*(9143*tmp2775 - 15663*tmp27&
            &84 + 24706*tmp2821) + 4*tmp1422*(332*tmp2775 - 229*tmp2821 + tmp3043) + tmp3045*&
            &tmp3172*tmp82 + (-4118*tmp2775 + 5713*tmp2784 - 8169*tmp2821)*tmp83)*tmp844)*tmp&
            &89 + tmp143*(tmp2175*tmp2370*(tmp3038 + (tmp2866*tmp3093)/tmp54) + tmp636*(betau&
            &*(tmp3039 + tmp3040)*tmp606 + (-1670*tmp2775 + 2857*tmp2784 + tmp3042)*tmp61 + (&
            &-7548*tmp2775 + 11548*tmp2784 - 17467*tmp2821)*tmp72) + tmp74*(tmp1650*(1890*tmp&
            &2775 - 6463*tmp2784 + 5511*tmp2821) + tmp1422*(19886*tmp2775 - 28061*tmp2784 + 4&
            &1523*tmp2821) + (tmp2780 + tmp2870 + tmp3041)*tmp788 + (-3548*tmp2775 + 5682*tmp&
            &2784 - 6050*tmp2821)*tmp83) + tmp60*(tmp2515*(tmp2787 + tmp2817 + tmp2870) + (-1&
            &878*tmp2775 + 2825*tmp2784 + tmp3042)*tmp641 + (-13998*tmp2775 + 16177*tmp2784 -&
            & 23216*tmp2821)*tmp642 + (-6118*tmp2775 + 18747*tmp2784 - 24914*tmp2821)*tmp61*t&
            &mp70 + ((-776*betas*(30*betat - 17*betau) + 30663*tmp2821)*tmp83)/tmp10) + tmp17&
            &*(tmp1650*(9018*tmp2775 - 17313*tmp2784 + 26248*tmp2821) + 4*tmp1422*(100*tmp278&
            &4 + tmp3058 + tmp3079) + betas*tmp2868*tmp788 + (-3338*tmp2775 + 5333*tmp2784 - &
            &7685*tmp2821)*tmp83)*tmp844)*tmp93 + tmp228*tmp244*tmp2775*(tmp2960*tmp632 + (tm&
            &p3023 + 100*tmp70 + 213*tmp72)*tmp74 - tmp60*(593*tmp1422 - 572*tmp1650 + 200*tm&
            &p82 + 28*tmp83) + (tmp3024 + 444*tmp642 + 100*tmp643 - 739*tmp61*tmp70 + (159*tm&
            &p83)/tmp10)/tmp55)*tt)) + tmp410*tmp5*tmp6*tmp966*(betas*tmp2821*tmp3*tmp86*(4*t&
            &mp211*(tmp2841 + tmp413 + tmp60*(tmp103 + tmp286 + tmp61) + (tmp105 + tmp112 + t&
            &mp70)/(tmp10*tmp55)) + tmp1708*tmp44*(tmp1847 + tmp60*(tmp359 + tmp421 - 169*tmp&
            &72) + (tmp134 + tmp149 + tmp352)*tmp72 + (180*tmp1422 - 177*tmp1650 + tmp186 + t&
            &mp780)/tmp55) + 2*tmp215*(tmp412 + tmp413 + tmp3111*tmp60 + (tmp61 + tmp762 + tm&
            &p828)/(tmp54*tmp55)) - 4*tmp6*(tmp413 + (147*tmp1422 + tmp1867 + tmp2658 + tmp65&
            &2)/tmp55 + tmp60*(tmp109 + tmp218 - 139*tmp72) + (tmp112 + tmp150 + tmp232)*tmp7&
            &2)*tmp89 + tmp156*tmp518*(tmp493 + (tmp190 + tmp208 + tmp61)*tmp72 + (-110*tmp14&
            &22 + tmp1851/tmp10 + tmp385 + tmp83)/tmp55 + tmp373*(tmp1532 + tmp80 + tmp92)) +&
            & tmp5*(tmp413 + (-522*tmp1422 + 523*tmp1650 + tmp2509 + tmp2650)/tmp55 + tmp60*(&
            &tmp595 - 18*tmp61 + 503*tmp72) + tmp72*(tmp2648 + tmp802 + tmp900))*tmp93 + tmp1&
            &43*(tmp493 + tmp60*(tmp1633 + tmp595 + 275*tmp72) + (-286*tmp1422 + 291*tmp1650 &
            &+ tmp640 + tmp788)/tmp55 + tmp72*(tmp606 + tmp766 + tmp789))*tt) + (tmp2768*(tmp&
            &211*tmp3*(tmp2815 + tmp2816 + betat*tmp3173)*tmp688 + tmp215*(betas*tmp210*tmp28&
            &40 + (-6*tmp2775 + tmp2817 + tmp3055)/tmp54 + (tmp2770 + tmp2823 + tmp2825)/tmp5&
            &5)*tmp688 + tmp44*tmp48*((tmp1057*tmp17*((60*tmp2775 - 241*tmp2784 + 221*tmp2821&
            &)/tmp10 + (-21*tmp2775 - 63*tmp2821 + tmp3030)/tmp54))/tmp10 + (tmp151*(-60*tmp2&
            &775 + 241*tmp2784 - 221*tmp2821) + 2*tmp1422*(-24*tmp2775 + 125*tmp2784 - 119*tm&
            &p2821) + tmp227*tmp2830 + tmp1650*(-396*tmp2784 + 364*tmp2821 + betas*tmp2890))/&
            &tmp55 + (tmp128*(168*tmp2784 - 158*tmp2821 + tmp2828) + tmp189*tmp2830)*tmp74 + &
            &(tmp155*tmp2830 + (99*tmp2775 - 409*tmp2784 + 379*tmp2821)*tmp70 + (-15*tmp2775 &
            &+ 43*tmp2784 - 39*tmp2821)*tmp72)*tmp781) + (tmp156*(tmp60*(tmp3012 + (-154*tmp2&
            &784 + 103*tmp2821 + betas*tmp2896)*tmp70 + (28*tmp2784 + tmp2826 + tmp2833)*tmp7&
            &2) + ((-28*tmp2775 + 59*tmp2784 + tmp2884)/tmp10 + tmp3009)*tmp74 + (tmp1650*(36&
            &*tmp2775 - 67*tmp2784 + 47*tmp2821) + tmp1422*(tmp2775 - 38*tmp2821 + tmp2943) +&
            & tmp3015 + (-65*tmp2775 + 95*tmp2784 - 56*tmp2821)*tmp82)/tmp55 - ((65*tmp2775 -&
            & 95*tmp2784 + 56*tmp2821)/tmp10 + (36*tmp2784 + tmp2826 + tmp2837)/tmp54)*tmp830&
            &))/tmp11 + tmp6*(((291*tmp2784 - 260*tmp2821 + tmp3059)/tmp10 + tmp2827/tmp54)*t&
            &mp74 + tmp60*((163*tmp2775 - 661*tmp2784 + 597*tmp2821)*tmp70 + (-41*tmp2821 + (&
            &betau + tmp2860)*tmp3029)*tmp72 + tmp2827*tmp80) + (tmp1650*(-343*tmp2784 + 296*&
            &tmp2821 + betas*tmp2941) + 3*tmp1422*(-26*tmp2775 + 88*tmp2784 + tmp2977) + (-88&
            &*tmp2775 + 370*tmp2784 - 337*tmp2821)*tmp82 + tmp2827*tmp83)/tmp55 - ((88*tmp277&
            &5 - 370*tmp2784 + 337*tmp2821)/tmp10 + (79*tmp2784 - 77*tmp2821 + tmp2832)/tmp54&
            &)*tmp830)*tmp89 + tmp5*(tmp2482*(tmp2831 + tmp128*(50*tmp2784 - 48*tmp2821 + tmp&
            &2832)) + tmp60*(tmp2824*tmp354 + (141*tmp2775 - 508*tmp2784 + 476*tmp2821)*tmp70&
            & + (76*tmp2784 - 68*tmp2821 + tmp2833)*tmp72) + (tmp1752*(54*tmp2821 + tmp2835 +&
            & tmp2865) + tmp1422*(tmp2836 + tmp2864 + tmp3013) + (-89*tmp2775 + 308*tmp2784 -&
            & 284*tmp2821)*tmp82 + tmp2834*tmp83)/tmp55 - ((89*tmp2775 - 308*tmp2784 + 284*tm&
            &p2821)/tmp10 + (108*tmp2784 - 92*tmp2821 + tmp2837)/tmp54)*tmp830)*tmp93 + tmp14&
            &3*(tmp60*(tmp2834*tmp61 + (-240*tmp2784 + 215*tmp2821 + tmp2822)*tmp70 + (tmp282&
            &0 + tmp2821 + tmp3125)*tmp72) + (tmp2831 + tmp128*(tmp2818 - 51*tmp2821 + tmp285&
            &7))*tmp74 + (2*tmp1650*(16*tmp2775 - 65*tmp2784 + 57*tmp2821) + tmp1422*(118*tmp&
            &2784 - 103*tmp2821 + tmp3010) + (126*tmp2784 - 113*tmp2821 + tmp2833)*tmp82 + tm&
            &p2824*tmp83)/tmp55 - ((-126*tmp2784 + 113*tmp2821 + betas*tmp3044)/tmp10 + (tmp2&
            &769 + tmp2815 + betat*tmp3176)/tmp54)*tmp830)*tt))/(tmp54*tmp55)) + 128*tmp272*(&
            &tmp2768*(170*tmp17*tmp21*tmp2120*tmp221*tmp2775 + (betat*tmp215*tmp2951*(tmp597*&
            &tmp632*tmp70 + tmp1772*(tmp2908 + tmp2909 + tmp2910 + tmp2911)*tmp72 + tmp636*(1&
            &89*tmp61 - 121*tmp70 + tmp807/tmp54) + tmp74*(-318*tmp1422 - 590*tmp1650 + tmp29&
            &12 + 378*tmp83) + tmp60*(189*tmp641 + 60*tmp642 - 121*tmp643 + 788*tmp61*tmp70 -&
            & (772*tmp83)/tmp10)))/tmp11 + tmp143*tmp6*(tmp2438*((-25*betas*tmp2886)/tmp10 + &
            &(betat*(-41*betau + tmp2882))/tmp54) + tmp636*(tmp208*(-767*tmp2775 + 294*tmp278&
            &4 - 770*tmp2821) + (-499*tmp2775 + tmp2883 + tmp2887)*tmp61 + (250*betas + 41*be&
            &tat)*tmp3177*tmp70) + (tmp1650*(-731*tmp2775 - 1300*tmp2784 + 2034*tmp2821) + tm&
            &p1422*(7329*tmp2775 - 1928*tmp2784 + 4938*tmp2821) + tmp151*(82*tmp2821 + (betat&
            & + 30*betau)*tmp2882) + tmp195*(-749*tmp2775 + 341*tmp2784 + tmp2887))*tmp74 + t&
            &mp60*(tmp2704*(-3574*tmp2775 + 447*tmp2784 - 1220*tmp2821) + tmp1653*(50*betas*(&
            &betat + 15*betau) + tmp3004) + (-999*tmp2775 + 316*tmp2784 + tmp2887)*tmp641 + t&
            &mp196*(445*tmp2775 + 431*tmp2784 - 814*tmp2821)*tmp70 + ((3677*tmp2775 - 2448*tm&
            &p2784 + 5014*tmp2821)*tmp83)/tmp10) + tmp17*(2*tmp1650*(394*tmp2775 - 775*tmp278&
            &4 + 1666*tmp2821) + 4*tmp1422*(780*tmp2775 + tmp2884 + tmp3107) + 50*betas*tmp28&
            &86*tmp82 + (-1107*tmp2775 - 670*tmp2821 + tmp2883)*tmp83)*tmp844) + tmp156*tmp5*&
            &(tmp2438*((tmp2905*tmp3114)/tmp10 + (betat*(11*betas - 222*betau))/tmp54) + tmp6&
            &36*((-267*tmp2775 + 253*tmp2784 + tmp2902)*tmp61 + (23*betas + 222*betat)*tmp317&
            &7*tmp70 + (-651*tmp2775 + 517*tmp2784 - 1606*tmp2821)*tmp72) + (tmp1650*(283*tmp&
            &2775 - 601*tmp2784 - 722*tmp2821) + tmp1422*(2087*tmp2775 - 1445*tmp2784 + 3862*&
            &tmp2821) + tmp151*(tmp2808 + 444*tmp2821 + tmp2907) + (-145*tmp2775 + 121*tmp278&
            &4 + tmp2906)*tmp656)*tmp74 + tmp60*(tmp1351*(-204*tmp2775 + 114*tmp2784 - 335*tm&
            &p2821) + tmp1653*(22*tmp2784 + 222*tmp2821 + tmp2907) + (-313*tmp2775 + 231*tmp2&
            &784 + tmp2902)*tmp641 + (-271*tmp2775 + 597*tmp2784 - 462*tmp2821)*tmp70*tmp80 +&
            & ((1661*tmp2775 - 1523*tmp2784 + 1998*tmp2821)*tmp83)/tmp10) + tmp17*(-4*tmp1650&
            &*(262*tmp2784 - 796*tmp2821 + tmp3054) + 4*tmp1422*(106*tmp2821 + betas*tmp2937 &
            &+ tmp3085) + tmp2905*tmp3088*tmp82 + (-635*tmp2775 - 1114*tmp2821 + tmp2903)*tmp&
            &83)*tmp844) + tmp48*(tmp2175*tmp369*((tmp2891*tmp3114)/tmp10 + (tmp2888*(betas +&
            & tmp2922))/tmp54) + tmp636*(betau*tmp2648*(tmp2888 + tmp3084) + (-1275*tmp2775 +&
            & 1021*tmp2784 + tmp2894)*tmp61 + (-3605*tmp2775 + 2297*tmp2784 - 6088*tmp2821)*t&
            &mp72) + (tmp1650*(1883*tmp2775 - 3175*tmp2784 + 4212*tmp2821) + tmp1422*(10427*t&
            &mp2775 - 5503*tmp2784 + 13916*tmp2821) + (170*tmp2821 + tmp2892 + tmp2893)*tmp38&
            &5 + (-485*tmp2775 + 329*tmp2784 - 496*tmp2821)*tmp667)*tmp74 + tmp60*(tmp2704*(-&
            &4171*tmp2775 + 1491*tmp2784 - 3754*tmp2821) + tmp2346*(-612*tmp2775 + 1514*tmp27&
            &84 - 2905*tmp2821) + tmp1212*(85*tmp2821 + tmp2893 + tmp2899) + (-1635*tmp2775 +&
            & 953*tmp2784 + tmp2894)*tmp641 + ((8265*tmp2775 - 6537*tmp2784 + 13040*tmp2821)*&
            &tmp83)/tmp10) + tmp17*(tmp1650*(3782*tmp2775 - 3926*tmp2784 + 9828*tmp2821) + ((&
            &tmp2796 + tmp2850 + tmp2889)*tmp2919)/tmp54 + tmp2891*tmp3172*tmp82 + (-2057*tmp&
            &2775 + 1201*tmp2784 - 2740*tmp2821)*tmp83)*tmp844)*tmp89 + 12*tmp223*tmp2821*(tm&
            &p1185 + ((tmp2656 + tmp2913 + tmp368 + tmp640)*tmp72)/tmp55 + tmp60*(tmp1194 + t&
            &mp1321 + tmp2915 + tmp350*tmp70 - (59*tmp83)/tmp10) + tmp636*(tmp114 + tmp762 + &
            &tmp916) + tmp74*(tmp2685 + tmp2914 + tmp368 + tmp917)) + tmp44*(tmp2438*((tmp289&
            &7*tmp3114)/tmp10 + (betat*(17*betas - 266*betau))/tmp54) + tmp636*((-991*tmp2775&
            & + 915*tmp2784 + tmp2898)*tmp61 + (93*betas + 266*betat)*tmp3177*tmp70 + (-2507*&
            &tmp2775 + 1939*tmp2784 - 5404*tmp2821)*tmp72) + (tmp1650*(1621*tmp2775 - 2471*tm&
            &p2784 + 2192*tmp2821) + tmp1422*(7357*tmp2775 - 5067*tmp2784 + 13080*tmp2821) + &
            &tmp151*(532*tmp2821 + tmp2892 + tmp2900) + (-542*tmp2775 - 558*tmp2821 + tmp2903&
            &)*tmp656)*tmp74 + tmp60*(tmp2704*(-2783*tmp2775 + 1520*tmp2784 - 3874*tmp2821) +&
            & tmp1653*(266*tmp2821 + tmp2899 + tmp2900) + (-1177*tmp2775 + 881*tmp2784 + tmp2&
            &898)*tmp641 + (-1348*tmp2775 + 2327*tmp2784 - 4258*tmp2821)*tmp70*tmp80 + ((6313&
            &*tmp2775 - 5653*tmp2784 + 10860*tmp2821)*tmp83)/tmp10) + tmp17*(tmp1650*(3418*tm&
            &p2775 - 3440*tmp2784 + 9660*tmp2821) + 4*tmp1422*(272*tmp2775 + tmp2895 + tmp290&
            &2) + tmp2897*tmp3088*tmp82 + (-1813*tmp2775 + 1311*tmp2784 - 3264*tmp2821)*tmp83&
            &)*tmp844)*tmp93 + betau*tmp211*tmp244*tmp3088*(-225*tmp2175*tmp70 - 3*(tmp365 - &
            &75*tmp70 - 92*tmp72)*tmp74 + tmp60*(-895*tmp1422 + 952*tmp1650 + tmp227 - 450*tm&
            &p82) + (tmp2881 + 730*tmp642 + 225*tmp643 - 1295*tmp61*tmp70 + (226*tmp83)/tmp10&
            &)/tmp55)*tt) + betat*tmp3*tmp3126*tmp3179*tmp86*tt*(29*tmp156*tmp418 + 70*tmp143&
            &*tmp21*tmp62 + (tmp2179*(tmp2880 + (tmp1619 + tmp344 + 49*tmp61)*tmp72 + tmp60*(&
            &tmp2161 + tmp2501 + tmp72) + (tmp1736 + tmp2692 + tmp661 + 49*tmp83)/tmp55))/tmp&
            &11 + tmp6*tmp715*(83*tmp413 + tmp60*(tmp2877 + 339*tmp61 + 90*tmp70) + (256*tmp6&
            &1 + 173*tmp70 - 429*tmp72)*tmp72 + (551*tmp1422 - 468*tmp1650 - 173*tmp82 + 256*&
            &tmp83)/tmp55) + tmp1545*tmp48*(208*tmp413 + tmp60*(tmp2878 + 559*tmp61 - 410*tmp&
            &72) + (tmp349*(tmp2397 + tmp502 - 38*tmp72))/tmp54 + (761*tmp1422 + tmp2879 - 14&
            &3*tmp82 + 351*tmp83)/tmp55) + 8*(tmp19*tmp617 - tmp60*(tmp1597 + tmp620) + (tmp2&
            &094/tmp10 + tmp2875 + tmp2876 - 29*tmp83)/tmp55 + tmp72*(tmp263 + tmp2874 + tmp8&
            &63))*tmp89*tt)) + 2048*tmp258*(betat*tmp3*tmp3126*tmp3179*tmp86*(tmp2792 + 9*tmp&
            &21*tmp44*tmp62 + tmp1535*(tmp1847 + tmp2523*tmp2791 + (tmp2638*tmp629)/tmp10 + (&
            &tmp1742 + tmp264 + tmp2690 + tmp90)/tmp55) + tmp1560*(tmp2789 + (tmp1814*tmp629)&
            &/tmp54 + (tmp2790 + tmp640 + tmp649 + 22*tmp82)/tmp55 - tmp2523*(tmp776 + tmp930&
            &)))*tt + tmp2768*(68*tmp143*tmp17*tmp21*tmp2120*tmp2775 + tmp44*tmp6*(tmp2438*((&
            &tmp2799*tmp3114)/tmp10 + (betat*(-26*betau + tmp3175))/tmp54) + tmp636*((-79*tmp&
            &2775 + tmp2780 + tmp2795)*tmp61 + (tmp2859 + tmp2936)*tmp3092*tmp70 + (tmp2770 -&
            & 151*tmp2775 - 116*tmp2821)*tmp72) + (tmp1422*(539*tmp2775 + tmp2802 + tmp2806) &
            &+ tmp1634*(97*tmp2775 + tmp2801 + tmp2810) + tmp1650*(79*tmp2775 - 28*tmp2784 - &
            &24*tmp2821) + tmp151*(tmp2800 + 52*tmp2821 + tmp2981))*tmp74 - tmp60*(tmp2704*(t&
            &mp2808 + 66*tmp2821 + tmp2893) + tmp1641*(tmp2800 + tmp2855 + tmp3011) + (115*tm&
            &p2775 + tmp2801 + tmp2815)*tmp641 + (tmp2895 + tmp3017 + tmp3078)*tmp358*tmp70 +&
            & ((-423*tmp2775 + tmp2802 - 132*tmp2821)*tmp83)/tmp10) + tmp17*(tmp1650*(84*tmp2&
            &775 + 220*tmp2821 + tmp2835) + 4*tmp1422*(tmp2796 + tmp2889 + tmp3037) + tmp2799&
            &*tmp3088*tmp82 + (-121*tmp2775 + tmp2780 + tmp2803)*tmp83)*tmp844) + tmp48*tmp5*&
            &(tmp2175*tmp3177*(tmp3088/tmp10 + tmp2807/tmp54)*tmp70 + tmp636*((tmp2804 + tmp2&
            &805 + tmp3018)*tmp61 + (tmp2807 + tmp3088)*tmp3177*tmp70 + (-73*tmp2775 + tmp283&
            &6 + tmp2892)*tmp72) + tmp74*(tmp195*(-53*tmp2775 + tmp2804 + tmp2805) + tmp1422*&
            &(219*tmp2775 - 35*tmp2784 + tmp2806) + tmp1650*(131*tmp2775 - 51*tmp2784 - 192*t&
            &mp2821) + tmp2948*(tmp2807 + tmp3029)*tmp82) + tmp60*(tmp2346*(-43*tmp2775 + 21*&
            &tmp2784 + 26*tmp2821) + tmp2704*(-81*tmp2775 + tmp2810 + tmp3104) + (-55*tmp2775&
            & + tmp2804 + tmp2805)*tmp641 + (6*betas + tmp2807)*tmp3177*tmp643 + tmp2538*(tmp&
            &2808 + tmp2809 + tmp2828)*tmp83) + tmp17*(2*tmp1650*(-17*tmp2784 + tmp2822 + tmp&
            &2946) + 4*tmp1422*(tmp2780 + tmp3016 + tmp3096) + tmp3222*tmp82 + (-61*tmp2775 +&
            & tmp2796 + tmp2803)*tmp83)*tmp844) + betat*tmp156*tmp2951*(tmp1754 + tmp60*(tmp1&
            &351 + tmp1753 + tmp1756 + tmp2814 + tmp275*tmp70) + (tmp72*(tmp1202 + tmp1203 + &
            &tmp1745 + tmp90))/tmp55 + tmp74*(tmp136 + tmp683 + tmp852 + tmp90) + tmp636*(tmp&
            &196 + tmp286 + tmp92)) + (betat*tmp2951*(((tmp1202 - 46*tmp1422 + tmp2671 + tmp2&
            &679)*tmp72)/tmp55 + tmp60*(tmp2497 + tmp2812 + tmp2813 + 140*tmp61*tmp70 - (111*&
            &tmp83)/tmp10) + tmp61*tmp632*tmp835 + tmp636*(tmp208 + tmp382 + tmp835) + tmp74*&
            &(tmp2171 + tmp2811 + tmp683 + tmp880))*tmp93)/tmp11 + betau*tmp244*tmp3088*(-50*&
            &tmp2175*tmp70 + (tmp1302 + tmp2580/tmp10 + tmp2794 + 136*tmp642 - 253*tmp61*tmp7&
            &0)/tmp55 + tmp74*(tmp2793 + tmp761 + tmp802) + tmp60*(-155*tmp1422 + 176*tmp1650&
            & - 100*tmp82 + tmp879))*tmp89*tt)) - 512*tmp259*(betat*tmp3*tmp3126*tmp3179*tmp8&
            &6*(tmp1467*tmp21*tmp62 + tmp738*(tmp2842 - (tmp1017/tmp10 + tmp2566 + tmp2844)*t&
            &mp60 + tmp72*(tmp2843 - 110*tmp70 + 189*tmp72) + (tmp1742 + tmp2347 + 110*tmp82 &
            &- 79*tmp83)/tmp55) + tmp172*tmp5*(tmp2880 + (tmp2845 + tmp2953 + tmp390)*tmp60 +&
            & (100*tmp1422 - 85*tmp1650 + tmp2260 - 36*tmp82)/tmp55 + tmp208*(tmp1592 + tmp42&
            &9 + tmp860)) - 2*tmp2731*(tmp2789 + (tmp134 + tmp1759 + tmp229)*tmp60 + (tmp134 &
            &+ tmp1985 + tmp369)*tmp72 + (tmp590 + tmp2189*tmp70 + tmp787 + tmp914)/tmp55) + &
            &3*tmp418*tmp93)*tt + tmp2768*(140*tmp17*tmp21*tmp211*tmp2120*tmp2775 + betau*tmp&
            &215*tmp3031*(tmp1725 + tmp2873 + ((tmp1202 + tmp1203 + tmp2788 + tmp674)*tmp72)/&
            &tmp55 + (tmp1114 + tmp661 + tmp674 + tmp683)*tmp74 + tmp60*(tmp1727 + tmp2151 + &
            &tmp2793*tmp61 - (47*tmp83)/tmp10 + tmp886)) + tmp6*(tmp2438*((tmp2853*tmp3114)/t&
            &mp10 + (betat*(19*betas - 44*betau))/tmp54) + tmp636*(betau*(tmp2882 + tmp2901)*&
            &tmp328 + (-289*tmp2775 + tmp2849 + tmp2854)*tmp61 + (-839*tmp2775 + 174*tmp2784 &
            &- 764*tmp2821)*tmp72) + (tmp1422*(2807*tmp2775 - 344*tmp2784 + 1588*tmp2821) + t&
            &mp195*(-389*tmp2775 + 71*tmp2784 + tmp2854) + tmp1650*(-364*tmp2784 + 660*tmp282&
            &1 + tmp2855) + tmp151*(88*tmp2821 + tmp2856 + tmp2858))*tmp74 + tmp60*(tmp2704*(&
            &-1384*tmp2775 - 396*tmp2821 + tmp2857) + tmp1212*(tmp2858 + tmp2991 + tmp3003) +&
            & (-489*tmp2775 + 52*tmp2784 + tmp2854)*tmp641 + (367*tmp2775 + 363*tmp2784 - 832&
            &*tmp2821)*tmp70*tmp80 + ((1795*tmp2775 - 552*tmp2784 + 1692*tmp2821)*tmp83)/tmp1&
            &0) + tmp17*(2*tmp1650*(184*tmp2775 - 217*tmp2784 + 620*tmp2821) + 4*tmp1422*(tmp&
            &2850 + tmp2856 + tmp2924) + tmp2853*tmp3088*tmp82 + (-529*tmp2775 - 268*tmp2821 &
            &+ tmp2849)*tmp83)*tmp844)*tmp89 + tmp48*tmp524*(tmp2175*((tmp2862*tmp3114)/tmp10&
            & + (betat*(-125*betau + tmp2859))/tmp54)*tmp70 + tmp636*((-205*tmp2775 + 99*tmp2&
            &784 - 128*tmp2821)*tmp61 + (43*betas + 125*betat)*tmp2773*tmp70 + (-418*tmp2775 &
            &+ 172*tmp2784 - 709*tmp2821)*tmp72) + tmp74*(tmp1650*(406*tmp2775 - 332*tmp2784 &
            &+ 363*tmp2821) + tmp1422*(1250*tmp2775 - 408*tmp2784 + 1567*tmp2821) + (tmp2796 &
            &+ tmp2863 + tmp2997)*tmp82 + (189*tmp2784 - 256*tmp2821 + tmp3097)*tmp83) + tmp1&
            &7*(2*tmp1422*(122*tmp2775 + 23*tmp2784 + 27*tmp2821) + 2*tmp1650*(258*tmp2775 - &
            &164*tmp2784 + 625*tmp2821) + betas*tmp2862*tmp82 + (-299*tmp2775 + 101*tmp2784 -&
            & 345*tmp2821)*tmp83)*tmp844 + tmp60*(tmp2704*(-495*tmp2775 + 104*tmp2784 - 456*t&
            &mp2821) + (125*tmp2821 + tmp2863 + betas*tmp3105)*tmp665 + (289*tmp2784 - 607*tm&
            &p2821 + tmp3087)*tmp70*tmp80 + ((1209*tmp2775 - 587*tmp2784 + 1417*tmp2821)*tmp8&
            &3)/tmp10 + (-124*tmp2775 - 64*tmp2821 + tmp2933)*tmp919)) + (betat*tmp156*tmp295&
            &1*(tmp61*tmp632*tmp778 + tmp636*(tmp135 + tmp2124 + tmp778) + tmp60*(tmp2584 - 8&
            &8*tmp642 - 58*tmp643 + 350*tmp61*tmp70 - (263*tmp83)/tmp10) + ((tmp192 + tmp2350&
            & + tmp2871 - 104*tmp82)*tmp844)/tmp54 + tmp74*(-259*tmp1650 + tmp2125 + 138*tmp8&
            &3 + tmp921)))/tmp11 + tmp5*(tmp2438*(tmp3038 + ((betas - 62*betau)*tmp2866)/tmp5&
            &4) + tmp636*((-143*tmp2775 + 98*tmp2784 + tmp2869)*tmp61 + (124*betat + tmp3039)&
            &*tmp3177*tmp70 + (-279*tmp2775 + 160*tmp2784 - 698*tmp2821)*tmp72) + (tmp1422*(8&
            &31*tmp2775 - 440*tmp2784 + 1642*tmp2821) + tmp1650*(-272*tmp2784 - 254*tmp2821 +&
            & tmp2980) + tmp151*(tmp2780 + tmp2870 + tmp3108) + (-78*tmp2775 + 48*tmp2784 + t&
            &mp2821)*tmp656)*tmp74 + tmp17*((tmp2506*(-52*tmp2775 + 37*tmp2784 - 170*tmp2821)&
            &)/tmp10 + 8*tmp1422*(tmp2784 + tmp2865 + tmp3103) + tmp2868*tmp3088*tmp82 + (-23&
            &1*tmp2775 - 454*tmp2821 + tmp2864)*tmp83)*tmp844 + tmp60*(tmp1653*(tmp2817 + tmp&
            &2870 + tmp3080) + (-169*tmp2775 + 94*tmp2784 + tmp2869)*tmp641 + (-221*tmp2775 +&
            & 220*tmp2784 - 182*tmp2821)*tmp70*tmp80 + ((861*tmp2775 - 548*tmp2784 + 898*tmp2&
            &821)*tmp83)/tmp10 + (-155*tmp2775 + 70*tmp2784 - 308*tmp2821)*tmp886))*tmp93 + b&
            &etau*tmp143*tmp244*tmp3088*(-140*tmp2175*tmp70 + (tmp2847 + tmp2848 + 81*tmp641 &
            &+ 416*tmp642 - 750*tmp61*tmp70)/tmp55 + (tmp2357 + tmp347 + 140*tmp70)*tmp74 + t&
            &mp60*(-498*tmp1422 + 534*tmp1650 + tmp2846 + tmp927))*tt)) - (4*tmp4*tt*(tmp17*t&
            &mp2821*tmp3*tmp3126*(8*tmp237*tmp418 + 4*tmp21*tmp228*tmp62 + tmp223*tmp433*(tmp&
            &2880 + (49*tmp1650 + tmp186 + tmp2913 + tmp385)/tmp55 + (tmp1759 + tmp366 + tmp3&
            &94)*tmp60 + (tmp105 + tmp149 + tmp190)*tmp72) + tmp211*tmp6*(33*tmp413 + (tmp263&
            &8*(tmp1996 + tmp362 + 84*tmp70))/tmp10 + tmp60*(-186*tmp61 - 285*tmp70 + 1943*tm&
            &p72) + (-2162*tmp1422 + 2195*tmp1650 + tmp2968 - 219*tmp83)/tmp55) + tmp221*tmp6&
            &90*(tmp2963 + (tmp287 + tmp2964 + tmp2965)*tmp72 + tmp60*(tmp226 + tmp2665 + 145&
            &*tmp72) + (181*tmp1650 + tmp2966 + tmp2967 - 13*tmp83)/tmp55) + tmp215*tmp5*(tmp&
            &3067 + tmp135*(tmp2972 + tmp61 + 128*tmp70) + tmp60*(126*tmp61 - 255*tmp70 + 129&
            &7*tmp72) + tmp243*(1298*tmp1422 - 1425*tmp1650 + tmp2973 + tmp83)) + tmp156*tmp1&
            &66*(tmp489 + tmp60*(tmp2971 + 305*tmp70 - 3095*tmp72) + (319*tmp61 + 312*tmp70 -&
            & 631*tmp72)*tmp72 + (3414*tmp1422 - 3407*tmp1650 - 312*tmp82 + 319*tmp83)/tmp55)&
            & + tmp143*tmp428*(129*tmp413 + tmp60*(674*tmp61 + 287*tmp70 - 3905*tmp72) + (545&
            &*tmp61 + 416*tmp70 - 961*tmp72)*tmp72 + (4450*tmp1422 - 4321*tmp1650 - 416*tmp82&
            & + 545*tmp83)/tmp55) + tmp3122*(101*tmp413 + tmp60*(tmp2969 + tmp2970 - 2153*tmp&
            &72) + (323*tmp61 + 222*tmp70 - 545*tmp72)*tmp72 + (2476*tmp1422 - 2375*tmp1650 -&
            & 222*tmp82 + 323*tmp83)/tmp55)*tmp93) + (tmp2768*((tmp1225*tmp2775*tmp3*tmp339)/&
            &tmp10 + (betat*tmp160*tmp3177*tmp356*tmp671)/tmp10 - 2*tmp223*tmp5*(tmp60*(tmp30&
            &12 + (-78*tmp2784 + tmp2988 + tmp3014)*tmp70 + (tmp2815 + tmp3013 + tmp3014)*tmp&
            &72) - (tmp17*((20*tmp2784 + tmp3010 + tmp3011)*tmp61 + tmp2850*tmp70 + (51*tmp27&
            &75 - 49*tmp2784 + tmp3041)*tmp72))/tmp10 + ((-20*tmp2775 + 29*tmp2784 - 21*tmp28&
            &21)/tmp10 + tmp3009)*tmp74 + (tmp3015 + tmp1650*(28*tmp2775 - 37*tmp2784 + tmp30&
            &16) + tmp1422*(tmp2820 + tmp2892 + tmp3017) + (tmp2787 + betas*tmp2807 + tmp3018&
            &)*tmp82)/tmp55) + tmp221*tmp6*(tmp60*((-148*tmp2784 + 118*tmp2821 + tmp2981)*tmp&
            &61 + (-665*tmp2775 + 1874*tmp2784 - 1895*tmp2821)*tmp70 + (-41*tmp2775 + 146*tmp&
            &2784 - 85*tmp2821)*tmp72) + (tmp17*((tmp2784 + tmp2982 + tmp2983)*tmp369 + (-35*&
            &tmp2775 + 154*tmp2784 - 155*tmp2821)*tmp61 + (302*tmp2775 - 1020*tmp2784 + 1031*&
            &tmp2821)*tmp72))/tmp10 + ((-862*tmp2784 + 872*tmp2821 + tmp2980)/tmp10 + tmp2986&
            &/tmp54)*tmp74 + (tmp1650*(-375*tmp2775 + 1162*tmp2784 - 1112*tmp2821) + tmp1422*&
            &(276*tmp2775 - 1016*tmp2784 + 965*tmp2821) + (398*tmp2775 - 1008*tmp2784 + 1019*&
            &tmp2821)*tmp82 + tmp2986*tmp83)/tmp55) + tmp211*tmp48*(tmp60*((142*tmp2775 - 394&
            &*tmp2784 + 316*tmp2821)*tmp61 + (-1576*tmp2775 + 4673*tmp2784 - 4832*tmp2821)*tm&
            &p70 + (-70*tmp2775 + 155*tmp2784 + 28*tmp2821)*tmp72) + (tmp17*((tmp2781 + tmp27&
            &84 + betas*tmp3100)*tmp352 + (-174*tmp2775 + 641*tmp2784 - 676*tmp2821)*tmp61 + &
            &(815*tmp2775 - 2669*tmp2784 + 2778*tmp2821)*tmp72))/tmp10 + ((681*tmp2775 - 2020&
            &*tmp2784 + 2086*tmp2821)/tmp10 + tmp2990/tmp54)*tmp74 + (tmp1650*(-925*tmp2775 +&
            & 2816*tmp2784 - 2734*tmp2821) + tmp1422*(671*tmp2775 - 2191*tmp2784 + 2090*tmp28&
            &21) + (187*tmp2775 - 529*tmp2784 + 546*tmp2821)*tmp668 + tmp2990*tmp83)/tmp55) +&
            & tmp215*tmp44*((tmp17*((tmp2850 + tmp3005)*tmp369 + (-265*tmp2775 + 288*tmp2784 &
            &- 161*tmp2821)*tmp61 + (477*tmp2775 - 741*tmp2784 + 676*tmp2821)*tmp72))/tmp10 +&
            & tmp60*((52*tmp2775 - 76*tmp2784 + 44*tmp2821)*tmp61 + (-701*tmp2775 + 1182*tmp2&
            &784 - 1095*tmp2821)*tmp70 + (165*tmp2775 - 132*tmp2784 + tmp3004)*tmp72) + ((216&
            &*tmp2775 - 449*tmp2784 + 483*tmp2821)/tmp10 + tmp3008/tmp54)*tmp74 + (tmp1650*(-&
            &316*tmp2775 + 605*tmp2784 - 603*tmp2821) + tmp1422*(-325*tmp2784 + 506*tmp2821 +&
            & betas*tmp2861) + (489*tmp2775 - 729*tmp2784 + 580*tmp2821)*tmp82 + tmp3008*tmp8&
            &3)/tmp55) + (betat*tmp237*tmp2951*(tmp151 - 12*tmp1650 + tmp192 + tmp60*(tmp1582&
            & + tmp658) - 4*tmp74 + (tmp286 + tmp80 + tmp81)/tmp55 + tmp874))/(tmp10*tmp11) +&
            & tmp156*((tmp17*((tmp2771 + tmp2784 + tmp2998)*tmp352 + (-412*tmp2775 + 807*tmp2&
            &784 - 746*tmp2821)*tmp61 + (909*tmp2775 - 2233*tmp2784 + 2366*tmp2821)*tmp72))/t&
            &mp10 + tmp60*(tmp207*(67*tmp2784 - 57*tmp2821 + tmp2996) + (-3866*tmp2821 + 727*&
            &betas*(tmp3049 + tmp3177))*tmp70 + (228*tmp2775 - 263*tmp2784 + tmp2997)*tmp72) &
            &+ ((513*tmp2775 - 1418*tmp2784 + 1580*tmp2821)/tmp10 + tmp3002/tmp54)*tmp74 + (t&
            &mp1650*(-697*tmp2775 + 1962*tmp2784 - 2076*tmp2821) + tmp1422*(253*tmp2775 - 117&
            &1*tmp2784 + 1410*tmp2821) + (957*tmp2775 - 2209*tmp2784 + 2246*tmp2821)*tmp82 + &
            &tmp3002*tmp83)/tmp55)*tmp89 + tmp143*(tmp60*((-462*tmp2784 + 394*tmp2821 + tmp29&
            &91)*tmp61 + (-1948*tmp2775 + 5679*tmp2784 - 6016*tmp2821)*tmp70 + (108*tmp2775 -&
            & 121*tmp2784 + 254*tmp2821)*tmp72) + (tmp17*((tmp2776 + tmp2850 + tmp2982)*tmp36&
            &9 + (-376*tmp2775 + 1057*tmp2784 - 1074*tmp2821)*tmp61 + (1114*tmp2775 - 3386*tm&
            &p2784 + 3593*tmp2821)*tmp72))/tmp10 + ((770*tmp2775 - 2317*tmp2784 + 2487*tmp282&
            &1)/tmp10 + tmp2995/tmp54)*tmp74 + (tmp1650*(-1038*tmp2775 + 3253*tmp2784 - 3307*&
            &tmp2821) + tmp1422*(598*tmp2775 - 2220*tmp2784 + 2297*tmp2821) + (1210*tmp2775 -&
            & 3350*tmp2784 + 3497*tmp2821)*tmp82 + tmp2995*tmp83)/tmp55)*tmp93 + tmp228*(tmp1&
            &28*tmp17*((tmp2777 + tmp2817 + tmp2823)*tmp61 + betas*tmp3179*tmp70 + (21*tmp277&
            &5 + tmp2978 + tmp2979)*tmp72) + (tmp128*(-66*tmp2784 + 64*tmp2821 + tmp2855) + t&
            &mp2976/tmp54)*tmp74 + (tmp151*(45*tmp2775 + tmp2978 + tmp2979) + 2*tmp1650*(82*t&
            &mp2784 - 76*tmp2821 + tmp2996) - 2*tmp1422*(78*tmp2784 + tmp2977 + betas*tmp3171&
            &) + tmp2976*tmp83)/tmp55 + ((tmp2802 + tmp2816 + tmp2819)*tmp61 + (64*tmp2775 - &
            &136*tmp2784 + 131*tmp2821)*tmp70 + (tmp2906 - 4*betas*(betau + tmp2947))*tmp72)*&
            &tmp993)*tt))/(tmp54*tmp55)))/tmp100 - 32*tmp252*(betat*tmp3*tmp3126*tmp3179*(72*&
            &tmp215*tmp418 + 85*tmp21*tmp211*tmp62 + tmp2427*tmp5*(29*tmp413 + tmp60*(tmp2918&
            & + tmp502 - 84*tmp72) + (tmp2124 + tmp2919 - 109*tmp72)*tmp72 + (153*tmp1422 - 1&
            &24*tmp1650 + tmp2582 - 40*tmp82)/tmp55) + tmp156*tmp518*(70*tmp413 + tmp60*(tmp2&
            &04 + 91*tmp61 - 119*tmp70) + tmp286*(tmp155 + tmp229 + tmp790) + (tmp2920 + tmp3&
            &48 + tmp763 + 49*tmp82)/tmp55) + tmp143*tmp690*(68*tmp413 - 3*tmp60*(tmp307 + tm&
            &p365 + 105*tmp70) + tmp72*(-179*tmp61 - 247*tmp70 + 426*tmp72) + (-317*tmp1422 +&
            & 385*tmp1650 + tmp2916 - 179*tmp83)/tmp55) + tmp3122*tmp6*(151*tmp413 + tmp60*(t&
            &mp2917 + 272*tmp70 - 939*tmp72) + (574*tmp61 + 423*tmp70 - 997*tmp72)*tmp72 + (1&
            &513*tmp1422 - 1362*tmp1650 - 423*tmp82 + 574*tmp83)/tmp55) + tmp428*tmp44*(655*t&
            &mp413 + tmp60*(tmp420 + 1960*tmp61 - 2019*tmp72) + tmp113*(261*tmp61 + 130*tmp70&
            & - 391*tmp72) + (3324*tmp1422 - 2669*tmp1650 - 650*tmp82 + 1305*tmp83)/tmp55))*t&
            &mp86*tt + tmp2768*(122*tmp17*tmp21*tmp2120*tmp228*tmp2775 + (betat*tmp223*tmp317&
            &9*(-37*tmp61*tmp632*tmp70 + tmp1772*(tmp2652 + tmp2954 + tmp2955 + tmp2164/tmp54&
            &)*tmp72 + tmp636*(tmp2953 - 37*tmp70 + 62*tmp72) + tmp2482*(-120*tmp1422 + tmp29&
            &56 + 37*tmp82 + 66*tmp83) + tmp60*(tmp2957 + 140*tmp642 - 37*tmp643 + 212*tmp61*&
            &tmp70 - (290*tmp83)/tmp10)))/tmp11 + betau*tmp237*tmp3031*(tmp287*tmp632*tmp70 +&
            & (-111*tmp1422 - 67*tmp1650 + tmp2353 + tmp2958)*tmp74 + (tmp72*(tmp2958 + tmp29&
            &59 + tmp2960 + tmp771))/tmp55 + tmp636*(tmp1532 + tmp1759 + tmp81) + tmp60*(tmp2&
            &262 + tmp2294 + tmp2961 + tmp2962 - (141*tmp83)/tmp10)) + tmp143*tmp49*(tmp2175*&
            &((tmp2932*tmp3114)/tmp10 + (betat*(45*betas - 121*betau))/tmp54)*tmp70 + tmp636*&
            &(tmp208*(-1321*tmp2775 + 1245*tmp2784 - 2368*tmp2821) + (-1018*tmp2775 + 1238*tm&
            &p2784 + tmp2934)*tmp61 + (311*betas + 121*betat)*tmp2773*tmp70) + tmp60*(tmp2704&
            &*(-4309*tmp2775 + 2411*tmp2784 - 4345*tmp2821) + (tmp195*(3673*tmp2775 - 4439*tm&
            &p2784 + 6982*tmp2821))/tmp10 + (-1329*tmp2775 + 1193*tmp2784 + tmp2934)*tmp641 +&
            & (121*tmp2821 + tmp2849 + tmp2935)*tmp665 + tmp196*(-293*tmp2775 + 1321*tmp2784 &
            &- 2063*tmp2821)*tmp70) + tmp74*(tmp1650*(1039*tmp2775 - 3629*tmp2784 + 4156*tmp2&
            &821) + tmp1422*(10959*tmp2775 - 8793*tmp2784 + 16232*tmp2821) + (242*tmp2821 + t&
            &mp2933 + tmp2935)*tmp82 + (-2347*tmp2775 + 2431*tmp2784 - 3090*tmp2821)*tmp83) +&
            & tmp17*(tmp195*(-861*tmp2775 + 802*tmp2784 - 1352*tmp2821) + 2*tmp1422*(1122*tmp&
            &2775 + 163*tmp2784 - 219*tmp2821) + 2*tmp1650*(1876*tmp2775 - 2855*tmp2784 + 524&
            &4*tmp2821) + betas*tmp2932*tmp82)*tmp844) + tmp156*tmp44*(tmp2438*((tmp2942*tmp3&
            &114)/tmp10 + (betat*(31*betas - 339*betau))/tmp54) + tmp636*((-1471*tmp2775 + 18&
            &21*tmp2784 + tmp2944)*tmp61 + (91*betas + 339*betat)*tmp3177*tmp70 + (-4633*tmp2&
            &775 + 5045*tmp2784 - 10676*tmp2821)*tmp72) + (tmp1650*(1123*tmp2775 - 3919*tmp27&
            &84 + 2568*tmp2821) + tmp1422*(14027*tmp2775 - 13455*tmp2784 + 26184*tmp2821) + t&
            &mp151*(678*tmp2821 + tmp2943 + tmp2945) + (-781*tmp2775 + 895*tmp2784 - 925*tmp2&
            &821)*tmp656)*tmp74 + tmp60*(tmp2704*(-5251*tmp2775 + 4093*tmp2784 - 7544*tmp2821&
            &) + tmp2346*(-885*tmp2775 + 2384*tmp2784 - 3860*tmp2821) + tmp1653*(62*tmp2784 +&
            & 339*tmp2821 + tmp2945) + (-1653*tmp2775 + 1759*tmp2784 + tmp2944)*tmp641 + ((99&
            &55*tmp2775 - 12491*tmp2784 + 19852*tmp2821)*tmp83)/tmp10) + tmp17*(4*tmp1422*(36&
            &8*tmp2775 + 87*tmp2784 - 105*tmp2821) + 2*tmp1650*(3589*tmp2775 - 4553*tmp2784 +&
            & 9530*tmp2821) + tmp2942*tmp3088*tmp82 + (-3835*tmp2775 + 3651*tmp2784 - 6608*tm&
            &p2821)*tmp83)*tmp844) + tmp211*tmp452*(tmp2175*((tmp2927*tmp3114)/tmp10 + (tmp31&
            &24*(tmp2922 + tmp3172))/tmp54)*tmp70 + tmp636*((tmp2923 + tmp2928 + tmp3082)*tmp&
            &61 + tmp2922*(63*betas + tmp3124)*tmp70 + (-1778*tmp2775 + 1086*tmp2784 - 2019*t&
            &mp2821)*tmp72) + tmp74*(tmp1650*(-898*tmp2775 - 1124*tmp2784 + 1431*tmp2821) + t&
            &mp1422*(5398*tmp2775 - 2392*tmp2784 + 4323*tmp2821) + (40*tmp2821 + tmp2929 + tm&
            &p3047)*tmp82 + (-787*tmp2775 + 656*tmp2784 - 858*tmp2821)*tmp83) + tmp60*(tmp270&
            &4*(-2575*tmp2775 + 603*tmp2784 - 1090*tmp2821) + (-551*tmp2775 + tmp2928 + tmp30&
            &28)*tmp641 + (tmp2929 + tmp3034 + tmp3110)*tmp665 + (1090*tmp2775 + 1221*tmp2784&
            & - 1869*tmp2821)*tmp70*tmp80 + ((2137*tmp2775 - 2514*tmp2784 + 3899*tmp2821)*tmp&
            &83)/tmp10) + tmp17*(2*tmp1650*(280*tmp2775 - 785*tmp2784 + 1316*tmp2821) + 4*tmp&
            &1422*(540*tmp2775 - 31*tmp2821 + tmp2924) + betas*tmp2927*tmp82 + (-627*tmp2775 &
            &- 449*tmp2821 + tmp2923)*tmp83)*tmp844) + tmp89*(tmp2438*((betat*(39*betas - 274&
            &*betau))/tmp54 + betas*tmp2938*tmp63) + tmp636*((-2580*tmp2775 + 3283*tmp2784 + &
            &tmp2939)*tmp61 + (108*betas + 137*betat)*tmp3092*tmp70 + (-8636*tmp2775 + 9379*t&
            &mp2784 - 18546*tmp2821)*tmp72) + tmp74*(tmp1650*(3446*tmp2775 - 8293*tmp2784 + 8&
            &214*tmp2821) + tmp1422*(24146*tmp2775 - 23597*tmp2784 + 44694*tmp2821) + tmp151*&
            &(648*tmp2775 + 548*tmp2821 + tmp2895) + (-5592*tmp2775 + 6488*tmp2784 - 7908*tmp&
            &2821)*tmp83) + tmp60*(tmp2704*(-8907*tmp2775 + 6905*tmp2784 - 12558*tmp2821) + t&
            &mp1212*(324*tmp2775 + 137*tmp2821 + tmp2895) + (-3012*tmp2775 + 3205*tmp2784 + t&
            &mp2939)*tmp641 + (-3737*tmp2775 + 9322*tmp2784 - 14960*tmp2821)*tmp70*tmp80 + ((&
            &17948*tmp2775 - 22841*tmp2784 + 36490*tmp2821)*tmp83)/tmp10) + tmp17*tmp844*(12*&
            &tmp1422*(264*tmp2775 + 47*tmp2784 - 86*tmp2821) + 2*tmp1650*(5451*tmp2775 - 7673&
            &*tmp2784 + 15202*tmp2821) + (-5002*tmp2775 + 5325*tmp2784 - 9730*tmp2821)*tmp83 &
            &+ betas*tmp2938*tmp90))*tmp93 + tmp215*tmp516*(tmp636*((-171*tmp2775 + 177*tmp27&
            &84 + tmp2946)*tmp61 + (16*betas + 275*betat)*tmp2773*tmp70 + (-500*tmp2775 + 488&
            &*tmp2784 - 1013*tmp2821)*tmp72) + tmp74*(-(tmp1650*(24*tmp2775 + 334*tmp2784 + 4&
            &89*tmp2821)) + tmp1422*(1708*tmp2775 - 1350*tmp2784 + 2027*tmp2821) + tmp151*(tm&
            &p2950 + 6*betas*tmp3094) + (-358*tmp2775 + 342*tmp2784 + 244*tmp2821)*tmp83) + t&
            &mp60*(tmp2704*(-666*tmp2775 + 409*tmp2784 - 540*tmp2821) + (-187*tmp2775 + 165*t&
            &mp2784 + tmp2946)*tmp641 + (tmp2950 + 24*betas*tmp2952)*tmp665 + (467*tmp2784 + &
            &tmp3057 + tmp3083)*tmp70*tmp80 + ((1108*tmp2775 - 1228*tmp2784 + 1009*tmp2821)*t&
            &mp83)/tmp10) + tmp17*(2*tmp1650*(480*tmp2775 - 499*tmp2784 + 991*tmp2821) + 2*tm&
            &p1422*(78*tmp2775 + 33*tmp2821 + tmp2899) + tmp192*(-120*tmp2775 - 97*tmp2821 + &
            &tmp3043) + tmp2949*tmp3172*tmp82)*tmp844 + tmp2175*tmp70*((betat*(12*betas - 275&
            &*betau))/tmp54 + betas*tmp2949*tmp987)) + tmp221*tmp244*tmp2775*(-412*tmp2175*tm&
            &p70 + (tmp2921 + 412*tmp70 + 633*tmp72)*tmp74 + (177*tmp641 + 1504*tmp642 + 412*&
            &tmp643 - 2610*tmp61*tmp70 + (517*tmp83)/tmp10)/tmp55 + (951*tmp1422 - 987*tmp165&
            &0 + 412*tmp82 + 29*tmp83)*tmp993)*tt)) - 8*tmp32*(betas*tmp2821*tmp3226*tmp86*tt&
            &*(40*tmp237*tmp418 + 24*tmp21*tmp228*tmp62 + tmp211*tmp6*(tmp3069 + tmp72*(-837*&
            &tmp61 - 850*tmp70 + 1687*tmp72) + tmp60*(-824*tmp61 - 863*tmp70 + 3703*tmp72) + &
            &(-4540*tmp1422 + 4553*tmp1650 + 850*tmp82 - 837*tmp83)/tmp55) + tmp215*tmp516*(t&
            &mp3070 + (tmp1988 + tmp3071 - 202*tmp70)*tmp72 + tmp60*(140*tmp61 - 373*tmp70 + &
            &941*tmp72) + (-972*tmp1422 + 1143*tmp1650 + tmp3072 - 31*tmp83)/tmp55) + tmp156*&
            &tmp715*(75*tmp413 + tmp60*(619*tmp61 + 394*tmp70 - 2517*tmp72) + (544*tmp61 + 46&
            &9*tmp70 - 1013*tmp72)*tmp72 + (3061*tmp1422 - 2986*tmp1650 - 469*tmp82 + 544*tmp&
            &83)/tmp55) + tmp143*tmp428*(561*tmp413 + tmp60*(2462*tmp61 + 779*tmp70 - 7489*tm&
            &p72) + (1901*tmp61 + 1340*tmp70 - 3241*tmp72)*tmp72 + (9390*tmp1422 - 8829*tmp16&
            &50 - 1340*tmp82 + 1901*tmp83)/tmp55) + tmp223*tmp518*(113*tmp413 + (149*tmp1650 &
            &+ tmp2967 + tmp3074 + tmp370)/tmp55 + tmp60*(tmp3073 + 190*tmp61 + 113*tmp72) + &
            &tmp72*(tmp1816 + tmp2965 + tmp833)) + tmp2730*(777*tmp413 + tmp60*(2896*tmp61 + &
            &565*tmp70 - 7829*tmp72) + (2119*tmp61 + 1342*tmp70 - 3461*tmp72)*tmp72 + (9948*t&
            &mp1422 - 9171*tmp1650 - 1342*tmp82 + 2119*tmp83)/tmp55)*tmp93 + tmp221*(tmp3067 &
            &- 3*tmp60*(tmp3068 + tmp354 - 187*tmp72) + tmp72*(-139*tmp61 - 266*tmp70 + 405*t&
            &mp72) + (-700*tmp1422 + 827*tmp1650 + 266*tmp82 - 139*tmp83)/tmp55)*tt) + tmp276&
            &8*((betau*tmp17*tmp21*tmp2120*tmp3088)/tmp11**10 + tmp221*tmp48*(tmp2438*(tmp308&
            &1 + (tmp3086*tmp3114)/tmp10) + tmp636*((524*tmp2784 + tmp3082 + tmp3083)*tmp61 +&
            & (betat + tmp3084)*tmp3177*tmp70 + (-1769*tmp2775 + 3472*tmp2784 - 4134*tmp2821)&
            &*tmp72) + (tmp1650*(152*tmp2775 - 1306*tmp2784 + 1007*tmp2821) + tmp1422*(4330*t&
            &mp2775 - 7734*tmp2784 + 9145*tmp2821) + tmp151*(tmp2784 + tmp2869 + tmp2981) + t&
            &mp195*(523*tmp2784 + tmp3083 + tmp3087))*tmp74 + tmp60*(tmp1653*(tmp2780 + tmp28&
            &21 + tmp2981) + (-272*tmp2775 + 522*tmp2784 + tmp3083)*tmp641 + (-3025*tmp2775 +&
            & 4206*tmp2784 - 4935*tmp2821)*tmp642 + (-993*tmp2775 + 4894*tmp2784 - 5297*tmp28&
            &21)*tmp61*tmp70 + ((2357*tmp2775 - 5622*tmp2784 + 6098*tmp2821)*tmp83)/tmp10) + &
            &tmp17*(tmp1650*(1633*tmp2775 - 4382*tmp2784 + 5171*tmp2821) + 4*tmp1422*(134*tmp&
            &2775 - 19*tmp2821 + tmp3085) + tmp3086*tmp3088*tmp82 + (-364*tmp2775 + 848*tmp27&
            &84 - 957*tmp2821)*tmp83)*tmp844) + tmp228*tmp6*(tmp1766*tmp2175*tmp2775 + tmp636&
            &*((-27*tmp2775 + tmp3076 + tmp3077)*tmp61 - 24*tmp2775*tmp70 + (-105*tmp2775 + 1&
            &52*tmp2784 - 176*tmp2821)*tmp790) + tmp74*(tmp1650*(-179*tmp2775 - 248*tmp2784 +&
            & 218*tmp2821) + tmp195*(tmp2828 + tmp3076 + tmp3077) - 7*tmp1422*(tmp3079 + 23*b&
            &etas*(tmp3040 + tmp3171)) + betas*tmp2937*tmp82) + tmp60*((tmp3018 + tmp3076 + t&
            &mp3077)*tmp641 + (-955*tmp2775 + 672*tmp2784 - 758*tmp2821)*tmp642 - 72*tmp2775*&
            &tmp643 + (255*tmp2775 + 872*tmp2784 - 938*tmp2821)*tmp61*tmp70 + tmp1580*(44*tmp&
            &2775 - 117*tmp2784 + tmp3080)*tmp83) + tmp17*(tmp1737*tmp2775 + tmp1650*(211*tmp&
            &2775 - 696*tmp2784 + 782*tmp2821) + 8*tmp1422*(tmp2784 + tmp2983 + tmp3078) + (-&
            &63*tmp2775 + tmp3076 + tmp3077)*tmp83)*tmp844) + tmp215*(tmp2438*((tmp3101*tmp31&
            &14)/tmp10 + ((betas - 9*betau)*tmp2866)/tmp54) + tmp636*((923*tmp2784 + tmp3097 &
            &+ tmp3102)*tmp61 + (tmp3098 + tmp3105)*tmp3177*tmp70 + (-2744*tmp2775 + 5579*tmp&
            &2784 - 7359*tmp2821)*tmp72) + (tmp1650*(-593*tmp2775 - 447*tmp2784 + 236*tmp2821&
            &) + 3*tmp1422*(2631*tmp2775 - 4801*tmp2784 + 5970*tmp2821) + tmp195*(-458*tmp277&
            &5 + 921*tmp2784 + tmp3102) + tmp151*(tmp2780 + tmp2865 + tmp3103))*tmp74 + tmp60&
            &*(tmp1653*(tmp2817 + tmp2865 + tmp2902) + (-463*tmp2775 + 919*tmp2784 + tmp3102)&
            &*tmp641 + (-5401*tmp2775 + 8720*tmp2784 - 10135*tmp2821)*tmp642 + (-1647*tmp2775&
            & + 6242*tmp2784 - 8499*tmp2821)*tmp61*tmp70 + ((4334*tmp2775 - 9371*tmp2784 + 11&
            &275*tmp2821)*tmp83)/tmp10) + tmp17*(tmp1650*(4645*tmp2775 - 9048*tmp2784 + 11527&
            &*tmp2821) + (tmp2370*(17*tmp2775 + tmp2800 + tmp3099))/tmp54 + tmp3088*tmp3101*t&
            &mp82 + (-2163*tmp2775 + 3353*tmp2784 - 3680*tmp2821)*tmp83)*tmp844)*tmp89 + tmp1&
            &43*tmp156*(tmp109*tmp2175*((tmp3094*tmp3114)/tmp10 + (betat*tmp3093)/tmp54) + tm&
            &p636*((-671*tmp2775 + 1499*tmp2784 + tmp3095)*tmp61 - 24*(betas + betat)*betau*t&
            &mp70 + (-4145*tmp2775 + 8945*tmp2784 - 11313*tmp2821)*tmp72) + tmp60*(tmp1764*(t&
            &mp2784 + tmp2869 + tmp3096) + (-695*tmp2775 + 1493*tmp2784 + tmp3095)*tmp641 + (&
            &-7156*tmp2775 + 13039*tmp2784 - 15671*tmp2821)*tmp642 + (-3354*tmp2775 + 10987*t&
            &mp2784 - 13047*tmp2821)*tmp61*tmp70 + ((6437*tmp2775 - 15063*tmp2784 + 17405*tmp&
            &2821)*tmp83)/tmp10) + tmp74*(tmp1650*(137*tmp2775 - 1774*tmp2784 + 902*tmp2821) &
            &+ tmp1422*(10837*tmp2775 - 22112*tmp2784 + 27376*tmp2821) + tmp195*(-683*tmp2775&
            & + 1496*tmp2784 + tmp3095) + (12*tmp2775 + tmp2784 + tmp2787)*tmp90) + tmp17*tmp&
            &844*(4*tmp1422*(128*tmp2775 + 35*tmp2784 - 98*tmp2821) + tmp1650*(5764*tmp2775 -&
            & 13447*tmp2784 + 16943*tmp2821) + (-2107*tmp2775 + 4356*tmp2784 - 5190*tmp2821)*&
            &tmp83 + betas*tmp3094*tmp90)) + tmp237*tmp5*(tmp2821*tmp632*tmp70*tmp810 + tmp60&
            &*(tmp1968*tmp2821 + tmp2704*(-241*tmp2775 + 207*tmp2784 + 116*tmp2821) + tmp1321&
            &*tmp3109 + (115*tmp2784 - 357*tmp2821 + betas*tmp3176)*tmp70*tmp80 + ((320*tmp27&
            &75 - 398*tmp2784 + 281*tmp2821)*tmp83)/tmp10) + (tmp17*((-16*tmp2821 + tmp3005)*&
            &tmp352 + (-282*tmp2775 + 184*tmp2784 + 129*tmp2821)*tmp61 + (458*tmp2775 - 438*t&
            &mp2784 + tmp3108)*tmp72)*tmp844)/tmp54 + tmp636*(-24*tmp2821*tmp70 + (-184*tmp27&
            &75 + 246*tmp2784 - 201*tmp2821)*tmp72 + tmp3109*tmp916) + tmp74*(tmp2504*tmp2821&
            & + tmp1422*(658*tmp2775 - 668*tmp2784 + 97*tmp2821) + tmp1650*(-146*tmp2775 + 20&
            &9*tmp2821 + tmp3110) + tmp3109*tmp917)) + (tmp3037*tmp356*(tmp663 + (tmp151 + tm&
            &p1635 + tmp2914 + tmp656)*tmp74 + tmp636*(tmp767 + tmp77 + tmp80) + ((tmp151 + t&
            &mp1726 + tmp186 + tmp277)*tmp844)/tmp54 + tmp60*(tmp1335 + tmp1349 + tmp2958/tmp&
            &54 + tmp665 + tmp919)))/tmp11 + tmp223*tmp44*(tmp2438*(tmp3053 + (betat*(betas -&
            & 18*betau))/tmp54) + tmp636*((betas + tmp3105)*tmp3177*tmp70 + (-1047*tmp2775 + &
            &1818*tmp2784 - 2411*tmp2821)*tmp72 + (-94*tmp2775 + 143*tmp2784 + tmp3104)*tmp80&
            &) + tmp74*(tmp1422*(3374*tmp2775 - 4843*tmp2784 + 5425*tmp2821) + tmp151*(tmp278&
            &4 + tmp2820 + tmp3103) + tmp1650*(-528*tmp2775 + tmp2994 + tmp3107) + (-378*tmp2&
            &775 + 570*tmp2784 - 400*tmp2821)*tmp83) + tmp17*(4*tmp1422*(20*tmp2775 + tmp2804&
            & - 74*tmp2821) + tmp2350*(725*tmp2775 - 1043*tmp2784 + 1250*tmp2821) + tmp3106 +&
            & (-1206*tmp2775 + 1257*tmp2784 - 971*tmp2821)*tmp83)*tmp844 + tmp60*(tmp1653*(tm&
            &p2780 + tmp2820 + tmp2902) + (-2403*tmp2775 + 2977*tmp2784 - 2718*tmp2821)*tmp64&
            &2 + (-367*tmp2775 + 1855*tmp2784 - 3272*tmp2821)*tmp61*tmp70 + ((1729*tmp2775 - &
            &3008*tmp2784 + 3579*tmp2821)*tmp83)/tmp10 + (-95*tmp2775 + 142*tmp2784 + tmp3104&
            &)*tmp919)) + tmp211*(tmp2438*(betas*tmp24*tmp3089 + (betat*(tmp2922 + tmp3088))/&
            &tmp54) + tmp636*((tmp128*(83*betas*(47*betat - 22*betau) - 4763*tmp2821))/tmp54 &
            &+ (-574*tmp2775 + 1270*tmp2784 + tmp3090)*tmp61 + (tmp3049 + tmp3084)*tmp3177*tm&
            &p70) + tmp60*((tmp2585*(-916*tmp2775 + 2165*tmp2784 - 2407*tmp2821))/tmp10 + tmp&
            &1653*(tmp2817 + tmp2981 + tmp3091) + (-610*tmp2775 + 1266*tmp2784 + tmp3090)*tmp&
            &641 + (-5908*tmp2775 + 10416*tmp2784 - 12443*tmp2821)*tmp642 + (-3132*tmp2775 + &
            &10388*tmp2784 - 11525*tmp2821)*tmp61*tmp70) + (tmp1650*(600*tmp2775 - 2354*tmp27&
            &84 + 1507*tmp2821) + 5*tmp1422*(1800*tmp2775 - 3666*tmp2784 + 4441*tmp2821) + tm&
            &p195*(-592*tmp2775 + 1268*tmp2784 + tmp3090) + (tmp2784 + tmp2855 + tmp3091)*tmp&
            &385)*tmp74 + tmp17*(4*tmp1422*(158*tmp2775 + 30*tmp2784 - 59*tmp2821) + tmp1650*&
            &(4228*tmp2775 - 10768*tmp2784 + 13191*tmp2821) + tmp3089*tmp3172*tmp82 + (-1172*&
            &tmp2775 + 2842*tmp2784 - 3409*tmp2821)*tmp83)*tmp844)*tmp93 + tmp244*tmp2775*tmp&
            &339*((tmp2477 + tmp3075 + 68*tmp642 + tmp669 - 104*tmp61*tmp70)/tmp55 + tmp74*(t&
            &mp1746 + tmp672 + tmp802) + tmp2175*tmp828 + (tmp195 + tmp2135 + tmp2793/tmp54 +&
            & tmp674)*tmp993)*tt + betat*tmp2951*(tmp3113 + tmp3111*tmp636 + tmp663 + (tmp151&
            & + tmp2350 + tmp2914 + tmp656)*tmp74 + tmp60*(tmp1350 + tmp2252 + tmp2455 + tmp6&
            &65 + tmp919))*tt**10))))/(tmp100*tmp96*tmp99) - (256*betas*tmp1560*tmp157*tmp177&
            &7*tmp2821*tmp685*((tmp1*tmp1584*tmp2614*tmp410*tmp522)/tmp96 + (tmp1535*tmp2760*&
            &tmp522*tmp527*tmp713)/(tmp9*tmp96) + (tmp2760*tmp48*tmp522*tmp527*tmp713)/(tmp9*&
            &tmp96) + (tmp1584*tmp410*tmp522*(tmp1827*tmp2608 + tmp241*tmp2612 - 256*tmp30 - &
            &16*tmp2606*tmp4 + tmp2603*tmp714)*tmp97)/tmp96 + (tmp1584*tmp410*tmp522*tmp550*t&
            &mp97)/(tmp96*tmp99) + (tmp2775*tmp527*(tmp3185*(m2*tmp2296*tmp522*tmp723 + (-(tm&
            &p3204*tmp522) + (tmp158*tmp705*tmp723)/tmp55)/tmp100) + tmp158*tmp522*(-((tmp276&
            &8/tmp100 + tmp1719*tmp2948)*tmp3126) + betau*tmp1688*tmp2768*tmp713)*tmp723*tt))&
            &/tmp9 + tmp2768*tmp2775*tmp527*(tmp158*tmp3192*(-(tmp3126/tmp100) + betau*tmp168&
            &8*tmp713)*tmp723*tt + ((tmp3193 + tmp3192*tmp3204)/tmp100 + m2*tmp3192*tmp690*tm&
            &p723)*(-8*betau*tmp4 + tmp2774*tmp709 + m2*(-2*tmp3126 + betau*tmp3178 + tmp2951&
            &*tt)))))/(tmp10*tmp96) + 256*tmp1053*tmp157*tmp158*tmp2821*tmp3126*tmp72*((tmp16&
            &4*tmp1693*tmp2723*tmp527*tmp581)/(tmp11*tmp54) + (tmp158*tmp1833*tmp2619*tmp469*&
            &tmp527*tmp581)/tmp54 + tmp1693*tmp1720*tmp410*tmp469*tmp527*tmp685*tmp701 + m2*t&
            &mp410*tmp469*tmp527*tmp535*tmp685*tmp7*tmp701 + (tmp2549*tmp2621*tmp410*tmp469*t&
            &mp581*tmp685*tmp713)/tmp11 + (tmp2551*tmp2621*tmp410*tmp469*tmp581*tmp685*tmp713&
            &)/tmp11 + tmp1560*tmp164*tmp2723*tmp527*tmp581*tmp85 + betat*tmp3114*tmp410*((tm&
            &p158*tmp175*tmp469*(betas*(-4*betat*(tmp3219*tmp36 + tmp3219*tmp684 + m2*(tmp322&
            &0 + tmp1772*tmp5 + tmp686/(tmp11*tmp54))) - (tmp2768*((tmp177 + tmp2550)/tmp11 +&
            & tmp241*(tmp160/tmp11 + tmp2767) + 4*tmp4*tmp7))/tmp99)*tt + (tmp2768*tmp3127*(t&
            &mp1058*tmp4 + m2*(tmp1772/tmp11 + tmp690*(1/tmp55 + tmp76)) + tmp3219*tt))/tmp96&
            &))/tmp11 + tmp3168*((m2*tmp158*tmp175*tmp2296*tmp469*tmp7)/tmp99 + (m2*tmp158*tm&
            &p175*tmp3221*tmp469 - (-(tmp469*(-(tmp175*(tmp2552/tmp11 + tmp158*(tmp2767 + tmp&
            &3221))) + tmp1056*tmp15*tmp697)) + tmp15*tmp250*(-48*tmp32 + 4*(11/tmp11 + tmp18&
            &27)*tmp4 + tmp428 + tmp44 + m2*(-12*tmp5 + 8*tmp6))*(tmp206 + tmp17*tmp60 + tmp6&
            &1/tmp55)*tt)/tmp99)/tmp96))) - (256*tmp157*tmp2821*tmp3126*tmp685*tmp72*tt*(m2*t&
            &mp2533*tmp2614*tmp410*tmp433*tmp564*tmp581 + tmp2533*tmp2614*tmp410*tmp564*tmp58&
            &1*tmp697 + tmp1*tmp180*tmp410*tmp527*tmp564*tmp685*tmp712 + tmp1720*tmp410*tmp52&
            &7*tmp564*tmp6*tmp685*tmp712 + tmp19*tmp2731*tmp2760*tmp527*tmp564*tmp581*tmp713 &
            &+ tmp1071*tmp2760*tmp5*tmp527*tmp564*tmp581*tmp713 + (tmp2533*tmp36*tmp410*tmp55&
            &0*tmp564*tmp581)/tmp99 + tmp2775*tmp527*(tmp36*tmp564*tmp723*((16*betau*tmp19*tm&
            &p2768*tmp32)/tmp96 - (tmp158*tmp3224*(tmp177 + tmp3225))/(tmp100*tmp96) + ((tmp3&
            &222*(-((tmp158*tmp7)/tmp100) + tmp19/(tmp11*tmp96)) - tmp2768*((betas*tmp158*tmp&
            &7)/tmp100 + (betau*tmp19)/(tmp11*tmp96)))*tmp966)/tmp100 + tmp241*(betas*tmp3179&
            &*((tmp158*tmp7*tmp713)/tmp100 + (m2*tmp19*tmp518)/tmp96) - tmp2768*((m2*tmp158*t&
            &mp3088*tmp7)/tmp100 + (tmp2533*tmp2773)/(tmp100*tmp96) + (tmp158*tmp3114*tmp7)/(&
            &tmp100*tmp96) + (3*betau*(m2*tmp19*tmp518 + (tmp158*tmp7)/tmp100))/tmp96 + (m2*t&
            &mp2951*tmp988)/tmp96))) + tmp3185*((m2*tmp19*tmp2296*tmp564*tmp723)/(tmp11*tmp96&
            &) + (m2*tmp158*tmp564*tmp690*tmp7*tmp723 + (-(tmp564*(tmp1074*tmp15*tmp697 - (tm&
            &p1070 + tmp158*(tmp2767 + tmp3226))*tmp723)) + tmp250*tmp2533*tmp723*tmp985*tt)/&
            &tmp96)/tmp100))))/tmp55 + (256*tmp1053*tmp157*tmp2821*tmp3126*tmp527*tmp685*tmp7&
            &*(m2*tmp249*tmp410*tmp685*tmp686*tmp712 + tmp249*tmp410*tmp685*tmp697*tmp712 - t&
            &mp1560*tmp249*tmp3170*tmp581*tmp967 + (tmp1693*tmp249*tmp3170*tmp581*tmp967)/tmp&
            &11 + tmp249*tmp2773*tmp3126*tmp723*(betau*tmp2768*tmp31 + (tmp241*(tmp3222*(tmp3&
            &174 + tmp565 + tmp6) + tmp2768*((tmp3175 + tmp3176)*tmp36 + (-3*betau + tmp3088)&
            &*tmp515 + (betas + tmp3177)*tmp6)))/tmp100 + tmp3224*tmp410*tmp526*tmp94 + 16*tm&
            &p32*(betas*tmp3179*tmp526 + tmp2768*((betas + tmp3173)/tmp11 + (betas + tmp3171)&
            &*tt)) - 4*tmp4*(betas*tmp3179*(tmp2600 + tmp452 + tmp939) + tmp2768*((-25*betau &
            &+ tmp2859)*tmp5 + (tmp3029 + tmp3173)*tmp6 + (tmp3172 + tmp3176)*tmp698*tt))) + &
            &tmp2775*tmp3185*((m2*tmp249*tmp518*tmp723)/tmp96 + (tmp158*tmp249*tmp723*tmp966 &
            &+ ((tmp15*tmp2541*(tmp2608 - 32*tmp32 + tmp1*tmp721 + tmp4*(40/tmp11 - 4*tt)))/t&
            &mp10 + tmp249*(4*tmp4*(tmp2414 + tmp41 + tmp515) + tmp528 + (tmp166 + tmp46 + tm&
            &p47 + tmp48)*tt + tmp1*(tmp40 + tmp452 - 3*tmp5)*tt - 16*tmp32*(tmp3178 + tt)))/&
            &tmp96)/tmp100)))/(tmp54*tmp9) + (256*tmp1053*tmp157*tmp1584*tmp2821*tmp3126*(tmp&
            &15*tmp1693*tmp1720*tmp2545*tmp3214*tmp410*tmp527 + (tmp158*tmp1833*tmp249*tmp261&
            &9*tmp527*tmp581)/tmp10 + (tmp1535*tmp249*tmp2723*tmp527*tmp581)/tmp10 + (tmp1560&
            &*tmp249*tmp2723*tmp527*tmp581)/tmp10 + m2*tmp158*tmp249*tmp3214*tmp410*tmp527*tm&
            &p535*tmp7 + m2*tmp1584*tmp249*tmp3207*tmp410*tmp433*tmp581*tmp713 + betat*tmp311&
            &4*tmp3168*tmp410*((m2*tmp1584*tmp175*tmp249*tmp518)/tmp96 + (m2*tmp158*tmp175*tm&
            &p2296*tmp249*tmp7 + ((tmp158*tmp175*tmp36*tmp689)/tmp10 + tmp249*(-(tmp175*(-2*t&
            &mp15*tmp250 + tmp3217 + tmp3218)) + tmp15*tmp697*tmp983))/tmp96)/tmp99) + tmp158&
            &*tmp249*tmp3207*tmp3215*tmp410*tmp581*tmp713*tt + tmp158*tmp175*tmp249*tmp2839*t&
            &mp3126*tmp410*((tmp2768*tmp2925*tmp32)/(tmp9*tmp96) + (tmp966*(-(tmp2768*((betat&
            &*tmp250)/tmp96 + (betas*tmp177)/tmp99)) + betat*tmp3172*(tmp250/tmp96 + tmp2767/&
            &tmp99)))/tmp99 + ((tmp2817 + tmp2768*(betas + tmp2839))*(tmp177 + tmp3215 + tmp1&
            &*tmp7)*tt)/(tmp96*tmp99) + tmp241*(betas*tmp3040*((m2*tmp518)/(tmp9*tmp96) + (tm&
            &p177*tmp713)/tmp99) - tmp2768*((tmp177*tmp3114*tmp713)/tmp99 + (betat*(tmp2424 +&
            & tmp3216*tmp6 + tmp241*(tmp518/tmp9 + (tmp108 + tmp1574 + tmp2)*tt)))/tmp96)))))&
            &/tmp54))/(betau*tmp15**4*tmp3047*tmp93)

  END FUNCTION NTSOFT
     
                          !!!!!!!!!!!!!!!!!!!!!!
                         END MODULE ee_ee2eegl_nts
                          !!!!!!!!!!!!!!!!!!!!!!
