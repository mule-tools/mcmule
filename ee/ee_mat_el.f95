
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE EE_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use ee_ee2eel, only: ee2eel
  use ee_ee2eeg, only: ee2eeg
  use ee_ee2eegg, only: ee2eegg
  use ee_ee2ee_a, only: ee2ee_altr, ee2ee_albx, ee2eeg_atr, ee2eeg_abx
  use ee_ee2ee_nfbx, only: ee2ee_nfbx
  use ee_ee2eegl_nts, only: ee2eegl_nts

  implicit none

  contains


                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!     MATRIX   ELEMENTS       !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !!!!!!!!!!    Born    !!!!!!!!!!!!

  FUNCTION EE2EE(p1,p2,q1,q2)
    !! e-(p1) e-(p2) -> e-(q1) e-(q2)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ee2ee

  ee2ee = ee2ee_tt(p1,p2,q1,q2) + ee2ee_tu(p1,p2,q1,q2)&
        + ee2ee_tt(p1,p2,q2,q1) + ee2ee_tu(p1,p2,q2,q1)

  END FUNCTION EE2EE

  FUNCTION EE2EE_TT(p1,p2,q1,q2)
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt, m2
  real (kind=prec) :: ee2ee_tt

  ss = sq(p1+p2)
  tt = sq(p1-q1)
  m2=sq(p1)

  ee2ee_tt = (2*(-2*m2 + ss)**2 + 2*ss*tt + tt**2)/tt**2
  ee2ee_tt = 32 * pi**2 * alpha**2 * ee2ee_tt
  END FUNCTION EE2EE_TT

  FUNCTION EE2EE_TU(p1,p2,q1,q2)
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt, m2
  real (kind=prec) :: ee2ee_tu

  ss = sq(p1+p2)
  tt = sq(p1-q1)
  m2=sq(p1)

  ee2ee_tu = -((12*m2**2 - 8*m2*ss + ss**2)/(tt*(-4*m2 + ss + tt)))
  ee2ee_tu = 32 * pi**2 * alpha**2 * ee2ee_tu
  END FUNCTION EE2EE_TU




  FUNCTION EE2EE_A(p1,p2,p3,p4)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4)
    !! massive electrons
   real(kind=prec) :: p1(4),p2(4),p3(4),p4(4)
   real(kind=prec) :: ee2ee_a
   real(kind=prec) :: t, u

   t = sq(p1-p3); u = sq(p1-p4)

   ee2ee_a = 2*deltalph_0_tot(t)*(ee2ee_tt(p1,p2,p3,p4)+ee2ee_tu(p1,p2,p3,p4)) &
           + 2*deltalph_0_tot(u)*(ee2ee_tt(p1,p2,p4,p3)+ee2ee_tu(p1,p2,p4,p3))


  END FUNCTION


  !!!!!!!!!!!!!!!!! Bhabha Scattering !!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EB2EB(p1,p2,q1,q2)
    !! e-(p1) e+(p2) -> e-(q1) e+(q2)
    !! for massive electrons & positrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: eb2eb

  eb2eb = ee2ee(p1,-q2,q1,-p2)
  END FUNCTION EB2EB

  FUNCTION EB2EBl(p1, p2, q1, q2,pole,img)
    !! e-(p1) e+(p2) -> e-(q1) e+(q2)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), q1(4), q2(4)
  real(kind=prec) :: eb2ebl
  real(kind=prec),optional :: pole, img(2)

  if(present(pole).AND.present(img)) then
    eb2ebl = ee2eel(p1,-q2,q1,-p2,pole=pole,img=img)
  else if(present(pole)) then
    eb2ebl = ee2eel(p1,-q2,q1,-p2,pole=pole)
  else if(present(img)) then
    eb2ebl = ee2eel(p1,-q2,q1,-p2,img=img)
  else
    eb2ebl = ee2eel(p1,-q2,q1,-p2)
  end if

  END FUNCTION

  FUNCTION EB2EB_A(p1, p2, q1, q2)
    !! e-(p1) e+(p2) -> e-(q1) e+(q2)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), q1(4), q2(4)
  real(kind=prec) :: eb2eb_a
  eb2eb_a = ee2ee_a(p1,-q2,q1,-p2)
  END FUNCTION

  FUNCTION EB2EBG(p1,p2,p3,p4,p5)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4) g(p5)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real(kind=prec) :: eb2ebg

  eb2ebg = ee2eeg(p1,-p4,p3,-p2,p5)

  END FUNCTION


  FUNCTION EB2EBGL_NTS(p1,p2,p3,p4,p5)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4) g(p5)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real(kind=prec) :: eb2ebgl_nts

  eb2ebgl_nts = ee2eegl_nts(p1,-p4,p3,-p2,p5)

  END FUNCTION



          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!              NNLO                  !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EE2EELLz(p1, p2, p3, p4, doub, sing)
    !! e-(p1) e-(p2) -> e-(p3) e-(p4)
    !! for massified electrons
  use massification
  use ee_twoloop2, only: bhabha
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2eellz
  real(kind=prec), optional :: doub, sing
  complex(kind=prec) :: born, ol(-2:2), tl(-4:0)
  real(kind=prec) :: olZ(-1:1), tlZ(-2:0)

  call bhabha(sq(p1-p3), sq(p1-p4), born, ol, tl, prescription=-1)

  call massify(log(sq(p2)/musq), 4., real(born), 2*pi*real(ol), 4*pi*pi*real(tl), olZ, tlZ)
  olZ = olZ / 2 / pi
  tlZ = tlZ / 4 / pi/pi

  ee2eellz = alpha**4*tlZ(0)
  if(present(doub)) doub = alpha**4*tlZ(-2)
  if(present(sing)) sing = alpha**4*tlZ(-1)

  END FUNCTION EE2EELLz

  FUNCTION EE2EEFFz(p1, p2, p3, p4)
    !! e-(p1) e-(p2) -> e-(p3) e-(p4)
    !! for massified electrons
  use massification
  use ee_twoloop2, only: bhabha
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2eeffz
  complex(kind=prec) :: born, ol(-2:2), tl(-4:0)
  real(kind=prec) :: olZ(-1:1), tlZ(-2:0), ct(-1:0)

  call bhabha(sq(p1-p3), sq(p1-p4), born, ol, tl, prescription=-1)

  call massify(log(sq(p2)/musq), 4., real(born), 2*pi*real(ol), 4*pi*pi*real(tl), olZ, tlZ)
  olZ = olZ / 2 / pi
  tlZ = tlZ / 4 / pi/pi

  born = ee2ee(p1, p2, p3, p4)
  olZ(0) = ee2eel(p1, p2, p3, p4, olZ(-1))

  ct(0) = Ieik(xieik2, sqrt(sq(p1+p2)), parts((/ part(p1, 1, 1), part(p2, 1, 1), &
                                                 part(p3, 1, -1), part(p4, 1, -1) /)), ct(-1))
  ct = alpha * ct / 2 / pi

#if 0
  write(*,900) abs(ct(-1)*real(born) + olZ(-1)) / olZ(-1), &
               abs(tlZ(-2) + ct(-1) * olZ(-1) + real(born) * ct(-1)**2/2) / tlZ(-2), &
               abs(tlZ(-1) + ct( 0) * olZ(-1) + ct(-1) * olZ(0) + real(born) * ct(-1)*ct(0)) / tlZ(-1)
900 format("res. pole: ", "a^3/ep = ", ES7.1E2, ", a^4/ep^2 = ", ES7.1E2, ", a^4/ep = ", ES7.1E2)
#endif
  ee2eeffz = tlZ(0) + ct(0)*olZ(0) + ct(-1)*olZ(1) + real(born)*ct(0)**2/2

  END FUNCTION EE2EEFFz


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! e-(p1) e-(p2) -> e-(p3) e-(p4) g(p5)
  ! for massive electrons
  FUNCTION EE2EEGL_coll(p1, p2, p3, p4, p5)
  use ee_ee2eegl_coll, only: qsym
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: ss,tt,s25,s35,s15,m,m2
  real(kind=prec) :: ee2eegl_coll

  ss = sq(p1+p2)
  tt = sq(p2-p4)
  m2 = sq(p1)
  m = sqrt(m2)
  s15 = s(p1,p5)
  s25 = s(p2,p5)
  s35 = s(p3,p5)

  ee2eegl_coll = qsym(ss, tt, s15, s25, s35, m) &
          + qsym(ss, s15 - s35 + tt, s25, s15, s15 + s25 - s35, m) & ! Symm 2
          + qsym(ss, 4*m2 - ss + s25 - tt, s15, s25, s15 + s25 - s35, m) & ! Symm 3
          + qsym(ss, 4*m2 - ss + s35 - tt, s25, s15, s35, m) ! Symm 23

  ee2eegl_coll = 2.**4*Pi**2*alpha**4*ee2eegl_coll
  END FUNCTION EE2EEGL_coll


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! e-(p1) e-(p2) -> e-(p3) e-(p4) g(p5)
  ! for massive electrons
  FUNCTION EE2EEGL_pvred(p1, p2, p3, p4, p5)
  use ee_ee2eegl, only: qsym
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: ss,tt,s25,s35,s15,m,m2
  real(kind=prec) :: ee2eegl_pvred

  ss = sq(p1+p2)
  tt = sq(p2-p4)
  m2 = sq(p1)
  m = sqrt(m2)
  s15 = s(p1,p5)
  s25 = s(p2,p5)
  s35 = s(p3,p5)

  ee2eegl_pvred = qsym(ss, tt, s15, s25, s35, m) &
          + qsym(ss, s15 - s35 + tt, s25, s15, s15 + s25 - s35, m) & ! Symm 2
          + qsym(ss, 4*m2 - ss + s25 - tt, s15, s25, s15 + s25 - s35, m) & ! Symm 3
          + qsym(ss, 4*m2 - ss + s35 - tt, s25, s15, s35, m) ! Symm 23

  ee2eegl_pvred = 2.**4*Pi**2*alpha**4*ee2eegl_pvred
  END FUNCTION EE2EEGL_pvred


  FUNCTION EE2EEGL(p1, p2, p3, p4, p5)
    !! e-(p1) e-(p2) -> e-(p3) e-(p4) g(p5)
    !! for massive electrons
  use olinterface
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: ee2eegl
  real(kind=prec) :: xi

  xi = 2*p5(4)/sqrt(scms)

  if(xi>ntsSwitch) then
      call openloops("ee2eeg", p1, p2, p3, p4, p5, fin=ee2eegl)
  else
      ee2eegl = ee2eegl_nts(p1,p2,p3,p4,p5)
  end if

  END FUNCTION EE2EEGL

  FUNCTION EE2EE_AA(p1,p2,p3,p4)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4)
    !! massive electrons
   real(kind=prec) :: p1(4),p2(4),p3(4),p4(4)
   real(kind=prec) :: ee2ee_aa
   real(kind=prec) :: t, u

   t = sq(p1-p3); u = sq(p1-p4)

   ! bubble chain & genuine 2-loop vp
   ee2ee_aa = 2*(real(deltalph_0_tot_cplx(t)**2)+deltalph_l_1_tot(t))*(ee2ee_tt(p1,p2,p3,p4)+ee2ee_tu(p1,p2,p3,p4))&
            + 2*(real(deltalph_0_tot_cplx(u)**2)+deltalph_l_1_tot(u))*(ee2ee_tt(p1,p2,p4,p3)+ee2ee_tu(p1,p2,p4,p3))

   ! bubble^2
   ee2ee_aa = ee2ee_aa + real(deltalph_0_tot_cplx(t)**2)*ee2ee_tt(p1,p2,p3,p4)&
                       + real(deltalph_0_tot_cplx(u)**2)*ee2ee_tt(p1,p2,p4,p3)&
                       + real(deltalph_0_tot_cplx(t)*deltalph_0_tot_cplx(u))*ee2ee_tu(p1,p2,p3,p4)&
                       + real(deltalph_0_tot_cplx(u)*deltalph_0_tot_cplx(t))*ee2ee_tu(p1,p2,p4,p3)
  END FUNCTION

  FUNCTION EE2EE_AL(p1,p2,p3,p4,pole)
    !! e-(p1) e-(p2) --> e-(q1) e-(q2)
    !! massive electrons
  real(kind=prec) :: ee2ee_al
  real(kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: pole_tr, pole_bx

  if(present(pole)) then
    ee2ee_al = ee2ee_altr(p1,p2,p3,p4,pole_tr) + ee2ee_albx(p1,p2,p3,p4,pole_bx)
    pole = pole_tr + pole_bx
  else
    ee2ee_al = ee2ee_altr(p1,p2,p3,p4) + ee2ee_albx(p1,p2,p3,p4)
  end if
  END FUNCTION EE2EE_AL


  FUNCTION EE2EEG_A(p1,p2,p3,p4,p5)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4) g(p5)
    !! massive electrons
   real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
   real(kind=prec) :: ee2eeg_a

   ee2eeg_a = ee2eeg_atr(p1,p2,p3,p4,p5) + ee2eeg_abx(p1,p2,p3,p4,p5)

  END FUNCTION


  FUNCTION EE2EE_NF(y,p1,p2,p3,p4)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4)
    !! massive electrons
    real (kind=prec) :: ee2ee_nf
    real (kind=prec), intent(in) :: y,p1(4),p2(4),p3(4),p4(4)

    ee2ee_nf = ee2ee_nftr(y,p1,p2,p3,p4) + ee2ee_nfbx(y,p1,p2,p3,p4)
  END FUNCTION


  FUNCTION EE2EE_NFTR(y,p1,p2,p3,p4)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4)
    !! massive electrons
    real (kind=prec) :: ee2ee_nftr
    real (kind=prec), intent(in) :: y,p1(4),p2(4),p3(4),p4(4)

    ee2ee_nftr = ee2ee_nftr_t(y,p1,p2,p3,p4)+ee2ee_nftr_t(y,p1,p2,p4,p3)
  END FUNCTION


  FUNCTION EE2EE_NFTR_T(y,p1,p2,p3,p4)
    real (kind=prec) :: ee2ee_nftr_t
    real (kind=prec), intent(in) :: y,p1(4),p2(4),p3(4),p4(4)
    real (kind=prec) :: ss,tt,m2
    real (kind=prec) :: f1,f2

    ss = sq(p1+p2)
    tt = sq(p1-p3)
    m2=sq(p1)

    f1 = f1nf(y,tt,m2)
    f2 = f2nf(y,tt,m2)

    ee2ee_nftr_t = f2*tt*(-16*m2**2 + 2*m2*(2*ss - 7*tt) + 3*tt*(2*ss + tt)) + 2*f1*(-32*m2**3 + 2*&
                 &ss**3 + m2**2*(40*ss - 4*tt) + 3*ss**2*tt + 3*ss*tt**2 + tt**3 - 4*m2*(4*ss**2 +&
                 &2*ss*tt + tt**2))

    ee2ee_nftr_t = (64*alpha**2*Pi**2)/(tt**2*(-4*m2 + ss + tt))*ee2ee_nftr_t

  END FUNCTION


  !!!!!!!!!!!!!!!!! Bhabha Scattering !!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EB2EBLLz(p1, p2, p3, p4, doub, sing)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4)
    !! for massified electrons
  use massification
  use ee_twoloop2, only: bhabha
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: eb2ebllz
  real(kind=prec), optional :: doub, sing
  complex(kind=prec) :: born, ol(-2:2), tl(-4:0)
  real(kind=prec) :: olZ(-1:1), tlZ(-2:0)

  call bhabha(sq(p1+p2), sq(p1-p3), born, ol, tl)

  call massify(log(sq(p2)/musq), 4., real(born), 2*pi*real(ol), 4*pi*pi*real(tl), olZ, tlZ)
  olZ = olZ / 2 / pi
  tlZ = tlZ / 4 / pi/pi

  eb2ebllz = alpha**4*tlZ(0)
  if(present(doub)) doub = alpha**4*tlZ(-2)
  if(present(sing)) sing = alpha**4*tlZ(-1)

  END FUNCTION EB2EBLLz

  FUNCTION EB2EBFFz(p1, p2, p3, p4)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4)
    !! for massified electrons
  use massification
  use ee_twoloop2, only: bhabha
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: eb2ebffz
  complex(kind=prec) :: born, ol(-2:2), tl(-4:0)
  real(kind=prec) :: olZ(-1:1), tlZ(-2:0), ct(-1:0)

  call bhabha(sq(p1+p2), sq(p1-p3), born, ol, tl)
  call massify(log(sq(p2)/musq), 4., real(born), 2*pi*real(ol), 4*pi*pi*real(tl), olZ, tlZ)
  olZ = alpha**3*olZ / 2 / pi
  tlZ = alpha**4*tlZ / 4 / pi/pi

  born = eb2eb(p1, p2, p3, p4)
  olZ(0) = eb2ebl(p1, p2, p3, p4, olZ(-1))

  ct(0) = Ieik(xieik2, sqrt(sq(p1+p2)), parts((/ part(p1, 1, 1), part(p2,-1, 1), &
                                                 part(p3, 1, -1), part(p4,-1, -1) /)), ct(-1))
  ct = alpha * ct / 2 / pi

#if 0
  write(*,900) abs(ct(-1)*real(born) + olZ(-1)) / olZ(-1), &
               abs(tlZ(-2) + ct(-1) * olZ(-1) + real(born) * ct(-1)**2/2) / tlZ(-2), &
               abs(tlZ(-1) + ct( 0) * olZ(-1) + ct(-1) * olZ(0) + real(born) * ct(-1)*ct(0)) / tlZ(-1)
900 format("res. pole: ", "a^3/ep = ", ES7.1E2, ", a^4/ep^2 = ", ES7.1E2, ", a^4/ep = ", ES7.1E2)
#endif
  eb2ebffz = tlZ(0) + ct(0)*olZ(0) + ct(-1)*olZ(1) + real(born)*ct(0)**2/2

  END FUNCTION EB2EBFFz

  FUNCTION EB2EB_AA(p1, p2, q1, q2)
    !! e-(p1) e+(p2) -> e-(q1) e+(q2)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), q1(4), q2(4)
  real(kind=prec) :: eb2eb_aa
  eb2eb_aa = ee2ee_aa(p1,-q2,q1,-p2)
  END FUNCTION

  FUNCTION EB2EBG_A(p1,p2,p3,p4,p5)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4) g(p5)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real(kind=prec) :: eb2ebg_a

  eb2ebg_a = ee2eeg_a(p1,-p4,p3,-p2,p5)

  END FUNCTION

  FUNCTION EB2EBGG(p1,p2,p3,p4,p5,p6)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4) g(p5) g(p6)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),p6(4)
  real(kind=prec) :: eb2ebgg

  eb2ebgg = ee2eegg(p1,-p4,p3,-p2,p5,p6)

  END FUNCTION


  FUNCTION EB2EBGL(p1, p2, p3, p4, p5)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4) g(p5)
    !! for massive electrons & positrons
  use olinterface
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: eb2ebgl
  real(kind=prec) :: xi

  xi = 2*p5(4)/sqrt(scms)

  if(xi>ntsSwitch) then
      call openloops("eb2ebg", p1, p2, p3, p4, p5, fin=eb2ebgl)
  else
      eb2ebgl = eb2ebgl_nts(p1,p2,p3,p4,p5)
  end if

  END FUNCTION EB2EBGL





          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!         integrand function         !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EE2EEF(p1,p2,q1,q2)
    !! e-(p1) e-(p2) --> e-(q1) e-(q2)
    !! massive electrons
  real(kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real(kind=prec) :: ee2eef, mat0, Epart

  Epart = sqrt(scms)
  mat0 = ee2ee(p1,p2,q1,q2)

  !! factor alpha / (2 * pi) due to the definition in (4.20) from paper 9512328

  ee2eef = ee2eel(p1,p2,q1,q2) + alpha / (2 * pi) * mat0 * &
       Ieik(xieik1, Epart, parts((/ part(p1, 1, 1), part(p2, 1, 1), &
                                    part(q1, 1, -1), part(q2, 1, -1) /)))

  END FUNCTION EE2EEF


  FUNCTION EE2EE_AF(p1,p2,q1,q2)
    !! e-(p1) e-(p2) --> e-(q1) e-(q2)
    !! massive electrons
  real(kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real(kind=prec) :: ee2ee_af, mat0, Epart

  Epart = sqrt(scms)
  mat0 = ee2ee_a(p1,p2,q1,q2)

  !! factor alpha / (2 * pi) due to the definition in (4.20) from paper 9512328

  ee2ee_af = ee2ee_al(p1,p2,q1,q2)

  ee2ee_af = ee2ee_af + alpha / (2 * pi) * mat0 * &
       Ieik(xieik1, Epart, parts((/ part(p1, 1, 1), part(p2, 1, 1), &
                                    part(q1, 1, -1), part(q2, 1, -1) /)))
  END FUNCTION EE2EE_AF


  FUNCTION EB2EBF(p1,p2,q1,q2)
    !! e-(p1) e+(p2) --> e-(q1) e+(q2)
    !! massive electrons & positrons
  real(kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real(kind=prec) :: eb2ebf, epart, mat0

  Epart = sqrt(scms)
  mat0 = eb2eb(p1,p2,q1,q2)
  eb2ebf = eb2ebl(p1,p2,q1,q2)

  eb2ebf = eb2ebf + alpha / (2 * pi) * mat0 * &
       Ieik(xieik1, Epart, parts((/ part(p1, 1, 1), part(p2,-1, 1), &
                                    part(q1, 1, -1), part(q2,-1, -1) /)))

  END FUNCTION EB2EBF

  FUNCTION EE2EEGF(p1,p2,q1,q2,q3)
    !! e-(p1) e-(p2) --> e-(q1) e-(q2) g(q3)
    !! massive electrons
  use olinterface
  real(kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real(kind=prec) :: ee2eegf, mat0, Epart

  Epart = sqrt(scms)
  mat0 = ee2eeg(p1,p2,q1,q2,q3)
  ee2eegf = ee2eegl(p1,p2,q1,q2,q3)

  !! factor alpha / (2 * pi) due to the definition in (4.20) from paper 9512328

  ee2eegf = ee2eegf + alpha / (2 * pi) * mat0 * &
       Ieik(xieik1, Epart, parts((/ part(p1, 1, 1), part(p2, 1, 1), &
                                    part(q1, 1, -1), part(q2, 1, -1) /)))
  END FUNCTION EE2EEGF


  FUNCTION EB2EBGF(p1,p2,q1,q2,q3)
    !! e-(p1) e+(p2) --> e-(q1) e+(q2) g(q3)
    !! massive electrons & positrons
  use olinterface
  real(kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real(kind=prec) :: eb2ebgf, epart, mat0

  Epart = sqrt(scms)
  mat0 = eb2ebg(p1,p2,q1,q2,q3)
  eb2ebgf = eb2ebgl(p1,p2,q1,q2,q3)

  eb2ebgf = eb2ebgf + alpha / (2 * pi) * mat0 * &
       Ieik(xieik1, Epart, parts((/ part(p1, 1, 1), part(p2,-1, 1), &
                                    part(q1, 1, -1), part(q2,-1, -1) /)))

  END FUNCTION EB2EBGF





                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!  massless matrix elements   !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                          !!!!!!!!!!!!!!!!!!!!!!
                          !      ONE LOOP      !
                          !!!!!!!!!!!!!!!!!!!!!!
  FUNCTION EE2MMl0(p1, p2, p3, p4, doub, sing, lin, quad)
  use ee_twoloop2, only: twoloop
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2mml0
  real(kind=prec), optional :: doub, sing, lin, quad
  complex(kind=prec) :: eemm(-2:2)
  real(kind=prec) :: tt, ss
  integer lans
  ss = sq(p1+p2)
  tt = sq(p1-p3)

  lans = 0
  if(present(lin )) lans = 1
  if(present(quad)) lans = 2

  call twoloop(-tt/ss, -log(cmplx(ss/musq)), eemmA1=eemm(-2:lans))

  ee2mml0 = real(eemm(0))
  if(present(doub)) doub = real(eemm(-2))
  if(present(sing)) sing = real(eemm(-1))
  if(present(lin )) lin  = real(eemm(+1))
  if(present(quad)) quad = real(eemm(+2))
  END FUNCTION EE2MMl0

  FUNCTION EB2EBl0(p1, p2, p3, p4, doub, sing, lin, quad, prescription)
  use ee_twoloop2, only: bhabha
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: eb2ebl0
  real(kind=prec), optional :: doub, sing, lin, quad
  integer, optional :: prescription
  real(kind=prec) :: ss, tt
  complex(kind=prec) :: ans(-2:2)

  ss = sq(p1+p2)
  tt = sq(p1-p3)

  call bhabha(ss, tt, ol=ans, prescription=prescription)
  eb2ebl0 = real(ans(0))
  if(present(doub)) doub = real(ans(-2))
  if(present(sing)) sing = real(ans(-1))
  if(present(lin )) lin  = real(ans(+1))
  if(present(quad)) quad = real(ans(+2))

  END FUNCTION

  FUNCTION EM2EMl0(p1, p2, p3, p4, doub, sing, lin, quad)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2eml0
  real(kind=prec), optional :: doub, sing, lin, quad
  em2eml0 = ee2mml0(p1,-p3,-p2,p4, doub, sing, lin, quad)
  END FUNCTION

  FUNCTION EE2EEl0(p1, p2, p3, p4, doub, sing, lin, quad)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2eel0
  real(kind=prec), optional :: doub, sing, lin, quad
  ee2eel0 = eb2ebl0(p1,-p3,p4,-p2, doub, sing, lin, quad, prescription=-1)
  END FUNCTION

                          !!!!!!!!!!!!!!!!!!!!!!
                          !      TWO LOOP      !
                          !!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EE2MMll0(p1, p2, p3, p4, quad, trip, doub, sing)
  use ee_twoloop2, only: twoloop
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2mmll0
  real(kind=prec), optional :: quad, trip, doub, sing
  complex(kind=prec) :: eemm(-4:0)
  real(kind=prec) :: tt, ss
  ss = sq(p1+p2)
  tt = sq(p1-p3)

  call twoloop(-tt/ss, -log(cmplx(ss/musq)), eemmA2=eemm)

  ee2mmll0 = real(eemm(0))
  if(present(quad)) quad = real(eemm(-4))
  if(present(trip)) trip = real(eemm(-3))
  if(present(doub)) doub = real(eemm(-2))
  if(present(sing)) sing = real(eemm(-1))
  END FUNCTION EE2MMll0

  FUNCTION EB2EBll0(p1, p2, p3, p4, quad, trip, doub, sing, prescription)
  use ee_twoloop2, only: bhabha
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: eb2ebll0
  real(kind=prec), optional :: quad, trip, doub, sing
  integer, optional :: prescription
  real(kind=prec) :: ss, tt
  complex(kind=prec) :: ans(-4:0)

  ss = sq(p1+p2)
  tt = sq(p1-p3)
  call bhabha(ss, tt, tl=ans, prescription=prescription)

  eb2ebll0 = real(ans(0))
  if(present(quad)) quad = real(ans(-4))
  if(present(trip)) trip = real(ans(-3))
  if(present(doub)) doub = real(ans(-2))
  if(present(sing)) sing = real(ans(-1))

  END FUNCTION

  FUNCTION EM2EMll0(p1, p2, p3, p4, quad, trip, doub, sing)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2emll0
  real(kind=prec), optional :: quad, trip, doub, sing
  em2emll0 = ee2mmll0(p1,-p3,-p2,p4, quad, trip, doub, sing)
  END FUNCTION

  FUNCTION EE2EEll0(p1, p2, p3, p4, quad, trip, doub, sing)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2eell0
  real(kind=prec), optional :: quad, trip, doub, sing
  ee2eell0 = eb2ebll0(p1,-p3,p4,-p2, quad, trip, doub, sing, prescription=-1)
  END FUNCTION



                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE EE_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
