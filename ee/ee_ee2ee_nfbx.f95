                   !!!!!!!!!!!!!!!!!!!!!!
                      MODULE EE_EE2EE_NFBX
                   !!!!!!!!!!!!!!!!!!!!!!

  use functions
  implicit none
  
  real(kind=prec) :: qq2
 
  INTERFACE D
    module procedure di, dij, dijk
  END INTERFACE


contains


  FUNCTION EE2EE_NFBX(z,p1,p2,q1,q2)
    !! e-(p1) e-(p2) -> e-(q1) e-(q2)
    !! for massive electrons
  real(kind=prec) :: z,p1(4),p2(4),q1(4),q2(4)
  real(kind=prec) :: ee2ee_nfbx
  real(kind=prec) :: ss,tt,m2,uu,x

  if(z<1E-3) then
    ee2ee_nfbx = 0._prec
    return
  endif

  x = 1 - z**5

  ss = sq(p1+p2); m2=sq(p1)
  tt = sq(p1-q1); uu = 4*m2-ss-tt
  qq2 = (m2*x**2)/(1 - x)

  ee2ee_nfbx = 5*z**4*(mbxt(x,ss,tt,m2) + mxbxt(x,ss,tt,m2)&
              + mbxt(x,ss,uu,m2) + mxbxt(x,ss,uu,m2))

  ! Factor of two since vp can attach to either photon in box
  ee2ee_nfbx = 2*ee2ee_nfbx
  END FUNCTION


  FUNCTION MBXT(x,ss,tt,m2)
    real(kind=prec) :: mbxt
    real(kind=prec), intent(in) :: x,ss,tt,m2

    mbxt = (256*m2**4 + 32*m2**3*(-14*ss + tt) + 16*m2**2*ss*(18*ss + tt) - 8*m2*ss*(10*ss*&
       &*2 + 4*ss*tt + 3*tt**2) + 2*ss*(4*ss**3 + 4*ss**2*tt + 3*ss*tt**2 + tt**3))*G012&
       &K(3,x,ss,tt,m2) - 2*(8*m2**2 - 6*m2*ss + ss**2 + 6*m2*tt - tt**2)*GIJ(0,1,3,x,ss&
       &,tt,m2) + 4*(8*m2**2 - 6*m2*ss + ss*(ss + tt))*GIJ(0,2,3,x,ss,tt,m2) - 2*(8*m2**&
       &2 - 6*m2*ss + ss**2 + 6*m2*tt - tt**2)*GIJ(0,3,3,x,ss,tt,m2) - 2*(8*m2**2 - 6*m2&
       &*ss + ss*(ss + tt))*GIJ(1,2,3,x,ss,tt,m2) + 2*(8*m2**2 + ss**2 - 6*m2*(ss - 2*tt&
       &) - ss*tt - 2*tt**2)*GIJ(1,3,3,x,ss,tt,m2) - 2*(8*m2**2 - 6*m2*ss + ss*(ss + tt)&
       &)*GIJ(2,3,3,x,ss,tt,m2) + 2*(32*m2**3 - 4*ss**3 - 3*ss**2*tt - 2*ss*tt**2 - tt**&
       &3 + m2**2*(-56*ss + 12*tt) + 4*m2*(7*ss**2 + tt**2))*GIJK(0,1,2,x,ss,tt,m2) + (3&
       &2*m2**2*(ss + tt) - 8*m2*(3*ss**2 + 2*ss*tt + tt**2) + 2*ss*(2*ss**2 + 3*ss*tt +&
       & tt**2))*GIJK(0,1,3,x,ss,tt,m2) + 2*(32*m2**3 - 4*ss**3 - 3*ss**2*tt - 2*ss*tt**&
       &2 - tt**3 + m2**2*(-56*ss + 12*tt) + 4*m2*(7*ss**2 + tt**2))*GIJK(0,2,3,x,ss,tt,&
       &m2) + 4*(ss**2*(ss + tt) + 4*m2**2*(2*ss + tt) - m2*(6*ss**2 + ss*tt + 2*tt**2))&
       &*GIJK(1,2,3,x,ss,tt,m2) + 4*(6*m2 - ss - tt)*tt*Gp13(x,ss,tt,m2) + 4*(8*m2**2 - &
       &6*m2*ss + ss*(ss + tt))*Gp133(x,ss,tt,m2) - 4*(6*m2 - ss - tt)*tt*Gp2(x,ss,tt,m2&
       &)
    mbxt = (8*alpha**3*Pi)*mbxt/(tt*(-4*m2 + ss + tt))
  END FUNCTION

  FUNCTION MXBXT(x,ss,tt,m2)
    real(kind=prec) :: mxbxt
    real(kind=prec), intent(in) :: x,ss,tt,m2

    mxbxt = (256*m2**4 - 224*m2**3*(2*ss + tt) + 8*m2**2*(36*ss**2 + 42*ss*tt + 8*tt**2) + 2&
        &*(ss + tt)*(4*ss**3 + 8*ss**2*tt + 7*ss*tt**2 + 2*tt**3) - 8*m2*(10*ss**3 + 20*s&
        &s**2*tt + 14*ss*tt**2 + 3*tt**3))*G012K(4,x,ss,tt,m2) - 2*(2*m2 - ss)*(4*m2 - ss&
        & - 2*tt)*GIJ(0,1,4,x,ss,tt,m2) + 4*(2*m2 - ss)*(4*m2 - ss - tt)*GIJ(0,2,4,x,ss,t&
        &t,m2) - 2*(2*m2 - ss)*(4*m2 - ss - 2*tt)*GIJ(0,4,4,x,ss,tt,m2) - 2*(2*m2 - ss)*(&
        &4*m2 - ss - tt)*GIJ(1,2,4,x,ss,tt,m2) + 2*(2*m2 - ss)*(4*m2 - ss - 3*tt)*GIJ(1,4&
        &,4,x,ss,tt,m2) - 2*(2*m2 - ss)*(4*m2 - ss - tt)*GIJ(2,4,4,x,ss,tt,m2) - 2*(96*m2&
        &**3 - 4*ss**3 - 9*ss**2*tt - 8*ss*tt**2 - 2*tt**3 - 4*m2**2*(26*ss + 11*tt) + 4*&
        &m2*(9*ss**2 + 11*ss*tt + 4*tt**2))*GIJK(0,1,2,x,ss,tt,m2) + (128*m2**3 + 40*m2*s&
        &s*(ss + tt) - 2*ss*(ss + tt)*(2*ss + tt) - 16*m2**2*(8*ss + 3*tt))*GIJK(0,1,4,x,&
        &ss,tt,m2) - 2*(96*m2**3 - 4*ss**3 - 9*ss**2*tt - 8*ss*tt**2 - 2*tt**3 - 4*m2**2*&
        &(26*ss + 11*tt) + 4*m2*(9*ss**2 + 11*ss*tt + 4*tt**2))*GIJK(0,2,4,x,ss,tt,m2) + &
        &2*(64*m2**3 - 2*ss*(ss + tt)**2 - 32*m2**2*(2*ss + tt) + 2*m2*(10*ss**2 + 13*ss*&
        &tt + tt**2))*GIJK(1,2,4,x,ss,tt,m2) + 4*(2*m2 - ss)*(4*m2 - ss - tt)*Gp134(x,ss,&
        &tt,m2) + 4*(-2*m2 + ss)*tt*Gp14(x,ss,tt,m2) + 4*(-2*m2 + ss)*tt*Gp4(x,ss,tt,m2)
    mxbxt = (8*alpha**3*Pi)*mxbxt/(tt*(-4*m2 + ss + tt))
  END FUNCTION

  FUNCTION VPCONVOLUTE(kernel,sub,tt)
    real(kind=prec) :: vpconvolute
    real(kind=prec), intent(in) :: kernel,sub,tt

    vpconvolute = (deltalph_0_tot(-qq2) - sub*deltalph_0_tot(tt))*kernel
  END FUNCTION


  FUNCTION GIJK(i,j,k,x,ss,tt,m2)
    real(kind=prec) :: gijk
    integer, intent(in) :: i,j,k
    real(kind=prec), intent(in) :: x,ss,tt,m2
    real(kind=prec) :: kernel

    if(i==0) then
      kernel = - d(j,k,x,ss,tt,m2)
      gijk = vpconvolute(kernel,0._prec,tt)
    else
      kernel = qq2*d(i,j,k,x,ss,tt,m2)
      gijk = vpconvolute(kernel,1._prec,tt)
    endif
  END FUNCTION


  FUNCTION GIJ(i,j,k,x,ss,tt,m2)
    real(kind=prec) :: gij
    integer, intent(in) :: i,j,k
    real(kind=prec), intent(in) :: x,ss,tt,m2 
    real(kind=prec) :: kernel

    if(i==0) then
      kernel = - d(j,x,ss,tt,m2) + qq2**2*d(1,2,k,x,ss,tt,m2)
    else
      kernel = qq2*d(i,j,x,ss,tt,m2) + qq2**2*d(1,2,k,x,ss,tt,m2)
    endif
    gij = vpconvolute(kernel,1._prec,tt)
  END FUNCTION

  FUNCTION G012K(k,x,ss,tt,m2)
    real(kind=prec) :: g012k
    integer, intent(in) :: k
    real(kind=prec), intent(in) :: x,ss,tt,m2 
    real(kind=prec) :: kernel, sub

    kernel = - d(1,2,k,x,ss,tt,m2)
    sub = 2*m2*x**2/(tt*(x - 1) + m2*x**2)
    g012k = vpconvolute(kernel,sub,tt)
  END FUNCTION

  FUNCTION GP13(x,ss,tt,m2)
    real(kind=prec) :: gp13
    real(kind=prec), intent(in) :: x,ss,tt,m2
    real(kind=prec) :: kernel
    
    kernel = -((4*m2 - 2*ss - tt)*d(2,x,ss,tt,m2) + ss*d(3,x,ss,tt,m2)&
            - (ss*tt +qq2*(-4*m2 + ss + tt))*d(2,3,x,ss,tt,m2))/(2.*(4*m2 - tt))
    gp13 = vpconvolute(kernel,0._prec,tt)
  END FUNCTION

  FUNCTION GP14(x,ss,tt,m2)
    real(kind=prec) :: gp14
    real(kind=prec), intent(in) :: x,ss,tt,m2
    real(kind=prec) :: uu, kernel
    
    uu = 4*m2-ss-tt
    kernel = -((4*m2 - tt - 2*uu)*d(2,x,ss,tt,m2) + uu*d(4,x,ss,tt,m2) - (tt*uu +&
           &qq2*(-4*m2 + tt + uu))*d(2,4,x,ss,tt,m2))/(2.*(4*m2 - tt))
    gp14 = vpconvolute(kernel,0._prec,tt)
  END FUNCTION

  FUNCTION GP2(x,ss,tt,m2)
    real(kind=prec) :: gp2
    real(kind=prec), intent(in) :: x,ss,tt,m2
    real(kind=prec) :: kernel
    
    kernel = (ss*d(1,x,ss,tt,m2) + (4*m2 - 2*ss - tt)*d(2,x,ss,tt,m2) - (ss*tt + &
           &qq2*(-4*m2 + ss + tt))*d(1,2,x,ss,tt,m2))/(2.*(4*m2 - tt))
    gp2 = vpconvolute(kernel,0._prec,tt)
  END FUNCTION

  FUNCTION GP4(x,ss,tt,m2)
    real(kind=prec) :: gp4
    real(kind=prec), intent(in) :: x,ss,tt,m2
    real(kind=prec) :: uu,kernel
    
    uu = 4*m2-ss-tt
    kernel = -(uu*d(1,x,ss,tt,m2) + (4*m2 - tt - 2*uu)*d(2,x,ss,tt,m2) - (tt*uu +&
           &qq2*(-4*m2 + tt + uu))*d(1,2,x,ss,tt,m2))/(2.*(4*m2 - tt))   
    gp4 = vpconvolute(kernel,0._prec,tt)
  END FUNCTION

  FUNCTION GP133(x,ss,tt,m2)
    real(kind=prec) :: gp133
    real(kind=prec), intent(in) :: x,ss,tt,m2
    real(kind=prec) :: kernel
    
    kernel = (tt*(ss*(d(1,x,ss,tt,m2) + d(3,x,ss,tt,m2)) + 2*qq2*ss*d(1,3,x,ss,tt,&
           &m2)))/(2.*ss*(-4*m2 + ss))
    gp133 = vpconvolute(kernel,0._prec,tt)
  END FUNCTION

  FUNCTION GP134(x,ss,tt,m2)
    real(kind=prec) :: gp134
    real(kind=prec), intent(in) :: x,ss,tt,m2
    real(kind=prec) :: uu,kernel
    
    uu = 4*m2-ss-tt
    kernel = (tt*(uu*(d(1,x,ss,tt,m2) + d(4,x,ss,tt,m2)) + 2*qq2*uu*d(1,4,x,ss,tt,&
           &m2)))/(2.*uu*(-4*m2 + uu))
    gp134 = vpconvolute(kernel,0._prec,tt)
  END FUNCTION


  FUNCTION DI(i,x,ss,tt,m2)
    real(kind=prec) :: di
    integer, intent(in) :: i
    real(kind=prec), intent(in) :: x,ss,tt,m2

    if(i==1) then
      di = d1(x,ss,tt,m2)
    elseif(i==2) then 
      di = d2(x,ss,tt,m2)
    elseif(i==3) then 
      di = d1(x,ss,tt,m2)
    elseif(i==4) then 
      di = d1(x,ss,tt,m2)
    else
      call crash("DI")
    endif
  END FUNCTION

  FUNCTION DIJ(i,j,x,ss,tt,m2)
    real(kind=prec) :: dij
    integer, intent(in) :: i,j
    real(kind=prec), intent(in) :: x,ss,tt,m2

    if(i==1 .and. j==2) then
      dij = d12(x,ss,tt,m2)
    elseif(i==2 .and. j==3) then 
      dij = d12(x,ss,tt,m2)
    elseif(i==2 .and. j==4) then 
      dij = d12(x,ss,tt,m2)
    elseif(i==1 .and. j==3) then 
      dij = d13(x,ss,tt,m2)
    elseif(i==1 .and. j==4) then 
      dij = d14(x,ss,tt,m2)
    else
      call crash("DIJ")
    endif
  END FUNCTION



  FUNCTION DIJK(i,j,k,x,ss,tt,m2)
    real(kind=prec) :: dijk
    integer, intent(in) :: i,j,k
    real(kind=prec), intent(in) :: x,ss,tt,m2

    if(i==1 .and. j==2 .and. k==3) then
      dijk = d123(x,ss,tt,m2)
    elseif(i==1 .and. j==2 .and. k==4) then
      dijk = d124(x,ss,tt,m2)
    else
      call crash("DIJK")
    endif
  END FUNCTION


  FUNCTION D1(x,ss,tt,m2)
    real(kind=prec) :: d1
    real(kind=prec), intent(in) :: x,ss,tt,m2

    d1 = -1 + 1/(x-1)
  END FUNCTION

  FUNCTION D2(x,ss,tt,m2)
    real(kind=prec) :: d2
    real(kind=prec), intent(in) :: x,ss,tt,m2
    real(kind=prec) :: zt2

    zt2 = m2/abs(tt)
    if(qq2 < -tt) then
      d2 = x*zt2*(x-2)/(x-1)**2
    else
      d2 = 1/(x - 1) - 2/x
    end if
  END FUNCTION

  FUNCTION D12(x,ss,tt,m2)
   real(kind=prec) :: d12
   real(kind=prec), intent(in) :: x,ss,tt,m2
   real(kind=prec) :: zt2,pref,xpeak,arg1,arg2

   zt2 = m2/abs(tt)
   pref = tt

   if(1-x<min(2E-1*zt2,1E-1)) then
     d12 = -(60*(-1 + x)**6 + 10*zt2*(-1 + x)**5*(-93 + 86*x) + 4*zt2**2*(-1 + x)**4*(1455 &
         &+ x*(-2651 + 1217*x)) + 3*zt2**3*(-1 + x)**3*(-6285 + 2*x*(8402 + x*(-7576 + 229&
         &9*x))) + 14*zt2**4*(-1 + x)**2*(2415 + x*(-8311 + 2*x*(5467 + x*(-3245 + 731*x))&
         &)) + 70*zt2**5*(-1 + x)*(-483 + x*(1946 + x*(-3255 + x*(2796 + x*(-1225 + 218*x)&
         &)))) + 420*zt2**6*(49 + x*(-203 + x*(385 + x*(-413 + x*(259 + x*(-89 + 13*x)))))&
         &))/(420.*zt2**7)
     d12 = d12/pref
     return
    endif

   xpeak = (-1 + Sqrt(1 + 4*zt2))/(2.*zt2)
   if(abs(x-xpeak)<3E-1*min(1/zt2,1-xpeak)) then
     arg1 = (zt2*(-1 + x + zt2*x**2))/((1 + 4*zt2)*(-1 + x))
     d12 = -(((1 - x)*(-2 + x)*(((1 + 2*zt2)*(-1 + x + zt2*x**2))/((1 + 4*zt2)*(-1 + x)) + &
         &((1 + 4*zt2 + 6*zt2**2)*(-1 + x + zt2*x**2)**2)/(2.*(1 + 4*zt2)**2*(-1 + x)**2) &
         &+ ((1 + 6*zt2 + 18*zt2**2 + 20*zt2**3)*(-1 + x + zt2*x**2)**3)/(3.*(1 + 4*zt2)**&
         &3*(-1 + x)**3) + ((1 + 8*zt2 + 36*zt2**2 + 80*zt2**3 + 70*zt2**4)*(-1 + x + zt2*&
         &x**2)**4)/(4.*(1 + 4*zt2)**4*(-1 + x)**4) + ((1 + 2*zt2)*(1 + 8*zt2 + 44*zt2**2 &
         &+ 112*zt2**3 + 126*zt2**4)*(-1 + x + zt2*x**2)**5)/(5.*(1 + 4*zt2)**5*(-1 + x)**&
         &5) + ((1 + 2*zt2*(6 + 45*zt2 + 200*zt2**2 + 525*zt2**3 + 756*zt2**4 + 462*zt2**5&
         &))*(-1 + x + zt2*x**2)**6)/(6.*(1 + 4*zt2)**6*(-1 + x)**6) + log(abs(arg1))))/(S&
         &qrt(1 + 4*zt2)*(-1 + x)**2*x))    
   else
     arg1 = (2 - 2*(1 + Sqrt(1 + 4*zt2))*x + (1 + 2*zt2 + Sqrt(1 + 4*zt2))*x**2)/(2 + x*(-2 &
        &+ 2*Sqrt(1 + 4*zt2) + x + 2*zt2*x - Sqrt(1 + 4*zt2)*x))
     d12 = ((-2 + x)*log(abs(arg1)))/(2.*Sqrt(1 + 4*zt2)*(-1 + x)*x)
   endif

   if(qq2 > -tt) then
     arg2 = (1 + 2*zt2 + Sqrt(1 + 4*zt2))/(2.*zt2)
     d12 = d12 + ((-2 + x)*log(abs(arg2)))/(Sqrt(1 + 4*zt2)*(-1 + x)*x)
   end if

   d12 = d12/pref
  END FUNCTION

  FUNCTION D13(x,ss,tt,m2)
   real(kind=prec) :: d13
   real(kind=prec), intent(in) :: x,ss,tt,m2
   real(kind=prec) :: zs2,pref,arg1,arg2

   zs2 = m2/ss
   pref = ss

   if(1-x<1E-2*zs2) then
     d13 = -(10*(-1 + x)**5 - 4*zs2*(-1 + x)**4*(-16 + 19*x) + 60*zs2**5*(-2 + x)*(3 + (-3 &
         &+ x)*x)*(1 + (-1 + x)*x) + 3*zs2**2*(-1 + x)**3*(49 + 22*x*(-5 + 3*x)) - 2*zs2**&
         &3*(-1 + x)**2*(-57 + x*(207 + 14*x*(-17 + 7*x))) + 10*zs2**4*(-1 + x)*(9 + x*(-2&
         &4 + x*(35 + x*(-24 + 7*x)))))/(60.*zs2**6)
     d13 = d13/pref
     return
    endif

   arg1 = -((1 + Sqrt(1 - 4*zs2) - 2*zs2)/(-1 + Sqrt(1 - 4*zs2) + 2*zs2))
   arg2 = (2*Sqrt(1 - 4*zs2) + x - Sqrt(1 - 4*zs2)*x)/(-2*Sqrt(1 - 4*zs2) + x + Sqrt(1 - 4&
        &*zs2)*x)

   d13 = -((-2 + x)*(log(abs(arg1)) - 2*log(abs(arg2))))/(2.*Sqrt(1 - 4*zs2)*(-1 + x)*x)
   d13 = d13/pref
  END FUNCTION


  FUNCTION D14(x,ss,tt,m2)
   real(kind=prec) :: d14
   real(kind=prec), intent(in) :: x,ss,tt,m2
   real(kind=prec) :: zst2,pref,arg1,arg2

   zst2 = m2/(ss + tt)
   pref = ss + tt

   if(1-x<1E-3*zst2) then
     d14 = (12*(-1 + x)**4 - 3*zst2*(-1 + x)**3*(-47 + 42*x) + 2*zst2**2*(-1 + x)**2*(311 +&
         & x*(-537 + 236*x)) - 10*zst2**3*(-1 + x)*(-130 + x*(316 + x*(-265 + 76*x))) + 60&
         &*zst2**4*(25 + x*(-70 + x*(80 + x*(-43 + 9*x)))))/(60.*zst2**5)
     d14 = d14/pref
     return
    endif

   arg1 = -((-1 + Sqrt(1 - 4*zst2) + 2*zst2)/(1 + Sqrt(1 - 4*zst2) - 2*zst2))
   arg2 = (2 + (-1 + Sqrt(1 - 4*zst2))*x)/(-2 + x + Sqrt(1 - 4*zst2)*x)

   d14 = -((-2 + x)*(log(abs(arg1)) + 2*log(abs(arg2))))/(2.*Sqrt(1 - 4*zst2)*(-1 + x)*x)
   d14 = d14/pref
  END FUNCTION


  FUNCTION D123(x,ss,tt,m2)
   real(kind=prec) :: d123
   real(kind=prec), intent(in) :: x,ss,tt,m2
   real(kind=prec) :: zs2,pref,sigma,arg1,arg2

   zs2 = m2/ss
   sigma = sign(1._prec, qq2 + tt)
   pref = sqrt(ss*(ss - 4*m2))*sigma*(qq2 + tt)

   if(1-x<1E-3*zs2 .and. sigma > 0) then
     d123 = ((-1 + Sqrt(1 - 4*zs2) + 4*zs2)*(12*(-1 + x)**4 - 3*zs2*(-1 + x)**3*(-17 + 22*x)&
          & + 2*zs2**2*(-1 + x)**2*(41 + x*(-87 + 56*x)) - 10*zs2**3*(-1 + x)*(2 + x*(4 + x&
          &*(-7 + 4*x))) + 60*zs2**4*(5 + x*(-10 + x*(10 + (-5 + x)*x)))))/(60.*(-1 + Sqrt(&
          &1 - 4*zs2))*zs2**5)
     d123 = d123/pref
     return
    endif

   arg1 = -((1 + Sqrt(1 - 4*zs2) - 2*zs2)/(-1 + Sqrt(1 - 4*zs2) + 2*zs2))
   arg2 = (2*Sqrt(1 - 4*zs2)*sigma + x - Sqrt(1 - 4*zs2)*sigma*x)/(-2*Sqrt(1 - 4*zs2)*sigma&
        &+ x + Sqrt(1 - 4*zs2)*sigma*x)

   d123 = ((-2 + x)*(log(abs(arg1)) - 2*log(abs(arg2))))/(2.*(-1 + x)*x)
   d123 = d123/pref
   END FUNCTION


  FUNCTION D124(x,ss,tt,m2)
   real(kind=prec) :: d124
   real(kind=prec), intent(in) :: x,ss,tt,m2
   real(kind=prec) :: zst2,pref,sigma,arg1,arg2
   
   zst2 = m2/(ss + tt)
   sigma = sign(1._prec, qq2 + tt)
   pref = sqrt((ss + tt)*(ss + tt - 4*m2))*sigma*(qq2 + tt)

   if(1-x<1E-2*zst2 .and. sigma > 0) then
     d124 = -((-1 + Sqrt(1 - 4*zst2) + 4*zst2)*(-10*(-1 + x)**5 + 4*zst2*(-1 + x)**4*(-34 + &
          &31*x) - 3*zst2**2*(-1 + x)**3*(241 + 2*x*(-215 + 97*x)) + 2*zst2**3*(-1 + x)**2*&
          &(-953 + x*(2463 - 2162*x + 642*x**2)) - 10*zst2**4*(-1 + x)*(265 + x*(-856 + x*(&
          &1075 + x*(-616 + 135*x)))) + 60*zst2**5*(-36 + x*(125 + x*(-190 + x*(153 + x*(-6&
          &4 + 11*x)))))))/(60.*(-1 + Sqrt(1 - 4*zst2))*zst2**6)     
     d124 = d124/pref
     return
    endif   

   arg1 = (1 + Sqrt(1 - 4*zst2) - 2*zst2)/(-1 + Sqrt(1 - 4*zst2) + 2*zst2)
   arg2 = (-(sigma*(-2 + x)) + Sqrt(1 - 4*zst2)*x)/(sigma*(-2 + x) + Sqrt(1 - 4*zst2)*x)

   d124 = ((-2 + x)*(-log(abs(arg1)) + 2*log(abs(arg2))))/(2.*(-1 + x)*x)
   
   d124 = d124/pref
  END FUNCTION


                     !!!!!!!!!!!!!!!!!!!!!!
                     END MODULE EE_EE2EE_NFBX
                     !!!!!!!!!!!!!!!!!!!!!!
